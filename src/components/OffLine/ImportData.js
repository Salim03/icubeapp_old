import React, {useState} from 'react';
import {View, ScrollView, Text} from 'react-native';
import CheckInput from '../../components/Custom/CheckInput';
import {Button} from '../../components/Custom/Button';
import {AlertMessage, isnull, Request} from '../../Helpers/HelperMethods';
import {GetSessionData, IsServerConnected} from '../../Helpers/HelperMethods';
import ICubeAIndicator from '../Tools/ICubeIndicator';
import {
  SaveSupplier,
  SaveDepartment,
  SaveItemName,
  SaveCategoryGroup,
  SaveCategories,
  SaveCategoryCaption,
  SaveLocation,
  FlushSupplier,
  FlushItemName,
  FlushDepartment,
  FlushCategoryGroup,
  FlushCategories,
} from '../../Data/DB/Purchase';
import {globalColorObject} from '../../Style/GlobalStyle';
import {useEffect} from 'react';
import LayoutWrapper, {layoutStyle} from '../Layout/Layout';

const ImportData = props => {
  const [downloadState, setDownloadState] = useState({
    state: 'Initial',
    InfoMessage: '',
  });
  const [APIData, setAPIData] = useState({
    IP: '',
    Token: '',
    UserCode: '',
    Loccode: 0,
  });
  const [refreshCount, setrefreshCount] = useState(0);
  const [isSupplier, setisSupplier] = useState(true);
  const [isDepartment, setisDepartment] = useState(true);
  const [isItemName, setisItemName] = useState(true);
  const [isCategoryGroup, setisCategoryGroup] = useState(true);
  const [isCategory1, setisCategory1] = useState(true);
  const [isCategory2, setisCategory2] = useState(true);
  const [isCategory3, setisCategory3] = useState(true);
  const [isCategory4, setisCategory4] = useState(true);
  const [isCategory5, setisCategory5] = useState(true);
  const [isCategory6, setisCategory6] = useState(true);
  const [isCategory7, setisCategory7] = useState(false);
  const [isCategory8, setisCategory8] = useState(false);
  const [isCategory9, setisCategory9] = useState(false);
  const [isCategory10, setisCategory10] = useState(false);
  const [isCategory11, setisCategory11] = useState(false);
  const [isCategory12, setisCategory12] = useState(false);
  const [isCategory13, setisCategory13] = useState(false);
  const [isCategory14, setisCategory14] = useState(false);
  const [isCategory15, setisCategory15] = useState(false);
  const [isCategory16, setisCategory16] = useState(false);

  useEffect(() => {
    getSessionData();
  }, []);

  const getSessionData = async () => {
    let {get_IP, get_Token, get_Location, get_UserCode} =
      await GetSessionData();
    setAPIData({
      IP: get_IP,
      Token: get_Token,
      UserCode: get_UserCode,
      Loccode: get_Location,
    });
  };

  const FlushData = async () => {
    let CategoryList = `${isCategory1 ? 'Category1,' : ''}${
      isCategory2 ? 'Category2,' : ''
    }${isCategory3 ? 'Category3,' : ''}${isCategory4 ? 'Category4,' : ''}${
      isCategory5 ? 'Category5,' : ''
    }${isCategory6 ? 'Category6,' : ''}${isCategory7 ? 'Category7,' : ''}${
      isCategory8 ? 'Category8,' : ''
    }${isCategory9 ? 'Category9,' : ''}${isCategory10 ? 'Category10,' : ''}${
      isCategory11 ? 'Category11,' : ''
    }${isCategory12 ? 'Category12,' : ''}${isCategory13 ? 'Category13,' : ''}${
      isCategory14 ? 'Category14,' : ''
    }${isCategory15 ? 'Category15,' : ''}${isCategory16 ? 'Category16,' : ''}`;
    if (
      isSupplier ||
      isDepartment ||
      isItemName ||
      isCategoryGroup ||
      CategoryList != ''
    ) {
      setDownloadState({
        state: 'Flushing',
        InfoMessage: 'Flushing Data ...',
      });
      let ErrorLog = '';
      if (isSupplier) {
        setDownloadState({state: 'Save', InfoMessage: 'Supplier Flushing ...'});
        let get_saveSupplierStatus = await FlushSupplier().catch(err => {
          return err;
        });
        ErrorLog += get_saveSupplierStatus != 'Saved' ? 'Supplier, ' : '';
      }
      if (isDepartment) {
        setDownloadState({
          state: 'Save',
          InfoMessage: 'Department Flushing ...',
        });
        let get_saveDepartmentStatus = await FlushDepartment().catch(err => {
          return err;
        });
        ErrorLog += get_saveDepartmentStatus != 'Saved' ? 'Department, ' : '';
      }
      if (isItemName) {
        setDownloadState({
          state: 'Save',
          InfoMessage: 'Item Name Flushing ...',
        });
        let get_saveItemNameStatus = await FlushItemName().catch(err => {
          return err;
        });
        ErrorLog += get_saveItemNameStatus != 'Saved' ? 'ItemName, ' : '';
      }
      if (isCategoryGroup) {
        setDownloadState({
          state: 'Save',
          InfoMessage: 'Category Group Flushing ...',
        });
        let get_saveCategoryGroupStatus = await FlushCategoryGroup().catch(
          err => {
            return err;
          },
        );
        ErrorLog +=
          get_saveCategoryGroupStatus != 'Saved' ? 'Category Group, ' : '';
      }
      if (CategoryList != '') {
        setDownloadState({
          state: 'Save',
          InfoMessage: 'Categories Flushing ...',
        });
        let get_saveCategoriesStatus = await FlushCategories(
          CategoryList,
        ).catch(err => {
          return err;
        });
        ErrorLog += get_saveCategoriesStatus != 'Saved' ? 'Categories, ' : '';
      }
      setDownloadState({state: 'Initial', InfoMessage: ''});
      if (ErrorLog != '') {
        AlertMessage('Failed to Flush ' + ErrorLog);
      } else {
        AlertMessage('Flush data successfully.');
      }
    } else {
      AlertMessage('select any item to Flush.');
    }
  };

  const FetchData = async () => {
    if (await IsServerConnected(true)) {
      let CategoryList = `${isCategory1 ? 'Category1,' : ''}${
        isCategory2 ? 'Category2,' : ''
      }${isCategory3 ? 'Category3,' : ''}${isCategory4 ? 'Category4,' : ''}${
        isCategory5 ? 'Category5,' : ''
      }${isCategory6 ? 'Category6,' : ''}${isCategory7 ? 'Category7,' : ''}${
        isCategory8 ? 'Category8,' : ''
      }${isCategory9 ? 'Category9,' : ''}${isCategory10 ? 'Category10,' : ''}${
        isCategory11 ? 'Category11,' : ''
      }${isCategory12 ? 'Category12,' : ''}${
        isCategory13 ? 'Category13,' : ''
      }${isCategory14 ? 'Category14,' : ''}${
        isCategory15 ? 'Category15,' : ''
      }${isCategory16 ? 'Category16,' : ''}`;
      if (
        isSupplier ||
        isDepartment ||
        isItemName ||
        isCategoryGroup ||
        CategoryList != ''
      ) {
        let PassURI = `${
          APIData.IP
        }/api/Purchase/OffLinePurchaseImport?isSupplier=${isSupplier}&isDepartment=${isDepartment}&isItemName=${isItemName}&isCategoryGroup=${isCategoryGroup}&isCategories=${
          CategoryList != ''
        }&CategoriesList=${CategoryList}&EmployeeId=${
          APIData.UserCode
        }&Loccode=${APIData.Loccode}`;
        // console.log(PassURI);
        setDownloadState({
          state: 'Downloading',
          InfoMessage: 'Downloading Data ...',
        });
        let res = await Request.get(PassURI, APIData.Token);
        let ErrorLog = '';
        // console.log('Data : ', res.data);
        if (res.data.Supplier) {
          setDownloadState({state: 'Save', InfoMessage: 'Supplier Saving ...'});
          let get_saveSupplierStatus = await SaveSupplier(
            res.data.Supplier,
          ).catch(err => {
            return err;
          });
          ErrorLog += get_saveSupplierStatus != 'Saved' ? 'Supplier, ' : '';
        }
        if (res.data.Department) {
          setDownloadState({
            state: 'Save',
            InfoMessage: 'Department Saving ...',
          });
          let get_saveDepartmentStatus = await SaveDepartment(
            res.data.Department,
          ).catch(err => {
            return err;
          });

          ErrorLog += get_saveDepartmentStatus != 'Saved' ? 'Department, ' : '';
        }
        if (res.data.ItemName) {
          setDownloadState({
            state: 'Save',
            InfoMessage: 'Item Name Saving ...',
          });
          let get_saveItemNameStatus = await SaveItemName(
            res.data.ItemName,
          ).catch(err => {
            return err;
          });
          ErrorLog += get_saveItemNameStatus != 'Saved' ? 'ItemName, ' : '';
        }
        if (res.data.CategoryGroup) {
          setDownloadState({
            state: 'Save',
            InfoMessage: 'Category Group Saving ...',
          });
          let get_saveCategoryGroupStatus = await SaveCategoryGroup(
            res.data.CategoryGroup,
          ).catch(err => {
            return err;
          });
          ErrorLog +=
            get_saveCategoryGroupStatus != 'Saved' ? 'Category Group, ' : '';
        }
        if (res.data.CategoryCaption) {
          setDownloadState({
            state: 'Save',
            InfoMessage: 'Category Caption Saving ...',
          });
          let get_saveCategoryCaptionStatus = await SaveCategoryCaption(
            res.data.CategoryCaption,
          ).catch(err => {
            return err;
          });
          ErrorLog +=
            get_saveCategoryCaptionStatus != 'Saved'
              ? 'Category Caption, '
              : '';
        }
        if (res.data.Categories) {
          setDownloadState({
            state: 'Save',
            InfoMessage: 'Categories Saving ...',
          });
          let get_saveCategoriesStatus = await SaveCategories(
            res.data.Categories,
          ).catch(err => {
            return err;
          });
          ErrorLog += get_saveCategoriesStatus != 'Saved' ? 'Categories, ' : '';
        }
        if (res.data.Location) {
          setDownloadState({state: 'Save', InfoMessage: 'Location Saving ...'});
          let get_saveCategoriesStatus = await SaveLocation(
            res.data.Location,
          ).catch(err => {
            return err;
          });
          ErrorLog += get_saveCategoriesStatus != 'Saved' ? 'Location, ' : '';
        }
        setDownloadState({state: 'Initial', InfoMessage: ''});
        if (ErrorLog != '') {
          AlertMessage('Failed to Import ' + ErrorLog);
        } else {
          AlertMessage('Import data successfully.');
        }
      } else {
        AlertMessage('select any item to download.');
      }
    }
  };

  return (
    <>
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={() => {
          setrefreshCount(refreshCount + 1);
        }}>
        <ScrollView style={{flex: 1}}>
          <View style={[layoutStyle.ScrollContentWrapper]}>
            <CheckInput
              placeholder="Supplier"
              value={isSupplier}
              onValueChange={changevalue => setisSupplier(changevalue)}
            />
            <CheckInput
              placeholder="Department"
              value={isDepartment}
              onValueChange={changevalue => setisDepartment(changevalue)}
            />
            <CheckInput
              placeholder="Item Name"
              value={isItemName}
              onValueChange={changevalue => setisItemName(changevalue)}
            />
            <CheckInput
              placeholder="Category Group"
              value={isCategoryGroup}
              onValueChange={changevalue => setisCategoryGroup(changevalue)}
            />
            <CheckInput
              placeholder="Category 1"
              value={isCategory1}
              onValueChange={changevalue => setisCategory1(changevalue)}
            />
            <CheckInput
              placeholder="Category 2"
              value={isCategory2}
              onValueChange={changevalue => setisCategory2(changevalue)}
            />
            <CheckInput
              placeholder="Category 3"
              value={isCategory3}
              onValueChange={changevalue => setisCategory3(changevalue)}
            />
            <CheckInput
              placeholder="Category 4"
              value={isCategory4}
              onValueChange={changevalue => setisCategory4(changevalue)}
            />
            <CheckInput
              placeholder="Category 5"
              value={isCategory5}
              onValueChange={changevalue => setisCategory5(changevalue)}
            />
            <CheckInput
              placeholder="Category 6"
              value={isCategory6}
              onValueChange={changevalue => setisCategory6(changevalue)}
            />
            <CheckInput
              placeholder="Category 7"
              value={isCategory7}
              onValueChange={changevalue => setisCategory7(changevalue)}
            />
            <CheckInput
              placeholder="Category 8"
              value={isCategory8}
              onValueChange={changevalue => setisCategory8(changevalue)}
            />
            <CheckInput
              placeholder="Category 9"
              value={isCategory9}
              onValueChange={changevalue => setisCategory9(changevalue)}
            />
            <CheckInput
              placeholder="Category 10"
              value={isCategory10}
              onValueChange={changevalue => setisCategory10(changevalue)}
            />
            <CheckInput
              placeholder="Category 11"
              value={isCategory11}
              onValueChange={changevalue => setisCategory11(changevalue)}
            />
            <CheckInput
              placeholder="Category 12"
              value={isCategory12}
              onValueChange={changevalue => setisCategory12(changevalue)}
            />
            <CheckInput
              placeholder="Category 13"
              value={isCategory13}
              onValueChange={changevalue => setisCategory13(changevalue)}
            />
            <CheckInput
              placeholder="Category 14"
              value={isCategory14}
              onValueChange={changevalue => setisCategory14(changevalue)}
            />
            <CheckInput
              placeholder="Category 15"
              value={isCategory15}
              onValueChange={changevalue => setisCategory15(changevalue)}
            />
            <CheckInput
              placeholder="Category 16"
              value={isCategory16}
              onValueChange={changevalue => setisCategory16(changevalue)}
            />
          </View>
        </ScrollView>
        {downloadState.state == 'Initial' && (
          <View
            style={{
              paddingHorizontal: 5,
              flexDirection: 'row',
            }}>
            <View style={{flex: 1}}>
              <Button value="Flush" onPressEvent={FlushData} />
            </View>
            <View style={{flex: 1}}>
              <Button value="Download" onPressEvent={FetchData} />
            </View>
          </View>
        )}
      </LayoutWrapper>
      <ICubeAIndicator
        isShow={isnull(downloadState.InfoMessage, '') != ''}
        ModalText={isnull(downloadState.InfoMessage, '')}
      />
    </>
  );
};

export {ImportData};
