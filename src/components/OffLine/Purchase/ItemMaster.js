import React, {useEffect, useState, useLayoutEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Button,
  TouchableOpacity,
  Modal,
  Image,
  TouchableHighlight,
  BackHandler,
  Platform,
  StatusBar,
} from 'react-native';

import CheckInput from '../../../components/Custom/CheckInput';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import ImagePicker from 'react-native-image-picker';

import ModalDatePicker from '../../Custom/ModalDatePicker';
import RichTextInput from '../../Custom/RichTextInput';
import {AccordionStyle} from '../../Custom/Accordion';
import {
  AlertMessage,
  Confirmation,
  getLayoutImageDimension,
  DocumentDirectoryFolder,
  isnull,
} from '../../../Helpers/HelperMethods';
import LayoutWrapper, {layoutStyle} from '../../Layout/Layout';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import {globalColorObject, ApplyStyleColor} from '../../../Style/GlobalStyle';
import {
  GetAllCombinedItemMasterData,
  GetNewUUID,
} from '../../../Data/DB/Purchase';
import RNFetchBlob from 'rn-fetch-blob';
import {FlatList} from 'react-native-gesture-handler';

// to hold the Image Dimension value
let ImageDimension;

const ItemMaster = ({navigation, route}) => {
  // class variables
  const visibleProductGroupFields = [
    'Department',
    'ItemName',
    'OemCode',
    'MfgDate',
  ];
  // const visibleCategoryGroupFields = [
  //   'Cat1',
  //   'Cat2',
  //   'Cat3',
  //   'Cat4',
  //   'Cat5',
  //   'Cat6',
  //   'Cat7',
  //   'Cat8',
  //   'Cat9',
  //   'Cat10',
  //   'Cat11',
  //   'Cat12',
  //   'Cat13',
  //   'Cat14',
  //   'Cat15',
  //   'Cat16',
  // ];
  const visiblePriceGroupFields = [
    'Quantity',
    'Rate',
    'Discount',
    'StdRate',
    'WSP',
    'RSP',
    // 'ListedMRP',
    // 'OnlineMRP',
  ];
  const DefaultValueOfCommonItemInfo = {
    Id: '',
    Cat1: null,
    Cat2: null,
    Cat3: null,
    Cat4: null,
    Cat5: null,
    Cat6: null,
    Cat7: null,
    Cat8: null,
    Cat9: null,
    Cat10: null,
    Cat11: null,
    Cat12: null,
    Cat13: null,
    Cat14: null,
    Cat15: null,
    Cat16: null,
    ...DefaultPriceFields,
  };
  const InitialItemDetails = {
    ...DefaultValueOfCommonItemInfo,
    OemCode: null,
    GroupCode: 0,
    CatGroupType: null,
    CatGroupCode: null,
    ItemName: '',
    MfgDate: null,
    ImageData: [],
    SubItemInfo: [],
  };
  const [ItemDetails, setItemDetails] = useState(InitialItemDetails);
  const DefaultPriceFields = {
    Qty: 0,
    Rate: 0,
    DiscountPer: null,
    DiscountAmt: null,
    StdRate: 0,
    WSP: null,
    RSP: null,
    ListedMRP: null,
    OnlineMRP: null,
  };
  const [CombinedMasterInfo, setCombinedMasterInfo] = useState({
    DepartmentList: [],
    ItemNameList: [],
    CategoryCaptionList: [],
    CategoryGroupList: [],
    CategoriesList: [],
  });
  const [state, setState] = useState({
    visibleCategoryGroupFields: [],
    isEditItem: false,
    isFetched: false,
    ItemDetailsList: [],
    GroupPopUpInput: {...DefaultPriceFields},
    // Properties to hold API data
    APIData: {
      ItemNameList: [],
      Cat1: [],
      Cat2: [],
      Cat3: [],
      Cat4: [],
      Cat5: [],
      Cat6: [],
      Cat7: [],
      Cat8: [],
      Cat9: [],
      Cat10: [],
      Cat11: [],
      Cat12: [],
      Cat13: [],
      Cat14: [],
      Cat15: [],
      Cat16: [],
    },
    // Properties to hold category names to set to the labels ( Example  1: "Brand", 2: "Size" )
    CategoryNames: {
      1: 'Category 1',
      2: 'Category 2',
      3: 'Category 3',
      4: 'Category 4',
      5: 'Category 5',
      6: 'Category 6',
      7: 'Category 7',
      8: 'Category 8',
      9: 'Category 9',
      10: 'Category 10',
      11: 'Category 11',
      12: 'Category 12',
      13: 'Category 13',
      14: 'Category 14',
      15: 'Category 15',
      16: 'Category 16',
    },

    // whether to create single product or groupwise product
    // dependencies for Group wise Product creation
    ShowGroupModal: false,
    CurrentGroupData: [],
    GroupcatCode: {}, // conatins Group Category Code like { 1: 'GR001', 2: 'GR0003' }
    GroupcatName: {}, // conatins Group Category Name like { 1: 'TestGroupColor', 2: 'TestGroupWidth' }
    GroupcatNo: [], // conatins Group Category Number like [1,2]

    // Images for Item
    ItemImages: [],

    // hold whether to show or hide camera
    ShowImagePreview: false,
    PreviewImagePath: '',

    // Whether user is selecting Images or not
    ImageSelection: false,
  });

  // to maintain the ByteArray data of the Images selected

  useEffect(() => {
    // adding event listener for hardware back button press
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    // getting and setting navigation state
    LoadInitialData();
  }, []);

  const ProcessMasterDetails = CombinedMasterInfo => {
    // Categories Iteration
    let Get_CateList = {};
    (CombinedMasterInfo.CategoriesList || []).forEach((objEachData, index) => {
      if (objEachData.List && objEachData.List.length > 0) {
        if (state.APIData[objEachData.Type.replace('Category', 'Cat')]) {
          Get_CateList[objEachData.Type.replace('Category', 'Cat')] =
            objEachData.List;
        }
      }
    });

    // CategoryGroup Iteration
    (CombinedMasterInfo.CategoryGroupList || []).forEach(
      (objEachData, index) => {
        let temp_CatType = objEachData.Type.replace('Category', 'Cat');
        if (state.APIData[temp_CatType]) {
          if (Get_CateList[temp_CatType]) {
            Get_CateList[temp_CatType] = [
              ...Get_CateList[temp_CatType],
              ...objEachData.List,
            ];
          } else {
            Get_CateList[temp_CatType] = objEachData.List;
          }
        }
      },
    );
    let visibleCategoryGroupFields = [];
    let get_CatCaption = {...state.CategoryNames};
    // CategoryCaption Iteration
    (CombinedMasterInfo.CategoryCaptionList || []).forEach(
      (objEachData, index) => {
        let temp_CatName = objEachData.Name.replace('Category', '');
        if (get_CatCaption[temp_CatName]) {
          get_CatCaption[temp_CatName] = objEachData.Caption;
        }
        if (objEachData.isUsed) {
          visibleCategoryGroupFields.push(
            objEachData.Name.replace('Category', 'Cat'),
          );
        }
      },
    );
    setCombinedMasterInfo(CombinedMasterInfo);
    setState(prevState => ({
      ...prevState,
      isFetched: true,
      APIData: {
        ...prevState.APIData,
        ...Get_CateList,
      },
      CategoryNames: get_CatCaption,
      visibleCategoryGroupFields,
    }));
  };

  const LoadInitialData = async () => {
    let {params} = route;
    let EditItemDetails = params.EditItem;
    if (EditItemDetails && isnull(EditItemDetails.Id, '') != '') {
      let get_ItemDetails = {...ItemDetails, ...EditItemDetails};
      let disamt = get_ItemDetails.Rate - get_ItemDetails.StdRate;
      get_ItemDetails.DiscountAmt = disamt;
      get_ItemDetails.DiscountPer =
        isnull(disamt, 0) != 0 ? 100 * (disamt / get_ItemDetails.Rate) : 0;
      let get_ImageData = (EditItemDetails.ImageData || []).map(elem => {
        return {path: elem.Path, selected: false};
      });
      // for (let count = 0; count < get_ImageData.length; count++) {
      //   let image_filePath = get_ImageData[count].path;
      //   let get_IsExists = await RNFetchBlob.fs.exists(image_filePath);
      //   console.log('exist : ', get_IsExists);
      //   if (get_IsExists) {
      //     let get_fileimageData = await RNFetchBlob.fs.readFile(
      //       image_filePath,
      //       'base64',
      //     );
      //     console.log('image get : ', get_fileimageData);
      //     console.log('image orginal : ', get_ImageData[count].data);
      //   }
      // }
      setItemDetails(get_ItemDetails);
      setState(prevState => ({
        ...prevState,
        isEditItem: true,
        ItemImages: get_ImageData,
      }));
    }
    await GetAllCombinedItemMasterData().then(res => {
      ProcessMasterDetails(res);
    });
  };

  useLayoutEffect(() => {
    const isCreate = !state.isEditItem;
    const iconStyle = {width: 30, height: 30};
    navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[{marginRight: 15, overflow: 'hidden'}]}
            onPress={ValidateEnrty.bind(this, isCreate)}>
            <Image
              style={iconStyle}
              source={require('../../../assets/images/save-light.png')}
            />
          </TouchableOpacity>
          {isCreate && (
            <TouchableOpacity
              style={[
                isCreate ? iconStyle : {width: 0, height: 0},
                {marginRight: 15},
              ]}
              onPress={ValidateEnrty.bind(this, false)}>
              <Image
                style={iconStyle}
                source={require('../../../assets/images/plus-light.png')}
              />
            </TouchableOpacity>
          )}
        </View>
      ),
    });
  }, [navigation, ItemDetails, state]);

  const RemoveBackButtonListener = () => {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
  };

  const PrepareItemImagesList = () => {
    return state.ItemImages.map((img, index) => (
      <View key={`img_${index}`} style={styles.ItemImageWrapper}>
        {state.ImageSelection && (
          <TouchableHighlight
            underlayColor={'rgba(0,0,0,0.5)'}
            onPress={ToggleImageSelection.bind(this, index)}
            style={[
              styles.ImageSelector,
              {
                backgroundColor: img.selected
                  ? 'rgba(0,0,0,.7)'
                  : 'rgba(0,0,0,.1)',
              },
            ]}>
            <View
              style={img.selected ? styles.SelectedImage : undefined}></View>
          </TouchableHighlight>
        )}
        <TouchableOpacity
          onPress={PreviewItemImage.bind(this, index)}
          onLongPress={ToggleImageSelection.bind(this, index)}>
          <Image style={styles.ItemImage} source={{uri: img.path}} />
        </TouchableOpacity>
      </View>
    ));
  };

  const PreviewItemImage = index => {
    if (index > -1) {
      setState(prevState => ({
        ...prevState,
        ShowImagePreview: true,
        PreviewImagePath: state.ItemImages[index].path,
      }));
    }
  };

  const CloseImagePreview = () => {
    setState(prevState => ({
      ...prevState,
      ShowImagePreview: false,
      PreviewImagePath: '',
    }));
  };

  const ToggleImageSelection = index => {
    let ItemImages = [...state.ItemImages];
    ItemImages[index].selected = !ItemImages[index].selected;
    setState(prevState => ({...prevState, ImageSelection: true, ItemImages}));
  };

  const RemoveSelectedImages = () => {
    let ItemImages = [...state.ItemImages].filter(item => !item.selected);
    setState(prevState => ({...prevState, ImageSelection: false, ItemImages}));
  };

  const RemoveSelectedImagesPath = () => {
    let ItemImages = [...state.ItemImages].filter(
      item => item.path != state.PreviewImagePath,
    );
    setState(prevState => ({
      ...prevState,
      ShowImagePreview: false,
      ItemImages,
    }));
  };

  const CancelImageSelection = () => {
    let ItemImages = [...state.ItemImages];
    ItemImages.forEach(itm => {
      itm.selected = false;
    });
    setState(prevState => ({...prevState, ImageSelection: false}));
  };

  const FinishCreation = passupdateddata => {
    RemoveBackButtonListener();
    navigation.goBack();
    if (state.isEditItem) {
      // console.log('ItemDetails : ', passupdateddata);
      route.params.onUpdateItem(passupdateddata);
    } else {
      // console.log('state.ItemDetailsList : ', state.ItemDetailsList);
      route.params.onFinishCreation(state.ItemDetailsList);
    }
  };

  const handleBackButtonClick = async () => {
    Confirmation('Do you want to go back ?', () => {
      RemoveBackButtonListener();
      navigation.goBack();
    });
    return false;
  };

  const HandleDepartmentChange = (GroupCode, GroupName) => {
    let getItemNameList = CombinedMasterInfo.ItemNameList.filter(
      objeitem => objeitem.GroupCode == GroupCode,
    );
    let resItemList = [];
    if (getItemNameList && getItemNameList.length > 0) {
      resItemList = getItemNameList[0].List || [];
    }
    setState(prevState => ({
      ...prevState,
      APIData: {...prevState.APIData, ItemNameList: resItemList},
    }));
    setItemDetails(prevState => ({
      ...prevState,
      GroupCode,
      ItemName:
        isnull(prevState.ItemName, '') != '' ? prevState.ItemName : GroupName,
    }));
  };

  const HandleCategoriesChange = (CategoryType, Code, Name, GroupDetails) => {
    let isGroup = GroupDetails && GroupDetails.length > 0 ? true : false;
    let CatField = isGroup ? Code : Name;
    let SubItemInfo = [];
    let GroupAlreadyExists = ItemDetails.SubItemInfo.length > 0;
    let CatGroupType = isGroup
      ? CategoryType
      : GroupAlreadyExists
      ? ItemDetails.CategoryType
      : 0;
    let CatGroupCode = isGroup
      ? Code
      : GroupAlreadyExists
      ? ItemDetails.CatGroupCode
      : '';
    // let isStartWithGroupCode = CAtField.startsWith("GN");
    // console.log({
    //   [`is Start With `]: isStartWithGroupCode,
    //   [`Field Value `]: CAtField,
    //   [`isGroup`]: isGroup,
    //   [`Cat${CategoryType}`]: Code,
    //   [`CatName${CategoryType}`]: Name,
    // });
    // console.log(`GroupDetails : ${JSON.stringify(GroupDetails)}`);
    if (isGroup) {
      SubItemInfo = GroupDetails.map(groupitem => {
        let iterate_CurrentGroupData = {};
        iterate_CurrentGroupData.Name = groupitem.Name;
        iterate_CurrentGroupData.Qty = 0;
        iterate_CurrentGroupData.Rate = 0;
        iterate_CurrentGroupData.StdRate = 0;
        iterate_CurrentGroupData.WSP = null;
        iterate_CurrentGroupData.RSP = null;
        iterate_CurrentGroupData.ListedMRP = null;
        iterate_CurrentGroupData.Select = true;
        return iterate_CurrentGroupData;
      });
    }
    setItemDetails(prevState => ({
      ...prevState,
      [`Cat${CategoryType}`]: CatField,
      CatGroupType,
      CatGroupCode,
    }));

    setState(prevState => ({
      ...prevState,
      ShowGroupModal: isGroup,
      CurrentGroupData: SubItemInfo || [],
      GroupPopUpInput: {...DefaultPriceFields},
    }));
  };

  const ApplySelectedCategoriesForGroup = () => {
    // to set state if the user has selected categories for group Product
    let CurrentGroupData = [...state.CurrentGroupData];
    let CurrentSelectedGroupData = CurrentGroupData.filter(
      item => item.Select === true,
    );

    if (CurrentSelectedGroupData.length > 0) {
      let get_GroupQty = 0,
        get_GroupRate = 0,
        get_GroupWSP = 0,
        get_GroupRSP = 0;
      let isInvalidQty = false,
        isInvalidRate = false;
      let SubItemInfo = CurrentSelectedGroupData.map(selecteditem => {
        let ReturnGroupData = {...DefaultValueOfCommonItemInfo};
        // ReturnGroupData.Id = GetNewUUID();
        ReturnGroupData[`Cat${ItemDetails.CatGroupType}`] = selecteditem.Name;
        ReturnGroupData.Qty = selecteditem.Qty;
        ReturnGroupData.Rate = selecteditem.Rate;
        ReturnGroupData.StdRate = selecteditem.StdRate;
        ReturnGroupData.WSP = selecteditem.WSP;
        ReturnGroupData.RSP = selecteditem.RSP;
        get_GroupQty += parseFloat(selecteditem.Qty);
        get_GroupRate =
          parseFloat(selecteditem.Rate) > get_GroupRate
            ? parseFloat(selecteditem.Rate)
            : get_GroupRate;
        get_GroupWSP =
          parseFloat(selecteditem.WSP) > get_GroupWSP
            ? parseFloat(selecteditem.WSP)
            : get_GroupWSP;
        get_GroupRSP =
          parseFloat(selecteditem.RSP) > get_GroupRSP
            ? parseFloat(selecteditem.RSP)
            : get_GroupRSP;
        if (parseFloat(isnull(selecteditem.Qty, 0)) <= 0) {
          isInvalidQty = true;
        }
        if (parseFloat(isnull(selecteditem.Rate, 0)) <= 0) {
          isInvalidRate = true;
        }
        return ReturnGroupData;
      });
      if (isInvalidQty) {
        AlertMessage('Enter valid qty.');
      }
      // else if(isInvalidRate){
      //   AlertMessage('Enter valid rate.');
      // }
      else {
        setItemDetails(prevState => ({
          ...prevState,
          SubItemInfo,
          Qty: get_GroupQty,
          Rate: get_GroupRate,
          DiscountAmt: 0,
          DiscountPer: 0,
          StdRate: get_GroupRate,
          WSP: get_GroupWSP,
          RSP: get_GroupRSP,
        }));
        setState(prevState => ({
          ...prevState,
          ShowGroupModal: false,
          CurrentGroupData: [],
          GroupPopUpInput: {...DefaultPriceFields},
        }));
      }
    } else {
      AlertMessage(
        `Select atleast one ${state.CategoryNames[ItemDetails.CatGroupType]}`,
      );
    }
  };

  const HideGroupModal = () => {
    setState(prevState => ({...prevState, ShowGroupModal: false}));
  };

  const UpdateAllSelectedGroupCatSelection = () => {
    // console.log('index : ', index, ' Field : ', Field, ' val : ', val);
    let CurrentGroupData = [...state.CurrentGroupData];
    CurrentGroupData.forEach(item => {
      if (item.Select) {
        item.Qty = parseFloat(
          state.GroupPopUpInput.Qty > 0 ? state.GroupPopUpInput.Qty : item.Qty,
        );
        item.Rate = parseFloat(
          state.GroupPopUpInput.Rate > 0
            ? state.GroupPopUpInput.Rate
            : item.Rate,
        );
        item.StdRate = parseFloat(
          state.GroupPopUpInput.Rate > 0
            ? state.GroupPopUpInput.StdRate
            : item.Rate,
        );
        item.WSP = parseFloat(
          state.GroupPopUpInput.WSP > 0 ? state.GroupPopUpInput.WSP : item.WSP,
        );
        item.RSP = parseFloat(
          state.GroupPopUpInput.RSP > 0 ? state.GroupPopUpInput.RSP : item.RSP,
        );
      }
    });
    // console.log('CurrentGroupData : ', CurrentGroupData);
    setState(prevState => ({...prevState, CurrentGroupData}));
  };

  const UpdateAllSelectedGroupCatSelectionForFields = (
    Field,
    Value,
    isPopup = true,
  ) => {
    // console.log('index : ', index, ' Field : ', Field, ' val : ', val);
    let CurrentGroupData = isPopup
      ? [...state.CurrentGroupData]
      : [...ItemDetails.SubItemInfo];
    CurrentGroupData.forEach(item => {
      if ((isPopup && item.Select) || !isPopup) {
        if (Field == 'Qty') {
          item.Qty = parseFloat(Value);
        } else if (Field == 'Rate') {
          item.Rate = parseFloat(Value);
          item.StdRate = parseFloat(Value);
        } else if (Field == 'WSP') {
          item.WSP = parseFloat(Value);
        } else if (Field == 'RSP') {
          item.RSP = parseFloat(Value);
        }
      }
    });
    if (isPopup) {
      setState(prevState => ({
        ...prevState,
        CurrentGroupData,
        GroupPopUpInput: {...prevState.GroupPopUpInput, [Field]: Value},
      }));
    } else {
      setState(prevState => ({
        ...prevState,
        GroupPopUpInput: {...prevState.GroupPopUpInput, [Field]: Value},
      }));
      setItemDetails(prevstate => ({
        ...prevstate,
        SubItemInfo: CurrentGroupData,
      }));
    }
  };

  const UpdateSubItemDetails = (index, Field, val) => {
    // console.log('index : ', index, ' Field : ', Field, ' val : ', val);
    let SubItemInfo = [...ItemDetails.SubItemInfo];
    SubItemInfo[index][Field] = parseFloat(val);
    let Qty = ItemDetails.Qty;
    if (Field == 'Qty') {
      Qty = SubItemInfo.reduce((prev, curr) => prev + curr.Qty, 0);
    } else if (Field == 'Rate') {
      SubItemInfo[index].StdRate = parseFloat(val);
    }

    setItemDetails(prevState => ({...prevState, SubItemInfo, Qty}));
  };

  const UpdateGroupCatSelection = (index, Field, val) => {
    // console.log('index : ', index, ' Field : ', Field, ' val : ', val);
    let CurrentGroupData = [...state.CurrentGroupData];
    CurrentGroupData[index][Field] = val;
    setState(prevState => ({...prevState, CurrentGroupData}));
  };

  const ValidateEnrty = isPopulateAllData => {
    let allowSave = false;
    // console.log('isPopulateAllData : ', isPopulateAllData);
    if (isPopulateAllData) {
      if (state.ItemDetailsList.length > 0) {
        allowSave = true;
      } else {
        AlertMessage('No product available to populate.');
      }
    } else {
      if (isnull(ItemDetails.GroupCode, '0') == '0')
        AlertMessage('Select Department');
      else if (isnull(ItemDetails.Qty, '0') == '0')
        AlertMessage('Quantity Cant Be Zero');
      else if (
        isnull(ItemDetails.CatGroupCode, '') != '' &&
        ItemDetails.SubItemInfo.length == 0
      )
        AlertMessage(`Select atleast one category to create group product`);
      else allowSave = true;
    }

    // return from method if allowsave is false
    if (!allowSave) return;

    let get_ItemDetails = {...ItemDetails};
    let get_imageData = [];
    get_imageData = (state.ItemImages || []).map(ele => {
      return {Path: ele.path};
    });
    get_ItemDetails.ImageData = get_imageData;
    // Calculating provisional data... for saving Item
    if (!state.isEditItem) {
      get_ItemDetails.Id = GetNewUUID();
    }
    if (!isPopulateAllData) {
      setState(prevState => ({
        ...prevState,
        ItemDetailsList: [...prevState.ItemDetailsList, {...get_ItemDetails}],
      }));
    }

    if (state.isEditItem) {
      AlertMessage('Product updated successfully');
      FinishCreation(get_ItemDetails);
    } else if (isPopulateAllData) {
      AlertMessage('Product populated successfully');
      FinishCreation();
    } else {
      AlertMessage('Product created successfully');
      ClearEntry();
    }

    // let ImagePath = state.ItemImages.map(img => img.path).join(' * ');
  };

  const HandleDateChange = (name, date) => {
    setItemDetails(prevState => ({...prevState, [name]: date}));
  };

  const OpenImageCapture = () => {
    let filedir = DocumentDirectoryFolder.ProductImage;
    const options = {
      title: 'Select Item Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: filedir,
      },
      maxWidth: 500,
      maxHeight: 500,
      quality: 0.6,
      base64: true,
    };
    ImagePicker.showImagePicker(options, res => {
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        AlertMessage(`Something went wrong\n ${res.error}`);
      } else {
        console.log('orgina path : ', res.path);
        let filepath =
          Platform.OS === 'android' ? 'file://' + res.path : '' + res.uri;
        let ItemImages = [
          ...state.ItemImages,
          {selected: false, path: filepath},
        ];
        setState(prevState => ({...prevState, ItemImages}));
      }
    });
  };

  const ClearEntry = () => {
    setItemDetails(InitialItemDetails);
    setState(prevState => ({
      ...prevState,
      ItemImages: [],
      GroupPopUpInput: {...DefaultPriceFields},
    }));
  };

  const refreshLayout = () => {
    ImageDimension = getLayoutImageDimension();
  };

  const FieldDepartment = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Department"
        data={CombinedMasterInfo.DepartmentList}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.GroupCode}
        onValueSelected={HandleDepartmentChange}
      />
    );
  };

  const FieldItemName = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Item Name"
        data={state.APIData.ItemNameList}
        labelProp="Name"
        valueProp="Name"
        selectedValue={ItemDetails.ItemName}
        defaultText={ItemDetails.ItemName}
        onValueSelected={ItemName =>
          setItemDetails(prevState => ({...prevState, ItemName}))
        }
        allowAddNew={true}
        newItemTextAsValue={true}
      />
    );
  };

  const FieldOemCode = key => {
    return (
      <RichTextInput
        key={key}
        placeholder="Oem Code"
        value={ItemDetails.OemCode}
        onChangeText={OemCode =>
          setItemDetails(prevState => ({...prevState, OemCode}))
        }
        inputProps={{returnKeyType: 'next'}}
      />
    );
  };

  const FieldMfgDate = key => {
    return (
      <ModalDatePicker
        key={key}
        placeholder={'Mfg Date'}
        selectedValue={ItemDetails.MfgDate}
        onValueSelected={selectedDate =>
          HandleDateChange('MfgDate', selectedDate)
        }
      />
    );
  };

  const FieldCat1 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['1'] || 'Category 1'}
        data={state.APIData.Cat1}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat1}
        defaultText={ItemDetails.Cat1}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(1, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat2 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['2'] || 'Category 2'}
        data={state.APIData.Cat2}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat2}
        defaultText={ItemDetails.Cat2}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(2, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat3 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['3'] || 'Category 3'}
        data={state.APIData.Cat3}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat3}
        defaultText={ItemDetails.Cat3}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(3, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat4 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['4'] || 'Category 4'}
        data={state.APIData.Cat4}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat4}
        defaultText={ItemDetails.Cat4}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(4, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat5 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['5'] || 'Category 5'}
        data={state.APIData.Cat5}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat5}
        defaultText={ItemDetails.Cat5}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(5, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat6 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['6'] || 'Category 6'}
        data={state.APIData.Cat6}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat6}
        defaultText={ItemDetails.Cat6}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(6, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat7 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['7'] || 'Category 7'}
        data={state.APIData.Cat7}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat7}
        defaultText={ItemDetails.Cat7}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(7, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat8 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['8'] || 'Category 8'}
        data={state.APIData.Cat8}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat8}
        defaultText={ItemDetails.Cat8}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(8, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat9 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['9'] || 'Category 9'}
        data={state.APIData.Cat9}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat9}
        defaultText={ItemDetails.Cat9}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(9, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat10 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['10'] || 'Category 10'}
        data={state.APIData.Cat10}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat10}
        defaultText={ItemDetails.Cat10}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(10, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat11 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['11'] || 'Category 11'}
        data={state.APIData.Cat11}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat11}
        defaultText={ItemDetails.Cat11}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(11, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat12 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['12'] || 'Category 12'}
        data={state.APIData.Cat12}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat12}
        defaultText={ItemDetails.Cat12}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(12, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat13 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['13'] || 'Category 13'}
        data={state.APIData.Cat13}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat13}
        defaultText={ItemDetails.Cat13}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(13, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat14 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['14'] || 'Category 14'}
        data={state.APIData.Cat14}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat14}
        defaultText={ItemDetails.Cat14}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(14, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat15 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['15'] || 'Category 15'}
        data={state.APIData.Cat15}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat15}
        defaultText={ItemDetails.Cat15}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(15, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldCat16 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={state.CategoryNames['16'] || 'Category 16'}
        data={state.APIData.Cat16}
        labelProp="Name"
        valueProp="Code"
        selectedValue={ItemDetails.Cat16}
        defaultText={ItemDetails.Cat16}
        onValueSelected={(value, label, index, item) =>
          HandleCategoriesChange(16, value, label, item && item.List)
        }
        allowAddNew={true}
      />
    );
  };

  const FieldQuantity = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="Qty"
        value={ItemDetails.Qty}
        onChangeText={Qty =>
          setItemDetails(prevState => ({...prevState, Qty: parseFloat(Qty)}))
        }
        inputProps={{
          returnKeyType: 'next',
          keyboardType: 'numeric',
          editable: !ItemDetails.SubItemInfo.length > 0,
        }}
      />
    );
  };

  const FieldRate = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="Rate"
        value={ItemDetails.Rate}
        onChangeText={Rate => {
          Rate = parseFloat(isnull(Rate, 0));
          let DiscountPer = parseFloat(isnull(ItemDetails.DiscountPer, 0));
          let DiscountAmt = (Rate * DiscountPer) / 100;
          let StdRate = Rate - DiscountAmt;
          setItemDetails(prevState => ({
            ...prevState,
            Rate,
            DiscountPer,
            DiscountAmt,
            StdRate,
          }));
        }}
        inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
      />
    );
  };

  const FieldStdRate = key => {
    return (
      <RichTextInput
        key={`Dis_stdrate_${key}`}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="Std Rate"
        value={ItemDetails.StdRate}
        inputProps={{
          editable: false,
          returnKeyType: 'next',
          keyboardType: 'numeric',
        }}
      />
    );
  };

  const FieldDiscount = key => {
    return (
      <View
        key={`Dis_${key}`}
        style={[
          {width: 130, flexDirection: 'row', justifyContent: 'space-between'},
        ]}>
        <RichTextInput
          customControl={true}
          wrapperStyle={{width: '48%'}}
          placeholder="Disc %"
          value={ItemDetails.DiscountPer}
          onChangeText={DiscountPer => {
            DiscountPer = parseFloat(isnull(DiscountPer, 0));
            let Rate = parseFloat(isnull(ItemDetails.Rate, 0));
            let DiscountAmt = Rate * (DiscountPer / 100);
            let StdRate = Rate - DiscountAmt;
            setItemDetails(prevState => ({
              ...prevState,
              DiscountPer,
              DiscountAmt,
              StdRate,
            }));
          }}
          inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
        />
        <RichTextInput
          customControl={true}
          wrapperStyle={{width: '48%'}}
          placeholder={`Disc $`}
          value={ItemDetails.DiscountAmt}
          onChangeText={DiscountAmt => {
            DiscountAmt = parseFloat(isnull(DiscountAmt, 0));
            let Rate = parseFloat(isnull(ItemDetails.Rate, 0));
            let DiscountPer = Rate == 0 ? 0 : 100 * (DiscountAmt / Rate);
            let StdRate = Rate - DiscountAmt;
            setItemDetails(prevState => ({
              ...prevState,
              DiscountPer,
              DiscountAmt,
              StdRate,
            }));
          }}
          inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
        />
      </View>
    );
  };

  const FieldWSP = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="WSP"
        value={ItemDetails.WSP}
        onChangeText={WSP =>
          setItemDetails(prevState => ({...prevState, WSP: parseFloat(WSP)}))
        }
        inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
      />
    );
  };

  const FieldRSP = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="RSP"
        value={ItemDetails.RSP}
        onChangeText={RSP =>
          setItemDetails(prevState => ({...prevState, RSP: parseFloat(RSP)}))
        }
        inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
      />
    );
  };

  const FieldListedMRP = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="Listed MRP"
        value={ItemDetails.ListedMRP}
        onChangeText={ListedMRP =>
          setItemDetails(prevState => ({
            ...prevState,
            ListedMRP: parseFloat(ListedMRP),
          }))
        }
        inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
      />
    );
  };

  const FieldOnlineMRP = key => {
    return (
      <RichTextInput
        key={key}
        customControl={true}
        wrapperStyle={{width: 130}}
        placeholder="Online MRP"
        value={ItemDetails.OnlineMRP}
        onChangeText={OnlineMRP =>
          setItemDetails(prevState => ({
            ...prevState,
            OnlineMRP: parseFloat(OnlineMRP),
          }))
        }
        inputProps={{returnKeyType: 'next', keyboardType: 'numeric'}}
      />
    );
  };

  const FieldList = {
    getFieldOnlineMRP: FieldOnlineMRP,
    getFieldListedMRP: FieldListedMRP,
    getFieldRSP: FieldRSP,
    getFieldWSP: FieldWSP,
    getFieldDiscount: FieldDiscount,
    getFieldStdRate: FieldStdRate,
    getFieldRate: FieldRate,
    getFieldQuantity: FieldQuantity,
    getFieldCat16: FieldCat16,
    getFieldCat15: FieldCat15,
    getFieldCat14: FieldCat14,
    getFieldCat13: FieldCat13,
    getFieldCat12: FieldCat12,
    getFieldCat11: FieldCat11,
    getFieldCat10: FieldCat10,
    getFieldCat9: FieldCat9,
    getFieldCat8: FieldCat8,
    getFieldCat7: FieldCat7,
    getFieldCat6: FieldCat6,
    getFieldCat5: FieldCat5,
    getFieldCat4: FieldCat4,
    getFieldCat3: FieldCat3,
    getFieldCat2: FieldCat2,
    getFieldCat1: FieldCat1,
    getFieldDepartment: FieldDepartment,
    getFieldItemName: FieldItemName,
    getFieldOemCode: FieldOemCode,
    getFieldMfgDate: FieldMfgDate,
  };

  // console.log('state.CurrentGroupData : ', state.CurrentGroupData);
  return !state.isFetched ? (
    <ICubeAIndicator />
  ) : (
    <>
      <StatusBar
        backgroundColor={globalColorObject.Color.OffLineStatus}
        barStyle="default"
      />
      <LayoutWrapper onLayoutChanged={refreshLayout}>
        <ScrollView
          style={styles.ScrollView}
          contentContainerStyle={{paddingBottom: 20}}>
          {/* Product Group */}
          <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
            <View style={[AccordionStyle.header]}>
              <Text
                style={[
                  AccordionStyle.headerTitle,
                  layoutStyle.AppFont,
                  {paddingTop: 5},
                ]}>
                Product
              </Text>
            </View>
            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
              {visibleProductGroupFields.map((fd, ind) =>
                FieldList[`getField${fd}`](ind),
              )}
              {(state.visibleCategoryGroupFields || []).map((fd, ind) =>
                FieldList[`getField${fd}`](ind),
              )}
            </View>
          </View>
          {/* Price Group */}
          <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
            <View style={[AccordionStyle.header]}>
              <Text
                style={[
                  AccordionStyle.headerTitle,
                  layoutStyle.AppFont,
                  {paddingTop: 5},
                ]}>
                Price
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-evenly',
              }}>
              {visiblePriceGroupFields.map((fd, ind) =>
                FieldList[`getField${fd}`](ind),
              )}
            </View>
          </View>
          {/* Category Group */}
          {ItemDetails.SubItemInfo.length > 0 && (
            <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
              <View
                key={`SumCatGrp_Caption`}
                style={[
                  {
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'space-evenly',
                  },
                  // ApplyStyleColor(globalColorObject.Color.Lightprimary,globalColorObject.ColorPropetyType.BackgroundColor),
                ]}>
                <View style={{alignSelf: 'flex-end'}}>
                  <Text style={[styles.GroupCatRowText]}>
                    {state.CategoryNames[ItemDetails.CatGroupType] || `Groups`}
                  </Text>
                </View>
                <RichTextInput
                  isShowAnimateText={false}
                  wrapperStyle={{width: 60}}
                  placeholder="Qty"
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                    editable: false,
                  }}
                />
                <RichTextInput
                  isShowAnimateText={false}
                  wrapperStyle={{width: 70}}
                  placeholder="Rate"
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                    editable: false,
                  }}
                />
                <RichTextInput
                  isShowAnimateText={false}
                  wrapperStyle={{width: 70}}
                  placeholder="WSP"
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                    editable: false,
                  }}
                />
                <RichTextInput
                  isShowAnimateText={false}
                  wrapperStyle={{width: 70}}
                  placeholder="RSP"
                  inputProps={{
                    returnKeyType: 'next',
                    editable: false,
                    keyboardType: 'numeric',
                  }}
                />
              </View>

              {ItemDetails.SubItemInfo.map((item, sgindex) => (
                <View
                  key={`SumCatGrp_${sgindex}`}
                  style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'space-evenly',
                  }}>
                  <View style={{alignSelf: 'flex-end'}}>
                    <Text style={[styles.GroupCatRowText]}>
                      {item[`Cat${ItemDetails.CatGroupType}`] || 'N/A'}
                    </Text>
                  </View>
                  <RichTextInput
                    isShowAnimateText={false}
                    wrapperStyle={{width: 60}}
                    placeholder="Qty"
                    value={item.Qty}
                    onChangeText={Qty =>
                      UpdateSubItemDetails(sgindex, 'Qty', Qty)
                    }
                    inputProps={{
                      returnKeyType: 'next',
                      keyboardType: 'numeric',
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    wrapperStyle={{width: 70}}
                    placeholder="Rate"
                    value={item.Rate}
                    onChangeText={Rate =>
                      UpdateSubItemDetails(sgindex, 'Rate', Rate)
                    }
                    inputProps={{
                      returnKeyType: 'next',
                      keyboardType: 'numeric',
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    wrapperStyle={{width: 70}}
                    placeholder="WSP"
                    value={item.WSP}
                    onChangeText={WSP =>
                      UpdateSubItemDetails(sgindex, 'WSP', WSP)
                    }
                    inputProps={{
                      returnKeyType: 'next',
                      keyboardType: 'numeric',
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    wrapperStyle={{width: 70}}
                    placeholder="RSP"
                    value={item.RSP}
                    onChangeText={RSP =>
                      UpdateSubItemDetails(sgindex, 'RSP', RSP)
                    }
                    inputProps={{
                      returnKeyType: 'next',
                      keyboardType: 'numeric',
                    }}
                  />
                </View>
              ))}
            </View>
          )}
          {/* Product Images */}
          {state.ItemImages.length > 0 && (
            <View
              style={[
                AccordionStyle.wrapper,
                {paddingVertical: 10, marginTop: 5},
              ]}>
              <View style={[AccordionStyle.header]}>
                <Text
                  style={[
                    AccordionStyle.headerTitle,
                    layoutStyle.AppFont,
                    {paddingTop: 5},
                  ]}>
                  Images
                </Text>
              </View>
              <ScrollView horizontal={true} style={styles.ItemImagesWrapper}>
                {PrepareItemImagesList()}
              </ScrollView>
            </View>
          )}
        </ScrollView>
        {/* Captured Images list */}
        {/* Capture floating Button on bottom */}
        <View>
          {!state.ImageSelection && (
            <TouchableOpacity
              style={styles.CaptureButton}
              onPress={OpenImageCapture}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/camera-light.png')}
              />
            </TouchableOpacity>
          )}
          {state.ImageSelection && (
            <>
              <TouchableOpacity
                style={styles.RemoveImageButton}
                onPress={RemoveSelectedImages}>
                <Image
                  style={{width: 25, height: 25}}
                  source={require('../../../assets/images/remove-light.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.CancelSelectionButton}
                onPress={CancelImageSelection}>
                <Image
                  style={{width: 30, height: 30}}
                  source={require('../../../assets/images/add-light.png')}
                />
              </TouchableOpacity>
            </>
          )}
        </View>

        {/* Modal Popup for Group details starts here */}
        <Modal
          transparent={true}
          animationType="fade"
          visible={state.ShowGroupModal}>
          <View style={styles.ModalBackDrop}>
            <View style={[styles.GroupDetailsWrapper]}>
              <View
                style={[
                  {
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    justifyContent: 'space-evenly',
                    height: 60,
                  },
                  ApplyStyleColor(
                    globalColorObject.Color.Lightprimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                <Text style={styles.GroupCatRowText}>{''}</Text>
                <RichTextInput
                  wrapperStyle={{width: 60}}
                  placeholder="Qty"
                  value={state.GroupPopUpInput.Qty}
                  onChangeText={Qty =>
                    UpdateAllSelectedGroupCatSelectionForFields('Qty', Qty)
                  }
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                  }}
                />
                <RichTextInput
                  wrapperStyle={{width: 70}}
                  placeholder="Rate"
                  value={state.GroupPopUpInput.Rate}
                  onChangeText={Rate =>
                    UpdateAllSelectedGroupCatSelectionForFields('Rate', Rate)
                  }
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                  }}
                />
                <RichTextInput
                  wrapperStyle={{width: 70}}
                  placeholder="WSP"
                  value={state.GroupPopUpInput.WSP}
                  onChangeText={WSP =>
                    UpdateAllSelectedGroupCatSelectionForFields('WSP', WSP)
                  }
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                  }}
                />
                <RichTextInput
                  wrapperStyle={{width: 70}}
                  placeholder="RSP"
                  value={state.GroupPopUpInput.RSP}
                  onChangeText={RSP =>
                    UpdateAllSelectedGroupCatSelectionForFields('RSP', RSP)
                  }
                  inputProps={{
                    returnKeyType: 'next',
                    keyboardType: 'numeric',
                  }}
                />
              </View>

              <FlatList
                style={[
                  ApplyStyleColor(
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}
                data={state.CurrentGroupData}
                renderItem={({item, index}) => (
                  <View
                    key={`grp_${index}`}
                    style={[
                      {
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        justifyContent: 'space-evenly',
                      },
                      ApplyStyleColor(
                        globalColorObject.Color.oppPrimary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),
                    ]}>
                    <View style={{alignSelf: 'flex-end'}}>
                      <Text
                        style={[styles.GroupCatRowText, {textAlign: 'center'}]}>
                        {item.Name}
                      </Text>
                    </View>
                    <RichTextInput
                      isShowAnimateText={false}
                      wrapperStyle={{width: 60}}
                      placeholder=""
                      value={item.Qty}
                      onChangeText={Qty =>
                        UpdateGroupCatSelection(index, 'Qty', Qty)
                      }
                      inputProps={{
                        returnKeyType: 'next',
                        keyboardType: 'numeric',
                      }}
                    />
                    <RichTextInput
                      isShowAnimateText={false}
                      wrapperStyle={{width: 70}}
                      placeholder=""
                      value={item.Rate}
                      onChangeText={Rate =>
                        UpdateGroupCatSelection(index, 'Rate', Rate)
                      }
                      inputProps={{
                        returnKeyType: 'next',
                        keyboardType: 'numeric',
                      }}
                    />
                    <RichTextInput
                      isShowAnimateText={false}
                      wrapperStyle={{width: 70}}
                      placeholder=""
                      value={item.WSP}
                      onChangeText={WSP =>
                        UpdateGroupCatSelection(index, 'WSP', WSP)
                      }
                      inputProps={{
                        returnKeyType: 'next',
                        keyboardType: 'numeric',
                      }}
                    />
                    <RichTextInput
                      isShowAnimateText={false}
                      wrapperStyle={{width: 70}}
                      placeholder=""
                      value={item.RSP}
                      onChangeText={RSP =>
                        UpdateGroupCatSelection(index, 'RSP', RSP)
                      }
                      inputProps={{
                        returnKeyType: 'next',
                        keyboardType: 'numeric',
                      }}
                    />
                    <CheckInput
                      customControl={true}
                      wrapperStyle={{width: 25}}
                      placeholder=""
                      value={item.Select}
                      onValueChange={Select =>
                        UpdateGroupCatSelection(index, 'Select', Select)
                      }
                    />
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshing={false}
                stickyHeaderIndices={[0]}
                ListFooterComponent={() => (
                  <View style={{width: '100%', marginTop: 20}} />
                )}
              />
              <View
                style={[
                  styles.GroupActionButtonWrapper,
                  ApplyStyleColor(
                    globalColorObject.Color.Lightprimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                <View style={{marginHorizontal: 10, width: 60}}>
                  <Button
                    // backgroundColor={globalColorObject.Color.Primary}
                    color={globalColorObject.Color.Primary}
                    title="Ok"
                    onPress={ApplySelectedCategoriesForGroup}
                  />
                </View>
                <View style={{marginHorizontal: 10, width: 80}}>
                  <Button
                    color={globalColorObject.Color.Primary}
                    title="Cancel"
                    onPress={HideGroupModal}
                  />
                </View>
              </View>
            </View>
          </View>
        </Modal>
        {/* Modal Popup for Group details ends here */}
        {/* Modal for Previewing ItemImage start here */}
        <Modal visible={state.ShowImagePreview} animationType={'slide'}>
          <View style={styles.ImagePreviewModal}>
            <View style={styles.ImagePreviewWrapper}>
              <Image
                style={styles.ImagePreviewer}
                source={{uri: state.PreviewImagePath}}
              />
            </View>
            <View style={styles.ImagePreviewCloseWrapper}>
              <TouchableHighlight
                style={{marginHorizontal: 20}}
                onPress={CloseImagePreview}>
                <Text style={styles.ImagePreviewCloseButton}>&copy;</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={{marginHorizontal: 20}}
                onPress={RemoveSelectedImagesPath}>
                <Text style={styles.ImagePreviewCloseButton}>&reg;</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
        {/* Modal for Previewing ItemImage ends here */}
      </LayoutWrapper>
      <ICubeAIndicator isShow={!state.isFetched} />
    </>
  );
};

const styles = StyleSheet.create({
  Loadercontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Tabs: {
    flexDirection: 'row',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  ImagesTab: {
    flex: 1,
  },
  CaptureButton: {
    width: 60,
    height: 60,
    backgroundColor: globalColorObject.Color.Primary,
    position: 'absolute',
    bottom: 30,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
  },
  ModalBackDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  GroupDetailsWrapper: {
    width: '95%',
    borderRadius: 10,
    // overflow: 'hidden',
  },
  GroupModalTitle: {
    fontSize: 20,
    textAlign: 'center',
    color: globalColorObject.Color.Primary,
    marginVertical: 10,
  },
  GroupScrollViewWrapper: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.04)',
    marginHorizontal: 10,
    borderRadius: 5,
    // flexWrap:'wrap',
  },
  GroupActionButtonWrapper: {
    paddingVertical: 12,
    flexDirection: 'row-reverse',
  },
  GroupCatRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: 10,
  },
  GroupCatRowText: {
    // fontSize: 18,
    minWidth: 70,
    // textAlign:'center',
    paddingBottom: 8,
  },
  CustomControl: {
    width: '100%',
  },
  ItemImagesWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    flexWrap: 'wrap',
    alignSelf: 'center',
    marginLeft: 10,
  },
  get ItemImageWrapper() {
    return {
      // position: 'relative',
      justifyContent: 'center',
      width: ImageDimension || 100,
      height: ImageDimension || 100,
      backgroundColor: 'rgba(0,0,0,0.06)',
    };
  },
  get ItemImage() {
    return {
      margin: 3,
      width: (ImageDimension || 100) - 6,
      height: (ImageDimension || 100) - 6,
      zIndex: 1,
    };
  },
  ImagePreviewModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.9)',
  },
  ImagePreviewWrapper: {
    flex: 1,
  },
  ImagePreviewer: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  ImagePreviewCloseWrapper: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  ImagePreviewCloseButton: {
    fontSize: 35,
    textAlign: 'center',
    color: 'rgba(255,255,255,.5)',
  },
  SelectedImage: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 20,
    height: 20,
    borderRadius: 20,
    backgroundColor: globalColorObject.Color.Primary,
    zIndex: 10,
  },
  ImageSelector: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 9,
  },
  RemoveImageButton: {
    width: 60,
    height: 60,
    backgroundColor: globalColorObject.Color.Primary,
    position: 'absolute',
    bottom: 110,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
  },
  CancelSelectionButton: {
    width: 60,
    height: 60,
    backgroundColor: globalColorObject.Color.Primary,
    position: 'absolute',
    bottom: 30,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
    transform: [{rotateZ: '45deg'}],
  },
});

export {ItemMaster};
