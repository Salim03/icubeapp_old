/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {useEffect, useState, useLayoutEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  Keyboard,
  BackHandler,
  StatusBar,
} from 'react-native';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import Menu, {MenuItem} from 'react-native-material-menu';
// Local Imports
import RichTextInput from '../../Custom/RichTextInput';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import {
  Confirmation,
  AlertMessage,
  isnull,
  RoundValue,
  AlertStatusError,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import {
  GetNewUUID,
  GetNextPurchaseOrderNumber,
  SavePurchaseOrder,
} from '../../../Data/DB/Purchase';

const ListItemHeader = React.memo(({headers}) => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        <View style={[styles.CardTextContainer, {width: 260}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Item Name
          </Text>
        </View>
        {headers.map(head => (
          <View
            key={head}
            style={[
              styles.CardTextContainer,
              CardItemLayout,
              {alignItems: 'center'},
            ]}>
            <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {head || ''}
            </Text>
          </View>
        ))}
      </View>
    </View>
  );
});

const ListItem = props => {
  let data = props.data;
  let index = props.index;
  const {CardItemLayout} = CardStyle;
  // console.log('data : ', data);
  let Cat1 = isnull(data.Cat1, '');
  let Cat2 = isnull(data.Cat2, '');
  let Cat3 = isnull(data.Cat3, '');
  let Cat4 = isnull(data.Cat4, '');
  let Cat5 = isnull(data.Cat5, '');
  let Cat6 = isnull(data.Cat6, '');
  Cat1 =
    Cat1 != isnull(data.CatGroupCode, '') ? (Cat1 != '' ? ` ${Cat1}` : '') : '';
  Cat2 =
    Cat2 != isnull(data.CatGroupCode, '') ? (Cat2 != '' ? ` ${Cat2}` : '') : '';
  Cat3 =
    Cat3 != isnull(data.CatGroupCode, '') ? (Cat3 != '' ? ` ${Cat3}` : '') : '';
  Cat4 =
    Cat4 != isnull(data.CatGroupCode, '') ? (Cat4 != '' ? ` ${Cat4}` : '') : '';
  Cat5 =
    Cat5 != isnull(data.CatGroupCode, '') ? (Cat5 != '' ? ` ${Cat5}` : '') : '';
  Cat6 =
    Cat6 != isnull(data.CatGroupCode, '') ? (Cat6 != '' ? ` ${Cat6}` : '') : '';
  return (
    <TouchableWithoutFeedback
      onLongPress={() => props.HandleEditProduct(index, data)}>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: 260}]}>
            <Text
              numberOfLines={2}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {`${data.ItemName} ${Cat1} ${Cat2} ${Cat3} ${Cat4} ${Cat5} ${Cat6}`}
            </Text>
          </View>
          {props.headers.map(field => (
            <View
              key={field}
              style={[
                styles.CardTextContainer,
                CardItemLayout,
                {alignItems: 'center'},
              ]}>
              <Text
                numberOfLines={1}
                style={ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                )}>
                {field == 'Amount' ? data.StdRate * data.Qty : data[field]}
              </Text>
            </View>
          ))}
        </View>
        <View style={styles.CardBtnOptionWrapper}>
          <TouchableOpacity
            style={styles.CardBtnOption}
            onPress={() => props.HandleShowMenu(index)}>
            <Text
              style={[
                styles.CardBtnOptionText,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.mediam.mm,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              &#8964;
            </Text>
          </TouchableOpacity>
          <Menu
            ref={props.setMenuRef}
            button={<Text style={{width: 0, height: 0}} />}>
            <MenuItem onPress={() => props.HandleEditProduct(index, data)}>
              Edit
            </MenuItem>
            <MenuItem onPress={() => props.HandleRemoveProduct(index)}>
              Remove
            </MenuItem>
          </Menu>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const AddPurchase = ({navigation, route}) => {
  // Declaring class variables
  let visibleCreatedItemsGroupFields = ['Qty', 'Rate', 'RSP', 'Amount'];
  let visibleCreatedItemsGroupCaptions = ['Quantity', 'Rate', 'MRP', 'Amount'];

  const menurefList = {};
  const setMenuRef = (ref, index) => {
    menurefList[`_menu${index}`] = ref;
  };

  const hideMenu = index => menurefList[`_menu${index}`].hide();

  const showMenu = index => menurefList[`_menu${index}`].show();

  const defaultLocalData = {
    EditInvoice: false,
    Id: '',
    InvoiceNo: '',
    InvoiceDate: new Date(),
    Status: 'Pending',
  };

  const [state, setState] = useState({
    ModalMessage: 'Saving Transaction',
    isSaved: false,
    isFetched: false,
    LocalData: {
      ...defaultLocalData,
      UserCode: '',
      Loccode: 1,
    },
    // Holds the data to be set to controls ( filled later by the data from API )
    VendorList: [],
    // Hold selected or entered values of controls
    Vendor: '',
    DocNo: '',
    DocDate: null,
    Remarks: '',
    BasicAmount: 0,
    TotalQty: 0,

    // holds CreatedItems Array
    CreatedItems: [],
    ProductSearchText: '',
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () =>
        state.LocalData.Status == 'Pending' && (
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 10}}
              onPress={ValidateInputs}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/save-light.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 10}}
              onPress={HandleAddProduct}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/add-light.png')}
              />
            </TouchableOpacity>
          </View>
        ),
    });
  }, [navigation, state]);

  useEffect(() => {
    // Adding a event listener for hardware back press
    AddBackButtonListener();

    LoadSelectedInvoice();
    return () => {
      RemoveBackButtonListener();
    };
  }, []);

  const LoadSelectedInvoice = async () => {
    // fetching selected Invoice data
    const {params} = route;
    let LocalData = {...state.LocalData};
    let EditData = params.EditData || {};
    let Loccode = parseInt(params.Loccode || 1);
    if (EditData && EditData.Id && EditData.Id != '') {
      // Setting Local Data
      LocalData.EditInvoice = true;
      LocalData.Id = EditData.Id;
      LocalData.InvoiceDate = new Date(EditData.Date);
      LocalData.InvoiceNo = EditData.No;
      LocalData.Status = EditData.Status;
      LocalData.Loccode = Loccode;
      // Setting state
      UpdateAmountCalculation(EditData.Details);
      setState(prevstate => ({
        ...prevstate,
        isFetched: true,
        VendorList: params.SupplierList || [],
        Vendor: EditData.Code || '',
        DocNo: EditData.DocNo,
        DocDate: EditData.DocDate || null,
        Remarks: EditData.Remarks,
        BasicAmount: EditData.Amount,
        TotalQty: EditData.Qty,
        CreatedItems: EditData.Details,
        LocalData: {...prevstate.LocalData, ...LocalData},
      }));
    } else {
      setState(prevstate => ({
        ...prevstate,
        isFetched: true,
        VendorList: params.SupplierList || [],
        LocalData: {...prevstate.LocalData, Loccode},
      }));
    }
  };

  const AddBackButtonListener = () => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
  };

  const RemoveBackButtonListener = () => {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
  };

  const handleBackButtonClick = async () => {
    await Confirmation('Do you want to go back ?', () => {
      RemoveBackButtonListener();
      navigation.goBack();
    });
    return false;
  };

  const HandleVendorChange = Vendor => {
    setState(prevstate => ({...prevstate, Vendor: Vendor}));
  };

  const HandleDateChange = (name, date) => {
    setState(prevstate => ({...prevstate, [name]: date}));
  };

  const UpdateAmountCalculation = passcreateitemdata => {
    let BasicAmount = 0,
      TotalQty = 0;
    passcreateitemdata.forEach(element => {
      BasicAmount += parseFloat(
        RoundValue(
          parseFloat(isnull(element.Qty, 0)) *
            parseFloat(isnull(element.StdRate, 0)),
        ),
      );
      TotalQty += parseFloat(RoundValue(parseFloat(isnull(element.Qty, 0))));
    });
    setState(prevstate => ({
      ...prevstate,
      BasicAmount: parseFloat(BasicAmount),
      TotalQty: parseFloat(TotalQty),
    }));
  };

  const ValidateInputs = async () => {
    try {
      Keyboard.dismiss();
      // Validate All Mandatory fields
      if (!state.Vendor) {
        AlertMessage('Select Vendor');
      } else if (!state.CreatedItems.length) {
        AlertMessage('Add atleast one product');
      } else {
        // Preparing Invoice Data
        let get_InvoiceNo = '';
        if (state.LocalData.EditInvoice) {
          get_InvoiceNo = state.LocalData.InvoiceNo;
        } else {
          await GetNextPurchaseOrderNumber().then(nextnumber => {
            get_InvoiceNo = nextnumber;
          });
        }
        let ObjSave = {
          // Invoice Master data
          Id: state.LocalData.EditInvoice ? state.LocalData.Id : GetNewUUID(),
          No: get_InvoiceNo,
          Date: state.LocalData.EditInvoice
            ? state.LocalData.InvoiceDate
            : new Date(),
          Location: state.LocalData.Loccode,
          Code: state.Vendor,
          Name: state.Vendor,
          DocNo: state.DocNo,
          DocDate: state.DocDate,
          Remarks: state.Remarks,
          Amount: state.BasicAmount || 0, // sum of amount of state.CreatedItems
          Qty: state.TotalQty || 0, // sum of qty of state.CreatedItems
          Status: state.LocalData.Status,
          CreatedByOrModifyBy: state.LocalData.UserCode,
          // Array Data with properties similar with DB Type
          Details: state.CreatedItems, // passing Invoice detail Array
        };
        // console.log('save object : ', JSON.stringify(ObjSave));
        SavePurchaseOrder([ObjSave])
          .then(retsaveobject => {
            if (retsaveobject.status == 'Saved') {
              state.LocalData.EditInvoice
                ? AlertMessage(
                    `${retsaveobject.data.No} updated successfully`,
                    NavigateBackToList,
                  )
                : AlertMessage(`${retsaveobject.data.No} saved successfully`);
              clearEntry();
            } else {
              AlertStatusError('Failed to save record.');
            }
          })
          .catch(err => {
            AlertMessage(`Failed to save record. \n${err.data}`);
          });
      }
    } catch (err) {
      console.error(err);
    }
  };

  const NavigateBackToList = () => {
    navigation.goBack();
    route.params.onUpdateInvoice();
  };

  const clearEntry = () => {
    setState(prevstate => ({
      ...prevstate,
      isFetched: true, // show or hide Save Modal popup
      isSaved: true,
      // Clearing Filed data
      Vendor: '',
      DocNo: '',
      DocDate: '',
      Remarks: '',
      // Clearing Created Items
      CreatedItems: [],
      // clearing LocalData
      LocalData: {
        ...prevstate.LocalData,
        ...defaultLocalData,
      },
    }));
    UpdateAmountCalculation([]);
  };

  const HandleAddProduct = async () => {
    // Check whether Vendor has been selected
    if (isnull(state.Vendor, '') == '') {
      AlertMessage('Select Vendor');
    } else {
      NavigateToItemMaster();
    }
  };

  const HandleEditItem = (index, item) => {
    hideMenu(index);
    NavigateToItemMaster(item);
  };

  const HandleRemoveItem = index => {
    hideMenu(index);
    Confirmation('Do you want to remove ?', () => {
      let CreatedItems = [...state.CreatedItems];
      CreatedItems.splice(index, 1);
      setState(prevstate => ({...prevstate, CreatedItems: CreatedItems}));
      UpdateAmountCalculation(CreatedItems);
    });
  };

  const HandleProductQtyChange = (index, value) => {
    value = value || 0;
    let CreatedItems = [...state.CreatedItems];
    CreatedItems[index].Quantity = parseFloat(value);
    setState(prevstate => ({...prevstate, CreatedItems: CreatedItems}));
    UpdateAmountCalculation(CreatedItems);
  };

  const NavigateToItemMaster = item => {
    if (state.Vendor !== '') {
      RemoveBackButtonListener();
      navigation.navigate('OffLine_AddItemPurchse', {
        EditItem: item,
        onFinishCreation: UpdateCreatedItem,
        onUpdateItem: RefreshUpdatedItem,
      });
    } else {
      AlertMessage('Select Vendor');
    }
  };

  const RefreshUpdatedItem = EditItem => {
    if (EditItem && Object.entries(EditItem).length > 0) {
      let get_CreatedItems = [...state.CreatedItems];
      for (let getIndex in get_CreatedItems) {
        if (get_CreatedItems[getIndex].Id == EditItem.Id) {
          get_CreatedItems[getIndex] = EditItem;
          break;
        }
      }
      UpdateAmountCalculation(get_CreatedItems);
      setState(prevstate => ({...prevstate, CreatedItems: get_CreatedItems}));
    } else {
      AlertStatusError('Invalid edit item information.');
    }
    AddBackButtonListener();
  };

  const UpdateCreatedItem = data => {
    // return if no data returned
    if (data.length && data.length > 0) {
      data.forEach(
        (item, index, fullarray) =>
          (fullarray[index].Amount = item.Qty * item.StdRate),
      );
      let CreatedItems = [...state.CreatedItems, ...data];
      console.log('populated items : ', CreatedItems);
      setState(prevstate => ({...prevstate, CreatedItems}));
      UpdateAmountCalculation(CreatedItems);
    }
    AddBackButtonListener();
  };

  const FieldVendor = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Vendor"
      data={state.VendorList}
      labelProp="Name"
      valueProp="Code"
      selectedValue={state.Vendor}
      onValueSelected={HandleVendorChange}
    />
  );

  const FieldDocInfo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <RichTextInput
        placeholder="Doc No"
        wrapperStyle={{width: '48%'}}
        value={state.DocNo}
        onChangeText={DocNo =>
          setState(prevstate => ({...prevstate, DocNo: DocNo}))
        }
        inputProps={{
          onSubmitEditing: () => {
            Keyboard.dismiss();
          },
        }}
      />

      <ModalDatePicker
        placeholder={'Doc Date'}
        fieldWrapperStyle={{width: '48%'}}
        selectedValue={state.DocDate}
        onValueSelected={selectedDate =>
          HandleDateChange('DocDate', selectedDate)
        }
      />
    </View>
  );

  const FieldRemarks = key => (
    <RichTextInput
      key={key}
      // inputStyle={{height: null, maxHeight: 100, lineHeight: 15}}
      placeholder="Remarks"
      value={state.Remarks}
      onChangeText={Remarks =>
        setState(prevstate => ({...prevstate, Remarks: Remarks}))
      }
      inputProps={{multiline: true, numberOfLines: 2}}
    />
  );

  const refreshLayout = () => {};
  // console.log('state : ', state);
  return !state.isFetched ? (
    <ICubeAIndicator />
  ) : (
    <>
    <StatusBar backgroundColor={globalColorObject.Color.OffLineStatus} 
    barStyle="default"
    />
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={refreshLayout}>
        <View style={styles.InvoiceTabWrapper}>
          <View
            style={{
              height:
                layoutStyle.FieldContainerCount == 1
                  ? 205
                  : layoutStyle.FieldContainerCount == 2
                  ? 145
                  : 90,
            }}>
            <ScrollView
              keyboardShouldPersistTaps="always"
              nestedScrollEnabled={true}
              style={[styles.ViewPager]}>
              <View style={[layoutStyle.ScrollContentWrapper]}>
                {FieldVendor()}
                {FieldDocInfo()}
                {FieldRemarks()}
              </View>
            </ScrollView>
          </View>
          <View
            style={[
              styles.CreatedItemsTab,
              {backgroundColor: globalColorObject.Color.Lightprimary},
            ]}>
            <FlatList
              style={[
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
              ]}
              data={state.CreatedItems}
              renderItem={({item, index}) => (
                <ListItem
                  data={item}
                  index={index}
                  setMenuRef={ref => setMenuRef(ref, index)}
                  HandleShowMenu={showMenu}
                  headers={visibleCreatedItemsGroupFields}
                  HandleEditProduct={HandleEditItem}
                  HandleRemoveProduct={HandleRemoveItem}
                  HandleQtyChange={HandleProductQtyChange}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={
                <ListItemHeader
                  headers={[...visibleCreatedItemsGroupCaptions]}
                />
              }
              ListFooterComponent={() => (
                <View style={{width: '100%', marginTop: 90}} />
              )}
              stickyHeaderIndices={[0]}
            />
          </View>

          <View
            style={[
              styles.SummaryTab,
              {backgroundColor: globalColorObject.Color.Lightprimary},
            ]}>
            <View style={[styles.SingleLine, {width: '50%'}]}>
              <Text
                style={[
                  styles.AmountLabel,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                Quantity:
              </Text>
              <Text
                style={[
                  styles.AmountText,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                {state.TotalQty}
              </Text>
            </View>
            <View style={[styles.SingleLine, {width: '50%'}]}>
              <Text
                style={[
                  styles.AmountLabel,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                Amount:
              </Text>
              <Text
                style={[
                  styles.AmountText,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                {state.BasicAmount}
              </Text>
            </View>
          </View>
        </View>
        {/* Custom Popup Loader starts here*/}
        {/* Custom Popup Loader ends here*/}
      </LayoutWrapper>
      <ICubeAIndicator isShow={!state.isFetched} />
    </>
  );
};

const styles = StyleSheet.create({
  FieldInput: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    textAlign: 'left',
  },
  closeButtonParent: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
  closeButton: {
    height: 25,
    width: 25,
  },
  ModalLoaderBackDrop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalLoaderContainer: {
    width: '70%',
    height: 100,
    borderRadius: 5,
    flexDirection: 'row',
  },
  ModalLoader: {
    flex: 1,
    alignSelf: 'center',
  },
  ModalLoaderText: {
    flex: 2,
    alignSelf: 'center',
  },
  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  ViewPager: {
    flex: 1,
  },
  Tabs: {
    flexDirection: 'row',
    // borderTopColor: 'rgb(221, 221, 221)',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  TabView: {
    padding: 10,
    // borderBottomWidth: 2,
    width: '100%',
    textAlign: 'center',
  },
  CreatedItemsTab: {
    flex: 1,
    // backgroundColor: '#f9f9f9',
  },
  AddNewItem: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  // Created Items List Styling starts here
  ButtonsContainer: {
    width: '100%',
    paddingVertical: 10,
    flexDirection: 'row',
    // backgroundColor: '#ededed',
    alignItems: 'center',
  },
  SearhBarWrapper: {
    paddingHorizontal: 10,
    flex: 1,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  BtnExtraActionsWrapper: {
    width: 40,
  },
  BtnExtraActions: {
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 3,
    marginVertical: 3,
    marginHorizontal: 13,
  },
  CardBtnOptionWrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  CardBtnOption: {
    width: 35,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  CardBtnOptionText: {
    lineHeight: 25,
    transform: [{scaleX: 1.7}],
    top: -5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  // Created Items List Styling ends here
  SummaryTab: {
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  SingleLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  AmountLabel: {
    textAlign: 'left',
  },
  AmountText: {
    textAlign: 'left',
  },
});

export { AddPurchase};
