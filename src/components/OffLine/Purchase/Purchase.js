import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  StatusBar,
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
// Local Imports
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {FloatingAction} from 'react-native-floating-action';
import {
  GetSessionData,
  ReturnDisplayStringForDate,
  Confirmation,
  isnull,
  AlertMessage,
  Request,
  IsServerConnected,
} from '../../../Helpers/HelperMethods';
import LayoutWrapper, {CardStyle} from '../../Layout/Layout';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';

import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../../Style/GlobalStyle';

import {
  // GetAllSupplier,
  // GetAllPurchaseOrder,
  GetAllPurchaseOrderWithSupplierForPO,
  DeletePurchaseOrder,
  GetSelectedPurchaseOrder,
  UpdateSelectedPurchaseOrderStatus,
} from '../../../Data/DB/Purchase';

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View style={[styles.CardTextContainer, {width: '80%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Vendor
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '20%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            Status
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Invoice
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Doc
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '20%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),

              {textAlign: 'right'},
            ]}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
};

const ListItem = props => {
  const data = props.data;
  let InvoiceDate = ReturnDisplayStringForDate(data.Date);
  let DocDate = data.DocDate ? ReturnDisplayStringForDate(data.DocDate) : '';
  const {CardItemLayout} = CardStyle;

  let statusvalue = data.Status;
  let statuscolor =
    statusvalue == 'Pending' //'Approved'
      ? 'gold'
      : statusvalue == 'Exported' //'Delivered'
      ? 'darkgreen'
      : 'black';
  return (
    <TouchableWithoutFeedback onLongPress={() => props.onLongPress(data)}>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: '80%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Name}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right', color: statuscolor},
              ]}>
              {statusvalue}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.No} {InvoiceDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.DocNo} {DocDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),

                {textAlign: 'right'},
              ]}>
              {(Math.round(data.Amount * 100) / 100).toFixed(2)}
            </Text>
          </View>
        </View>

        <View
          style={[
            {
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              backgroundColor: globalColorObject.Color.Lightprimary,
            },
          ]}>
          <View
            style={[
              {
                width: 0,
                height: 0,
                backgroundColor: globalColorObject.Color.Lightprimary,
                borderRightWidth: 20,
                borderTopWidth: 20,
                borderRightColor: globalColorObject.Color.Lightprimary,
                borderTopColor: globalColorObject.Color.oppPrimary,
              },
              {
                transform: [{rotate: '90deg'}],
              },
            ]}></View>

          <TouchableOpacity
            onPress={() => props.onDeleteInvoice(data)}
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <Image
              source={require('../../../assets/images/Delete.png')}
              style={styles.ActionImage}
            />
          </TouchableOpacity>
          {statusvalue == 'Pending' ? (
            <TouchableOpacity
              onPress={() => props.onSyncInvoice([data])}
              style={{
                paddingVertical: 2,
                paddingHorizontal: 10,
                height: 20,
                backgroundColor: globalColorObject.Color.oppPrimary,
                alignItems: 'center',
              }}>
              <Image
                style={styles.ActionImage}
                source={require('../../../assets/images/post-light.png')}
              />
            </TouchableOpacity>
          ) : (
            <View
              style={{
                paddingVertical: 2,
                paddingHorizontal: 10,
                height: 20,
                backgroundColor: globalColorObject.Color.oppPrimary,
                alignItems: 'center',
              }}>
              <Text style={{height: 18, width: 18}} />
            </View>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const Purchase = props => {
  const [LocalData, setLocalData] = useState({
    Loccode: 0,
    RoleId: '',
    UserCode: '',
    IP: '',
    Token: '',
  });

  const [state, setState] = useState({
    ModalMessage: 'Loading',
    isFetched: false,
    SelectedStatus: 'All',
    SearchText: '',
    show: false,
    POList: [],
    Location: [],
    ListSourceData: [],
    SupplierList: [],
  });

  const ActionButtons = [
    {
      text: 'Sync All',
      icon: require('../../../assets/images/reset-light.png'),
      name: 'SyncAll',
      color: globalColorObject.Color.Primary,
      buttonSize: 45,
      position: 2,
    },
    {
      text: 'New',
      icon: require('../../../assets/images/add-light.png'),
      name: 'New',
      color: globalColorObject.Color.Primary,
      buttonSize: 45,
      position: 3,
    },
  ];

  useEffect(() => {
    // console.log('laod purchase : ', props);
    LoadInitialData();
  }, []);

  const onDeleteSelectedInvoice = async PassPOItem => {
    Confirmation('Do you want to remove ?', () => {
      let getPOList = [...state.POList];
      getPOList.splice(getPOList.indexOf(PassPOItem), 1);
      DeletePurchaseOrder(PassPOItem);
      HandleSearch(state.SearchText, getPOList);
    });
  };

  const ApplyDataForList = data => {
    setState(prevstate => ({
      ...prevstate,
      SupplierList: data.SupplierList || [],
      Location: (data.Location || []).filter(item => item.Extinct == false),
    }));
    // console.log('POList:', JSON.stringify(data.Location));
    HandleSearch(state.SearchText, data.POList || []);
  };

  const SyncPurchaseOrder = async poNumberList => {
    if (await IsServerConnected(true)) {
      setState(prevstate => ({
        ...prevstate,
        isFetched: false,
        ModalMessage: 'Image conversion loading ....',
      }));
      let isCallPost = false;
      let PostDataList = [];
      for (
        let get_PoNumber_Count = 0;
        get_PoNumber_Count < poNumberList.length;
        get_PoNumber_Count++
      ) {
        let get_PODetails = await GetSelectedPurchaseOrder(
          poNumberList[get_PoNumber_Count].No,
        );
        let get_ImageListProcess = [];
        for (let count = 0; count < get_PODetails.Details.length; count++) {
          let get_DetailItem = get_PODetails.Details[count];
          let get_itemimageList = get_DetailItem.ImageData || [];
          for (
            let imagecount = 0;
            imagecount < get_itemimageList.length;
            imagecount++
          ) {
            let get_singleImage = get_itemimageList[imagecount];
            let image_filePath = get_singleImage.Path;
            let get_IsExists = await RNFetchBlob.fs.exists(image_filePath);
            if (get_IsExists) {
              let get_fileimageData = await RNFetchBlob.fs.readFile(
                image_filePath,
                'base64',
              );
              get_singleImage.Image = get_fileimageData;
            }
            get_singleImage.DetailId = get_DetailItem.Id;
          }
          get_ImageListProcess = [
            ...get_ImageListProcess,
            ...get_itemimageList,
          ];
          delete get_DetailItem.ImageData;
        }
        get_PODetails.ImageData = get_ImageListProcess;
        PostDataList.push(get_PODetails);
        isCallPost = true;
      }
      if (isCallPost) {
        setState(prevstate => ({
          ...prevstate,
          isFetched: false,
          ModalMessage: 'Exporting data to server ....',
        }));
        let save_Offline_OrderDEtails = await Request.post_ReturnObject(
          `${LocalData.IP}api/Purchase/OffLinePurchaseExport`,
          PostDataList,
          LocalData.Token,
        );
        if (save_Offline_OrderDEtails.status == '200') {
          let result_Data = save_Offline_OrderDEtails.data || [];
          let get_Failed_Transaction = result_Data.filter(
            item => item.Status != 'Saved',
          );
          let get_saved_Transaction = result_Data.filter(
            item => item.Status == 'Saved',
          );
          if (get_saved_Transaction.length > 0) {
            let get_UpdateStatus = await UpdateSelectedPurchaseOrderStatus(
              get_saved_Transaction,
            ).then(res => {
              return res;
            });
            // console.log('get_UpdateStatus : ', get_UpdateStatus);
            if (get_UpdateStatus.length > 0) {
              LoadPurchaseData(LocalData.Loccode);
            }
          }
          if (get_Failed_Transaction.length > 0) {
            let get_Exportdata = get_Failed_Transaction.map(item => {
              return item.No + ' : ' + item.Message;
            });
            AlertMessage('Export failed.\n' + get_Exportdata.join('\n'));
          } else {
            AlertMessage('Export data successfully.');
          }
        } else {
          AlertMessage('Failed to export.');
        }
      }
      setState(prevstate => ({
        ...prevstate,
        isFetched: true,
        ModalMessage: 'Loading ...',
      }));
    }
  };

  const LoadPurchaseData = async Loccode => {
    // console.log('locc : ', Loccode, ' state loccode : ', LocalData.Loccode);
    GetAllPurchaseOrderWithSupplierForPO(ApplyDataForList, Loccode);
  };

  const LoadInitialData = async () => {
    const {get_UserCode, get_Location, get_RoleId, get_Token, get_IP} =
      await GetSessionData();

    setLocalData({
      Loccode: get_Location,
      RoleId: get_RoleId,
      UserCode: get_UserCode,
      IP: get_IP,
      Token: get_Token,
    });
    // await SavePurchaseForTemp();
    // await SavePurchaseForTempFull();
    LoadPurchaseData(get_Location);

    setState(prevstate => ({
      ...prevstate,
      isFetched: true,
    }));
  };

  const HandleSearch = async (SearchText, getPOList) => {
    let POList = getPOList ? getPOList : state.POList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = [];
    if (isnull(txt, '') != '') {
      ListSourceData = POList.filter(obj => {
        // Get all the texts

        let InvoiceDate = ReturnDisplayStringForDate(obj.Date);
        let DocDate = obj.DocDate
          ? ReturnDisplayStringForDate(obj.DocDate)
          : '';
        let Status = (obj.Status || '').toString().toLowerCase();
        let InvoiceNo = (obj.No || '').toString().toLowerCase();
        let VENDORNAME = (obj.Name || '').toString().toLowerCase();
        let DOCNO = (obj.DocNo || '').toString().toLowerCase();
        let Amount = (Math.round(obj.Amount * 100) / 100).toFixed(2).toString();

        return (
          InvoiceDate.includes(txt) ||
          DocDate.includes(txt) ||
          InvoiceNo.includes(txt) ||
          Status.includes(txt) ||
          VENDORNAME.includes(txt) ||
          DOCNO.includes(txt) ||
          Amount.includes(txt)
        );
      });
    } else {
      ListSourceData = [...POList];
    }
    setState(prevstate => ({
      ...prevstate,
      SearchText: SearchText,
      POList,
      ListSourceData,
    }));
  };

  const showCancel = () => {
    setState(prevstate => ({
      ...prevstate,
      show: true,
    }));
  };

  const hideCancel = () => {
    setState(prevstate => ({
      ...prevstate,
      show: false,
    }));
  };

  const NavigateToPO = async EditData => {
    props.navigation.navigate('OffLine_PurchaseOrderNew', {
      CallFrom: 'PO',
      HeaderTitle: 'Purchase Order',
      EditData: EditData || {},
      Loccode: LocalData.Loccode,
      SupplierList: state.SupplierList,
      UserCode: LocalData.UserCode,
      onUpdateInvoice: LoadPurchaseData.bind(this, LocalData.Loccode),
    });
  };

  const HandleActionButtonClick = name => {
    if (name === 'New') {
      NavigateToPO();
    } else if (name === 'SyncAll') {
      let get_FilterRecord = state.ListSourceData.filter(
        item => item.Status == 'Pending',
      );
      SyncPurchaseOrder(get_FilterRecord);
    }
  };

  const refreshLayout = () => {
    /*this.forceUpdate();*/
  };
  return !state.isFetched ? (
    <ICubeAIndicator ModalText={state.ModalMessage} />
  ) : (
    <>
    <StatusBar backgroundColor={globalColorObject.Color.OffLineStatus} 
          barStyle="default"/>
      <LayoutWrapper
        onLayoutChanged={refreshLayout}
        backgroundColor={globalColorObject.Color.Lightprimary}>
        <View
          style={[
            styles.SearhBarWrapper,
            {
              //paddingBottom: this.LocalData.CallFrom == 'PO' ? 0 : 10,
              width: '100%',
              flexDirection: 'row',
            },
          ]}>
          <TextInput
            style={[
              styles.SearchInput,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderColor,
              ),
              {width: '70%'},
            ]}
            placeholder={'Search here'}
            value={state.SearchText}
            onChangeText={HandleSearch}
            autoCorrect={false}
            returnKeyType="search"
            autoCapitalize="none"
            onFocus={showCancel}
            onBlur={hideCancel}
          />

          <View style={{width: '30%'}}>
            <ModalSearchableLabel
              placeholder="Location"
              fieldWrapperStyle={{height: 30, width: '100%'}}
              data={state.Location}
              labelProp="Name"
              valueProp="Code"
              selectedValue={LocalData.Loccode}
              inputStyle={[
                {
                  paddingLeft: 5,
                  marginTop: 10,
                  marginBottom: 0,
                  borderBottomWidth: 0,
                  textAlign: 'center',
                  textDecorationLine: 'underline',
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderBottomColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
              onValueSelected={Loccode => {
                setLocalData(prevstate => ({
                  ...prevstate,
                  Loccode,
                }));
                LoadPurchaseData(Loccode);
              }}
            />
          </View>
          {/* <View style={{width: '30%'}}>
            <ModalSearchableLabel
              placeholder="Status"
              fieldWrapperStyle={{height: 30, width: '100%'}}
              data={[
                {Label: 'Pending', Status: 'Pending'},
                {Label: 'Exported', Status: 'Exported'},
              ]}
              labelProp="Label"
              valueProp="Status"
              selectedValue={state.SelectedStatus}
              inputStyle={[
                {
                  paddingLeft: 5,
                  marginTop: 10,
                  marginBottom: 0,
                  borderBottomWidth: 0,
                  textAlign: 'center',
                  textDecorationLine: 'underline',
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderBottomColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
              onValueSelected={SelectedStatus => {
                setState(prevstate => ({
                  ...prevstate,
                  SelectedStatus: SelectedStatus,
                }));
                HandleSearch(SelectedStatus != 'All' ? SelectedStatus : '');
              }}
            />
          </View> */}
        </View>
        {state.ListSourceData?.length > 0 && (
          <View style={styles.NoDataBanner}>
            <Text
              style={{
                fontSize: 25,
                color: 'rgba(0,0,0,0.2)',
                textAlign: 'center',
                marginTop: 20,
              }}>
              No Data Found
            </Text>
            <Text
              style={{
                fontSize: 18,
                color: 'rgba(0,0,0,0.2)',
                textAlign: 'center',
                marginTop: 5,
              }}>
              Pull to refresh
            </Text>
          </View>
        )}
        <FlatList
          style={[
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}
          data={state.ListSourceData}
          renderItem={({item, index}) => (
            <ListItem
              data={item}
              onLongPress={NavigateToPO}
              index={index}
              onDeleteInvoice={onDeleteSelectedInvoice}
              onSyncInvoice={SyncPurchaseOrder}
            />
          )}
          keyExtractor={item => item.No.toString()}
          refreshing={false}
          onRefresh={LoadPurchaseData.bind(this, LocalData.Loccode)}
          ListHeaderComponent={<ListItemHeader />}
          stickyHeaderIndices={[0]}
          // to have some breathing space on bottom
          ListFooterComponent={() => (
            <View style={{width: '100%', marginTop: 85}} />
          )}
        />
        <FloatingAction
          actions={ActionButtons}
          floatingIcon={require('../../../assets/images/undo-light.png')}
          onPressItem={HandleActionButtonClick}
          color={globalColorObject.Color.Primary}
          iconWidth={30}
          iconHeight={30}
          onRefresh={() => {}}
          animated={true}
          visible={state.ActivaTab != 'Profile'}
          distanceToEdge={{vertical: 30, horizontal: 30}}
        />
      </LayoutWrapper>
      <ICubeAIndicator isShow={!state.isFetched} />
    </>
  );
};

const styles = StyleSheet.create({
  SearhBarWrapper: {
    padding: 10,
    // backgroundColor: '#ededed',
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
    top: 70,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderRadius: 5,
    // paddingTop: 15,
    // paddingBottom: 7,
    // // borderWidth: 1,
    // borderBottomWidth: 0,
    // borderLeftWidth: 0,
    // borderColor: 'lightgray',
    marginTop: 5,
    marginHorizontal: 9,
    // elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  removeItemIconContainer: {
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },
  removeItemIcon: {
    width: 20,
    height: 20,
    margin: 2,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  closeButton: {
    height: 30,
    width: 30,
    // paddingLeft:30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 12,
    top: 14,
  },

  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});

export {Purchase};
