import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Orientation from 'react-native-orientation';

import LayoutWrapper from '../../components/Layout/Layout';
import {
  Confirmation,
  AlertMessage,
  IsServerConnected,
} from '../../Helpers/HelperMethods';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
} from '../../Style/GlobalStyle';
const OffLineHome = props => {
  const [isOnline, setisOnline] = useState(false);
  const [OfflineForms, setOfflineForms] = useState({
    isPurchaseOrder: false,
    isSalesOrder: false,
  });
  const NavigateToForm = passNavigationname => {
    props.navigation.navigate(passNavigationname, {
      HeaderTitle: 'Order',
      CallFrom: 'PO',
      FormIndex: '3_1_0_0',
    });
  };

  useEffect(() => {
    Orientation.unlockAllOrientations();
    UpdateInterNetStatus(false, false);
    GetOffLineFormList();
  }, []);

  const GetOffLineFormList = async () => {
    let get_FormsList = await AsyncStorage.getItem('OffLine_Forms');
    get_FormsList = JSON.parse(get_FormsList);
    // console.log('get_FormsList : ', JSON.stringify(get_FormsList));
    let isPurchaseOrder =
      (get_FormsList || []).filter(r => r.SeqNo == '3_1_0_0').length > 0;
    let isSalesOrder =
      (get_FormsList || []).filter(r => r.SeqNo == '4_5_0_0').length > 0;
    setOfflineForms({isPurchaseOrder, isSalesOrder});
  };

  const UpdateInterNetStatus = async (
    showmessage = true,
    isNavigate = false,
  ) => {
    let isConnect = await IsServerConnected(showmessage);
    setisOnline(isConnect);
    if (isConnect && isNavigate) {
      props.runinDiffType && props.runinDiffType('Online');
    }
  };
  // const NavigateToOnline = async () => {
  //   let isConnect = await IsServerConnected();
  //   console.log('isConnect', isConnect);
  //   setisOnline(isConnect);
  //   if (isConnect) {
  //     props.runinDiffType && props.runinDiffType('Online');
  //   } else {
  //     AlertMessage('Please check the network connection to go online.');
  //   }
  // };

  return (
    <LayoutWrapper
      backgroundColor={globalColorObject.Color.oppPrimary}
      onLayoutChanged={() => {}}>
      <ScrollView
        style={[
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
          {paddingVertical: 10},
        ]}>
        {OfflineForms.isPurchaseOrder && (
          <View
            style={{
              flexDirection: 'column',
              paddingBottom: 5,
            }}>
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.mm,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {
                  textAlign: 'left',
                  paddingLeft: 5,
                  textDecorationLine: 'underline',
                },
              ]}>
              OffLine
            </Text>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 5,
              }}>
              <TouchableOpacity
                disabled={!isOnline}
                style={
                  isOnline
                    ? styles.FormContainerEnable
                    : styles.FormContainerDisable
                }
                onPress={NavigateToForm.bind(this, 'OffLine_ImportData')}>
                <Image
                  source={require('../../assets/images/sort_down.png')}
                  style={styles.FormImages}
                />
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sxl,
                    ),
                    styles.FormLabel,
                  ]}>
                  Import
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {OfflineForms.isPurchaseOrder && (
          <View
            style={{
              flexDirection: 'column',
              paddingBottom: 5,
            }}>
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.mm,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {
                  textAlign: 'left',
                  paddingLeft: 5,
                  textDecorationLine: 'underline',
                },
              ]}>
              Purchase
            </Text>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 5,
              }}>
              <TouchableOpacity
                style={styles.FormContainer}
                onPress={NavigateToForm.bind(this, 'OffLine_PurchaseOrder')}>
                <Image
                  source={require('../../assets/images/FieldList1.png')}
                  style={styles.FormImages}
                />
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sxl,
                    ),
                    styles.FormLabel,
                  ]}>
                  Order
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
              style={styles.FormContainer}
              // onPress={NavigateToForm.bind(this, 'OffLine_ImportData')}
            >
              <Image
                source={require('../../assets/images/Home/Procurement/purchase.png')}
                style={styles.FormImages}
              />
              <Text
                style={[
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sxl,
                  ),
                  styles.FormLabel,
                ]}>
                Sales Order
              </Text>
            </TouchableOpacity> */}
            </View>
          </View>
        )}
      </ScrollView>
      <TouchableOpacity
        style={[
          styles.AddNewInvoice,
          ApplyStyleColor(
            globalColorObject.Color.Primary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}
        onPress={() =>
          Confirmation(
            'Are you sure want to go online ?',
            UpdateInterNetStatus.bind(this, true, true),
          )
        }>
        <Image
          style={{width: 30, height: 30}}
          source={require('../../assets/images/logout-white.png')}
        />
      </TouchableOpacity>

      {/* <View style={styles.LoadContainer}>
        <TouchableOpacity
          style={styles.FormContainer}
          onPress={NavigateToForm.bind(this, 'OffLine_PurchaseOrder')}>
          <Image
            source={require('../../assets/images/share.png')}
            style={styles.FormImages}
          />
          <Text style={styles.FormLabel}>Purchase Order</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.FormContainer}
          onPress={NavigateToForm.bind(this, 'OffLine_ImportData')}>
          <Image
            source={require('../../assets/images/share.png')}
            style={styles.FormImages}
          />
          <Text style={styles.FormLabel}>Import</Text>
        </TouchableOpacity>
      </View> */}
    </LayoutWrapper>
  );
};

const styles = StyleSheet.create({
  FormImages: {
    width: 30,
    height: 30,
    margin: 5,
    resizeMode: 'stretch',
  },
  AddNewInvoice: {
    width: 55,
    height: 55,
    position: 'absolute',
    bottom: 30,
    right: 10,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  FormLabel: {
    textAlign: 'center',
    // fontWeight: 'bold',
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  FormContainerDisable: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
    backgroundColor: 'lightgray',
  },
  FormContainerEnable: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
  },
  FormContainer: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
  },
  TabContainer: {
    width: '20%',
    alignItems: 'center',
    borderTopWidth: 3,
  },
  TabImageContainer: {
    width: 24,
    height: 26,
    resizeMode: 'stretch',
  },
});

export default OffLineHome;
