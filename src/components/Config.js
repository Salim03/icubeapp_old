import React from 'react';
import {StackActions} from '@react-navigation/native';

import {
  Text,
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  Image,
  ScrollView,
} from 'react-native';
import {Button} from '../components/Custom/Button';
import ICubeAIndicator from '../components/Tools/ICubeIndicator';

import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../Style/GlobalStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Orientation from 'react-native-orientation';
import ModalSearchablePicker from './Custom/ModalSearchablePicker';
import {
  AlertMessage,
  Request,
  LogoutUser,
  GetSessionData,
  isnull,
} from '../Helpers/HelperMethods';

export default class Config extends React.Component {
  constructor() {
    super();
    this.state = {
      IP: '',
      DashboardURL: '',
      isShowLocation: true,
      LocationCode: '0',
      LocationList: [],
      ApplyText: 'Next',
      isLoading: false,
    };
  }

  componentDidMount() {
    Orientation.lockToPortrait();
    this.CheckAssignedIP();
  }
  CheckAssignedIP = async () => {
    const {get_IP, get_Location} = await GetSessionData();
    this.setState({IP: get_IP});
    if (isnull(get_IP, '') != '') {
      this.HandleLoctiondata(get_IP, parseInt(isnull(get_Location, 0)), true);
    }
  };
  ChangeIp = async () => {
    let {IP} = this.state;
    IP = IP.trim();
    if (IP === null || IP === '') {
      AlertMessage('Enter IP Address');
      this.refs.ip.focus();
    } else {
      Keyboard.dismiss();
      IP = IP.startsWith('http') ? IP : 'http://' + IP;
      this.HandleLoctiondata(IP, this.state.LocationCode);
    }
  };
  ConfigureIPAddress = async () => {
    let {IP, LocationCode, LocationList, DashboardURL} = this.state;
    console.log(
      'loca list : ',
      LocationList.find(r => r.LocationCode == LocationCode) || [],
    );
    IP = IP.trim();
    if (IP === null || IP === '') {
      AlertMessage('Enter IP Address');
      this.refs.ip.focus();
    } else if (LocationList.length == 0) {
      AlertMessage('No Location available');
    } else if (
      (LocationList.find(r => r.LocationCode == LocationCode) || []).length == 0
    ) {
      AlertMessage('Invalid Location selected.');
    } else {
      Keyboard.dismiss();
      IP = IP.startsWith('http') ? IP : 'http://' + IP;
      await AsyncStorage.multiSet([
        ['IP', IP],
        ['LocationCode', LocationCode.toString()],
        ['DashboardURL', DashboardURL],
      ]);
      LogoutUser();
      this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
    }
  };

  HandleLoctiondata = async (PassIp, LocationCode, isCallFromLoad) => {
    this.setState(prevstate => ({
      ...prevstate,
      isLoading: true,
    }));
    Request.get(`${PassIp}api/common/getactivelocation`)
      .then(res => {
        if (res.data.length > 0) {
          let Loca_Code = LocationCode;
          if (res.data.length > 0 && Loca_Code == 0) {
            Loca_Code = res.data[0].LocationCode;
          }
          this.setState({
            LocationList: res.data,
            LocationCode: Loca_Code,
            ApplyText: isCallFromLoad || Loca_Code == 0 ? 'Next' : 'Apply',
            isLoading: false,
          });
          return;
        } else {
          this.setState({
            LocationList: [],
            ApplyText: 'Next',
            isLoading: false,
          });
          AlertMessage('No Location found');
        }
      })
      .catch(err => {
        this.setState({
          LocationList: [],
          isLoading: false,
        });
        console.error(err);
        AlertMessage('Something Went wrong');
      });
  };

  HandleChangeDashboardURL = event => {
    this.setState({
      DashboardURL: event.nativeEvent.text,
    });
  };
  HandleChange = event => {
    this.setState({
      IP: event.nativeEvent.text,
    });
  };
  render() {
    return (
      <>
        <ICubeAIndicator isShow={this.state.isLoading} />
        <ScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}
          style={styles.ScrollViewWrapper}>
          <Text
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.mediam.ml,
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.Color,
              ),
              styles.captionTextCustom,
            ]}>
            Configuration
          </Text>
          <View style={styles.InputsContainer}>
            <TextInput
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sxxl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                styles.Inputs,
              ]}
              placeholder="Please enter cloud or server ID"
              ref={'ip'}
              onChange={this.HandleChange}
              value={this.state.IP}
              autoCapitalize="none"
              editable={this.state.ApplyText != 'Apply'}
            />
            {/* <TouchableOpacity
              style={styles.ConfigButon}
              onPress={this.ChangeIp}>
              <Image
                resizeMode="stretch"
                source={require('../assets/images/post-light.png')}
                style={styles.FormImages}
              />
            </TouchableOpacity> */}
          </View>
          {this.state.ApplyText == 'Apply' && (
            <View style={styles.ScrollContentWrapper}>
              <ModalSearchablePicker
                placeholder="Location"
                fieldWrapperStyle={{width: '98%'}}
                data={this.state.LocationList}
                labelProp="LocationName"
                valueProp="LocationCode"
                selectedValue={this.state.LocationCode}
                onValueSelected={LocationCode =>
                  this.setState({LocationCode: LocationCode})
                }
                inputStyle={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sxl,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BorderBottomColor,
                  ),
                ]}
              />
            </View>
          )}
          {/* <View style={styles.Dashboard}>
          </View> */}
          {/* <TextInput
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sxxl,
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderColor,
              ),
              styles.Dashboard,
            ]}
            placeholder="Dashboard URL"
            onChange={this.HandleChangeDashboardURL}
            value={this.state.DashboardURL}
            autoCapitalize="none"
          /> */}
          <Button
            value={this.state.ApplyText == 'Apply' ? 'Apply' : 'Connect'}
            onPressEvent={
              this.state.ApplyText == 'Apply'
                ? this.ConfigureIPAddress
                : this.ChangeIp
            }
          />
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  ScrollViewWrapper: {
    flex: 1,
    paddingHorizontal: 8,
    paddingTop: '40%',
  },
  Dashboard: {
    paddingVertical: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    zIndex: 0,
    borderBottomWidth: 2,
  },
  ScrollContentWrapper: {
    paddingVertical: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 10,
    zIndex: 0,
  },
  WrapperContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: 'white',
  },
  captionTextCustom: {
    paddingLeft: 10,
    marginBottom: 10,
  },
  InputsContainer: {
    width: '100%',
    flexDirection: 'row',
    paddingHorizontal: 10,
    height: 45,
  },
  FormImages: {
    width: 40,
    height: 45,
    margin: 2,
  },
  Inputs: {
    borderBottomWidth: 2,
    color: 'black',
    width: '100%',
    borderRadius: 8,
  },
  ConfigButon: {
    paddingHorizontal: 10,
    borderRadius: 8,
    height: 40,
  },
});
