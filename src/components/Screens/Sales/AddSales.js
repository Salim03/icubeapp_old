/* eslint-disable no-alert */
/* eslint-disable eqeqeq */
/* eslint-disable react/no-string-refs */
/* eslint-disable radix */
/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {Component} from 'react';
import {
  Alert,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Text,
  TextInput,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
  FlatList,
  Image,
  Modal,
  Switch,
  ScrollView,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {
  Confirmation,
  AlertMessage,
  AlertError,
  AlertStatusError,
  Request,
  getPropsFromArray,
  GetSessionData,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import {AccordionStyle} from '../../Custom/Accordion';
import RichTextInput from '../../Custom/RichTextInput';
import Ripple from 'react-native-material-ripple';

import {FloatingAction} from 'react-native-floating-action';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import ViewPager from '@react-native-community/viewpager';
import Menu, {MenuItem} from 'react-native-material-menu';
import {CameraScreen} from 'react-native-camera-kit';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';


const ListItemHeader = props => {
  
  return (
    <>
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardTextContainer, {width: '25%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Barcode
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: '50%', marginLeft: 3}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Description
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: '10%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}
        />
      </View>
      <View style={[styles.CardTextContainer, {width: '25%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Qty
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: '25%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Price
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: '20%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          MRP
        </Text>
      </View>
      <View
        style={[
          styles.CardTextContainer,
          {width: '30%', alignItems: 'flex-end'},
        ]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSize(
              globalFontObject.Font.Bold,
              globalFontObject.Size.sxxl,
            ),
          ]}>
          Amount{' '}
        </Text>
      </View>

    </View>
      {props.showPrevList ?
      <View style={[styles.CardTextContainer, {width: '100%',backgroundColor:globalColorObject.Color.oppPrimary,padding:15}]}>
          <TouchableOpacity onPress={props.onShowPrevs}>
              <Text style={{textAlign:'center',fontSize:globalFontObject.Size.mediam.ms}}>Previous Lists.....</Text>
          </TouchableOpacity>
      </View>
      :<></>}
</>
  );
};

const ListItem = props => {
  const {CardItemLayout} = CardStyle;
  try {
    return (
      <>
        <View style={styles.ListCard}>
          <View style={styles.CardLineList}>
            <View style={{flexDirection: 'row'}}>
              
              <View style={[styles.CardTextContainer, {width: '30%'}]}>
                <Text numberOfLines={1} style={[styles.cardTextFieldList]}>
                  {props.data.Barcode || props.data.Icode}
                </Text>
              </View>
              <View style={[styles.CardTextContainer, {width: '60%'}]}>
                <Text numberOfLines={1} style={[styles.cardTextFieldList]}>
                  {props.data.Description || ''}
                </Text>
              </View>
              <View
                style={[styles.Shar, {width: '10%', alignItems: 'flex-end'}]}>
                <TouchableOpacity onPress={props.onPress}>
                  <Image
                    source={require('../../../assets/images/Delete.png')}
                    style={[styles.removeItemIconContainer]}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flexDirection: 'row',paddingTop:20}}>
              <View style={[styles.CardTextContainer, {width: '30%',flexDirection:'row'}]}>
              <TouchableOpacity
              
              onPress={() => props.quantityHandler('less', props.index,1)}
              >
              <Image
                style={[styles.ConfigLogo, /*{marginTop: 20}*/]}
                source={require('../../../assets/images/minus-salmon.png')}
              />
               </TouchableOpacity>
               <TextInput
                 style={[/*styles.cardTextFieldList,*/
                 {marginLeft:5 ,paddingBottom:10,
                  marginTop:-5,
                  height: 40,
                  width: 40,
                  color: 'black',
                  alignItems: 'center',
                  textAlign:'center',
                  borderRadius: 5,
                  borderColor:'gray',
                  borderWidth: 1,
                  fontSize: 14,
                 /*backgroundColor: '#6ED4C8'*/}]}
                keyboardType="numeric"
                returnKeyType="next"
                autoCorrect={false}
                autoCapitalize="none"
               
                onChangeText={Quantity => {
                  props.quantityHandler('input', props.index,Quantity);
                }}
                value={`${props.data.Quantity}` }
              />
                {/* <Text numberOfLines={1} style={[styles.cardTextFieldList,{marginLeft:5 ,marginRight:15}]}>
                  {' '}
                  {props.data.Quantity || 0}
                </Text> */}
                <TouchableOpacity
                onPress={() => props.quantityHandler('add', props.index,1)}
                >
                <Image
                  style={[styles.ConfigLogo, /*{marginTop: 20}*/]}
                  source={require('../../../assets/images/icons8_add_48px.png')}
                />
              </TouchableOpacity>
              </View>
              <View style={[styles.CardTextContainer, {width: '30%'}]}>
                <Text numberOfLines={1} style={[styles.cardTextFieldList]}>
                  {' '}
                  {props.data.Price || 0}
                </Text>
              </View>
              {/* <View style={[styles.CardTextContainer, {width: '25%'}]}>
              <TextInput
                key={'Q'+props.index.toString()}
                style={[styles.cardTextFieldList, styles.BoldText]}
                keyboardType="numeric"
                onChangeText={Quantity => {
                  props.onQtyChange(props.index, Quantity);
                }}
                value={(props.data.Quantity || 0).toString()}
              />
            </View>
            <View style={[styles.CardTextContainer, {width: '25%'}]}>
              <TextInput
                key={'R'+props.index.toString()}
                style={[styles.cardTextFieldList]}
                keyboardType="numeric"
                onChangeText={Rate => {
                  props.onPriceChange(props.index, Rate);
                }}
                value={(props.data.Price || 0).toString()}
              />
            </View> */}
              <View style={[styles.CardTextContainer, {width: '20%'}]}>
                <Text numberOfLines={1} style={[styles.cardTextFieldList]}>
                  {' '}
                  {props.data.ListedMRP || 0}
                </Text>
              </View>
              <View
                style={[
                  styles.CardTextContainer,
                  {width: '20%', alignItems: 'flex-end'},
                ]}>
                <Text
                  numberOfLines={1}
                  style={[styles.cardTextFieldList, styles.BoldText]}>
                  {isNaN(props.data.Amount ) ? 0.00:(Math.round(props.data.Amount * 100) / 100).toFixed(2) }{' '}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  } catch (ex) {}
};
const DBDupListItemHeader = () => {
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Barcode
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '70%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Description
          </Text>
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            AvailableQty
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '20%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Qty
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '20%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            WSP
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {width: '20%', alignItems: 'center'},
          ]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            MRP{' '}
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '10%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}
          />
        </View>
      </View>
    </View>
  );
};

const DBDupListItem = props => {
  console.log('Db Barcode Dup', props.data);
  return (
    <View style={styles.ListCard}>
      <View style={styles.CardLineList}>
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.CardTextContainer, {width: '30%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {props.data.Barcode}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '70%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {props.data.Description}
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={[styles.CardTextContainer, {width: '30%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {props.data.AvailableQty}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <TextInput
              style={{
                height: 40,
                width: 50,
                color: 'black',
                alignItems: 'center',
                borderRadius: 5,
                fontSize: 14,
                backgroundColor: '#6ED4C8',
              }}
              keyboardType="numeric"
              onChangeText={Quantity => {
                props.onQtyChange('Qty', props.index, Quantity);
              }}
              value={props.data.Quantity.toString()}
            />
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <Text numberOfLines={1} style={{}}>
              {props.data.WSP.toString()}
            </Text>
          </View>
          <View
            style={[
              styles.CardTextContainer,
              {width: '20%', alignItems: 'center'},
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {props.data.ListedMRP}
            </Text>
          </View>
          <View style={{alignItems: 'center', width: '10%'}}>
            <Switch
              style={{
                marginRight: 10,
                transform: [{scaleX: 0.8}, {scaleY: 0.8}],
              }}
              onValueChange={Select => {
                props.onQtyChange('isActive', props.index, Select);
              }}
              value={props.data.isActive}
            />
          </View>
        </View>
      </View>
    </View>
  );
};
const DiscountListItem = props => {
  // console.log('DiscountListItem',props)
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        props.onPress(
          props.data.Factor,
          props.data.Type,
          props.data.AllowChangable,
          props.index,
        )
      }>
      <View style={{flexDirection: 'row', padding: 8, width: '100%'}}>
        <Text style={{fontSize: 16, textAlign: 'center', width: '33.33%'}}>
          {props.data.DiscountName}
        </Text>
        <Text style={{fontSize: 16, textAlign: 'center', width: '33.33%'}}>
          {props.data.Factor}
        </Text>
        <Text style={{fontSize: 16, textAlign: 'center', width: '33.33%'}}>
          {props.data.Type}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class AddSales extends Component {
  //
  constructor(props) {
    super(props);
    // Local Object to store all the navigation and other informations
    this.ExistingBarcode = [];
    this.LocalData = {
      CallFrom: 'SI',
      InvoiceNo: '',
      Loccode: '1',
      RoleId: 'R1',
      FinacialYearId: 1,
    };

    this.APILink = {
      API_LoadInitialData: '/api/WholeSale/Sales_InitialData?', //CallFrom=PO&LocCode=1
      API_AgainstList: '/api/WholeSale/AgaintDataSourceList?', //CallFrom={CallFrom}&Vendor={Vendor}&InvoiceNo={InvoiceNo}&InvoiceType={InvoiceType}&BasedOn={BasedOn}&LocCode={LocCode}`
      API_CustomerList: '/api/WholeSale/CustomerList?',
      API_SalesSave: '/api/WholeSale/SalesSave',
      API_SelectedInvoiceDetails: '/api/WholeSale/GetSelectedInvoiceDetails?',
      API_SelectedAgainstInvoiceDetails:
        '/api/WholeSale/GetSelectedAgainstInvoiceDetails?',
      API_ItemBindDataForSearchItemInfo:
        '/api/WholeSale/GetItemBindDataForSearchItemInfo?',
      API_GetDiscount: '/api/WholeSale/GetDiscount?CallFrom=DC&LocCode=1',
      API_StatusChange: '/api/SoApproval/ApprovalSave?',
    };
    this.state = {
      isFetched: false,
      PrevList:[],
      // Holds the data to be set to controls ( filled later by the data from API )
      APIData: {
        InvoiceType: [],
        BasedOnList: [],
        AgainstList: [],
        PaymentTermList: [],
        SalesAccountList: [],
        ReqCompanyList: [],
        StockPoint: [],
        CustomerList: [],
        TransporterList: [],
        AgentList: [],
        OrderModeList: [],
        CurrencyList: [],
        ScanBarcode: [],
        DupBarcodeList: [],
        ItemsList: [],
        Qty: [],
        ReceivedRatio: [],
        Charges: [],
        Discount: [],
        SetcodeList: [],
        CategoryList: [],
      },
      TotalCategoryRatioInfo: {Value: 0, Size: ''},
      CategoryRatioInfo: {
        StyleCategory: 'Cat3',
        SizeCategory: 'Cat9',
        isCategoryRatio: false,
        CategoryRatio: [],
        CategoryRatioSize: [],
        UniqueSizeList: [],
      },
      //Modal
      isCategoryConfig: false,
      isRRatio: false,
      isCharges: false,
      isDupBarcode: false,
      isDiscount: false,
      // Hold selected or entered values of controls
      StatusFlag: 0,
      Status: '',
      InvoiceID: '0',
      InvoiceType: '',
      InvoiceNO: '',
      InvoiceDate: '',
      Type: 1,
      LOCCODE: this.LocalData.Loccode,
      BasedOn: '',
      Against: '',
      Customer: '',
      DocNo: '',
      DocDate: '',
      GroupBarcode: 'Default',
      BuyerName: '',
      PaymentTerm: '',
      StockPoint: '1',
      Barcode: '',
      Round_Off: 0.0,
      Remarks: '',
      ValidFrom: '',
      ValidTo: '',
      Transporter: '',
      Agent: '',
      Currency: '',
      OrderMode: '',
      IsCashSales: false,
      BasicAmount: 0.0,
      NetAmount: 0.0,
      NetQty:0.0,
      NewRefAmount: '0.00',
      SalesAccountID: '',
      ReqLocationCode: '',
      Agentcommission: '',
      AgainstRefAmount: '',
      AgainstIDList: '',
      AgainstSignList: '',
      AgainstAdjusedAmountList: '',
      AgainstDiscountAmountList: '',
      AgainstTDSAmountList: '',
      AgainstOthersAmountList: '',
      AgainstAmountList: '',
      CreatedByOrModifyBy: '',
      CustomField1: '',
      CustomField2: '',
      CustomField3: '',
      CustomField4: '',
      CustomField5: '',
      CFCombo1: '',
      CFCombo2: '',
      CFRadio1: true,
      CFRadio2: true,
      CFDate1: '',
      CFDate2: '',
      IsSAB: false,
      TermCode: '',
      ActiveTab: 'Invoice',
      MDiscount: '0.00',
      isChangeDisc: false,
      openScanner: false,
      qrvalue: '',
      isAllDisc: false,
      isSetcode: false,
    };

    this.ScanBarApidata ={
       EntIDLists :0,
       UOMIDLists :0,
       SeqnoLists :0,
       MaxSeqNos  :0,
       c:0,
       netAmt:0,
       netQty:0,
       pIndex:0,
       pQty:0.0,


    }
    this.PrevItems =[]
    this.ActionButtons = [
      {
        text: 'Clear',
        icon: require('../../../assets/images/Clear.png'),
        name: 'Clear',
        color: globalColorObject.Color.Primary,
        buttonSize: 45,
        position: 1,
      },
      {
        text: 'Save',
        icon: require('../../../assets/images/Save.png'),
        name: 'Save',
        color: globalColorObject.Color.Primary,
        buttonSize: 45,
        position: 2,
      },
    ];
  }

  NavigateBackToList = () => {
    this.props.navigation.goBack();
    this.props.route.params.onUpdateInvoice();
  };

  HandleClearData = () => {
    //	console.log(this.state.Customer);
    this.setState(prevState => ({
      StatusFlag: 0,
      Status: '',
      InvoiceID: '0',
      InvoiceType: '',
      LOCCODE: '1',
      Type: '1',
      BasedOn: '',
      Against: '',
      Customer: '',
      DocNo: '',
      DocDate: '',
      GroupBarcode: 'Default',
      PaymentTerm: '',
      StockPoint: '',
      Barcode: '',
      Remarks: '',
      ReqCompany: '',
      ValidFrom: '',
      ValidTo: '',
      Transporter: '',
      Agent: '',
      Currency: '',
      OrderMode: '',
      BasicAmount: 0.0,
      NetAmount: 0.0,
      NewRefAmount: '',
      AgainstRefAmount: '',
      AgainstIDList: '',
      AgainstSignList: '',
      AgainstAdjusedAmountList: '',
      AgainstDiscountAmountList: '',
      AgainstTDSAmountList: '',
      AgainstOthersAmountList: '',
      AgainstAmountList: '',
      CreatedByOrModifyBy: '',
      CustomField1: '',
      CustomField2: '',
      CustomField3: '',
      CustomField4: '',
      CustomField5: '',
      CFCombo1: '',
      CFCombo2: '',
      CFRadio1: true,
      CFRadio2: true,
      CFDate1: '',
      CFDate2: '',
      APIData: {...prevState.APIData, ScanBarcode: []},
    }));
    this.PrevItems=[];
    this.ScanBarApidata.EntIDLists=0;
    this.ScanBarApidata.UOMIDLists=0;
    this.ScanBarApidata.SeqnoLists=0;
    this.ScanBarApidata.MaxSeqNos=0;
    this.ScanBarApidata.c=0;
    this.ScanBarApidata.netAmt=0.0;
    this.ScanBarApidata.netQty=0.0;
    this.ScanBarApidata.pIndex=0;
    this.ScanBarApidata.pQty=0
  };
  CategoryRatioApply = () => {
    try {
      let GetTemp_CAtegoryRAtio = JSON.parse(
        JSON.stringify(this.state.CategoryRatioInfo.CategoryRatio),
      );
      var ScanItemDetails = (this.state.APIData.ScanBarcode || []).map(
        itertaeCAtegoryData => {
          // let it_Icode = itertaeCAtegoryData.Icode;
          let it_Category =
            itertaeCAtegoryData[this.state.CategoryRatioInfo.StyleCategory];
          let it_Size =
            itertaeCAtegoryData[this.state.CategoryRatioInfo.SizeCategory];
          // if (it_Category != '' || it_Size != '38/M') {
          //   return;
          // }
          // console.log(
          //   'Apply data = Icode : ',
          //   it_Icode,
          //   ' Category : ',
          //   it_Category,
          //   ' Size : ',
          //   it_Size,
          // );

          let TotalsizeLength = GetTemp_CAtegoryRAtio[it_Category].length;
          for (let i = 0; i < TotalsizeLength; i++) {
            let element = GetTemp_CAtegoryRAtio[it_Category][i];
            if (element.Size == it_Size) {
              let avgQty = element.Quantity / element.IcodeCount;
              avgQty = avgQty < 1 ? 1 : Math.round(avgQty);
              // console.log(
              //   'AllottedIcodeCount:',
              //   element.AllottedIcodeCount,
              //   ' IcodeCount:',
              //   element.IcodeCount,
              // );
              let ApplyQty =
                element.Quantity <= element.AllottedQuantity
                  ? 0
                  : element.AllottedIcodeCount + 1 == element.IcodeCount
                  ? element.Quantity - element.AllottedQuantity
                  : element.Quantity - element.AllottedQuantity < avgQty
                  ? element.Quantity - element.AllottedQuantity
                  : avgQty;
              if (ApplyQty > 0) {
                element.AllottedQuantity += ApplyQty;
              }
              element.AllottedIcodeCount += 1;
              itertaeCAtegoryData.Quantity = ApplyQty;
              itertaeCAtegoryData.Amount = this.CalculateTotalAmount(
                itertaeCAtegoryData.Quantity,
                itertaeCAtegoryData.Price,
                itertaeCAtegoryData.Discount,
              );
              // console.log(
              //   'Quantity:',
              //   element.Quantity,
              //   ' AllottedQuantity:',
              //   element.AllottedQuantity,
              //   ' ApplyQty:',
              //   ApplyQty,
              // );
              break;
            }
          }
          return itertaeCAtegoryData;
        },
      );

      let Total_Amount = ScanItemDetails.reduce(
        (accum, item) => accum + item.Amount,
        0,
      );
      this.setState(prevState => ({
        NetAmount: Total_Amount,
        BasicAmount: Total_Amount,
        APIData: {...prevState.APIData, ScanBarcode: ScanItemDetails || []},
      }));
      this.HandleisShowCAtegoryRatio(false);
      AlertMessage('Apply ratio completed');
      // console.log('Apply data : ', ScanItemDetails);
    } catch (ex) {
      AlertMessage('Error while apply ratio : ', ex.message);
    }
  };
  HandleActionButtonClick = name => {
    if (name === 'Save') {
      this.SaveSales();
    } else if (name === 'Clear') {
      this.HandleClearData();
    }
  };
  quantityHandler =(action,index,qty)=>{
    // console.log(action,index,this.state.APIData);
    let existData = this.state.APIData.ScanBarcode
    let NetValue
    // console.log('state',this.state);
    //   console.log('ssss',existData);
      if(action==='add'){
        existData[index].Quantity =(parseFloat(existData[index].Quantity)  +1).toString();
        //console.log('existData[index].Quantity.toString()  +1', typeof((existData[index].Quantity  +1).toString()));
        // existData[index].Amount += (existData[index].WSP-existData[index].CustomerDiscount)
        existData[index].Amount += existData[index].WSP

        this.setState((prev)=> ({APIData:{...prev,ScanBarcode:[...existData]}}))
        //  NetValue = this.state.APIData.ScanBarcode.reduce(
        //   (accum, item) => accum + parseFloat(item.Amount),
        //   0,
        // )
        this.ScanBarApidata.netAmt += existData[index].WSP
        this.ScanBarApidata.netQty += 1
        this.setState({NetAmount:parseFloat(this.ScanBarApidata.netAmt).toFixed(2),
                        NetQty:parseFloat(this.ScanBarApidata.netQty).toFixed(2)
                      })
        this.ScanBarApidata.pIndex=index
        this.ScanBarApidata.pQty=parseFloat(existData[index].Quantity)
        // this.setState(
        //   prevState => ({
        //     CategoryRatioInfo: {
        //       ...prevState.CategoryRatioInfo,
        //       SizeCategory,
        //       StyleCategory,
        //     },
        //   }),
        console.log('NetValue',NetValue);
         console.log('sssss',this.state)
      }
      
    
    else if(action==='less'){
      if(existData[index].Quantity >1){
        existData[index].Quantity =(parseFloat(existData[index].Quantity)  -1).toString()
        existData[index].Amount -= existData[index].WSP;
        this.setState((prev)=> ({APIData:{...prev,ScanBarcode:[...existData]}}));
        //  NetValue = this.state.APIData.ScanBarcode.reduce(
        //   (accum, item) => accum + parseFloat(item.Amount),
        //   0,
        // )
        this.ScanBarApidata.netAmt -= existData[index].WSP
        this.ScanBarApidata.netQty -= 1
        this.setState({NetAmount:parseFloat(this.ScanBarApidata.netAmt).toFixed(2),
                        NetQty:parseFloat(this.ScanBarApidata.netQty).toFixed(2)
                      })
        this.ScanBarApidata.pIndex=index
        this.ScanBarApidata.pQty=parseFloat(existData[index].Quantity)
        // this.setState({NetAmount:parseFloat(NetValue).toFixed(2)})
      }
      else{
          // existData[index].Quantity >1?existData[index].Quantity -=1 
        AlertMessage("Minimum Qty is 1");
        // existData[index].Quantity -=1
        console.log(existData[index].Quantity)

      }
      
   
    }
    else{
      // 
      let eQty = existData[index].Quantity 
      existData[index].Quantity =qty;
      existData[index].Amount = existData[index].WSP* parseFloat(qty);
      this.setState((prev)=> ({APIData:{...prev,ScanBarcode:[...existData]}}))
      //  NetValue = this.state.APIData.ScanBarcode.reduce(
      //   (accum, item) => accum + parseFloat(item.Amount),
      //   0,
      // )
      let not_NaNQty=qty.toString() ===''? 0 : parseFloat(qty)
     
      if(this.ScanBarApidata.pIndex ==index ){
        this.ScanBarApidata.netAmt -=  existData[index].WSP* this.ScanBarApidata.pQty
        this.ScanBarApidata.netQty -= this.ScanBarApidata.pQty
        console.log('itssssssssssss pQty',existData[index].WSP * this.ScanBarApidata.pQty);
        console.log('itssrrrrs pQty',this.ScanBarApidata.pQty);
        // console.log('type of ',typeof qty);
        // console.log('type of convert ',typeof qty.toString());
        // console.log('type of ',qty.toString() ===''? 0 : parseFloat(qty))
        
       
      }
      else{
        this.ScanBarApidata.netAmt -= existData[index].WSP* parseFloat(eQty==""?0:eQty)
        this.ScanBarApidata.netQty -= parseFloat(eQty==""?0:eQty)
      }
      
      this.ScanBarApidata.netAmt += existData[index].WSP* parseFloat(existData[index].Quantity==""?0:existData[index].Quantity)
      this.ScanBarApidata.netQty += not_NaNQty
      console.log('this.ScanBarApidata.netQty',this.ScanBarApidata.netQty);
      console.log('ScanBarApidata.netAmt',this.ScanBarApidata.netAmt);
      this.setState({NetAmount:parseFloat(this.ScanBarApidata.netAmt).toFixed(2),
                      NetQty:parseFloat(this.ScanBarApidata.netQty).toFixed(2)
                    })
                    console.log('not_NaNQty',not_NaNQty);
                    console.log('not_NaNQty amt',existData[index].WSP* not_NaNQty);
      this.ScanBarApidata.pIndex=index
      this.ScanBarApidata.pQty=not_NaNQty
      // this.setState({NetAmount:parseFloat(NetValue).toFixed(2)})
      
    }

  }
  componentDidMount() {
    this.LoadInitialData();
  }
  SaveSales = async () => {
    Keyboard.dismiss();
    
    if(this.state.APIData.ScanBarcode.length>0){
      // console.log('this.state.APIData.ScanBarcode eeeeeeeeeerrrrrrr',this.state.APIData.ScanBarcode.length);
      const allowed = [
        'SeqNo',
        'InvoiceLotDetailID',
        'ItemID',
        'Barcode',
        'Description',
        'Rate',
        'TaxCode',
        'TaxAmount',
        'StdRate',
        'MRP',
        'Quantity',
        'FOCQty',
        'OrderQty',
        'Discount',
        'Amount',
        'GRPCODE',
        'OutputPer',
        'EffRate',
        'Charges',
        'Icode',
        'Oemcode',
        'WSP',
        'ListedMRP',
        'OnlineMRP',
        'Price',
        'UOMID',
        'UOMQty',
        'ItemDiscountType',
        'ItemDiscountFactor',
        'ItemDiscountValue',
        'CustomerDiscount',
        'CustDiscPercentage',
        'EmployeeID',
        'AgainstDetID',
        'Remarks',
      ];
      console.log('this.state.APIData.ScanBarcode eeeeeeeeee',this.state.APIData.ScanBarcode.length);
      // let ScanBarcode = (this.state.PrevList || []).concat(this.state.APIData.ScanBarcode);
      let ScanBarcode = (this.PrevItems || []).concat(this.state.APIData.ScanBarcode);
      let Arr = [];
      for (let j = 0, count = ScanBarcode.length; j < count; j++) {
        let Obj = {};
        for (let i = 0, len = allowed.length; i < len; i++) {
          Obj[allowed[i]] = ScanBarcode[j][allowed[i]];
        }
        Arr.push(Obj);
      }
  
      // console.log(this.state.APIData.ScanBarcode);
      //let { state } = this;
      let SaveObject = {
        InvoiceID: this.state.InvoiceID,
        InvoiceNumber: this.state.InvoiceNO,
        InvoiceType: this.state.InvoiceType,
        BasedOn: this.state.BasedOn,
        InvoiceDate: this.state.InvoiceDate || new Date().toLocaleDateString(),
        LOCCODE: this.state.LOCCODE,
        CustomerCode: this.state.Customer,
        DOCNO: this.state.DocNo,
        DOCDATE: this.state.DocDate,
        ValidFrom: this.state.ValidFrom,
        ValidTill: this.state.ValidTo,
        Stockpoint: this.state.StockPoint,
        TransportName: this.state.Transporter,
        BuyerName: this.state.BuyerName,
        AgentName: this.state.Agent,
        Remarks: this.state.Remarks,
        BasicAmount: this.state.BasicAmount,
        NetAmount: this.state.NetAmount,
        Round_Off: this.state.Round_Off,
        PaymentTerm: this.state.PaymentTerm,
        OrderMode: this.state.OrderMode,
        ReqLocationCode: this.state.ReqLocationCode,
        Agentcommission: this.state.Agentcommission,
        CurrencyID: this.state.Currency,
        AgainstID: this.state.Against,
        IsCashSales: this.state.IsCashSales,
        SalesAccountID: this.state.SalesAccountID,
        NewRefAmount:
          this.state.NewRefAmount == '' ? '0.00' : this.state.NewRefAmount,
        AgainstRefAmount: this.state.AgainstRefAmount,
        AgainstIDList: this.state.AgainstIDList,
        AgainstSignList: this.state.AgainstSignList,
        AgainstAdjusedAmountList: this.state.AgainstAdjusedAmountList,
        AgainstDiscountAmountList: this.state.AgainstDiscountAmountList,
        AgainstTDSAmountList: this.state.AgainstTDSAmountList,
        AgainstOthersAmountList: this.state.AgainstOthersAmountList,
        AgainstAmountList: this.state.AgainstAmountList,
        CreatedByOrModifyBy: this.state.CreatedByOrModifyBy,
        CustomField1: this.state.CustomField1,
        CustomField2: this.state.CustomField2,
        CustomField3: this.state.CustomField3,
        CustomField4: this.state.CustomField4,
        CustomField5: this.state.CustomField5,
        CFCombo1: this.state.CFCombo1,
        CFCombo2: this.state.CFCombo2,
        CFRadio1: this.state.CFRadio1,
        CFRadio2: this.state.CFRadio2,
        CFDate1: this.state.CFDate1,
        CFDate2: this.state.CFDate2,
        DtPassImages: null,
        IsSAB: true,
        SDetail: Arr,
        Scharges: this.state.APIData.Charges,
        SReceiveRatio: this.state.APIData.ReceivedRatio,
      };
      //console.log('save object : ', SaveObject);
  
      Request.post(
        `${this.IP}${this.APILink.API_SalesSave}`,
        SaveObject,
        this.Token,
      )
        .then(async res => {
          let json = await res.json();
          this.setState({ShowModalLoader: false});
          if (json != 'Failed') {
            AlertMessage(`${json} saved successfully`, this.NavigateBackToList);
            this.HandleClearData;
            return true;
          } else {
            AlertMessage('Something went wrong\nCould not save Courier.');
            return false;
          }
        })
        .catch(err => {
          AlertMessage('Could not save \n' + err);
          this.setState({ShowModalLoader: false});
        });
      this.setState({ShowModalLoader: true, ModalLoaderText: 'Saving Data...'});
    }
    else{
      AlertMessage("Atleast Add One Product")
    }
    
  };
  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };
  LoadInitialData = async () => {
    
    // this.IP = await AsyncStorage.getItem('IP');
    // this.Token = await AsyncStorage.getItem('Token');
    let [[, IP], [, Token], [, SizeCategory], [, StyleCategory]] =
      await AsyncStorage.multiGet([
        'IP',
        'Token',
        'SizeCategory',
        'StyleCategory',
      ]);
    SizeCategory = SizeCategory
      ? SizeCategory.replace('Category', 'Cat')
      : 'Cat3';
    StyleCategory = StyleCategory
      ? StyleCategory.replace('Category', 'Cat')
      : 'Cat5';
    console.log('Cat Sixe : ', SizeCategory, ' Style :', StyleCategory);
    this.IP = IP;
    this.Token = Token;
    this.setState(
      prevState => ({
        CategoryRatioInfo: {
          ...prevState.CategoryRatioInfo,
          SizeCategory,
          StyleCategory,
        },
      }),
      this.PrepareListForUnChangedData,
    );

    let params = this.props.route.params;
    console.log('params',params);
    this.props.navigation.setOptions({
      title: params.HeaderTitle || 'Sales Order',
      headerRight: () =>{
        console.log('Sales Orderrrrrrrrrrrr');
        return (
        <View style={{flexDirection: 'row', paddingRight: 10}}>
          {/* <TouchableOpacity
            style={{width: 30, height: 30,paddingRight : 5,}}
            onPress={this.HandleClearData}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/ClearTest.png')}
            />
          </TouchableOpacity> */}

          <TouchableOpacity
            style={{width: 30, height: 30}}
            onPress={this.SaveSales}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/save-light.png')}
            />
          </TouchableOpacity>
        </View>
      )}
    });
    // console.log(`${this.IP}${this.APILink.API_LoadInitialData}CallFrom=${params.Type}&LocCode=1`);

    Request.get(
      `${this.IP}${this.APILink.API_LoadInitialData}CallFrom=${params.Type}&LocCode=${this.LocalData.Loccode}`,
      this.Token,
    )
      .then(res => {
        // console.log(res);
        if (params.SId !== 0) {
          this.LoadSelectedInvoice(
            params.Type,
            params.SId,
            params.Loccode,
            res,
          );
        } else {
          // console.log(params.Type);
          this.setState(
            prevState => ({
              isFetched: true,
              InvoiceType: params.Type,
              APIData: {
                ...prevState.APIData,
                StockPoint: res.data.StockPoint,
                InvoiceType: res.data.InvoiceType,
                BasedOnList: res.data.BasedOn,
                ReqCompanyList: res.data.Locations,
                TransporterList: res.data.Transpoter,
                AgentList: res.data.Agent,
                MerchandiserList: res.data.Employee,
                OrderModeList: res.data.OrderMode,
                CurrencyList: res.data.Currency,
                PaymentTermList: res.data.PaymentTerm,
                SalesAccountList: res.data.SalesAccount,
                CategoryList: res.data.CategoryList,
              },
            }),
            this.PrepareListForUnChangedData,
          );
        }
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
    //  console.log(`${this.IP}${this.APILink.API_CustomerList}CallFrom=${this.LocalData.CallFrom}&BasedOn=0&InvoiceNo=&LocCode=${this.LocalData.Loccode}&EmpId=''`);
    Request.get(
      `${this.IP}${this.APILink.API_CustomerList}CallFrom=${params.Type}&BasedOn=0&InvoiceNo=&LocCode=${this.LocalData.Loccode}&EmpId=''`,
      this.Token,
    )
      .then(res => {
        //console.log(res.data);
        this.setState(
          prevState => ({
            isFetched: true,
            APIData: {...prevState.APIData, CustomerList: res.data},
          }),
          this.PrepareListForUnChangedData,
        );
      })
      .catch(err => AlertMessage(JSON.stringify(err)));

    // console.log('temp', this.state.BasicAmount)
  };
  // Accepts the array and key
  groupByKeyValue = (array, key) => {
    // Return the end result
    return array.reduce((result, currentValue) => {
      // If an array already present for key, push it to the array. Else create an array and push the object
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue,
      );
      // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
      return result;
    }, {}); // empty object is the initial value for result object
  };

  ResetTotalRatioInfo = () => {
    this.setState(prevState => ({
      TotalCategoryRatioInfo: {
        ...prevState.TotalCategoryRatioInfo,
        Value: 0,
        Size: '',
      },
    }));
  };

  HandleisShowCAtegoryRatio = isShow => {
    this._menu && this._menu.hide();
    let CategoryRatioCategory = [];
    let CategoryRatioSize = [];
    let uniqueSize = [];
    let isCategoryRatio = isShow;
    if (isShow) {
      isCategoryRatio = true;
      let helper = {};
      let CategoryRatio = this.state.APIData.ScanBarcode.reduce((r, o) => {
        let key =
          o[this.state.CategoryRatioInfo.StyleCategory] +
          '-' +
          o[this.state.CategoryRatioInfo.SizeCategory];
        if (!helper[key]) {
          helper[key] = Object.assign(
            {},
            {
              Category: o[this.state.CategoryRatioInfo.StyleCategory],
              Size: o[this.state.CategoryRatioInfo.SizeCategory],
              ActQuantity: o.Quantity,
              Quantity: o.Quantity,
              IcodeCount: 1,
              AllottedIcodeCount: 0,
              AllottedQuantity: 0,
            },
          ); // create a copy of o
          r.push(helper[key]);
        } else {
          helper[key].ActQuantity += o.Quantity;
          helper[key].Quantity += o.Quantity;
          helper[key].IcodeCount += 1;
        }

        return r;
      }, []);

      let helper_Size = {};
      CategoryRatioSize = CategoryRatio.reduce((r, o) => {
        let key = o.Size;
        if (!helper_Size[key]) {
          let NewCatPush = [];
          NewCatPush.push(o.Category);
          helper_Size[key] = Object.assign(
            {},
            {
              Size: o.Size,
              SizeQuantity: o.ActQuantity,
              SizeCount: 1,
              AllottedQuantity: 0,
              ApplyRatio: 0,
              CategoryList: NewCatPush,
            },
          );
          r.push(helper_Size[key]);
        } else {
          let NewCatPush = helper_Size[key].CategoryList;
          NewCatPush.push(o.Category);
          helper_Size[key].SizeQuantity += o.ActQuantity;
          helper_Size[key].SizeCount += 1;
          helper_Size[key].CategoryList = NewCatPush;
        }

        return r;
      }, []);

      uniqueSize = [
        ...new Set((CategoryRatioSize || []).map(item => item.Size)),
      ].sort();
      CategoryRatioCategory = this.groupByKeyValue(CategoryRatio, 'Category');
    }
    this.setState(prevState => ({
      CategoryRatioInfo: {
        ...prevState.CategoryRatioInfo,
        CategoryRatio: CategoryRatioCategory,
        CategoryRatioSize,
        isCategoryRatio,
        UniqueSizeList: uniqueSize,
      },
    }));
    console.log(
      'Han Cange',
      this.state.CategoryRatioInfo.CategoryRatio,
      this.state.CategoryRatioInfo.isCategoryRatio,
    );
  };

  HandleTabsClick = ActiveTab => {
  
    if(!this.state.Customer){
      AlertMessage("Select Customer")
    }
    else{
    this.setState({ActiveTab});
    this.refs.ViewPager.setPage(ActiveTab || 0);
    }
  };
  HighlightSelectedTab = event => {
    Keyboard.dismiss();
    this.setState({ActiveTab: event.nativeEvent.position});
  };
  LoadSelectedInvoice = (Type, Sid, Loccode, InitialData) => {
    // console.log(Type, Sid, Loccode, InitialData);
    // console.log(`${this.IP}${this.APILink.API_SelectedInvoiceDetails}CallFrom=${Type}&PassInvoiceID=${Sid}&LocCode=${Loccode}`);
    this.setState({isFetched: false});
    Request.get(
      `${this.IP}${this.APILink.API_SelectedInvoiceDetails}CallFrom=${Type}&PassInvoiceID=${Sid}&LocCode=${Loccode}`,
      this.Token,
    )
      .then(res => {
        // console.log(
        //   'SalesMaster From Add Sales LoadSelectedInvoice',
        //   res.data.SalesMaster,'scanbarcode',res.data
        // );
        if(res.data.SalesDetail.length){
          this.ScanBarApidata.EntIDLists =  this.state.APIData.ScanBarcode.reduce(
              (accum, item) => accum + item.ItemID + ',',
              0,
            )  ;
          this.ScanBarApidata.UOMIDLists =  res.data.SalesDetail.reduce(
              (accum, item) => accum + item.UOMID + ',',
              0,
            );
          
          this.ScanBarApidata.SeqnoLists =  res.data.SalesDetail.reduce(
              (accum, item) => accum + item.SeqNo + ',',
              0,
            )
          this.ScanBarApidata.MaxSeqNos = Math.max(
          ...res.data.SalesDetail.map(o => o.SeqNo),
          0,
        );
        this.ScanBarApidata.netAmt= res.data.SalesDetail.reduce(
          (accum, item) => accum + item.Amount,
          0,
        );
        this.ScanBarApidata.netQty= res.data.SalesDetail.reduce(
          (accum, item) => accum + item.Quantity,
          0,
        );

        }
        
        this.setState(
          prevState => ({
            InvoiceID: Sid,
            InvoiceType: Type,
            StatusFlag: res.data.SalesMaster[0].StatusFlag || 0,
            Status: res.data.SalesMaster[0].Status || '',
            InvoiceNO: res.data.SalesMaster[0].InvoiceNo,
            InvoiceDate: res.data.SalesMaster[0].InvoiceDate,
            Type: '1',
            LOCCODE: this.LocalData.Loccode,
            BasedOn:
              res.data.SalesMaster[0].BasedOn == null
                ? ''
                : res.data.SalesMaster[0].BasedOn,
            Against:
              res.data.SalesMaster[0].AgainstID == null
                ? ''
                : res.data.SalesMaster[0].AgainstID,
            Customer:
              res.data.SalesMaster[0].CustomerName == null
                ? ''
                : res.data.SalesMaster[0].CustomerName,
            DocNo:
              res.data.SalesMaster[0].DocNo == null
                ? ''
                : res.data.SalesMaster[0].DocNo,
            DocDate:
              res.data.SalesMaster[0].DocDate == null
                ? ''
                : res.data.SalesMaster[0].DocDate,
            GroupBarcode: 'Default',
            BuyerName:
              res.data.SalesMaster[0].BuyerName == null
                ? ''
                : res.data.SalesMaster[0].BuyerName,
            PaymentTerm:
              res.data.SalesMaster[0].PaymentTerm == null
                ? ''
                : res.data.SalesMaster[0].PaymentTerm,
            StockPoint:
              res.data.SalesMaster[0].Stockpoint == null
                ? ''
                : res.data.SalesMaster[0].Stockpoint,
            Barcode: '',
            Round_Off:
              res.data.SalesMaster[0]['Round Off'] == null
                ? ''
                : res.data.SalesMaster[0]['Round Off'],
            Remarks:
              res.data.SalesMaster[0].Remarks == null
                ? ''
                : res.data.SalesMaster[0].Remarks,
            ValidFrom:
              res.data.SalesMaster[0].ValidFrm == null
                ? ''
                : res.data.SalesMaster[0].ValidFrm,
            ValidTo:
              res.data.SalesMaster[0].ValidTill == null
                ? ''
                : res.data.SalesMaster[0].ValidTill,
            Transporter:
              res.data.SalesMaster[0].TransportName == null
                ? ''
                : res.data.SalesMaster[0].TransportName,
            Agent:
              res.data.SalesMaster[0].AgentName == null
                ? ''
                : res.data.SalesMaster[0].AgentName,
            Currency:
              res.data.SalesMaster[0].CurrencyID == null
                ? ''
                : res.data.SalesMaster[0].CurrencyID,
            OrderMode: '',
            IsCashSales:
              res.data.SalesMaster[0].IsCashSales == null
                ? ''
                : res.data.SalesMaster[0].IsCashSales,
            BasicAmount:
              res.data.SalesMaster[0].BasicAmount == null
                ? ''
                : res.data.SalesMaster[0].BasicAmount,
            // NetAmount:
            //   res.data.SalesMaster[0].NetAmount == null
            //     ? ''
            //     : res.data.SalesMaster[0].NetAmount,
            NetAmount:this.ScanBarApidata.netAmt,
            NetQty:this.ScanBarApidata.netQty,
            NewRefAmount: '0.00',
            SalesAccountID: '',
            ReqLocationCode: '',
            Agentcommission: '',
            AgainstRefAmount: '',
            AgainstIDList: '',
            AgainstSignList: '',
            AgainstAdjusedAmountList: '',
            AgainstDiscountAmountList: '',
            AgainstTDSAmountList: '',
            AgainstOthersAmountList: '',
            AgainstAmountList: '',
            CreatedByOrModifyBy: '',
            CustomField1: '',
            CustomField2: '',
            CustomField3: '',
            CustomField4: '',
            CustomField5: '',
            CFCombo1: '',
            CFCombo2: '',
            CFRadio1: true,
            CFRadio2: true,
            CFDate1: '',
            CFDate2: '',
            IsSAB:
              res.data.SalesMaster[0].IsSAB == null
                ? ''
                : res.data.SalesMaster[0].IsSAB,
            isFetched: true,

            APIData: {
              ...prevState.APIData,
              ReceivedRatio: res.data.SalesReceivedRatio || [],
              ScanBarcode: res.data.SalesDetail || [],
              StockPoint: InitialData.data.StockPoint,
              InvoiceType: InitialData.data.InvoiceType,
              BasedOnList: InitialData.data.BasedOn,
              ReqCompanyList: InitialData.data.Locations,
              TransporterList: InitialData.data.Transpoter,
              AgentList: InitialData.data.Agent,
              MerchandiserList: InitialData.data.Employee,
              OrderModeList: InitialData.data.OrderMode,
              CurrencyList: InitialData.data.Currency,
              PaymentTermList: InitialData.data.PaymentTerm,
              SalesAccountList: InitialData.data.SalesAccount,
              CategoryList: InitialData.data.CategoryList,
            },
          }),
          () => {
            this.PrepareListForUnChangedData();
          },
        );
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };
  HandleTypeChange = Type => {
    // get Against data for selected Type
    Request.get(
      `${this.IP}${this.APILink.API_AgainstList}CallFrom=${this.state.InvoiceType}&Customer=${this.state.Customer}&InvoiceNo=&InvoiceType=${Type}&BasedOn=0&LocCode=${this.LocalData.Loccode}`,
      this.Token,
    )
      .then(res => {
        //	console.log(res.data);
        this.PrepareListDataToRender(
          res.data,
          'Invoice No',
          'ID',
          'AgainstList',
        );
        this.setState(prevState => ({
          APIData: {...prevState.APIData, AgainstList: res.data || []},
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
    this.setState({Type});
  };
  CalculateTotalAmount = (Quantity, Price, Discount) => {
    return Quantity * (Price - Discount);
  };
  HandleAgainstInvoice = InvNo => {
    // console.log(`${this.IP}${this.APILink.API_SelectedAgainstInvoiceDetails}CallFrom=${this.state.InvoiceType}&CallFromID=${this.state.Type}&GetAgainstID=${InvNo}`);
    HandleDateChange = (name, date) => {
      this.setState({[name]: date});
    };
    Request.get(
      `${this.IP}${this.APILink.API_SelectedAgainstInvoiceDetails}CallFrom=${this.state.InvoiceType}&CallFromID=${this.state.Type}&GetAgainstID=${InvNo}`,
      this.Token,
    )
      .then(res => {
        // console.log(res.data);
        // this.setState((prevState) => ({ APIData: { ...prevState.APIData, CustomerList: res.data } }));
        this.setState(
          prevState => ({
            LOCCODE: this.LocalData.Loccode,
            BasedOn:
              res.data.SalesMaster[0].BasedOn == null
                ? ''
                : res.data.SalesMaster[0].BasedOn,
            Customer:
              res.data.SalesMaster[0].CustomerName == null
                ? ''
                : res.data.SalesMaster[0].CustomerName,
            DocNo:
              res.data.SalesMaster[0].DocNo == null
                ? ''
                : res.data.SalesMaster[0].DocNo,
            DocDate:
              res.data.SalesMaster[0].DocDate == null
                ? ''
                : res.data.SalesMaster[0].DocDate,
            GroupBarcode: 'Default',
            BuyerName:
              res.data.SalesMaster[0].BuyerName == null
                ? ''
                : res.data.SalesMaster[0].BuyerName,
            PaymentTerm:
              res.data.SalesMaster[0].PaymentTerm == null
                ? ''
                : res.data.SalesMaster[0].PaymentTerm,
            StockPoint:
              res.data.SalesMaster[0].Stockpoint == null
                ? ''
                : res.data.SalesMaster[0].Stockpoint,
            Barcode: '',
            Round_Off:
              res.data.SalesMaster[0]['Round Off'] == null
                ? ''
                : res.data.SalesMaster[0]['Round Off'],
            Remarks:
              res.data.SalesMaster[0].Remarks == null
                ? ''
                : res.data.SalesMaster[0].Remarks,
            ValidFrom:
              res.data.SalesMaster[0].ValidFrm == null
                ? ''
                : res.data.SalesMaster[0].ValidFrm,
            ValidTo:
              res.data.SalesMaster[0].ValidTill == null
                ? ''
                : res.data.SalesMaster[0].ValidTill,
            Transporter:
              res.data.SalesMaster[0].TransportName == null
                ? ''
                : res.data.SalesMaster[0].TransportName,
            Agent:
              res.data.SalesMaster[0].AgentName == null
                ? ''
                : res.data.SalesMaster[0].AgentName,
            Currency:
              res.data.SalesMaster[0].CurrencyID == null
                ? ''
                : res.data.SalesMaster[0].CurrencyID,
            OrderMode: '',
            IsCashSales:
              res.data.SalesMaster[0].IsCashSales == null
                ? ''
                : res.data.SalesMaster[0].IsCashSales,
            BasicAmount:
              res.data.SalesMaster[0].BasicAmount == null
                ? ''
                : res.data.SalesMaster[0].BasicAmount,
            NetAmount:
              res.data.SalesMaster[0].NetAmount == null
                ? ''
                : res.data.SalesMaster[0].NetAmount,
            NewRefAmount: '0.00',
            SalesAccountID: '',
            ReqLocationCode: '',
            Agentcommission: '',
            AgainstRefAmount: '',
            AgainstIDList: '',
            AgainstSignList: '',
            AgainstAdjusedAmountList: '',
            AgainstDiscountAmountList: '',
            AgainstTDSAmountList: '',
            AgainstOthersAmountList: '',
            AgainstAmountList: '',
            CreatedByOrModifyBy: '',
            CustomField1: '',
            CustomField2: '',
            CustomField3: '',
            CustomField4: '',
            CustomField5: '',
            CFCombo1: '',
            CFCombo2: '',
            CFRadio1: true,
            CFRadio2: true,
            CFDate1: '',
            CFDate2: '',
            IsSAB:
              res.data.SalesMaster[0].IsSAB == null
                ? ''
                : res.data.SalesMaster[0].IsSAB,
            isFetched: true,

            APIData: {
              ...prevState.APIData,
              ScanBarcode: res.data.SalesDetail || [],
            },
          }),
          () => {
            this.PrepareListForUnChangedData();
          },
        );
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
    this.setState({Against: InvNo});
  };
  ChangeStatus = changestatus => {
    this.setState({isFetched: false});
    console.log(
      `${this.IP}${this.APILink.API_StatusChange}SalesType=${this.state.InvoiceType}&PassID=${this.state.InvoiceID}^&PassType=${changestatus}`,
    );
    Request.post(
      `${this.IP}${this.APILink.API_StatusChange}SalesType=${this.state.InvoiceType}&PassID=${this.state.InvoiceID}^&PassType=${changestatus}`,
      null,
      this.Token,
    )
      .then(res => res.json())
      .then(json => {
        console.log('json data : ', json);
        if (json && json == 'Saved') {
          AlertMessage(`Status updated successfully`, this.NavigateBackToList);
        } else {
          AlertMessage('Failed to update status');
        }
        this.setState({isFetched: true});
      })
      .catch(err => {
        this.setState({isFetched: true});
      });
  };

  ChangeStatusDataList = () => {
    return this.state.InvoiceType == 'SO' || this.state.InvoiceType == 'SQ'
      ? this.state.StatusFlag == 1 // Open
        ? [
            {label: 'Approve', value: 'Approval'},
            {label: 'Cancel', value: 'Cancel'},
          ]
        : this.state.StatusFlag == 2 // Approved
        ? [
            {label: 'Hold', value: 'Hold'},
            {label: 'Cancel', value: 'Cancel'},
          ]
        : this.state.StatusFlag == 3 // Receiving
        ? [
            {label: 'Hold', value: 'Hold'},
            {label: 'Close', value: 'Close'},
          ]
        : this.state.StatusFlag == 7 // Hold
        ? [{label: 'Release', value: 'Release'}]
        : []
      : [];
  };

  HandleScanBarcode = () => {
   

    if (this.state.Barcode !== '') {
      if(this.state.APIData.ScanBarcode.length >40){
        let findex = this.state.APIData.ScanBarcode.length -15
        let previtemslist = this.state.APIData.ScanBarcode.filter((items,index)=> index<=findex)
        let currentitems = this.state.APIData.ScanBarcode.filter((items,index)=> index>findex)
     
        console.log('previtems',[...this.PrevItems,[...previtemslist]]);
        console.log('currentitems',[...this.PrevItems,currentitems]);
        
         
        
        this.PrevItems.push(...previtemslist)
        console.log('previtems',this.PrevItems);
        this.setState(prevState =>({
          // PrevList:PB,
          APIData:{...prevState.APIData,ScanBarcode:currentitems}}) )
        


      }
        if(this.state.APIData.ScanBarcode.length === 1){
          let lastIndex = this.state.APIData.ScanBarcode.length-1;
          this.ScanBarApidata.EntIDLists += this.state.APIData.ScanBarcode[lastIndex].ItemID;
          this.ScanBarApidata.UOMIDLists += this.state.APIData.ScanBarcode[lastIndex].UOMID;
          this.ScanBarApidata.SeqnoLists += this.state.APIData.ScanBarcode[lastIndex].SeqNo;
          this.ScanBarApidata.MaxSeqNos = this.ScanBarApidata.MaxSeqNos>this.state.APIData.ScanBarcode[lastIndex].UOMID ? this.ScanBarApidata.MaxSeqNos : this.state.APIData.ScanBarcode[lastIndex].SeqNo;
        }
        else if (this.state.APIData.ScanBarcode.length>1){
          let lastIndex = this.state.APIData.ScanBarcode.length-1;
          this.ScanBarApidata.EntIDLists += ','+this.state.APIData.ScanBarcode[lastIndex].ItemID;
          this.ScanBarApidata.UOMIDLists += ','+this.state.APIData.ScanBarcode[lastIndex].UOMID;
          this.ScanBarApidata.SeqnoLists += ','+this.state.APIData.ScanBarcode[lastIndex].SeqNo;
          this.ScanBarApidata.MaxSeqNos = this.ScanBarApidata.MaxSeqNos>this.state.APIData.ScanBarcode[lastIndex].SeqNo ? this.ScanBarApidata.MaxSeqNos : this.state.APIData.ScanBarcode[lastIndex].SeqNo;
        }
        
        

        Request.get(
          `${this.IP}${
            this.APILink.API_ItemBindDataForSearchItemInfo
          }CallFrom=${
            this.state.InvoiceType
          }&PassStockCode=1&GetuomIDList=${this.ScanBarApidata.UOMIDLists}&PassSearchItemInfo=${
            this.state.Barcode
          }&GetEntIDList=${this.ScanBarApidata.EntIDLists}&GetMaxSeqNo=${this.ScanBarApidata.MaxSeqNos}&Customer=${
            this.state.Customer
          }&Employee=&GetSeqnoList=${this.ScanBarApidata.SeqnoLists}&IsScanSetcode=${
            this.state.GroupBarcode === 'SetCode' ? true : false
          }&LocCode=${this.LocalData.Loccode}`,
          this.Token,
        )
          .then(res => {
           
            
            if (res.data.length > 0) {
              if (
                (res.data.length > 0 &&
                  this.state.GroupBarcode === 'SetCode' &&
                  !this.state.isSetcode) ||
                (res.data.length === 1 &&
                  this.state.GroupBarcode === 'Default' &&
                  !this.state.isSetcode)
              ) {
                
                let BIndex = this.state.APIData.ScanBarcode.findIndex(
                  obj =>
                    obj.SeqNo === res.data[0].SeqNo &&
                    obj.ItemID === res.data[0].ItemID &&
                    obj.UOMID === res.data[0].UOMID,
                );
                
                if (BIndex === -1) {
                 
                  let SB = (this.state.APIData.ScanBarcode || []).concat(
                    res.data,
                  );
                  
                  this.ScanBarApidata.netAmt +=SB[SB.length-1].Amount
                  this.ScanBarApidata.netQty +=SB[SB.length-1].Quantity
                  this.ScanBarApidata.pIndex =SB.length+(this.PrevItems.length?-2:-1);
                  this.ScanBarApidata.pQty =SB[SB.length-1].Quantity
                  console.log('sb',SB[SB.length-1].Amount);
                  this.setState(prevState => ({
                   
                    NetAmount:this.ScanBarApidata.netAmt ,
                    NetQty:this.ScanBarApidata.netQty,
                    BasicAmount: this.ScanBarApidata.netAmt
                    
                    ,
                    APIData: {...prevState.APIData, ScanBarcode: SB || []},
                  }));
                } else {
                
                  let ScanBarcode = [...this.state.APIData.ScanBarcode];
                  ScanBarcode[BIndex].Quantity =
                    parseInt(ScanBarcode[BIndex].Quantity || 0) +
                    parseInt(res.data[0].Quantity || 0);
                  ScanBarcode[BIndex].Amount += this.CalculateTotalAmount(
                    parseInt(res.data[0].Quantity),
                    res.data[0].Price,
                    res.data[0].Discount,
                  );
                  this.setState(prevState => ({
                    Barcode: '',
                    APIData: {...prevState.APIData, ScanBarcode},
                  }));
                }
              }
             
              else {
                var result = res.data.map(function (el) {
                  var o = Object.assign({}, el);
                  o.isActive = true;
                  return o;
                });
                this.setState(prevState => ({
                  isDupBarcode: true,
                  APIData: {...prevState.APIData, DupBarcodeList: result || []},
                }));
              }
            } else {
              AlertMessage('Invalid Barcode');
            }
          })
          .catch(err => AlertMessage(JSON.stringify(err),'its in api get'));
        this.setState({Barcode: ''});
        this.ScanBarApidata.c+=1
        console.log('scan count ', this.ScanBarApidata.c);
     // }
    }
  };
  HandleShowPrev =()=>{
    
    
    let ScanBarcode = (this.PrevItems || []).concat(
      this.state.APIData.ScanBarcode,   );
    this.setState(prevstate => ({
      
      APIData:{...prevstate.APIData,ScanBarcode},
    }))
      this.PrevItems=[]
     
      console.log('sss',this.state.APIData)

  }
  HandleCustomerChange = Customer => {
    
    Request.get(
      `${this.IP}${this.APILink.API_CustomerList}CallFrom=${this.state.InvoiceType}&BasedOn=0&InvoiceNo=&LocCode=${this.LocalData.Loccode}`,
      this.Token,
    )
      .then(res => {
       
        this.setState(prevState => ({
          APIData: {...prevState.APIData, CustomerList: res.data},
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));

    this.setState({Customer});
  };
  PrepareListDataToRender = (data, label, value, variableHolder) => {
    this[variableHolder] = data.map(item => ({
      label: item[label],
      value: item[value],
    }));
  };
  CategoryRatioCategoryQtyChange = size => {
    let totalratio = this.state.TotalCategoryRatioInfo.Value;
    console.log('data qty : ', totalratio);
    let statesize = this.state.TotalCategoryRatioInfo.Size;
    let CategoryRatioData = this.state.CategoryRatioInfo.CategoryRatio;
    let CategorySizeRatioData = this.state.CategoryRatioInfo.CategoryRatioSize;
    let TotalSizeQty = 0,
      TotalSizeCount = 0,
      AllottedTotalSizeCount = 0,
      TotalAllotedQty = 0,
      SizeCategoryList = [],
      isExists = false;
    if (size == statesize) {
      for (let i = 0; i < CategorySizeRatioData.length; i++) {
        if (CategorySizeRatioData[i].Size == size) {
          isExists = true;
          TotalSizeQty = CategorySizeRatioData[i].SizeQuantity;
          TotalSizeCount = CategorySizeRatioData[i].SizeCount;
          SizeCategoryList = CategorySizeRatioData[i].CategoryList;
          break;
        }
      }
      if (!isExists) {
        AlertMessage('data not match with item data');
        return;
      }
      let RAtioQtywor = TotalSizeQty * (totalratio / 100);
      let RAtioQty = RAtioQtywor < 1 ? 1 : Math.round(RAtioQtywor);
      let avgQty = RAtioQtywor / TotalSizeCount;
      avgQty = avgQty < 1 ? 1 : Math.round(avgQty);
      if (totalratio == 0) {
        RAtioQty = 0;
        avgQty = 0;
      }
      for (
        let sizecatLoop = 0;
        sizecatLoop < SizeCategoryList.length;
        sizecatLoop++
      ) {
        let category = SizeCategoryList[sizecatLoop];
        let TotalsizeLength = CategoryRatioData[category].length;
        for (var i = 0; i < TotalsizeLength; i++) {
        
          if (CategoryRatioData[category][i].Size == size) {
            let ApplyQty =
              RAtioQty <= TotalAllotedQty
                ? 0
                : AllottedTotalSizeCount + 1 == TotalSizeCount
                ? RAtioQty - TotalAllotedQty
                : RAtioQty - TotalAllotedQty < avgQty
                ? RAtioQty - TotalAllotedQty
                : avgQty;
            TotalAllotedQty += ApplyQty;
            AllottedTotalSizeCount += 1;
            CategoryRatioData[category][i].Quantity = ApplyQty;
            console.log(
              'Qty change C : ',
              category,
              ' S : ',
              size,
              ' Q : ',
              ApplyQty,
            );
            break;
          }
        }
      }

      
    }
    this.ResetTotalRatioInfo();
  };
  CategoryRatioQtyChange = (category, size, quantity) => {
  
    quantity = quantity || 0;
    let CategoryRatioData = this.state.CategoryRatioInfo.CategoryRatio;
    console.log('Category Ratio for  date : ', CategoryRatioData);
    let TotalsizeLength = CategoryRatioData[category].length;
    for (var i = 0; i < TotalsizeLength; i++) {
      console.log('Itr=earte ', TotalsizeLength, ' Of', i, 'TotalsizeLength');
      console.log('Current daa ', CategoryRatioData[category]);
      console.log('Current size ', CategoryRatioData[category][i].Size);
      if (CategoryRatioData[category][i].Size == size) {
        CategoryRatioData[category][i].Quantity = quantity;
        console.log(
          'Qty change C : ',
          category,
          ' S : ',
          size,
          ' Q : ',
          quantity,
        );
        break;
      }
    }
    this.setState(prevState => ({
      CategoryRatioInfo: {
        ...prevState.CategoryRatioInfo,
        CategoryRatio: CategoryRatioData || [],
      },
    }));
  };
  UpdateCodeValue = (Type, index, Value) => {
    console.log('UpdateCodeValue :', Type, ':', index, ':', Value);
    if (Type === 'Qty') {
      this.state.APIData.DupBarcodeList[index].Quantity = Value;
      this.state.APIData.DupBarcodeList[index].Amount =
        this.CalculateTotalAmount(
          this.state.APIData.DupBarcodeList[index].Quantity,
          this.state.APIData.DupBarcodeList[index].Price,
          this.state.APIData.DupBarcodeList[index].Discount,
        );
      this.forceUpdate();
    } else if (Type === 'isActive') {
      this.state.APIData.DupBarcodeList[index].isActive = Value;
      this.forceUpdate();
    }
  };
  UpdateQuantity = (index, qty) => {
    this.state.APIData.ScanBarcode[index].Quantity = qty;
    this.state.APIData.ScanBarcode[index].Amount = this.CalculateTotalAmount(
      this.state.APIData.ScanBarcode[index].Quantity,
      this.state.APIData.ScanBarcode[index].Price,
      this.state.APIData.ScanBarcode[index].Discount,
    );
    this.forceUpdate();
  };
  UpdatePrice = (index, Price) => {
   
    this.state.APIData.ScanBarcode[index].Price = Price;
    this.state.APIData.ScanBarcode[index].Amount = this.CalculateTotalAmount(
      this.state.APIData.ScanBarcode[index].Quantity,
      this.state.APIData.ScanBarcode[index].Price,
      this.state.APIData.ScanBarcode[index].Discount,
    );
    this.forceUpdate();
  };
  DeleteBarcode = index => {
    let SB = this.state.APIData.ScanBarcode[index]
    Alert.alert('iCube Alert', 'Are You Sure You Want To Delete', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: (S=SB) => {
          this.state.APIData.ScanBarcode.splice(index, 1);
          this.forceUpdate();
          console.log(S);
          this.ScanBarApidata.netAmt -= parseFloat(S.Amount)
          this.ScanBarApidata.netQty -= parseFloat(S.Quantity)
          this.setState({
            NetAmount:this.ScanBarApidata.netAmt,
            NetQty:this.ScanBarApidata.netQty
          })

        },
      },
    ]);
  };
  _menu = null;
  setMenuRef = ref => {
    this._menu = ref;
  };
  hideMenu = () => {
    this._menu.hide();
  };
  showMenu = () => {
    this._menu.show();
  };
  SelectedMenu = (menu, value) => {
    this._menu.hide();
    if (menu === 'RRatio') {
      this.CallRRatioVisible(!this.state.isRRatio);
    } else if (menu === 'Charges') {
    } else if (menu === 'Discount') {
      this.CallDiscount(value);
    } else if (menu === 'SalesMan') {
    }
  };

  InvoiceTypeList = [];
  BasedOnList = [];
  ReqCompanyList = [];
  TranspoterList = [];
  AgentList = [];
  MerchandiserList = [];
  StockPointList = [];
  OrderModeList = [];
  AgainstList = [];
  PaymentTermList = [];
  CurrencyList = [];
  CustomerList = [];
  SalesAccountList = [];
  CategoryList = [];

  PrepareListForUnChangedData = () => {
    let {APIData} = this.state;
   
    if (APIData.InvoiceType.length) {
     
      this.InvoiceTypeList = APIData.InvoiceType.map(item => ({
        label: item.TYPENAME,
        value: item.ID,
      }));
    }
    if (APIData.SalesAccountList.length) {
      // this.SalesAccountList = APIData.SalesAccountList.map((item, index) => <Picker.Item key={index} label={item.GLName} value={item.GLID} />);
      this.SalesAccountList = APIData.SalesAccountList.map(item => ({
        label: item.GLName,
        value: item.GLID,
      }));
    }
    if (APIData.CategoryList.length) {
      this.CategoryList = (APIData.CategoryList || []).map(item => ({
        label: item.DisplayName,
        value: item.Category,
      }));
    }
    if (APIData.PaymentTermList.length) {
      // this.PaymentTermList = APIData.PaymentTermList.map((item, index) => <Picker.Item key={index} label={item.NAME} value={item.ID} />);
      this.PaymentTermList = APIData.PaymentTermList.map(item => ({
        label: item.NAME,
        value: item.ID,
      }));
    }
    if (APIData.StockPoint.length) {
      // this.StockPointList = APIData.StockPoint.map((item, index) => <Picker.Item key={index} label={item.STKPTNAME} value={item.STKPTCODE} />);
      this.StockPointList = APIData.StockPoint.map(item => ({
        label: item.STKPTNAME,
        value: item.STKPTCODE,
      }));
    }
    // Preparing List items for BasedOn
    if (APIData.BasedOnList.length) {
      // this.BasedOnList = APIData.BasedOnList.map((item, index) => <Picker.Item key={index} label={item.TYPENAME} value={item.ID} />);
      this.BasedOnList = APIData.BasedOnList.map(item => ({
        label: item.TYPENAME,
        value: item.ID,
      }));
    }
    // Preparing List items for Transporter
    if (APIData.TransporterList.length) {
      // this.TranspoterList = APIData.TransporterList.map((item, index) => <Picker.Item key={index} label={item.PARTYNAME} value={item.PARTYCODE} />);
      this.TranspoterList = APIData.TransporterList.map(item => ({
        label: item.PARTYNAME,
        value: item.PARTYCODE,
      }));
    }
    // Prepare List items for Agent
    if (APIData.AgentList.length) {
      // this.AgentList = APIData.AgentList.map((item, index) => <Picker.Item key={index} label={item.PARTYNAME} value={item.PARTYCODE} />);
      this.AgentList = APIData.AgentList.map(item => ({
        label: item.PARTYNAME,
        value: item.PARTYCODE,
      }));
    }

    if (APIData.CurrencyList.length) {
      // this.CurrencyList = APIData.CurrencyList.map((item, index) => <Picker.Item key={index} label={item.CurrencyName} value={item.SetupId} />);
      this.CurrencyList = APIData.CurrencyList.map(item => ({
        label: item.CurrencyName,
        value: item.SetupId,
      }));
    }

    if (APIData.CustomerList.length) {
      // this.CustomerList = APIData.CustomerList.map((item, index) => <Picker.Item key={index} label={item.CUSTNAME} value={item.CUSTCODE} />);
      this.CustomerList = APIData.CustomerList.map(item => ({
        label: item.CUSTNAME,
        value: item.CUSTCODE,
      }));
    }
    // Force Update the view to render all the data
    this.forceUpdate();
  };
  HandleDupBar = Barcode => {
    // console.log('a');
    this.setState({Barcode: Barcode, isDupBarcode: false});
    this.HandleScanBarcode();
  };
  setRRatioVisible = RRatio => {
    // console.log(RRatio);
    // this.setState((prevState) => ({ APIData: { ...prevState.APIData, ReceivedRatio: RRatio || [] } }));
  };

  CallDiscount = isAlDis => {
    // console.log(`${this.IP}${this.APILink.API_GetDiscount}`);
    this.setState({isDiscount: true, isAllDisc: isAlDis});

    Request.get(`${this.IP}${this.APILink.API_GetDiscount}`, this.Token)
      .then(res => {
        // console.log(res);
        this.setState(prevState => ({
          APIData: {...prevState.APIData, Discount: res.data || []},
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };
  CallRRatioVisible() {
    var Quantity = this.state.APIData.ScanBarcode.reduce(
      (accum, item) => accum + item.Quantity,
      0,
    );
    // console.log(Quantity);
    if (this.state.DocDate === '') {
      AlertMessage('Enter Doc Date');
      try {
        this.refs.DocDate.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else if (this.state.ValidFrom === '') {
      AlertMessage('Enter Valid From');
      try {
        this.refs.ValidFrom.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else if (this.state.ValidTo === '') {
      AlertMessage('Enter Valid To');
      try {
        this.refs.ValidTo.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else {
      this.props.navigation.navigate('ReceivedRatioNavigator', {
        ValidFrom: this.state.ValidFrom,
        ValidTo: this.state.ValidTo,
        TQty: Quantity.toFixed(2),
        RRatio: this.state.APIData.ReceivedRatio,
        onFinishCreation: this.setRRatioVisible,
      });
    }

    // var array = this.state.APIData.ScanBarcode;
  }
  isShowModal(Module, visible) {
    this._menu && this._menu.hide();
    if (Module === 'Discount') {
      this.setState({isDiscount: visible});
    } else if (Module === 'DupBarcode') {
      this.setState({isDupBarcode: visible});
    } else if (Module === 'Scanner') {
      this.setState({openScanner: visible});
    } else if (Module === 'CategoryConfig') {
      this.setState({isCategoryConfig: visible});
    } else if (Module === 'Setcode') {
      // this.setState({ isSetcode: visible });
      this.setState(prevState => ({
        Barcode: '',
        isSetcode: visible,
        APIData: {...prevState.APIData, SetcodeList: []},
      }));
    }
  }
  setChargesVisible(visible) {
    this.setState({isCharges: visible});
  }
  ReturnCustomerAndItemDiscount(
    Price,
    CustDiscPercentage,
    GetDiscType,
    GetDiscFactor,
    GetItemDiscValue,
    GetCustDiscValue,
  ) {
    if (/*_IsCustomerDiscountAfterItemDiscount*/ false) {
      GetItemDiscValue =
        GetDiscType == 'Percentage'
          ? Price * (GetDiscFactor / 100)
          : GetDiscFactor;
      GetCustDiscValue =
        (Price - GetItemDiscValue) * (CustDiscPercentage / 100);
    } else {
      GetCustDiscValue = Price * (CustDiscPercentage / 100);
      GetItemDiscValue =
        GetDiscType == 'Percentage'
          ? (Price - GetCustDiscValue) * (GetDiscFactor / 100)
          : GetDiscFactor;
    }
  }
  GetSelectedDiscountDetails(/*discType, discfactor,*/ FocusItemIndex) {

    this.ReturnCustomerAndItemDiscount(
      this.state.APIData.ScanBarcode[FocusItemIndex].Price,
    );
  }
  HandleSelectedDiscount() {
    if (this.state.isAllDisc) {
      let SCCount = this.state.APIData.ScanBarcode.length;
      if (SCCount > 0) {
        for (let RowCount = 0; RowCount < SCCount; RowCount++) {
          // console.log(SCCount);
          this.GetSelectedDiscountDetails(RowCount);
        }
      }
    }
  }

  async onBarcodeScan  (qrvalue) {
    
    await this.setState( {Barcode: qrvalue, openScanner: false,openScanner: false}, );
    // this.isShowModal('Scanner', !this.state.openScanner);
    console.log(this.state.Barcode,this.state.openScanner);
    
    this.HandleScanBarcode();
  }

  BindSetcode = () => {
    this.setState({isDupBarcode: false});
    let SetcodeList = this.state.APIData.DupBarcodeList;
    var IsDatas = SetcodeList.filter(function (e) {
      return e.isActive === true;
    });
    // console.log('ssdsd', IsDatas.length)

    let ScanBar = [...this.state.APIData.ScanBarcode];

    for (let i = 0; i < IsDatas.length; i++) {
      let BIndex = this.state.APIData.ScanBarcode.findIndex(
        obj =>
          obj.SeqNo === IsDatas[i].SeqNo &&
          obj.ItemID === IsDatas[i].ItemID &&
          obj.UOMID === IsDatas[i].UOMID,
      );
      console.log(
        'BindSetcode For each :',
        IsDatas[i].SeqNo,
        IsDatas[i].ItemID,
        IsDatas[i].UOMID,
        BIndex,
      );
      //  BIndex = a.findIndex(x => x.prop2 ===res.data[0].Barcode.toUpperCase());
      if (BIndex === -1) {
        // this.ExistingBarcode.push(res.data[0].Barcode.toUpperCase());

        ScanBar = (ScanBar || []).concat(IsDatas[i]);
        // console.log(ScanBarcode);
        // this.setState((prevState) => ({ NetAmount: SB.reduce((accum, item) => accum + item.Amount, 0), BasicAmount: SB.reduce((accum, item) => accum + item.Amount, 0), APIData: { ...prevState.APIData, ScanBarcode: SB || [] } }));
      } else {
        let ScanBarcode = [...this.state.APIData.ScanBarcode];
        // console.log(typeof (IsDatas[i].Quantity));
        ScanBarcode[BIndex].Quantity =
          parseInt(ScanBarcode[BIndex].Quantity || 0) +
          parseInt(IsDatas[i].Quantity || 0);
        ScanBarcode[BIndex].Amount += this.CalculateTotalAmount(
          IsDatas[i].Quantity,
          IsDatas[i].Price,
          IsDatas[i].Discount,
        );
        this.setState(prevState => ({
          Barcode: '',
          APIData: {...prevState.APIData, ScanBarcode, SetcodeList: []},
        }));
      }
    }
    // console.log("Updated ScanBarcodes", ScanBar);
    this.setState(prevState => ({
      NetAmount: ScanBar.reduce((accum, item) => accum + item.Amount, 0),
      BasicAmount: ScanBar.reduce((accum, item) => accum + item.Amount, 0),
      APIData: {...prevState.APIData, ScanBarcode: ScanBar || []},
    }));

    // let SB = (this.state.APIData.ScanBarcode || []).concat(IsDatas);
    // this.setState((prevState) => ({  APIData: { ...prevState.APIData, ScanBarcode: SB || [],SetcodeList:[] } }));
  };
  HandleDiscount = (discount, Type, isChange) => {
    // console.log('aa', discount, Type, isChange, index);
    this.setState({
      MDiscount: discount,
      isChangeDisc: isChange.toLowerCase() === 'true',
    });
  };

  onOpenScanner() {
  
      var that = this;
      //To Start Scanning
      if (Platform.OS === 'android') {
        async function requestCameraPermission() {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'iCube App Camera Permission',
                message: 'iCube App needs access to your camera ',
              },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //If CAMERA Permission is granted
              that.setState({qrvalue: '', openScanner: true});
              // that.setState({ openScanner: true });
            } else {
              alert('CAMERA permission denied');
            }
          } catch (err) {
            alert('Camera permission err', err);
            console.warn(err);
          }
        }
        //Calling the camera permission function
        requestCameraPermission();
      } else {
        that.setState({qrvalue: ''});
        that.setState({openScanner: true});
      }
    // }
  }

  IsAgainst = () => {
    let IsAgainst = false;
    if (
      ((this.LocalData.CallFrom == 'SO' ||
        this.LocalData.CallFrom == 'DC' ||
        this.LocalData.CallFrom == 'SR') &&
        this.state.Type == 2) ||
      ((this.LocalData.CallFrom == 'SI' || this.LocalData.CallFrom == 'CN') &&
        (this.state.Type == 2 || this.state.Type == 3 || this.state.Type == 4))
    ) {
      IsAgainst = true;
    }
    return IsAgainst;
  };

  FieldType = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <ModalSearchablePicker
        placeholder="Type"
        fieldWrapperStyle={{width: this.IsAgainst() ? '48%' : '100%'}}
        data={this.InvoiceTypeList}
        labelProp="label"
        valueProp="value"
        selectedValue={this.state.Type}
        onValueSelected={this.HandleTypeChange}
      />
      {this.IsAgainst() && (
        <ModalSearchablePicker
          placeholder="Against"
          // key={key}
          data={this.AgainstList}
          labelProp="label"
          valueProp="value"
          fieldWrapperStyle={{width: '48%'}}
          selectedValue={this.state.Against}
          onValueSelected={this.HandleAgainstInvoice}
        />
      )}
    </View>
  );

  FieldBasedon = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Based On"
      data={this.BasedOnList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.BasedOn}
      onValueSelected={BasedOn => this.setState({BasedOn})}
    />
  );

  FieldGST = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Gst"
      data={this.SalesAccountList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.SalesAccountID}
      onValueSelected={SalesAccountID => this.setState({SalesAccountID})}
    />
  );
  FieldCustomer = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Customer"
      data={this.CustomerList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Customer}
      onValueSelected={this.HandleCustomerChange}
    />
  );

  FieldDocInfo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <RichTextInput
        placeholder="Doc No"
        wrapperStyle={{width: '48%'}}
        value={this.state.DocNo}
        onChangeText={DocNo => this.setState({DocNo})}
        inputProps={{
          onSubmitEditing: () => {
            Keyboard.dismiss();
          },
        }}
      />
      <ModalDatePicker
        placeholder={'Doc Date'}
          fieldWrapperStyle={{width: '48%'}}
        selectedValue={this.state.DocDate}
        onValueSelected={(selectedDate) => this.HandleDateChange('DocDate',selectedDate ) }
      />
    </View>
  );

  FieldValidFromAndTo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      
      <ModalDatePicker
        placeholder={'Valid From'}
          fieldWrapperStyle={{width: '48%'}}
        selectedValue={this.state.ValidFrom}
        onValueSelected={(selectedDate) => this.HandleDateChange('ValidFrom',selectedDate ) }
      />
      
      <ModalDatePicker
        placeholder={'Valid To'}
          fieldWrapperStyle={{width: '48%'}}
        selectedValue={this.state.ValidTo}
        onValueSelected={(selectedDate) => this.HandleDateChange('ValidTo',selectedDate ) }
      />
    </View>
  );
  FieldPaymenttype = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Payment Term"
      data={this.PaymentTermList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.PaymentTerm}
      onValueSelected={PaymentTerm => this.setState({PaymentTerm})}
    />
  );
  FieldStockpoint = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Stock Point"
      data={this.StockPointList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.StockPoint}
      onValueSelected={StockPoint => this.setState({StockPoint})}
    />
  );
  FieldRemarks = key => (
    <RichTextInput
      key={key}
      inputStyle={{height: null, maxHeight: 100, lineHeight: 20}}
      placeholder="Remarks"
      value={this.state.Remarks}
      onChangeText={Remarks => this.setState({Remarks})}
      inputProps={{multiline: true, numberOfLines: 2}}
    />
  );
  FieldTransport = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Transporter"
      data={this.TranspoterList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Transporter}
      onValueSelected={Transporter => this.setState({Transporter})}
    />
  );
  FieldAgent = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Agent"
      data={this.AgentList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Agent}
      onValueSelected={Agent => this.setState({Agent})}
    />
  );
  FieldCurrency = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Currency"
      data={this.CurrencyList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Currency}
      onValueSelected={Currency => this.setState({Currency})}
    />
  );
  FieldOrdermode = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Order Mode"
      data={this.OrderModeList || []}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.OrderMode}
      onValueSelected={OrderMode => this.setState({OrderMode})}
    />
  );

  refreshLayout = () => this.forceUpdate();

  render() {
    let IsChangeStatus = this.ChangeStatusDataList().length > 0;
   
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }

    // if (this.state.openScanner) {
    //   return (
    //     <View style={styles.modalContainer}>
    //       <CameraScreen
    //         actions={{leftButtonText: 'Cancel'}}
    //         onBottomButtonPressed={() => {
    //           this.isShowModal('Scanner', !this.state.openScanner);
    //         }}
    //         showFrame={true}
    //         // style={{height: '100%'}}
    //         scanBarcode={true}
    //         // laserColor={'white'}
    //         // frameColor={'lightgreen'}
    //         colorForScannerFrame={'black'}
            
    //         onReadCode={event =>
    //           this.onBarcodeScan(event.nativeEvent.codeStringValue)
    //         }
    //       />
    //     </View>
    //   );
    // }
    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.oppPrimary}
          onLayoutChanged={this.refreshLayout}
          >
          <View style={styles.Tabs}>
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 0)}
              style={[
                styles.Tab,
                // {width: (IsChangeStatus ? '31%' : '50%'),},
              ]}>
              <Text
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxxl,
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Invoice
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 1)}
              style={[
                styles.Tab,
                // {width: (IsChangeStatus ? '31%' : '50%'),}
              ]}>
              <Text
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxxl,
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Products
              </Text>
            </TouchableOpacity>
          </View>
        
          {this.state.openScanner &&
          <View style={styles.modalContainer}>
          <CameraScreen
            actions={{leftButtonText: 'Cancel'}}
            onBottomButtonPressed={() => {
              this.isShowModal('Scanner', !this.state.openScanner);
            }}
            showFrame={true}
            // style={{height: '100%'}}
            scanBarcode={true}
            // laserColor={'white'}
            // frameColor={'lightgreen'}
            colorForScannerFrame={'black'}
            
            onReadCode={event =>
              this.onBarcodeScan(event.nativeEvent.codeStringValue)
            }
          />
        </View>
  }
         
         
         
         
         
         
          <ViewPager
            style={styles.ViewPager}
            initialPage={0}
            onPageSelected={this.HighlightSelectedTab}
            ref={'ViewPager'}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.isDupBarcode}
              onRequestClose={() => {
                Alert.alert('Screen has been closed.');
              }}>
              <View style={{justifyContent: 'flex-end'}}>
                <FlatList
                  style={
                    (styles.FlatList,
                    {
                      marginTop: 5,
                      backgroundColor: globalColorObject.Color.Lightprimary,
                    })
                  }
                  data={this.state.APIData.DupBarcodeList}
                  ListHeaderComponent={<DBDupListItemHeader />}
                  renderItem={({item, index}) => (
                    <DBDupListItem
                      data={item}
                      index={index}
                      onLongPress={this.HandleDupBar}
                      onQtyChange={this.UpdateCodeValue}
                      onActiveChange={this.UpdateCodeValue}
                    />
                  )}
                  keyExtractor={item => item.Icode.toString()}
                />
                <View
                  style={{
                    marginRight: 10,
                    marginTop: 10,
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    flexDirection: 'row',
                    marginBottom: 5,
                  }}>
                  <TouchableOpacity
                    style={{
                      flex: 2,
                      backgroundColor: globalColorObject.Color.Primary,
                      marginHorizontal: 9,
                      marginBottom: 5,
                      textAlign: 'center',
                      justifyContent: 'center',
                      borderRadius: 5,
                      paddingVertical: 5,
                      paddingHorizontal: 5,
                      paddingBottom: 15,
                      top: 2.5,
                      elevation: 9,
                      fontSize: 18,
                    }}
                    onPress={this.BindSetcode}>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        justifyContent: 'center',
                        fontSize: 18,
                      }}>
                      OK
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      flex: 2,
                      backgroundColor: globalColorObject.Color.Primary,
                      marginHorizontal: 9,
                      marginBottom: 5,
                      textAlign: 'center',
                      justifyContent: 'center',
                      borderRadius: 5,
                      paddingVertical: 5,
                      paddingHorizontal: 5,
                      paddingBottom: 15,
                      elevation: 9,
                      top: 2.5,
                      fontSize: 18,
                    }}
                    onPress={() => {
                      this.isShowModal('DupBarcode', !this.state.isDupBarcode);
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        justifyContent: 'center',
                        fontSize: 18,
                      }}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.CategoryRatioInfo.isCategoryRatio}
              onRequestClose={() => {
                Alert.alert('Screen has been closed.');
              }}>
              <View
                style={{
                  flex: 1,
                  margin: 5,
                  backgroundColor: globalColorObject.Color.Lightprimary,
                }}>
                <FlatList
                  data={Object.keys(this.state.CategoryRatioInfo.CategoryRatio)
                    .map(datalist => datalist)
                    .sort()}
                  renderItem={({item, index}) => {
                    var DynamicSizeData = [];
                    this.state.CategoryRatioInfo.UniqueSizeList.map(
                      (myheadsize, uindex) => {
                        let TotalsizeLength =
                          this.state.CategoryRatioInfo.CategoryRatio[item]
                            .length;
                        let IsSizeElementExists = false,
                          SizeElementQuantity = '';
                        for (let i = 0; i < TotalsizeLength; i++) {
                          let element =
                            this.state.CategoryRatioInfo.CategoryRatio[item][i];
                          if (element.Size == myheadsize) {
                            IsSizeElementExists = true;
                            SizeElementQuantity = element.Quantity;
                            break;
                          }
                        }
                        if (IsSizeElementExists) {
                          DynamicSizeData.push(
                            <View
                              style={[styles.CardTextContainer, {width: 80}]}>
                              <TextInput
                                key={`cg_${index}_${uindex}`}
                                style={[
                                  styles.cardTextFieldHeader,
                                  {color: 'black', textAlign: 'center'},
                                ]}
                                keyboardType="numeric"
                                onChangeText={changeQty => {
                                  this.CategoryRatioQtyChange(
                                    item,
                                    myheadsize,
                                    changeQty,
                                  );
                                }}
                                value={(SizeElementQuantity || 0).toString()}
                              />
                            </View>,
                          );
                        } else {
                          DynamicSizeData.push(
                            <View
                              style={[styles.CardTextContainer, {width: 80}]}>
                              <Text
                                style={[
                                  styles.cardTextFieldHeader,
                                  {textAlign: 'center', color: 'black'},
                                ]}
                              />
                            </View>,
                          );
                        }
                      },
                    );
                    return (
                      <View style={[styles.Card]}>
                        <View style={[styles.CardTextContainer, {width: 120}]}>
                          <Text
                            style={[
                              styles.cardTextFieldHeader,
                              {textAlign: 'left', color: 'black'},
                            ]}>
                            {item}
                          </Text>
                        </View>
                        {DynamicSizeData}
                      </View>
                    );
                  }}
                  keyExtractor={item => item.toString()}
                  ListHeaderComponent={
                    <>
                      <View style={styles.StickeyHeaderCard}>
                        <View style={[styles.CardTextContainer, {width: 120}]}>
                          <Text
                            style={[
                              styles.cardTextFieldHeader,
                              {textAlign: 'left'},
                            ]}>
                            Category
                          </Text>
                        </View>
                        {this.state.CategoryRatioInfo.UniqueSizeList.map(
                          myheadsize => {
                            return (
                              <View
                                style={[styles.CardTextContainer, {width: 80}]}>
                                <Text
                                  numberOfLines={1}
                                  style={[
                                    styles.cardTextFieldHeader,
                                    {textAlign: 'center'},
                                  ]}>
                                  {myheadsize || 'N/A'}
                                </Text>
                              </View>
                            );
                          },
                        )}
                      </View>
                      <View style={styles.StickeyHeaderCard}>
                        <View style={[styles.CardTextContainer, {width: 120}]}>
                          <Text
                            style={[
                              styles.cardTextFieldHeader,
                              {textAlign: 'left'},
                            ]}>
                            Ratio (%)
                          </Text>
                        </View>
                        {this.state.CategoryRatioInfo.UniqueSizeList.map(
                          (myheadsize, index) => {
                            let GetValue =
                              this.state.TotalCategoryRatioInfo.Value &&
                              this.state.TotalCategoryRatioInfo.Value != 0 &&
                              this.state.TotalCategoryRatioInfo.Size ==
                                myheadsize
                                ? this.state.TotalCategoryRatioInfo.Value.toString()
                                : '';
                            return (
                              <View
                                style={[styles.CardTextContainer, {width: 80}]}>
                                <TextInput
                                  key={`hcg_${index}`}
                                  onFocus={this.ResetTotalRatioInfo}
                                  style={[
                                    styles.cardTextFieldHeader,
                                    {
                                      textAlign: 'center',
                                      color: 'black',
                                      backgroundColor: 'white',
                                      marginLeft: 5,
                                      marginRight: 5,
                                    },
                                  ]}
                                  keyboardType="numeric"
                                  onSubmitEditing={() => {
                                    this.CategoryRatioCategoryQtyChange(
                                      myheadsize,
                                    );
                                  }}
                                  onChangeText={changeQty => {
                                    this.setState(prevState => ({
                                      TotalCategoryRatioInfo: {
                                        ...prevState.TotalCategoryRatioInfo,
                                        Value: changeQty || 0,
                                        Size: myheadsize,
                                      },
                                    }));
                                  }}
                                  value={(GetValue || '').toString()}
                                />
                              </View>
                            );
                          },
                        )}
                      </View>
                    </>
                  }
                  stickyHeaderIndices={[0]}
                  // to have some breathing space on bottom
                  ListFooterComponent={
                    <View style={{width: '100%', marginTop: 15}} />
                  }
                />
                <View
                  style={[
                    styles.CardTextContainer,
                    {
                      width: '100%',
                      // marginLeft: 5,
                      // marginRight: 5,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: globalColorObject.Color.Primary,
                    },
                  ]}>
                  <TouchableOpacity
                    style={{
                      width: '50%',
                      height: 35,
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      alignItems: 'center',
                      borderRadius: 10,
                      elevation: 5,
                      backgroundColor: globalColorObject.Color.Primary,
                    }}
                    onPress={this.HandleisShowCAtegoryRatio.bind(this, false)}>
                    <Text
                      numberOfLines={1}
                      style={[
                        styles.cardTextFieldHeader,
                        {textAlign: 'center'},
                      ]}>
                      Close
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      paddingLeft: 10,
                      width: '48%',
                      height: 35,
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      alignItems: 'center',
                      borderRadius: 10,
                      elevation: 5,
                      backgroundColor: globalColorObject.Color.Primary,
                    }}
                    onPress={this.CategoryRatioApply.bind(this)}>
                    <Text
                      numberOfLines={1}
                      style={[
                        styles.cardTextFieldHeader,
                        {textAlign: 'center'},
                      ]}>
                      Apply
                    </Text>
                  </TouchableOpacity>
                </View>
                {/*
      <View
      style={[
      styles.CardTextContainer,
      {
      width: '100%',
      backgroundColor: '#393D41',
      },
      ]}>
      <TouchableOpacity onPress={this.CategoryRatioApply.bind(this)}>
      <Text
      numberOfLines={1}
      style={[
      styles.cardTextFieldHeader,
      {textAlign: 'center', height: 30},
      ]}>
      Apply
      </Text>
      </TouchableOpacity>
      </View> */}
              </View>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.isDiscount}
              onRequestClose={() => {
                Alert.alert('Screen has been closed.');
              }}>
              <ScrollView style={styles.colo}>
                <View
                  style={{
                    marginTop: 22,
                    height: '100%',
                    backgroundColor: globalColorObject.Color.Lightprimary,
                  }}>
                  <View>
                    <View
                      style={[
                        styles.StickeyHeaderCard,
                        {backgroundColor: globalColorObject.Color.Lightprimary},
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <View
                        style={[
                          styles.CardTextContainer,
                          {paddingLeft: '1%', width: '39%'},
                        ]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          {' '}
                          Name
                        </Text>
                      </View>
                      <View style={[styles.CardTextContainer, {width: '34%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          Factor
                        </Text>
                      </View>
                      <View style={[styles.CardTextContainer, {width: '11%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          {' '}
                          Type
                        </Text>
                      </View>
                    </View>
                    <View>
                      <FlatList
                        style={styles.FlatList}
                        data={this.state.APIData.Discount}
                        renderItem={({item, index}) => (
                          <DiscountListItem
                            data={item}
                            index={index}
                            onPress={this.HandleDiscount}
                          />
                        )}
                        // keyExtractor={item => item.Discount}
                      />
                    </View>
                  </View>
                </View>
              </ScrollView>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  backgroundColor: globalColorObject.Color.Primary,
                }}>
                <View style={{width: '50%', marginLeft: 5, height: 70}}>
                  <Text
                    style={{
                      fontSize: 16,
                      marginBottom: 3,
                      marginLeft: 5,
                      color: 'white',
                    }}>
                    Discount Factor
                  </Text>
                  <TextInput
                    style={{
                      height: 40,
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}
                    value={this.state.MDiscount.toString()}
                    onChangeText={MDiscount => this.setState({MDiscount})}
                    keyboardType="numeric"
                    editable={this.state.isChangeDisc}
                    returnKeyType="next"
                    autoCorrect={false}
                    autoCapitalize="none"
                    ref={'MDiscount'}
                  />
                </View>
                <View
                  style={{
                    width: '50%',
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      padding: 5,
                      backgroundColor: '#1253bc',
                      width: '40%',
                      height: 35,
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      alignItems: 'center',
                      borderRadius: 10,
                      elevation: 5,
                    }}
                    onPress={() => {
                      this.HandleSelectedDiscount();
                    }}>
                    <Image
                      source={require('../../../assets/images/Tick.png')}
                    />
                    <Text style={{fontSize: 12, color: 'white'}}>OK</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      padding: 5,
                      backgroundColor: 'rgb(237, 36, 36)',
                      width: '40%',
                      height: 35,
                      flexDirection: 'row',
                      justifyContent: 'space-evenly',
                      alignItems: 'center',
                      borderRadius: 10,
                      elevation: 5,
                    }}
                    onPress={() => {
                      this.isShowModal('Discount', !this.state.isDiscount);
                    }}>
                    <Image
                      source={require('../../../assets/images/BClear.png')}
                    />
                    <Text style={{fontSize: 12, color: 'white'}}>Clear</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.isCategoryConfig}
              onRequestClose={() => {
                Alert.alert('Screen has been closed.');
              }}>
              <View style={styles.colo}>
                <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
                  <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                    <ScrollView
                      style={{
                        width: '100%',
                        backgroundColor: globalColorObject.Color.oppPrimary,
                      }}>
                      <ModalSearchablePicker
                        key={'cc_f_style'}
                        placeholder="Style Category"
                        data={this.CategoryList || []}
                        labelProp="label"
                        valueProp="value"
                        selectedValue={this.state.CategoryRatioInfo.StyleCategory.replace(
                          'Cat',
                          'Category',
                        )}
                        onValueSelected={changeStyleCategory => {
                          this.setState(prevState => ({
                            CategoryRatioInfo: {
                              ...prevState.CategoryRatioInfo,
                              StyleCategory: changeStyleCategory.replace(
                                'Category',
                                'Cat',
                              ),
                            },
                          }));
                          console.log('Cahnge style : ', changeStyleCategory);
                          AsyncStorage.setItem(
                            'StyleCategory',
                            changeStyleCategory,
                          );
                        }}
                      />
                      <ModalSearchablePicker
                        key={'cc_f_size'}
                        ref={'size'}
                        placeholder="Size Category"
                        data={this.CategoryList}
                        labelProp="label"
                        valueProp="value"
                        selectedValue={this.state.CategoryRatioInfo.SizeCategory.replace(
                          'Cat',
                          'Category',
                        )}
                        onValueSelected={changesizeCategory => {
                          this.setState(prevState => ({
                            CategoryRatioInfo: {
                              ...prevState.CategoryRatioInfo,
                              SizeCategory: changesizeCategory.replace(
                                'Category',
                                'Cat',
                              ),
                            },
                          }));
                          console.log('Cahnge size : ', changesizeCategory);
                          AsyncStorage.setItem(
                            'SizeCategory',
                            changesizeCategory,
                          );
                        }}
                      />
                    </ScrollView>
                  </View>

                  <View
                    style={[
                      styles.CardTextContainer,
                      {
                        width: '100%',
                        // marginLeft: 5,
                        // marginRight: 5,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: globalColorObject.Color.Primary,
                      },
                    ]}>
                    <TouchableOpacity
                      style={{
                        width: '100%',
                        height: 35,
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                        borderRadius: 10,
                        elevation: 5,
                        backgroundColor: globalColorObject.Color.Primary,
                      }}
                      onPress={this.isShowModal.bind(
                        this,
                        'CategoryConfig',
                        false,
                      )}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.cardTextFieldHeader,
                          {textAlign: 'center'},
                        ]}>
                        Close
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            <View style={styles.InvoiceTabWrapper}>
              <ScrollView
                keyboardShouldPersistTaps="always"
                nestedScrollEnabled={true}
                // style={{ViewPager}}>
                style={styles.ViewPager}>
                {/* style={styles.PageContainer} */}
                <View style={[layoutStyle.ScrollContentWrapper]}>
                  {(this.state.InvoiceType === 'SI' ||
                    this.state.InvoiceType === 'CN') && (
                    <View style={styles.FieldContainer}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginTop: 15,
                          marginBottom: 15,
                          width: '100%',
                        }}>
                        <Text
                          numberOfLines={1}
                          style={{
                            marginLeft: 10,
                            marginRight: 15,
                            fontSize: 16,
                            fontWeight: 'bold',
                          }}>
                          {this.state.IsCashSales ? 'Cash' : 'Credit'}
                        </Text>
                        <Switch
                          style={styles.TypeSwitch}
                          onValueChange={IsCashSales =>
                            this.setState({IsCashSales})
                          }
                          value={this.state.IsCashSales}
                        />
                      </View>
                    </View>
                  )}
                  {IsChangeStatus && (
                    <ModalSearchableLabel
                      fieldWrapperStyle={
                        (ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        {
                          width: '100%',
                          paddingLeft: '72%',
                          height: 50,
                          paddingRight: 5,
                          marginVertical: 0,
                        })
                      }
                      placeholder={`${
                        this.state.Status && this.state.Status != ''
                          ? this.state.Status
                          : 'Status'
                      }`}
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.state.StatusFlag}
                      inputStyle={[
                        {
                          marginTop: 0,
                          borderBottomWidth: 0,
                          paddingRight: 5,
                          paddingTop: 15,
                          marginBottom: 0,
                          borderTopLeftRadius: 10,
                          borderBottomLeftRadius: 10,
                          textAlign: 'center',
                        },
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )}
                  {this.FieldType('FieldType')}
                  {this.FieldAgent('FieldAgent')}
                  {this.FieldRemarks('FieldRemarks')}
                  {this.FieldDocInfo('FieldDocInfo')}
                  {this.FieldCustomer('FieldCustomer')}
                  {this.FieldTransport('FieldTransport')}
                  {this.FieldOrdermode('FieldOrdermode')}
                  {this.FieldStockpoint('FieldStockpoint')}
                  {this.FieldPaymenttype('FieldPaymenttype')}
                  {this.state.InvoiceType === 'SO' &&
                    this.FieldBasedon('FieldBasedon')}
                  {(this.state.InvoiceType === 'SI' ||
                    this.state.InvoiceType === 'CN') &&
                    this.FieldGST('FieldGST')}
                  {(this.state.InvoiceType === 'SQ' ||
                    this.state.InvoiceType === 'SO') &&
                    this.FieldValidFromAndTo('FieldValidFromAndTo')}
                  {this.FieldCurrency('FieldCurrency')}
                </View>
              </ScrollView>

              <View
                style={{
                  justifyContent: 'flex-start',
                  paddingBottom: 5,
                  backgroundColor: globalColorObject.Color.Lightprimary,
                }}>
                <View style={styles.ValueContainer}>
                  <Text style={styles.ValueLabel}>Total Qty</Text>
                  <Text style={styles.ValueLabel}>
                    {parseFloat(this.state.NetQty).toFixed(2)}
                  </Text>
                </View>
                {/* <View style={styles.ValueContainer}>
      <Text style={styles.ValueLabel}>Basic Value</Text>
      <Text style={styles.ValueLabel}>
      {this.state.BasicAmount.toFixed(2)}
      </Text>
      </View> */}
                <View style={styles.ValueContainer}>
                  <Text style={styles.ValueLabel}>Net Value</Text>
                  <Text style={styles.ValueLabel}>
                    {parseFloat(this.state.NetAmount).toFixed(2)}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{flex: 1}}>
              <View style={styles.scanComponentContainer}>
                <Ripple
                  onPress={this.showMenu}
                  style={styles.dotMenuIconContainer}
                  rippleCentered={true}
                  rippleContainerBorderRadius={50}>
                  <Image
                    source={require('../../../assets/images/dot-menu-grey.png')}
                    style={styles.dotMenuIcon}
                  />
                  <Menu ref={this.setMenuRef} button={<></>}>
                    {this.state.InvoiceType === 'SO' && (
                      <MenuItem
                        onPress={this.SelectedMenu.bind(this, 'RRatio', false)}>
                        Received Ratio
                      </MenuItem>
                    )}
                    <MenuItem
                      onPress={this.isShowModal.bind(
                        this,
                        'CategoryConfig',
                        true,
                      )}>
                      Category Config
                    </MenuItem>
                    <MenuItem
                      onPress={this.HandleisShowCAtegoryRatio.bind(this, true)}>
                      Category Ratio
                    </MenuItem>
                    <MenuItem
                      onPress={this.SelectedMenu.bind(this, 'Discount', true)}>
                      Discount
                    </MenuItem>
                    <MenuItem onPress={this.hideMenu}>Sales Man</MenuItem>
                  </Menu>
                </Ripple>

                <TextInput
                  onBlur={this.HandleScanBarcode}
                  style={[
                    layoutStyle.FieldInput,
                    styles.SearchInput,
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.BackgroundColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderColor,
                    ),
                    //  {marginTop: 0, paddingBottom: 8, fontSize: 18},
                  ]}
                  onChangeText={Barcode => this.setState({Barcode})}
                  value={this.state.Barcode}
                  returnKeyType="next"
                  autoCorrect={false}
                  autoCapitalize="none"
                  ref={'Barcode'}
                  placeholder="Scan Barcode"
                  placeholderTextColor="#AFAFAF"
                />
                <TouchableOpacity onPress={() => this.onOpenScanner()}>
                  <Image
                    source={require('../../../assets/images/ScanBarcode.png')}
                    style={styles.ScanBarcode}
                  />
                </TouchableOpacity>
              </View>
             
              <FlatList
                style={[
                  ApplyStyleColor(
                    globalColorObject.Color.Lightprimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}
                data={this.state.APIData.ScanBarcode}
                renderItem={({item, index}) => (
                  <ListItem
                    ListHeaderComponent={<ListItemHeader  />}
                    data={item}
                    index={index}
                    onQtyChange={this.UpdateQuantity}
                    onPriceChange={this.UpdatePrice}
                    onPress={this.DeleteBarcode.bind(this, index)}
                    quantityHandler={this.quantityHandler}
                  />
                )}
                keyExtractor={item => item.Icode.toString()}
                // refreshing={false}
                // onRefresh={this.LoadInitialData}
                // ListHeaderComponent={<ListItemHeader showPrevList={this.state.PrevList.length>1?true:false} onShowPrevs={this.HandleShowPrev}/>}
                ListHeaderComponent={<ListItemHeader showPrevList={this.PrevItems.length>1?true:false} onShowPrevs={this.HandleShowPrev}/>}
                stickyHeaderIndices={[0]}
                // to have some breathing space on bottom
                ListFooterComponent={() => (
                  <View style={{width: '100%', marginTop: 85}} />
                )}
              />
            </View>
          </ViewPager>
        </LayoutWrapper>
      </>
    );
  }
}
const styles = StyleSheet.create({
  ValueContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
  },
  scanComponentContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
  },
  CreatedItemsTab: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
  dotMenuIconContainer: {
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'salmon'
  },
  dotMenuIcon: {
    width: 27,
    height: 34,
  },
  ScanBarcode: {
    width: 40,
    height: 40,
    top: 5,

    // borderRadius: 40,
    // borderWidth: 3,
    // borderColor: "#FFF"
  },
  ValueLabel: {
    fontSize: 15,
  },
  Loadercontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  ViewPager: {
    flex: 1,
  },
  OptionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
  Buttons: {
    padding: 10,
  },
  TabView: {
    padding: 10,
    //borderBottomWidth: 2,
    width: '100%',
    textAlign: 'center',
  },
  Tabs: {
    flexDirection: 'row',
    // borderTopColor: 'rgb(221, 221, 221)',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  ActiveTab: {
    //color: globalColorObject.Color.Primary,
    padding: 10,
    borderTopWidth: 2,
    //borderTopColor: globalColorObject.Color.Primary,
    width: '100%',
    textAlign: 'center',
  },
  InActiveTab: {
    padding: 10,
    color: 'gray',
    width: '100%',
    //borderBottomWidth: 2,
    //borderBottomColor: 'rgba(0,0,0,0.15)',
    textAlign: 'center',
  },
  ActiveText: {
    fontWeight: 'bold',
    color: 'blue',
  },
  FlatList: {
    //paddingVertical: 0,
    //marginRight: 0,
    //marginLeft: 0,
    // borderRadius: 50,
  },
  PageContainer: {
    paddingHorizontal: 10,
  },
  TypeSwitch: {
    marginRight: 10,
    // transform: [{scaleX: 1.2}, {scaleY: 1.2}],
  },
  FieldContainer: {
    // width: "100%",
    flexDirection: 'row',
    position: 'relative',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.07)',
    marginVertical: 5,
  },
  PickerFieldContainer: {
    width: '100%',
    borderBottomColor: 'rgba(0,0,0,0.07)',
    // marginVertical: 5,
  },
  CardLine: {
    // paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    // borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: 'white',
  },
  CusFieldContainer: {
    flexDirection: 'row',
    position: 'relative',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.07)',
    marginVertical: 5,
  },
  modalContainer: {
    height:'100%',
    width:'100%'
    // flex: 1,
  },
  // FieldLabel: {
  // 	flex: 1,
  // 	alignSelf: 'center',
  // 	fontSize: 12,
  // 	// fontWeight: 'bold',
  // 	position: 'absolute',
  // 	top: 0,
  // 	left: 5,
  // 	color: 'gray',
  // },
  FieldInput: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    marginTop: 8,
  },
  FieldInputMultiline: {
    flex: 2,
    // height: 45,
    alignSelf: 'center',
    textAlign: 'left',
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderRadius: 5,
    // paddingTop: 15,
    // paddingBottom: 7,
    // // borderWidth: 1,
    // borderBottomWidth: 0,
    // borderLeftWidth: 0,
    // borderColor: 'lightgray',
    marginTop: 2,
    marginHorizontal: 9,
    // elevation: 3,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#26B1AD',
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },

  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },

  SearhBarWrapper: {
    borderColor: 'gray',
    flexDirection: 'row',
    // justifyContent: 'space-between',
    padding: 10,
    backgroundColor: '#ededed',
  },
  SearchInput: {
    backgroundColor: globalColorObject.Color.oppPrimary,
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
    marginRight: 5,
    borderBottomColor: globalColorObject.Color.Primary,
    borderBottomWidth: 0.8,
    //fontSize: 15,
    //width: '100%',
  },
  closeButton: {
    height: 30,
    width: 30,
    // paddingLeft:30,
    marginLeft: 10,
  },
  colo: {
    backgroundColor: globalColorObject.Color.Lightprimary,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 10,
    top: 14,
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  cardTextFieldHeader: {
    fontSize:globalFontObject.Size.small.sxl,
    color: 'white',
    paddingLeft: 10,
    textAlign:'center'
  },
  AddNewItem: {
    width: 120,
    alignSelf: 'flex-end',
    height: 40,
    marginRight: 10,
    marginVertical: 8,
  },
  TestButton: {
    position: 'relative',
  },
  CardLineList: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  ListCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: globalColorObject.Color.oppPrimary,
    marginHorizontal: 9,
    // marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    //elevation: 5,
    marginTop: 5,
  },
  cardTextFieldList: {
    fontSize:globalFontObject.Size.small.sxl,
    paddingLeft: 10,
  },
  removeItemIconContainer: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 5,
    marginTop: 5,
    height: 20,
    width: 20,
  },
  Shar: {
    alignContent: 'flex-end',
  },
  ConfigLogo: {
    // marginTop: 5,
    width: 30,
    height: 30,
    alignSelf: 'flex-end',
  }
});
