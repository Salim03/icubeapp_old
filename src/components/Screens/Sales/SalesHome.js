import React, { Component } from 'react';
import {
	View, SafeAreaView, StatusBar, Text, StyleSheet, ScrollView, Image, TouchableOpacity, Dimensions,
} from 'react-native';

import { getFormsForModule, AlertError, getPropsFromArray } from '../../../Helpers/HelperMethods';

export default class SalesHome extends Component {
	constructor() {
		super();
		this.state = {
			ScreenWidth: 0,
			forms: []
		};
	}
	componentDidMount() {
		this.setState({ ScreenWidth: Dimensions.get('window').width });

		getFormsForModule(8)
			.then(data => {
				const forms = data && getPropsFromArray(data, ["MENUNAME"])["MENUNAME"].map(elem => elem.replace(/ /g, ""));
				this.setState({ forms });
			})
			.catch(AlertError);
	}

	NavigateToForm = (form,Type,FormIndex) => {

		if (form === "Sales") {
			this.props.navigation.navigate("SalesNavigator", {
				HeaderTitle : (Type== 'SO' ? 'Sales Order' : 
				(Type== 'DC' ? 'Delivery Challan' : 
				(Type=='SR'?'Sales Return' : 
				(Type=='SI'?'Sales Invoice': 
				(Type=='CN'?'CreditNote':
				(Type== 'SQ' ? 'Sales Quotation' : undefined)))))) ,
				CallFrom : Type,
				FormIndex
			});

		} 
		else {
			this.props.navigation.navigate(form, {
				CallFrom: Type,
				FormIndex
			});
			
		}
	}

// Purchase NAV REFERENCE
	// NavigateToForm = (nav, HeaderTitle, CallFrom, FormIndex) => {
	// 	this.props.navigation.navigate(nav, {
	// 		HeaderTitle,
	// 		CallFrom,
	// 		FormIndex
	// 	});
	// 	console.log(FormIndex,"hisman FormIndex check Purchase" );

	// }
	FormSalesQuotation = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"Sales", "SQ","4.12.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/SalesEstimation.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Sales</Text>
				<Text style={styles.FormLabel}>Quotation</Text>
			</TouchableOpacity>
		)
	}
	// onPress={this.NavigateToForm.bind(this, "Sales","Sales Quotation",3)}

	FormSalesOrder = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"Sales", "SO","4.5.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/order.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Sales</Text>
				<Text style={styles.FormLabel}>Order</Text>
			</TouchableOpacity>
		)
	}
	// onPress={this.NavigateToForm.bind(this, "Sales","Sales Order",3)}

	FormDeliveryChallan = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "Sales","DC","4.1.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/delivery.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Delivery</Text>
				<Text style={styles.FormLabel}>Challan</Text>
			</TouchableOpacity>
		)
	}
	// onPress={this.NavigateToForm.bind(this, "Sales","Delivery Challan",3)}

	FormSalesInvoice = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "Sales","SI","4.3.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/Sales.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Sales</Text>
				<Text style={styles.FormLabel}>Invoice</Text>
			</TouchableOpacity>
		)
	}
	// onPress={this.NavigateToForm.bind(this, "Sales","Sales Invoice",3)}

	FormSalesReturn = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"Sales", "SR","4.2.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/return.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Sales</Text>
				<Text style={styles.FormLabel}>Return</Text>
			</TouchableOpacity>
		)
	}
	// onPress={this.NavigateToForm.bind(this, "Sales","Sales Return",3)}

	FormCreditNote = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"Sales", "CN","22.12.0")}>
				<Image
					source={require('../../../assets/images/Home/Sales/CreditNote.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Credit</Text>
				<Text style={styles.FormLabel}>Note</Text>
			</TouchableOpacity>
		)
	}
	render() {
		return (
			<>
				<SafeAreaView style={{ flex: 1 }}>
					<View style={styles.LoadContainer}>
						<ScrollView style={styles.DataContainer}>
							<View style={styles.ListContainer}>
								{this.state.forms?.map(form => this[`Form${form}`]?.(form))}
								{/* <TouchableOpacity style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "CustomerHistoryNav", "Customer")}>
									<Image
										source={require('../../../assets/images/Home/common/vendor-history.png')}
										style={styles.FormImages}
									/>
									<Text style={styles.FormLabel}>Vendor</Text>
									<Text style={styles.FormLabel}>History</Text>
								</TouchableOpacity> */}
							</View>
						</ScrollView>
					</View>
					<View style={{ backgroundColor: 'rgba(0,0,0,0)', position: 'absolute', bottom: 0, right: 0 }}>
						{/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
						<View style={{ alignItems: 'flex-end', marginRight: 20, flexDirection: 'row', marginBottom: 5 }}>
							<Text style={{ textAlign: 'right' }}>Powered By</Text>
							<Image
								source={require('../../../assets/images/Drawer/Logo.png')}
								style={{ height: 30, width: 100 }}
							/>
						</View>
					</View>
				</SafeAreaView>
			</>
		);
	}
};

const styles = StyleSheet.create({
	Text: {
		marginTop: '80%',
		textAlign: 'center',
		fontSize: 22,
		fontWeight: 'bold',
	},
	LoadContainer: {
		// backgroundColor: 'rgb(43, 54, 255)',
		backgroundColor: '#81D4FA',
		flex: 1
	},
	FormImages: {
		width: 30,
		height: 30,
		marginLeft: 5,
		marginRight: 5,
		marginBottom: 5,
		marginTop: 5
	},
	FormLabel: {
		textAlign: 'center',
		fontSize: 16,
		// fontWeight: 'bold',
	},
	DataContainer: {
		backgroundColor: 'white',
		marginTop: 30,
		// marginLeft: 5,
		// marginRight: 5,
		borderTopLeftRadius: 35,
		borderTopRightRadius: 35,
		flex: 1,
		width: '100%',
		elevation: 50,
	},
	ListContainer: {
		// marginTop: 10,
		// marginLeft:20,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		width: '100%',
		// marginBottom: 5,
		flexWrap: 'wrap',
	},
	FormContainer: {
		// backgroundColor: 'rgb(252, 172, 182)',
		backgroundColor: 'white',
		marginBottom: 10,
		padding: 5,
		height: 100,
		width: 100,
		alignItems: 'center',
		borderRadius: 10,
		elevation: 15,
	},
});
	// export {Retail};