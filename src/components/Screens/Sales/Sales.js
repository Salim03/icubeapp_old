/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {Component} from 'react';
import {
  Image,
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import moment from 'moment';
import {
  globalFontObject,
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
// Local Imports
import {CardStyle} from '../../Layout/Layout';

import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {
  Request,
  AlertStatusError,
  AlertError,
  sharefileForCallFRom,
  GetSessionData,
  AlertMessage,
  DeleteSelectedInvoice,
} from '../../../Helpers/HelperMethods';
const ListItemHeader = props => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View
          style={[
            styles.CardTextContainer,
            {
              width:
                props.callFrom == 'SO' || props.callFrom == 'SQ'
                  ? '74%'
                  : '100%',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Customer Name
          </Text>
        </View>
        {(props.callFrom == 'SO' || props.callFrom == 'SQ') && (
          <View style={[styles.CardTextContainer, {width: '24%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              Status
            </Text>
          </View>
        )}

        <View style={[styles.CardTextContainer, {width: '34%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Number
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '24%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Date
          </Text>
        </View>
        <View
          style={[styles.CardTextContainer, CardItemLayout, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
};
const ListItem = props => {
  const data = props.data;
  let isDoubleCard = props.isDoubleCard;
  let FullInvoiceDate = new Date(props.data.InvoiceDate);
  let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
    '0' +
    (FullInvoiceDate.getMonth() + 1)
  ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
  let statusvalue = data.StatusFlag;
  let statuscolor =
    statusvalue == 2 //'Approved'
      ? 'blue'
      : statusvalue == 4 //'Delivered'
      ? 'maroon'
      : statusvalue == 3 || statusvalue == 4 //statusvalue == 'In Progress' || statusvalue == 'Partial Delivered'
      ? '#684E53'
      : statusvalue == 5 //'Canceled'
      ? 'red'
      : statusvalue == 1 //'Open'
      ? 'darkgreen'
      : statusvalue == 7 //'Hold'
      ? 'gold'
      : 'black';

  return (
    <TouchableWithoutFeedback
      onLongPress={() => props.onLongPress(data.ID, data.InvoiceNo)}>
      <View
        style={[
          styles.Card,
          isDoubleCard && {maxWidth: '49%'},
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View
          style={[
            styles.CardLine,
            // {
            //   borderLeftWidth: 1,
            //   borderColor: 'lightgray',
            // },
          ]}>
          <View
            style={[
              styles.CardTextContainer,
              {
                width:
                  props.callFrom == 'SO' || props.callFrom == 'SQ'
                    ? '74%'
                    : '100%',
              },
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.CustomerName}
            </Text>
          </View>
          {(props.callFrom == 'SO' || props.callFrom == 'SQ') && (
            <ModalSearchableLabel
              placeholder={
                data.Status && data.Status != '' ? data.Status : 'Status'
              }
              fieldWrapperStyle={[
                styles.CardTextContainer,
                {width: '24%', marginHorizontal: 0, marginVertical: 5},
              ]}
              data={props.ChangeStatusDataList(data.StatusFlag)}
              labelProp="label"
              valueProp="value"
              selectedValue={data.StatusFlag}
              inputStyle={[
                {
                  marginTop: 0,
                  paddingBottom: 0,
                  borderBottomWidth: 0,
                  paddingTop: 0,
                  padding: 0,
                  marginHorizontal: 0,
                  paddingHorizontal: 0,
                  marginBottom: 0,
                  height: 'auto',
                  textAlign: 'right',
                  // textDecorationLine: 'underline',
                  color: statuscolor,
                },
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                ),
                // ApplyStyleColor(
                //   globalColorObject.Color.oppPrimary,
                //   globalColorObject.ColorPropetyType.Color,
                // ),
              ]}
              onValueSelected={changevalue =>
                props.ChangeStatus(changevalue, data.ID)
              }
            />
            // <View
            //   style={[
            //     styles.CardTextContainer,
            //     {width: '24%', alignContent: 'center'},
            //   ]}>
            //   <Text
            //     numberOfLines={1}
            //     style={[
            //       ApplyStyleFontAndSizeAndColor(
            //         globalFontObject.Font.Bold,
            //         globalFontObject.Size.small.sm,
            //         globalColorObject.Color.BlackColor,
            //         globalColorObject.ColorPropetyType.Color,
            //       ),
            //       {
            //         color: statuscolor,
            //         // color: 'white',
            //         textAlign: 'right',
            //       },
            //     ]}>
            //     {data.Status}
            //   </Text>
            // </View>
          )}
          <View style={[styles.CardTextContainer, {width: '34%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.InvoiceNo}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '24%'}]}>
            <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {InvoiceDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {(Math.round(data.NetAmount * 100) / 100).toFixed(2)}
            </Text>
          </View>
        </View>

        <View
          style={[
            {
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              backgroundColor: globalColorObject.Color.Lightprimary,
            },
          ]}>
          <View
            style={[
              {
                width: 0,
                height: 0,
                backgroundColor: globalColorObject.Color.Lightprimary,
                borderRightWidth: 20,
                borderTopWidth: 20,
                borderRightColor: globalColorObject.Color.Lightprimary,
                borderTopColor: globalColorObject.Color.oppPrimary,
              },
              {
                transform: [{rotate: '90deg'}],
              },
            ]}></View>
          <TouchableOpacity
            onPress={() => props.onDeleteInvoice(data.ID)}
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              // borderBottomWidth: 1,
              // borderBottomColor: 'lightgray',
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/3096/3096673.png',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              // borderBottomWidth: 1,
              // borderRightWidth: 1,
              // borderColor: 'lightgray',
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                data.ID,
                data.InvoiceNo,
                moment(new Date(InvoiceDate)).format('YYYY-MM-DD'),
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1059/1059106.png',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class Sales extends Component {
  constructor() {
    super();
    this.LocalData = {
      Title: '',
      CallFrom: '',
      Loccode: 0,
      FormIndex: '',
      RoleId: '',
      UserCode: '',
      FinancialYearId: '0',
    };
    this.state = {
      isFetched: false,
      StatusGroupData: [],
      SelectedStatus: 'All',
      SAList: [],
      ListSourceData: [],
      NoDataAvailable: false,
      GetType: '',
      SearchText: '',
      show: false,
    };
  }

  componentDidMount() {
    let params = this.props.route.params;
    this.LocalData.Title = params.HeaderTitle || 'Sales Order';
    this.LocalData.CallFrom = params.CallFrom || 'SO';
    this.LocalData.FormIndex = params.FormIndex || '4.5.0';
    // this.props.navigation.setOptions({
    //   title: this.LocalData.Title,
    // });
    this.LoadInitialData();
    // this.setState({ ScreenWidth: Dimensions.get('window').width })
    // console.log(Title,"hisman Title check" );
    // console.log(CallFrom,"hisman CallFrom check" );
    // console.log(FormIndex,"hisman FormIndex check" );
  }

  ChangeStatus = (changestatus, invoiceid) => {
    this.setState({isFetched: false});
    Request.post(
      `${this.IP}/api/SoApproval/ApprovalSave?SalesType=${this.LocalData.CallFrom}&PassID=${invoiceid}^&PassType=${changestatus}`,
      null,
      this.Token,
    )
      .then(res => res.json())
      .then(json => {
        console.log('json data : ', json);
        if (json && json == 'Saved') {
          AlertMessage(`Status updated successfully`, this.LoadAllSales);
        } else {
          AlertMessage('Failed to update status');
        }
        this.setState({isFetched: true});
      })
      .catch(err => {
        this.setState({isFetched: true});
      });
  };

  ChangeStatusDataList = passstatusflag => {
    return passstatusflag == 1 // Open
      ? [
          {label: 'Approve', value: 'Approval'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : passstatusflag == 2 // Approved
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : passstatusflag == 3 // Receiving
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Close', value: 'Close'},
        ]
      : passstatusflag == 7 // Hold
      ? [{label: 'Release', value: 'Release'}]
      : [];
  };

  LoadInitialData = async () => {
    const {
      get_IP,
      get_Token,
      get_UserCode,
      get_Location,
      get_FinancialYearId,
      get_RoleId,
    } = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.Loccode = get_Location;
    this.LocalData.RoleId = get_RoleId;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.FinancialYearId = get_FinancialYearId;

    this.LoadAllSales();
  };

  LoadAllSales = async () => {
    console.log(
      `${this.IP}/api/WholeSale/AllInvoice?CallFrom=${this.LocalData.CallFrom}&ShowOnlySAB=1&LocCode=${this.LocalData.Loccode}&EmpId=${this.LocalData.UserCode}`,
    );
    Request.get(
      `${this.IP}/api/WholeSale/AllInvoice?CallFrom=${this.LocalData.CallFrom}&ShowOnlySAB=1&LocCode=${this.LocalData.Loccode}&EmpId=${this.LocalData.UserCode}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            console.log('my list data', res.data);
            let GetresData = res.data;
            let groupData = {};
            GetresData.reduce((r, o) => {
              let key = o.Status;
              if (!groupData[key]) {
                groupData[key] = Object.assign(
                  {},
                  {
                    Status: o.Status,
                    Count: 1,
                  },
                ); // create a copy of o
                // r.push(groupData[key]);
              } else {
                groupData[key].Count += 1;
              }
            }, []);
            let GroupStatusOut = [];
            let TotalCount = GetresData.length;
            GroupStatusOut.push(
              Object.assign(
                {},
                {
                  Status: 'All'.toString(),
                  Count: TotalCount,
                  Label: 'All' + ` (${TotalCount})`,
                },
              ),
            );
            Object.keys(groupData).map(key => {
              // console.log(key, groupData[key], groupData[key].Count);
              GroupStatusOut.push(
                Object.assign(
                  {},
                  {
                    Status: key.toString(),
                    Count: groupData[key].Count,
                    Label: key + ` (${groupData[key].Count})`,
                  },
                ),
              );
            });
            this.setState({
              isFetched: true,
              SAList: GetresData,
              ListSourceData: GetresData,
              StatusGroupData: GroupStatusOut,
              SelectedStatus: 'All',
              // SelectedStatus:
              //   GroupStatusOut && GroupStatusOut.length > 0
              //     ? GroupStatusOut[0].Status
              //     : 'All',
            });
          } else {
            this.setState({isFetched: true, NoDataAvailable: true});
          }
        } else {
          AlertStatusError(res);
          this.setState({isFetched: true});
        }
      })
      .catch(err => AlertError(err) && this.setState({isFetched: true}));
  };

  HandleSearch = async SearchText => {
    this.setState({SearchText});
    let SAList = this.state.SAList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = SAList.filter(obj => {
      // Get all the texts
      let FullInvoiceDate = new Date(obj.InvoiceDate),
        FullDocDate = obj.DOCDATE ? new Date(obj.DOCDATE) : '',
        FullAgainstDate = obj.AgainstDate ? new Date(obj.AgainstDate) : '';
      let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
        '0' +
        (FullInvoiceDate.getMonth() + 1)
      ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
      let DocDate = FullDocDate
        ? `${('0' + FullDocDate.getDate()).slice(-2)}/${(
            '0' +
            (FullDocDate.getMonth() + 1)
          ).slice(-2)}/${FullDocDate.getFullYear()}`
        : '';
      let AgainstDate = FullAgainstDate
        ? `${('0' + FullAgainstDate.getDate()).slice(-2)}/${(
            '0' +
            (FullAgainstDate.getMonth() + 1)
          ).slice(-2)}/${FullAgainstDate.getFullYear()}`
        : '';
      let InvoiceNo = (obj.InvoiceNo || '').toString().toLowerCase();
      let Status = (obj.Status || '').toString().toLowerCase();
      let CustomerName = (obj.CustomerName || '').toString().toLowerCase();
      let DOCNO = (obj.DOCNO || '').toString().toLowerCase();
      let AgainstNo = (obj.AgainstNo || '').toString().toLowerCase();
      let Amount = (Math.round(obj.NetAmount * 100) / 100)
        .toFixed(2)
        .toString();

      return (
        InvoiceDate.includes(txt) ||
        DocDate.includes(txt) ||
        AgainstDate.includes(txt) ||
        InvoiceNo.includes(txt) ||
        Status.includes(txt) ||
        CustomerName.includes(txt) ||
        DOCNO.includes(txt) ||
        AgainstNo.includes(txt) ||
        Amount.includes(txt)
      );
    });
    this.setState({ListSourceData});
  };

  _get = async url => {
    const res = await fetch(url, {
      headers: {
        Authorization: this.Token,
      },
    });
    const json = await res.json();
    return {
      data: json,
      status: res.status,
    };
  };

  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={styles.closeButtonParent}
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  clearInput = () => {
    this.HandleSearch('');
  };

  NavigateToEditSales = (ID, InvoiceID, InvoiceNO, FormIndex) => {
    this.props.navigation.navigate('AddSalesNavigator', {
      HeaderTitle: this.LocalData.Title,
      InvoiceID,
      InvoiceNO,
      CallFrom: this.LocalData.CallFrom,
      Type: this.LocalData.CallFrom,
      SId: ID,
      Loccode: this.LocalData.Loccode,
      FormIndex,
      onUpdateInvoice: this.LoadAllSales,
    });
  };

  onDeleteSelectedInvoice = async invoiceid => {
    await DeleteSelectedInvoice(
      this.LocalData.CallFrom,
      this.LocalData.FormIndex,
      invoiceid,
      this.LocalData.Loccode,
      this.LocalData.RoleId,
      this.LocalData.UserCode,
      null, //this.EnableLoadScreen,
      this.LoadAllSales,
    );
    // this.DsableLoadScreen();
  };

  CallShareFile = async (InvoiceID, InvoiceNO, InvoiceDate) => {
    try {
      this.HandleFetchFlag(false);
      await sharefileForCallFRom(
        this.LocalData.CallFrom,
        InvoiceID,
        InvoiceNO,
        InvoiceDate,
        this.LocalData.FormIndex,
        this.LocalData.Loccode,
      );
    } catch (fgdd) {}
    this.HandleFetchFlag(true);
  };

  HandleFetchFlag = isFetched => {
    this.setState({isFetched: isFetched});
  };

  refreshLayout = () => this.forceUpdate();
  render() {
    console.log(this.state.isFetched, 'Fetching Status');
    let isDoubleCard = false;
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    return (
      <>
        <SafeAreaView style={styles.container}>
          <View
            style={[
              styles.SearhBarWrapper,
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
              {
                //paddingBottom: this.LocalData.CallFrom == 'SO' ? 0 : 10
                width: '100%',
                flexDirection: 'row',
              },
            ]}>
            <TextInput
              style={[
                styles.SearchInput,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                {
                  width:
                    this.LocalData.CallFrom == 'SO' ||
                    this.LocalData.CallFrom == 'SQ'
                      ? '70%'
                      : '100%',
                },
              ]}
              placeholder={'Search here'}
              value={this.state.SearchText}
              onChangeText={this.HandleSearch}
              autoCorrect={false}
              returnKeyType="search"
              autoCapitalize="none"
              onFocus={this.showCancel}
              onBlur={this.hideCancel}
            />
            {(this.LocalData.CallFrom == 'SO' ||
              this.LocalData.CallFrom == 'SQ') && (
              <View style={{width: '30%'}}>
                <ModalSearchableLabel
                  placeholder="Status"
                  fieldWrapperStyle={{height: 30, width: '100%'}}
                  data={this.state.StatusGroupData}
                  labelProp="Label"
                  valueProp="Status"
                  selectedValue={this.state.SelectedStatus}
                  inputStyle={[
                    {
                      paddingLeft: 5,
                      marginTop: 10,
                      marginBottom: 0,
                      borderBottomWidth: 0,
                      textAlign: 'center',
                      textDecorationLine: 'underline',
                    },
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderBottomColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}
                  onValueSelected={SelectedStatus => {
                    this.setState({SelectedStatus});
                    this.HandleSearch(
                      SelectedStatus != 'All' ? SelectedStatus : '',
                    );
                  }}
                />
              </View>
            )}
          </View>
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={{
                  fontSize: 25,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 165,
                }}>
                No Data Found
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 5,
                }}>
                Pull to refresh
              </Text>
            </View>
          )}
          <FlatList
            style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            numColumns={isDoubleCard && 2} // set number of columns
            columnWrapperStyle={
              isDoubleCard && {
                flex: 1,
                justifyContent: 'space-around',
              }
            } // space them out evenly
            data={this.state.ListSourceData}
            ListHeaderComponent={() => (
              <ListItemHeader callFrom={this.LocalData.CallFrom} />
            )}
            renderItem={({item}) => (
              <ListItem
                data={item}
                callFrom={this.LocalData.CallFrom}
                isDoubleCard={isDoubleCard}
                onLongPress={this.NavigateToEditSales.bind(
                  this,
                  item.ID,
                  item.ID,
                  item.InvoiceNo,
                  this.LocalData.FormIndex,
                )}
                onPdfClick={this.CallShareFile}
                onDeleteInvoice={this.onDeleteSelectedInvoice}
                ChangeStatusDataList={this.ChangeStatusDataList}
                ChangeStatus={this.ChangeStatus}
              />
            )}
            keyExtractor={item => item.ID.toString()}
            stickyHeaderIndices={[0]}
            refreshing={false}
            onRefresh={this.LoadAllSales}
          />
          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.props.navigation.navigate('AddSalesNavigator', {
                HeaderTitle: this.LocalData.Title,
                Type: this.LocalData.CallFrom,
                SId: 0,
                Loccode: this.LocalData.Loccode,
                onUpdateInvoice: this.LoadAllSales,
              });
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  SearhBarWrapper: {
    padding: 10,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
    height: 150,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderRadius: 5,
    // paddingTop: 15,
    // paddingBottom: 7,
    // // borderWidth: 1,
    // borderBottomWidth: 0,
    // borderLeftWidth: 0,
    // borderColor: 'lightgray',
    marginTop: 2,
    marginHorizontal: 9,
    // elevation: 3,
  },
  CardLine: {
    // paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    // borderBottomWidth: 1,
    paddingLeft: 5,
    // borderColor: 'lightgray',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 10,
    marginBottom: 5,
    paddingVertical: 5,
    elevation: 5,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  closeButton: {
    height: 25,
    width: 25,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 12,
    top: 16,
  },
  Shar: {
    alignContent: 'flex-end',
  },
  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});
