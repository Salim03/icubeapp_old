import React, { useEffect, useState } from 'react';
import { StatusBar, SafeAreaView,  StyleSheet, ScrollView, View, Text } from 'react-native';
import { Table, TableWrapper, Cell, Row } from 'react-native-table-component';

import AsyncStorage from '@react-native-async-storage/async-storage';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import { Request, AlertMessage } from '../../../Helpers/HelperMethods';

const VendorHistoryItemWise = (props) => {
   // getting selected InvoiceID from nvaigation params
   let params = props.route.params;
   const InvoiceID = params && params.InvoiceID;
   const VendorType = params && params.VendorType;

   let [reportData, setReportData] = useState({ isFetched: false, data: [] });

   useEffect(() => {
      FetchInvoiceData();
   }, []);

   const FetchInvoiceData = async () => {
      let IP = await AsyncStorage.getItem("IP");
      let Token = await AsyncStorage.getItem("Token");
      let APILink = `${IP}/api/VendorHistory/GetSelectedInvoiceItems?BusinessPartnerType=${VendorType}&InvoiceID=${InvoiceID}`;
      // fetching Selected Invoice Item data
      Request.get(APILink, Token)
         .then(res => {
            console.log(res);
            if (res.status === 200) {
               if (res.data.length) setReportData({ isFetched: true, data: res.data });
            } else AlertMessage(`${res.status} - ${res.data['Message']}`);
         });
   };

   const DescriptionElem = (Description) => (<Text numberOfLines={3} style={styles.tableDataText}>{Description}</Text>);

   const header = ["Barcode", "Description", "Rate", "MRP", "Qty", "Adjusted", "Sales", "OnHand", "MU", "MD", "Sales %", "ROI"];
   const headerWidthArr = [65, 120, 60, 60, 37, 67, 45, 60, 46, 46, 55, 46];

   if (!reportData.isFetched) {
      return (
         <ICubeAIndicator />
      );
   }
   return (
      <>
         
         <SafeAreaView style={styles.container}>
            <ScrollView horizontal style={styles.ScrollView}>
               <View>
                  <Table borderStyle={{ borderWidth: 1, borderColor: '#B5B5B5' }}>
                     {/* <TableWrapper style={{ width: '100%', flexDirection: 'row', backgroundColor: '#E9E9E9' }}>
                        {headers.map(title => (
                           <Cell key={title} data={title} style={{ width: "Description".includes(ind) ? 100 : 80 }} />
                        ))
                        }</TableWrapper> */}
                     <Row data={header} widthArr={headerWidthArr} textStyle={styles.headerField} style={{ width: '100%', backgroundColor: '#E9E9E9' }} />
                  </Table>
                  <ScrollView>
                     <Table borderStyle={{ borderWidth: 1, borderColor: '#B5B5B5' }}>
                        <TableWrapper style={{ width: '100%', backgroundColor: '#F8F8F8' }}>
                           {reportData.data.map((item, ind) => (
                              <TableWrapper key={ind} style={{ width: '100%', flexDirection: 'row' }}>
                                 <Cell data={item.Barcode} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[0] }]} />
                                 <Cell data={DescriptionElem(item.Description)} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[1] }]} />
                                 <Cell data={item.Rate} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[2] }]} />
                                 <Cell data={item.MRP} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[3] }]} />
                                 <Cell data={item.PurchaseQty} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[4] }]} />
                                 <Cell data={item.AdjustedQty} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[5] }]} />
                                 <Cell data={item.SalesQty} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[6] }]} />
                                 <Cell data={item.OnHandQty} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[7] }]} />
                                 <Cell data={item.MarkUp} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[8] }]} />
                                 <Cell data={item.MarkDown} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[9] }]} />
                                 <Cell data={item.SalesPer} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[10] }]} />
                                 <Cell data={item.ROI} textStyle={styles.tableDataText} style={[styles.tableDataField, { width: headerWidthArr[11] }]} />
                              </TableWrapper>
                           ))}
                        </TableWrapper>
                     </Table>
                  </ScrollView>
               </View>
            </ScrollView>
         </SafeAreaView>
      </>
   );
};

export default VendorHistoryItemWise;

const styles = StyleSheet.create({
   container: {
      flex: 1,
      alignItems: 'center'
   },
   ScrollView: {
      flex: 1,
      margin: 20,
      // borderWidth: 1,
      // borderColor: '#B5B5B5',
   },
   headerField: {
      fontSize: 13.5,
      // fontWeight: "400",
      paddingVertical: 7,
      paddingLeft: 3,
      color: '#000000',
      textAlign: 'left',
   },
   tableDataField: {
      paddingVertical: 7,
      paddingLeft: 3,
   },
   tableDataText: {
      fontSize: 12.5,
      color: '#4B4B4B',
      textAlign: 'left',
   }
});