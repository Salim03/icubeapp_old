import React, { Component } from 'react';
import {
    Alert, View, SafeAreaView, StatusBar, Text, Switch, StyleSheet, ActivityIndicator,
    TouchableWithoutFeedback, FlatList,
} from 'react-native';
// import { FloatingAction } from 'react-native-floating-action';

export default class Attendance extends Component {
    render() {
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <View>
                        <TouchableOpacity>
                            <Image source={require("../../../assets/images/ScanBarcode.png")}  />
                        </TouchableOpacity>
                        <TextInput
                            style={styles.FieldInput}
                            value={this.state.DocNo}
                            onChangeText={(DocNo) => this.setState({ DocNo })}
                            // keyboardType="numeric"
                            returnKeyType='next'
                            autoCorrect={false}
                            // onSubmitEditing={() => { this.refs.Freight.focus() }}
                            autoCapitalize="none"
                            ref={'DocNo'}
                        // placeholder="Rate" placeholderTextColor="black" 
                        />
                    </View>
                    <Text style={styles.Text}>Attendance Under Contruction</Text>
                </SafeAreaView>
            </>
        );
    }
};

const styles = StyleSheet.create({
    Text: {
        marginTop: '80%',
        textAlign: 'center',
        fontSize: 22,
        fontWeight: 'bold',
    }, container: {
        flex: 1,
    },
});
	// export {Retail};