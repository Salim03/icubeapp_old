// Module Imports
import React, { Component } from 'react';
import { StatusBar, Image, SafeAreaView, StyleSheet, ScrollView, View, Text, Modal, ActivityIndicator, TouchableOpacity, Keyboard }
   from 'react-native';

   import AsyncStorage from '@react-native-async-storage/async-storage';
// Local Imports
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import { Request, AlertMessage, AlertError, AlertStatusError } from '../../../Helpers/HelperMethods';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';

export default class VendorHistory extends Component {

   constructor() {
      super();
      this.state = {
         isFetched: false,
         Vendor: '',
         Summary: {
            CreditDays: '',
            STRPercent: '',
            NoOfTransaction: '',
            Outstanding: '',
            AvgCost: '',
            AvgMRP: '',
            AvgPaymentDays: '',
            AvgSoldDays: '',
            LastPurchaseBillNo: '',
            LastPurchaseBillDate: '',
            LastPurchaseBillQty: '',
            LastPurchaseBillAmount: '',
            OnHandQty: '',
            OnHandCost: '',
            OnHandMRP: '',
         },
         // Show or Hide Fetching Loader
         ShowModalLoader: false,
      };
      this.IP = "";
      this.Token = "";
      this.VendorType = "";
      let self = this;
      this.APILink = {
         get loadVendors() { return `${self.IP}/api/VendorHistory/GetAllBusinessPartners?BusinessPartnerType=${self.VendorType}` },
         get getSummaryDataForVendor() {
            return `${self.IP}/api/VendorHistory/GetSelectedBusinessPartnerSummaryReport?BusinessPartnerType=${self.VendorType}&VendorCode=${self.state.Vendor}`
         },
      }
   }

   componentDidMount() {
      let params = this.props.route.params;
      this.VendorType = params["CallFrom"] || 'Supplier';
      this.LoadInitialData();
      
		this.props.navigation.setOptions({
         headerRight: () => (
            // <View style={{
            //    position: 'absolute',
            //    right: 0,
            // }}>
               <TouchableOpacity
                  style={{ width: 40, height: 40, justifyContent: 'center', alignContent: 'flex-end' }}
                  onPress={this.navigateToDetailedHistory}>
                  <Image source={require('../../../assets/images/info-i.png')} style={{ width: 20, height: 30 }} />
               </TouchableOpacity>
         // </View>
         ),
         });
   }

   LoadInitialData = async () => {
      this.IP = await AsyncStorage.getItem("IP");
      this.Token = await AsyncStorage.getItem("Token");
      let { Token, APILink } = this;
      Request.get(APILink.loadVendors, Token)
         .then(res => {
            if (res.status === 200) {
               this.VendorList = res.data;
               this.setState({ isFetched: true });
            } else AlertStatusError(res);
         })
         .catch(AlertError);
   }

   VendorList;

   HandleVendorChange = Vendor => {
      this.setState({ Vendor, ShowModalLoader: true }, this.LoadSummaryData);
   }

   LoadSummaryData = () => {
      let { Token, APILink } = this;
      Request.get(APILink.getSummaryDataForVendor, Token)
         .then(res => {
            this.setState({ ShowModalLoader: false });
            if (res.status === 200) {
               if (!res.data.length) { AlertMessage("No data found"); return; }

               this.setState({ Summary: res.data[0] });
            }
            else AlertMessage(`${res.status} - ${res.data["Message"]}`);
         })
         .catch(err => { this.setState({ ShowModalLoader: false }); AlertMessage(JSON.stringify(err)); });
   }

   navigateToDetailedHistory = () => {
      if (this.state.Vendor === "")
         AlertMessage("Select Vendor");
      else {
         let { VendorType } = this;
         this.props.navigation.navigate(`${VendorType}HistoryDetailNav`, {
            VendorType,
            VendorCode: this.state.Vendor,
         });
      }
   }

   render() {
      if (!this.state.isFetched) {
         return <ICubeAIndicator />
      }
      let { state } = this;
      return (
         <>
           
            <SafeAreaView style={styles.container}>
               <ScrollView style={styles.ScrollView} keyboardShouldPersistTaps={'handled'}>
                  <ModalSearchablePicker
                     placeholder="Vendor"
                     data={this.VendorList}
                     labelProp="PARTYNAME"
                     valueProp="PARTYCODE"
                     selectedValue={this.state.Vendor}
                     onValueSelected={this.HandleVendorChange}
                     fieldWrapperStyle={{ marginTop: 10, width: '100%' }}
                  />
                  <View style={styles.SummaryWrapper}>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>OnHand Qty</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.OnHandQty || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Stock Value</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.OnHandCost || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Credit Days</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.CreditDays || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>STR %</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.STRPercent || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>No of Invoice</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.NoOfTransaction || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Payment Days</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.AvgPaymentDays || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Avg Cost</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.AvgCost || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Avg MRP</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.AvgMRP || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Avg Sold Days</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.AvgSoldDays || 0}</Text>
                     </View>
                     <View style={styles.SummaryField}>
                        <Text style={styles.SummaryFieldLabel}>Outstanding</Text>
                        <Text style={styles.SummaryFieldData}>{state.Summary.Outstanding || 0}</Text>
                     </View>
                  </View>
               </ScrollView>
               {/* <TouchableOpacity style={styles.BtnDetail} onPress={this.navigateToDetailedHistory}>
                  <Text style={styles.BtnDetailText}>View Detail</Text>
               </TouchableOpacity> */}
               {/* Modal Loader starts here*/}
               <Modal
                  transparent={true}
                  animationType='fade'
                  visible={state.ShowModalLoader}>
                  <View style={styles.ModalLoaderBackDrop}>
                     <View style={styles.ModalLoaderContainer}>
                        <ActivityIndicator size="large" style={styles.ModalLoader} />
                        <Text style={styles.ModalLoaderText}>Fetching data</Text>
                     </View>
                  </View>
               </Modal>
               {/* Modal Loader ends here*/}
            </SafeAreaView>
         </>
      )
   }
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
   },
   ScrollView: {
      flex: 1,
      paddingHorizontal: 8,
   },
   SummaryWrapper: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      marginTop: 10,
   },
   SummaryField: {
      width: '46%',
      paddingTop: 20,
      paddingBottom: 10,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
      elevation: 5,
      marginVertical: 7,
      marginHorizontal: '2%',
      borderRadius: 8
   },
   SummaryFieldLabel: {
      fontSize: 19,
      fontWeight: 'bold',
      marginBottom: 12,
   },
   SummaryFieldData: {
      fontSize: 20,
      // backgroundColor: 'salmon'
   },
   // BtnDetail: {
   //    position: 'absolute',
   //    bottom: 30,
   //    right: 20,
   //    backgroundColor: '#17697B',
   //    paddingHorizontal: 15,
   //    paddingVertical: 10,
   //    borderRadius: 25,
   //    elevation: 4,
   // },
   // BtnDetailText: {
   //    color: 'white',
   //    fontSize: 20
   // },
   ModalLoaderBackDrop: {
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.5)',
      justifyContent: 'center',
      alignItems: 'center',
   },
   ModalLoaderContainer: {
      width: '70%',
      height: 100,
      backgroundColor: 'white',
      borderRadius: 5,
      flexDirection: 'row',
   },
   ModalLoader: {
      flex: 1,
      alignSelf: 'center',
   },
   ModalLoaderText: {
      flex: 2,
      alignSelf: 'center',
      fontSize: 17
   },
});