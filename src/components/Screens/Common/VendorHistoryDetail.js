// Module Imports
import React, {Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {Table, TableWrapper, Cell} from 'react-native-table-component';
import Orientation from 'react-native-orientation';

// Local Imports
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {Request, AlertMessage} from '../../../Helpers/HelperMethods';

export default class VendorHistoryDetail extends Component {
  constructor() {
    super();
    this.state = {
      isFetched: false,
      data: [],
    };
    this.IP = '';
    this.Token = '';
    this.VendorType = '';
    this.VendorCode = '';
    this.InitialOrientation = '';
    let self = this;
    this.APILink = {
      get getDetailedDataForVendor() {
        return `${self.IP}/api/VendorHistory/GetSelectedBusinessPartnerDetailReport?BusinessPartnerType=${self.VendorType}&VendorCode=${self.VendorCode}`;
      },
    };
  }

  componentDidMount() {
    // getting current Orientation and lock it to Landscape
    Orientation.getOrientation((err, orientation) => {
      this.InitialOrientation = orientation;
      Orientation.lockToLandscape();
    });
    let params = this.props.route.params;
    this.VendorType = params['VendorType'];
    this.VendorCode = params['VendorCode'];
    this.LoadDetailedData();
  }

  componentWillUnmount() {
    this.InitialOrientation === 'PORTRAIT' && Orientation.lockToPortrait();
  }

  LoadDetailedData = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');

    let {Token, APILink} = this;
    Request.get(APILink.getDetailedDataForVendor, Token)
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          this.setState({isFetched: true, data: res.data});
        } else AlertMessage(`${res.status} - ${res.data['Message']}`);
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };

  combineHeaderCells = (top, bottom) => (
    <View style={styles.headerBlock}>
      <Text style={styles.headerCol}>{top}</Text>
      {bottom && (
        <Text style={[styles.headerCol, styles.headerBottom]}>{bottom}</Text>
      )}
    </View>
  );

  combineRowCells = (top, bottom) => (
    <View style={styles.rowBlock}>
      <Text numberOfLines={1} style={styles.rowCol}>
        {top}
      </Text>
      {bottom !== undefined && (
        <Text numberOfLines={1} style={[styles.rowCol, styles.rowBottom]}>
          {bottom}
        </Text>
      )}
    </View>
  );

  formatDateValue = date => {
    let dt = date && new Date(date);
    return (
      (dt &&
        `${('0' + dt.getDate()).slice(-2)}/${('0' + (dt.getMonth() + 1)).slice(
          -2,
        )}/${dt.getFullYear()}`) ||
      date
    );
  };

  moreButtonCell = id => (
    <View style={styles.btnBlock}>
      <Button title="More" onPress={() => this.navigateToItemWiseReport(id)} />
    </View>
  );

  navigateToItemWiseReport = InvoiceID => {
    let {VendorType} = this;
    this.props.navigation.navigate(`${VendorType}HistoryItemWiseDetailNav`, {
      InvoiceID,
      VendorType: this.VendorType,
    });
  };

  headers = [
    ['Entry No', 'Entry Date'],
    ['Doc No', 'Doc Date'],
    ['Purchase', 'Sales'],
    ['Adjusted', 'On Hand'],
    ['Sales %', 'ROI'],
    ['Amount', 'Paid'],
    ['Balance'],
    ['Details'],
  ];
  // widthArr = [100,100,100,100,100,100,100,100];

  render() {
    console.log('data : ', this.state.data);
    if (!this.state.isFetched) {
      return <ICubeAIndicator />;
    }

    return (
      <>
        <SafeAreaView style={styles.container}>
          <ScrollView horizontal style={styles.ScrollView}>
            <View>
              <Table borderStyle={{borderWidth: 1, borderColor: '#B5B5B5'}}>
                <TableWrapper
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    backgroundColor: '#E9E9E9',
                  }}>
                  {this.headers.map(([top, bottom], ind) => (
                    <Cell
                      key={ind}
                      data={this.combineHeaderCells(top, bottom)}
                      style={{width: '0,1'.includes(ind) ? 85 : 70}}
                    />
                  ))}
                </TableWrapper>
              </Table>
              <ScrollView>
                <Table borderStyle={{borderWidth: 1, borderColor: '#B5B5B5'}}>
                  <TableWrapper
                    style={{width: '100%', backgroundColor: '#F8F8F8'}}>
                    {this.state.data.map((item, ind) => (
                      <TableWrapper
                        key={ind}
                        style={{width: '100%', flexDirection: 'row'}}>
                        <Cell
                          data={this.combineRowCells(
                            item.EntryNo,
                            this.formatDateValue(item.EntryDate),
                          )}
                          style={{width: 85}}
                        />
                        <Cell
                          data={this.combineRowCells(
                            item.DocNo,
                            this.formatDateValue(item.DocDate),
                          )}
                          style={{width: 85}}
                        />
                        <Cell
                          data={this.combineRowCells(
                            item.PurchaseQty,
                            item.SalesQty,
                          )}
                          style={styles.dataBlock}
                        />
                        <Cell
                          data={this.combineRowCells(
                            item.AdjustedQty,
                            item.OnHandQty,
                          )}
                          style={styles.dataBlock}
                        />
                        <Cell
                          data={this.combineRowCells(item.SalesPer, item.ROI)}
                          style={styles.dataBlock}
                        />
                        <Cell
                          data={this.combineRowCells(
                            item.NetAmount,
                            item.PaidAmount,
                          )}
                          style={styles.dataBlock}
                        />
                        <Cell
                          data={this.combineRowCells(item.Balance)}
                          style={styles.dataBlock}
                        />
                        <Cell
                          data={this.moreButtonCell(item.BillID)}
                          style={styles.dataBlock}
                        />
                      </TableWrapper>
                    ))}
                  </TableWrapper>
                </Table>
              </ScrollView>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  ScrollView: {
    flex: 1,
    margin: 20,
    // borderWidth: 1,
    // borderColor: '#B5B5B5',
  },
  headerText: {
    padding: 10,
  },
  headerCol: {
    fontSize: 14,
    paddingVertical: 5,
    paddingLeft: 5,
    color: '#000',
    textAlign: 'left',
  },
  headerBottom: {
    borderTopWidth: 1,
    borderTopColor: '#CACACA',
  },
  rowCol: {
    fontSize: 13.5,
    paddingVertical: 5,
    paddingLeft: 5,
    color: '#4B4B4B',
    textAlign: 'left',
  },
  rowBottom: {
    borderTopWidth: 1,
    borderTopColor: '#D9D9D9',
  },
  dataBlock: {
    width: 70,
  },
  btnBlock: {
    paddingHorizontal: 5,
    transform: [{scale: 0.8}],
  },
});