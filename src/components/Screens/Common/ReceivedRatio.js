import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import ModalDatePicker from '../../Custom/ModalDatePicker';

import {AlertMessage, Confirmation} from '../../../Helpers/HelperMethods';

const RRListItem = props => {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <View style={{width: '30%'}}>
          <Tooltip
            animated={true}
            //(Optional) When true, tooltip will animate in/out when showing/hiding
            arrowSize={{width: 16, height: 8}}
            //(Optional) Dimensions of arrow bubble pointing to the highlighted element
            backgroundColor="rgba(0,0,0,0.5)"
            //(Optional) Color of the fullscreen background beneath the tooltip.
            isVisible={props.index === props.Fromerror.index}
            //(Must) When true, tooltip is displayed
            content={
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../assets/images/error.png')}
                  style={{width: 25, height: 25}}
                />
                <Text>{props.Fromerror.message}</Text>
              </View>
            }
            //(Must) This is the view displayed in the tooltip
            placement="bottom"
            //(Must) top, bottom, left, right, auto.
            onClose={props.UpdateFromToolTip}
            //(Optional) Callback fired when the user taps the tooltip
          >
            <ModalDatePicker
              placeholder={'From'}
              fieldWrapperStyle={{width: '100%'}}
              selectedValue={props.data.From}
              onValueSelected={selectedDate =>
                props.onFromDateChange(props.index, selectedDate)
              }
            />
            {/* <DatePicker
                            style={{ width: '100%', fontSize: 16, }}
                            format="YYYY-MM-DD"
                            mode="date"
                            date={props.data.From.toString()}
                            onDateChange={From => { props.onFromDateChange(props.index, From) }}
                            confirmBtnText="ok"
                            cancelBtnText="cancel"
                            customStyles={{
                                dateInput: {
                                    borderColor: 'transparent',
                                },
                                dateText: {
                                    alignSelf: 'flex-start',
                                },
                                placeholderText: {
                                    alignSelf: 'flex-start',
                                    color: 'black',
                                }
                            }}
                            // showIcon={false}
                            placeholder=" "
                        /> */}
          </Tooltip>
        </View>
        <View style={{width: '30%'}}>
          <Tooltip
            animated={true}
            //(Optional) When true, tooltip will animate in/out when showing/hiding
            arrowSize={{width: 16, height: 8}}
            //(Optional) Dimensions of arrow bubble pointing to the highlighted element
            backgroundColor="rgba(0,0,0,0.5)"
            //(Optional) Color of the fullscreen background beneath the tooltip.
            isVisible={props.index === props.Toerror.index}
            //(Must) When true, tooltip is displayed
            content={
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../assets/images/error.png')}
                  style={{width: 25, height: 25}}
                />
                <Text>{props.Toerror.message}</Text>
              </View>
            }
            //(Must) This is the view displayed in the tooltip
            placement="bottom"
            //(Must) top, bottom, left, right, auto.
            onClose={props.UpdateToToolTip}
            //(Optional) Callback fired when the user taps the tooltip
          >
              
            <ModalDatePicker
              placeholder={'To'}
              fieldWrapperStyle={{width: '100%'}}
              selectedValue={props.data.To}
              onValueSelected={selectedDate =>
                props.onToDateChange(props.index, selectedDate)
              }
            />
            {/* <DatePicker
              style={{width: '100%', fontSize: 16}}
              format="YYYY-MM-DD"
              mode="date"
              date={props.data.To.toString()}
              onDateChange={To => {
                props.onToDateChange(props.index, To);
              }}
              confirmBtnText="ok"
              cancelBtnText="cancel"
              customStyles={{
                dateInput: {
                  borderColor: 'transparent',
                },
                dateText: {
                  alignSelf: 'flex-start',
                },
                placeholderText: {
                  alignSelf: 'flex-start',
                  color: 'black',
                },
              }}
              // showIcon={false}
              placeholder=" "
            /> */}
          </Tooltip>
        </View>
        <View style={{width: '15%'}}>
          <TextInput
            style={{
              height: 40,
              width: '100%',
              color: 'black',
              textAlign: 'center',
              fontSize: 16,
            }}
            keyboardType="numeric"
            onChangeText={Ratio => {
              props.onRatioChange(props.index, Ratio);
            }}
            value={props.data.Ratio.toString()}
          />
        </View>
        <View style={{width: '15%'}}>
          <TextInput
            style={{
              height: 40,
              width: '100%',
              color: 'black',
              textAlign: 'center',
              fontSize: 16,
            }}
            keyboardType="numeric"
            onChangeText={Qty => {
              props.onQtyChange(props.index, Qty);
            }}
            value={props.data.Qty.toString()}
          />
        </View>

        <View style={{alignItems: 'center', marginTop: '2%'}}>
          <TouchableOpacity onPress={() => props.onPress()}>
            <Image
              source={require('../../../assets/images/Delete.png')}
              style={{width: 25, height: 25, marginLeft: 10}}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default class ReceivedRatio extends Component {
  constructor() {
    super();
    this.state = {
      isFetched: false,
      isRefresh: false,
      Type: 'Sales',

      FromtoolTip: -1,
      FromtoolError: '',
      TotoolTip: -1,
      TotoolError: '',

      ValidFrom: '',
      ValidTo: '',
      TotalQty: 0,
      TotalRatio: 0,
      NoDataAvailable: false,
      RRatio: [],
    };
  }
  LoadInitialData = async () => {
    let params = this.props.route.params;
    let ERR = {From: '', ID: 0, Qty: '', Ratio: '', To: ''};
    console.log((params.RRatio || []).concat(ERR));
    let RR = (params.RRatio || []).concat(ERR);
    this.setState({
      RRatio: RR,
      TotalQty: params.TQty,
      ValidFrom: params.ValidFrom,
      ValidTo: params.ValidTo,
    });
  };

  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };
  NavigateBackToAddSales = () => {
    console.log(this.state.TotalQty);
    let Ln = this.state.RRatio.length;
    if (this.state.RRatio[Ln - 1].From === '') {
      this.state.RRatio.splice(Ln - 1, 1);
      this.forceUpdate();
    }
    // console.log(this.state.TotalQty.toFixed(2));
    if (
      this.state.TotalQty !==
      this.state.RRatio.reduce(
        (accum, item) => accum + Number(item.Qty),
        0,
      ).toFixed(2)
    ) {
      AlertMessage('Qty Should be Equal To Total Qty');
      return;
    }

    this.props.navigation.goBack();
    this.props.route.params.onFinishCreation(this.state.RRatio);
  };

  componentDidMount() {
    this.LoadInitialData();
  }
  UpdateQuantity = (index, qty) => {
    console.log(index, qty);
    this.state.RRatio[index].Qty = qty;
    this.state.RRatio[index].Ratio = ((qty / 14) * 100).toFixed(2);
    this.forceUpdate();
  };
  UpdateRatio = (index, Ratio) => {
    console.log((Ratio / 100) * 14);
    let TT = 14;

    this.state.RRatio[index].Ratio = Ratio;
    this.state.RRatio[index].Qty = ((Ratio / 100) * 14).toFixed(2);
    this.forceUpdate();
  };
  UpdateFromDate = (index, From) => {
    if (From === '') {
      this.setState({
        FromtoolTip: index,
        FromtoolError: 'Value Cannot Be Empty',
      });
    } else if (From !== '') {
      if (index === 0 && this.state.ValidFrom > From) {
        this.setState({
          FromtoolTip: index,
          FromtoolError: 'From date didnt less than the valid from date ',
        });
      } else {
        let Todate = this.state.RRatio[index].To;
        if (Todate !== '') {
          if (Todate < From) {
            this.setState({
              FromtoolTip: index,
              FromtoolError: 'From date didnt greater than the valid from date',
            });
          }
        }
        if (index > 0) {
          let preToDate = this.state.RRatio[index - 1].To;
          if (preToDate !== '') {
            if (From <= preToDate) {
              this.setState({
                FromtoolTip: index,
                FromtoolError:
                  'From date didnt less than the previous valid to date',
              });
            }
          }
        }
      }
    }

    this.state.RRatio[index].From = From;
    this.forceUpdate();
  };
  UpdateToDate = (index, To) => {
    if (To === '') {
      this.setState({TotoolTip: index, TotoolError: 'Enter To Date'});
    } else if (To !== '') {
      if (this.state.ValidTo < To) {
        this.setState({
          TotoolTip: index,
          TotoolError: 'To date didnt greater than the valid to date',
        });
      } else {
        let fromDate = this.state.RRatio[index].From;
        if (fromDate > To) {
          this.setState({
            TotoolTip: index,
            TotoolError: 'From date didnt greater than the to date',
          });
        }
      }
    }
    this.state.RRatio[index].To = To;
    this.forceUpdate();
  };
  UpdateFromToolTip = () => {
    this.setState({FromtoolTip: -1});
  };
  UpdateToToolTip = () => {
    this.setState({TotoolTip: -1});
  };
  AddRow = () => {
    let ERR = {From: '', ID: 0, Qty: '', Ratio: '', To: ''};
    let RR = (this.state.RRatio || []).concat(ERR);
    this.setState({RRatio: RR});
    console.log(this.state.RRatio);
  };
  DeleteBarcode = index => {
    Confirmation('Are You Sure You Want To Delete', () => {
      this.state.RRatio.splice(index, 1);
      this.forceUpdate();
    });
  };

  render() {
    return (
      <ScrollView>
        <View
          style={{
            alignItems: 'flex-end',
            marginRight: 10,
            marginTop: 10,
            marginBottom: 10,
          }}>
          <TouchableOpacity
            style={{
              width: 100,
              flexDirection: 'row',
              backgroundColor: 'blue',
              padding: 5,
              borderRadius: 10,
              justifyContent: 'space-evenly',
            }}
            onPress={this.AddRow}>
            <Image
              source={require('../../../assets/images/plus-light.png')}
              style={{width: 25, height: 25}}
            />
            <Text
              numberOfLines={1}
              style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
              Add
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.HeaderContainer}>
          <Text numberOfLines={1} style={[styles.HeaderText, {width: '30%'}]}>
            From
          </Text>
          <Text numberOfLines={1} style={[styles.HeaderText, {width: '30%'}]}>
            To
          </Text>
          <Text numberOfLines={1} style={[styles.HeaderText, {width: '16%'}]}>
            Ratio
          </Text>
          <Text numberOfLines={1} style={[styles.HeaderText, {width: '16%'}]}>
            Qty
          </Text>
          <Image
            source={require('../../../assets/images/Delete.png')}
            style={{width: 25, height: 25, marginTop: 11}}
          />
        </View>

        <ScrollView>
          <FlatList
            style={styles.FlatList}
            data={this.state.RRatio}
            renderItem={({item, index, date, name}) => (
              <RRListItem
                data={item}
                index={index}
                onQtyChange={this.UpdateQuantity}
                onPress={this.DeleteBarcode.bind(this, index)}
                onRatioChange={this.UpdateRatio}
                onFromDateChange={this.UpdateFromDate}
                onToDateChange={this.UpdateToDate}
                UpdateFromToolTip={this.UpdateFromToolTip}
                Fromerror={{
                  index: this.state.FromtoolTip,
                  message: this.state.FromtoolError,
                }}
                UpdateToToolTip={this.UpdateToToolTip}
                Toerror={{
                  index: this.state.TotoolTip,
                  message: this.state.TotoolError,
                }}
                onDateChange={this.HandleDateChange.bind(name, date)}
              />
            )}
            // keyExtractor={item => item.Barcode.toString()}
          />
        </ScrollView>

        <View style={{backgroundColor: 'rgba(0,0,0,0.1)', padding: 10}}>
          <View style={styles.TotalContainer}>
            <View style={{marginLeft: '10%'}}>
              <Text numberOfLines={1} style={styles.TotalText}>
                {' '}
                {Math.round(
                  this.state.RRatio.reduce(
                    (accum, item) => accum + Number(item.Ratio),
                    0,
                  ).toFixed(2),
                )}
              </Text>
            </View>
            <View style={{marginLeft: '10%'}}>
              <Text numberOfLines={1} style={styles.TotalText}>
                {this.state.RRatio.reduce(
                  (accum, item) => accum + Number(item.Qty),
                  0,
                ).toFixed(2)}
              </Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              alignItems: 'flex-end',
              marginRight: 10,
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              style={{
                width: 100,
                flexDirection: 'row',
                backgroundColor: 'blue',
                padding: 5,
                borderRadius: 10,
                justifyContent: 'space-evenly',
              }}
              onPress={this.NavigateBackToAddSales}>
              <Image
                source={require('../../../assets/images/Save.png')}
                style={{width: 25, height: 25}}
              />
              <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
                Save
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              alignItems: 'flex-end',
              marginRight: 10,
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              style={{
                width: 100,
                flexDirection: 'row',
                backgroundColor: 'blue',
                padding: 5,
                borderRadius: 10,
                justifyContent: 'space-evenly',
              }}
              onPress={() => {
                this.setChargesVisible(!this.state.isCharges);
              }}>
              <Image
                source={require('../../../assets/images/Cancel.png')}
                style={{width: 25, height: 25}}
              />
              <Text
                numberOfLines={1}
                style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  HeaderContainer: {
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  TotalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '60%',
    marginRight: '10%',
  },
  TotalText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  HeaderText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    padding: 10,
    // borderLeftWidth:1,
    // borderLeftColor:'black',
  },
  FlatList: {
    paddingVertical: 10,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 50,
  },
});
