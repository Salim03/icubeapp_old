import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  StatusBar,
} from 'react-native';
import CheckInput from '../../Custom/CheckInput';
// Local Imports
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {FloatingAction} from 'react-native-floating-action';
import {
  GetSessionData,
  ReturnDisplayStringForDate,
  Confirmation,
  isnull,
  AlertStatusError,
  AlertError,
  AlertMessage,
  Request,
  IsServerConnected,
} from '../../../Helpers/HelperMethods';
import LayoutWrapper, {CardStyle} from '../../Layout/Layout';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';

import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../../Style/GlobalStyle';

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View style={[styles.CardTextContainer, {width: '70%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            From [ To ]
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            Package
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            PTF
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            STF
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '20%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),

              {textAlign: 'right'},
            ]}>
            Quantity
          </Text>
        </View>
      </View>
    </View>
  );
};

const ListItem = props => {
  const data = props.data;
  let InvoiceDate = ReturnDisplayStringForDate(data.PTFDate);

  return (
    <TouchableWithoutFeedback 
    // onLongPress={() => props.onLongPress(data)}
    >
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: '80%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.TransferDetails}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {data.PackageNo}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.PTFNo} {InvoiceDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.STFNo}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '20%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),

                {textAlign: 'right'},
              ]}>
              {(Math.round(data.Qty * 100) / 100).toFixed(2)}
            </Text>
          </View>
        </View>

        <View
          style={[
            {
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              backgroundColor: globalColorObject.Color.Lightprimary,
            },
          ]}>
          <View
            style={[
              {
                width: 0,
                height: 0,
                backgroundColor: globalColorObject.Color.Lightprimary,
                borderRightWidth: 20,
                borderTopWidth: 20,
                borderRightColor: globalColorObject.Color.Lightprimary,
                borderTopColor: globalColorObject.Color.oppPrimary,
              },
              {
                transform: [{rotate: '90deg'}],
              },
            ]}></View>
          <TouchableOpacity
            // onPress={() => props.onSyncInvoice([data])}
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}>
            <Image
              style={[styles.ActionImage]}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1059/1059106.png',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const Reconsilation = props => {
  const [LocalData, setLocalData] = useState({
    Loccode: 0,
    RoleId: '',
    UserCode: '',
    IP: '',
    Token: '',
  });

  const [state, setState] = useState({
    ModalMessage: 'Loading',
    isFetched: false,
    SelectedStatus: 'All',
    SearchText: '',
    show: false,
    POList: [],
    StatusGroupData: [],
    ListSourceData: [],
  });

  const ActionButtons = () => {
    let retArray = [
      {
        text: 'New',
        icon: require('../../../assets/images/add-light.png'),
        name: 'New',
        color: globalColorObject.Color.Primary,
        buttonSize: 45,
        position: 3,
      },
    ];
    return retArray;
  };

  useEffect(() => {
    LoadInitialData();
  }, []);

  useEffect(() => {
    LoadAllInvoice();
  }, [LoadAllInvoice, LocalData]);


  const LoadAllInvoice = useCallback(() => {
    if (isnull(LocalData.IP, '') != '') {
      Request.get(
        `${LocalData.IP}/api/Distribution/Package/GetAllReconciliation`,
        LocalData.Token,
      )
        .then(res => {
          if (res.status === 200) {
            if (res.data.length) {
              let GetresData = res.data;
              let GroupStatusOut = [];
              let groupData = {};
              GetresData.reduce((r, o) => {
                let key = o.Sender;
                if (!groupData[key]) {
                  groupData[key] = Object.assign(
                    {},
                    {
                      Sender: o.Sender,
                      Count: 1,
                    },
                  ); // create a copy of o
                  // r.push(groupData[key]);
                } else {
                  groupData[key].Count += 1;
                }
              }, []);
              let TotalCount = GetresData.length;
              GroupStatusOut.push(
                Object.assign(
                  {},
                  {
                    Sender: 'All'.toString(),
                    Count: TotalCount,
                    Label: 'All' + ` (${TotalCount})`,
                  },
                ),
              );
              Object.keys(groupData).map(key => {
                // console.log(key, groupData[key], groupData[key].Count);
                GroupStatusOut.push(
                  Object.assign(
                    {},
                    {
                      Sender: key.toString(),
                      Count: groupData[key].Count,
                      Label: key + ` (${groupData[key].Count})`,
                    },
                  ),
                );
              });
              setState(prevstate => ({
                ...prevstate,
                isFetched: true,
                POList: GetresData,
                ListSourceData: GetresData,
                StatusGroupData: GroupStatusOut,
                SelectedStatus: 'All',
              }));
            } else {
              setState(prevstate => ({
                ...prevstate,
                isFetched: true,
                NoDataAvailable: true,
              }));
            }
          } else {
            AlertStatusError(res);
            setState(prevstate => ({...prevstate, isFetched: true}));
          }
        })
        .catch(
          err =>
            AlertError(err) &&
            setState(prevstate => ({...prevstate, isFetched: true})),
        );
    }
  }, [LocalData.IP, LocalData.Token]);

  const LoadInitialData = async () => {
    const {get_UserCode, get_Location, get_RoleId, get_Token, get_IP} =
      await GetSessionData();

    setLocalData({
      Loccode: get_Location,
      RoleId: get_RoleId,
      UserCode: get_UserCode,
      IP: get_IP,
      Token: get_Token,
    });

    // setState(prevstate => ({
    //   ...prevstate,
    //   isFetched: true,
    // }));
  };

  const HandleSearch = async (SearchText, getPOList) => {
    let POList = getPOList ? getPOList : state.POList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = [];
    if (isnull(txt, '') != '') {
      ListSourceData = POList.filter(obj => {
        // Get all the texts

        let InvoiceDate = ReturnDisplayStringForDate(obj.PackageDate);
        // let DocDate = obj.STFDate
        //   ? ReturnDisplayStringForDate(obj.STFDate)
        //   : '';
        let Status = (obj.Status || '').toString().toLowerCase();
        let InvoiceNo = (obj.PackageNo || '').toString().toLowerCase();
        let VENDORNAME = (obj.TransferDetails || '').toString().toLowerCase();
        let DOCNO = (obj.STFNo || '').toString().toLowerCase();
        let Amount = (Math.round(obj.Qty * 100) / 100).toFixed(2).toString();

        return (
          InvoiceDate.includes(txt) ||
          // DocDate.includes(txt) ||
          InvoiceNo.includes(txt) ||
          Status.includes(txt) ||
          VENDORNAME.includes(txt) ||
          DOCNO.includes(txt) ||
          Amount.includes(txt)
        );
      });
    } else {
      ListSourceData = [...POList];
    }
    setState(prevstate => ({
      ...prevstate,
      SearchText: SearchText,
      POList,
      ListSourceData,
    }));
  };

  const showCancel = () => {
    setState(prevstate => ({
      ...prevstate,
      show: true,
    }));
  };

  const hideCancel = () => {
    setState(prevstate => ({
      ...prevstate,
      show: false,
    }));
  };

  const NavigateToPO = async EditData => {
    props.navigation.navigate('AddReconsilationNav', {
      CallFrom: 'PTF',
      HeaderTitle: 'Add Reconsilation',
      EditData: EditData || {},
      LocalData: LocalData,
      onUpdateInvoice: LoadAllInvoice.bind(this),
    });
  };

  const HandleActionButtonClick = name => {
    if (name === 'New') {
      NavigateToPO();
    }
  };

  const refreshLayout = () => {
    /*this.forceUpdate();*/
  };
  // console.log('log state : ', state.POList);
  return !state.isFetched ? (
    <ICubeAIndicator ModalText={state.ModalMessage} />
  ) : (
    <>
      <StatusBar
        backgroundColor={globalColorObject.Color.OffLineStatus}
        barStyle="default"
      />
      <LayoutWrapper
        onLayoutChanged={refreshLayout}
        backgroundColor={globalColorObject.Color.Lightprimary}>
        <View
          style={[
            styles.SearhBarWrapper,
            {
              width: '100%',
              flexDirection: 'row',
            },
          ]}>
          <TextInput
            style={[
              styles.SearchInput,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderColor,
              ),
              {width: '70%'},
            ]}
            placeholder={'Search here'}
            value={state.SearchText}
            onChangeText={HandleSearch}
            autoCorrect={false}
            returnKeyType="search"
            autoCapitalize="none"
            onFocus={showCancel}
            onBlur={hideCancel}
          />

          <View style={{width: '30%'}}>
            <ModalSearchableLabel
              placeholder="Sender"
              fieldWrapperStyle={{height: 30, width: '100%'}}
              data={state.StatusGroupData}
              labelProp="Label"
              valueProp="Sender"
              selectedValue={state.SelectedStatus}
              inputStyle={[
                {
                  paddingLeft: 5,
                  marginTop: 10,
                  marginBottom: 0,
                  borderBottomWidth: 0,
                  textAlign: 'center',
                  textDecorationLine: 'underline',
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderBottomColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
              onValueSelected={SelectedStatus => {
                setState(prevstate => ({
                  ...prevstate,
                  SelectedStatus,
                }));
                HandleSearch(SelectedStatus != 'All' ? SelectedStatus : '');
              }}
            />
          </View>
        </View>
        {state.ListSourceData?.length > 0 && (
          <View style={styles.NoDataBanner}>
            <Text
              style={{
                fontSize: 25,
                color: 'rgba(0,0,0,0.2)',
                textAlign: 'center',
                marginTop: 20,
              }}>
              No Data Found
            </Text>
            <Text
              style={{
                fontSize: 18,
                color: 'rgba(0,0,0,0.2)',
                textAlign: 'center',
                marginTop: 5,
              }}>
              Pull to refresh
            </Text>
          </View>
        )}
        <FlatList
          style={[
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}
          data={state.ListSourceData}
          renderItem={({item, index}) => (
            <ListItem
              data={item}
              onLongPress={NavigateToPO}
              index={index}
            />
          )}
          keyExtractor={item => item.PTFNo.toString()}
          refreshing={false}
          onRefresh={LoadAllInvoice.bind(this)}
          ListHeaderComponent={<ListItemHeader />}
          stickyHeaderIndices={[0]}
          // to have some breathing space on bottom
          ListFooterComponent={() => (
            <View style={{width: '100%', marginTop: 85}} />
          )}
        />
        <FloatingAction
          actions={ActionButtons()}
          floatingIcon={require('../../../assets/images/undo-light.png')}
          onPressItem={HandleActionButtonClick}
          color={globalColorObject.Color.Primary}
          iconWidth={30}
          iconHeight={30}
          onRefresh={() => {}}
          animated={true}
          visible={state.ActivaTab != 'Profile'}
          distanceToEdge={{vertical: 30, horizontal: 30}}
        />
      </LayoutWrapper>
      <ICubeAIndicator isShow={!state.isFetched} />
    </>
  );
};

const styles = StyleSheet.create({
  SearhBarWrapper: {
    padding: 10,
    // backgroundColor: '#ededed',
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
    top: 70,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderRadius: 5,
    // paddingTop: 15,
    // paddingBottom: 7,
    // // borderWidth: 1,
    // borderBottomWidth: 0,
    // borderLeftWidth: 0,
    // borderColor: 'lightgray',
    marginTop: 5,
    marginHorizontal: 9,
    // elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  removeItemIconContainer: {
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },
  removeItemIcon: {
    width: 20,
    height: 20,
    margin: 2,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  closeButton: {
    height: 30,
    width: 30,
    // paddingLeft:30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 12,
    top: 14,
  },

  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});

export default Reconsilation;
