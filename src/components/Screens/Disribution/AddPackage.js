/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {useEffect, useState, useLayoutEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  Keyboard,
  PermissionsAndroid,
  BackHandler,
  TextInput,
  StatusBar,
  Platform,
  Modal,
} from 'react-native';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {CameraScreen} from 'react-native-camera-kit';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import Menu, {MenuItem} from 'react-native-material-menu';
// Local Imports
import RichTextInput from '../../Custom/RichTextInput';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import {
  Confirmation,
  AlertMessage,
  isnull,
  nullif_Number,
  RoundValue,
  AlertStatusError,
  AlertError,
  Request,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';

const ListItemHeader = React.memo(({headers}) => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Barcode
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '68%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Item Name
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            CardItemLayout,
            {alignItems: 'flex-end', width: '25%'},
          ]}>
          <Text
            numberOfLines={1}
            style={ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            )}>
            Quantity
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            CardItemLayout,
            {alignItems: 'flex-end', width: '28%'},
          ]}>
          <Text
            numberOfLines={1}
            style={ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            )}>
            Rate
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            CardItemLayout,
            {alignItems: 'flex-end', width: '40%'},
          ]}>
          <Text
            numberOfLines={1}
            style={ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            )}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
});

const ListItem = props => {
  let data = props.data;
  let index = props.index;
  let isAgainst = props.isAgainst;
  const {CardItemLayout} = CardStyle;
  return (
    <TouchableWithoutFeedback
      onLongPress={
        !isAgainst ? props.HandleRemoveProduct.bind(this, index) : () => {}
      }>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: '30%'}]}>
            <Text
              numberOfLines={2}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Barcode}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '68%'}]}>
            <Text
              numberOfLines={2}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Description}
            </Text>
          </View>

          <View
            style={[styles.CardTextContainer, CardItemLayout, {width: '25%'}]}>
            <RichTextInput
              isShowAnimateText={false}
              placeholder=""
              value={isnull(nullif_Number(data.TransferQty, 0), '')}
              wrapperStyle={{width: '100%', marginVertical: 0}}
              inputStyle={[
                {marginTop: 0, textAlign: 'right', width: '100%'},
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
              onChangeText={Qty => {
                props.HandleQtyChange(index, Qty);
              }}
              inputProps={{
                keyboardType: 'phone-pad',
              }}
            />
            {/* <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {data.TransferQty}
            </Text> */}
          </View>

          <View
            style={[
              styles.CardTextContainer,
              CardItemLayout,
              {justifyContent: 'flex-end', width: '28%'},
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {
                  textAlign: 'right',
                  paddingBottom: 8,
                },
              ]}>
              {data.TransferRate}
            </Text>
          </View>

          <View
            style={[
              styles.CardTextContainer,
              CardItemLayout,
              {justifyContent: 'flex-end', width: '40%'},
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {
                  textAlign: 'right',
                  paddingBottom: 8,
                },
              ]}>
              {data.TransferQty * data.TransferRate}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const DBDupListItemHeader = () => {
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Barcode
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '70%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Description
          </Text>
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.CardTextContainer, {width: '45%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            AvailableQty
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {width: '45%', alignItems: 'center'},
          ]}>
          <Text
            numberOfLines={1}
            style={[
              styles.cardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            MRP{' '}
          </Text>
        </View>
      </View>
    </View>
  );
};

const DBDupListItem = props => {
  // console.log('Db Barcode Dup', props.data);
  return (
    <TouchableOpacity onPress={props.onLongPress}>
      <View style={styles.Card}>
        <View style={styles.CardLine}>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.CardTextContainer, {width: '30%'}]}>
              <Text
                numberOfLines={1}
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                {props.data.Icode}
              </Text>
            </View>
            <View style={[styles.CardTextContainer, {width: '70%'}]}>
              <Text
                numberOfLines={1}
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                {props.data.Description}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.CardTextContainer, {width: '45%'}]}>
              <Text
                numberOfLines={1}
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                {props.data.Stock}
              </Text>
            </View>
            <View
              style={[
                styles.CardTextContainer,
                {width: '45%', alignItems: 'center'},
              ]}>
              <Text
                numberOfLines={1}
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                {props.data.MRP}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const AddPackage = ({navigation, route}) => {
  let ScanBarcodeRef = useRef(null);
  // Declaring class variables
  let visibleCreatedItemsGroupFields = [
    'TransferQty',
    'TransferRate',
    'Amount',
  ];
  let visibleCreatedItemsGroupCaptions = ['Quantity', 'Rate', 'Amount'];

  const menurefList = {};
  const setMenuRef = (ref, index) => {
    menurefList[`_menu${index}`] = ref;
  };

  const onOpenScanner = () => {
    //To Start Scanning
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'iCube App Camera Permission',
              message: 'iCube App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            setState(prevstate => ({
              ...prevstate,
              ScanBarcode: '',
              openScanner: true,
            }));
            // that.setState({ openScanner: true });
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      setState(prevstate => ({
        ...prevstate,
        ScanBarcode: '',
        openScanner: true,
      }));
    }
  };

  const [state, setState] = useState({
    ModalMessage: 'Saving Transaction',
    isFetched: false,
    Barcode: '',
    EditInvoice: false,
    InvoiceNo: '',
    InvoiceDate: new Date(),
    Status: 'Pending',
    // Holds the data to be set to controls ( filled later by the data from API )
    LocationList: [],
    StockList: [],
    TypeList: [
      {Code: 'Package Creation', Name: 'Package'},
      {Code: 'Against Requisition', Name: 'Requisition'},
      {Code: 'Goods Received Note', Name: 'Goods Received'},
      {Code: 'Delivery Challan', Name: 'Delivery Challan'},
    ],
    AgainstList: [],
    // Hold selected or entered values of controls
    Stock: 0,
    Destination: 0,
    Type: 'Package Creation',
    AgainstInfo: '',
    Remarks: '',
    BasicAmount: 0,
    TotalQty: 0,
    // holds CreatedItems Array
    CreatedItems: [],
    isDubBarcode: false,
    DubBarcodeList: [],
    openScanner: false,
    ProductSearchText: '',
  });

  const [LocalData, setLocalData] = useState({
    Loccode: 0,
    RoleId: '',
    UserCode: '',
    IP: '',
    Token: '',
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () =>
        state.Status == 'Pending' && (
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 5}}
              onPress={ValidateInputs.bind(this, true, false, false)}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/save-light.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 5}}
              onPress={ValidateInputs.bind(this, false, true, false)}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/logout-white.png')}
              />
            </TouchableOpacity>
          </View>
        ),
    });
  }, [navigation, state, LocalData]);

  useEffect(() => {
    // console.log('init load');
    // Adding a event listener for hardware back press
    AddBackButtonListener();
    LoadSelectedInvoice();
    return () => {
      RemoveBackButtonListener();
    };
  }, []);

  useEffect(() => {
    // console.log('update LocalData');
    LoadDefaultData(true);
  }, [LocalData]);

  const LoadDefaultData = async isApplyDefaultStock => {
    // console.log('call default load', LocalData);
    if (isnull(LocalData.IP, '') != '') {
      Request.get(
        `${LocalData.IP}/api/Purchase/GetAllLocation?CallFrom=PC`,
        LocalData.Token,
      )
        .then(res => {
          if (res.data.length) {
            let GetresData = (res.data || []).filter(
              location => location.LocationCode != LocalData.Loccode,
            );
            setState(prevstate => ({
              ...prevstate,
              LocationList: GetresData,
            }));
          }
        })
        .catch(err => console.error('Location Fetch', err));
      Request.get(
        `${LocalData.IP}/api/Common/GetStockPointList?LocationCode=${LocalData.Loccode}`,
        LocalData.Token,
      )
        .then(res => {
          if (res.data.length) {
            let GetresData = (res.data || []).sort(
              (a, b) => Number(b.DEFAULT) - Number(a.DEFAULT),
            );
            setState(prevstate => ({
              ...prevstate,
              StockList: res.data,
              Stock:
                isApplyDefaultStock && GetresData.length > 0
                  ? Number(GetresData[0].STKPTCODE)
                  : prevstate.Stock,
            }));
          }
        })
        .catch(err => console.error('stock Fetch', err));
    }
  };

  const GetAgainstInfo = AgainstType => {
    // console.log(
    //   'call api : ',
    //   `${LocalData.IP}/api/Distribution/Package/GetDocumentTypeDropDownSource?Type=${AgainstType}&DestinationCode=${state.Destination}&Loccode=${LocalData.Loccode}&Stockcode=${state.Stock}`,
    // );
    // console.log('call against bind api', AgainstType);
    Request.get(
      `${LocalData.IP}/api/Distribution/Package/GetDocumentTypeDropDownSource?Type=${AgainstType}&DestinationCode=${state.Destination}&Loccode=${LocalData.Loccode}&Stockcode=${state.Stock}`,
      LocalData.Token,
    )
      .then(res => {
        let get_Against = res.data?.length > 0 ? res.data : [];
        // console.log('bind against list : ', get_Against);
        setState(prevstate => ({
          ...prevstate,
          AgainstList: get_Against,
        }));
      })
      .catch(err => console.error('GetSelectedAgainstInfo : ', err));
  };

  const LoadSelectedInvoice = async () => {
    // fetching selected Invoice data
    const {params} = route;
    let EditData = params.EditData || {};
    setLocalData({...params.LocalData});
    if (EditData && EditData.PackageNo && EditData.PackageNo != '') {
      // Setting Local Data
      let get_EditInvoice = true;
      let get_InvoiceDate = new Date(EditData.PackageDate);
      let get_InvoiceNo = EditData.PackageNo;
      // console.log('edit data : ', EditData);
      let getItemDetails = [];

     let Return_data = await Request.get(
        `${params.LocalData.IP}/api/Distribution/Package/GetSelectedPackageDetailsInfo?PackageNo=${get_InvoiceNo}`,
        params.LocalData.Token,
      )
        .then(res => {
          return res.data?.length > 0 ? res.data : [];
        })
        .catch(err => console.error('GetSelectedAgainstDetailsInfo : ', err));

        Return_data = Return_data || [];
        // console.log('edit data details  : ', Return_data);
      // Setting state
      UpdateAmountCalculation(Return_data);
      setState(prevstate => ({
        ...prevstate,
        isFetched: true,
        EditInvoice: get_EditInvoice,
        InvoiceNo: get_InvoiceNo,
        InvoiceDate: get_InvoiceDate,
        Destination: EditData.DestinationCode || '0',
        Type: EditData.Type,
        AgainstInfo: isnull(EditData.AgainstNo,''),
        Remarks: EditData.Remarks,
        Status: Return_data.length > 0 ? Return_data[0]['Status'] : EditData.Status,
        BasicAmount: EditData.Amount,
        TotalQty: EditData.Qty,
        CreatedItems: Return_data,
      }));
    } else {
      setState(prevstate => ({
        ...prevstate,
        isFetched: true,
      }));
    }
  };

  const AddBackButtonListener = () => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
  };

  const RemoveBackButtonListener = () => {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
  };

  const handleBackButtonClick = async () => {
    await Confirmation('Do you want to go back ?', () => {
      RemoveBackButtonListener();
      navigation.goBack();
    });
    return false;
  };

  const HandleDestinationChange = Destination => {
    setState(prevstate => ({...prevstate, Destination}));
  };

  const HandleTypeChange = async Type => {
    if (Type == 'Package Creation') {
      setState(prevstate => ({
        ...prevstate,
        Type,
        AgainstList: [],
        AgainstInfo: '',
        CreatedItems: [],
      }));
    } else {
      setState(prevstate => ({...prevstate, Type}));
      GetAgainstInfo(Type);
    }
  };

  const HandleStockChange = Stock => {
    setState(prevstate => ({...prevstate, Stock}));
  };

  const HandleAgainstInfoChange = (AgainstId, AgainstLabel, index, item) => {
    Request.get(
      `${LocalData.IP}/api/Distribution/Package/InvoiceDetailsForAgainstType?Type=${state.Type}&AgainstId=${AgainstId}&DestinationCode=${state.Destination}&Stockcode=${state.Stock}`,
      LocalData.Token,
    )
      .then(res => {
        let get_Against = res.data?.length > 0 ? res.data : [];
        UpdateAmountCalculation(get_Against);
        setState(prevstate => ({
          ...prevstate,
          CreatedItems: get_Against,
          AgainstInfo: item.No,
        }));
      })
      .catch(err => console.error('GetSelectedAgainstInfo : ', err));
  };

  const UpdateAmountCalculation = passcreateitemdata => {
    let BasicAmount = 0,
      TotalQty = 0;
    (passcreateitemdata || []).forEach(element => {
      BasicAmount += parseFloat(
        RoundValue(
          parseFloat(isnull(element.TransferQty, 0)) *
            parseFloat(isnull(element.TransferRate, 0)),
        ),
      );
      TotalQty += parseFloat(
        RoundValue(parseFloat(isnull(element.TransferQty, 0))),
      );
    });
    setState(prevstate => ({
      ...prevstate,
      BasicAmount: parseFloat(BasicAmount),
      TotalQty: parseFloat(TotalQty),
    }));
  };

  const GetScanITemDetails = async ScandedCode => {
    if (isnull(ScandedCode, '') != '') {
      // console.log(
      //   'ScandedCode : ',
      //   ScandedCode,
      //   ' api :',
      //   `${LocalData.IP}/api/Distribution/Package/ScanPackage?ScanBarcode=${ScandedCode}&Stockcode=${state.Stock}&DestinationCode=${state.Destination}`,
      // );

      Request.get(
        `${LocalData.IP}/api/Distribution/Package/ScanPackage?ScanBarcode=${ScandedCode}&Stockcode=${state.Stock}&DestinationCode=${state.Destination}`,
        LocalData.Token,
      )
        .then(res => {
          if (res.status === 200) {
            let Check = res.data;
            if (Check.length > 1) {
              setState(prevstate => ({
                ...prevstate,
                isDubBarcode: true,
                DubBarcodeList: Check,
                Barcode: '',
              }));
            } else if (Check.length) {
              BindProductListData(Check[0]);
            } else {
              AlertMessage('Invalid Barcode');
            }
          }
        })
        .catch(err => AlertError(err));
    }
  };

  const BindProductListData = async objScanItemDetails => {
    // console.log('bind data : ', objScanItemDetails);
    const newArray = state.CreatedItems; // Create a copy
    let IcodeDetailes = newArray.find(
      e => e.Icode.toLowerCase() === objScanItemDetails.Icode.toLowerCase(),
    );
    // console.log('IcodeDetailes : ', IcodeDetailes);
    if (IcodeDetailes) {
      let Qty = IcodeDetailes['TransferQty'];
      IcodeDetailes['TransferQty'] = parseFloat(Qty) + 1;
    } else {
      newArray.push(objScanItemDetails);
    }
    ScanBarcodeRef.focus();
    setState(prevstate => ({
      ...prevstate,
      CreatedItems: newArray,
      openScanner: false,
      isDubBarcode: false,
      Barcode: '',
    }));
    UpdateAmountCalculation(newArray);
  };

  const isItemBind = () => {
    return state.CreatedItems?.length > 0;
  };

  const ValidateInputs = async (
    isOnlySave,
    isSaveAndSendTransfer,
    isManualSend,
  ) => {
    // console.log('call save');
    try {
      Keyboard.dismiss();
      // Validate All Mandatory fields
      if (isnull(state.Destination, '0') == '0') {
        AlertMessage('Select Destination');
      } else if (isnull(state.Stock, '0') == '0') {
        AlertMessage('Select Stock point');
      } else if ((state.CreatedItems || []).length == 0) {
        AlertMessage('Add atleast one product');
      } else {
        setState(prevstate => ({...prevstate, isFetched: false}));
        // Preparing Invoice Data
        let get_InvoiceNo = '';
        if (state.EditInvoice) {
          get_InvoiceNo = state.InvoiceNo;
        }

        let ObjSave = {
          // Invoice Master data
          PackageNo: get_InvoiceNo,
          Packagedate: state.InvoiceDate,
          TransitDays: 0,
          Stkptcode: state.Stock,
          DestinationCode: state.Destination,
          Remarks: state.Remarks,
          Trstype: state.Type,
          TableLotDetails: state.CreatedItems,
          Loccode: LocalData.Loccode,
          CreatedByOrModifyBy: LocalData.UserCode,
          IsSendTransfer: isOnlySave ? false : isSaveAndSendTransfer,
          IsManualSend: isOnlySave ? false : isManualSend,
          RoleId: LocalData.RoleId,
          EmployeeID: LocalData.UserCode,
        };
        // console.log('API : ', `${LocalData.IP}api/Distribution/Package/SavePackageCreation`);
        // console.log('Token : ', `${LocalData.Token}`);
        // console.log('ObjSave : ', `${JSON.stringify(ObjSave)}`);
        Request.post(
          `${LocalData.IP}api/Distribution/Package/SavePackageCreation`,
          ObjSave,
          LocalData.Token,
        )
          .then(async res => {
            // console.log('result res : ', res);
            let json = await res.json();
            // console.log('result save : ', JSON.stringify(json));
            setState(prevstate => ({...prevstate, isFetched: true}));
            let IsPackageSaved =
                isnull(json?.IsPackageSaved, 'false').toString() == 'true',
              IsTransferSaved =
                isnull(json?.IsTransferSaved, 'false').toString() == 'true';
            let PackageNo = isnull(json?.PackageNo, ''),
              STFNoList = isnull(json?.STFNoList, ''),
              message = isnull(json?.message, '');
            if (res.status == 200 && IsPackageSaved) {
              // if (
              //   (isManualSend && IsTransferSaved) ||
              //   (isSaveAndSendTransfer && IsTransferSaved)
              // )
              if (isManualSend || isSaveAndSendTransfer) {
                AlertMessage(
                  IsTransferSaved
                    ? `saved ${PackageNo} and sent ${STFNoList} successfully`
                    : `saved ${PackageNo} successfully but failed to send transfer.`,
                  NavigateBackToList,
                );
              } else {
                AlertMessage(
                  `${PackageNo} saved successfully`,
                  NavigateBackToList,
                );
              }
              clearEntry();
              setState(prevstate => ({...prevstate, isFetched: true}));
              return true;
            } else {
              setState(prevstate => ({...prevstate, isFetched: true}));
              AlertMessage(isnull(message, 'Failed to save.'));
              return false;
            }
          })
          .catch(err => {
            console.error('result save failed : ', err);
            AlertMessage('Failed to save. \n' + err);
            setState(prevstate => ({...prevstate, isFetched: true}));
          });
      }
    } catch (err) {
      console.error(err);
    }
  };

  const NavigateBackToList = () => {
    navigation.goBack();
    route.params.onUpdateInvoice();
  };

  const clearEntry = () => {
    setState(prevstate => ({
      ...prevstate,
      isFetched: true, // show or hide Save Modal popup
      // Clearing Filed data
      Destination: '0',
      EditInvoice: false,
      InvoiceNo: '',
      InvoiceDate: new Date(),
      Status: 'Pending',
      Type: 'Package Creation',
      AgainstInfo: '',
      Remarks: '',
      // Clearing Created Items
      CreatedItems: [],
    }));
    UpdateAmountCalculation([]);
  };

  const HandleRemoveItem = index => {
    // console.log('remove item hand');
    Confirmation('Do you want to remove ?', () => {
      let CreatedItems = [...state.CreatedItems];
      CreatedItems.splice(index, 1);
      setState(prevstate => ({...prevstate, CreatedItems: CreatedItems}));
      UpdateAmountCalculation(CreatedItems);
    });
  };

  const HandleProductQtyChange = (index, value) => {
    value = value || 0;
    let CreatedItems = [...state.CreatedItems];
    CreatedItems[index].TransferQty = parseFloat(value);
    setState(prevstate => ({...prevstate, CreatedItems: CreatedItems}));
    UpdateAmountCalculation(CreatedItems);
  };

  const FieldTransfer = key => (
    <View
      pointerEvents={isItemBind() || isAgainst() ? 'none' : 'auto'}
      key={key}
      style={[
        layoutStyle.FieldLayout,
        {flexDirection: 'row'},
        layoutStyle.FieldContainerCount == 2 &&
          isnull(state.Destination, '0') == 0 && {width: '98%'},
      ]}>
      <ModalSearchablePicker
        fieldWrapperStyle={{width: '48%'}}
        placeholder="Destination"
        data={state.LocationList}
        labelProp="LocationName"
        valueProp="LocationCode"
        selectedValue={state.Destination}
        onValueSelected={HandleDestinationChange}
      />
      <ModalSearchablePicker
        fieldWrapperStyle={{width: '48%'}}
        placeholder="Stock"
        data={state.StockList}
        labelProp="STKPTNAME"
        valueProp="STKPTCODE"
        selectedValue={state.Stock}
        onValueSelected={HandleStockChange}
      />
    </View>
  );

  const isAgainst = () => {
    if (isnull(state.Type, '') == 'Package Creation') {
      return false;
    }
    return true;
  };

  const FieldDocInfo = key => (
    <View
      pointerEvents={isItemBind() ? 'none' : 'auto'}
      key={key}
      style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <ModalSearchablePicker
        fieldWrapperStyle={{width: isAgainst() ? '48%' : '98%'}}
        placeholder="Type"
        data={state.TypeList}
        labelProp="Name"
        valueProp="Code"
        selectedValue={state.Type}
        onValueSelected={HandleTypeChange}
      />
      {isAgainst() && (
        <ModalSearchablePicker
          fieldWrapperStyle={{width: '48%'}}
          placeholder="Against"
          data={state.AgainstList}
          labelProp="Display"
          valueProp="Code"
          defaultText={state.AgainstInfo}
          onValueSelected={HandleAgainstInfoChange}
        />
      )}
    </View>
  );

  const isValidDestinationAndStock = () => {
    if (
      isnull(state.Destination, '0') != '0' &&
      isnull(state.Stock, '0') != '0'
    ) {
      return true;
    }
    return false;
  };

  const isValidDestinationStockAndPackage = ValidateItems => {
    if (ValidateItems) {
      if (
        isValidDestinationAndStock() &&
        !isAgainst() &&
        (state.CreatedItems || []).length > 0
      ) {
        return true;
      }
    } else {
      if (isValidDestinationAndStock() && !isAgainst()) {
        return true;
      }
    }
    return false;
  };

  const isShowType = () => {
    if (
      (isValidDestinationAndStock() && isAgainst()) ||
      (isValidDestinationAndStock() &&
        !isAgainst() &&
        (state.CreatedItems || []).length == 0)
    ) {
      return true;
    }
    return false;
  };

  const FieldRemarks = key => (
    <View
      key={key}
      style={[layoutStyle.FieldLayout, {flexDirection: 'row', width: '100%'}]}>
      <RichTextInput
        wrapperStyle={{
          width: isValidDestinationStockAndPackage(false) ? '48%' : '98%',
        }}
        placeholder="Remarks"
        value={state.Remarks}
        onChangeText={Remarks =>
          setState(prevstate => ({...prevstate, Remarks: Remarks}))
        }
        inputProps={{multiline: true, numberOfLines: 2}}
      />
      {isValidDestinationStockAndPackage(false) && (
        <View style={{flexDirection: 'row', width: '48%'}}>
          <RichTextInput
            inputref={ref => {
              ScanBarcodeRef = ref;
            }}
            wrapperStyle={{width: '75%'}}
            placeholder="Scan Barcode"
            value={state.Barcode}
            onChangeText={Barcode =>
              setState(prevstate => ({...prevstate, Barcode}))
            }
            onBlur={value => GetScanITemDetails(value)}
            inputProps={{multiline: false}}
          />
          <TouchableOpacity onPress={() => onOpenScanner()}>
            <Image
              source={require('../../../assets/images/ScanBarcode.png')}
              style={styles.ScanBarcode}
            />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );

  const Scanner = ({}) => {
    return (
      <View style={styles.modalContainer}>
           <CameraScreen
            actions={{leftButtonText: 'Cancel'}}
            onBottomButtonPressed={() => {
              setState(prevstate => ({...prevstate, openScanner: false}));
            }}
            showFrame={true}
            // style={{height: '100%'}}
            scanBarcode={true}
            // laserColor={'white'}
            // frameColor={'lightgreen'}
            colorForScannerFrame={'black'}
            onReadCode={event =>
              GetScanITemDetails(event.nativeEvent.codeStringValue)
            }
          />
      </View>
    );
  };

  const refreshLayout = () => {};
  // console.log('isFetched : ', state.isFetched);
  return !state.isFetched ? (
    <ICubeAIndicator />
  ) : (
    <>
      <StatusBar
        backgroundColor={globalColorObject.Color.OffLineStatus}
        barStyle="default"
      />
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={refreshLayout}>
        <View style={styles.InvoiceTabWrapper}>
          <View
            style={{
              height:
                layoutStyle.FieldContainerCount == 1
                  ? isShowType()
                    ? 205
                    : 145
                  : layoutStyle.FieldContainerCount == 2
                  ? 145
                  : 90,
            }}>
            <ScrollView
              keyboardShouldPersistTaps="always"
              nestedScrollEnabled={true}
              style={[styles.ViewPager]}>
              <View style={[layoutStyle.ScrollContentWrapper]}>
                {FieldTransfer()}
                {isShowType() && FieldDocInfo()}
                {FieldRemarks()}
              </View>
            </ScrollView>
          </View>
          <View
            style={[
              styles.CreatedItemsTab,
              {backgroundColor: globalColorObject.Color.Lightprimary},
            ]}>
            <FlatList
              style={[
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
              ]}
              data={state.CreatedItems}
              renderItem={({item, index}) => (
                <ListItem
                  data={item}
                  index={index}
                  setMenuRef={ref => setMenuRef(ref, index)}
                  isAgainst={isAgainst()}
                  headers={visibleCreatedItemsGroupFields}
                  HandleRemoveProduct={HandleRemoveItem}
                  HandleQtyChange={HandleProductQtyChange}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={
                <ListItemHeader
                  headers={[...visibleCreatedItemsGroupCaptions]}
                />
              }
              ListFooterComponent={() => (
                <View style={{width: '100%', marginTop: 90}} />
              )}
              stickyHeaderIndices={[0]}
            />
          </View>

          <View
            style={[
              styles.SummaryTab,
              {backgroundColor: globalColorObject.Color.Lightprimary},
            ]}>
            <View style={[styles.SingleLine, {width: '50%'}]}>
              <Text
                style={[
                  styles.AmountLabel,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                Quantity:
              </Text>
              <Text
                style={[
                  styles.AmountText,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                {state.TotalQty}
              </Text>
            </View>
            <View style={[styles.SingleLine, {width: '50%'}]}>
              <Text
                style={[
                  styles.AmountLabel,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                Amount:
              </Text>
              <Text
                style={[
                  styles.AmountText,
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                  ),
                ]}>
                {state.BasicAmount}
              </Text>
            </View>
          </View>
        </View>

        {/* Dublicate BArcode */}
        <Modal
          animationType="slide"
          transparent={false}
          visible={state.isDubBarcode}
          onRequestClose={() => {
            // Alert.alert('Screen has been closed.');
          }}>
          <FlatList
            style={{
              marginTop: 5,
              backgroundColor: globalColorObject.Color.Lightprimary,
            }}
            data={state.DubBarcodeList}
            ListHeaderComponent={<DBDupListItemHeader />}
            renderItem={({item, index}) => (
              <DBDupListItem
                data={item}
                index={index}
                onLongPress={BindProductListData.bind(this, item)}
              />
            )}
            keyExtractor={item => item.Icode.toString()}
            stickyHeaderIndices={[0]}
          />
        </Modal>

        {/* Barcode Scanner*/}
        <Modal animationType="slide" visible={state.openScanner}>
              <Scanner
                // onCancel={() => {
                //   setState(prevstate => ({...prevstate, openScanner: false}));
                // }}
                // onReadCode={() => {
                //   setState(prevstate => ({...prevstate, openScanner: false}));
                // }}
              />
            </Modal>
        {/* Custom Popup Loader ends here*/}
      </LayoutWrapper>
    </>
  );
};

const styles = StyleSheet.create({
  ScanBarcode: {
    width: 30,
    height: 30,
    top: 15,
  },
  FieldInput: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    textAlign: 'left',
  },
  closeButtonParent: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
  closeButton: {
    height: 25,
    width: 25,
  },
  ModalLoaderBackDrop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalLoaderContainer: {
    width: '70%',
    height: 100,
    borderRadius: 5,
    flexDirection: 'row',
  },
  ModalLoader: {
    flex: 1,
    alignSelf: 'center',
  },
  ModalLoaderText: {
    flex: 2,
    alignSelf: 'center',
  },
  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  ViewPager: {
    flex: 1,
  },
  Tabs: {
    flexDirection: 'row',
    // borderTopColor: 'rgb(221, 221, 221)',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  TabView: {
    padding: 10,
    // borderBottomWidth: 2,
    width: '100%',
    textAlign: 'center',
  },
  CreatedItemsTab: {
    flex: 1,
    // backgroundColor: '#f9f9f9',
  },
  AddNewItem: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  // Created Items List Styling starts here
  ButtonsContainer: {
    width: '100%',
    paddingVertical: 10,
    flexDirection: 'row',
    // backgroundColor: '#ededed',
    alignItems: 'center',
  },
  SearhBarWrapper: {
    paddingHorizontal: 10,
    flex: 1,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderBottomWidth: 1,
  },
  BtnExtraActionsWrapper: {
    width: 40,
  },
  BtnExtraActions: {
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 3,
    marginVertical: 3,
    marginHorizontal: 13,
  },
  CardBtnOptionWrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  CardBtnOption: {
    width: 35,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  CardBtnOptionText: {
    lineHeight: 25,
    transform: [{scaleX: 1.7}],
    top: -5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '30%',
    marginVertical: 3,
  },
  // Created Items List Styling ends here
  SummaryTab: {
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  SingleLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  AmountLabel: {
    textAlign: 'left',
  },
  AmountText: {
    textAlign: 'left',
  },
  
  modalContainer: {
    flex: 1,
  },
});

export default AddPackage;
