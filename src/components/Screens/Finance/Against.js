import React, {Component} from 'react';
import {
  View,
  Text,
  Switch,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
} from 'react-native';
import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
} from '../../../Style/GlobalStyle';
import LayoutWrapper, {CardStyle} from '../../Layout/Layout';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import RichTextInput from '../../Custom/RichTextInput';
import {
  Request,
  AlertMessage,
  GetSessionData,
  ReturnDisplayStringForDate,
  isnull,
  isnull_number,
  nullif_Number,
  GetGlobalSettings,
} from '../../../Helpers/HelperMethods';

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.Card,
        {justifyContent: 'space-between', marginTop: 0},
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardTextContainer, {width: 175}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Invoice
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: 175}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Document
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 140}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Net Amount
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Previous Adj
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 80}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Discount
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 60}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Dis %
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 100}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          TDS
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 100}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Others
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Adjustment
        </Text>
      </View>
      {/* Current Adj */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Current Adj
        </Text>
      </View>

      {/* Balance */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Balance
        </Text>
      </View>
      {/* Difference */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          Difference
        </Text>
      </View>

      <View style={[styles.CardTextContainer, CardItemLayout, {width: 80}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Due Date
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 40}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          O.D
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 40}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}></Text>
      </View>
    </View>
  );
};

const ListItem = props => {
  const data = props.data;
  let InvoiceDate = ReturnDisplayStringForDate(data.ENTRYDATE),
    DocDate = ReturnDisplayStringForDate(data.DOCDATE),
    DueDate = ReturnDisplayStringForDate(data.DUEDATE);
  let Disc_Per = isnull(nullif_Number(props.data.DicountInPercent, 0), '');
  let isNegative = data.Sign == '-';
  const {CardItemLayout} = CardStyle;
  let current_Adj =
    isnull_number(props.data.ADJUSTMENT, 0) +
    isnull_number(props.data.OTHERS, 0) +
    isnull_number(props.data.TDS, 0) +
    isnull_number(props.data.DISCOUNT, 0);
  let balance =
    isnull_number(props.data.NETAMT, 0) -
    isnull_number(props.data.PREVADJ, 0) -
    current_Adj;
  return (
    <View
      style={[
        styles.Card,
        {justifyContent: 'space-between'},
        ApplyStyleColor(
          globalColorObject.Color.oppPrimary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardTextContainer, {width: 175}]}>
        <Text
          numberOfLines={1}
          style={[
            {
              color: isNegative ? 'maroon' : globalColorObject.Color.BlackColor,
            },
            ApplyStyleFontAndSize(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
            ),
          ]}>
          {`${data.Sign}`} {data.ENTRYNO} {InvoiceDate}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, {width: 175}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          {data.DOCNO} {DocDate}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 140}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {(Math.round(data.NETAMT * 100) / 100).toFixed(2)}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {(Math.round(data.PREVADJ * 100) / 100).toFixed(2)}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 80}]}>
        <RichTextInput
          isShowAnimateText={false}
          placeholder="Discount"
          value={isnull(nullif_Number(data.DISCOUNT, 0), '')}
          wrapperStyle={{width: '100%', marginVertical: 0,}}
          inputStyle={{marginTop: 0, textAlign: 'right', width: '100%'}}
          onChangeText={DISCOUNT => {
            props.onValueChange(props.index, DISCOUNT, 'DISCOUNT');
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 60}]}>
        <RichTextInput
          isShowAnimateText={false}
          placeholder="Dis %"
          value={
            isnull(nullif_Number(props.data.DicountInPercent, 0), '')
            //   isnull(
            //   nullif_Number(
            //     (
            //       Math.round(
            //         (props.data.DISCOUNT / props.data.NETAMT) * 100 * 100,
            //       ) / 100
            //     ).toFixed(2),
            //     0,
            //   ),
            //   '',
            // )
          }
          wrapperStyle={{width: '100%', marginVertical: 0}}
          inputStyle={{marginTop: 0, textAlign: 'right'}}
          onChangeText={DicountInPercent => {
            props.onValueChange(
              props.index,
              DicountInPercent,
              'DicountInPercent',
            );
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 100}]}>
        <RichTextInput
          isShowAnimateText={false}
          placeholder="TDS"
          value={isnull(nullif_Number(props.data.TDS, 0), '')}
          wrapperStyle={{width: '100%', marginVertical: 0}}
          inputStyle={{marginTop: 0, textAlign: 'right'}}
          onChangeText={TDS => {
            props.onValueChange(props.index, TDS, 'TDS');
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 100}]}>
        <RichTextInput
          isShowAnimateText={false}
          placeholder="Others"
          value={isnull(nullif_Number(props.data.OTHERS, 0), '')}
          wrapperStyle={{width: '100%', marginVertical: 0}}
          inputStyle={{marginTop: 0, textAlign: 'right'}}
          onChangeText={OTHERS => {
            props.onValueChange(props.index, OTHERS, 'OTHERS');
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <RichTextInput
          isShowAnimateText={false}
          placeholder="Adjustment"
          value={isnull(nullif_Number(props.data.ADJUSTMENT, 0), '')}
          wrapperStyle={{width: '100%', marginVertical: 0}}
          inputStyle={{marginTop: 0, textAlign: 'right'}}
          onChangeText={ADJUSTMENT => {
            props.onValueChange(props.index, ADJUSTMENT, 'ADJUSTMENT');
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
      </View>
      {/* Current Adj */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {(Math.round(current_Adj * 100) / 100).toFixed(2)}
        </Text>
      </View>
      {/* Balance */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {(Math.round(balance * 100) / 100).toFixed(2)}
        </Text>
      </View>
      {/* Difference */}
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 120}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {current_Adj > 0 ? isnull(nullif_Number(balance, 0), '') : ''}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 80}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'right'},
          ]}>
          {DueDate}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 40}]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {textAlign: 'center'},
          ]}>
          {isnull(nullif_Number(data.OVERDUEDAYS, 0), '')}
        </Text>
      </View>
      <View style={[styles.CardTextContainer, CardItemLayout, {width: 40}]}>
        <Switch
          onValueChange={Select => {
            props.onValueChange(props.index, Select, 'SELECT');
          }}
          value={props.data.SELECT}
        />
      </View>
    </View>
  );
};

export default class Against extends Component {
  constructor() {
    super();
    this.APILink = {
      API_LoadAgainstData: `/api/Accounts/BindAgainstSource?`, //CallFrom=PO&LocCode=1
    };
    this.state = {
      isFetched: false,
      IsBasedOnValidation: false,
      Amount: 0.0,
      New_Ref_Amount: 0.0,
      VoucherTypeID: 0,
      Loccode: 0,
      FinancialId: 0,
      Against: [],
      ListSourceData: [],
      SearchText: '',
      DateFormat: 'dd/MM/yyyy',
    };
  }

  HandleSearch = async SearchText => {
    this.setState({SearchText});
    let POList = this.state.Against;
    let txt = SearchText.toLowerCase();
    let ListSourceData = POList.filter(obj => {
      // Get all the texts

      let InvoiceDate = ReturnDisplayStringForDate(obj.ENTRYDATE).toString(),
        DocDate = ReturnDisplayStringForDate(obj.DOCDATE).toString(),
        DueDate = ReturnDisplayStringForDate(obj.DUEDATE).toString();
      let InvoiceNo = isnull(obj.ENTRYNO, '').toString().toLowerCase();
      let Sign = isnull(obj.Sign, '').toLowerCase().toString();
      let DOCNO = isnull(obj.DOCNO, '').toLowerCase().toString();
      let NETAMT = isnull(nullif_Number(obj.NETAMT, 0), '').toString();
      let PREVADJ = isnull(nullif_Number(obj.PREVADJ, 0), '').toString();
      let TDS = isnull(nullif_Number(obj.TDS, 0), '').toString();
      let DISCOUNT = isnull(nullif_Number(obj.DISCOUNT, 0), '').toString();
      let DISCOUNT_Per = isnull(
        nullif_Number(obj.DicountInPercent, 0),
        '',
      ).toString();

      (
        Math.round(
          nullif_Number((props.data.DISCOUNT / props.data.NETAMT) * 100, 0) *
            100,
        ) / 100
      ).toFixed(2);
      let OTHERS = isnull(nullif_Number(obj.OTHERS, 0), '').toString();
      let ADJUSTMENT = isnull(nullif_Number(obj.ADJUSTMENT, 0), '').toString();
      let Cur_Adj = (
        Math.round(
          (isnull_number(obj.ADJUSTMENT, 0) +
            isnull_number(obj.OTHERS, 0) +
            isnull_number(obj.TDS, 0) +
            isnull_number(obj.DISCOUNT, 0)) *
            100,
        ) / 100
      )
        .toFixed(2)
        .toString();
      let Balance = (
        Math.round(
          (isnull_number(obj.NETAMT, 0) -
            (isnull_number(obj.ADJUSTMENT, 0) +
              isnull_number(obj.OTHERS, 0) +
              isnull_number(obj.TDS, 0) +
              isnull_number(obj.DISCOUNT, 0))) *
            100,
        ) / 100
      )
        .toFixed(2)
        .toString();
      let Difference = isnull(nullif_Number(obj.RUNNING, 0), '').toString();
      let OverDueDays = isnull(
        nullif_Number(obj.OVERDUEDAYS, 0),
        '',
      ).toString();

      return (
        InvoiceDate.includes(txt) ||
        DocDate.includes(txt) ||
        DueDate.includes(txt) ||
        InvoiceNo.includes(txt) ||
        Sign.includes(txt) ||
        DOCNO.includes(txt) ||
        NETAMT.includes(txt) ||
        PREVADJ.includes(txt) ||
        TDS.includes(txt) ||
        DISCOUNT.includes(txt) ||
        OTHERS.includes(txt) ||
        ADJUSTMENT.includes(txt) ||
        Cur_Adj.includes(txt) ||
        Balance.includes(txt) ||
        Difference.includes(txt) ||
        OverDueDays.includes(txt)
      );
    });
    this.setState({ListSourceData});
  };
  componentDidMount() {
    // // getting current Orientation and lock it to Landscape
    // Orientation.getOrientation((err, orientation) => {
    //   console.log(this.InitialOrientation);
    //   this.InitialOrientation = orientation;
    //   Orientation.lockToLandscape();
    // });

    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={{right: 10, width: 30, height: 30}}
          onPress={this.HandleCompleteAgainst}>
          <Image
            style={{width: 30, height: 30}}
            source={require('../../../assets/images/AddIma.png')}
          />
        </TouchableOpacity>
      ),
    });
    this.LoadInitialData();
  }
  componentWillUnmount() {
    // this.InitialOrientation === 'PORTRAIT' && Orientation.lockToPortrait();
  }

  UpdateValue = (index, value, Type) => {
    let get_Against = this.state.Against;
    let New_Ref = 0 , Amount = this.state.Amount;
    let E_Value = isnull_number(value, 0);
    let SeqNo = isnull_number(get_Against[index].SeqNo, "0");
    let Adjustment = Type == "ADJUSTMENT" ? E_Value : isnull_number(get_Against[index].ADJUSTMENT, 0);
    let Discount = Type == "DISCOUNT" ? E_Value  : isnull_number(get_Against[index].DISCOUNT, 0);
    let Tds = Type == "TDS" ? E_Value : isnull_number(get_Against[index].TDS, 0);
    let Others = Type == "OTHERS" ? E_Value : isnull_number(get_Against[index].OTHERS, 0);
    let SignValue = isnull_number(((get_Against[index].Sign) + "1"),0);
    // console.log('SignValue : ', SignValue);
    let Current = (Adjustment + Discount + Tds + Others);
    let NetAmt = isnull_number(get_Against[index].NETAMT, 0);
    let Previous = isnull_number(get_Against[index].PREVADJ, 0);
    let CurrAdjust = isnull_number(
      get_Against.reduce((accum, obj) => {
        let get_SeqNo = isnull_number(obj.SeqNo, 0);
        let get_signvalue = isnull_number(obj.Sign + '1', 0);
        let get_Amount =
          isnull_number(obj.ADJUSTMENT, 0) +
          isnull_number(obj.DISCOUNT, 0) +
          isnull_number(obj.TDS, 0) +
          isnull_number(obj.OTHERS, 0);
        return accum + ( get_SeqNo != SeqNo ? get_signvalue * get_Amount : 0);
      }, 0),
      0,
    );
    let _Amount = isnull_number(Amount, 0)
    let _IsBasedOnValidation = this.state.IsBasedOnValidation;
    // console.log('my based on : ', _IsBasedOnValidation);
    if ((Type == "DicountInPercent") || (Type == "ADJUSTMENT") || (Type == "DISCOUNT") || (Type == "TDS") || (Type == "OTHERS"))
    {
        let isModify = false;
        if (Type == "DicountInPercent")
        {
            Current -= Discount;
            Discount =  (Math.round(((E_Value * NetAmt) / 100) * 100) / 100).toFixed(2);
            Current += Discount;
        }
        if ((_Amount < CurrAdjust + (SignValue * Current)) && _IsBasedOnValidation)
        {
          AlertMessage("Adjusted value should not greater than the total amount.");
        }
        else if (NetAmt < Current)
        {
            AlertMessage("Enter amount should not be greater than net amount.");
        }
        else if ((NetAmt-Previous) < Current)
        {
            AlertMessage("Enter amount should not be greater than balance amount.");
        }
        else
        {
            let isModify= true;
            get_Against[index][Type] = E_Value;
            get_Against[index].SELECT = (Current > 0 ? true : false);
            get_Against[index].CURRENTADJUSTMENT = Current;
            get_Against[index].RUNNING = NetAmt - (Previous + Current);
            if (_IsBasedOnValidation)
                New_Ref = (_Amount - (CurrAdjust + (SignValue * Current)));
            else
            {
                New_Ref = 0.00;
                Amount = (CurrAdjust + (SignValue * Current));
            }
        }
        if(isModify){
          if (Type == "DISCOUNT")
          {
              get_Against[index].DicountInPercent = (Math.round((Discount / NetAmt) * 100 * 100) / 100).toFixed(2);
          }
          else if (Type == "DicountInPercent")
          {
              get_Against[index].DISCOUNT = Discount;
          }
        }
    }
    else if (Type == "SELECT")
    {
        if (value)
        {
            if (SignValue < 0)
            {
                let CurrentBalance = NetAmt - Previous;
                get_Against[index].ADJUSTMENT = CurrentBalance;
                get_Against[index].DISCOUNT = 0;
                get_Against[index].DicountInPercent = 0;
                get_Against[index].TDS = 0;
                get_Against[index].OTHERS = 0;
                get_Against[index].CURRENTADJUSTMENT = CurrentBalance;
                get_Against[index].RUNNING = 0;
                get_Against[index].SELECT = true;
                if (_IsBasedOnValidation)
                    New_Ref = (_Amount - (CurrAdjust + (SignValue * CurrentBalance)));
                else
                {
                    New_Ref = 0.00;
                    Amount = (CurrAdjust + (SignValue * CurrentBalance));
                }
            }
            else
            {
              let GetUnUdjusedValue = _Amount - (CurrAdjust + ((Discount + Tds + Others) * SignValue));
                if (((_Amount <= CurrAdjust && SignValue > 0) || (_Amount < CurrAdjust && SignValue < 0) ||
                    (GetUnUdjusedValue <= 0 && SignValue > 0) || (GetUnUdjusedValue < 0 && SignValue < 0)) && _IsBasedOnValidation)
                {
                    AlertMessage("Adjusted value should not greater than the total amount.");
                }
                else
                {
                    let RunningValue = (NetAmt - (Previous + Discount + Tds + Others));
                    Adjustment = _IsBasedOnValidation ? (GetUnUdjusedValue >= RunningValue ? RunningValue : GetUnUdjusedValue) : (RunningValue > 0 ? RunningValue : 0);
                    Current = Adjustment + Discount + Tds + Others;
                    get_Against[index].SELECT = true;
                    get_Against[index].ADJUSTMENT = Adjustment;
                    get_Against[index].CURRENTADJUSTMENT = Current;
                    get_Against[index].RUNNING = (NetAmt - (Previous + Current));
                    if (_IsBasedOnValidation)
                        New_Ref = (_Amount - (CurrAdjust + (SignValue * Current)));
                    else
                    {
                        New_Ref = 0.00;
                        Amount = (CurrAdjust + (SignValue * Current));
                    }
                }
            }
        }
        else
        {
            get_Against[index].ADJUSTMENT = 0;
            get_Against[index].DISCOUNT = 0;
            get_Against[index].DicountInPercent = 0;
            get_Against[index].TDS = 0;
            get_Against[index].OTHERS = 0;
            get_Against[index].CURRENTADJUSTMENT = 0;
            get_Against[index].RUNNING = NetAmt - Previous;
            get_Against[index].SELECT = false;
            if (_IsBasedOnValidation)
                New_Ref = (_Amount - (CurrAdjust));
            else
            {
                New_Ref = 0.00;
                Amount = (CurrAdjust);
            }
        }
    }
    this.setState({
      Amount: Amount,
      Against: get_Against,
      New_Ref_Amount: New_Ref,
    });
  };

  HandleCompleteAgainst = () => {
    let isCompleted = false;
    let NewRefAmount = this.state.Amount;
    let AgainstAmount = 0;
    let ListSourceData = [];
    // let AgainstAmountList = '';
    // let AgainstSignList = '';
    // let AgainstAdjusedAmountList = '';
    // let AgainstDiscountAmountList = '';
    // let AgainstTDSAmountList = '';
    // let AgainstOthersAmountList = '';
    // let AgainstIDList = '';

    let AgainstList = this.state.Against;
    if (AgainstList.length > 0) {
      ListSourceData = AgainstList.filter(obj => {
        let Select = (obj.SELECT || '').toString().toLowerCase();
        let Amount =
          isnull_number(obj.ADJUSTMENT, 0) +
          isnull_number(obj.DISCOUNT, 0) +
          isnull_number(obj.TDS, 0) +
          isnull_number(obj.OTHERS, 0);
        return Select.includes(true) && Amount > 0;
      });
      let GetBalvalue = this.Return_AgainstValue(ListSourceData);
      let tot = this.state.Amount - GetBalvalue;
      if (tot < 0)
          AlertMessage("Adjusted value should not greater than the total amount.");
      else if (GetBalvalue < 0)
          AlertMessage("Adjusted value should not greater than the total amount.");
      else
      {
          isCompleted = true;
          NewRefAmount = tot;
          AgainstAmount = GetBalvalue;
          // AgainstAmountList = ListSourceData.map(function (obj) {
          //   return (
          //     parseFloat(obj.ADJUSTMENT) +
          //     parseFloat(obj.DISCOUNT) +
          //     parseFloat(obj.TDS) +
          //     parseFloat(obj.OTHERS)
          //   );
          // }).join(',');
          // AgainstSignList = ListSourceData.map(function (obj) {
          //   return obj.Sign;
          // }).join(',');
          // AgainstAdjusedAmountList = ListSourceData.map(function (obj) {
          //   return obj.ADJUSTMENT;
          // }).join(',');
          // AgainstDiscountAmountList = ListSourceData.map(function (obj) {
          //   return obj.DISCOUNT;
          // }).join(',');
          // AgainstTDSAmountList = ListSourceData.map(function (obj) {
          //   return obj.TDS;
          // }).join(',');
          // AgainstOthersAmountList = ListSourceData.map(function (obj) {
          //   return obj.OTHERS;
          // }).join(',');
          // AgainstIDList = ListSourceData.map(function (obj) {
          //   return obj.ID;
          // }).join(',');
      }
    }
    else
      isCompleted = true;
    // console.log(
    //   'hh',
    //   NewRefAmount,
    //   AgainstAmount,
    //   // AgainstAmountList,
    //   // AgainstAdjusedAmountList,
    //   // AgainstDiscountAmountList,
    //   // AgainstTDSAmountList,
    //   // AgainstOthersAmountList,
    //   // AgainstIDList,
    //   // AgainstSignList,
    //   ListSourceData,
    // );
    if(isCompleted){
      this.props.navigation.goBack();
      this.props.route.params.onFinishCreation(
        NewRefAmount,
        AgainstAmount,
        // AgainstAmountList,
        // AgainstSignList,
        // AgainstAdjusedAmountList,
        // AgainstDiscountAmountList,
        // AgainstTDSAmountList,
        // AgainstOthersAmountList,
        // AgainstIDList,
        ListSourceData,
      );
    }
  };

  Return_AgainstValue = (passListData) => {
    let GetBalvalue = 0;
    if(passListData && passListData.length > 0){
      GetBalvalue = isnull_number(
        passListData.reduce((accum, obj) => {
          let get_signvalue = isnull_number(obj.Sign + '1', 0);
          let Amount =
            isnull_number(obj.ADJUSTMENT, 0) +
            isnull_number(obj.DISCOUNT, 0) +
            isnull_number(obj.TDS, 0) +
            isnull_number(obj.OTHERS, 0);
          return accum + (get_signvalue * Amount);
        }, 0),
        0,
      );
    }
    return GetBalvalue;
  }

  LoadInitialData = async () => {
    const {
      get_IP,
      get_Token,
      get_UserCode,
      get_Location,
      get_FinancialYearId,
      get_RoleId,
      get_DateFormat,
    } = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    let params = this.props.route.params;
    let AmountValue = isnull_number(params.NewRefAmount, 0);
    let get_IsEnableTotalAmount = params.IsEnableTotalAmount;
    let Get_Settings = await GetGlobalSettings('AccountVouchers');
    let get_IsBasedOnValidation = Get_Settings.VoucherAgainstValidation == 'True' ? true : false;
    // console.log('my settings : ', Get_Settings, ' orginal : ', get_IsEnableTotalAmount ? get_IsBasedOnValidation : true);
    this.setState(
      {
        IsBasedOnValidation : get_IsEnableTotalAmount ? get_IsBasedOnValidation : true,
        VoucherTypeID: params.VoucherTypeID,
        DateFormat: get_DateFormat,
      },
      () => {
        let url = `${this.IP}${this.APILink.API_LoadAgainstData}VoucherTypeID=${this.state.VoucherTypeID}&EntryID=${params.EntryId}&SubLedgerID=${params.SubLedgerID}&AdjusedIdList=0&AdjusedAmountList=${params.NewRefAmount}&isCallForBy=${params.isCallForBy}&loccode=${params.Loccode}&FinancialId=${params.FinancialId}`;
        Request.get(url, this.Token)
          .then(res => {
            let get_data = res.data || [];
            let PendingValue = 0;
            // if (get_data.length > 0) {
            //   let GetBalvalue = this.Return_AgainstValue(get_data);
            //   if (this.state.IsBasedOnValidation) {
            //     PendingValue =
            //       AmountValue - GetBalvalue < 0 ? 0 : AmountValue - GetBalvalue;
            //   } else {
            //     AmountValue = GetBalvalue;
            //   }
            // }
            // this.setState({
            //   isFetched: true,
            //   Amount: AmountValue,
            //   Against: get_data,
            //   ListSourceData: get_data,
            //   New_Ref_Amount: PendingValue,
            // });

            let GetBalvalue = this.Return_AgainstValue(get_data);
            // console.log('tot : ', GetBalvalue , this.state.IsBasedOnValidation, AmountValue - GetBalvalue);
              if (this.state.IsBasedOnValidation)
                  PendingValue = (AmountValue - GetBalvalue) < 0 ? 0 : (AmountValue - GetBalvalue);
              else
                  AmountValue = GetBalvalue;
            this.setState({
              isFetched: true,
              Amount: AmountValue,
              Against: get_data,
              ListSourceData: get_data,
              New_Ref_Amount: PendingValue,
            });
          })
          .catch(err => AlertMessage(JSON.stringify(err)));
      },
    );
  };

  refreshLayout = () => this.forceUpdate();

  render() {
    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.Lightprimary}
          onLayoutChanged={this.refreshLayout}>
          <View
            style={[
              styles.SearhBarWrapper,
              {
                width: '100%',
                flexDirection: 'row',
              },
            ]}>
            <TextInput
              style={[
                styles.SearchInput,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                {width: '100%'},
              ]}
              placeholder={'Search here'}
              value={this.state.SearchText}
              onChangeText={this.HandleSearch}
              autoCorrect={false}
              returnKeyType="search"
              autoCapitalize="none"
              onFocus={this.showCancel}
              onBlur={this.hideCancel}
            />
          </View>
          <FlatList
            style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            data={this.state.ListSourceData}
            renderItem={({item, index, Type}) => (
              <ListItem
                data={item}
                index={index}
                onValueChange={this.UpdateValue}
              />
            )}
            keyExtractor={item => item.ID.toString()}
            refreshing={false}
            ListHeaderComponent={<ListItemHeader />}
            stickyHeaderIndices={[0]}
            // to have some breathing space on bottom
            ListFooterComponent={() => (
              <View style={{width: '100%', marginTop: 85}} />
            )}
          />

          <View
            style={[
              {
                marginVertical: 3,
                width: '100%',
                flexDirection: 'row',
                height: 50,
              },
            ]}>
            <RichTextInput
              isShowAnimateText={true}
              placeholder="Amount"
              value={isnull(nullif_Number(this.state.Amount, 0), '')}
              wrapperStyle={{width: '42%', marginVertical: 0}}
              inputStyle={{marginTop: 0, textAlign: 'right'}}
              onChangeText={Amount => 
                {
                  let _Amount = isnull_number(Amount, 0), PendingValue = 0;
                  if (this.state.IsBasedOnValidation)
                  {
                      let GetBalvalue = this.Return_AgainstValue(this.state.ListSourceData);
                      PendingValue = (_Amount - GetBalvalue) < 0 ? 0 : (_Amount - GetBalvalue);
                  }
                  this.setState({Amount: _Amount, New_Ref_Amount: PendingValue})
                }}
              inputProps={{
                keyboardType: 'phone-pad',
                editable: this.state.IsBasedOnValidation,
              }}
            />
            <RichTextInput
              isShowAnimateText={true}
              placeholder="New Ref"
              value={isnull(nullif_Number(this.state.New_Ref_Amount, 0), '')}
              wrapperStyle={{width: '42%', marginVertical: 0}}
              inputStyle={{marginTop: 0, textAlign: 'right'}}
              onChangeText={New_Ref_Amount => this.setState({New_Ref_Amount})}
              inputProps={{
                keyboardType: 'phone-pad',
                editable: false,
              }}
            />

            <TouchableOpacity
              style={{
                paddingHorizontal: 10,
                width: '15%',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.HandleCompleteAgainst}>
              <Image
                style={{width: 30}}
                source={require('../../../assets/images/Fetch.png')}
              />
            </TouchableOpacity>
          </View>
        </LayoutWrapper>
        <ICubeAIndicator isShow={!this.state.isFetched} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  SearhBarWrapper: {
    padding: 10,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
    top: 70,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 10,
    paddingHorizontal: 5,
  },
  CardTextContainer: {
    // width: '25%',
    marginVertical: 5,
    borderColor: 'black',
    // borderWidth: 1,
    justifyContent: 'center',
  },
});
