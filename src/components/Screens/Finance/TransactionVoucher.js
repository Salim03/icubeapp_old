import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';

import {CardStyle} from '../../Layout/Layout';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import {
  Request,
  AlertMessage,
  GetSessionData,
  sharefileForCallFRom,
  DeleteSelectedInvoice,
} from '../../../Helpers/HelperMethods';
import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../../Style/GlobalStyle';
// console.log(global.SampleVar);

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '100%',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            From
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '100%',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              // {textAlign: 'right'},
            ]}>
            To
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '34%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Number
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '24%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Date
          </Text>
        </View>
        <View
          style={[styles.CardTextContainer, CardItemLayout, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
  // 	const stickySegmentControlX = this.state.scrollY.interpolate({
  // 		inputRange: [0, STICKY_SCROLL_DISTANCE],
  // 		outputRange: [INIT_STICKY_HEADER, HEADER_MIN_HEIGHT],
  // 		extrapolate: 'clamp'
  //   })
};

const ListItem = props => {
  const data = props.data;
  let isDoubleCard = false;
  let FullInvoiceDate = new Date(data.VoucherDate);
  let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
    '0' +
    (FullInvoiceDate.getMonth() + 1)
  ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
  return (
    <TouchableWithoutFeedback
      onLongPress={() => props.onLongPress(props.data.ID)}>
      <View
        style={[
          styles.Card,
          isDoubleCard && {maxWidth: '49%'},
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View
            style={[
              styles.CardTextContainer,
              {
                width: '100%',
              },
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.AccountBy}
            </Text>
          </View>
          <View
            style={[
              styles.CardTextContainer,
              {
                width: '100%',
              },
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                // {textAlign: 'right'},
              ]}>
              {data.AccountTo}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '34%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.VoucherNo}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '24%'}]}>
            <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {InvoiceDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {(Math.round(data.Amount * 100) / 100).toFixed(2)}
            </Text>
          </View>
        </View>

        <View
          style={[
            {
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              backgroundColor: globalColorObject.Color.Lightprimary,
            },
          ]}>
          <View
            style={[
              {
                width: 0,
                height: 0,
                backgroundColor: globalColorObject.Color.Lightprimary,
                borderRightWidth: 20,
                borderTopWidth: 20,
                borderRightColor: globalColorObject.Color.Lightprimary,
                borderTopColor: globalColorObject.Color.oppPrimary,
              },
              {
                transform: [{rotate: '90deg'}],
              },
            ]}></View>
          <TouchableOpacity
            onPress={() => props.onDeleteInvoice(data.ID)}
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              // borderBottomWidth: 1,
              // borderBottomColor: 'lightgray',
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/3096/3096673.png',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              // borderBottomWidth: 1,
              // borderRightWidth: 1,
              // borderColor: 'lightgray',
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                data.ID,
                data.VoucherNo,
                moment(new Date(InvoiceDate)).format('YYYY-MM-DD'),
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1059/1059106.png',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class TransactionVoucher extends Component {
  constructor() {
    super();
    this.LocalData = {
      Title: '',
      UserCode: '',
      FinancialYearId: 0,
      RoleId: '',
      FormIndex: '',
    };
    this.state = {
      Voucher: [],
      VoucherType: [],
      Location: [],
      show: false,
      SearchText: '',

      isFetched: false,
      isRefresh: false,
      NoDataAvailable: false,

      MasterVoucherTypeId: 0,
      GetVoucherTypeId: 0,
      Loccode: 0,
    };
    this.ActionButtons = [
      {
        text: 'New',
        icon: require('../../../assets/images/add-light.png'),
        name: 'AddNew',
        color: '#414d4c',
        buttonSize: 45,
        position: 1,
      },
    ];
  }

  EnableLoadScreen = () => {
    this.setState({isFetched: false});
  };
  HandleFetchFlag = isFetched => {
    this.setState({isFetched: isFetched});
  };
  CallShareFile = async (InvoiceID, InvoiceNO, InvoiceDate) => {
    this.HandleFetchFlag(false);
    await sharefileForCallFRom(
      this.state.GetVoucherTypeId,
      InvoiceID,
      InvoiceNO,
      InvoiceDate,
      this.LocalData.FormIndex,
      this.state.Loccode,
    );
    this.HandleFetchFlag(true);
  };

  componentDidMount() {
    let params = this.props.route.params;
    this.LocalData.Title = params.HeaderTitle;
    this.LocalData.FormIndex = params.FormIndex;
    // console.log('load Localdata: ', this.LocalData);
    // this.props.navigation.setOptions({
    //   title: this.LocalData.Title,
    // });
    this.InitialSetup();
  }

  DeleteSelectedVoucher = async invoiceid => {
    await DeleteSelectedInvoice(
      'AccountVouchers',
      this.LocalData.FormIndex,
      invoiceid,
      this.state.Loccode,
      this.LocalData.RoleId,
      this.LocalData.UserCode,
      null, // this.EnableLoadScreen,
      this.HandleVoucherType,
    );
    // this.setState({isFetched: true});
  };

  InitialSetup = async () => {
    const {
      get_IP,
      get_Token,
      get_UserCode,
      get_Location,
      get_FinancialYearId,
      get_RoleId,
    } = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.setState({Loccode: get_Location});
    this.LocalData.RoleId = get_RoleId;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.FinancialYearId = get_FinancialYearId;
    this.LoadAllPart();
  };
  HandleVoucherType = () => {
    let uri = `${this.IP}/api/Accounts/AllTransVouchers?_MainVoucherTypeID=${this.state.MasterVoucherTypeId}&_GetVoucherTypeID=${this.state.GetVoucherTypeId}&loccode=${this.state.Loccode}&FinancialId=${this.LocalData.FinancialYearId}`;
    Request.get(uri, this.Token)
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            this.setState({
              NoDataAvailable: false,
              isFetched: true,
              Voucher: res.data,
            });
          } else {
            this.setState({
              isFetched: true,
              Voucher: [],
              NoDataAvailable: true,
            });
          }
        }
      })
      .catch(err => {
        console.error(err);
        AlertMessage('Something Went wrong\n' + JSON.stringify(err));
        this.setState({isFetched: true});
      });
  };
  LoadAllPart = async () => {
    let params = this.props.route.params;
    this.setState({MasterVoucherTypeId: params.CallFrom});
    Request.get(
      `${this.IP}/api/Accounts/GetInitalData?_MainVoucherTypeID=${this.state.MasterVoucherTypeId}&RoleId=${this.LocalData.RoleId}&empid=${this.LocalData.UserCode}&loccode=${this.state.Loccode}&FinancialId=${this.LocalData.FinancialYearId}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          this.setState(
            {
              VoucherType: res.data.VoucherType || [],
              Location: res.data.Location || [],
              isFetched: true,
              GetVoucherTypeId: res.data.VoucherType[0].VoucherTypeId || 0,
            },
            () => {
              this.HandleVoucherType();
            },
          );
        }
      })
      .catch(err => {
        console.error(err);
        AlertMessage('Something Went wrong\n' + JSON.stringify(err));
        this.setState({isFetched: true});
      });
  };

  NavigateToEditVoucher = ID => {
    // console.log('is', ID);
    this.props.navigation.navigate('AddTransVoucherNavigator', {
      HeaderTitle: this.LocalData.Title,
      MVoucherType: this.state.MasterVoucherTypeId,
      GVoucherType: this.state.GetVoucherTypeId,
      VoucherId: ID,
      Loccode: this.state.Loccode,
      FinanceId: this.LocalData.FinancialYearId,
      RefrshVoucherList: this.HandleVoucherType,
    });
    // console.log(ID);
  };
  clearInput = () => {
    this.HandleSearch('');
  };
  HandleSearch = async SearchText => {
    this.setState({SearchText});
  };

  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={styles.closeButtonParent}
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  FieldLocation = key => (
    <ModalSearchablePicker
      key={key}
      ref={'Location'}
      placeholder="Location"
      data={this.state.Location}
      labelProp="LocationName"
      valueProp="LocationCode"
      selectedValue={this.state.Loccode}
      onValueSelected={Loccode => this.setState({Loccode})}
    />
  );
  FieldVoucherType = key => (
    <ModalSearchableLabel
      placeholder="Voucher"
      fieldWrapperStyle={[
        styles.CardTextContainer,
        {width: '100%', marginHorizontal: 0, marginVertical: 0, marginTop: 10},
      ]}
      data={this.state.VoucherType}
      labelProp="VoucherTypeName"
      valueProp="VoucherTypeId"
      inputStyle={[
        {
          marginTop: 5,
          paddingBottom: 0,
          borderBottomWidth: 0,
          paddingTop: 0,
          padding: 0,
          marginHorizontal: 0,
          paddingHorizontal: 0,
          marginBottom: 0,
          height: 'auto',
          textAlign: 'center',
          textDecorationLine: 'underline',
        },
        ApplyStyleFontAndSizeAndColor(
          globalFontObject.Font.Bold,
          globalFontObject.Size.small.sl,
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.Color,
        ),
      ]}
      selectedValue={this.state.GetVoucherTypeId}
      onValueSelected={(GetVoucherTypeId, l_lebel, l_index, l_item) => {
        this.setState({GetVoucherTypeId}, this.HandleVoucherType);
        this.LocalData.title = l_lebel;
        this.LocalData.FormIndex = `${l_item.PINDEX}.${l_item.CINDEX}.${l_item.GCINDEX}`;
      }}
    />
  );
  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }

    return (
      <>
        <View
          style={[
            styles.PageContainer,
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}>
          <View
            style={[
              {
                width: '100%',
                flexDirection: 'row',
                padding: 5,
              },
            ]}>
            <View style={[styles.SearhBarWrapper, {width: '60%'}]}>
              <TextInput
                style={[
                  styles.SearchInput,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                  ApplyStyleColor(
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BorderColor,
                  ),
                ]}
                placeholder={'Search here'}
                value={this.state.SearchText}
                onChangeText={this.HandleSearch}
                autoCorrect={false}
                returnKeyType="search"
                autoCapitalize="none"
                onFocus={this.showCancel}
                // onSubmitEditing={this.search}
              />
              {/* {this.renderTouchableHighlight()} */}
            </View>

            <ScrollView style={styles.Container}>
              {this.FieldVoucherType()}
            </ScrollView>
          </View>

          <FlatList
            style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            data={this.state.Voucher}
            ListHeaderComponent={() => <ListItemHeader />}
            renderItem={({item}) => (
              <ListItem
                data={item}
                type={this.state.PartyType}
                onPdfClick={this.CallShareFile}
                onDeleteInvoice={this.DeleteSelectedVoucher}
                onLongPress={this.NavigateToEditVoucher}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.isRefresh}
            stickyHeaderIndices={[0]}
            onRefresh={this.LoadAllPart}
          />

          {/*           
        <View
          style={[
            styles.container,
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}> */}
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.LightGrayColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {
                    textAlign: 'center',
                    // marginTop: 20,
                  },
                ]}>
                No Data Found
              </Text>
              <Text
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sxxl,
                    globalColorObject.Color.LightGrayColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {
                    textAlign: 'center',
                    marginTop: 5,
                  },
                ]}>
                Pull to refresh
              </Text>
            </View>
          )}
          {/* 
        </View> */}

          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.props.navigation.navigate('AddTransVoucherNavigator', {
                HeaderTitle: this.LocalData.Title,
                MVoucherType: this.state.MasterVoucherTypeId,
                GVoucherType: this.state.GetVoucherTypeId,
                VoucherId: 0,
                Loccode: this.state.Loccode,
                FinanceId: this.LocalData.FinancialYearId,
                RefrshVoucherList: this.HandleVoucherType,
              });
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
        </View>
        {/* 
        <View
          style={[
            styles.container,
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}>
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={{
                  fontSize: 25,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 20,
                }}>
                No Data Found
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 5,
                }}>
                Pull to refresh
              </Text>
            </View>
          )}

          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.props.navigation.navigate('AddTransVoucherNavigator', {
                HeaderTitle: this.LocalData.Title,
                MVoucherType: this.state.MasterVoucherTypeId,
                GVoucherType: this.state.GetVoucherTypeId,
                Voucherid: 0,
                Loccode: this.state.Loccode,
                FinanceId: this.LocalData.FinancialYearId,
              });
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
          //<FloatingAction
						//actions={this.ActionButtons}
						//onPressItem={this.HandleActionButtonClick}
						//// color="rgba(50,74,84,1)" iconWidth={20} iconHeight={20}
						//color="#47b5ad" iconWidth={20} iconHeight={20}
					///>
        </View>
      */}
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  NoDataBanner: {
    // backgroundColor:'gray',
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    // position: 'absolute',
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 9,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '50%',
    marginVertical: 3,
  },
  PageContainer: {
    flex: 1,
    padding: 5,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  SearhBarWrapper: {
    padding: 5,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  closeButton: {
    height: 30,
    width: 30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 10,
    top: 10,
  },

  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});
// export {Retail};
