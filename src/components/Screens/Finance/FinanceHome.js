import React, { Component } from 'react';
import {
	View, SafeAreaView, StatusBar, Text, StyleSheet, ScrollView, Image, TouchableOpacity
} from 'react-native';

export default class SalesHome extends Component {

	NavigateToForm = (nav, HeaderTitle, CallFrom) => {
		console.log(nav, HeaderTitle, CallFrom)
		this.props.navigation.navigate(nav, {
			HeaderTitle,
			CallFrom
		});
	}



	render() {
		return (
			<>
				<SafeAreaView style={{ flex: 1 }}>
					<View style={styles.LoadContainer}>
						<ScrollView style={styles.DataContainer}>
							<View style={styles.ListContainer}>
								<TouchableOpacity style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"TransNav", "Payment Voucher",4)}>
									<Image
										source={require('../../../assets/images/Home/Finance/payment.png')}
										style={styles.FormImages}
									/>
									<Text style={styles.FormLabel}>Payment</Text>
									<Text style={styles.FormLabel}>Voucher</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.FormContainer} onPress={this.NavigateToForm.bind(this,"TransNav", "Receipt Voucher",5)}>
									<Image
										source={require('../../../assets/images/Home/Finance/receipt.png')}
										style={styles.FormImages}
									/>
									<Text style={styles.FormLabel}>Receipt</Text>
									<Text style={styles.FormLabel}>Voucher</Text>
								</TouchableOpacity>
								<TouchableOpacity style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "TransNav","Contra Voucher",3)}>
									<Image
										source={require('../../../assets/images/Home/Finance/Contra.png')}
										style={styles.FormImages}
									/>
									<Text style={styles.FormLabel}>Contra</Text>
									<Text style={styles.FormLabel}>Voucher</Text>
								</TouchableOpacity>

							</View>
							
						</ScrollView>
					</View>
					<View style={{ backgroundColor: 'rgba(0,0,0,0)', position: 'absolute', bottom: 0, right: 0 }}>
						{/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
						<View style={{ alignItems: 'flex-end', marginRight: 20, flexDirection: 'row', marginBottom: 5 }}>
							<Text style={{ textAlign: 'right'}}>Powered By</Text>
							<Image
								source={require('../../../assets/images/Drawer/Logo.png')}
								style={{ height: 30, width: 100 }}
							/>
						</View>
					</View>
				</SafeAreaView>
			</>
		);
	}
};


const styles = StyleSheet.create({
	Text: {
		marginTop: '80%',
		textAlign: 'center',
		fontSize: 22,
		fontWeight: 'bold',
	},
	LoadContainer: {
		// backgroundColor: 'rgb(43, 54, 255)',
		backgroundColor: '#9b7edc',
		flex: 1
	},
	FormImages: {
		width: 30,
		height: 30,
		marginLeft: 5,
		marginRight: 5,
		marginBottom: 5,
		marginTop:5
	},
	FormLabel: {
		textAlign: 'center',
		fontSize: 16,
		// fontWeight: 'bold',
	},
	DataContainer: {
		backgroundColor: 'white',
		marginTop: 30,
		// marginLeft: 5,
		// marginRight: 5,
		borderTopLeftRadius: 35,
		borderTopRightRadius: 35,
		flex: 1,
		width: '100%',
		elevation: 50,
	},
	ListContainer: {
		marginTop: 10,
		// marginLeft:20,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		width: '100%',
		marginBottom: 5,
		flexWrap: 'wrap',
	},
	FormContainer: {
		// backgroundColor: 'rgb(252, 172, 182)',
		backgroundColor: 'white',
		marginBottom:10,
		padding: 5,
		height:100,
		width:100,
		alignItems:'center',
		borderRadius: 10,
		elevation: 15,
	},
});