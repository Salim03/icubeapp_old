import React, {Component} from 'react';
import {
  Alert,
  View,
  SafeAreaView,
  StatusBar,
  Text,
  Switch,
  StyleSheet,
  Image,
  Keyboard,
  FlatList,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
  Animated,
  PermissionsAndroid,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {Button} from '../../Custom/Button';
import RichTextInput from '../../Custom/RichTextInput';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';

import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import {
  Request,
  AlertMessage,
  GetSessionData,
  isnull,
  ReturnDisplayStringForDate,
  SqlDateFormat,
  isnull_number,
  nullif_Number,
} from '../../../Helpers/HelperMethods';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import ModalDatePicker from '../../Custom/ModalDatePicker';
// import RNFetchBlob from 'rn-fetch-blob';
import {
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  globalFontObject,
  ApplyStyleColor,
  globalBorderWidth,
  globalColorObject,
} from '../../../Style/GlobalStyle';

const ListItemMain = props => {
  return (
    <View
      key={`addledgetList_${isnull(props.data.LedgerID, '0_0')}_${props.index}`}
      style={[
        layoutStyle.FieldLayout,
        layoutStyle.FieldContainer,
        {
          flexDirection: 'row',
          marginLeft: 0,
        },
      ]}>
      <ModalSearchablePicker
        placeholder={`Add Ledger ${props.index + 1}`}
        fieldWrapperStyle={{width: '64%'}}
        data={props.LedgerList || []}
        labelProp="Name"
        valueProp="ID"
        selectedValue={props.data.LedgerID || ''}
        onValueSelected={(value, label, index, item) => {
          props.onLedgerChange('LedgerId', props.index, value, item);
        }}
      />
      <View style={{width: '34%', flexDirection: 'row'}}>
        <RichTextInput
          placeholder="Amount"
          value={isnull(nullif_Number(props.data.Amount, 0), '')}
          wrapperStyle={{width: '80%'}}
          onChangeText={Amount => {
            props.onAmountChange('Amount', props.index, Amount.toString());
          }}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
        <TouchableOpacity
          style={{width: '20%', alignSelf: 'center'}}
          onPress={() => props.onPress()}
          disabled={!props.IsEnable}>
          <Image
            source={require('../../../assets/images/Delete.png')}
            style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '30%',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'center'},
            ]}>
            Factor
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '30%',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'center'},
            ]}>
            Unit
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
};

const DenominListItem = props => {
  let isDoubleCard = false;
  return (
    <View
      style={[
        styles.Card,
        isDoubleCard && {maxWidth: '49%'},
        ApplyStyleColor(
          globalColorObject.Color.oppPrimary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '30%',
              flexDirection: 'row',
              alignSelf: 'center',
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'center', width: '85%'},
            ]}>
            {props.data.Factor}
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'center', width: '15%'},
            ]}>
            {' *'}
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <RichTextInput
            isShowAnimateText={false}
            placeholder=" "
            value={isnull(nullif_Number(props.data.Unit, 0), '')}
            wrapperStyle={{width: '100%', marginVertical: 0}}
            inputStyle={{
              marginTop: 0,
              textAlign: 'center',
              backgroundColor: globalColorObject.Color.Lightprimary,
              // borderBottomWidth: 1,
              // borderBottomColor: globalColorObject.Color.Primary,
            }}
            onChangeText={Unit => {
              props.onAmountChange('Denomin', props.index, Unit.toString());
            }}
            inputProps={{
              keyboardType: 'phone-pad',
            }}
          />
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {width: '40%', flexDirection: 'row', alignSelf: 'center'},
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'center', width: '15%'},
            ]}>
            {' ='}
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right', width: '85%'},
            ]}>
            {(
              Math.round((props.data.Factor * props.data.Unit || 0) * 100) / 100
            ).toFixed(2)}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default class AddTransactionVoucher extends Component {
  constructor(props) {
    super(props);
    this.LocalData = {
      Title: '',
      MainVoucherTypeID: 0,
      VoucherTypeId: 0,
      RoleId: '',
      LocationCode: 0,
      FinacialYearId: 0,
      VoucherId: 0,
      UserCode: '',
    };

    // Local Object to store all the navigation and other informations
    this.APILink = {
      API_VoucherSave: '/api/Accounts/TransactionVoucherSave',
      API_LoadInitialData: '/api/Accounts/TRVoucher_InitialData?', //CallFrom=PO&LocCode=1
      API_SelectedData: '/api/Accounts/TRVoucher_SelectedData?',
      API_CashDenomin: '/api/Accounts/GetCashDenomin?',
      API_GetRefNoList: '/api/Accounts/GetRefNoList?',
    };
    this.state = {
      isFetched: false,
      // Holds the data to be set to controls ( filled later by the data from API )
      APIData: {
        LedgerByList: [],
        LedgerToList: [],
        BankAndCashLedgerList: [],
        RefNoList: [],
        // AccountByList: [],
        // AccountToList: [],
        // SubAccountByList: [],
        // SubAccountToList: [],
        // AdditionalGeneralLedgerList: [],
        // AdditionalSubLedgerList: [],
        // RefAllSubLedgerList: [],
        RefTypeList: [],
        LedgerList: [],
        VoucherDenominList: [],
      },
      // Hold selected or entered values of controls

      LedgerById: '0_0',
      LedgerToId: '0_0',

      isDenomin: false,
      IsValidateRefNo: true,
      VoucherNo: '',
      VoucherDate: '',
      FavourOf: '',
      Narration: '',
      AccountBy: '',
      AccountByName: '',
      AccountTo: '',
      AccountToName: '',
      SubAccountBy: '',
      SubAccountTo: '',
      isAdditional: true,
      // AdditionalGeneralLedger: '',
      // AdditionalSubLedger: '',
      // RefSubLedger: '',
      RefType: '',
      RefId: 0,
      RefNo: '',
      RefDate: '',

      AccountBy_AgainstInfoList: [],
      AccountTo_AgainstInfoList: [],

      // AccountBy_AgainstIDList: '',
      // AccountTo_AgainstIDList: '',
      // AccountBy_AgainstSignList: '',
      // AccountTo_AgainstSignList: '',
      // AccountBy_AgainstAmountList: '',
      // AccountTo_AgainstAmountList: '',
      // AccountBy_AgainstAdjusedAmountList: '',
      // AccountTo_AgainstAdjusedAmountList: '',
      // AccountBy_AgainstDiscountAmountList: '',
      // AccountTo_AgainstDiscountAmountList: '',
      // AccountBy_AgainstTDSAmountList: '',
      // AccountTo_AgainstTDSAmountList: '',
      // AccountBy_AgainstOthersAmountList: '',
      // AccountTo_AgainstOthersAmountList: '',

      ByNewRefAmount: 0.0,
      ByAgainstRefAmount: 0.0,
      ToNewRefAmount: 0.0,
      ToAgainstRefAmount: 0.0,
      AdditionalAmount: 0.0,
      NetAmount: '0.00',
      BasicAmount: '0.00',
      Extinct: false,

      isHistory: false,
      isPostedInfo: false,

      FreezedContentHeight: new Animated.Value(40),
      isFreezedContentOpen: false,
      isCallFor: false,
      OpenAdditionalLedger: false,
      OpenDenomin: false,
    };
  }
  ToggleFreezedContent = () => {
    Animated.timing(this.state.FreezedContentHeight, {
      toValue: this.state.isFreezedContentOpen ? 40 : 250,
      duration: 250,
    }).start();
    this.setState({isFreezedContentOpen: !this.state.isFreezedContentOpen});
  };
  componentDidMount() {
    let {params} = this.props.route;
    // console.log(
    //   'date : ',
    //   ReturnDisplayStringForDate(new Date(), SqlDateFormat),
    // );
    // this.LocalData[0].Title = 3;
    this.LocalData.Title = params.HeaderTitle;
    this.LocalData.MainVoucherTypeID = params.MVoucherType;
    this.LocalData.VoucherTypeId = params.GVoucherType;
    this.LocalData.VoucherId = params.VoucherId;
    this.LocalData.LocationCode = params.Loccode;
    this.LocalData.FinacialYearId = params.FinanceId;
    this.props.navigation.setOptions({
      title: this.LocalData.Title,

      headerRight: () => (
        <View style={styles.headerContainer}>
          <View style={styles.headerIconsContainer}>
            {/* <TouchableOpacity onPress={this.HandleClearData}>
              <Image
                source={require('../../../assets/images/Clear.png')}
                style={styles.headerIcon}
              />
            </TouchableOpacity> */}
            <TouchableOpacity onPress={this.SaveVoucher}>
              <Image
                source={require('../../../assets/images/save-light.png')}
                style={styles.headerIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
      ),
    });

    this.LoadInitialData();
  }

  HandleAddition = (Type, index, Value, item) => {
    if (Type === 'Amount') {
      let getledgerList = this.state.APIData.LedgerList;
      getledgerList[index].Amount = Value;
      let AdditionalAmouont = Math.round(
        getledgerList
          .reduce((accum, item) => accum + Number(item.Amount), 0)
          .toFixed(2),
      );
      this.setState(prevState => ({
        APIData: {...prevState.APIData, LedgerList: getledgerList},
        AdditionalAmount: AdditionalAmouont,
        NetAmount: parseInt(this.state.BasicAmount) + AdditionalAmouont,
      }));
    } else if (Type === 'LedgerId') {
      let get_LedgerList = this.state.APIData.LedgerList;
      get_LedgerList[index].GLID = item.GLID;
      get_LedgerList[index].SLID = item.SLID;
      get_LedgerList[index].LedgerID = item.ID;
      this.setState(prevState => ({
        APIData: {
          ...prevState.APIData,
          LedgerList: get_LedgerList,
        },
      }));
    } else if (Type === 'Denomin') {
      let get_DenominList = this.state.APIData.VoucherDenominList;
      get_DenominList[index].Unit = Value;
      this.setState(prevState => ({
        APIData: {
          ...prevState.APIData,
          VoucherDenominList: get_DenominList,
        },
      }));
    }
  };

  GetVoucherDenomin = () => {
    // console.log(
    //   'get',
    //   `${this.IP}${this.APILink.API_CashDenomin}MasterID=${this.LocalData.VoucherId}`,
    // );
    Request.get(
      `${this.IP}${this.APILink.API_CashDenomin}MasterID=${this.LocalData.VoucherId}`,
      this.Token,
    )
      .then(res => {
        // console.log(res);
        this.setState(prevState => ({
          isFetched: true,
          APIData: {
            ...prevState.APIData,
            VoucherDenominList: res.data,
          },
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };

  NavigateBackToList = () => {
    this.props.route.params.RefrshVoucherList();
    this.props.navigation.goBack();
  };

  isAllowSave = () => {
    let cn = false;
    try {
      // DataView dv = new DataView();
      // if (ts_AdditionalCharges.IsOn)
      //     dv = new DataView(((DataTable)dgv_Additional.DataSource), "Amount <= 0 AND (GLID <> 0)", "", DataViewRowState.CurrentRows);
      if (isnull(this.LocalData.VoucherTypeId, '') == '') {
        AlertMessage('Select Voucher Type.');
      } else if (isnull(this.state.VoucherDate, '') == '') {
        AlertMessage('Select Voucher Date.');
      }
      // else if (this.state.BasicAmount != Convert.ToDecimal(ERPModule.isnull(Convert.ToString(txtAmount.EditValue), '0')))
      // {
      //   AlertMessage('Refresh Amount.', 'iCube - Alert');
      // }
      else if (isnull_number(this.state.BasicAmount, 0) < 0) {
        AlertMessage('Amount should not be less than Zero.');
      }
      // else if ((ERPModule.isnull(Convert.ToString(Srch_ReferanceType.EditValue), '') == '') &&
      //     Srch_ReferanceType.Enabled)
      // {
      //     iCubeUtilities.Utilities.ShowMessageBox('Select Reference Type.', 'iCube - Alert', MessageBoxButtons.OK, MessageBoxIcon.Information);
      //     Srch_ReferanceType.Focus();
      // }
      // else if ((ERPModule.isnull(Convert.ToString(Srch_ReferanceType.EditValue), '') == 'Cheque') && IsValidateRefNo &&
      //                 (Convert.ToInt32(ERPModule.isnull(Convert.ToString(txtCHQ.SelectedValue), '0')) == 0))
      // {
      //     iCubeUtilities.Utilities.ShowMessageBox('Invalid Reference Number.', 'iCube - Alert', MessageBoxButtons.OK, MessageBoxIcon.Information);
      //     txtCHQ.Focus();
      // }
      else if (
        isnull(this.state.RefType, '') != '' &&
        isnull(this.state.RefNo, '') == '' &&
        isnull(this.state.RefType, '') != 'Deposit' &&
        isnull(this.state.RefType, '') != 'Withdraw'
      ) {
        AlertMessage('Enter Reference Number.');
      } else if (
        isnull(this.state.RefType, '') != '' &&
        isnull(this.state.RefDate, '') == '' &&
        isnull(this.state.RefType, '') != 'Deposit' &&
        isnull(this.state.RefType, '') != 'Withdraw'
      ) {
        AlertMessage('Enter Reference Date.');
      } else if (isnull(this.state.AccountBy, '0') == '0') {
        AlertMessage('Select LedgerBy.');
      } else if (isnull(this.state.AccountTo, '0') == '0') {
        AlertMessage('Select LedgerTo.');
      } else if (
        isnull(this.state.AccountBy, '0') == isnull(this.state.AccountTo, '0')
      ) {
        AlertMessage("Can't tag the same ledger.");
      }
      // else if ((ERPModule.isnull(Convert.ToString(srchSubLedgerBy.EditValue), '0') == '0') &&
      //     (Convert.ToBoolean(ERPModule.isnull(Convert.ToString(srchSubLedgerBy.Properties.Tag), 'False'))))
      // {
      //     iCubeUtilities.Utilities.ShowMessageBox('Select SubLedgerBy', 'iCube - Alert', MessageBoxButtons.OK, MessageBoxIcon.Information);
      //     srchSubLedgerBy.Focus();
      // }
      // else if ((ERPModule.isnull(Convert.ToString(srchSubLedgerTo.EditValue), '0') == '0') &&
      //     (Convert.ToBoolean(ERPModule.isnull(Convert.ToString(srchSubLedgerTo.Properties.Tag), 'False'))))
      // {
      //     iCubeUtilities.Utilities.ShowMessageBox('Select SubLedgerTo', 'iCube - Alert', MessageBoxButtons.OK, MessageBoxIcon.Information);
      //     srchSubLedgerTo.Focus();
      // }
      // else if ((
      //   isnull(this.state.SubAccountBy, '0') != '0' &&
      //     isnull(
      //       this.state.SubAccountBy,
      //       'False',
      //     )
      //   ) &&
      //   this.state.BasicAmount !=
      //     this.state.ByNewRefAmount + this.state.ByAgainstRefAmount
      // ) {
      //   AlertMessage("Total amount does't match with ledgerby Referance amount.");
      // }
      // else if ((
      //   isnull(this.state.SubAccountTo, '0') != '0' &&
      //     isnull(
      //       this.state.SubAccountTo,
      //       'False',
      //     )
      //   ) &&
      //   this.state.BasicAmount !=
      //     this.state.ToNewRefAmount + this.state.ToAgainstRefAmount
      // ) {
      //   AlertMessage("Total amount does't match with ledgerto Referance amount.");
      // }
      else if (
        isnull_number(this.state.ByNewRefAmount, 0) < 0 ||
        isnull_number(this.state.ByAgainstRefAmount, 0) < 0
      ) {
        AlertMessage("By Reference Value should't less than zero.");
      } else if (
        isnull_number(this.state.ToNewRefAmount, 0) < 0 ||
        isnull_number(this.state.ToAgainstRefAmount, 0) < 0
      ) {
        AlertMessage("To Reference Value should't less than zero.");
      } else if (
        (this.LocalData.MainVoucherTypeID == 3) &
        (isnull_number(this.state.AdditionalAmount, 0) > 0)
      ) {
        AlertMessage("Can't Tag additional charges for contra vouchers.");
      } else if (
        isnull_number(this.state.BasicAmount, 0) +
          isnull_number(this.state.AdditionalAmount, 0) !=
        isnull_number(this.state.NetAmount, 0)
      ) {
        AlertMessage('Amount total mismatched.');
      }
      // else if (
      //   Convert.ToDecimal(
      //     ERPModule.isnull(Convert.ToString(txtAmount.EditValue), '0'),
      //   ) +
      //     ReturnAdditinalAmountTotal() !=
      //   _Account_NetAmount
      // ) {
      //   iCubeUtilities.Utilities.ShowMessageBox(
      //     'Amount total mismatched.',
      //     'iCube - Alert',
      //     MessageBoxButtons.OK,
      //     MessageBoxIcon.Information,
      //   );
      //   txtAmount.Focus();
      // } else if (dv.Count > 0) {
      //   iCubeUtilities.Utilities.ShowMessageBox(
      //     'Entered Additonal Amount should be greater than zero.',
      //     'iCube - Alert',
      //     MessageBoxButtons.OK,
      //     MessageBoxIcon.Information,
      //   );
      //   dgv_Additional.Focus();
      // } else cn = ValidateLimitation();
      else {
        cn = true;
      }
    } catch (ex) {}
    return cn;
  };

  CSaveVoucher = async () => {
    this.setState({isFetched: false});
    // console.log('save', this.state.APIData.VoucherDenominList);
    let SaveObject = {
      MainVoucherTypeID: this.LocalData.MainVoucherTypeID,
      GetVoucherTypeID: this.LocalData.VoucherTypeId,
      SelectedLoccode: this.LocalData.LocationCode,
      decCurrentFinancialYearId: this.LocalData.FinacialYearId,
      ID: this.LocalData.VoucherId,
      VoucherTypeID: this.LocalData.VoucherTypeId,
      VoucherNo: this.state.VoucherNo,
      VoucherDate: this.state.VoucherDate || new Date().toLocaleDateString(),
      AccountBy: this.state.AccountBy,
      AccountTo: this.state.AccountTo,
      SubAccountBy: this.state.SubAccountBy,
      SubAccountTo: this.state.SubAccountTo,
      RefType: this.state.RefType,
      RefId: this.state.RefId,
      RefNo: this.state.RefNo,
      RefDate: this.state.RefDate,
      BasicAmount: this.state.BasicAmount,
      AdditionalAmount: this.state.AdditionalAmount,
      NetAmount: this.state.NetAmount,
      FavourOf: this.state.FavourOf,
      Naration: this.state.Narration,
      Extinct: this.state.Extinct,
      IsPost: this.state.isPostedInfo,

      ByAgainstRefAmount: this.state.ByAgainstRefAmount,
      ByNewRefAmount: this.state.ByNewRefAmount,
      ToAgainstRefAmount: this.state.ToAgainstRefAmount,
      ToNewRefAmount: this.state.ToNewRefAmount,

      ByAgainstList: this.state.AccountBy_AgainstInfoList,
      ToAgainstList: this.state.AccountTo_AgainstInfoList,

      // ByAgainstIdList: this.state.AccountBy_AgainstIDList,
      // ToAgainstIdList: this.state.AccountTo_AgainstIDList,

      // ByAgainstSignList: this.state.AccountBy_AgainstSignList,
      // ToAgainstSignList: this.state.AccountTo_AgainstSignList,

      // ByAgainstAmountList: this.state.AccountBy_AgainstAmountList,
      // ToAgainstAmountList: this.state.AccountTo_AgainstAmountList,

      // ByAgainstAdjusedAmountList: this.state.AccountBy_AgainstAdjusedAmountList,
      // ToAgainstAdjusedAmountList: this.state.AccountTo_AgainstAdjusedAmountList,

      // ByAgainstDiscountAmountList:
      //   this.state.AccountBy_AgainstDiscountAmountList,
      // ToAgainstDiscountAmountList:
      //   this.state.AccountTo_AgainstDiscountAmountList,

      // ByAgainstTDSAmountList: this.state.AccountBy_AgainstTDSAmountList,
      // ToAgainstTDSAmountList: this.state.AccountTo_AgainstTDSAmountList,

      // ByAgainstOthersAmountList: this.state.AccountBy_AgainstOthersAmountList,
      // ToAgainstOthersAmountList: this.state.AccountTo_AgainstOthersAmountList,
      // //Unused in mobile application
      CustomField1: '',
      CustomField2: '',
      CustomField3: '',
      CustomField4: '',
      CustomField5: '',
      CFCombo1: '',
      CFCombo2: '',
      CFRadio1: false,
      CFRadio2: false,
      CFDate1: '1900-01-01',
      CFDate2: '1900-01-01',
      // Default
      AgainstType: 'Vouchers',
      AgainstMasterId: 0,
      AgainstDetailIdList: '',
      AgainstAutoNo: '',
      AgainstAutoDate: '1900-01-01',

      loccode: this.LocalData.LocationCode,
      InvoiceID: this.LocalData.VoucherId,
      Type: this.state.isDenomin ? 'Denomination' : '',
      TVAdditionalChargesMaster: this.state.APIData.LedgerList,
      TVVoucherDenomin: this.state.APIData.VoucherDenominList,
    };
    // console.log('save object : ', SaveObject);
    Request.post_ReturnObject(
      `${this.IP}${this.APILink.API_VoucherSave}`,
      SaveObject,
      this.Token,
    )
      .then(res => {
        let GetresData = res.data;
        if (res.status === 200) {
          if (GetresData && GetresData.Message == 'Saved') {
            AlertMessage(
              GetresData.Description
                ? GetresData.Description
                : 'saved successfully.',
            );
            this.NavigateBackToList();
            return true;
          } else {
            AlertMessage(
              GetresData.Description
                ? GetresData.Description
                : 'Failed to save.',
            );
          }
        } else {
          AlertMessage(
            GetresData.Description
              ? GetresData.Description
              : `Failed to save : \n ${JSON.stringify(GetresData)}`,
          );
        }
        return false;
      })
      .catch(err => {
        AlertMessage('Could not save \n' + err);
        this.setState({isFetched: true, ShowModalLoader: false});
      });
    this.setState({ShowModalLoader: true, ModalLoaderText: 'Saving Data...'});
  };
  isCashLedger = () => {
    var get_CashLedger = (
      this.state.APIData.BankAndCashLedgerList || []
    ).filter(el => {
      return (
        (el.GLID == this.state.AccountBy || el.GLID == this.state.AccountTo) &&
        el.LedgerType == 'Cash'
      );
    });
    return get_CashLedger.length > 0;
  };
  isBankLedger = () => {
    // let IsBank = false;
    // if(passLedgetId != undefined)
    // {
    //   console.log('pass data ledger for bank');
    //   IsBank = ((
    //     this.state.APIData.BankAndCashLedgerList || []
    //   ).filter(el => {
    //     return (el.GLID == passLedgetId && el.LedgerType == 'Bank' );
    //   })).length > 0
    // }
    // else
    // {
    //   IsBank = ((
    //     this.state.APIData.BankAndCashLedgerList || []
    //   ).filter(el => {
    //     return (
    //       (el.GLID == this.state.AccountBy || el.GLID == this.state.AccountTo) &&
    //       el.LedgerType == 'Bank'
    //     );
    //   })).length > 0
    // }
    // return IsBank
    return (
      (this.state.APIData.BankAndCashLedgerList || []).filter(el => {
        return el.GLID == this.state.AccountBy && el.LedgerType == 'Bank';
      }).length > 0
    );
  };

  HandleDenomin = () => {
    let gettoatalvalue = 0;
    var Denomin = this.state.APIData.VoucherDenominList.filter(el => {
      if (el.Unit > 0) {
        gettoatalvalue =
          gettoatalvalue +
          isnull_number(el.Factor, 0) * isnull_number(el.Unit, 0);
      }
      return el.Unit > 0;
    });
    // console.log('denomin value : ', gettoatalvalue, ', data :', Denomin);
    if (gettoatalvalue != isnull_number(this.state.NetAmount, 0)) {
      AlertMessage(
        `Entered denomination must be equal to Total Amount [ ${this.state.NetAmount} ]. `,
      );
    } else if (this.isAllowSave()) {
      this.setState(
        prevState => ({
          isFetched: false,
          isDenomin: true,
          OpenDenomin: false,
          APIData: {
            ...prevState.APIData,
            VoucherDenominList: Denomin,
          },
        }),
        () => {
          this.CSaveVoucher();
        },
      );
    }
  };
  SaveVoucher = async () => {
    Keyboard.dismiss();
    if (this.isAllowSave()) {
      this.setState({isFetched: false});
      if (this.LocalData.MainVoucherTypeID == 3 && this.isCashLedger()) {
        this.GetVoucherDenomin();
        this.setState({OpenDenomin: true});
      } else {
        this.CSaveVoucher();
      }
    }
  };
  _menu = null;
  setMenuRef = ref => {
    this._menu = ref;
  };
  hideMenu = () => {
    this._menu.hide();
  };
  showMenu = () => {
    this._menu.show();
  };
  HandleDateChange = (name, date) => {
    // console.log(name, date);
    this.setState({[name]: date});
  };

  HandleClearData = () => {
    let get_VoucherDate = ReturnDisplayStringForDate(new Date(), SqlDateFormat);
    this.setState({
      VoucherNo: '',
      VoucherDate: get_VoucherDate,
      Date: '',
      FavourOf: '',
      Narration: '',
      AccountBy: '',
      AccountTo: '',
      SubAccountBy: '',
      SubAccountTo: '',
      isAdditional: true,
      // AdditionalGeneralLedger: '',
      // AdditionalSubLedger: '',
      // RefSubLedger: '',
      RefType: '',
      RefId: 0,
      RefNo: '',
      RefDate: '',

      AccountBy_AgainstInfoList: [],
      AccountTo_AgainstInfoList: [],

      // AccountBy_AgainstIDList: '',
      // AccountTo_AgainstIDList: '',
      // AccountBy_AgainstSignList: '',
      // AccountTo_AgainstSignList: '',
      // AccountBy_AgainstAmountList: '',
      // AccountTo_AgainstAmountList: '',
      // AccountBy_AgainstAdjusedAmountList: '',
      // AccountTo_AgainstAdjusedAmountList: '',
      // AccountBy_AgainstDiscountAmountList: '',
      // AccountTo_AgainstDiscountAmountList: '',
      // AccountBy_AgainstTDSAmountList: '',
      // AccountTo_AgainstTDSAmountList: '',
      // AccountBy_AgainstOthersAmountList: '',
      // AccountTo_AgainstOthersAmountList: '',

      ByNewRefAmount: 0.0,
      ByAgainstRefAmount: 0.0,
      ToNewRefAmount: 0.0,
      ToAgainstRefAmount: 0.0,
      AdditionalAmount: 0.0,
      NetAmount: 0.0,
      BasicAmount: 0.0,
      Extinct: false,

      isHistory: false,
      isPostedInfo: false,

      FreezedContentHeight: new Animated.Value(40),
      isFreezedContentOpen: false,
    });
  };

  isChequBookShow = () => {
    return (
      (this.LocalData.MainVoucherTypeID == '3' ||
        this.LocalData.MainVoucherTypeID == '4') &&
      this.state.RefType == 'Cheque' &&
      this.isBankLedger()
    );
  };

  HandleRefNoList = () => {
    if (this.isChequBookShow()) {
      Request.get(
        `${this.IP}${this.APILink.API_GetRefNoList}_MainVoucherTypeID=${this.LocalData.MainVoucherTypeID}&_GetVoucherTypeID=${this.LocalData.VoucherTypeId}&VocuherID=${this.LocalData.VoucherId}&LedgerById=${this.state.AccountBy}&loccode=${this.LocalData.LocationCode}&FinancialId=${this.LocalData.FinacialYearId}`,
        this.Token,
      )
        .then(res => {
          let get_RefNoList = res.data || [];
          this.setState(prevState => ({
            APIData: {
              ...prevState.APIData,
              RefNoList: get_RefNoList,
            },
          }));
        })
        .catch(err => console.error(err));
    } else {
      this.setState(prevState => ({
        APIData: {
          ...prevState.APIData,
          RefNoList: [],
        },
      }));
    }
  };

  HandleSelectedVoucher = Initial => {
    // console.log(
    //   'my selected invoice data : ',
    //   `${this.IP}${this.APILink.API_SelectedData}_MainVoucherTypeID=${this.LocalData.MainVoucherTypeID}&_GetVoucherTypeID=${this.LocalData.VoucherTypeId}&VocuherID=${this.LocalData.VoucherId}&loccode=${this.LocalData.LocationCode}&FinancialId=${this.LocalData.FinacialYearId}`,
    // );
    Request.get(
      `${this.IP}${this.APILink.API_SelectedData}_MainVoucherTypeID=${this.LocalData.MainVoucherTypeID}&_GetVoucherTypeID=${this.LocalData.VoucherTypeId}&VocuherID=${this.LocalData.VoucherId}&loccode=${this.LocalData.LocationCode}&FinancialId=${this.LocalData.FinacialYearId}`,
      this.Token,
    )
      .then(res => {
        // console.log('data summary : ', res.data);
        let get_AdditionalCharges = res.data.AdditionalCharges || [];
        let get_VoucherSummary = res.data.VoucherSummary || {};
        let get_By_AgainstInfoList = res.data.ByAgainstList || [];
        let get_To_AgainstInfoList = res.data.ToAgainstList || [];
        let get_RefNoList = res.data.RefNoList || [];
        // console.log('Temkk',get_AdditionalCharges);
        this.setState(prevState => ({
          isFetched: true,
          VoucherNo: get_VoucherSummary.VoucherNo,
          VoucherDate: get_VoucherSummary.VoucherDate,
          BasicAmount: get_VoucherSummary.BasicAmount,
          FavourOf: get_VoucherSummary.FavourOf,
          Narration: get_VoucherSummary.Naration,
          AccountBy: get_VoucherSummary.AccountBy,
          AccountTo: get_VoucherSummary.AccountTo,
          SubAccountBy: get_VoucherSummary.SubAccountBy,
          SubAccountTo: get_VoucherSummary.SubAccountTo,

          AccountBy_AgainstInfoList: get_By_AgainstInfoList,
          AccountTo_AgainstInfoList: get_To_AgainstInfoList,

          // AdditionalGeneralLedger: '',
          // AdditionalSubLedger: '',
          // RefSubLedger: '',
          RefId: get_VoucherSummary.RefId,
          RefType: get_VoucherSummary.RefType,
          RefNo: get_VoucherSummary.RefNo,
          RefDate: get_VoucherSummary.RefDate,
          VoucherId: this.LocalData.VoucherId,

          ByNewRefAmount: get_VoucherSummary.ByNewRefAmount,
          ByAgainstRefAmount: get_VoucherSummary.ByAgainstRefAmount,
          ToNewRefAmount: get_VoucherSummary.ToNewRefAmount,
          ToAgainstRefAmount: get_VoucherSummary.ToAgainstRefAmount,
          AdditionalAmount: get_VoucherSummary.AdditionalAmount,
          NetAmount: get_VoucherSummary.NetAmount,
          // isAdditional: get_AdditionalCharges.length > 0 ? true : false,
          LedgerById: `${get_VoucherSummary.AccountBy}_${get_VoucherSummary.SubAccountBy}`,
          LedgerToId: `${get_VoucherSummary.AccountTo}_${get_VoucherSummary.SubAccountTo}`,
          APIData: {
            ...prevState.APIData,
            // AccountByList: Initial.data.AccountBy,
            LedgerList: get_AdditionalCharges,
            LedgerByList: Initial.LedgerBy,
            RefNoList: get_RefNoList,
            LedgerToList: Initial.LedgerTo,
            RefTypeList: Initial.ReferanceType,
            BankAndCashLedgerList: Initial.BankAndCashLedgerList,
            // AccountToList: Initial.data.AccountTo,
            // SubAccountByList: Initial.data.SubAccountBy,
            // SubAccountToList: Initial.data.SubAccountTo,
            // AdditionalGeneralLedgerList: Initial.data.AdditionalGeneralLedger,
            // AdditionalSubLedgerList: Initial.data.AdditionalSubLedger,
            // RefAllSubLedgerList: Initial.data.AllSubLedgerList,
          },
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };

  ClearReferanceDetails(IsBy) {
    if (IsBy) {
      this.setState({
        ByNewRefAmount: 0,
        ByAgainstRefAmount: 0,
        AccountBy_AgainstInfoList: [],
        // AccountBy_AgainstIDList: '',
        // AccountBy_AgainstSignList: '',
        // AccountBy_AgainstAdjusedAmountList: '',
        // AccountBy_AgainstDiscountAmountList: '',
        // AccountBy_AgainstTDSAmountList: '',
        // AccountBy_AgainstOthersAmountList: '',
        // AccountBy_AgainstAmountList: '',
      });
    } else {
      this.setState({
        ToNewRefAmount: 0,
        ToAgainstRefAmount: 0,
        AccountTo_AgainstInfoList: [],
        // AccountTo_AgainstIDList: '',
        // AccountTo_AgainstSignList: '',
        // AccountTo_AgainstAdjusedAmountList: '',
        // AccountTo_AgainstDiscountAmountList: '',
        // AccountTo_AgainstTDSAmountList: '',
        // AccountTo_AgainstOthersAmountList: '',
        // AccountTo_AgainstAmountList: '',
      });
    }
  }

  isAllowtoAddExtraLedger = () => {
    return !(
      (this.state.APIData.LedgerList || []).filter(
        rowdata => rowdata.GLID == 0 || rowdata.Amount == '0',
      ).length > 0
    );
  };

  AddRowAddingCharges = () => {
    this.hideMenu();

    let ERR = {ID: 0, GLID: 0, SLID: 0, Amount: '0', LedgerID: '0_0'};
    if (this.state.isAdditional) {
      if (this.isAllowtoAddExtraLedger()) {
        let RR = [...(this.state.APIData.LedgerList || []), ERR];
        this.setState(prevState => ({
          APIData: {
            ...prevState.APIData,
            LedgerList: RR,
          },
        }));
      } else {
        AlertMessage('Fill exist extra ledger info.');
      }
    } else {
      AlertMessage('Switch ON the Additional Chargers');
    }
  };
  HandleBasicAmount = Amount => {
    this.setState({
      BasicAmount: Amount,
      NetAmount: this.state.AdditionalAmount + parseFloat(Amount),
      ByNewRefAmount:
        this.LocalData.VoucherTypeId == 5 ? parseFloat(Amount) : 0.0,
      ToNewRefAmount:
        this.LocalData.VoucherTypeId == 4 ? parseFloat(Amount) : 0.0,
    });
  };

  DeleteAddLedgRow = index => {
    // console.log(index);
    Alert.alert('iCube Alert', 'Are You Sure You Want To Delete', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => {
          let get_LedgerList = this.state.APIData.LedgerList;
          get_LedgerList.splice(index, 1);
          let get_AdditionalAmount = Math.round(
            get_LedgerList
              .reduce((accum, item) => accum + Number(item.Amount), 0)
              .toFixed(2),
          );
          this.setState(prevState => ({
            AdditionalAmount: get_AdditionalAmount,
            NetAmount: parseInt(this.state.BasicAmount) + get_AdditionalAmount,
            APIData: {
              ...prevState.APIData,
              LedgerList: get_LedgerList,
            },
          }));
        },
      },
    ]);
  };

  LoadInitialData = async () => {
    const {
      get_IP,
      get_Token,
      get_UserCode,
      get_Location,
      get_FinancialYearId,
      get_RoleId,
    } = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.RoleId = get_RoleId;
    this.LocalData.UserCode = get_UserCode;
    this.setState({
      VoucherDate: ReturnDisplayStringForDate(new Date(), SqlDateFormat),
    });
    // console.log(
    //   'load initial data : ',
    //   `${this.IP}${this.APILink.API_LoadInitialData}_MainVoucherTypeID=${this.LocalData.MainVoucherTypeID}&_GetVoucherTypeID=${this.LocalData.VoucherTypeId}&RoleId=${this.LocalData.RoleId}&loccode=${this.LocalData.LocationCode}&FinancialId=${this.LocalData.FinacialYearId}`,
    // );
    Request.get(
      `${this.IP}${this.APILink.API_LoadInitialData}_MainVoucherTypeID=${this.LocalData.MainVoucherTypeID}&_GetVoucherTypeID=${this.LocalData.VoucherTypeId}&RoleId=${this.LocalData.RoleId}&loccode=${this.LocalData.LocationCode}&FinancialId=${this.LocalData.FinacialYearId}`,
      this.Token,
    )
      .then(res => {
        // console.log('out initial data : ', res.data);
        if (this.LocalData.VoucherId != 0) {
          this.HandleSelectedVoucher(res.data);
        } else {
          this.setState(prevState => ({
            isFetched: true,
            APIData: {
              ...prevState.APIData,
              LedgerByList: res.data.LedgerBy,
              LedgerToList: res.data.LedgerTo,
              BankAndCashLedgerList: res.data.BankAndCashLedgerList,
              // AccountByList: res.data.AccountBy,
              // AccountToList: res.data.AccountTo,
              // SubAccountByList: res.data.SubAccountBy,
              // SubAccountToList: res.data.SubAccountTo,
              // AdditionalGeneralLedgerList: res.data.AdditionalGeneralLedger,
              RefTypeList: res.data.ReferanceType,
              // AdditionalSubLedgerList: res.data.AdditionalSubLedger,
              // RefAllSubLedgerList: res.data.AllSubLedgerList,
            },
          }));
        }
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };

  IsHavingBillWise = SLID => {
    let getIsHavingBillWise = false;
    // console.log('against number ', isnull_number(SLID, 0));
    if (isnull_number(SLID, 0) > 0) {
      let SubLedgerList =
        this.LocalData.MainVoucherTypeID == 4
          ? this.state.APIData.LedgerToList
          : this.LocalData.MainVoucherTypeID == 5
          ? this.state.APIData.LedgerByList
          : [];
      let ListSourceData = SubLedgerList.filter(obj => {
        let Billwise = isnull(obj.Billwise, 'false').toString().toLowerCase();
        let ID = isnull_number(obj.SLID, 0);
        return Billwise.includes(true) && ID == SLID;
      });
      if (ListSourceData.length > 0) {
        getIsHavingBillWise = true;
      }
    }
    return getIsHavingBillWise;
  };

  HandleAgainstData = (
    NewRefAmount,
    AgainstAmount,
    // AgainstAmountList,
    // AgainstSignList,
    // AgainstAdjusedAmountList,
    // AgainstDiscountAmountList,
    // AgainstTDSAmountList,
    // AgainstOthersAmountList,
    // AgainstIDList,
    AgainstInfoList,
  ) => {
    // console.log(
    //   'complete',
    //   NewRefAmount,
    //   AgainstAmount,
    //   // AgainstAmountList,
    //   // AgainstSignList,
    //   // AgainstAdjusedAmountList,
    //   // AgainstDiscountAmountList,
    //   // AgainstTDSAmountList,
    //   // AgainstOthersAmountList,
    //   // AgainstIDList,
    //   AgainstInfoList,
    // );
    if (this.state.isCallFor) {
      this.setState({
        BasicAmount:
          isnull_number(NewRefAmount, 0) + isnull_number(AgainstAmount, 0),
        NetAmount:
          isnull_number(NewRefAmount, 0) +
          isnull_number(this.state.AdditionalAmount, 0) +
          isnull_number(AgainstAmount, 0),
        ByNewRefAmount: NewRefAmount,
        ByAgainstRefAmount: AgainstAmount,
        AccountBy_AgainstInfoList: AgainstInfoList,
        // AccountBy_AgainstIDList: AgainstIDList,
        // AccountBy_AgainstSignList: AgainstSignList,
        // AccountBy_AgainstAmountList: AgainstAmountList,
        // AccountBy_AgainstAdjusedAmountList: AgainstAdjusedAmountList,
        // AccountBy_AgainstDiscountAmountList: AgainstDiscountAmountList,
        // AccountBy_AgainstTDSAmountList: AgainstTDSAmountList,
        // AccountBy_AgainstOthersAmountList: AgainstOthersAmountList,
      });
    } else {
      this.setState({
        BasicAmount:
          isnull_number(NewRefAmount, 0) + isnull_number(AgainstAmount, 0),
        NetAmount:
          isnull_number(NewRefAmount, 0) +
          isnull_number(this.state.AdditionalAmount, 0) +
          isnull_number(AgainstAmount, 0),
        ToNewRefAmount: NewRefAmount,
        ToAgainstRefAmount: AgainstAmount,
        AccountTo_AgainstInfoList: AgainstInfoList,
        // AccountTo_AgainstIDList: AgainstIDList,
        // AccountTo_AgainstSignList: AgainstSignList,
        // AccountTo_AgainstAmountList: AgainstAmountList,
        // AccountTo_AgainstAdjusedAmountList: AgainstAdjusedAmountList,
        // AccountTo_AgainstDiscountAmountList: AgainstDiscountAmountList,
        // AccountTo_AgainstTDSAmountList: AgainstTDSAmountList,
        // AccountTo_AgainstOthersAmountList: AgainstOthersAmountList,
      });
    }
  };
  CallAgainstVisible = () => {
    let isCall = this.LocalData.MainVoucherTypeID == 5;
    // console.log('iscall', isCall);
    this._menu.hide();
    this.setState({isCallFor: isCall}, () => {
      let SLID = isCall ? this.state.SubAccountBy : this.state.SubAccountTo;
      if (this.IsHavingBillWise(SLID)) {
        this.props.navigation.navigate('AddAgainstNavigator', {
          VoucherTypeID: this.LocalData.VoucherTypeId,
          IsEnableTotalAmount: true,
          EntryId: this.LocalData.VoucherId,
          isCallForBy: isCall,
          NewRefAmount: this.state.BasicAmount,
          SubLedgerID: isCall
            ? this.state.SubAccountBy
            : this.state.SubAccountTo,
          GeneralLedgerID: isCall ? this.state.AccountBy : this.state.AccountTo,
          Loccode: this.LocalData.LocationCode,
          FinancialId: this.LocalData.FinacialYearId,
          onFinishCreation: this.HandleAgainstData,
        });
      } else {
        // console.log('Wrong');
      }
    });
    // else
    // MessageBox.Show("This ledger don't have billwise option.", "iCube Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
  };

  HandleLedgerList = (Type, Id, Index, item) => {
    // this.setState({AccountBy:this.state.APIData.LedgerByList[LedgerByIndex-1].GLID,SubAccountBy:this.state.APIData.LedgerByList[LedgerByIndex-1].ID,FavourOf:this.state.APIData.LedgerByList[LedgerByIndex-1].Name})
    if (Type == 'ByLedger') {
      // console.log('ByL', Type, Index, item.GLID, item.SLID);
      this.setState(
        {
          RefId: 0,
          LedgerById: Id,
          AccountBy: item.GLID,
          SubAccountBy: item.SLID,
          FavourOf: item.Name,
        },
        () => {
          this.HandleRefNoList();
        },
      );
    } else {
      this.setState(
        {
          LedgerToId: Id,
          AccountTo: item.GLID,
          SubAccountTo: item.SLID,
        },
        () => {
        },
      );
    }
  };

  HandleMenuitems = Type => {
    this.hideMenu();
    if (Type == 'AdditionalLedger') {
      this.setState({OpenAdditionalLedger: true});
    } else if (Type == 'Against') {
      if (
        this.LocalData.MainVoucherTypeID == 5 ||
        this.LocalData.MainVoucherTypeID == 4
      ) {
        // console.log('call against');
        this.CallAgainstVisible();
      }
    }
  };

  FieldAmount = key => (
    <View
      key={key}
      style={[
        layoutStyle.FieldLayout,
        layoutStyle.FieldContainer,
        {flexDirection: 'row', marginLeft: 0},
      ]}>
      <RichTextInput
        placeholder="Amount"
        value={isnull(nullif_Number(this.state.BasicAmount, 0), '')}
        wrapperStyle={{width: '85%'}}
        onChangeText={BasicAmount => this.HandleBasicAmount(BasicAmount)}
        inputProps={{
          keyboardType: 'phone-pad',
        }}
      />

      <View
        style={{
          alignItems: 'center',
          justifyContent: 'flex-end',
          width: '15%',
        }}>
        <Menu
          ref={this.setMenuRef}
          button={
            <TouchableOpacity
              style={{
                width: 30,
                borderRadius: 5,
                marginTop: 5,
                backgroundColor: globalColorObject.Color.Primary,
              }}
              onPress={this.showMenu}>
              <Image
                source={require('../../../assets/images/AddIma.png')}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>
          }>
          {/* <MenuItem
            onPress={() => {
              this.HandleMenuitems('AdditionalLedger');
            }}>
            Additional Ledger
          </MenuItem> */}
          {(this.LocalData.MainVoucherTypeID == 4 ||
            this.LocalData.MainVoucherTypeID == 5) && (
            <>
              <MenuItem
                onPress={() => {
                  this.HandleMenuitems('Against');
                }}>
                Against
              </MenuItem>
              {isnull(this.state.AccountBy, '0') != '0' &&
                isnull(this.state.AccountTo, '0') != '0' && (
                  <MenuItem
                    onPress={() => {
                      this.AddRowAddingCharges();
                    }}>
                    Extra Ledger
                  </MenuItem>
                )}
            </>
          )}
        </Menu>
      </View>
    </View>
  );

  FieldLedgerBy = key => (
    <ModalSearchablePicker
      key={key}
      placeholder={'From'}
      data={this.state.APIData.LedgerByList}
      labelProp="Name"
      valueProp="ID"
      selectedValue={this.state.LedgerById}
      onValueSelected={(RNID, lbl, index, item) =>
        this.HandleLedgerList('ByLedger', RNID, index, item)
      }
    />
  );

  FildRefType = key => (
    <ModalSearchablePicker
      key={key}
      // ref={'SubAccountTo'}
      placeholder="Ref Type"
      data={this.state.APIData.RefTypeList}
      labelProp="Type"
      valueProp="Id"
      selectedValue={this.state.RefType}
      onValueSelected={RefType => {
        this.setState(
          {
            RefId: 0,
            RefType: RefType,
          },
          () => {
            this.HandleRefNoList();
          },
        );
      }}
    />
  );

  FildLedgerID = key => (
    <ModalSearchablePicker
      key={key}
      placeholder={'To'}
      data={this.state.APIData.LedgerToList}
      labelProp="Name"
      valueProp="ID"
      selectedValue={this.state.LedgerToId}
      onValueSelected={(RNID, lbl, index, item) =>
        this.HandleLedgerList('ToLedger', RNID, index, item)
      }
    />
  );

  FildToAndAmount = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <ModalSearchablePicker
        placeholder={'To'}
        fieldWrapperStyle={{width: '64%'}}
        data={this.state.APIData.LedgerToList}
        labelProp="Name"
        valueProp="ID"
        selectedValue={this.state.LedgerToId}
        onValueSelected={(RNID, lbl, index, item) =>
          this.HandleLedgerList('ToLedger', RNID, index, item)
        }
      />
      <View
        style={[
          // layoutStyle.FieldContainer,
          {flexDirection: 'row', width: '34%'},
        ]}>
        <RichTextInput
          placeholder="Amount"
          value={isnull(nullif_Number(this.state.BasicAmount, 0), '')}
          wrapperStyle={{flex: 1}}
          onChangeText={BasicAmount => this.HandleBasicAmount(BasicAmount)}
          inputProps={{
            keyboardType: 'phone-pad',
          }}
        />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}>
          <Menu
            ref={this.setMenuRef}
            button={
              <TouchableOpacity
                style={[
                  {
                    width: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    // backgroundColor: globalColorObject.Color.Primary,
                  },
                  //   {
                  //   width: 30,
                  //   borderRadius: 5,
                  //   marginTop: 5,
                  //   backgroundColor: globalColorObject.Color.Primary,
                  // }
                ]}
                onPress={this.showMenu}>
                <Image
                  source={require('../../../assets/images/dot-menu-grey.png')}
                  style={{width: 30, height: 30}}
                />
              </TouchableOpacity>
            }>
            {(this.LocalData.MainVoucherTypeID == 4 ||
              this.LocalData.MainVoucherTypeID == 5) && (
              <>
                <MenuItem
                  onPress={() => {
                    this.HandleMenuitems('Against');
                  }}>
                  Against
                </MenuItem>
                {isnull(this.state.AccountBy, '0') != '0' &&
                  isnull(this.state.AccountTo, '0') != '0' && (
                    <MenuItem
                      onPress={() => {
                        this.AddRowAddingCharges();
                      }}>
                      Extra Ledger
                    </MenuItem>
                  )}
              </>
            )}
          </Menu>
        </View>
      </View>
    </View>
  );

  FieldFromLedgerAndDate = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <ModalSearchablePicker
        placeholder={'From'}
        fieldWrapperStyle={{width: '64%'}}
        data={this.state.APIData.LedgerByList}
        labelProp="Name"
        valueProp="ID"
        selectedValue={this.state.LedgerById}
        onValueSelected={(RNID, lbl, index, item) =>
          this.HandleLedgerList('ByLedger', RNID, index, item)
        }
      />
      <ModalDatePicker
        placeholder={'Date'}
        fieldWrapperStyle={{width: '34%'}}
        selectedValue={this.state.VoucherDate}
        onValueSelected={selectedDate =>
          this.HandleDateChange('VoucherDate', selectedDate)
        }
      />
    </View>
  );

  FieldFavourOf = key => (
    <RichTextInput
      key={key}
      // inputStyle={{height: null, maxHeight: 100, lineHeight: 15}}
      placeholder="Favour Of"
      value={this.state.FavourOf}
      onChangeText={FavourOf => this.setState({FavourOf})}
      inputProps={{multiline: true, numberOfLines: 2}}
    />
  );

  FieldNaration = key => (
    <RichTextInput
      // wrapperStyle={{width: '100%'}}
      key={key}
      placeholder="Naration"
      value={this.state.Narration}
      onChangeText={Narration => this.setState({Narration})}
    />
  );

  FieldExtraLedger = (item, index) => (
    <View
      key={`extrledger_${index}_${item.LedgerID}`}
      style={[layoutStyle.ScrollContentWrapper]}>
      <ModalSearchablePicker
        placeholder={'Ledger'}
        fieldWrapperStyle={{width: '60%'}}
        data={props.LedgerList || []}
        labelProp="Name"
        valueProp="ID"
        selectedValue={item.LedgerID || ''}
        onValueSelected={(value, label, l_index, item) => {
          this.HandleAddition('LedgerId', index, value, item);
        }}
      />

      <RichTextInput
        placeholder="Amount"
        value={isnull(this.item.Amount, '')}
        wrapperStyle={{width: '40%'}}
        onChangeText={Amount => {
          this.HandleAddition('Amount', index, Amount.toString());
        }}
        inputProps={{
          keyboardType: 'phone-pad',
        }}
      />
    </View>
  );

  FieldRefInfoWithDate = key => {
    return (
      <>
        <ModalSearchablePicker
          key={`${key}_1`}
          placeholder="Ref Type"
          fieldWrapperStyle={{
            width: layoutStyle.FieldContainerCount == 1 ? '98%' : '49%',
          }}
          data={this.state.APIData.RefTypeList}
          labelProp="Type"
          valueProp="Id"
          selectedValue={this.state.RefType}
          onValueSelected={RefType => {
            this.setState(
              {
                RefId: 0,
                RefType: RefType,
              },
              () => {
                this.HandleRefNoList();
              },
            );
          }}
        />
        <View
          key={`${key}_2`}
          style={[
            {
              flexDirection: 'row',
              width: layoutStyle.FieldContainerCount == 1 ? '98%' : '49%',
            },
          ]}>
          {this.isChequBookShow() ? (
            <ModalSearchablePicker
              placeholder="Ref No"
              fieldWrapperStyle={{width: '64%'}}
              data={this.state.APIData.RefNoList}
              labelProp="Number"
              valueProp="ID"
              selectedValue={this.state.RefId}
              onValueSelected={(RefId, lbl, index, item) =>
                this.setState(
                  prevState => ({
                    RefId: RefId,
                    RefNo: lbl,
                  }),
                  () => console.log('state data : ', this.state),
                )
              }
            />
          ) : (
            <RichTextInput
              placeholder="Ref No"
              wrapperStyle={{width: '64%'}}
              value={this.state.RefNo}
              onChangeText={RefNo => this.setState({RefNo})}
              inputProps={{
                onSubmitEditing: () => {
                  Keyboard.dismiss();
                },
              }}
            />
          )}

          <ModalDatePicker
            placeholder={'Ref Date'}
            fieldWrapperStyle={{width: '34%'}}
            selectedValue={this.state.RefDate}
            onValueSelected={RefDate =>
              this.HandleDateChange('RefDate', RefDate)
            }
          />
        </View>
      </>
    );
  };

  refreshLayout = () => this.forceUpdate();

  render() {
    // console.log(
    //   'load width : by : ',
    //   isnull(this.state.AccountBy, '0') != '0',
    //   ' Acoo to : ',
    //   isnull(this.state.AccountTo, '0') != '0',
    // );
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }

    return (
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={this.refreshLayout}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}
          style={{flex: 1}}>
          <View style={[layoutStyle.ScrollContentWrapper]}>
            {/* {this.FieldLedgerBy()} */}
            {this.FieldFromLedgerAndDate('fieldKey_LedgerBy')}
            {/* {this.FildLedgerID()} */}
            {this.FildToAndAmount('fieldKey_LedgerTo')}
            {/* {this.FieldVoucherDate()} */}
            {/* {this.FieldAmount()} */}
            {(this.state.APIData.LedgerList || []).map((item, index) => {
              //  <>
              //  {this.FieldExtraLedger(item,index)}
              //  </>
              return (
                <ListItemMain
                  key={`listitembind_${item.LedgerID}_${index}`}
                  data={item}
                  index={index}
                  LedgerList={
                    this.LocalData.MainVoucherTypeID == 5
                      ? this.state.APIData.LedgerByList
                      : this.LocalData.MainVoucherTypeID == 4
                      ? this.state.APIData.LedgerToList
                      : []
                  }
                  onAmountChange={this.HandleAddition}
                  onLedgerChange={this.HandleAddition}
                  onPress={this.DeleteAddLedgRow.bind(this, index)}
                  IsEnable={this.state.isAdditional}
                />
              );
            })}
            {this.FieldRefInfoWithDate('fieldKey_RefInfo')}
            {this.FieldFavourOf('fieldKey_FavourOf')}
            {this.FieldNaration('fieldKey_NAration')}
          </View>
        </ScrollView>

        <View
          style={[
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
            {
              paddingBottom: 5,
              paddingTop: 10,
              paddingHorizontal: 10,
            },
          ]}>
          {/* {this.state.AdditionalAmount > 0 && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 5,
              }}>
              <Text
                style={[
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                  ),
                  {marginLeft: 5},
                ]}>
                Additional Amount
              </Text>
              <Text
                style={[
                  ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                  ),
                  {marginLeft: 5},
                ]}>
                {this.state.AdditionalAmount.toFixed(2)}
              </Text>
            </View>
          )} */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 5,
            }}>
            <Text
              style={[
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                ),
                {marginLeft: 5},
              ]}>
              Total
            </Text>
            <Text
              style={[
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                ),
                {marginLeft: 5},
              ]}>
              {this.state.NetAmount}
            </Text>
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          // visible={true}
          visible={this.state.OpenDenomin}
          onRequestClose={() => {
            this.setState({OpenDenomin: false});
          }}>
          <View style={{flex: 1}}>
            <FlatList
              style={[
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
              ]}
              data={this.state.APIData.VoucherDenominList}
              renderItem={({item, index}) => (
                <DenominListItem
                  data={item}
                  index={index}
                  onAmountChange={this.HandleAddition}
                />
              )}
              ListHeaderComponent={() => <ListItemHeader />}
              keyExtractor={(item, index) => index.toString()}
              stickyHeaderIndices={[0]}
              refreshing={this.state.isRefresh}
            />
            <View
              style={[
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                {
                  paddingBottom: 5,
                  paddingTop: 10,
                  paddingHorizontal: 10,
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  // marginBottom: 5,
                }}>
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxl,
                    ),
                    {marginLeft: 5},
                  ]}>
                  Total
                </Text>
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxl,
                    ),
                    {marginRight: 10},
                  ]}>
                  {this.state.APIData.VoucherDenominList.reduce(
                    (accum, item) => accum + Number(item.Unit * item.Factor),
                    0.0,
                  ).toFixed(2)}
                </Text>
              </View>
            </View>

            <View
              style={[
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                {
                  paddingBottom: 5,
                  paddingTop: 10,
                  paddingHorizontal: 10,
                },
              ]}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                }}>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <Button value="Apply" onPressEvent={this.HandleDenomin} />
                </View>
                {/* <View
            style={{
              width:'50%',
            }}>
            <Button value="Cancel" onPressEvent={() => {
                    this.setState({OpenDenomin: false});
                  }} />
            </View> */}
              </View>
            </View>
          </View>
        </Modal>
      </LayoutWrapper>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 9,
    marginVertical: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 10,
    paddingHorizontal: 5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '50%',
    marginVertical: 3,
    justifyContent: 'center',
  },
  headerIconsContainer: {
    flexDirection: 'row',
  },
  headerIcon: {
    width: 35,
    height: 35,
    marginRight: 8,
    marginLeft: 8,
  },
  // FieldContainer: {
  //   width: '100%',
  //   flexDirection: 'row',
  //   position: 'relative',
  //   borderBottomWidth: 2,
  //   borderBottomColor: 'rgba(0,0,0,0.07)',
  //   marginVertical: 5,
  // },
  // FieldLabel: {
  //   flex: 1,
  //   alignSelf: 'center',
  //   fontSize: 14,
  //   fontWeight: 'bold',
  //   position: 'absolute',
  //   top: 0,
  //   left: 5,
  //   color: 'gray',
  // },
  container: {
    flex: 1,
    padding: 5,
  },
  // FieldInput: {
  //   width: '100%',
  //   flex: 2,
  //   height: 40,
  //   alignSelf: 'center',
  //   marginTop: 8,
  // },
});
