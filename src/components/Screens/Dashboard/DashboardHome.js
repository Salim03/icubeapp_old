/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {WebView} from 'react-native-webview';

// import HamburgerMenu from '../../HamburgerMenu';
// import CheckInAndOut from './CheckInAndOut';
import {Request, AlertMessage} from '../../../Helpers/HelperMethods';

export default class DashboardHome extends Component {
  constructor() {
    super();
    this.LocalData = {
      RoleId: '1',
      Location: '1',
    };
    this.state = {
      isShowHomeDashboard: true,
      DashboadURL: '',
      islocal: false,
      isFetched: false,
      iSDates: true,
      NoDataAvailable: true,
      DataDash: [],
    };
  }
  componentDidMount() {
    this.InitialSetup();
  }

  ChangeView = async () => {
    this.setState(prevState => {
      return {isShowHomeDashboard: !prevState.isShowHomeDashboard};
    });
    console.log(JSON.stringify(this.state));
  };

  InitialSetup = async () => {
    let [[, get_IP], [, get_Token], [, get_RoleId], [, get_DashboardURL]] =
      await AsyncStorage.multiGet(['IP', 'Token', 'RoleId', 'DashboardURL']);
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.RoleId = get_RoleId;
    this.setState({DashboadURL: get_DashboardURL});
    this.LoadDashboard();
  };
  LoadDashboard = async () => {
    let uri = `${this.IP}/api/Dashboard/GetMobileReportList?RoleId=${this.LocalData.RoleId}&LocationCode=${this.LocalData.Location}`;
    console.log('my url:', uri, 'mytoken:', this.Token);
    Request.get(uri, this.Token)
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            // console.log(res.data)
            this.setState({isFetched: true, DataDash: res.data}, () => {
              this.PrepareListForUnChangedData();
            });
          } else {
            this.MDashBoardList = (
              <Text style={styles.FormLabel}>No Dashboard Available</Text>
            );
            // AlertMessage(`No Data found`);
            this.setState({isFetched: true, NoDataAvailable: true});
            this.forceUpdate();
          }
        }
      })
      .catch(err => {
        console.error(err);
        AlertMessage(
          'Could not get dashboard list info : \n' + JSON.stringify(err),
        );
        this.setState({isFetched: true});
      });
  };
  MDashBoardList;
  PrepareListForUnChangedData = () => {
    if (this.state.DataDash.length) {
      // console.log('rrrr',this.state.DataDash);
      this.MDashBoardList = this.state.DataDash.map((item, index) => (
        <TouchableOpacity
          key={index}
          style={styles.FormContainer}
          onPress={this.NavigateToForm.bind(this, item.Name, item.ID)}>
          <Image source={{uri: item.Icon}} style={styles.FormImages} />
          <Text style={styles.FormLabel}>{item.Name}</Text>
        </TouchableOpacity>
      ));
    } else {
      this.MDashBoardList = (
        <View
          style={styles.FormContainer}
          onPress={this.NavigateToFormStatic.bind(this, 'CRM')}>
          <Image
            source={require('../../../assets/images/Home/Dashboard/crm.png')}
            style={styles.FormImages}
          />
          <Text style={styles.FormLabel}>No Dashboard Available</Text>
        </View>
      );
    }
    this.forceUpdate();
  };

  NavigateToForm = (form, ReportId) => {
    this.props.navigation.navigate('DDynamicNav', {
      HeaderTitle: form,
      CallFrom: ReportId,
    });
  };

  NavigateToFormStatic = form => {
    if (form === 'CRM') {
      this.props.navigation.navigate('DCrmNav');
    } else if (form === 'Retail') {
      this.props.navigation.navigate('DRetailNav');
    }
  };

  render() {
    let BinInfo = this.state.isShowHomeDashboard ? (
      <View style={styles.LoadContainer}>
        <WebView source={{uri: this.state.DashboadURL}} />
      </View>
    ) : (
      <View style={styles.LoadContainer}>
        <ScrollView style={styles.DataContainer}>
          <View style={styles.ListContainer}>{this.MDashBoardList}</View>
        </ScrollView>
      </View>
    );
    return (
      <>
        <SafeAreaView style={{flex: 1}}>
          {BinInfo}
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0)',
            }}>
            <TouchableOpacity
              style={styles.LoginButton}
              onPress={this.ChangeView}>
              <Text
                style={[styles.TextDefault, {color: 'white', fontSize: 25}]}>
                {this.state.isShowHomeDashboard ? 'List' : 'Home'}
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    marginTop: '80%',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  LoadContainer: {
    // backgroundColor: 'rgb(43, 54, 255)',
    backgroundColor: '#69e781',
    flex: 1,
  },
  LoginButton: {
    padding: 10,
    zIndex: 0,
    backgroundColor: '#26B1AD',
    borderRadius: 8,
    alignItems: 'center',
  },
  FormImages: {
    width: 30,
    height: 30,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    marginTop: 5,
  },
  FormLabel: {
    textAlign: 'center',
    fontSize: 18,
    // fontWeight: 'bold',
  },
  DataContainer: {
    backgroundColor: 'white',
    marginTop: 30,
    // marginLeft: 5,
    // marginRight: 5,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    flex: 1,
    width: '100%',
    elevation: 50,
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  FormContainer: {
    // backgroundColor: 'rgb(252, 172, 182)',
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 5,
    height: 120,
    width: 100,
    alignItems: 'center',
    borderRadius: 10,
    elevation: 15,
  },
});
// export {Retail};
