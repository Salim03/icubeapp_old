import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  SectionList,
} from 'react-native';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import {WebView} from 'react-native-webview';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import LayoutWrapper, {layoutStyle} from '../../../components/Layout/Layout';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {
  Request,
  isnull,
  sharefileForCallFRomAPI,
  ReturnDisplayStringForDate,
  SqlDateFormat,
  AlertError,
  CallBluetoothPrint,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';
import {GroupValuesByKey, GetSessionData} from '../../../Helpers/HelperMethods';

const ListItem = props => {
  let data = props.item;
  return (
    <TouchableOpacity
      onPress={props.onReportClick.bind(
        this,
        data.SNo,
        data.DesignTypeID,
        data.MenuName,
      )}
      style={[styles.item]}>
      <Text style={styles.title}>{data.MenuName}</Text>
    </TouchableOpacity>
  );
};

class AnalyticsReport extends React.Component {
  DefaultVStatealues = {
    FromDate: new Date(),
    ToDate: new Date(),
    Name1: '',
    isCentral: false,
    ExportType: 'html',
    HTMLString: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      isShowCentral: false,
      isFetched: false,
      ReportType: 'Fixed',
      SelectedDesignTypeID: 0,
      SelectedReportSeqNo: '',
      SelectedReportName: '',
      isShowReportView: false,
      NameList: [],
      forms: [],
      Storage: {
        IP: '',
        Token: '',
        RoleID: '',
        FinancialYearId: 0,
        ProfileId: '',
        Location: 0,
        UserCode: '',
        PrinterName: '',
      },
      ...this.DefaultVStatealues,
    };
  }

  InitialSetup = async () => {
    let {
      get_IP,
      get_Token,
      get_RoleId,
      get_Location,
      get_UserCode,
      get_ProfileId,
      get_FinancialYearId,
      get_POSPrintTerminalName,
    } = await GetSessionData();
    let myobject = {};
    myobject.IP = get_IP;
    myobject.Token = get_Token;
    myobject.RoleID = get_RoleId;
    myobject.Location = get_Location == null ? 0 : parseInt(get_Location);
    myobject.UserCode = get_UserCode;
    myobject.FinancialYearId = get_FinancialYearId;
    myobject.ProfileId = get_ProfileId;
    myobject.PrinterName = get_POSPrintTerminalName;
    this.setState(prevstate => {
      return {
        ...prevstate,
        isShowCentral: myobject.Location == '1',
        Storage: myobject,
      };
    });
    await this.RefreshReportList(this.state.ReportType);
  };

  RefreshReportList = async ReportType => {
    this.setState(prevstate => ({
      ...prevstate,
      isFetched: false,
    }));
    await Request.get(
      `${this.state.Storage.IP}/api/Common/GetReportNavigationList?ViewType=${ReportType}&RoleId=${this.state.Storage.RoleID}&LocationCode=${this.state.Storage.Location}`,
      this.state.Storage.Token,
    )
      .then(res => res.data || [])
      .then(data => {
        let forms = [];
        if (data && data.length > 0) {
          let groupdataforms = GroupValuesByKey(data, 'ModuleName');
          forms = Object.keys(groupdataforms).map(key => {
            return {
              data: groupdataforms[key],
              title: key,
            };
          });
        }
        this.setState(prevstate => ({
          ...prevstate,
          forms,
        }));
      })
      .catch(err => {
        AlertError(err);
        console.error(err);
      });
    this.setState(prevstate => {
      return {
        ...prevstate,
        isFetched: true,
      };
    });
  };

  ShowAction = async () => {
    this.setState(prevstate => ({
      ...prevstate,
      isFetched: false,
    }));
    let get_URL = `${
      this.state.Storage.IP
    }/api/Common/GetAnalyticsReportGenerateFileBasedOnType?ReportSeqNo=${
      this.state.SelectedReportSeqNo
    }&DesignTypeID=${this.state.SelectedDesignTypeID}&ExtensionName=${
      this.state.ExportType
    }&PassFromDate=${ReturnDisplayStringForDate(
      new Date(),
      SqlDateFormat,
    )}&PassToDate=${ReturnDisplayStringForDate(
      new Date(),
      SqlDateFormat,
    )}&PassCode1Value=${this.state.Name1}&PassCode2Value=&IsCentralized=${
      this.state.isCentral
    }&IsStaticReport=${this.state.ReportType == 'Fixed'}&RoleId=${
      this.state.Storage.RoleID
    }&PassLoccode=${this.state.Storage.Location}&FinancialYearId=${
      this.state.Storage.FinancialYearId
    }`;
    if (this.state.ExportType == 'html' || this.state.ExportType == 'txt') {
      await Request.get_File(get_URL, this.state.Storage.Token)
        .then(async response => {
          // console.log('response.status :', response.status);
          if (response.status == '200') {
            return await response.text();
          }
        })
        .then(result => {
          if (this.state.ExportType == 'txt') {
            CallBluetoothPrint(
              isnull(this.state.Storage.PrinterName, ''),
              1,
              isnull(result, ''),
            );
          } else {
            this.setState(prevstate => ({
              ...prevstate,
              HTMLString: isnull(result, ''),
            }));
          }
        })
        .catch(err => console.error(err));
    } else {
      sharefileForCallFRomAPI(
        this.state.ReportType,
        get_URL,
        'test',
        this.state.ExportType,
      );
    }
    this.setState(prevstate => ({
      ...prevstate,
      isFetched: true,
    }));
  };

  isShowFromDate = DesignTypeID => {
    if ([1, 6, 7, 10, 11, 14].includes(DesignTypeID)) {
      return true;
    }
    return false;
  };

  isShowName = DesignTypeID => {
    if ([7, 9, 11, 13, 14, 15].includes(DesignTypeID)) {
      return true;
    }
    return false;
  };

  CallReportBasedOnSelection = async (
    ReportSeqNo,
    DesignTypeID,
    ReportName,
  ) => {
    let NameList = [];
    if (this.isShowName(DesignTypeID)) {
      this.setState(prevstate => ({
        ...prevstate,
        ...this.DefaultVStatealues,
        isFetched: false,
      }));
      NameList =
        (await Request.get(
          `${this.state.Storage.IP}/api/Common/GetDataForNameList1?ReportSeqNo=${ReportSeqNo}&RoleId=${this.state.Storage.RoleID}&LocationCode=${this.state.Storage.Location}&FinancialYearId=${this.state.Storage.FinancialYearId}`,
          this.state.Storage.Token,
        )
          .then(res => {
            let REturnNAmeList = res.data;
            return REturnNAmeList;
          })
          .catch(err => AlertError(err))) || [];
    }
    this.setState(prevstate => ({
      ...prevstate,
      ...this.DefaultVStatealues,
      isFetched: true,
      NameList: NameList,
      isShowReportView: true,
      SelectedDesignTypeID: DesignTypeID,
      SelectedReportSeqNo: ReportSeqNo,
      SelectedReportName: isnull(ReportName, 'No Name'),
    }));
  };

  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: 'row', paddingRight: 10}}>
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              marginHorizontal: 8,
              justifyContent: 'center',
            }}
            onPress={() => {
              let Type =
                this.state.ReportType == 'Analytics' ? 'Fixed' : 'Analytics';
              this.RefreshReportList(Type);
              this.props.navigation.setOptions({
                title: Type,
              });
              this.setState(prevstate => ({
                ...prevstate,
                ReportType: Type,
              }));
            }}>
            {/* <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/undo-light.png')}
            /> */}
            <Text
              style={[
                {textAlign: 'center'},
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.mxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              &#9851;
            </Text>
          </TouchableOpacity>
        </View>
      ),
    });
    this.InitialSetup();
  }

  refreshLayout = () => this.forceUpdate();

  render() {
    return (
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={this.refreshLayout}>
        <ICubeAIndicator isShow={!this.state.isFetched} />
        <SectionList
          sections={this.state.forms}
          keyExtractor={(item, index) => item.SNo}
          renderItem={({item}) => (
            <ListItem
              item={item}
              onReportClick={this.CallReportBasedOnSelection}
            />
          )}
          stickySectionHeadersEnabled={true}
          renderSectionHeader={({section: {title}}) => (
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.ms,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Lightprimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                styles.headerlist,
              ]}>
              {title}
            </Text>
          )}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isShowReportView}
          onRequestClose={() => {
            this.setState({isShowReportView: false});
          }}>
          <SafeAreaView
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <View style={styles.Header}>
              <TouchableOpacity
                onPress={() => this.setState({isShowReportView: false})}
                style={styles.closeBottom}>
                <Text
                  style={[
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                    {textAlign: 'left'},
                  ]}>
                  &#60;{`  ${this.state.SelectedReportName}`}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.ShowAction}
                style={styles.closeBottom1}>
                <Image
                  style={{width: 30, height: 30}}
                  fadeDuration={300}
                  source={require('../../../assets/images/reset-light.png')}
                />
              </TouchableOpacity>
            </View>
            <View>
              <ScrollView
                keyboardShouldPersistTaps="always"
                nestedScrollEnabled={true}
                style={[styles.ViewPager]}>
                <View
                  style={[
                    layoutStyle.ScrollContentWrapper,
                    {paddingBottom: 5},
                  ]}>
                  <View
                    style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
                    {this.isShowFromDate(this.state.SelectedDesignTypeID) && (
                      <ModalDatePicker
                        placeholder={'From'}
                        fieldWrapperStyle={{width: '48%'}}
                        selectedValue={this.state.FromDate}
                        onValueSelected={selectedDate =>
                          this.HandleDateChange('FromDate', selectedDate)
                        }
                      />
                    )}
                    <ModalDatePicker
                      placeholder={'To'}
                      fieldWrapperStyle={{
                        width: this.isShowFromDate(
                          this.state.SelectedDesignTypeID,
                        )
                          ? '48%'
                          : '98%',
                      }}
                      selectedValue={this.state.ToDate}
                      onValueSelected={selectedDate =>
                        this.HandleDateChange('ToDate', selectedDate)
                      }
                    />
                  </View>
                  {this.isShowName(this.state.SelectedDesignTypeID) && (
                    <ModalSearchablePicker
                      placeholder="Name"
                      data={this.state.NameList}
                      labelProp="Name"
                      valueProp="Code"
                      selectedValue={this.state.Name1}
                      onValueSelected={(value, label) =>
                        this.setState(prevState => ({
                          ...prevState,
                          Name1: isnull(value, 0),
                        }))
                      }
                    />
                  )}
                  <View
                    style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
                    {this.state.isShowCentral && (
                      <ModalSearchablePicker
                        placeholder="Type"
                        fieldWrapperStyle={{width: '48%'}}
                        data={[
                          {Id: 'false', Name: 'Local'},
                          {Id: 'true', Name: 'Cenral'},
                        ]}
                        labelProp="Name"
                        valueProp="Id"
                        selectedValue={this.state.isCentral.toString()}
                        onValueSelected={(value, label) =>
                          this.setState(prevState => ({
                            ...prevState,
                            isCentral: isnull(value, 'false') == 'true',
                          }))
                        }
                      />
                    )}
                    <ModalSearchablePicker
                      placeholder="Output"
                      fieldWrapperStyle={{
                        width: this.state.isShowCentral ? '48%' : '98%',
                      }}
                      data={[
                        {Id: 'html', Name: 'View'},
                        {Id: 'xlsx', Name: 'Excel'},
                        {Id: 'pdf', Name: 'PDF'},
                        {Id: 'txt', Name: 'Print'},
                      ]}
                      labelProp="Name"
                      valueProp="Id"
                      selectedValue={this.state.ExportType}
                      onValueSelected={(value, label) =>
                        this.setState(prevState => ({
                          ...prevState,
                          ExportType: isnull(value, ''),
                        }))
                      }
                    />
                  </View>
                </View>
              </ScrollView>
            </View>

            <WebView
              source={
                isnull(this.state.HTMLString, '') == ''
                  ? {
                      html: `<body>
                        <br/>
                        <br/>
                        <h1 style="text-align:center;">No data available ...</h1>
                  </body>`,
                    }
                  : {html: this.state.HTMLString}
              }
            />
          </SafeAreaView>
        </Modal>
      </LayoutWrapper>
    );
  }
}

const styles = StyleSheet.create({
  closeBottom1: {
    right: 10,
  },
  Header: {
    height: 55,
    alignItems: 'center',
    backgroundColor: globalColorObject.Color.Primary,
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  FormImages: {
    width: 30,
    height: 30,
    margin: 5,
    resizeMode: 'stretch',
  },
  FormLabel: {
    textAlign: 'center',
    // fontWeight: 'bold',
  },

  closeBottom: {
    // width: '35%',
    borderRadius: 5,
    //borderWidth: 0.3,
    // left: 3,
    // borderColor:globalColorObject.Color.Lightprimary,
  },
  FormContainer: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
  },
  item: {
    paddingLeft: 25,
    paddingVertical: 15,
  },
  headerlist: {
    paddingLeft: 5,
    paddingVertical: 10,
  },
  title: {
    fontSize: 16,
  },
});

export default AnalyticsReport;
