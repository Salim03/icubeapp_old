import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, Text, View, Modal, ActivityIndicator} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import Ripple from 'react-native-material-ripple';
import {
  Confirmation,
  AlertMessage,
  ShowToastMsg,
  Request,
  AlertError,
  AlertStatusError,
  GetSessionData,
} from '../../../Helpers/HelperMethods';
import Geolocation from '@react-native-community/geolocation';
import {Button} from '../../../components/Custom/Button';
import ICubeAIndicator from '../../Tools/ICubeIndicator';

// const BingAPI_KEY = `Gipp9fb1pSaHhiH7jxP0~z7RfGdwXmIjWwma0y4JbMw~AmY2BKAExbzj9Z-02h7oN9OD58HMyitq2Oax_fCVJJgLMWx9GTWP3SkQWrSZZYo0`;
const BingAddressFetcher = {
  latitude: 0,
  longitude: 0,
  API_KEY: '',
  get addressFetchURL() {
    return `https://dev.virtualearth.net/REST/v1/Locations/${this.latitude},${this.longitude}?o=json&key=${this.API_KEY}`;
  },
};

const statusValue = {
  in: 'Check In',
  out: 'Check Out',
};

const API = {
  _userId: '',
  _IP: '',
  _Token: '',
  Location: '0',
  set Token(token) {
    this._Token = token;
  },
  get Token() {
    return this._Token;
  },
  set IP(ip) {
    this._IP = ip;
  },
  set userId(id) {
    this._userId = id;
  },
  get userId() {
    return this._userId;
  },
  get CurrentStatusOfUser() {
    return `${this._IP}/api/common/CurrentStatusOfUser?UserId=${this._userId}`;
  },
  get CheckInOrOutUser() {
    return `${this._IP}/api/common/CheckInOrOutUser`;
  },
};

const CheckInAndOut = () => {
  const [status, setStatus] = useState('');
  const [showModal, setShowModal] = useState(false);
  const CheckedInID = useRef(0);
  const UserShiftTime = useRef('');

  useEffect(() => {
    GetUserCurrentStatus();
  }, []);

  const GetUserCurrentStatus = async () => {
    // // getting user details from storage
    // API.IP = await AsyncStorage.getItem('IP');
    // API.Token = await AsyncStorage.getItem('Token');
    // API.userId = await AsyncStorage.getItem('UserId');

    let {get_IP, get_Token, get_Location, get_UserId} = await GetSessionData();

    API.IP = get_IP;
    API.Token = get_Token;
    API.userId = get_UserId;
    API.Location = get_Location;
    console.log(',my location : ', get_Location, ' : ', API.Location);

    FetchStatusFromAPI();
  };

  const FetchStatusFromAPI = () => {
    console.log('User Id', API.userId);
    // returning if the user id is undefined or 0 ( for "sa" user the id will be 0 )
    if (!API.userId || API.userId === '0') return;

    // fetching current checkin status of the user
    Request.get(API.CurrentStatusOfUser, API.Token)
      .then(res => {
        // console.log(res);
        if (res.status === 200) {
          if (res.data.length) {
            SetUserStatus(res.data[0]);
          } else AlertMessage('Could not fetch Checkin details');
        } else AlertStatusError(res);
      })
      .catch(AlertError);
  };
  const SetUserStatus = data => {
    CheckedInID.current = data['ID'] || 0;
    UserShiftTime.current = data['ShiftTime'] || '';
    BingAddressFetcher.API_KEY = data['BingKey'] || '';
    setStatus(CheckedInID.current ? statusValue.out : statusValue.in);
  };
  const confirmAction = () =>
    Confirmation(
      `Are you sure to ${status} ?\n${
        UserShiftTime.current && `Shift Time: ${UserShiftTime.current}`
      }`,
      toggleStatus,
    );
  const toggleStatus = () => {
    setShowModal(true);
    getUserLocation();
  };
  // to Fetch the location of the user using GPS
  const getUserLocation = () =>
    Geolocation.getCurrentPosition(
      getAddressFromLocation,
      errorGettingLocation,
    );
  // to Fetch the Address of the user Location using Bing Virtual Earth API
  const getAddressFromLocation = position => {
    const {latitude, longitude} = position.coords;
    BingAddressFetcher.latitude = latitude;
    BingAddressFetcher.longitude = longitude;

    // Fetching address info from Bing Virtual Earth API
    fetch(BingAddressFetcher.addressFetchURL)
      .then(res => res.json())
      .then(json => {
        if (json.resourceSets.length && json.resourceSets[0].resources.length) {
          toggleUserStatusWithAPI({
            latitude,
            longitude,
            address: json.resourceSets[0].resources[0].address.formattedAddress,
          });
        } else {
          AlertMessage('Could not get Address for the Location');
        }
      })
      .catch(err => {
        AlertMessage(`Could not get location address\n${JSON.stringify(err)}`);
        console.warn(err);
        setShowModal(false);
      });
  };
  const toggleUserStatusWithAPI = position => {
    // preparing Object for save and calling API
    const obj = prepareDataObject(position);
    Request.post(API.CheckInOrOutUser, obj, API.Token)
      .then(res => res.json())
      .then(json => {
        json = JSON.parse(json);
        if (json['message'] && json['message'] === 'Saved Successful') {
          setStatus(
            statusValue.in === status ? statusValue.out : statusValue.in,
          );
          ShowToastMsg(`${status.replace('Check', 'Checked')} successfully`);
          // Fetching the current status data from API
          FetchStatusFromAPI();
        } else AlertMessage('Something went wrong');
      })
      .catch(AlertError)
      .finally(() => {
        console.log('After API Finally');
        // closing Modal and change the status text
        setShowModal(false);
      });
  };
  const prepareDataObject = location => {
    let dt = new Date().toISOString().replace(/[T,Z]/gi, ' ');
    const objIn = {
      Id: 0,
      EmpID: API.userId,
      Date: dt.split(' ')[0],
      CheckedIn: dt,
      CheckedOut: '1900-01-01',
      CheckInLatitude: location.latitude,
      CheckInLongitude: location.longitude,
      CheckOutLatitude: '',
      CheckOutLongitude: '',
      CheckINAddress: location.address,
      CheckOutAddress: '',
      CheckoutStatus: false,
      UserId: API.userId,
      Loccode: API.Location,
    };
    const objOut = {
      Id: CheckedInID.current,
      EmpID: API.userId,
      Date: dt.split(' ')[0],
      CheckedIn: dt,
      CheckedOut: dt,
      CheckInLatitude: '',
      CheckInLongitude: '',
      CheckOutLatitude: location.latitude,
      CheckOutLongitude: location.longitude,
      CheckINAddress: '',
      CheckOutAddress: location.address,
      CheckoutStatus: true,
      UserId: API.userId,
      Loccode: API.Location,
    };
    return CheckedInID.current > 0 ? objOut : objIn;
  };
  const errorGettingLocation = err => {
    setShowModal(false);
    AlertMessage('Error while getting loaction');
    console.error('final ', err);
  };
  console.log('sss stat : ', status);
  return (
    status !== '' && (
      <>
        <Button value={status} onPressEvent={() => confirmAction()} />
        <ICubeAIndicator isShow={showModal} />
      </>
    )
  );
  // <View style={styles.mainWrapper}>
  //   {status !== '' && (
  //     <Ripple
  //       style={[styles.statusTogglerWrapper, styles.centeredStyle]}
  //       rippleContainerBorderRadius={5}
  //       onPress={confirmAction}>
  //       <Text style={styles.statusTxt}>{status.toUpperCase()}</Text>
  //     </Ripple>
  //   )}
  //   <Modal visible={showModal} transparent>
  //     <View style={[styles.ModalBackdrop, styles.centeredStyle]}>
  //       <View style={[styles.modalLoaderBox]}>
  //         <ActivityIndicator size="large" />
  //         <Text style={styles.modalLoaderText}>{`${status.replace(
  //           'Check',
  //           'Checking',
  //         )} the user`}</Text>
  //       </View>
  //     </View>
  //   </Modal>
  // </View>
};

// const styles = StyleSheet.create({
//   mainWrapper: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   centeredStyle: {
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   statusTogglerWrapper: {
//     width: 100,
//     height: 30,
//     marginRight: 10,
//     borderRadius: 5,
//   },
//   statusTxt: {
//     fontSize: 15,
//     color: 'black',
//   },
//   ModalBackdrop: {
//     flex: 1,
//     backgroundColor: 'rgba(0,0,0,.4)',
//   },
//   modalLoaderBox: {
//     width: 300,
//     height: 120,
//     flexDirection: 'row',
//     backgroundColor: 'white',
//     borderRadius: 10,
//     justifyContent: 'space-evenly',
//     alignItems: 'center',
//   },
//   modalLoaderText: {
//     fontSize: 17,
//     color: '#242424',
//   },
// });

export default React.memo(CheckInAndOut);
