/* eslint-disable react/no-string-refs */
/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Animated,
  Modal,
  SafeAreaView,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {Table, TableWrapper, Cell} from 'react-native-table-component';

import ModalDatePicker from '../../Custom/ModalDatePicker';
import {LineChart, PieChart} from 'react-native-chart-kit';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import moment from 'moment';
import {layoutStyle} from '../../Layout/Layout';
import {Request, AlertMessage} from '../../../Helpers/HelperMethods';

const LocationDetailsBind = props => {
  var LocationInfo = props.data;
  let isCompany = false;
  if (LocationInfo && LocationInfo.length > 0) {
    if (LocationInfo[0].Company) {
      isCompany = true;
    }
  }
  var helper = {};
  var FinalData = [];
  if (isCompany) {
    var result = LocationInfo.reduce((r, o) => {
      var key = o.Company;
      if (!helper[key]) {
        helper[key] = Object.assign({}, {Company: o.Company, Value: o.Value});
        r.push(helper[key]);
      } else {
        helper[key].Value += o.Value;
      }
      return r;
    }, []);

    FinalData = result.reduce((r, o) => {
      r.push(
        Object.assign({}, {Type: 'Company', Header: o.Company, Value: o.Value}),
      );
      LocationInfo.filter(city => city.Company == o.Company).map(city =>
        r.push(
          Object.assign(
            {},
            {Type: 'Location', Header: city.Header, Value: city.Value},
          ),
        ),
      );
      return r;
    }, []);
  } else {
    FinalData = LocationInfo;
  }
  console.log(FinalData);
  return (
    <TableWrapper style={{width: '100%'}}>
      {FinalData.map((item, ind) => (
        <TableWrapper
          key={ind}
          style={{
            width: '100%',
            flexDirection: 'row',
          }}>
          <Cell
            textStyle={{
              color: item.Type == 'Company' && '#ffffff',
              fontSize: 16,
              fontWeight: item.Type == 'Company' ? 'bold' : '500',
            }}
            data={item.Header}
            style={{
              backgroundColor:
                item.Type == 'Company' ? '#000000' : ind % 2 && '#f2f2f2',
              width: '70%',
              alignItems: 'flex-start',
              padding: 5,
             }}
          />
          <Cell
            textStyle={{
              color: item.Type == 'Company' && '#ffffff',
              fontSize: 16,
              fontWeight: item.Type == 'Company' ? 'bold' : '500',
            }}
            data={item.Value.toFixed(2).replace(
              /(\d)(?=(\d{3})+(?!\d))/g,
              '$1,',
            )}
            style={{
              backgroundColor:
                item.Type == 'Company' ? '#000000' : ind % 2 && '#f2f2f2',
              width: '30%',
              alignItems: 'flex-end',
              padding: 5,
            }}
          />
        </TableWrapper>
      ))}
    </TableWrapper>
  );
};

export default class DynamicDash extends Component {
  constructor(props) {
    super(props);
    var date = new Date();
    var dateString = date.toISOString().split('T')[0];
    // Local Object to store all the navigation and other informations
    this.LocalData = {
      CallFrom: '',
      Title: '',
      RoleId: '1',
      Location: '1',
    };
    this.state = {
      LocationInfo_isShow: false,
      LocationInfo_DetailInfo: [],
      islocal: false,
      isFetched: false,
      iSDates: true,
      FromDate: dateString,
      ToDate: dateString,
      ActiveTab: 0,
      FreezedContentHeight: new Animated.Value(0),
      Sales: [],
      YearSales: [],
      MonthSales: [],
      WeekSales: [],
      HourSales: [],
      TopSalesMan: [],
      TopProducts: [],
      TopPerformar: [],
      TopNonPerformar: [],
      DataDash: [],
    };
  }
  componentDidMount() {
    let {params} = this.props.route;
    this.LocalData.Title = params.HeaderTitle;
    // console.log('my dash : ', this.LocalData.Title , ' par tit :' , params.HeaderTitle);
    this.LocalData.CallFrom = params.CallFrom;
    // console.log('my call from : ', this.LocalData.CallFrom , ' par callfr om :' , params.CallFrom);
    this.props.navigation.setOptions({title: params.HeaderTitle});
    this.InitialSetup();
  }
  InitialSetup = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    this.LocalData.RoleId = await AsyncStorage.getItem('RoleId');
    this.LoadRetail();
  };

  MDashBoardList;
  LoadRetail = async () => {
    let uri = `${this.IP}/api/Dashboard/GetSelectedMobileReportData?ReportMasterId=${this.LocalData.CallFrom}&FromDate=${this.state.FromDate}&ToDate=${this.state.ToDate}&LocationCode=${this.LocalData.Location}&RoleId=${this.LocalData.RoleId}`;
    console.log(uri, ' ', this.Token);
    Request.get(uri, this.Token)
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            this.setState({isFetched: true, DataDash: res.data}, () => {
              this.PrepareListForUnChangedData();
            });
          } else {
            this.MDashBoardList = [];
            AlertMessage('No Data found');
            this.setState({isFetched: true, NoDataAvailable: true});
          }
        }
      })
      .catch(err => {
        console.error(err);
        AlertMessage('Something Went wrong\n' + JSON.stringify(err));
        this.setState({isFetched: true});
      });
  };
  ToggleFreezedContent = () => {
    this.setState({iSDates: !this.state.iSDates}, () => {
      Animated.timing(this.state.FreezedContentHeight, {
        toValue: this.state.iSDates ? 0 : 50,
        duration: 500,
      }).start();
    });
  };
  HighlightSelectedTab = event => {
    this.setState({ActiveTab: event.nativeEvent.position});
  };

  HandlesetDate = Type => {
    this.setState({isFetched: false}, this.HandleProceed(Type));
  };
  getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  RetunValueinThousands = num => {
    var si = [
      {value: 1, symbol: ''},
      {value: 1e3, symbol: ' k'},
      {value: 1e6, symbol: ' M'},
      {value: 1e9, symbol: ' G'},
      {value: 1e12, symbol: ' T'},
      {value: 1e15, symbol: ' P'},
      {value: 1e18, symbol: ' E'},
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(2).replace(rx, '$1') + si[i].symbol;
  };

  RoundNumberValue = num => {
    return (Math.round(num * 100) / 100)
      .toString()
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  };

  HandleProceed = Type => {
    var current = new Date();
    var Start = '';
    var End = '';
    if (Type === 'Day') {
      Start = moment(new Date()).format('YYYY-MM-DD');
      End = moment(new Date()).format('YYYY-MM-DD');
    } else if (Type === 'Week') {
      var weekstart = current.getDate() - current.getDay();
      var weekend = weekstart + 7; // end day is the first day + 6
      Start = new Date(current.setDate(weekstart));
      End = new Date(current.setDate(weekend));
    } else if (Type === 'Month') {
      Start = new Date(current.getFullYear(), current.getMonth(), 1);
      End = new Date(current.getFullYear(), current.getMonth() + 1, 0);
    } else if (Type === 'Year') {
      if (current.getMonth() + 1 <= 3) {
        Start = current.getFullYear() - 1 + '-04-' + '01';
        End = current.getFullYear() + '-03-' + '31';
      } else {
        Start = current.getFullYear() + '-04-' + '01';
        End = current.getFullYear() + 1 + '-03-' + '31';
      }
    } else if (Type === 'CustDate') {
      Start = this.state.FromDate;
      End = this.state.ToDate;
    }
    console.log(Start, End);
    this.setState(
      {
        FromDate: moment(Start).format('YYYY-MM-DD'),
        ToDate: moment(End).format('YYYY-MM-DD'),
        isFetched: true,
      },
      this.LoadRetail,
    );
  };

  NavigateToLocation = LocationInfo => {
    this.setState({
      LocationInfo_isShow: true,
      LocationInfo_DetailInfo: LocationInfo,
    });
  };

  PrepareListForUnChangedData = () => {
    if (this.state.DataDash.length) {
      this.MDashBoardList = this.state.DataDash.map(item =>
        item.Type === 'Card' ? (
          item.SubMenu.map(tem =>
            tem.DataInfo.map((head, index) =>
              head.LocationInfo && head.LocationInfo.length > 0 ? (
                <View
                  key={`mycardview_${tem.Key}_${index}`}
                  style={[
                    styles.RSMContainer,
                    {alignSelf: 'stretch'},
                    head.BackColor
                      ? {
                          backgroundColor: head.BackColor.toLowerCase(),
                          borderLeftWidth: 0,
                        }
                      : {
                          borderColor: head.Color
                            ? head.Color.toLowerCase()
                            : this.getRandomColor(),
                        },
                    ,
                  ]}>
                  <TouchableOpacity
                    onPress={() => this.NavigateToLocation(head.LocationInfo)}>
                    <Text
                      style={[
                        styles.HText,
                        {
                          color: head.Color
                            ? head.Color.toLowerCase()
                            : 'black',
                        },
                      ]}>
                      {head.Header}
                    </Text>
                    <Text
                      style={[
                        styles.AText,
                        {
                          color: head.Color
                            ? head.Color.toLowerCase()
                            : 'black',
                        },
                      ]}>
                      {' '}
                      {this.RetunValueinThousands(head.Value)}
                    </Text>
                    {head.Value > 999 && (
                      <Text
                        adjustsFontSizeToFit
                        numberOfLines={1}
                        style={{
                          textAlignVertical: 'center',
                          textAlign: 'center',
                          backgroundColor: 'rgba(0,0,0,0)',
                          color: '#cc0000',
                        }}>
                        {' '}
                        {'( ' + this.RoundNumberValue(head.Value) + ' )'}
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  key={`mycardview_${tem.Key}_${index}`}
                  style={[
                    styles.RSMContainer,
                    {alignSelf: 'stretch'},
                    head.BackColor
                      ? {
                          backgroundColor: head.BackColor.toLowerCase(),
                          borderLeftWidth: 0,
                        }
                      : {
                          borderColor: head.Color
                            ? head.Color.toLowerCase()
                            : this.getRandomColor(),
                        },
                    ,
                  ]}>
                  <Text
                    style={[
                      styles.HText,
                      {
                        color: head.Color ? head.Color.toLowerCase() : 'black',
                      },
                    ]}>
                    {head.Header}
                  </Text>
                  <Text
                    style={[
                      styles.AText,
                      {
                        color: head.Color ? head.Color.toLowerCase() : 'black',
                      },
                    ]}>
                    {' '}
                    {this.RetunValueinThousands(head.Value)}
                  </Text>
                  {head.Value > 999 && (
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={1}
                      style={{
                        textAlignVertical: 'center',
                        textAlign: 'center',
                        backgroundColor: 'rgba(0,0,0,0)',
                        color: '#cc0000',
                      }}>
                      {' '}
                      {'( ' + this.RoundNumberValue(head.Value) + ' )'}
                    </Text>
                  )}
                </View>
              ),
            ),
          )
        ) : item.Type === 'Pie' ? (
          item.SubMenu.map(tem => (
            <View
              key={`myPieview_${tem.Key}`}
              style={{height: 250, width: 350, marginTop: 5}}>
              <View
                style={{
                  padding: 3  ,
                  height: 30,
                  alignContent: 'center',
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                 borderBottomWidth: 2,
                
                  borderBottomColor: '#2962ff',
                }}>
                <Text style={{fontSize: 18, fontWeight: 'bold', textAlign: 'left'}}>
                  {tem.Name}
                </Text>
              </View>
              {tem.DataInfo && tem.DataInfo.length > 0 ? (
                <View style={{alignItems: 'center', height: 270}}>
                  <PieChart
                    data={tem.DataInfo.map(elem => ({
                      ...elem,
                      name: elem.Header,
                      legendFontColor: '#7F7F7F',
                      legendFontSize: 15,
                      color: this.getRandomColor(),
                    }))}
                    width={350}
                    height={220}
                    chartConfig={{
                      backgroundColor: '#1cc910',
                      backgroundGradientFrom: '#eff3ff',
                      backgroundGradientTo: '#efefef',
                      decimalPlaces: 2,
                      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                      style: {
                        borderRadius: 16,
                      },
                    }}
                    style={{
                      // marginVertical: 8,
                      borderRadius: 16,
                    }}
                    accessor="Value"
                    backgroundColor="transparent"
                    paddingLeft="10"
                  />
                </View>
              ) : (
                <Text
                  style={{
                    marginLeft: '30%',
                    marginRight: '30%',
                    marginTop: '30%',
                    marginBottom: '30%',
                  }}>
                  No Data Available
                </Text>
              )}
            </View>
          ))
        ) : item.Type == 'Line' ? (
          item.SubMenu.map(tem => (
            <View
              key={`myLineview_${tem.Key}`}
              style={{height: 250, width: 350, marginTop: 5}}>
              <View
                style={{
                  padding: 5,
                  height: 30,
                  alignContent: 'center',
                  borderTopLeftRadius: 5,
                  borderTopRightRadius: 5,
                  borderBottomWidth: 2,
                  borderBottomColor: this.getRandomColor(),
                }}>
                <Text
                  style={{fontSize: 18, fontWeight: 'bold', textAlign: 'left'}}>
                  {tem.Name}
                </Text>
              </View>
              {tem.DataInfo && tem.DataInfo.length > 0 ? (
                <View style={{alignItems: 'center', height: 270}}>
                  <LineChart
                    data={{
                      labels: tem.DataInfo.map(obj => obj.Header), //this.state.HourSales.map(obj => obj.Hour),
                      datasets: [
                        {
                          data: tem.DataInfo.map(obj => obj.Value),
                        },
                      ],
                    }}
                    width={350} // from react-native
                    height={210}
                    yLabelsOffset={5}
                    withInnerLines={false}
                    chartConfig={{
                      backgroundColor: 'white',
                      backgroundGradientFrom: 'white',
                      backgroundGradientTo: this.getRandomColor(),
                      fillShadowGradient: this.getRandomColor(),

                      decimalPlaces: 0, // optional, defaults to 2dp
                      color: () => this.getRandomColor(),
                      labelColor: () => 'black',

                      style: {
                        marginLeft: 10,
                      },
                      propsForDots: {
                        r: '6',
                        strokeWidth: '10',
                        barPercentage: 0.5,

                        // stroke: "#ffa726"
                      },
                    }}
                    bezier
                    style={{
                      // marginVertical: 8,
                      borderRadius: 16,
                    }}
                  />
                </View>
              ) : (
                <Text
                  style={{
                    marginLeft: '30%',
                    marginRight: '30%',
                    marginTop: '30%',
                    marginBottom: '30%',
                  }}>
                  No Data Available
                </Text>
              )}
            </View>
          ))
        ) : (
          <Text />
        ),
      );
    }
    this.forceUpdate();
  };

  render() {
    return (
      <>
        <View style={{flex: 1}}>
          <View>
            <View
              style={{flexDirection: 'row', width: '100%', marginTop: '2%'}}>
              <View style={styles.RContainer}>
                <TouchableOpacity onPress={() => this.HandlesetDate('Day')}>
                  <Image
                    style={styles.RImage}
                    source={require('../../../assets/images/Day.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.RContainer}>
                <TouchableOpacity onPress={() => this.HandlesetDate('Week')}>
                  <Image
                    style={styles.RImage}
                    source={require('../../../assets/images/Week.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.RContainer}>
                <TouchableOpacity onPress={() => this.HandlesetDate('Month')}>
                  <Image
                    style={styles.RImage}
                    source={require('../../../assets/images/Month.png')}
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.RContainer}>
                <TouchableOpacity onPress={() => this.HandlesetDate('Year')}>
                  <Image
                    style={styles.RImage}
                    source={require('../../../assets/images/Year.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={{width: '40%'}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      flexDirection: 'row',
                      backgroundColor: '#0D2946',
                      borderRadius: 10,
                      padding: 5,
                      marginRight: 5,
                    }}
                    onPress={() => this.HandlesetDate('CustDate')}>
                    <Image
                      style={{
                        height: 25,
                        width: 25,
                        marginRight: 3,
                        marginTop: 3,
                      }}
                      source={require('../../../assets/images/correct.png')}
                    />
                    <Text style={{color: 'white'}}>Proceed</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      backgroundColor: '#0D2946',
                      flexDirection: 'row',
                      borderRadius: 10,
                      padding: 5,
                    }}
                    onPress={this.ToggleFreezedContent}>
                    <Image
                      style={{
                        height: 25,
                        width: 25,
                        marginRight: 3,
                        marginTop: 3,
                      }}
                      source={require('../../../assets/images/calendar.png')}
                    />
                    <Text style={{color: 'white', textAlign: 'center'}}>
                      Dates
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Animated.View style={[{height: this.state.FreezedContentHeight}]}>
            <View style={{width: '98%', flexDirection: 'row',position:'absolute',}}>
                  <ModalDatePicker
                  isShowAnimateText={false}
                  placeholder={'From'}
                      fieldWrapperStyle={{width: '50%'}}
                    selectedValue={this.state.FromDate}
                    onValueSelected={(FromDate) => this.setState({FromDate})}
                    />        
                  <ModalDatePicker
                  isShowAnimateText={false}
                    placeholder={'From'}
                      fieldWrapperStyle={{width: '50%'}}
                    selectedValue={this.state.ToDate}
                    onValueSelected={(ToDate) => this.setState({ToDate})}
                  />
                {/* <DatePicker
                  ref={'FromDate'}
                  style={[{width:'50%'}]}
                  format="YYYY-MM-DD"
                  mode="date"
                  date={this.state.FromDate}
                  onDateChange={FromDate => this.setState({FromDate})}
                  confirmBtnText="ok"
                  cancelBtnText="cancel"
                  customStyles={{
                    dateInput: {
                      borderColor: 'transparent',
                    },
                    dateText: [
                      {
                        alignSelf: 'flex-start',
                        paddingLeft: 4,
                        paddingTop: 8,
                      },
                      layoutStyle.FieldInputFont,
                    ],
                    placeholderText: {
                      alignSelf: 'flex-start',
                      color: 'black',
                    },
                  }}
                  // showIcon={false}
                  placeholder="From"
                />

                <DatePicker
                  ref={'ToDate'}
                  style={[{width:'50%'}]}
                  format="YYYY-MM-DD"
                  mode="date"
                  date={this.state.ToDate}
                  onDateChange={ToDate => this.setState({ToDate})}
                  confirmBtnText="ok"
                  cancelBtnText="cancel"
                  customStyles={{
                    dateInput: {
                      borderColor: 'transparent',
                    },
                    dateText: [
                      {
                        alignSelf: 'flex-start',
                        paddingLeft: 4,
                        paddingTop: 8,
                      },
                      layoutStyle.FieldInputFont,
                    ],
                    placeholderText: {
                      alignSelf: 'flex-start',
                      color: 'black',
                    },
                  }}
                  // showIcon={false}
                  placeholder="To"
                /> */}

              </View>
            </Animated.View>
          </View>
        
          <ScrollView style={{paddingVertical: 10, backgroundColor: 'white'}}>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                marginTop:5,
              }}>
              {this.MDashBoardList}
            </View>
          </ScrollView>
          <Modal
            transparent={false}
            animationType="fade"
            visible={this.state.LocationInfo_isShow}>
            <SafeAreaView
              style={{
                width: '100%',
                height: '100%',
                borderRadius: 5,
                flexDirection: 'column',
                justifyContent: 'space-evenly',
              }}>
              <View
                style={{
                  width: '100%',
                  height: '95%',
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ScrollView>
                  <Table
                    borderStyle={{
                      borderWidth: 5,
               
                      borderColor: '#B5B5B5',
                    }}>
                    <LocationDetailsBind
                      data={this.state.LocationInfo_DetailInfo}
                    />
                  </Table>
                </ScrollView>
              </View>
              <View
                style={{
                  width: '100%',
                  height: '5%',
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      LocationInfo_isShow: false,
                      LocationInfo_DetailInfo: [],
                    })
                  }>
                  <Image
                    style={styles.RImage}
                    source={require('../../../assets/images/Close.png')}
                  />
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </Modal>
        </View>
        <ICubeAIndicator isShow={!this.state.isFetched} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  RContainer: {
    width: 45,
    alignItems: 'center',
  },
  RText: {
    textAlign: 'center',
    fontSize: 14,
  },
  RImage: {
    width: 35,
    height: 35,
  },
  RMContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 5,
  },

  ActiveTab: {
    color: 'black',
    fontSize: 50,
  },
  InActiveTab: {
    color: 'red',
    fontSize: 50,
  },
  Tabs: {
    flexDirection: 'row',
    borderTopColor: 'rgb(221, 221, 221)',
    justifyContent: 'center',
    height: 50,
  },
  RSMContainer: {
    padding: 10,
    width: 170,
    borderLeftWidth: 8,
    marginBottom: 10,
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 5,
  },
  HText: {
    fontSize: 14,
  },
  AText: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  ViewPager: {
    height: 240,
    backgroundColor: 'white',
    paddingBottom: 5,
    elevation: 5,
  },
});
