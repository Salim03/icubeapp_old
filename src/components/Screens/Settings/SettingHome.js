import React, { Component } from 'react';
import {
	View, SafeAreaView, StatusBar, Text, StyleSheet, ScrollView, Image, TouchableOpacity
} from 'react-native';

export default class SettingHome extends Component {

	NavigateToForm = form => {

		if (form === "Config") {
			this.props.navigation.navigate("Config");
			// this.props.navigation.navigate("Config", { fromSettings: true });
		}
	}
	render() {
		return (
			<>
				<SafeAreaView style={{ flex: 1 }}>
					<View style={styles.LoadContainer}>
						<ScrollView style={styles.DataContainer}>
							<View style={styles.ListContainer}>
								<TouchableOpacity style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "Config")}>
									<Image
										source={require('../../../assets/images/Home/Setting/ip.png')}
										style={styles.FormImages}
									/>
									<Text style={styles.FormLabel}>Config</Text>
								</TouchableOpacity>
								{/* <View style={styles.FormContainer}>
                        <TouchableOpacity>
                                <Image
                                    source={require('../../../assets/images/Home/Sales/Sales.png')}
                                    style={styles.FormImages}
                                />
                                <Text style={styles.FormLabel}>Collection</Text>
                            </TouchableOpacity>
                        </View> */}
							</View>
						</ScrollView>
					</View>
					<View style={{ backgroundColor: 'rgba(0,0,0,0)', position: 'absolute', bottom: 0, right: 0 }}>
						{/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
						<View style={{ alignItems: 'flex-end', marginRight: 20, flexDirection: 'row', marginBottom: 5 }}>
							<Text style={{ textAlign: 'right' }}>Powered By</Text>
							<Image
								source={require('../../../assets/images/Drawer/Logo.png')}
								style={{ height: 30, width: 100 }}
							/>
						</View>
					</View>
				</SafeAreaView>
			</>
		);
	}
};

const styles = StyleSheet.create({
	Text: {
		marginTop: '80%',
		textAlign: 'center',
		fontSize: 22,
		fontWeight: 'bold',
	},
	LoadContainer: {
		// backgroundColor: 'rgb(43, 54, 255)',
		backgroundColor: '#81D4FA',
		flex: 1
	},
	FormImages: {
		width: 30,
		height: 30,
		marginLeft: 5,
		marginRight: 5,
		marginBottom: 5,
		marginTop: 5
	},
	FormLabel: {
		textAlign: 'center',
		fontSize: 16,
		// fontWeight: 'bold',
	},
	DataContainer: {
		backgroundColor: 'white',
		marginTop: 30,
		// marginLeft: 5,
		// marginRight: 5,
		borderTopLeftRadius: 35,
		borderTopRightRadius: 35,
		flex: 1,
		width: '100%',
		elevation: 50,
	},
	ListContainer: {
		marginTop: 10,
		// marginLeft:20,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		width: '100%',
		marginBottom: 5,
		flexWrap: 'wrap',
	},
	FormContainer: {
		// backgroundColor: 'rgb(252, 172, 182)',
		backgroundColor: 'white',
		marginBottom: 10,
		padding: 5,
		height: 80,
		width: 100,
		alignItems: 'center',
		borderRadius: 10,
		elevation: 15,
	},
});
	// export {Retail};