import React from 'react';

import {
  Text,
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  ScrollView,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {StackActions} from '@react-navigation/native';
import Menu, {MenuItem} from 'react-native-material-menu';
import {Button} from '../../../components/Custom/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ICubeAIndicator from '../../../components/Tools/ICubeIndicator';

import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import {
  AlertMessage,
  Request,
  LogoutUser,
  isnull,
  GetNewUUID,
  IsServerConnected,
} from '../../../Helpers/HelperMethods';

const ListItem = props => {
  let {data, index, isSelectedServer} = props;
  return (
    <TouchableWithoutFeedback onLongPress={() => props.HandleEditConfig(index)}>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: '80%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sxxl,
                  isSelectedServer
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {`${data.Name} - [ ${data.IP} ]`}
            </Text>
          </View>
          {/* <View style={[styles.CardTextContainer, {width: '48%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.IP}
            </Text>
          </View> */}
        </View>

        <View style={styles.CardBtnOptionWrapper}>
          <TouchableOpacity
            style={styles.CardBtnOption}
            onPress={() => props.HandleShowMenu(index)}>
            <Text
              style={[
                styles.CardBtnOptionText,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.mediam.mm,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              
            </Text>
          </TouchableOpacity>
          <Menu
            ref={props.setMenuRef}
            button={<Text style={{width: 0, height: 0}} />}>
            <MenuItem onPress={() => props.GoConfig(index)}>Go</MenuItem>
            <MenuItem onPress={() => props.HandleEditConfig(index)}>
              Edit
            </MenuItem>
            <MenuItem onPress={() => props.HandleRemoveConfig(index)}>
              Remove
            </MenuItem>
          </Menu>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class Config extends React.Component {
  constructor() {
    super();
    this.state = {
      ShowType: '',
      SelectedIndex: -1,
      ConfigList: [],
      isShowLocation: false,
      LocationList: [],
      GUID: '',
      Name: '',
      IP: '',
      LocationCode: 0,
      DashboardURL: '',
      Session_GUID: '',
    };
  }

  SetStateForNewConfig = (PassConfigObject, showAddScreen, selectedindex) => {
    let get_isShowLocation = false,
      get_SelectedIndex = selectedindex > -1 ? selectedindex : -1,
      get_GUID = '',
      get_IP = '',
      get_Name = '',
      get_LocationCode = 0,
      get_DashboardURL = '';
    if (PassConfigObject) {
      get_isShowLocation = PassConfigObject.LocationCode > 0;
      get_IP = PassConfigObject.IP;
      get_LocationCode =
        PassConfigObject.LocationCode == null
          ? 0
          : parseInt(PassConfigObject.LocationCode);
      // this.HandleLoctiondata(get_IP, get_LocationCode, true);
      get_GUID = PassConfigObject.GUID;
      get_Name = PassConfigObject.Name;
      get_DashboardURL = PassConfigObject.DashboardURL;
    }
    this.setState({
      isShowLocation: get_isShowLocation,
      SelectedIndex: get_SelectedIndex,
      GUID: get_GUID,
      IP: get_IP,
      Name: get_Name,
      LocationCode: get_LocationCode,
      DashboardURL: get_DashboardURL,
      ShowType: showAddScreen ? 'Add' : 'List',
    });
  };
  componentDidMount() {
    this.initLoad();
  }
  initLoad = async () => {
    const [[, get_Config], [, get_GUID]] = await AsyncStorage.multiGet([
      'ConfigList',
      'GUID',
    ]);
    let FinaData = get_Config ? JSON.parse(get_Config) : [];
    this.setState({
      ConfigList: FinaData,
      Session_GUID: get_GUID ? get_GUID : '',
      isLoading: false,
      ShowType: FinaData && FinaData.length > 0 ? 'List' : 'Add',
    });
  };

  SavetoStorage = async passconfigList => {
    await AsyncStorage.setItem('ConfigList', JSON.stringify(passconfigList));
  };

  ChangeIp = async () => {
    let {IP} = this.state;
    IP = IP.trim();
    if (IP === null || IP === '') {
      AlertMessage('Enter IP Address');
      this.refs.ip.focus();
    } else {
      Keyboard.dismiss();
      IP = this.applyipformat(IP);
      this.HandleLoctiondata(IP, this.state.LocationCode, false);
    }
  };

  applyipformat = passip => {
    passip = passip.startsWith('http') ? passip : 'http://' + passip;
    passip = passip.endsWith('/') ? passip : passip + '/';
    return passip;
  };

  ConfigureIPAddress = async () => {
    let {IP, LocationCode, Name, LocationList, DashboardURL} = this.state;
    IP = IP.trim();
    if (isnull(IP, '') == '') {
      AlertMessage('Enter IP Address');
      this.refs.ip.focus();
    } else if (isnull(LocationCode, '0') == '0') {
      AlertMessage('select location');
    } else if (isnull(Name, '') == '') {
      AlertMessage('Enter Name.');
    } else if (LocationList.length == 0) {
      AlertMessage('No Location available');
    } else if (
      (LocationList.find(r => r.LocationCode == LocationCode) || []).length == 0
    ) {
      AlertMessage('Invalid Location selected.');
    } else {
      Keyboard.dismiss();
      IP = this.applyipformat(IP);
      let IsAdd = true;
      let OldStateConfigList = [...this.state.ConfigList];
      let SaveObject = {
        GUID: GetNewUUID(),
        Name: Name,
        IP: IP,
        LocationCode: LocationCode,
        DashboardURL: DashboardURL,
      };
      if (this.state.SelectedIndex > -1) {
        let getData = OldStateConfigList[this.state.SelectedIndex];
        if (getData) {
          SaveObject.GUID = getData.GUID;
          OldStateConfigList[this.state.SelectedIndex] = SaveObject;
          IsAdd = false;
        }
      }
      if (IsAdd) {
        OldStateConfigList = [...OldStateConfigList, SaveObject];
      }
      // console.log('config list : ', OldStateConfigList);
      this.SavetoStorage(OldStateConfigList);
      this.setState({ConfigList: OldStateConfigList, ShowType: 'List'}, () => {
        LogoutUser();
        this.applylocalstorageGlobalData(
          SaveObject.GUID,
          IP,
          LocationCode,
          DashboardURL,
        );
        this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
      });
    }
  };

  applylocalstorageGlobalData = async (
    GUID,
    IP,
    LocationCode,
    DashboardURL,
  ) => {
    await AsyncStorage.multiSet([
      ['GUID', GUID.toString()],
      ['IP', IP],
      ['LocationCode', LocationCode.toString()],
      ['DashboardURL', DashboardURL],
    ]);
  };

  HandleLoctiondata = async (PassIp, LocationCode) => {
    this.setState(prevstate => ({
      ...prevstate,
      isLoading: true,
    }));
    Request.get(`${PassIp}api/common/getactivelocation`)
      .then(res => {
        if (res.data.length > 0) {
          let Loca_Code = LocationCode;
          if (res.data.length > 0 && Loca_Code == 0) {
            Loca_Code = res.data[0].LocationCode;
          }
          this.setState({
            LocationList: res.data,
            LocationCode: Loca_Code,
            ApplyText: Loca_Code == 0 ? 'Next' : 'Apply',
            isLoading: false,
          });
          return;
        } else {
          this.setState({
            LocationList: [],
            ApplyText: 'Next',
            isLoading: false,
          });
          AlertMessage('No Location found');
        }
      })
      .catch(err => {
        this.setState({
          LocationList: [],
          isLoading: false,
        });
        console.error(err);
        AlertMessage('Something Went wrong');
      });
  };

  HandleChangeName = event => {
    this.setState({
      Name: event.nativeEvent.text,
    });
  };

  HandleChangeDashboardURL = event => {
    this.setState({
      DashboardURL: event.nativeEvent.text,
    });
  };
  HandleChange = event => {
    this.setState({
      IP: event.nativeEvent.text,
    });
  };
  setMenuRef = (ref, index) => {
    this[`_menu${index}`] = ref;
  };

  setExtraActionMenuRef = ref => {
    this._ExtraActionMenu = ref;
  };

  showExtraActionsMenu = () => this._ExtraActionMenu.show();

  HideExtraActionsMenu = () => this._ExtraActionMenu.hide();

  hideMenu = index => this[`_menu${index}`].hide();

  showMenu = index => this[`_menu${index}`].show();

  GoConfig = async index => {
    if (index > -1) {
      this.hideMenu(index);
      let get_ConfigList = this.state.ConfigList;
      let getData = get_ConfigList[index];
      if (getData) {
        this.setState({isLoading: true});
        let isConnect = await IsServerConnected(true, getData.IP);
        if (isConnect) {
          this.applylocalstorageGlobalData(
            getData.GUID,
            getData.IP,
            getData.LocationCode,
            getData.DashboardURL,
          );
          this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
        }
      }
    }
    this.setState({isLoading: false});
  };
  HandleEditConfig = index => {
    if (index > -1) {
      this.hideMenu(index);
    }
    this.SetStateForNewConfig(
      index > -1 && this.state.ConfigList[index],
      true,
      index,
    );
  };
  HandleRemoveConfig = async index => {
    if (index > -1) {
      this.hideMenu(index);
      let get_ConfigList = this.state.ConfigList;
      let getData = get_ConfigList[index];
      if (getData) {
        if (this.state.Session_GUID == getData.GUID) {
          console.log('remove same config');
          this.applylocalstorageGlobalData('', '', '0', '');
        }
        get_ConfigList.splice(index, 1);
        this.setState({ConfigList: get_ConfigList, ShowType: 'List'});
        this.SavetoStorage(get_ConfigList);
      }
    }
  };
  render() {
    // console.log(
    //   'render : ',
    //   this.state.Session_GUID,
    //   ' Config data : ',
    //   this.state.ConfigList,
    // );
    if (this.state.ShowType == 'List') {
      return (
        <SafeAreaView style={{flex:1}}>
          <ICubeAIndicator isShow={this.state.isLoading} />
          <FlatList
            style={[
              {paddingVertical: 10},
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            data={this.state.ConfigList}
            renderItem={({item, index}) => (
              <ListItem
                data={item}
                index={index}
                HandleEditConfig={this.HandleEditConfig}
                GoConfig={this.GoConfig}
                HandleRemoveConfig={this.HandleRemoveConfig}
                HandleShowMenu={this.showMenu}
                setMenuRef={ref => this.setMenuRef(ref, index)}
                isSelectedServer={this.state.Session_GUID == item.GUID}
              />
            )}
            keyExtractor={(item, index) => item.GUID.toString()}
            // ListHeaderComponent={
            //   <ListItemHeader
            //     isAgainst={this.state.Against !== ''}
            //     headers={[...this.visibleCreatedItemsGroupCaptions]}
            //   /> // [...] spreading this to create a new array everytime, just to make the header rerender
            // }
            stickyHeaderIndices={[0]}
          />

          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.HandleEditConfig(-1);
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
        </SafeAreaView>
      );
    } else if (this.state.ShowType == 'Add') {
      return (
        <>
          <ICubeAIndicator isShow={this.state.isLoading} />
          <ScrollView
            keyboardShouldPersistTaps="always"
            nestedScrollEnabled={true}
            style={styles.ScrollViewWrapper}>
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.ml,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.captionTextCustom,
              ]}>
              Configuration
            </Text>
            <View style={styles.InputsContainer}>
              <TextInput
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sxxl,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BorderColor,
                  ),
                  styles.Inputs,
                ]}
                placeholder="Please enter cloud or server ID"
                ref={'ip'}
                onChange={this.HandleChange}
                value={this.state.IP}
                autoCapitalize="none"
                editable={this.state.ApplyText != 'Apply'}
              />
              {/* <TouchableOpacity
              style={styles.ConfigButon}
                onPress={this.ChangeIp}>
                <Image
                  resizeMode="stretch"
                  source={require('../../../assets/images/post-light.png')}
                  style={styles.FormImages}
              />
            </TouchableOpacity> */}
            </View>
            {this.state.ApplyText == 'Apply' && (
              <>
                <View style={styles.ScrollContentWrapper}>
                  <ModalSearchablePicker
                    placeholder="Location"
                    fieldWrapperStyle={{width: '98%'}}
                    data={this.state.LocationList}
                    labelProp="LocationName"
                    valueProp="LocationCode"
                    selectedValue={this.state.LocationCode}
                    onValueSelected={LocationCode =>
                      this.setState({LocationCode})
                    }
                    inputStyle={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Regular,
                        globalFontObject.Size.small.sxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BorderBottomColor,
                      ),
                    ]}
                  />
                </View>
                <TextInput
                  style={[
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderColor,
                    ),
                    styles.Dashboard,
                  ]}
                  placeholder="Name"
                  onChange={this.HandleChangeName}
                  value={this.state.Name}
                  autoCapitalize="none"
                />
              </>
            )}
            {/* <TextInput
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sxxl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                styles.Dashboard,
              ]}
              placeholder="Dashboard URL"
              onChange={this.HandleChangeDashboardURL}
              value={this.state.DashboardURL}
              autoCapitalize="none"
            /> */}
            <View
              style={{flexDirection: 'row', justifyContent: 'center', flex: 1}}>
              {this.state.ConfigList.length > 0 && (
                <View style={{width: '50%'}}>
                  <Button
                    value="Close"
                    onPressEvent={() => {
                      this.setState({ShowType: 'List'});
                    }}
                  />
                </View>
              )}
              <View style={{width: '50%'}}>
                <Button
                  value={this.state.ApplyText == 'Apply' ? 'Apply' : 'Connect'}
                  onPressEvent={
                    this.state.ApplyText == 'Apply'
                      ? this.ConfigureIPAddress
                      : this.ChangeIp
                  }
                />
              </View>
            </View>
          </ScrollView>
        </>
      );
    } else {
      return <></>;
    }
  }
}

const styles = StyleSheet.create({
  ScrollViewWrapper: {
    flex: 1,
    paddingHorizontal: 8,
    paddingTop: '30%',
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,

    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  Dashboard: {
    paddingVertical: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    zIndex: 0,
    borderBottomWidth: 2,
  },
  ScrollContentWrapper: {
    paddingVertical: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 10,
    zIndex: 0,
  },
  WrapperContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    backgroundColor: 'white',
  },
  captionTextCustom: {
    paddingLeft: 10,
    marginBottom: 10,
  },
  InputsContainer: {
    width: '100%',
    flexDirection: 'row',
    paddingHorizontal: 10,
    height: 45,
  },
  FormImages: {
    width: 40,
    height: 45,
    margin: 2,
  },
  Inputs: {
    borderBottomWidth: 2,
    color: 'black',
    width: '100%',
    borderRadius: 8,
  },
  ConfigButon: {
    paddingHorizontal: 10,
    borderRadius: 8,
    height: 40,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 5,
    marginVertical: 3,
    marginHorizontal: 5,
  },

  CardBtnOptionWrapper: {
    position: 'absolute',
    top: 20,
    right: 5,
  },
  CardBtnOptionText: {
    lineHeight: 25,
    transform: [{scaleX: 1.7}],
    top: -5,
  },
  CardBtnOption: {
    width: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
