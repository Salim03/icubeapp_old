// Module Imports
import React, { Component } from 'react';
import {
	TextInput,View,SafeAreaView,StatusBar,Text,Switch,StyleSheet,
	Image,TouchableOpacity,TouchableWithoutFeedback,FlatList,
	 	} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import { FloatingAction } from 'react-native-floating-action';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {AlertMessage,Request} from '../../../Helpers/HelperMethods';
import {
	globalFontObject,
	globalColorObject,
	ApplyStyleFontAndSizeAndColor,
	ApplyStyleColor,
 } from '../../../Style/GlobalStyle';
//  import renderTouchableHighlight from '../Common/SearchClear';
// Local Imports
const ListItemHeader = () => {
		return(
			<View style={[
				styles.StickeyHeaderCard,
				ApplyStyleColor(
				  globalColorObject.Color.Primary,
				  globalColorObject.ColorPropetyType.BackgroundColor,
				),
			 ]}>
			<View style={[styles.CardTextContainer, { width: '50%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>{'  '}Invoice Id   {/* {props.data.DOCNO || ''} */}</Text>
				 {/* <Text numberOfLines={1} style={styles.BlockDetailData}>  </Text>  */}
				
			</View>
			<View style={[styles.CardTextContainer, { width: '49%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            {textAlign: 'right'}]}>Customer Id  {/* {DocDate || ''} */}</Text>
				{/* <Text numberOfLines={1} style={styles.BlockDetailData}>{DocDate || ''} </Text> */}
				
			</View>
			<View style={[styles.CardTextContainer, { width: '50%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>{'  '}Check In date {/* {props.data.DOCNO || ''} */}</Text>
				 {/* <Text numberOfLines={1} style={styles.BlockDetailData}>  </Text>  */}
				
			</View>
			<View style={[styles.CardTextContainer, { width: '49%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            {textAlign: 'right'}]}>Check Out Date {/* {DocDate || ''} */}</Text>
				{/* <Text numberOfLines={1} style={styles.BlockDetailData}>{DocDate || ''} </Text> */}
				
			</View>
			<View style={[styles.CardTextContainer, { width: '100%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>{'  '}Customer Name {/* {props.data.AgainstNo || ''} */} </Text>
				{/* <Text numberOfLines={1} style={styles.BlockDetailData}>	 {props.data.AgainstNo || ''}  </Text> */}
				</View>
			<View style={[styles.CardTextContainer, { width: '100%' }]}>
				<Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>{'  '}Courier Status {/* { AgainstDate || ''} */} </Text>
				{/* <Text numberOfLines={1} style={styles.BlockDetailData}> { AgainstDate || ''} </Text> */}
				
			</View>
		</View>
		);
	}

const ListItem = (props) => {
	let FullTrackDate = new Date(props.data[props.type ? 'CourierDate' : 'OutCourierDate']), FullLRdate = new Date(props.data[props.type ? 'ConsignmentDate' : 'OutConsignmentDate']);
	let TrackDate = `${("0" + FullTrackDate.getDate()).slice(-2)}/${("0" + (FullTrackDate.getMonth() + 1)).slice(-2)}/${FullTrackDate.getFullYear()}`;
	let CourierDate = `${("0" + FullLRdate.getDate()).slice(-2)}/${("0" + (FullLRdate.getMonth() + 1)).slice(-2)}/${FullLRdate.getFullYear()}`;

	return (
		<TouchableWithoutFeedback onLongPress={() => props.onLongPress(props.type ? 'CourierIn' : 'CourierOut', props.data[props.type ? 'Id' : 'OutId'])}>
			<View style={styles.Card}>
				{/* First Line to show TrackNo AND LRNo */}
				<View style={styles.CardLine}>
					<View style={styles.CardTextContainer}>
						<Text numberOfLines={1} style={[
                	ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                	),]}>
						{props.data[props.type ? 'InCourierNo' : 'OutCourierNo']}
						</Text>
					</View>
					<View style={styles.CardTextContainer}>
						<Text numberOfLines={1} style={[
                	ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'}]}>{props.data.ConsignmentId}</Text>
					</View>
				</View>
				{/* Second Line to show TrackDate and LrDate */}
				<View style={styles.CardLine}>
					<View style={styles.CardTextContainer}>
						<Text numberOfLines={1} style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>{TrackDate}</Text>
					</View>
					<View style={styles.CardTextContainer}>
						<Text numberOfLines={1} style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),{textAlign: 'right'}
              ]}>{CourierDate}</Text>
					</View>
				</View>
				<Text numberOfLines={1} style={[styles.CardSupplierText,
				ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),]}>{props.data.Agent}</Text>
				<Text numberOfLines={1} style={[styles.CardTransporterText,
				ApplyStyleFontAndSizeAndColor(
					globalFontObject.Font.Regular,
					globalFontObject.Size.small.sl,
					globalColorObject.Color.BlackColor,
					globalColorObject.ColorPropetyType.Color,
				 ),
				]}>{props.data.DispatchThrough}</Text>
			</View>
		</TouchableWithoutFeedback>
	);
}

export default class Courier extends Component {

	constructor() {
		super();
		this.state = {
			isFetched: false,
			isRefresh: false,
			CourierInOut: true,
			NoDataAvailable: false,
			Courier: [],
			SearchText: '',
			COList: [],
			show:false,
		};
		this.ActionButtons = [
			{
				text: 'New',
				icon: require('../../../assets/images/add-light.png'),
				name: 'AddNew',
				color: '#414d4c',
				buttonSize: 45,
				position: 1,
			},
		];
		// this.clearText= this.clearText.bind(this);
	}

	componentDidMount() {
		this.InitialSetup();
	}

	InitialSetup = async () => {
		
		this.IP = await AsyncStorage.getItem("IP");
		this.Token = await AsyncStorage.getItem("Token");

		//console.log(this.IP);
		//console.log(this.Token);
		this.LoadAllCourier();
	}

	LoadAllCourier = async () => {

		Request.get(`${this.IP}/api/Courier/Courier_InAutoNo`,this.Token)
			.then(res => {
				//console.log(this.IP);
				//console.log(this.Token);
				//console.log(res);
				if (res.status === 200) {
					this.setState({ isFetched: true, COList: res.data, Courier: res.data });
					if (res.data.length === 0) {
						AlertMessage("No Courier found");
						this.setState({ NoDataAvailable: true });
					} else this.setState({ NoDataAvailable: false });
				}
			})
			.catch(err => {
				console.error(err);
				AlertMessage("Something Went wrong\n" + JSON.stringify(err));
				this.setState({ isFetched: true });
			});
	}

	

	HandleInAndOutChange = async (CourierInOut) => {
		this.setState({ CourierInOut, isFetched: false });
		let uri = CourierInOut ? `${this.IP}/api/Courier/Courier_InAutoNo` : `${this.IP}/api/Courier/Courier_OutAutoNo`;
		Request.get(uri,this.Token)
			.then(res => {
				if (res.status === 200) {
					this.setState({ isFetched: true, Courier: res.data });
					if (res.data.length === 0) {
						AlertMessage("No Courier found");
						this.setState({ NoDataAvailable: true });
					} else this.setState({ NoDataAvailable: false });
				}
				else
				{
					this.setState({ isFetched: true, NoDataAvailable: true });
				}
			})
			.catch(err => {
				console.error(err);
				AlertMessage("Something Went wrong\n" + JSON.stringify(err));
				this.setState({ isFetched: true });
			});
	}

	HandleSearch = async SearchText => {
		this.setState({ SearchText });
		let COList = this.state.COList;
      let txt = SearchText.toLowerCase();
      let Courier = COList.filter(obj => {
			let ConsignmentId = (obj.ConsignmentId || "").toString().toLowerCase();
			let TrackDate=(obj.TrackDate || "").toString().toLowerCase();
			let CourierDate=(obj.CourierDate || "").toString().toLowerCase();
			let Agent=(obj.Agent || "").toString().toLowerCase();
			let DispatchThrough=(obj.DispatchThrough || "").toString().toLowerCase();

		
			return (ConsignmentId.includes(txt) ||TrackDate.includes(txt) || CourierDate.includes(txt)|| Agent.includes(txt)||DispatchThrough.includes(txt));
		});
		
		this.setState({ Courier });
	}


	NavigateToEditCourier = (CourierType, CourierId) => {
		console.log(CourierType, CourierId);
		this.props.navigation.navigate("AddCourierNavigator", {
			CourierType: CourierType,
			CourierId: CourierId,
		});
	}

	showCancel=()=>{
		this.setState({show:true})
  		};

  	hideCancel=()=>{
		this.setState({show:false})
 	  	 };

    renderTouchableHighlight(){
 	 	if(this.state.show){
 	 		return(
	 			<TouchableOpacity
	 			style={styles.closeButtonParent }
	 			onPress={this.clearInput}>
             <Image
              style={styles.closeButton}
               source={require('../../../assets/images/Clear_1.png')}
             />
          	</TouchableOpacity>
 	 		)
 	 	}
 	 	return null;
	 }
	
	clearInput = () => {
		this.HandleSearch('');
	}

	render() {
		if (!this.state.isFetched) {
			return (
				<>
					<ICubeAIndicator />
				</>
			)
		}
		//  renderTouchableHighlight()
		return (
		<>
		<View style={[styles.SearhBarWrapper,{backgroundColor: globalColorObject.Color.Lightprimary}
		]}>
		<TextInput
		style={[
		styles.SearchInput,
		ApplyStyleFontAndSizeAndColor(
		globalFontObject.Font.Regular,
		globalFontObject.Size.sxl,
		globalColorObject.Color.oppPrimary,
		globalColorObject.ColorPropetyType.BackgroundColor,
		),
		ApplyStyleColor(
		globalColorObject.Color.Primary,
		globalColorObject.ColorPropetyType.BorderColor,
		),
		]}
		style={styles.SearchInput}
		placeholder={'Search here'}
		value={this.state.SearchText}
		onChangeText={this.HandleSearch}
		returnKeyType='search'
		type="search"
		onFocus={this.showCancel}
		onBlur={(this.hideCancel)}
		/>
		<ModalSearchableLabel
              placeholder="Select Courier"
              fieldWrapperStyle={{height: 30, width: '30%'}}
              data={[
                {label: 'Courier-In', value: true},
                {label: 'Courier-Out', value: false},                
              ]}
				  labelProp="label"
              valueProp="value"				
				  selectedValue={this.state.CourierInOut}
              onValueSelected={this.HandleInAndOutChange}
              inputStyle={[
                {
                  paddingLeft: 5,
                  marginTop: 10,
                  marginBottom: 0,
                  borderBottomWidth: 0,
                  textAlign: 'center',
                  textDecorationLine: 'underline',
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderBottomColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
            />  
		{/* {this.renderTouchableHighlight()} */}

		</View>
		 <SafeAreaView style={[styles.container,{backgroundColor: globalColorObject.Color.Lightprimary}]}>
		 {/* <View style={styles.TypeContainer}>
			<Text numberOfLines={1}
			style={[styles.TypeText,			
			ApplyStyleFontAndSizeAndColor(
			globalFontObject.Font.Regular,
			globalFontObject.Size.small.sxl,
			globalColorObject.Color.BlackColor,   			             
			), 
			]}>{' '}
				{this.state.CourierInOut ? 'Courier In' : 'Courier Out'}</Text>
			<Switch
			style={styles.TypeSwitch}
			onValueChange={this.HandleInAndOutChange}
			value={this.state.CourierInOut}
			/>
		</View>  */}
		{this.state.NoDataAvailable &&<View style={styles.NoDataBanner}>
		<Text numberOfLines={1} style={{ fontSize: 25, color: 'rgba(0,0,0,0.2)', textAlign: "center",marginTop: 520 }}>No Data Found</Text>
		<Text numberOfLines={1} style={{ fontSize: 18, color: 'rgba(0,0,0,0.2)', textAlign: "center" }}>Pull to refresh</Text>
		</View>
		}
		<FlatList
		data={this.state.Courier}
		ListHeaderComponent={() => <ListItemHeader />}
		stickyHeaderIndices={[0]}
		renderItem={({ item }) => <ListItem data={item} type={this.state.CourierInOut} onLongPress={this.NavigateToEditCourier} />}
		keyExtractor={(item, index) => index.toString()}
		refreshing={this.state.isRefresh}
		onRefresh={this.LoadAllCourier}
		/>
		<TouchableOpacity style=
		{[styles.AddNewInvoice,
		ApplyStyleColor(
		globalColorObject.Color.Primary,
		globalColorObject.ColorPropetyType.BackgroundColor,
		),
		]} onPress={() => {
		this.props.navigation.navigate("AddCourierNavigator",{CourierType:this.state.CourierInOut,CourierId: 0});
		//console.log(CourierType);
		}}>
		<Image
		style={{ width: 30, height: 30 }}
		source={require('../../../assets/images/add-light.png')}
		/>
		</TouchableOpacity>

		</SafeAreaView> 
		</>
		)
	}
}

const styles = StyleSheet.create({
	LoaderContainer: {
		width: '70%',
		height: 100,
		backgroundColor: 'rgba(0,0,0,0.1)',
		borderRadius: 5,
		flexDirection: 'row',
	},
	LoaderBackdrop: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		// backgroundColor: 'rgba(0,0,0,0.5)',
	},
	Loader: {
		flex: 1,
		alignSelf: 'center',
	},
	LoaderText: {
		flex: 2,
		alignSelf: 'center',
		fontSize: 17
	},
	container: {
		flex: 1,
	},
	FlatList: {
		paddingVertical: 10
	},
	NoDataBanner: {
		width: '100%',
		position: 'absolute'
	},
	Card: {
		width: '95%',
		alignSelf: 'center',
		backgroundColor: globalColorObject.Color.oppPrimary,
		marginVertical: 3,
		borderRadius: 5,
		paddingVertical: 15,
		paddingHorizontal: 10,
		//elevation: 3,
	},
	CardLine: {
		width: '100%',
		// backgroundColor: 'salmon',
		position: 'relative',
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	CardTextContainer: {
		width: '50%',
		marginVertical: 3,
	},
	CardSupplierText: {
		marginVertical: 3,
	},
	CardTransporterText: {
		marginVertical: 3,
	},
	TypeContainer:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop:10,
		elevation: 3,
		height:40,
	},

	cardTextFieldHeader: {
      fontSize: 16,
		color: 'white',
		paddingLeft:10,
   },
	StickeyHeaderCard: {
		width: '95%',
		alignSelf: 'center',
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      backgroundColor: globalColorObject.Color.Primary,
      marginBottom: 5,
      borderRadius: 5,
      paddingVertical: 7,
		//elevation: 5,
	},
	
	TypeText:{
		fontSize: 22,
		color: '#222',
		fontWeight: 'bold',
		marginLeft:10,
	},
	TypeSwitch:{
		marginRight:10,
		transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }],
	},
	SearhBarWrapper: {
		flexDirection: 'row',
		// justifyContent: 'space-between',
      padding: 10,
   },
	SearchInput: {
		backgroundColor: 'white',
		paddingVertical: 5,
		paddingHorizontal: 7,
		borderRadius: 5,
		//fontSize: 15,
		width: '70%',
		borderWidth:1,
		marginLeft:4,
		borderColor:globalColorObject.Color.Primary,
	},
	closeButton: {
		height: 30,
		width: 30,
		// paddingLeft:30,
		marginLeft:10,
	 },
	 closeButtonParent: {
		position: 'absolute',
		right: 10,
		top:14,
		width:'38%'
	 },
	 AddNewInvoice: {
		width: 60,
		height: 60,
		position: 'absolute',
		bottom: 30,
		right: 30,
		elevation: 10,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 60,
		// alignSelf: 'flex-end',
		// marginRight: 10,
		// marginVertical: 8,
	},
	//  searchcontainer: {
	// 	flex: 1,
	// 	flexDirection: 'column',
	// 	justifyContent: 'center',
	//  },
});

// export default {Courier};