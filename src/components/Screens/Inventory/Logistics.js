// Module Imports
import React, {Component} from 'react';
import {
  TextInput,
  View,
  SafeAreaView,
  StatusBar,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Request,
  AlertMessage,
  GetSessionData,
} from '../../../Helpers/HelperMethods';
import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
} from '../../../Style/GlobalStyle';
const ListItemHeader = () => {
  // 	const stickySegmentControlX = this.state.scrollY.interpolate({
  // 		inputRange: [0, STICKY_SCROLL_DISTANCE],
  // 		outputRange: [INIT_STICKY_HEADER, HEADER_MIN_HEIGHT],
  // 		extrapolate: 'clamp'
  //   })

  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardTextContainer, {width: '50%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Invoice Id {/* {props.data.DOCNO || ''} */}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}>  </Text>  */}
      </View>
      <View style={[styles.CardTextContainer, {width: '49%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            {textAlign: 'right'},
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          No {/* {DocDate || ''} */}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}>{DocDate || ''} </Text> */}
      </View>
      <View style={[styles.CardTextContainer, {width: '50%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Check In date {/* {props.data.DOCNO || ''} */}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}>  </Text>  */}
      </View>
      <View style={[styles.CardTextContainer, {width: '49%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            {textAlign: 'right'},
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Check Out Date {/* {DocDate || ''} */}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}>{DocDate || ''} </Text> */}
      </View>
      <View style={[styles.CardTextContainer, {width: '100%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Customer Name {/* {props.data.AgainstNo || ''} */}{' '}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}>	 {props.data.AgainstNo || ''}  </Text> */}
      </View>
      <View style={[styles.CardTextContainer, {width: '100%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Courier Status {/* { AgainstDate || ''} */}{' '}
        </Text>
        {/* <Text numberOfLines={1} style={styles.BlockDetailData}> { AgainstDate || ''} </Text> */}
      </View>
    </View>
  );
};
const ListItem = props => {
  let FullTrackDate = new Date(props.data.TrackDate),
    FullLRdate = new Date(props.data.LRDate);
  let TrackDate = `${('0' + FullTrackDate.getDate()).slice(-2)}/${(
    '0' +
    (FullTrackDate.getMonth() + 1)
  ).slice(-2)}/${FullTrackDate.getFullYear()}`;
  let LRDate = `${('0' + FullLRdate.getDate()).slice(-2)}/${(
    '0' +
    (FullLRdate.getMonth() + 1)
  ).slice(-2)}/${FullLRdate.getFullYear()}`;

  return (
    <TouchableWithoutFeedback
      onLongPress={() =>
        props.onLongPress(props.data.TrackNo, props.data.TrackDate)
      }>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        {/* First Line to show TrackNo AND LRNo */}
        <View style={styles.CardLine}>
          <View style={(styles.CardTextContainer, {width: '49%'})}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {props.data.TrackNo}
            </Text>
          </View>

          <View style={(styles.CardTextContainer, {width: '50%'})}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {props.data.LRNo}
            </Text>
          </View>
        </View>
        {/* Second Line to show TrackDate and LrDate */}
        <View style={styles.CardLine}>
          <View style={(styles.CardTextContainer, {width: '49%'})}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {TrackDate}
            </Text>
          </View>

          <View style={(styles.CardTextContainer, {width: '50%'})}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {LRDate}
            </Text>
          </View>
        </View>
        <Text
          numberOfLines={1}
          style={[
            styles.CardSupplierText,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          {props.data.Supplier}
        </Text>
        <Text
          numberOfLines={1}
          style={[
            styles.CardTransporterText,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          {props.data.Transporter}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class Logistics extends Component {
  constructor() {
    super();
    this.LocalData = {
      Location: '1',
      UserCode: '',
    };
    this.state = {
      isFetched: false,
      // isRefresh: false,
      NoDataAvailable: false,
      Logistics: [],
      ListSourceData: [],
      SearchText: '',
      POList: [],
      show: false,
    };
  }

  componentDidMount() {
    this.InitialSetup();
  }

  InitialSetup = async () => {
    // this.IP = await AsyncStorage.getItem('IP');
    // this.Token = await AsyncStorage.getItem('Token');

    let {get_IP, get_Token, get_UserCode, get_Location, get_FinancialYearId} =
      await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.Location = get_Location;
    this.LoadAllLogistics();
  };

  LoadAllLogistics = async () => {
    Request.get(
      `${this.IP}/api/LRInward/LRInward_GetLRInList?LocationCode=${this.LocalData.UserCode}`,
      this.Token,
    )
      .then(res => {
        // console.log(res);
        if (res.status === 200) {
          this.setState({
            isFetched: true,
            POList: res.data,
            ListSourceData: res.data,
            Logistics: res.data,
          });
          if (res.data.length === 0) this.setState({NoDataAvailable: true});
          else this.setState({NoDataAvailable: false});
        }
      })
      .catch(err => {
        console.error(err);
        AlertMessage('Something Went wrong\n' + JSON.stringify(err));
        this.setState({isFetched: true});
      });
  };

  NavigateToEditLR = (TrackNo, TrackDate) => {
    // console.log(TrackNo, TrackDate);
    this.props.navigation.navigate('AddLogisticsInNav', {
      LRNo: TrackNo,
      LRDate: TrackDate,
    });
  };
  HandleSearch = async SearchText => {
    this.setState({SearchText});
    let POList = this.state.POList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = POList.filter(obj => {
      let TrackNo = (obj.TrackNo || '').toString().toLowerCase();
      let LRNo = (obj.LRNo || '').toString().toLowerCase();
      let Supplier = (obj.Supplier || '').toString().toLowerCase();
      let Transporter = (obj.Transporter || '').toString().toLowerCase();

      return (
        TrackNo.includes(txt) ||
        Transporter.includes(txt) ||
        Supplier.includes(txt) ||
        LRNo.includes(txt)
      );
    });

    this.setState({ListSourceData});
  };

  NavigateToAddLR = () => {
    this.props.navigation.navigate('AddLogisticsInNav');
  };
  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={styles.closeButtonParent}
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  clearInput = () => {
    this.HandleSearch('');
  };

  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    return (
      <>

        <View
          style={[
            styles.SearhBarWrapper,
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
          ]}>
          <TextInput
            style={styles.SearchInput}
            placeholder={'Search here'}
            value={this.state.SearchText}
            onChangeText={this.HandleSearch}
            autoCorrect={false}
            returnKeyType="search"
            autoCapitalize="none"
            onFocus={this.showCancel}
            onBlur={this.hideCancel}
            // onSubmitEditing={this.search}
          />
          {this.renderTouchableHighlight()}
        </View>
        <SafeAreaView style={styles.container}>
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={{
                  fontSize: 25,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 150,
                }}>
                No Data Found
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                }}>
                Pull to refresh
              </Text>
            </View>
          )}
          <FlatList
            style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            stickyHeaderIndices={[0]}
            data={this.state.ListSourceData}
            ListHeaderComponent={() => <ListItemHeader />}
            renderItem={({item}) => (
              <ListItem data={item} onLongPress={this.NavigateToEditLR} />
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={false}
            onRefresh={this.LoadAllLogistics}
          />
          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={this.NavigateToAddLR}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>

          {/* <SwipeListView
						data={this.state.Logistics}
						// renderItem={(item) => <ListItem data={item} />}
						renderItem={(item) => {
							<TouchableHighlight
								onPress={() => console.log('You touched me')}
								// style={styles.rowFront}
								underlayColor={'#AAA'}
							>
								<View>
									<Text>I am {item.LRNo} in a SwipeListView</Text>
								</View>
							</TouchableHighlight>
						}}
						renderHiddenItem={(item) => {
							<View>
								<Text>Edit</Text>
								<Text>Delete</Text>
							</View>
						}}
					/> */}
          {/* <FloatingAction
						actions={this.ActionButtons}
						onPressItem={this.HandleActionButtonClick}
						// color="rgba(50,74,84,1)" iconWidth={20} iconHeight={20}
						color="#47b5ad" iconWidth={20} iconHeight={20}
					/> */}

          {/* <TouchableOpacity
						onPress={this.NavigateToAddLR}
						style={styles.AddButton}
					>
						<Image
							style={{ width: 30, height: 30 }}
							source={require('../../assets/images/plus-light.png')}
						/>
					</TouchableOpacity> */}
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Loadercontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  FlatList: {
    paddingVertical: 10,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
  },
  Card: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: globalColorObject.Color.Lightprimary,
    marginVertical: 2,
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    //elevation: 3,
  },
  CardLine: {
    // paddingHorizontal: 2,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '50%',
    marginVertical: 3,
  },
  CardSupplierText: {
    //fontSize: 16.5,
    marginVertical: 3,
    color: '#222',
    fontWeight: 'bold',
  },
  CardTransporterText: {
    marginVertical: 3,
    color: '#222',
  },
  AddButton: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    borderRadius: 50,
    backgroundColor: globalColorObject.Color.Lightprimary,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
  },

  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: globalColorObject.Color.Primary,
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },

  SearhBarWrapper: {
    padding: 10,
    backgroundColor: '#ededed',
  },
  SearchInput: {
    backgroundColor: globalColorObject.Color.oppPrimary,
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    fontSize: 15,
    borderWidth: 1,
    borderColor: globalColorObject.Color.Primary,
  },
  cardTextFieldHeader: {
    fontSize: 16,
    color: 'white',
    paddingLeft: 10,
  },
  closeButton: {
    height: 30,
    width: 30,
    // paddingLeft:30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 10,
    top: 14,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    backgroundColor: globalColorObject.Color.Primary,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
});
