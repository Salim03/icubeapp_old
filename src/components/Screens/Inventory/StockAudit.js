import React, { useEffect, useState } from 'react';
import {
   View, Text, StyleSheet, Image, TextInput, FlatList
} from 'react-native';
import {
   ApplyStyleColor,
   globalColorObject,
   globalFontObject,
   ApplyStyleFontAndSizeAndColor,
   ApplyStyleFontAndSize,
 } from '../../../Style/GlobalStyle';
import Ripple from 'react-native-material-ripple';

import IActivityIndicator from '../../Tools/ICubeIndicator';
import { Request, getIPAndTokenFromAsyncStorage, AlertError, HandleResponseStatus } from '../../../Helpers/HelperMethods';
import LayoutWrapper, { CardStyle } from '../../Layout/Layout';

const API = {
   IP: "",
   Token: "",
   callfrom: '',
   loccode: 1,
   get GetAllInvoice() { return `${this.IP}/api/Inventory/StockEntry/GetAllInvoice?callFrom=${this.callfrom}&loccode=${this.loccode}` }
};

const initialState = {
   NoDataAvailable: false,
   isFetching: true,
   Invoices: [],
};

let InvoicesArray = [];

// Invoice List Header Component
const ListItemHeader = (props) => {
	return(
		<View style={[styles.StickeyHeaderCard, {backgroundColor:globalColorObject.Color.Lightprimary },
      ApplyStyleColor(
        globalColorObject.Color.Primary,
        globalColorObject.ColorPropetyType.BackgroundColor,
      ),
    ]}>
       <View style={[styles.CardTextContainer, {paddingLeft:'1%', width: '39%' }]}>
         <Text numberOfLines={1}
          style={[
             ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
              ]}> Name</Text>
      </View>
      <View style={[styles.CardTextContainer, { width: '34%' }]}>
         <Text numberOfLines={1} style={[
            ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),]}>AutoNo</Text> 
      </View>
      <View style={[styles.CardTextContainer, { width: '11%' }]}>
         <Text numberOfLines={1} style={[
            ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),]}> Date</Text>
      </View>
 </View>
	);
}
// Invoice List Item Component
const ListItem = ({ data, onLongPress }) => {

   const { CardItemLayout } = CardStyle;
   const hanldeLongPress = () => onLongPress(data);

   return (
      <Ripple onLongPress={hanldeLongPress} style={[
         styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
      ]}>
         <View style={styles.CardLine}>
            <View style={[styles.CardTextContainer, { width: '38%' }]}>
               <Text numberOfLines={1} style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>{data.Name}
              </Text>
            </View>
            <View style={[styles.CardTextContainer, CardItemLayout, { width: '36%' }]}>
               <Text numberOfLines={1} style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>{data.AutoNo}</Text>
            </View>
            <View style={[styles.CardTextContainer, CardItemLayout, { width: '24%' }]}>
               <Text numberOfLines={1} style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>{data.Date}</Text>
            </View>
         </View>
      </Ripple>
   );
};

const StockAudit = (props) => {

   // states
   const [state, setState] = useState(initialState);
   const [searchText, setSearchText] = useState("");
   const [show, setshowCancel] = useState(false);
   // Effects
   useEffect(() => {
      fetchData();
   }, []);

   // Functions
   const fetchData = async () => {
      const { IP, Token } = await getIPAndTokenFromAsyncStorage();
      API.IP = IP, API.Token = Token;
      API.callfrom = props.route.params.callFrom;
      console.log('my fffff head' ,props.route.params.callFrom);
      loadAllInvoice();
   }
   const loadAllInvoice = () => {
      Request.get(API.GetAllInvoice, API.Token)
         .then(HandleResponseStatus)
         .then(data => {
            InvoicesArray = data;
            setState({ ...state, isFetching: false, Invoices: data, NoDataAvailable: data?.length === 0 });
         })
         .catch(AlertError).catch(() => setState({ ...state, isFetching: false }));
   }
   const navigateToAdd = () => navigateToEntryPage()
   const navigateToEntryPage = item => {
      props.navigation.navigate("AddStockAuditNav", { 
         callFrom: API.callfrom, InvoiceId: item?.Id, AutoNo: item?.AutoNo, InvoiceDate: item?.Date, Remarks: item?.Remarks, onUpdate: loadAllInvoice
      });
   }
   const refreshLayout = () => { setState({ ...state }); }
   const handleSearch = text => {
      setSearchText(text);
      const filterText = text.toLowerCase();
      const Invoices = InvoicesArray.filter(itm => {
         const AutoNo = itm["AutoNo"]?.toLowerCase(), Date = itm["Date"]?.toLowerCase(), Name = itm["Name"]?.toLowerCase();
         return (AutoNo.includes(filterText) || Date.includes(filterText) || Name.includes(filterText));
      });
      setState({ ...state, Invoices });
   }
   const renderTouchableHighlight=()=> {
      if (show==true) {
        return (
          <TouchableOpacity
            style={styles.closeButtonParent}
            onPress={clearInput}>
            <Image
              style={styles.closeButton}
              source={require('../../../assets/images/Clear_1.png')}
            />
          </TouchableOpacity>
        );
      }
      return null;
    }
    const clearInput = () => {
      handleSearch('');
    };
    const showCancel = ()=>{
      setshowCancel(true)
    }
    const hideCancel = ()=>{
      setshowCancel(false)
    }
   if (state.isFetching)
      return <IActivityIndicator />;
   else
      return (
         <LayoutWrapper 
         onLayoutChanged={refreshLayout}
         backgroundColor={globalColorObject.Color.Lightprimary}>         
            <View style={styles.SearhBarWrapper} >
               <TextInput
                  style={[styles.SearchInput,
                     ApplyStyleFontAndSizeAndColor(
                       globalFontObject.Font.Regular,
                       globalFontObject.Size.small.sl,
                       globalColorObject.Color.oppPrimary,
                       globalColorObject.ColorPropetyType.BackgroundColor,
                     ),
                     ApplyStyleColor(
                       globalColorObject.Color.Primary,
                       globalColorObject.ColorPropetyType.BorderColor,
                     ),]}
                  placeholder={'Search here'}
                  value={searchText}
                  onChangeText={handleSearch}
                  autoCorrect={false}
                  returnKeyType='search'
                  autoCapitalize='none'
                  onFocus={showCancel}
                  onBlur={hideCancel}
               />
                {renderTouchableHighlight}

            </View>
            {state.NoDataAvailable &&
               <View style={styles.NoDataBanner}>
                  <Text style={{ fontSize: 25, color: 'rgba(0,0,0,0.2)', textAlign: "center", marginTop: 520 }}>No Data Found</Text>
                  <Text style={{ fontSize: 18, color: 'rgba(0,0,0,0.2)', textAlign: "center" }}>Pull to refresh</Text>
               </View>
            }
            <FlatList
               data={state.Invoices}
               renderItem={({ item }) => <ListItem 
                 data={item} 
                 onLongPress={navigateToEntryPage} />}
               keyExtractor={item => item.Id.toString()}
               refreshing={false}
               onRefresh={loadAllInvoice}
               ListHeaderComponent={<ListItemHeader  />}
               stickyHeaderIndices={[0]}
               keyboardShouldPersistTaps='always'
               // to have some breathing space on bottom
               ListFooterComponent={() => <View style={{ width: '100%', marginTop: 85 }}></View>}
            />
            <Ripple style=
            {[styles.AddNewInvoice,
               ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
               ),
            ]} 
            onPress={navigateToAdd} 
            rippleContainerBorderRadius={60}>
               <Image
                  style={{ width: 30, height: 30 }}
                  source={require('../../../assets/images/add-light.png')}
               />
            </Ripple>
         </LayoutWrapper>
      )
}

const styles = StyleSheet.create({
   closeButton: {
		height: 30,
		width: 30,
		marginLeft: 10,
	 },
	 closeButtonParent: {
		position: 'absolute',
		right: 12,
		top: 14,
	 },
   SearhBarWrapper: {
      padding: 10,
      //backgroundColor: '#ededed',
   },
   SearchInput: {
      //backgroundColor: 'white',
      backgroundColor:globalColorObject.Color.oppPrimary,
      paddingVertical: 5,
      paddingHorizontal: 7,
      borderRadius: 5,
      borderWidth:1,
      borderBottomColor:globalColorObject.Color.Primary
   },
   NoDataBanner: {
      width: '100%',
      position: 'absolute',
      top: 70,
   },
   AddNewInvoice: {

      width: 60,
      height: 60,
      position: 'absolute',
      bottom: 30,
      right: 30,
      elevation: 10,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 60,
   },
   // FlatList Component style
   StickeyHeaderCard: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      backgroundColor: globalColorObject.Color.Primary,
      marginHorizontal: 9,
      marginBottom: 5,
      borderRadius: 5,
      paddingVertical: 7,
      elevation: 5,
   },
   cardTextFieldHeader: {
      fontSize: 16,
      color: 'white'
   },
   Card: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      backgroundColor: 'white',
      borderRadius: 5,
      paddingTop: 10,
      paddingBottom: 10,
      marginTop: 5,
      marginHorizontal: 9,      
   },
   CardLine: {
      paddingHorizontal: 10,
      flexDirection: 'row',
      flex: 1,
      flexWrap: 'wrap'
   },
   CardTextContainer: {
      // width: '30%',
      marginVertical: 3,
   },
   BoldText: {
      fontWeight: 'bold'
   },
   cardTextField:{
      fontWeight: 'bold'
   },
})

export default StockAudit;