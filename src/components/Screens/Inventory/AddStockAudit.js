/* eslint-disable no-bitwise */
/* eslint-disable react-native/no-inline-styles */
import React, {useLayoutEffect, useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Modal,
  PermissionsAndroid,
  Platform,
  TextInput,
  FlatList,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import Menu, {MenuItem} from 'react-native-material-menu';
import {CameraScreen} from 'react-native-camera-kit';
import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../../Style/GlobalStyle';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import {
  getIPAndTokenFromAsyncStorage,
  Request,
  HandleResponseStatus,
  AlertError,
  AlertMessage,
  Confirmation,
  getItemsFromAsyncStorage,
} from '../../../Helpers/HelperMethods';
import ModalSearchList from '../../Custom/ModalSerachList';

// Initial State
const InitialState = {
  fetching: true,
  planList: [],
  selectedPlan: '',
  barcodes: [],
  setCodes: [],
  barcodesData: [],
};

// API variable to hold the url and params
const API = {
  IP: '',
  Token: '',
  callFrom: '',
  SelectedInvoiceNo: 0,
  loccode: 1,
  planCode: '',
  planId: '',
  get PlanList() {
    return `${this.IP}/api/Inventory/StockEntry/GetAllFilterListData?callFrom=${this.callFrom}&invoiceNo=${this.SelectedInvoiceNo}&loccode=${this.loccode}`;
  },
  GetItemForBarcode: function (barcode, seqNo, isSetCode) {
    return `${this.IP}/api/Inventory/StockEntry/GetItemBindDataForSearchItemInfo?callFrom=${this.callFrom}&PassStockCode=${this.planCode}&PassSearchItemInfo=${barcode}&seqNo=${seqNo}&IsScanSetcode=${isSetCode}&StockPoint=${this.planId}&loccode=${this.loccode}`;
  },
  GetSelectedInvoiceDetails(id) {
    return `${this.IP}/api/Inventory/StockEntry/GetSelectedInvoiceDetails?callFrom=${this.callFrom}&InvoiceNo=${id}&loccode=${this.loccode}`;
  },
  get SaveStockEntry() {
    return `${this.IP}/api/Inventory/StockEntry/SaveStockEntry`;
  },
};

// Camera Permission Variable
let CameraPermission = false;

// Selected Invoice Id value
let SelectedInvoiceId = 0;
let SelectedInvoiceAutoNo = '';
let SelectedInvoiceDate = '';
let SelectedInvoiceRemarks = '';

// Barcode Scanner component
const Scanner = ({onCancel, onReadCode}) => {
  return (
    <View style={styles.modalContainer}>
      <CameraScreen
        actions={{leftButtonText: 'Cancel'}}
        onBottomButtonPressed={onCancel}
        showFrame={true}
        scanBarcode={true}
        laserColor={'white'}
        frameColor={'lightgreen'}
        // colorForScannerFrame={'black'}
        onReadCode={event => onReadCode(event.nativeEvent.codeStringValue)}
      />
    </View>
  );
};

// Modal PlanList Render Item Component
const PlanListItem = ({data, onSelectItem}) => (
  <TouchableOpacity
    style={styles.planListItem}
    onPress={() => onSelectItem(data)}>
    <Text>{data.Name}</Text>
  </TouchableOpacity>
);

// Barcodes List Item Header Component

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        <View style={[styles.CardLine1]}>
          <View style={[styles.CardTextContainer, CardItemLayout, {width: 90}]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {marginRight: 5},
              ]}>
              Item Name
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              Barcode
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              WSP
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right', marginRight: 5},
              ]}>
              RSP
            </Text>
          </View>
        </View>
        <View style={[styles.CardLine2]}>
          <View style={[styles.CardTextContainer, CardItemLayout, {width: 90}]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              Stock Qty
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}></View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              Qty
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right', marginRight: 5},
              ]}>
              Amount
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

// Barcodes List Item Component
// wrapped in forward Ref to make use of the remove Icon Ref from the main Component
const ListItem = props => {
  let data = props.data;
  const index = props.index;
  const Quantity = (data.Quantity || 0) | null;
  const MRP = (data.MRP || 0) | null;
  const Amount = Quantity * MRP;
  const {CardItemLayout} = CardStyle;

  const HandleQtyChange = value => props.HandleQtyChange(value, index);
  const removeItem = () => props.onRemoveItem(index);

  return (
    <View
      style={[
        styles.Card,
        ApplyStyleColor(
          globalColorObject.Color.oppPrimary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <TouchableOpacity
        style={styles.removeItemIconContainer}
        onPress={removeItem}>
        <Image
          source={require('../../../assets/images/minus-salmon.png')}
          style={styles.removeItemIcon}
        />
      </TouchableOpacity>
      <View style={[styles.CardLine]}>
        <View style={[styles.CardLine1]}>
          <View style={[styles.CardTextContainer, CardItemLayout, {width: 90}]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Description}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Icode || data.Barcode || ''}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.WSP || 0}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right', marginRight: 5},
              ]}>
              {MRP}
            </Text>
          </View>
        </View>
        <View style={[styles.CardLine2]}>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.StockQuantity || 0}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}></View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <TextInput
              style={styles.CardQtyTextInput}
              value={Quantity.toString()}
              onChangeText={HandleQtyChange}
            />
          </View>
          <View style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextField,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {Amount}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

// Duplicate Barcodes List Items Component
const DuplicateBarcodeListItem = ({data, onSelectItem}) => {
  return (
    <TouchableOpacity
      onPress={() => onSelectItem(data)}
      style={{
        //borderWidth: 0.5,
        //borderColor: 'red',
        // borderRadius: 10,
        marginTop: 5,
        backgroundColor: globalColorObject.Color.oppPrimary,
      }}>
      <View style={{flexDirection: 'row', marginLeft: 15}}>
        <View style={{width: '30%'}}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Barcode
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {data.Barcode}
          </Text>
        </View>
        <View>
          <Text />
        </View>
        <View style={{width: '50%'}}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Description
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {data.Description}
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginLeft: 15,
          borderTopColor: '#052c49',
          justifyContent: 'space-around',
        }}>
        <View style={{alignItems: 'center'}}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Stock Qty
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {data.StockQuantity}
          </Text>
        </View>
        <View>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Qty
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {'   '}
            {data.Quantity}
          </Text>
        </View>
        <View>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            RSP
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {'  '}
            {data.MRP || 0}
          </Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Amount
          </Text>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {data.Rate}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

// Duplicate Barcodes Modal Component
const DuplicateBarcodesSelection = ({
  visible,
  onCloseModal,
  data,
  onSelectItem,
}) => {
  return (
    <Modal
      animationType="slide"
      visible={visible}
      onRequestClose={onCloseModal}>
      <ScrollView backgroundColor={globalColorObject.Color.Lightprimary}>
        <View style={{marginTop: 22}}>
          <FlatList
            style={styles.FlatList}
            data={data}
            renderItem={({item}) => (
              <DuplicateBarcodeListItem
                data={item}
                onSelectItem={barcodeData => {
                  onCloseModal();
                  onSelectItem(barcodeData);
                }}
              />
            )}
            keyExtractor={item => item.Barcode.toString()}
          />
        </View>
      </ScrollView>
    </Modal>
  );
};

// Main component of the screen
const AddStockAudit = props => {
  
  const materialMenuRef = useRef();
  const ScanBarcodeRef = useRef(null);

  // states
  const [scanText, setScanText] = useState('');
  const [state, setState] = useState(InitialState);
  const [showScanner, setShowScanner] = useState(false);
  const [showPlanList, setShowPlanList] = useState(false);
  const [isSetCode, setIsSetCode] = useState(false);
  const [showDuplicateBarcodeModal, setShowDuplicateBarcodeModal] =
    useState(false);
    const [DuplicateBarcodeModalData, setDuplicateBarcodeModalData] =
      useState([]);
  const [saveTrigger, setSaveTrigger] = useState(0);
  const [savingInvoice, setSavingInvoice] = useState(false);

  // Effects
  // setting params to navigation header to trigger PlanNameList Popup and Save Entry
  useLayoutEffect(() => {
    API.callFrom = props.route.params['callFrom'];
    SelectedInvoiceId = props.route.params['InvoiceId'] || 0;
    SelectedInvoiceAutoNo = props.route.params['AutoNo'] || '';
    SelectedInvoiceDate = props.route.params['InvoiceDate'] || '';
    SelectedInvoiceRemarks = props.route.params['Remarks'] || '';
    // props.navigation.setParams({
    //   showPlanList: togglePlanList,
    //   saveData: toggleSave,
    // });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  useLayoutEffect(() => {
    saveTrigger && saveData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [saveTrigger]);
  // Fetching the PlanList and also checking the Camera Permission
  useEffect(() => {
    FetchInitialData();
    checkCameraPermission();

    props.navigation.setOptions({
      headerRight: () => (
        <View style={styles.headerContainer}>
          <View style={styles.headerIconsContainer}>
            <TouchableOpacity onPress={togglePlanList}>
              <Image
                source={require('../../../assets/images/popup-blue1.png')}
                style={[
                  styles.headerIcon,
                  // { Color:'white'}
                ]}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={toggleSave}>
              <Image
                source={require('../../../assets/images/save-light.png')}
                style={[styles.headerIcon]}
              />
            </TouchableOpacity>
          </View>
        </View>
      ),
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // Functions
  const FetchInitialData = async () => {
    const {IP, Token} = await getIPAndTokenFromAsyncStorage();
    API.IP = IP;
    API.Token = Token;
    Request.get(API.PlanList, Token)
      .then(HandleResponseStatus)
      .then(data => {
        SelectedInvoiceId && SelectedInvoiceId > 0
          ? FetchSelectedInvoiceDetails(data)
          : setState({...state, fetching: false, planList: data});
      })
      .catch(AlertError);
  };
  const FetchSelectedInvoiceDetails = PlanData => {
    Request.get(API.GetSelectedInvoiceDetails(SelectedInvoiceId), API.Token)
      .then(HandleResponseStatus)
      .then(data => {
        if (data && data.length) {
          const barcodes = data.map(itm => itm.Barcode || itm.Icode);
          API.planId = data[0].AgainstId;
          API.planCode = PlanData.find(itm => itm.Id === API.planId)?.['Code'];
          setState({
            ...state,
            fetching: false,
            planList: PlanData,
            selectedPlan: API.planId,
            barcodes,
            barcodesData: data,
          });
          // console.log("After updates", state);
        }
      })
      .catch(AlertError);
  };
  const checkCameraPermission = () => {
    PermissionsAndroid.check('android.permission.CAMERA').then(per => {
      CameraPermission = per;
    });
  };
  // const middleware = () => { console.log(saveTrigger); setSaveTrigger(saveTrigger + 1); }
  const toggleSave = num => {
    setSaveTrigger(num);
  };
  const saveData = async () => {
    // console.log("While Saving", state);
    if (!state.selectedPlan) {
      AlertMessage('Select Plan');
    } else if (!state.barcodesData.length) {
      AlertMessage('Scan atleast one item');
    } else {
      setSavingInvoice(true);
      const {UserId} = await getItemsFromAsyncStorage(['UserId']);
      const saveObj = {
        Type: API.callFrom,
        InvoiceId: SelectedInvoiceId,
        InvoiceNo: SelectedInvoiceAutoNo,
        Date: SelectedInvoiceDate || new Date().toISOString().split('T')[0],
        AgainstId: state.selectedPlan,
        Stkptcode: API.planCode,
        Remarks: SelectedInvoiceRemarks || '',
        UserId: UserId,
        Items: [...state.barcodesData],
      };

      Request.post(API.SaveStockEntry, saveObj, API.Token)

        .then(res => res.json())
        .then(data => {
          console.log('Saved response', data);
          if (data && data.Result) {
            AlertMessage(data.InvoceNo + ' saved successfully', () => {
              SelectedInvoiceId
                ? (props.navigation.goBack(), props.route.params?.onUpdate?.())
                : clearData();
            });
          } else {
            AlertMessage('Failed to save');
          }
        })
        .catch(AlertError)
        .finally(() => {
          setSavingInvoice(false);
        });
    }
  };
  const clearData = () => {
    console.log();
    API.planId = '';
    API.planCode = '';
    setState({
      ...state,
      selectedPlan: '',
      barcodes: [],
      barcodesData: [],
      setCodes: [],
    });
    setScanText('');
    setIsSetCode(false);
  };
  const openMenu = () => {
    materialMenuRef.current.show();
  };
  const hideMenu = () => materialMenuRef.current.hide();
  const toggleSetCode = () => {
    hideMenu();
    setIsSetCode(!isSetCode);
  };
  const handleLayoutChange = () => {
    setState({...state});
  };
  const handleBarcodeChange = txt => setScanText(txt);
  const handleScanBarcodeSubmit = () => {
    scanText && fetchBarcodeData(scanText);
  };
  const handlePlanNameSelect = (planId, plan) => {
    let {barcodes, setCodes, barcodesData} = {...state};
    API.planId !== planId &&
      ((barcodes = []), (setCodes = []), (barcodesData = []));
    API.planId = planId;
    API.planCode = plan.Code;
    setState({
      ...state,
      selectedPlan: planId,
      barcodes,
      setCodes,
      barcodesData,
    });
    togglePlanList();
  };
  const handleItemQtyChange = (val, index) => {
    const data = [...state.barcodesData];
    data[index].Quantity = val;
    setState({...state, barcodesData: data});
  };
  const requestCameraPermission = () => {
    if (Platform.OS === 'android') {
      const reqPermission = async () => {
        const permissionResult = await PermissionsAndroid.request(
          'android.permission.CAMERA',
        );
        return permissionResult === PermissionsAndroid.RESULTS.GRANTED;
      };
      reqPermission().then(res => {
        res && ((CameraPermission = res), toggleScanner());
      });
    } else {
      toggleScanner();
    }
  };
  const openScanner = () => {
    CameraPermission ? toggleScanner() : requestCameraPermission();
  };
  const toggleScanner = () => {
    setShowScanner(!showScanner);
  };
  const togglePlanList = () => {
    setShowPlanList(!showPlanList);
  };
  const toggleDuplicateBarcodes = () => {
    setShowDuplicateBarcodeModal(!showDuplicateBarcodeModal);
  };
  const checkBarcodeExists = barcode => {
    return isSetCode
      ? state.setCodes.includes(barcode.toUpperCase())
      : state.barcodes.includes(barcode.toUpperCase());
  };
  const AddQtyOnSetCode = setCode => {
    const updtaedBarcodesData = state.barcodesData.map(itm => {
      if (itm.SetCode === setCode) {
        itm.Quantity += 1;
      }
      return itm;
    });
    setState({...state, barcodesData: updtaedBarcodesData});
  };
  const AddQtyOnBarcode = async barcode => {
    barcode = barcode.toUpperCase();
    // calling AddQtyOnBarcode function if it is setCode and return from the function
    if (isSetCode) {
      AddQtyOnSetCode(barcode);
    } else {
      // if it is not setCode then adding Qty on the barcode
      const Index = state.barcodesData.findIndex(
        itm =>
          itm.Icode?.toUpperCase() === barcode ||
          itm.Barcode?.toUpperCase() === barcode,
      );
      // console.log("Adding Qty on Index", Index, barcode);
      if (Index + 1) {
        const data = [...state.barcodesData];
        data[Index].Quantity += 1;
        setState({...state, barcodesData: data});
        ScanBarcodeRef.current.focus();
        setScanText('');
        console.log('qty')
      }
    }
  };
  const fetchBarcodeData = barcode => {
    if (state.selectedPlan) {
      // if (checkBarcodeExists(barcode)) {
      //   AddQtyOnBarcode(barcode);
      // } else {
        // console.log(API.GetItemForBarcode(barcode, 0, isSetCode));
        Request.get(API.GetItemForBarcode(barcode, 0, isSetCode), API.Token)
          .then(HandleResponseStatus)
          .then(data => {
            validateBarcodeData(data)
          })
          .catch(AlertError);
      // }
    } else {
      AlertMessage('Select a Plan');
    }
  };
  const handleDuplicateBarcodes = data => {
    setDuplicateBarcodeModalData(data);
    setShowDuplicateBarcodeModal(true);
  };
  const handleSingleBarcodeBind = data => {
    const currentBarcode = (data.Barcode || data.Icode).toUpperCase();
    if (checkBarcodeExists(data.Icode)) {
      AddQtyOnBarcode(data.Icode);
    } else {
      setState({
        ...state,
        barcodes: [currentBarcode, ...state.barcodes],
        barcodesData: [data, ...state.barcodesData],
      });
    }
    ScanBarcodeRef.current.focus();
  };
  const handleSetCodeData = data => {
    const currentSetCode = data[0].SetCode;
    const currentBarcodes = data.map(itm => itm.Barcode || itm.Icode);
    setState({
      ...state,
      setCodes: [currentSetCode, ...state.setCodes],
      barcodes: [...currentBarcodes, ...state.barcodes],
      barcodesData: [...data, ...state.barcodesData],
    });
    ScanBarcodeRef.current.focus();
  };
  const handleBarcodeSelectFromDuplicate = data => {
    const currentBarcode = data.Barcode || data.Icode;
    checkBarcodeExists(currentBarcode)
      ? AddQtyOnBarcode(currentBarcode)
      : handleSingleBarcodeBind(data);
  };
  const validateBarcodeData = data => {
    console.log('Barcode Data', data);
    if (data.length) {
      setScanText('');
      // checking condition whether setCode option selected or not , if setCode option selected then calling handleSetCodeData function
      // if it is in default, then checking whether there is only one data for the barcodes or if there is duplicate barcode
      // if there is any duplicate barcodes then calling handleDuplicateBarcodes function otherwise callling handleSingleBarcodeBind function
      isSetCode
        ? handleSetCodeData(data)
        : data.length > 1
        ? handleDuplicateBarcodes(data)
        : handleSingleBarcodeBind(data[0]);
    } else {
      AlertMessage('Invalid Barcode');
    }
    setShowScanner(false);
  };
  const removeBarcodeItem = index => {
    const {barcodes, barcodesData} = {...state};
    barcodes.splice(index, 1);
    barcodesData.splice(index, 1);
    setState({...state, barcodes, barcodesData});
  };
  const confirmRemove = index =>
    Confirmation('Are you sure to remove ?', () => removeBarcodeItem(index));

  if (state.fetching) {
    return <ICubeAIndicator />;
  } else {
    // const { CardItemLayout } = CardStyle;
    return (
      <LayoutWrapper
        onLayoutChanged={handleLayoutChange}
        backgroundColor={globalColorObject.Color.Lightprimary}>
        <View style={styles.scanComponentContainer}>
          <Ripple
            onPress={openMenu}
            style={styles.dotMenuIconContainer}
            rippleCentered={true}
            rippleContainerBorderRadius={50}>
            <Image source={require('../../../assets/images/dot-menu-grey.png')} style={styles.dotMenuIcon} />
            <Menu
              // ref={ref => {
              //   materialMenuRef.current = ref;
              // }}
              ref={materialMenuRef}
              button={<></>}>
              <MenuItem onPress={toggleSetCode} disabled={!isSetCode}>
                Lot No
              </MenuItem>
              <MenuItem onPress={toggleSetCode} disabled={isSetCode}>
                Set Code
              </MenuItem>
            </Menu>
          </Ripple>
          <TextInput
            style={[
              layoutStyle.FieldInput,
              styles.SearchInput,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderColor,
              ),
            ]}
            ref={ScanBarcodeRef}
            onChangeText={handleBarcodeChange}
            value={scanText}
            returnKeyType="next"
            autoCorrect={false}
            autoCapitalize="none"
            onSubmitEditing={handleScanBarcodeSubmit}
            placeholder="Scan Here"
            placeholderTextColor="#AFAFAF"
          />
          <TouchableOpacity onPress={openScanner}>
            <Image source={require('../../../assets/images/ScanBarcode.png')} style={styles.ScanInput} />
          </TouchableOpacity>
        </View>
        <View style={styles.BarcodeListContainer}>
          <FlatList
            data={state.barcodesData}
            contentContainerStyle={styles.flatListContents}
            renderItem={({item, index}) => (
              <ListItem
                data={item}
                onRemoveItem={confirmRemove}
                index={index}
                HandleQtyChange={handleItemQtyChange}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={<ListItemHeader />}
            ListFooterComponent={
              <View style={{width: '100%', marginTop: 15}} />
            }
            stickyHeaderIndices={[0]}
          />
        </View>

        <View style={styles.StickeyFooterCard}>
          <View style={styles.CardLine2}>
            {/* <View
            style={{
              paddingHorizontal: 5,
              flexDirection: 'row',
              flex: 1,
              width: '100%',
              flexWrap: 'nowrap',
              justifyContent: 'center',
            }}> */}
            <View style={{marginVertical: 2}}>
              <Text numberOfLines={1} style={styles.cardTextFieldFooter}>
                {'    '}Stock Qty :{' '}
                {Object.values(state.barcodesData).reduce(
                  (r, {StockQuantity}) => r + StockQuantity,
                  0,
                )}
              </Text>
            </View>
            <View style={{marginVertical: 2}}>
              <Text numberOfLines={1} style={styles.cardTextFieldFooter}></Text>
            </View>
            <View style={{marginVertical: 2}}>
              <Text numberOfLines={1} style={styles.cardTextFieldFooter}>
                Qty :{' '}
                {Object.values(state.barcodesData).reduce(
                  (r, {Quantity}) => r + Quantity,
                  0,
                )}
              </Text>
            </View>
            <View style={{width: 120, marginVertical: 2}}>
              <Text numberOfLines={1} style={styles.cardTextFieldFooter}>
                {'         '}Amount :{' '}
                {Object.values(state.barcodesData).reduce(
                  (r, {Quantity, MRP}) => r + Quantity * MRP,
                  0,
                )}
              </Text>
            </View>
          </View>
        </View>
        {/* </View> */}
        {/* Modal components */}
        {/* Scanner Modal component */}
        <Modal animationType="slide" visible={showScanner}>
          <Scanner onCancel={toggleScanner} onReadCode={fetchBarcodeData} />
        </Modal>
        {/* Plan Selection Modal component */}
        <ModalSearchList
          dataArray={state.planList}
          value={state.selectedPlan}
          valueProp="Id"
          labelProp="Name"
          showModal={showPlanList}
          RenderItemComp={PlanListItem}
          onCancel={togglePlanList}
          onSelectItem={handlePlanNameSelect}
        />
        {/* Duplicate Modal component */}
        <DuplicateBarcodesSelection
          visible={showDuplicateBarcodeModal}
          onCloseModal={toggleDuplicateBarcodes}
          data={DuplicateBarcodeModalData}
          onSelectItem={handleBarcodeSelectFromDuplicate}
        />
        {/* Saving Invoice Modal */}
        <Modal transparent={true} animationType="fade" visible={savingInvoice}>
          <View style={styles.ModalLoaderBackDrop}>
            <View style={styles.ModalLoaderContainer}>
              <ActivityIndicator size="large" style={styles.ModalLoader} />
              <Text style={styles.ModalLoaderText}>Saving Invoice</Text>
            </View>
          </View>
        </Modal>
      </LayoutWrapper>
    );
  }
};

const styles = StyleSheet.create({
  // Header style starts here
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 20,
    color: 'white',
  },
  headerIconsContainer: {
    flexDirection: 'row',
  },
  headerIcon: {
    width: 35,
    height: 35,
    marginRight: 8,
    marginLeft: 8,
  },
  SearchInput: {
    //backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    //fontSize: 15,
    borderWidth: 1,
    borderBottomWidth: 1,
    borderBottomColor: globalColorObject.Color.Primary,
  },
  // screen component style starts here
  scanComponentContainer: {
    flexDirection: 'row',
    paddingVertical: 5,
    alignItems: 'center',
  },
  dotMenuIconContainer: {
    width: 40,
    height: 35,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'salmon'
  },
  dotMenuIcon: {
    width: 27,
    height: 34,
  },
  scanTextInputWrapper: {
    flex: 1,
  },
  ScanInput: {
    width: 45,
    height: 45,
    top: 5,
    right: 2,
    left: 2,
  },
  modalContainer: {
    flex: 1,
  },
  planListItem: {
    padding: 10,
  },
  BarcodeListContainer: {
    flex: 1,
  },
  // Flat List Style
  flatListContents: {
    // flexDirection: 'column-reverse',
  },
  removeItemIconContainer: {
    width: 30,
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },
  removeItemIcon: {
    width: 22,
    height: 22,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: globalColorObject.Color.Primary,
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    //elevation: 5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    flexWrap: 'wrap',
  },
  CardLine1: {
    paddingHorizontal: 5,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  CardLine2: {
    paddingHorizontal: 5,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  CardTextContainer: {
    width: '25%',
    marginVertical: 2,
  },
  StickeyFooterCard: {
    flexDirection: 'row',
    paddingVertical: 10,
    //alignItems: 'center',
    flexWrap: 'wrap',
    backgroundColor: globalColorObject.Color.Primary,
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    width: '95%',
    //elevation: 3,
  },
  cardTextFieldFooter: {
    fontSize: 12,
    color: globalColorObject.Color.oppPrimary,
  },
  cardTextFieldHeader: {
    fontSize: 16,
    color: globalColorObject.Color.oppPrimary,
  },
  Card: {
    flex: 1,
    // flexDirection: 'row',
    flexWrap: 'wrap',
    //backgroundColor: '#f1f1f1',
    // backgroundColor: 'white',
    borderRadius: 5,
    // paddingTop: 10,
    paddingBottom: 7,
    marginVertical: 2,
    marginHorizontal: 13,
    //elevation: 3,
  },
  cardTextField: {
    // fontSize: 16,
    // color: '#2f2f2f',
  },
  CardQtyTextInput: {
    fontSize: 14,
    marginVertical: 3,
    color: '#555',
    fontWeight: 'bold',
    marginRight: 15,
    padding: 0,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(0,0,0,.1)',
    borderRadius: 10,
  },
  // Duplicate Barcode Component styles
  FlatList: {
    paddingVertical: 10,
    marginRight: 10,
    marginLeft: 10,
    // borderRadius: 50,
  },
  // Saving Invoice Modal Component Styles
  ModalLoaderBackDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalLoaderContainer: {
    width: '70%',
    height: 100,
    backgroundColor: globalColorObject.Color.oppPrimary,
    borderRadius: 5,
    flexDirection: 'row',
  },
  ModalLoader: {
    flex: 1,
    alignSelf: 'center',
  },
  ModalLoaderText: {
    flex: 2,
    alignSelf: 'center',
    fontSize: 17,
  },
});

export default AddStockAudit;
