import React, {Component} from 'react';
import {
  StatusBar,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Orientation from 'react-native-orientation';
import {Table, TableWrapper, Cell, Rows} from 'react-native-table-component';
import {Request, AlertError} from '../../../Helpers/HelperMethods';
import moment from 'moment';

export default class TransctionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      APIData: {
        StockDetail: [],
      },
      data: [],
      Check: 123,
      Location: '',
      StockPoint: '',
      Dates: '',
      Type: '',
      Qty: '',
      WSP: '',
      RSP: '',
      ListedMRP: '',
      OnlineMRP: '',
      Icode: '',
    };
  }

  componentDidMount() {
    this.LoadInitialData();
    Orientation.lockToLandscape();
  }
  componentWillUnmount() {
    Orientation.lockToPortrait();
  }
  LoadInitialData = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    this.GetApi();
  };
  GetApi = async () => {
    const {params} = this.props.route;
    const Icode = params.Icode;
    this.setState({Icode: Icode});
    console.log(this.state.Icode);
    Request.get(
      `${this.IP}/api/Inventory/BarcodeHistory/GetSelectedIcodeTransactionDetailsForMobile?Icode=${this.state.Icode}`,
      this.Token,
    )
      .then(res => {
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, StockDetail: res.data},
        }));
      })
      .catch(err => AlertError(err));
  };
  renderTableData() {
    return this.state.APIData.StockDetail.map((TableData, sd_Index) => {
      const {
        Location,
        StockPoint,
        Date,
        Type,
        Qty,
        WSP,
        RSP,
        OnlineMRP,
        ListedMRP,
      } = TableData;
      return (
        <TableWrapper
          key={`STD_${sd_Index}`}
          style={{
            width: '100%',
            flexDirection: 'row',
            backgroundColor: 'white',
          }}>
          <Cell data={Location} style={styles.dataBlock} />
          <Cell data={StockPoint} style={styles.dataBlock} />

          <Cell
            data={moment(Date).format('DD-MM-YYYY')}
            style={styles.dataBlock}
          />
          <Cell data={Type} style={styles.dataBlock} />
          <Cell data={Qty} style={styles.dataBlockr} />
          <Cell data={WSP} style={styles.dataBlockr} />
          <Cell data={RSP} style={styles.dataBlockr} />
          <Cell data={OnlineMRP} style={styles.dataBlockr} />
          <Cell data={ListedMRP} style={styles.dataBlockr} />
        </TableWrapper>
      );
    });
  }

  render() {
    return (
      <>
        <SafeAreaView style={styles.container}>
          <ScrollView horizontal style={styles.ScrollView}>
            <View>
              <Table borderStyle={{borderWidth: 1, borderColor: '#B5B5B5'}}>
                <TableWrapper
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    backgroundColor: '#E9E9E9',
                  }}>
                  <Cell data={'Location'} style={styles.dataBlock} />
                  <Cell data={'Stock Point'} style={styles.dataBlock} />
                  <Cell data={'Date'} style={styles.dataBlock} />
                  <Cell data={'Type'} style={styles.dataBlock} />
                  <Cell data={'Qty'} style={styles.dataBlock} />
                  <Cell data={'WSP'} style={styles.dataBlock} />
                  <Cell data={'RSP'} style={styles.dataBlock} />
                  <Cell data={'Online MRP'} style={styles.dataBlock} />
                  <Cell data={'Listed MRP'} style={styles.dataBlock} />
                </TableWrapper>
              </Table>
              <ScrollView>
                <Table borderStyle={{borderWidth: 1, borderColor: '#B5B5B5'}}>
                  {this.renderTableData()}
                </Table>
              </ScrollView>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  dataBlock: {
    width: 120,
    alignItems: 'center',
  },
  dataBlockr: {
    width: 120,
    alignItems: 'flex-end',
    padding: 5,
  },
  ScrollView: {
    flex: 1,
    marginTop: 10,
    marginLeft: 2,
    marginRight: 2,
  },
});
