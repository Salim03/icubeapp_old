import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  getFormsForModule,
  AlertError,
  getPropsFromArray,
} from '../../../Helpers/HelperMethods';

export default class InventoryHome extends Component {
  state = {
    forms: [],
  };

  componentDidMount() {
    getFormsForModule(4)
      .then(data => {
        const forms =
          data &&
          getPropsFromArray(data, ['MENUNAME'])['MENUNAME'].map(elem =>
            elem.replace(/ /g, ''),
          );
        this.setState({forms});
      })
      .catch(AlertError);
  }

  NavigateToForm = form => {
    switch (form) {
      case 'Courier':
        this.props.navigation.navigate('CourierNav');
        break;
      case 'Logistic':
        this.props.navigation.navigate('LogisticNav');
        break;
      case 'Stock Audit':
        this.props.navigation.navigate('StockAuditNav', {callFrom: 'SE'});
        break;
      case 'Barcode History':
        this.props.navigation.navigate('BarcodeHistoryNav');
        break;
      default:
        break;
    }
  };

  FormCourier = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Courier')}>
        <Image
          source={require('../../../assets/images/Home/Inventory/courier.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Courier</Text>
      </TouchableOpacity>
    );
  };

  FormLogistic = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Logistic')}>
        <Image
          source={require('../../../assets/images/Home/Inventory/Logistic.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Logistic</Text>
      </TouchableOpacity>
    );
  };

  FormStockEntry = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Stock Audit')}>
        <Image
          source={require('../../../assets/images/add_shopping_cart.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Stock</Text>
        <Text style={styles.FormLabel}>Audit</Text>
      </TouchableOpacity>
    );
  };

  FormBarcodeHistory = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Barcode History')}>
        <Image
          source={require('../../../assets/images/popup-blue.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Barcode</Text>
        <Text style={styles.FormLabel}>History</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.LoadContainer}>
            <ScrollView style={styles.DataContainer}>
              <View style={styles.ListContainer}>
                {this.state.forms?.map(form => this[`Form${form}`]?.(form))}
              </View>
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0)',
              position: 'absolute',
              bottom: 0,
              right: 0,
            }}>
            {/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
            <View
              style={{
                alignItems: 'flex-end',
                marginRight: 20,
                flexDirection: 'row',
                marginBottom: 5,
              }}>
              <Text style={{textAlign: 'right'}}>Powered By</Text>
              <Image
                source={require('../../../assets/images/Drawer/Logo.png')}
                style={{height: 30, width: 100}}
              />
            </View>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    marginTop: '80%',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  LoadContainer: {
    // backgroundColor: 'rgb(43, 54, 255)',
    backgroundColor: '#fa6881',
    flex: 1,
  },
  FormImages: {
    width: 30,
    height: 30,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    marginTop: 5,
  },
  FormLabel: {
    textAlign: 'center',
    fontSize: 16,
    // fontWeight: 'bold',
  },
  DataContainer: {
    backgroundColor: 'white',
    marginTop: 30,
    // marginLeft: 5,
    // marginRight: 5,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    flex: 1,
    width: '100%',
    elevation: 50,
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  FormContainer: {
    // backgroundColor: 'rgb(252, 172, 182)',
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 5,
    width: 100,
    alignItems: 'center',
    borderRadius: 10,
    elevation: 15,
  },
});
