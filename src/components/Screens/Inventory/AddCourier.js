// Module Imports
import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  TextInput,
  // Picker,
  Keyboard,
  Image,
  StyleSheet,
  ActivityIndicator,
  Alert,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CheckBox from '@react-native-community/checkbox';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import LayoutWrapper, {CardStyle, layoutStyle} from '../../Layout/Layout';
import RichTextInput from '../../Custom/RichTextInput';
//import RNPickerSelect from 'react-native-picker-select';
// Local Imports
import {Request, AlertMessage} from '../../../Helpers/HelperMethods';

export default class AddCourier extends Component {
  constructor() {
    super();
    this.APILink = {
      API_InitialDataJSON: `/api/Courier/Courier_InitialData?`, //location=1`,
      API_SaveCourier: `/api/Courier/CourierSave`,
      API_GetCourierData: `/api/Courier/CourierInOutById?`, //Type=CourierOut&Id=2`,
    };
    this.state = {
      APIData: {
        AgentListData: [],
        TransportListData: [],
        FromAndToListData: [],
        ReceivedByAndHandoverToData: [],
      },
      CourierType: '',
      isFetched: false,
      Type: '',
      courierNo: '',
      courierDate: '',
      dispatch: '',
      consignmentNo: '',
      consignmentDate: '',
      agent: '',
      remarks: '',
      amount: '',
      fromTo: '',
      receivedBy: '',
      receivedDate: '',
      HandOverTo: '',
      Extinct: false,
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={this.ValidateInputs}>
          <Image
            style={{width: 30, height: 30}}
            source={require('../../../assets/images/save-light.png')}
          />
        </TouchableOpacity>
      ),
    });
    this.LoadInitialdata();
  }
  SaveCourier = async () => {
    Keyboard.dismiss();
    let {state} = this;
    let SaveObject = {
      CourierType: this.state.CourierType,
      CourierNo: this.state.courierNo,
      CourierDate: this.state.courierDate || new Date().toLocaleDateString(),
      DispatchedThrough: this.state.dispatch,
      ConsignmentId: this.state.consignmentNo,
      ConsignmentDate: this.state.consignmentDate,
      Agent: this.state.agent,
      DeclareAmount: this.state.amount,
      Remarks: this.state.remarks,
      InFromOutTo: this.state.fromTo,
      ReceiveBy: this.state.receivedBy,
      ReceivedBydate: this.state.receivedDate,
      HandOverTo: this.state.HandOverTo,
      image: '',
      Ext: this.state.Extinct,
    };

    let get_ddd = `${this.IP}${this.APILink.API_SaveCourier}`;
    console.log('API :', get_ddd);
    //console.log('Data :', SaveObject);
    //console.log('Token :', this.Token);
    Request.post(
      `${this.IP}${this.APILink.API_SaveCourier}`,
      SaveObject,
      this.Token,
    )
      .then(async res => {
        let json = await res.json();
        this.setState({ShowModalLoader: false});
        if (json != 'Failed') {
          AlertMessage(`Courier ${json} Successfully`);
          console.log('Json Result : ', json);
          // this.NavigatePage("LogisticNav");
          this.CourierType ? 'CourierIn' : 'CourierOut', (this.CourierNo = '');
          return true;
        } else {
          AlertMessage('Something went wrong\nCould not save Courier.');
          return false;
        }
      })
      .catch(err => {
        AlertMessage('Could not save \n' + err);
        this.setState({ShowModalLoader: false});
      });
    this.setState({ShowModalLoader: true, ModalLoaderText: 'Saving Data...'});
  };
  LoadInitialdata = async () => {
    //  await AsyncStorage.setItem("Token", `bearer Tj0ESp33MzWVLaaG8FXm1LwTaWnYIFOatBglFYN3go0dUTI_TiterrxOZbFMGtMU4SIi1iXKJNZs0TRpORpSG27LuYzCOVY66FyQ8FVyvAoG4PitgnhVgY3tbhPmJm2h6s6L4pO6b1cuZszVrvmIDVH-c_LlIU9fbO1j5j5dHBphgdejhqq94Z30mTy2r7lSBvejGQjIdUE3n2_t-odZBlXmi1217GXFTMZiN4Rtg1R81avhQjOuuAO1FCTXiOCxu9FGc73p1IARp76dO21zsLlb9djz3xQuSjZjU8pV-jg`);
    // Getting and Setting Configured IP and User Token from AsyncStorage Once when the Component Mounted
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    //console.log(this.IP, this.Token);

    // Fetch Initial Data once
    Request.get(`${this.IP}${this.APILink.API_InitialDataJSON}`, this.Token)
      .then(res => {
        let params = this.props.route.params;
        //console.log(params.CourierType,params.CourierId);
        //this.setState({CourierType:params.CourierType?'CourierIn':'CourierOut'.toString()});
        this.LoadSelectedCourier(
          params.CourierType ? 'CourierIn' : 'CourierOut'.toString(),
          params.CourierId,
          res,
        );
        this.setState(PrevState => ({
          isFetched: true,
          APIData: {
            ...PrevState.APIData,
            AgentListData: res.data.Agent,
            TransportListData: res.data.Transporter,
            ReceivedByAndHandoverToData: res.data.Employee,
            FromAndToListData: res.data.FromTo,
          },
        }));
        //console.log(params);
        //alert(params.CourierType)
        this.setState({CourierType: params.CourierType});
      })
      .catch(err => {
        AlertMessage('Error while fetching data from server\n' + err);
        return false;
      });
  };
  LoadSelectedCourier = async (CourierType, Id, InitialData) => {
    //console.log(CourierType, Id,InitialData);
    //AlertMessage(CourierType, Id,);
    Request.get(
      `${this.IP}${this.APILink.API_GetCourierData}Type=${CourierType}&Id=${Id}`,
      this.Token,
    )
      .then(res => {
        //console.log(CourierType);
        //alert(CourierType);
        let data = res.data[0];
        console.log(data);
        this.setState({
          isFetched: true,
          courierNo:
            data[CourierType == 'CourierIn' ? 'Id' : 'OutId'].toString(),
          courierDate:
            data[
              CourierType == 'CourierIn' ? 'CourierDate' : 'OutCourierDate'
            ].toString(),
          dispatch: data.DispatchThrough.toString(),
          consignmentNo: data.ConsignmentId.toString(),
          ConsignmentDate:
            data[
              CourierType == 'CourierIn'
                ? 'ConsignmentDate'
                : 'OutConsignmentDate'
            ].toString(),
          agent: data.Agent.toString(),
          remarks: data.Remark.toString(),
          amount:
            CourierType == 'CourierOut' ? data.DeclarationAmt.toString() : '',
          fromTo: data[CourierType == 'CourierIn' ? 'From' : 'To'].toString(),
          //fromTo:data.CourierType.toString(),
          receivedBy:
            CourierType == 'CourierIn' ? data.ReceivedBy.toString() : '',
          receivedDate:
            CourierType == 'CourierIn' ? data.ReceivedByDate.toString() : '',
          HandOverTo:
            CourierType == 'CourierIn' ? data.HandOverTo.toString() : '',
          image: '',
          Extinct: data.Extinct,
          //APIData: { ...AftState.APIData, AgentListData: res.data.Agent, TransportListData: res.data.Transporter, ReceivedByAndHandoverToData: res.data.Employee, FromAndToListData: res.data.FromTo },
        }); //,()=>{console.log(this.state)});
      })
      .catch(_err => AlertMessage(JSON.stringify(err)));
  };
  ValidateInputs = async () => {
    let Inputs = `dispatch,consignmentNo,consignmentDate,agent,fromTo`.split(
      ',',
    );
    for (let i = 0, len = Inputs.length; i < len; i++) {
      if (this.state[Inputs[i]] === '') {
        AlertMessage('Enter ' + Inputs[i]);
        try {
          this.refs[Inputs[i]].focus();
        } catch (error) {
          console.log(error);
        }
        return;
      }
    }
    if (this.state.CourierType === 'CourierIn') {
      if (this.state.receivedBy === '') {
        AlertMessage('Enter ReceivedBy');
        try {
          this.refs.receivedBy.focus();
        } catch (error) {
          console.log(error);
        }
        return;
      } else if (this.state.consignmentDate === '') {
        AlertMessage('Enter ConsignmentDate Date');
        try {
          this.refs.ConsignmentDate.focus();
        } catch (error) {
          console.log(error);
        }
        return;
      } else if (this.state.receivedDate === '') {
        AlertMessage('Enter ReceivedBy Date');
        try {
          this.refs.receivedDate.focus();
        } catch (error) {
          console.log(error);
        }
        return;
      } else if (this.state.HandOverTo === '') {
        AlertMessage('Enter HandOverTo');
        try {
          this.refs.HandOverTo.focus();
        } catch (error) {
          console.log(error);
        }
        return;
      }
    }
    this.SaveCourier();
  };
  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };
  refreshLayout = () => this.forceUpdate();

  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    FieldDispatchedThrough = key => (
      <ModalSearchablePicker
        key={key}
        style={layoutStyle.FieldContainer}
        placeholder="Dispatched Through"
        data={this.state.APIData.TransportListData || []}
        labelProp="PARTYNAME"
        valueProp="PARTYCODE"
        selectedValue={this.state.dispatch}
        onValueSelected={dispatch => this.setState({dispatch})}
      />
    );
    FieldConsignmentNo = key => (
      <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
        <RichTextInput
          placeholder="Consignment No"
          wrapperStyle={{width: '48%'}}
          value={this.state.consignmentNo}
          onChangeText={consignmentNo => this.setState({consignmentNo})}
        />
        <ModalDatePicker
          placeholder={'Consignment Date'}
          fieldWrapperStyle={{width: '48%'}}
          selectedValue={this.state.consignmentDate}
          onValueSelected={selectedDate =>
            this.HandleDateChange('consignmentDate', selectedDate)
          }
        />
      </View>
    );
    FieldAgent = key => (
      <ModalSearchablePicker
        key={key}
        style={layoutStyle.FieldContainer}
        placeholder="Agent"
        data={this.state.APIData.AgentListData || []}
        labelProp="PARTYNAME"
        valueProp="PARTYCODE"
        selectedValue={this.state.agent}
        onValueSelected={agent => this.setState({agent})}
      />
    );
    FieldRemarks = key => (
      <RichTextInput
        key={key}
        multiline={true}
        numberOfLines={5}
        style={layoutStyle.FieldContainer}
        value={this.state.remarks}
        onChangeText={remarks => this.setState({remarks})}
        // keyboardType="numeric"
        placeholder="Remarks"
        placeholderTextColor="black"
      />
    );
    FieldFrom = key => (
      <ModalSearchablePicker
        key={key}
        placeholder="From"
        data={this.state.APIData.FromAndToListData || []}
        labelProp="PartyName"
        valueProp="PARTYCODE"
        selectedValue={this.state.fromTo}
        onValueSelected={fromTo => this.setState({fromTo})}
      />
    );
    FieldReceivedBy = key => (
      <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
        <ModalSearchablePicker
          key={key}
          fieldWrapperStyle={{width: '48%'}}
          placeholder="RecievedBy"
          data={this.state.APIData.ReceivedByAndHandoverToData || []}
          labelProp="Firstname"
          valueProp="EMPLOYEEID"
          selectedValue={this.state.receivedBy}
          onValueSelected={receivedBy => this.setState({receivedBy})}
        />
        <ModalDatePicker
          placeholder={'Received Date'}
          fieldWrapperStyle={{width: '48%'}}
          selectedValue={this.state.receivedDate}
          onValueSelected={selectedDate =>
            this.HandleDateChange('receivedDate', selectedDate)
          }
        />
      </View>
    );
    FieldHandoverTo = key => (
      <ModalSearchablePicker
        key={key}
        placeholder="Handover To"
        data={this.state.APIData.ReceivedByAndHandoverToData || []}
        labelProp="Firstname"
        valueProp="EMPLOYEEID"
        selectedValue={this.state.HandOverTo}
        onValueSelected={HandOverTo => this.setState({HandOverTo})}
      />
    );
    FieldTo = key => (
      <ModalSearchablePicker
        key={key}
        placeholder="To"
        data={this.state.APIData.FromAndToListData || []}
        labelProp="PartyName"
        valueProp="PARTYCODE"
        selectedValue={this.state.fromTo}
        onValueSelected={fromTo => this.setState({fromTo})}
      />
    );
    FieldDeclarationAmount = key => (
      <RichTextInput
        key={key}
        multiline={true}
        numberOfLines={5}
        style={styles.FieldInput}
        value={this.state.amount}
        onChangeText={amount => this.setState({amount})}
        keyboardType="numeric"
        placeholder="Declaration Amount"
        placeholderTextColor="black"
      />
    );
    FieldExtinct = key => (
      <View key={key} style={[layoutStyle.FieldLayout]}>
        <View
          style={[
            layoutStyle.FieldContainer,
            {
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              marginTop: 20,
            },
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.BlackColor,
              ),
            ]}>
            {'  '}
            Extinct
          </Text>
          <CheckBox
            //style={layoutStyle.FieldInput}
            fieldWrapperStyle={{width: '48%'}}
            onValueChange={Value => this.setState({Extinct: Value})}
            value={this.state.Extinct}
          />
        </View>
      </View>
    );

    // let TypeValue = this.state.type;
    // let AgentListDataListItems, TransporterListItems, ReceivedByAndHandoverToList, FromToListItems;
    // Preparing UI from fetched data
    //APIData: {  }
    // Preparing Dynamic List for Transporter
    // if (APIData.AgentListData) {
    // 	AgentListDataListItems = APIData.AgentListData.map((data, index) => {
    // 		// return (<Picker.Item key={index} label={data["PARTYNAME"]} value={data["PARTYCODE"]} />);
    // 		return { label: data["PARTYNAME"], value: data["PARTYCODE"] };
    // 	});
    // }
    // if (APIData.TransportListData) {
    // 	TransporterListItems = APIData.TransportListData.map((data, index) => {
    // 		// return (<Picker.Item key={index} label={data["PARTYNAME"]} value={data["PARTYCODE"]} />);
    // 		return { label: data["PARTYNAME"], value: data["PARTYCODE"] };
    // 	});
    // }
    // if (APIData.ReceivedByAndHandoverToData) {
    // 	ReceivedByAndHandoverToList = APIData.ReceivedByAndHandoverToData.map((data, index) => {
    // 		// return (<Picker.Item key={index} label={data["Firstname"]} value={data["EMPLOYEEID"]} />);
    // 		return { label: data["Firstname"], value: data["EMPLOYEEID"] };
    // 	});
    // }
    // if (APIData.FromAndToListData) {
    // 	FromToListItems = APIData.FromAndToListData.map((data, index) => {
    // 		// return (<Picker.Item key={index} label={data["PartyName"]} value={data["PARTYCODE"]} />);
    // 		return { label: data["PartyName"], value: data["PARTYCODE"] };
    // 	});
    // }
    let xtype =
      this.state.CourierType.toString() == 'true'
        ? 'CourierIn'
        : this.state.CourierType.toString() == 'false'
        ? 'CourierOut'
        : this.state.CourierType.toString();
    //alert(xtype);
    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.oppPrimary}
          onLayoutChanged={this.refreshLayout}>
          <View style={styles.InvoiceTabWrapper}>
            <ScrollView style={styles.pagern}>
              <View style={layoutStyle.ScrollContentWrapper}>
                {FieldDispatchedThrough()}
                {FieldConsignmentNo()}
                {FieldAgent()}
                {FieldRemarks()}
                {xtype == 'CourierIn' && FieldFrom()}
                {xtype == 'CourierOut' && FieldTo()}
                {xtype == 'CourierOut' && FieldDeclarationAmount()}
                {xtype == 'CourierIn' && FieldReceivedBy()}
                {xtype == 'CourierIn' && FieldHandoverTo()}
                {FieldExtinct()}
              </View>
            </ScrollView>
          </View>
        </LayoutWrapper>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  LoaderBackdrop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
  },
  Loader: {
    flex: 1,
    alignSelf: 'center',
  },
  LoaderText: {
    flex: 2,
    alignSelf: 'center',
    fontSize: 17,
  },
  LoaderContainer: {
    width: '70%',
    height: 100,
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 5,
    flexDirection: 'row',
  },
  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  pagern: {
    flex: 1,
    backgroundColor: globalColorObject.Color.oppPrimary,
  },
  ModalLoaderBackDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalLoaderContainer: {
    width: '70%',
    height: 100,
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'row',
  },
  ModalLoaderText: {
    flex: 2,
    alignSelf: 'center',
    fontSize: 17,
  },
  FieldContainer: {
    // flexDirection: 'row',
    position: 'relative',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    marginVertical: 6,
  },
  FieldInput: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    textAlign: 'left',
  },
  ScrollViewWrapperContainer: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 8,
  },
  SaveButtonContainer: {
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.025)',
    padding: 10,
  },
  SaveButton: {
    width: 90,
    backgroundColor: '#2091ec',
    padding: 10,
    borderRadius: 5,
    elevation: 5,
  },
  SaveButtonText: {
    fontSize: 17,
    fontWeight: '100',
    textAlign: 'center',
    color: 'white',
  },
  ExtContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    elevation: 3,
    height: 40,
  },
  ExtText: {
    fontSize: 16,
    color: '#222',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  ExtCheck: {
    marginRight: 10,
    transform: [{scaleX: 1.0}, {scaleY: 1.0}],
  },
});

// export {AddCourier};
