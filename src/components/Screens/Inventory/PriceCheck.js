// Module Imports
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
  ScrollView,
  Animated,
  Modal,
  SafeAreaView,
} from 'react-native';
import {CameraScreen} from 'react-native-camera-kit';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {layoutStyle} from '../../Layout/Layout';
import {TextInput} from 'react-native-gesture-handler';
import {
  Request,
  AlertError,
  AlertMessage,
  isnull,
  GetSessionData,
} from '../../../Helpers/HelperMethods';
import {
  globalFontObject,
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
  ApplyStyleFontAndSize,
} from '../../../Style/GlobalStyle';


export default class PriceCheck extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetched: true,
      APIData: {
        IcodeDetails: [],
      },
      isDupBarcode: false,
      Barcode: '',
      ItemName: '',
      Category1: '',
      Category2: '',
      Category3: '',
      Category4: '',
      Category5: '',
      Category6: '',
      Category7: '',
      Category8: '',
      Category9: '',
      Category10: '',
      Category11: '',
      Category12: '',
      Category13: '',
      Category14: '',
      Category15: '',
      Category16: '',
      RSP: '',
      ListedMRP: '',
      scanText: '',
      Invalidcolor: false,
      isPrice: true,
      FreezedPriceHeight: new Animated.Value(0),
      qrvalue: '',
      openScanner: false,
      DubList: false,
      Icode: '',
    };
  }
  componentDidMount() {
    this.LoadInitialData();
  }
  LoadInitialData = async () => {
    const {get_IP, get_Token} = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
  };

  DuplicateBarcodeAPI = async qrvalues => {
    this.setState({openScanner: false, isFetched: false});
    Request.get(
      `${this.IP}/api/Inventory/BarcodeHistory/ValidateScanedBarcode?ScanBarcode=${qrvalues}`,
      this.Token,
    )
      .then(async res => {
        const BarcodeDetails = res.data || [];
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, IcodeDetails: BarcodeDetails},
        }));
        if (BarcodeDetails.length >= 2) {
          this.setState({DubList: true, isFetched: true});
        } else if (BarcodeDetails.length == 0) {
          AlertMessage('Invalid barcode');
          this.clearAll();
          this.setState({Invalidcolor: true, isFetched: true});
        } else {
          let get_Icode = isnull(BarcodeDetails[0].Icode, '');
          await this.setState({Icode: get_Icode, Barcode: qrvalues});
          this.GeneralApiNew(get_Icode);
        }
      })
      .catch(err => {
        this.setState({isFetched: true});
        AlertError('No response from the server');
      });
  };

  GeneralApiNew = async qrvalues => {
    this.setState({isFetched: false});
    await Request.get(
      `${this.IP}/api/Inventory/BarcodeHistory/GetSelectedIcodeDetailsForMobile?Icode=${qrvalues}`,
      this.Token,
    )
      .then(res => {
        let get_ItemDetail = res.data.ItemDetail;
        if ((res.data.StockDetail || []).length === 0) {
          AlertMessage('Invalid Barcode');
          this.clearAll();
          this.setState({Invalidcolor: true, isFetched: true});
        } else {
          this.setState({
            openScanner: false,
            scanText: '',
            isFetched: true,
          });
          const result = get_ItemDetail.map(obj => {
            return {
              key: Object.keys(obj),
              val: Object.values(obj),
            };
          });
          result[0].key.map((getind_key, getind_index) => {
            this.setState({[getind_key]: result[0].val[getind_index]});
          });
        }
      })
      .catch(() => {
        this.setState({isFetched: true});
      });
  };
  onBarcodeScan = qrvalues => {
    const ScanValue = qrvalues;
    this.setState({scanText: ScanValue});
    this.DuplicateBarcodeAPI(qrvalues);
  };

  onOpenScanner = () => {
    this.clearAll();
    var that = this;
    // To Start Scanning

    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'CameraExample App Camera Permission',
              message: 'CameraExample App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({qrvalue: '', openScanner: true});
            // that.setState({ openScanner: true });
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      this.setState({qrvalue: ''});
      this.setState({openScanner: true});
    }
  };

  clearAll = () => {
    this.setState({
      Icode: '',
      Barcode: '',
      ItemName: '',
      Category1: '',
      Category2: '',
      Category3: '',
      Category4: '',
      Category5: '',
      Category6: '',
      Category7: '',
      Category8: '',
      Category9: '',
      Category10: '',
      Category11: '',
      Category12: '',
      Category13: '',
      Category14: '',
      Category15: '',
      Category16: '',
      RSP: '',
      ListedMRP: '',
    });
  };

  InfoText = () => {
    if (this.state.scanText.length > 1) {
      var A = this.state.scanText;
      var B = A.toUpperCase();
      this.setState({scanText: B});
      this.DuplicateBarcodeAPI(B);
    }
  };

  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    if (this.state.openScanner) {
      const handleBarcodeRead = event => {
        this.onBarcodeScan(event.nativeEvent.codeStringValue);
      };
      return (
        <View style={styles.modalContainer}>
          <CameraScreen
            actions={{leftButtonText: 'Cancel'}}
            onBottomButtonPressed={() => {
              this.setState({openScanner: false});
            }}
            showFrame={true}
            scanBarcode={true}
            colorForScannerFrame={'black'}
            onReadCode={handleBarcodeRead}
          />
        </View>
      );
    }
    return (
      <View style={styles.Container}>
        {/* Dublicate barcode popup*/}
        <View style={styles.cView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.DubList}
            onRequestClose={() => {
              alert('Modal has been closed.');
            }}>
            <SafeAreaView style={{height: '100%', width: '100%'}}>
              <View>
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: globalColorObject.Color.Lightprimary,
                  }}>
                  <View style={styles.StickeyHeaderCard}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={[styles.CardTextContainer, {width: '60%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.cardTextFieldHeader,

                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          Barcode
                        </Text>
                      </View>
                      <View style={[styles.CardTextContainer, {width: '40%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.cardTextFieldHeader,
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          Icode
                        </Text>
                      </View>
                    </View>
                    <View style={[styles.CardTextContainer, {width: '100%'}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.cardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        Description
                      </Text>
                    </View>
                  </View>
                  {this.state.APIData.IcodeDetails.map((person, index) => (
                    <View
                      key={`icodedet_${index}`}
                      style={{
                        borderWidth: 2,
                        borderColor: 'white',

                        color: 'white',
                        margin: 5,
                        borderRadius: 15,
                        padding: 5,
                        backgroundColor: globalColorObject.Color.oppPrimary,
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.clearAll();
                          this.setState({
                            scanText: person.Icode,
                            Icode: person.Icode,
                            DubList: false,
                          });
                          this.GeneralApiNew(person.Icode);
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Text
                            // key={index}
                            style={[
                              styles.CardTextContainer,
                              {width: '60%'},

                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Regular,
                                globalFontObject.Size.small.sl,
                              ),
                            ]}>
                            {person.Barcode}
                          </Text>
                          <Text
                            // key={index}
                            style={[
                              styles.CardTextContainer,
                              {width: '40%'},
                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Regular,
                                globalFontObject.Size.small.sl,
                              ),
                            ]}>
                            {person.Icode}
                          </Text>
                        </View>
                        <Text
                          // key={index}
                          style={[
                            styles.CardTextContainer,
                            {width: '100%'},

                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Regular,
                              globalFontObject.Size.small.sl,
                            ),
                          ]}>
                          {person.Description}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              </View>
            </SafeAreaView>
          </Modal>
        </View>
        <View style={[styles.SearhBarWrapper, {flexDirection: 'row'}]}>
          <TextInput
            style={[
              styles.SearchInput,
              layoutStyle.FieldInput,
              {
                borderBottomColor: globalColorObject.Color.Primary,
                borderBottomWidth: 1,
              },
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderColor,
              ),
              {width: '78%'},
            ]}
            onChangeText={scanText => this.setState({scanText})}
            value={this.state.scanText}
            autoCapitalize="characters"
            returnKeyType="next"
            autoCorrect={false}
            onBlur={this.InfoText}
            placeholder="Scan Here"
            placeholderTextColor="#AFAFAF"
            onFocus={this.showCancel}
          />
          {isnull(this.state.Icode, '') != '' && (
            <>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  marginHorizontal: 8,
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}
                onPress={this.clearAll}>
                <Text
                  style={[
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.large.ls,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  &#9851;
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 30,
                  height: 30,
                  marginHorizontal: 8,
                  alignSelf: 'center',
                  justifyContent: 'center',
                }}
                onPress={this.onOpenScanner}>
                <Text
                  style={[
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.large.ls,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  &#9974;
                </Text>
              </TouchableOpacity>
            </>
          )}
        </View>
        {isnull(this.state.Icode, '') != '' ? (
          <>
            <View
              style={{
                flex: 1,
                width: '85%',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <View style={styles.ItemCard}>
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.large.ls,
                    ),{
                    textAlign: 'center',
                    color: 'white',
                    paddingHorizontal: 2,
                    paddingTop: 10,
                    paddingBottom: 5,
                    borderBottomWidth: 5,
                    borderColor: 'white',
                  }]}>
                  {isnull(this.state.ItemName, 'Product Name').toUpperCase()}
                </Text>
                <Text
                  style={[
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.mediam.ml,
                    ),{
                    color: 'white',
                    textAlign: 'center',
                    paddingVertical: 10,
                  }]}>
                  {`MRP : ₹${this.state.ListedMRP}`}
                </Text>
                <Text
                  style={[                    ApplyStyleFontAndSize(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                  ),{
                    color: 'white',
                    textDecorationLine: 'underline',
                    textAlign: 'center',
                  }]}>
                  {'Our Price'}
                </Text>
                <Text
                  style={[ApplyStyleFontAndSize(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.mxl,
                  ),{
                    textAlign: 'center',
                    color: 'white',
                    paddingBottom: 5,
                  }]}>
                  {`₹ ${this.state.RSP}`}
                </Text>
                {this.state.ListedMRP - this.state.RSP > 0 && (
                  <View
                    style={[{
                      width: '100%',
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingVertical: 5,
                    }]}>
                    <Text
                      style={[ApplyStyleFontAndSize(
                        globalFontObject.Font.Regular,
                        globalFontObject.Size.small.sl,
                      ),{
                        textAlign: 'center',
                        color: 'white',
                      }]}>
                      {'Save : '}
                    </Text>
                    <Text
                      style={[ApplyStyleFontAndSize(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.mediam.mxl,
                      ),{
                        textAlign: 'center',
                        fontWeight: 'bold',
                        color: 'white',
                      }]}>
                      {`₹${this.state.ListedMRP - this.state.RSP}`}
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </>
        ) : (
          <View
            style={{
              flex: 1,
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: 50,
                height: 50,
                marginHorizontal: 8,
                alignSelf: 'center',
                justifyContent: 'center',
              }}
              onPress={this.onOpenScanner}>
              <Text
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.extraLarge.xlxxl,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {textAlign: 'center'},
                ]}>
                &#9974;
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
  },
  Container: {
    backgroundColor: globalColorObject.Color.Lightprimary,
    flex: 1,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderBottomColor: globalColorObject.Color.Primary,
  },
  ItemCard: {
    backgroundColor: 'red',
    color: 'white',
    borderColor: 'rgba(0,0,0,0.15)',
    borderRadius: 10,
    paddingVertical: 10,
  },
  cView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  StickeyHeaderCard: {
    backgroundColor: globalColorObject.Color.Primary,
    margin: 5,
  },
  SearhBarWrapper: {
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
    width: '100%',
  },
  CardTextContainer: {
    width: '50%',
    marginVertical: 3,
  },
  cardTextFieldHeader: {
    color: 'white',
    paddingLeft: 10,
  },
});
