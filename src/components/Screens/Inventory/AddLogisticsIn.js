// Module Imports
import React, {Component} from 'react';
import {
	View,
	ScrollView,
	StatusBar,
	Text,
	TextInput,
	Image,
	StyleSheet,
	ActivityIndicator,
	TouchableOpacity,
	Switch,
	Modal,
	Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import ModalDatePicker from '../../Custom/ModalDatePicker';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import RichTextInput from '../../Custom/RichTextInput';
import LayoutWrapper, {CardStyle, layoutStyle} from '../../Layout/Layout';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {
  Request,
  AlertMessage,
  GetSessionData,
} from '../../../Helpers/HelperMethods';

export default class AddLogisticsIn extends Component {
  constructor() {
    super();
    this.APILink = {
      API_InitialDataJSON: `/api/LRInward/LoadInitialData?`, //location=1`,
      API_SelectedtrackNoData: `/api/LRInward/LRInward_GetLRINDetail?`, //LRNo=L1/LRI0020&LRDate=2019-12-02&Type=LRIN&Loccode=1`,
      API_CourierNoList: `/api/LRInward/LRInward_Retrievecourier?`, //?SupCode=${this.APIParams.CourierNo.SupCode}&CourCode=${this.APIParams.CourierNo.CourCode}`,
      API_SupplierList: `/api/LRInward/LRInward_GetPartyBasedForLR?`, //Type=Supplier&Loccode=1`,
      API_Transporter: `/api/LRInward/LRInward_GetPartyBasedOnTypeForLR?`, //Type=transporter&Loccode=1`,
      // API_Station: ``,
      API_SaveLogistics: `/api/LRInward/LRSave`,
    };
    this.LocalData = {
      CallFrom: '',
      Title: '',
      InvoiceNo: '',
      InvoiceID: 0,
      InvoiceDate: '',
      Location: '1',
      UserCode: '',
      FinancialYearId: '0',
    };
    this.FetchedSupplierData;
    this.state = {
      isFetched: false,
      APIData: {
        CourierNoData: [],
        SupplierData: [],
        TransporterData: [],
        StationData: [],
        UOM: [],
      },
      // Prop to Show Modal Loader for saving Logistics or Binding Logitics Data
      ShowModalLoader: false,
      ModalLoaderText: 'Saving Data...',

      lrType: 'LRIN',
      // Props to hold the value of all the controls
      Type: '',
      CourierNo: '',
      // Property to hold the Supplier of the selected LR
      SupplierOfSelectedLR: '',
      // property to hold the value of current selected Supplier
      Supplier: '',
      LrNo: '',
      LrDate: '',
      EWayNo: '',
      EWayDate: '',
      Transporter: '',
      Mode: 'Road',
      StationFrom: '',
      StationTo: '',
      Unit: '',
      Quantity: '',
      ActualWeight: '',
      ChargedWeight: '',
      ToPay: true,
      Rate: '',
      Freight: '',
      Others: '',
      TotalFreight: '',
      DeclarationAmount: '',
      VehicleNo: '',
      Policy: '',
      PermitNo: '',
      Remarks: '',
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={this.ValidateInputs}>
          <Image
            style={{width: 30, height: 30, paddingLeft: 5}}
            source={require('../../../assets/images/save-light.png')}
          />
        </TouchableOpacity>
      ),
    });
    // this.props.navigation.setParams({
    // 	ValidateInputs: this.ValidateInputs
    // });
    this.LoadInitialdata();
    // .then(result => {
    // 	console.log(result);
    // 	if (result && .params) {
    // 		this.setState({ isFetched: true });
    // 	}
    // })
    // .catch(err => this.setState({ isFetched: true }));
  }

  HandleTypeChange = Type => {
    // console.log("Type Change ", Type);
    this.FetchedSupplierData = false;
    let SupplierOfSelectedLR = this.state.SupplierOfSelectedLR;
    this.setState({Type, Supplier: SupplierOfSelectedLR});
    if (Type !== '') {
      Type =
        Type === 'Goods Received Note'
          ? 'Supplier'
          : Type === 'Purchase Transfer'
          ? 'Location'
          : Type === 'Sales Return'
          ? 'Customer'
          : 'Supplier';
      this.LoadDataForSelectedType(Type);
    }
  };

  HandleActualWeightChange = ActualWeight => {
    this.setState({ActualWeight, ChargedWeight: ActualWeight}, () => {
      this.CalculateFreightAmount();
    });
    // console.log(this.state.ActualWeight,this.state.ChargedWeight);
    // this.ValidateFreightCalculation(false, ActualWeight);
  };

  HandleChargedWeightChange = ChargedWeight => {
    this.setState({ChargedWeight}, () => {
      this.CalculateFreightAmount();
    });
  };

  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };

  HandleChargedWeightBlur = () => {
    // try {
    // 	if (this.state.ChargedWeight < this.state.ActualWeight) {
    // 		AlertMessage("Charged Weight should be greater than or equal to Actual Weight");
    // 		this.refs.ChargedWeight.focus();
    // 	}
    // }
    // catch (err) { console.log(err) }
    this.ValidateFreightCalculation(true);
  };

  HandleRateBlur = () => {
    this.ValidateFreightCalculation();
  };

  ValidateFreightCalculation = (validateChargedWeight = false) => {
    try {
      // variable to hold the status Whether allow user to save or not
      let AllowSave = true;

      // check whether user entered Rate
      if (this.state.Rate !== '') {
        // Check whether user entered Actual weight
        // console.log(this.state.ActualWeight,ActualWeight);
        // ActualWeight = ActualWeight === '' ? this.state.ActualWeight : ActualWeight;
        if (this.state.ActualWeight === '') {
          AlertMessage('Enter Actual Weight');
          this.refs.ActualWeight.focus();
          // this.setState({ Rate: '' });
          AllowSave = false;
        } else if (this.state.ChargedWeight === '') {
          AlertMessage('Enter Charged Weight');
          this.refs.ChargedWeight.focus();
          // this.setState({ Rate: '' });
          AllowSave = false;
        } else {
          this.CalculateFreightAmount();
        }
      }
      // Validate Charged Weight is greater than or equal to Actual Weight
      if (validateChargedWeight) {
        // console.log("Validating Weight");
        // console.log(`${this.state.ChargedWeight} < ${this.state.ActualWeight}`);
        // console.log(typeof (parseFloat(this.state.ChargedWeight)), typeof (parseFloat(this.state.ActualWeight)));
        // console.log(typeof (parseFloat(this.state.ChargedWeight)) < typeof (parseFloat(this.state.ActualWeight)));
        if (
          parseFloat(this.state.ChargedWeight) <
          parseFloat(this.state.ActualWeight)
        ) {
          AlertMessage(
            'Charged Weight should be greater than or equal to Actual Weight',
          );
          this.refs.ChargedWeight.focus();
        }
      }
      return AllowSave;
    } catch (err) {
      console.log(err);
    }
  };

  CalculateFreightAmount = () => {
    try {
      // Freight charge will be calculated by Multipling ChargedWeight and Rate ( ChargedWeight * Rate )
      let ChargedWeight = parseFloat(this.state.ChargedWeight || 0);
      let Rate = parseFloat(this.state.Rate || 0);
      this.setState({Freight: (ChargedWeight * Rate).toString()});
    } catch (err) {
      console.log(err);
    }
  };

  CalculateTotalFreight = () => {
    try {
      let {Freight, Others} = this.state;
      this.setState({
        TotalFreight: (
          parseFloat(Freight || 0) + parseFloat(Others || 0)
        ).toString(),
      });
    } catch (err) {
      console.log(err);
    }
  };

  LoadDataForSelectedType = async Type => {
    console.log('actual type: ', Type);
    console.log(
      'Call API : ',
      `${this.IP}${this.APILink.API_SupplierList}Type=${Type}&Loccode=${this.LocalData.Location}`,
    );
    console.log('Call token : ', this.Token);
    Request.get(
      `${this.IP}${this.APILink.API_SupplierList}Type=${Type}&Loccode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        this.FetchedSupplierData = true;
        console.log(res.status, ' : ', res.data);
        if (res.status === 200) {
          this.setState(PrevState => ({
            APIData: {...PrevState.APIData, SupplierData: res.data},
          }));
        } else {
          AlertMessage('Error while getting data for selected type');
          this.setState(PrevState => ({
            APIData: {...PrevState.APIData, SupplierData: []},
          }));
        }
      })
      .catch(err => {
        AlertMessage(
          'Error while getting data for selected type\n' + JSON.stringify(err),
        );
        this.FetchedSupplierData = true;
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, SupplierData: []},
        }));
      });
  };

  NavigatePage = page => {
    this.props.navigation.navigate(page);
  };

  LoadInitialdata = async () => {
    // await AsyncStorage.setItem("Token", `bearer hTnQONiYsP-Rq6QPcXK9SPeHxOIBexJgNa8cYgf6PEpkc4ulJPRiLdD1_f1lguR6BcCqc7Tk3RmXcWXjojnOCkIRhgIO9dlQaFLILdtp27l9ARxvEyWPpaZRUlwGZ4Vs7cySL8l2FSvXvXxKukc_lOLREd3Ti7kIyYkNaUx2dqLrvpGSgFUM5nLJwAvwQoxa2LEOsS3chNcSBIDljnKmH123II81TGWNDn7CyrJS46lSRkwKBEmxLc4jJfNaEl3Z2rKnEh9fCfcEjjEskSITmMK_BC9J1rFXTWBxdf9pa6c`);
    // Getting and Setting Configured IP and User Token from AsyncStorage Once when the Component Mounted

    let {get_IP, get_Token, get_UserCode, get_Location, get_FinancialYearId} =
      await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.Location = get_Location;
    this.LocalData.FinancialYearId = get_FinancialYearId;

    this.LocalData.InvoiceID = 0;
    this.LocalData.InvoiceNo = '';

    // Fetch Initial Data once
    Request.get(
      `${this.IP}${this.APILink.API_InitialDataJSON}location=1`,
      this.Token,
    )
      .then(res => {
        this.setState(PrevState => ({
          isFetched: true,
          APIData: {
            ...PrevState.APIData,
            CourierNoData: res.data.Courier,
            TransporterData: res.data.Transporter,
            StationData: res.data.Station,
            UOM: res.data.UOM,
          },
        }));
        let params = this.props.route.params;
        if (params && params['LRNo'] && params['LRDate']) {
          // console.log(params)
          this.setState({
            ShowModalLoader: true,
            ModalLoaderText: 'Binding Data...',
          });
          // console.log(params.LRNo, params.LRDate);
          this.LoadDataForSelectedLR(params.LRNo, params.LRDate);
          this.LocalData.InvoiceNo = params.LRNo;
          this.LocalData.InvoiceDate = params.LRDate;
        }
      })
      .catch(err => {
        AlertMessage(
          'Error while fetching data from server\n' + JSON.stringify(err),
        );
      });
  };

  LoadDataForSelectedLR = async (LRNo, LRDate) => {
    // Fetch Data for Selected TrackNo on previous page

    // console.log(`${this.IP}${this.APILink.API_SelectedtrackNoData}LRNo=${LRNo}&LRDate=${LRDate}&Type=LRIN&Loccode=1`);
    Request.get(
      `${this.IP}${this.APILink.API_SelectedtrackNoData}LRNo=${LRNo}&LRDate=${LRDate}&Type=LRIN&Loccode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        let data = res.data[0];
        // console.log(data);
        this.LocalData.InvoiceID = data.ID;
        // console.log(this.LocalData.InvoiceNo, this.LrDate, this.ID);
        this.setState({
          // isFetched: true,
          Type: data.TypeOf.toString(), //=== "Goods Received Note" ? "Supplier" : data.TypeOf === "" ? "Location" : data.TypeOf === "" ? "" : "",
          CourierNo: data.CourierNO.toString(),
          SupplierOfSelectedLR: data.Supplier.toString(),
          Supplier: data.Supplier.toString(),
          LrNo: data.LRNo.toString(),
          LrDate: data.LRDate.toString(),
          EWayNo: data.EwayNo.toString(),
          EWayDate: data.EwayDate.toString(),
          Transporter: data.Transporter.toString(),
          Mode: data.Mode.toString(),
          StationFrom: data.StationFrom.toString(),
          StationTo: data.StationTo.toString(),
          Unit: data.Unit.toString(),
          Quantity: data.Qty.toString(),
          ActualWeight: data.ActualWeight.toString(),
          ChargedWeight: data.ChargedWeight.toString(),
          ToPay: data.ToPay.toLowerCase() === 'y',
          Rate: data.Rate.toString(),
          Freight: data.Freight.toString(),
          Others: data.Others.toString(),
          TotalFreight: data.TotalFreight.toString(),
          DeclarationAmount: data.DeclarationAmount.toString(),
          VehicleNo: data.VehicleNo.toString(),
          Policy: data.Policy.toString(),
          PermitNo: data.PermitNo.toString(),
          Remarks: data.Remarks.toString(),
          // Hide the Binding Data... loader
          ShowModalLoader: false,
          // APIData: { ...PrevState.APIData, CourierNoData: InitialData.Courier, TransporterData: InitialData.Transporter, StationData: InitialData.Station, UOM: InitialData.UOM },
        });
        // console.log(this.state);
        // this.LoadDataForSelectedType(data.TypeOf.toString());
        let Type =
          data.TypeOf.toString() === 'Goods Received Note'
            ? 'Supplier'
            : state.Type === 'Purchase Transfer'
            ? 'Location'
            : state.Type === 'Sales Return'
            ? 'Customer'
            : 'Supplier';
        this.LoadDataForSelectedType(Type);
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };
  ValidateInputs = async () => {
    // Validate all the mandatory inputs
    let Inputs =
      `Type,Supplier,LrNo,LrDate,Transporter,Mode,Unit,Quantity`.split(',');
    for (let i = 0, len = Inputs.length; i < len; i++) {
      if (this.state[Inputs[i]] === '') {
        AlertMessage('Enter ' + Inputs[i]);
        try {
          this.refs[Inputs[i]].focus();
        } catch (error) {
          console.log(error);
        }
        return;
      }
    }

    // validate Charged weight , Rate , Freight etc based on condition
    if (this.ValidateFreightCalculation()) {
      // Save Logistics
      this.SaveLogistics();
    }
  };

  SaveLogistics = async () => {
    Keyboard.dismiss();
    // Preparing json for send data to API
    // lrType,logisticId,logisticNumber,logisticDate,selectedType,selectedCourierId,selectedPartnerCode,lrNumber,lrDate,eWayNo,eWayDate,selectedTransportCode,mode,pickupStation,dropStation,unit,quantity,actualWeight,chargedWeight,toPay,rate,freightAmount,otherAmount,totalFreightAmount,inVoiceAmount,vehicleNo,policy,permitNo,remarks,locationCode,UserId,objDocument
    let {state} = this;
    let SaveObject = {
      lrType: this.state.lrType,
      logisticId: this.LocalData.InvoiceID || 0,
      logisticNumber: this.LocalData.InvoiceNo || '',
      logisticDate:
        this.LocalData.InvoiceDate || new Date().toLocaleDateString(),
      selectedType: state.Type, // === "Supplier" ? "Goods Received Note" : state.Type === "Location" ? "Purchase Transfer" : state.Type === "Customer" ? "Sales Return" : "Goods Received Note",
      selectedCourierId: state.CourierNo,
      selectedPartnerCode: state.Supplier,
      lrNumber: state.LrNo,
      lrDate: state.LrDate,
      eWayNo: state.EWayNo,
      eWayDate: state.EWayDate,
      selectedTransportCode: state.Transporter,
      mode: state.Mode,
      pickupStation: state.StationFrom,
      dropStation: state.StationTo,
      unit: state.Unit,
      quantity: state.Quantity,
      actualWeight: state.ActualWeight,
      chargedWeight: state.ChargedWeight,
      toPay: state.ToPay,
      rate: state.Rate,
      freightAmount: state.Freight,
      otherAmount: state.Others,
      totalFreightAmount: state.TotalFreight,
      inVoiceAmount: state.DeclarationAmount,
      vehicleNo: state.VehicleNo,
      policy: state.Policy,
      permitNo: state.PermitNo,
      remarks: state.Remarks,
      locationCode: this.LocalData.Location,
      UserId: await AsyncStorage.getItem('UserCode'),
      objDocument: [],
    };
    Request.post(
      `${this.IP}${this.APILink.API_SaveLogistics}`,
      SaveObject,
      this.Token,
    )
      .then(async res => {
        let json = await res.json();
        this.setState({ShowModalLoader: false});
        if (json != 'Failed') {
          AlertMessage(`Logistic ${json} Successfully`, () => {
            this.NavigatePage('LogisticNav');
            // if (this.LocalData.InvoiceNo)
            // {
            // 	console.log('Nav Call',this.LocalData.InvoiceNo);
            // 	this.NavigatePage("LogisticNav");}
            // else{
            // 	console.log('Clear Entry',this.LocalData.InvoiceNo);
            // 	 this.ClearEnrty();}
            this.LocalData.InvoiceID = 0;
            this.LocalData.InvoiceNo = '';
          });
          return true;
        } else {
          AlertMessage('Something went wrong\nCould not save Logistic.');
          return false;
        }
      })
      .catch(err => {
        AlertMessage('Could not save \n' + JSON.stringify(err));
        this.setState({ShowModalLoader: false});
      });
    this.setState({ShowModalLoader: true, ModalLoaderText: 'Saving Data...'});
    this.forceUpdate();
  };

  ClearEnrty = () => {
    this.setState({
      Type: '',
      CourierNo: '',
      // Property to hold the Supplier of the selected LR
      SupplierOfSelectedLR: '',
      // property to hold the value of current selected Supplier
      Supplier: '',
      LrNo: '',
      LrDate: '',
      EWayNo: '',
      EWayDate: '',
      Transporter: '',
      Mode: 'Road',
      StationFrom: '',
      StationTo: '',
      Unit: '',
      Quantity: '',
      ActualWeight: '',
      ChargedWeight: '',
      ToPay: false,

      Rate: '',
      Freight: '',
      Others: '',
      TotalFreight: '',
      DeclarationAmount: '',
      VehicleNo: '',
      Policy: '',
      PermitNo: '',
      Remarks: '',
    });
  };

  // OpenDatePicker = async (state) => {
  // 	let { action, year, month, day } = await DatePickerIOS.open();
  // 	if (action !== DatePickerIOS.dismissedAction) {
  // 		this.setState({ state: `${day}-${month + 1}-${year}` });
  // 	}
  // }

  // Local variables to save the list items and it can be accessible through out the class

  refreshLayout = () => this.forceUpdate();
  FieldTypeCourierNo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>   
    <ModalSearchablePicker
      key={key}
      placeholder="Type"
      fieldWrapperStyle={{width:'48%'}}	
      data={[
        {label: 'Goods Received Note', value: 'Goods Received Note'},
        {label: 'Purchase Transfer', value: 'Purchase Transfer'},
        {label: 'Sales Return', value: 'Sales Return'},
      ]}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Type}
      onValueSelected={this.HandleTypeChange}
    />
    <ModalSearchablePicker
      key={key}
      placeholder="Courier No"
      fieldWrapperStyle={{width:'48%'}}	
      data={this.state.APIData.CourierNoData}
      labelProp="COURIERNO"
      valueProp="COURIERID"
      selectedValue={this.state.CourierNo}
      onValueSelected={CourierNo => this.setState({CourierNo})}
    />
    </View>
   
  );
  FieldSupplier = key => {
    let TypeValue = this.state.Type;
    let LabelName =
      TypeValue === 'Goods Received Note'
        ? 'PARTY NAME'
        : TypeValue === 'Purchase Transfer'
        ? 'LocationName'
        : TypeValue === 'Sales Return'
        ? 'CUSTOMERNAME'
        : '';
    let ValueName =
      TypeValue === 'Goods Received Note'
        ? 'PARTY CODE'
        : TypeValue === 'Purchase Transfer'
        ? 'LocationCode'
        : TypeValue === 'Sales Return'
        ? 'CUSTOMERCODE'
        : '';

    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Supplier"
        data={this.state.APIData.SupplierData}
        labelProp={LabelName}
        valueProp={ValueName}
        selectedValue={this.state.Supplier}
        onValueSelected={Supplier => this.setState({Supplier})}
      />
    );
  };
  FieldLRNoDate = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>		
    <RichTextInput
      key={key}
      placeholder="LR No"
	  //style={layoutStyle.FieldInput}
    wrapperStyle={{width:'48%'}}
      value={this.state.LrNo}
      onChangeText={LrNo => this.setState({LrNo})}
      ref={'LrNo'}
    /> 
    <ModalDatePicker
      placeholder={'Lr Date'}
      fieldWrapperStyle={{width: '48%'}}
      selectedValue={this.state.LrDate}
      onValueSelected={(selectedDate) => this.HandleDateChange('LrDate',selectedDate ) }
    />
   </View>
  );
  FieldEWayNoDate = key => (
	  <View key={key} style={[layoutStyle.FieldLayout,{flexDirection:'row'}]}>
	  
		<RichTextInput
      key={key}
      placeholder="E-Way No"
      wrapperStyle={{width:'48%'}}
     // style={layoutStyle.FieldInput}
	  value={this.state.EWayNo}
      onChangeText={EWayNo => this.setState({EWayNo})}
      ref={'EWayNo'}
   			 />
   <ModalDatePicker
      placeholder={'E-Way Date'}
      fieldWrapperStyle={{width: '48%'}}
      selectedValue={this.state.EWayDate}
      onValueSelected={(selectedDate) => this.HandleDateChange('EWayDate',selectedDate ) }
    />
  </View>
  );
 
  FieldTransporter = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Transporter"
      data={this.state.APIData.TransporterData}
      labelProp="TRANSPORTER NAME"
      valueProp="TRANSPORTER CODE"
      selectedValue={this.state.Transporter}
      onValueSelected={Transporter => this.setState({Transporter})}
    />
  );

  FieldMode = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Mode"
      data={[
        {label: 'Road', value: 'Road'},
        {label: 'Air', value: 'Air'},
        {label: 'Sea', value: 'Sea'},
      ]}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Mode}
      onValueSelected={Mode => this.setState({Mode})}
    />
  );
  FieldStationFrom = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Station From"
      data={this.state.APIData.StationData}
      labelProp="CITY"
      valueProp="CID"
      selectedValue={this.state.StationFrom}
      onValueSelected={StationFrom => this.setState({StationFrom})}
    />
  );
  FieldStationTo = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Station To"
      data={this.state.APIData.StationData}
      labelProp="CITY"
      valueProp="CID"
      selectedValue={this.state.StationTo}
      onValueSelected={StationTo => this.setState({StationTo})}
    />
  );
  FieldUnitQuantity = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>	
    <ModalSearchablePicker
      key={key}
      placeholder="Unit"
      data={this.state.APIData.UOM}
      fieldWrapperStyle={{width:'48%'}}
      labelProp="UOMName"
      valueProp="UOMID"
      selectedValue={this.state.Unit}
      onValueSelected={Unit => this.setState({Unit})}
    />
    <RichTextInput
      key={key}
      placeholder="Quantity"
      style={layoutStyle.FieldInput}
      wrapperStyle={{width:'48%'}}
      value={this.state.Quantity}
      onChangeText={Quantity => this.setState({Quantity})}
      ref={'Quantity'}
    />   
    </View>
  );
  FieldActualWeightChargedWeight = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
    <RichTextInput
      key={key}
      placeholder="Actual Weight"
      value={this.state.ActualWeight}
      wrapperStyle={{width:'48%'}}
      onChangeText={this.HandleActualWeightChange}
      ref={'ActualWeight'}
      keyboardType="numeric"
    />
    <RichTextInput
      key={key}
      placeholder="Charged Weight"
      value={this.state.ChargedWeight}
      wrapperStyle={{width:'48%'}}
      onChangeText={this.HandleChargedWeightChange}
      onBlur={this.HandleChargedWeightBlur}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'ChargedWeight'}
    />
    </View>
  );
  FieldRateToPay = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>		
    <View style={styles.SwitchFieldContainer}>
      <Text style={styles.FieldLabelToPay}>To Pay</Text>
      <Switch
        value={this.state.ToPay}
        onValueChange={(ToPay) => this.setState({ ToPay })} />
    </View>
    <RichTextInput
        key={key}
        placeholder="Rate"
        onBlur={this.HandleRateBlur}
        wrapperStyle={{width:'48%'}}
        style={styles.FieldInput}
        value={this.state.Rate}
        onChangeText={Rate => this.setState({ Rate })}
        keyboardType="numeric"
        returnKeyType="next"
        autoCorrect={false}
        autoCapitalize="none"
        ref={'Rate'} />
        </View>        
  );
  FieldFreightTotalFreight = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
    <RichTextInput
      key={key}
      placeholder="Freight"
      value={this.state.Others}
      wrapperStyle={{width:'48%'}}
      onChangeText={Others =>
        this.setState({Others}, this.CalculateTotalFreight)
      }
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Others'}
    />
    <RichTextInput
      key={key}
      placeholder="Total Freight"
      wrapperStyle={{width:'48%'}}
      underlineColorAndroid="transparent"
      editable={false}
      value={this.state.TotalFreight}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'TotalFreight'}
    />
  </View>
  );
  FieldDeclaration = key => (
    <RichTextInput
      key={key}
      placeholder="Declaration "
      value={this.state.DeclarationAmount}
      onChangeText={DeclarationAmount => this.setState({DeclarationAmount})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      onSubmitEditing={() => {
        this.refs.VehicleNo.focus();
      }}
      autoCapitalize="none"
      ref={'DeclarationAmount'}
    />
  );

  FieldVehicleNoPolicy = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
    <RichTextInput
      key={key}
      placeholder="Vehicle No"
      wrapperStyle={{width:'48%'}}
      value={this.state.VehicleNo}
      onChangeText={VehicleNo => this.setState({VehicleNo})}
      ref={'VehicleNo'}
      onSubmitEditing={() => {
        this.refs.Policy.focus();
      }}
    />
  
    <RichTextInput
      key={key}
      placeholder="Policy"
      value={this.state.Policy}
      wrapperStyle={{width:'48%'}}
      onChangeText={Policy => this.setState({Policy})}
      ref={'Policy'}
      onSubmitEditing={() => {
        this.refs.PermitNo.focus();
      }}
    />
    </View>
  );
  PermitNo = key => (
    <RichTextInput
      key={key}
      placeholder="Permit No"
      value={this.state.PermitNo}
      onChangeText={PermitNo => this.setState({PermitNo})}
      ref={'PermitNo'}
      onSubmitEditing={() => {
        this.refs.Remarks.focus();
      }}
    />
  );
  FieldRemarks = key => (
    <RichTextInput
      key={key}
      inputStyle={{height: null, maxHeight: 100, lineHeight: 20}}
      placeholder="Remarks"
      value={this.state.Remarks}
      onChangeText={Remarks => this.setState({Remarks})}
      inputProps={{multiline: true, numberOfLines: 2}}
      ref={'Remarks'}
    />
  );

  render() {
    /* {
		let IsChangeStatus = this.ChangeStatusDataList().length > 0;
		console.log(
		  'Render',
		  'my log status : ',
		  this.setState
		  ' load data : ',
		  IsChangeStatus,
		  
		);}*/
    // Showing loader untill Initial Data has been fetched
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    // refersh layout		  // Preparing List Items for Initial or Unchanged Data
    //   this.PrepareListsForInitialData();
    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.Lightprimary}
          onLayoutChanged={this.refreshLayout}>
          <View style={styles.InvoiceTabWrapper}>
            <ScrollView style={styles.pagern}>
              <View style={layoutStyle.ScrollContentWrapper}>
                {this.FieldTypeCourierNo()}
                {this.FieldSupplier()}
                {this.FieldLRNoDate()}
			          {this.FieldEWayNoDate()}
                {this.FieldTransporter()}
                {this.FieldMode()}
                {this.FieldStationFrom()}
                {this.FieldStationTo()}
                {this.FieldUnitQuantity()}
               
                {this.FieldActualWeightChargedWeight()}
             
                {this.FieldRateToPay()}
                {this.FieldFreightTotalFreight()}
                
                {this.FieldDeclaration()}
                {this. FieldVehicleNoPolicy()}
  
                {this.PermitNo()}
                {this.FieldRemarks()}
              </View>
              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.ShowModalLoader}>
                <View style={styles.ModalLoaderBackDrop}>
                  <View style={styles.ModalLoaderContainer}>
                    <ActivityIndicator
                      size="large"
                      style={styles.ModalLoader}
                    />
                    <Text style={styles.ModalLoaderText}>
                      {this.state.ModalLoaderText}
                    </Text>
                  </View>
                </View>
              </Modal>
              {/* Custom Popup Loader ends here*/}
            </ScrollView>
          </View>
        </LayoutWrapper>
      </>
    );
  }
}

const styles = StyleSheet.create({
	LoaderBackdrop: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		// backgroundColor: 'rgba(0,0,0,0.5)',
	},
	LoaderContainer: {
		width: '70%',
		height: 100,
		backgroundColor: 'rgba(0,0,0,0.1)',
		borderRadius: 5,
		flexDirection: 'row',
	},
	Loader: {
		flex: 1,
		alignSelf: 'center',
	},
	LoaderText: {
		flex: 2,
		alignSelf: 'center',
		fontSize: 17
	},
	pagern:{
		flex: 1,
		backgroundColor:globalColorObject.Color.oppPrimary,
	},
	ModalLoaderBackDrop: {
		flex: 1,
		backgroundColor: 'rgba(0,0,0,0.5)',
		justifyContent: 'center',
		alignItems: 'center',
	},
	ModalLoaderContainer: {
		width: '70%',
		height: 100,
		backgroundColor: 'white',
		borderRadius: 5,
		flexDirection: 'row',
	},
	
	InvoiceTabWrapper: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor:globalColorObject.Color.Lightprimary,
	  },
	ModalLoader: {
		flex: 1,
		alignSelf: 'center',
	},
	ModalLoaderText: {
		flex: 2,
		alignSelf: 'center',
		fontSize: 17
	},
	// container: {
	// 	flex: 1,
	// 	justifyContent: 'flex-end',
	// 	// elevation: 5,
	// },
	ScrollViewWrapperContainer: {
		padding:5,
		// flex: 1,
		// paddingVertical: 10,
		// paddingHorizontal: 8,
	},
	SwitchFieldContainer: {
		flexDirection: 'row',
    width:'48%',
		position: 'relative',
    marginTop:15,
    marginLeft:5,
		// borderBottomWidth: 2,
		// borderBottomColor: 'rgba(0,0,0,0.1)',
		paddingVertical: 5,
	},
	FieldContainer: {
		// flexDirection: 'row',
		// position: 'relative',
		// borderBottomWidth: 2,
		// borderBottomColor: 'rgba(0,0,0,0.1)',
		// marginVertical: 5,
	},
	ModalFieldContainer: {
		// flexDirection: 'row',
		// position: 'relative',
		// marginVertical: 5,
	},

	


	FieldLabel: {
		flex: 1,
		alignSelf: 'center',
		fontSize: 12,
		position: 'absolute',
		top: 0,
		left: 5,
		color: 'gray',
	},
	FieldLabelToPay: {
		flex: 1,
		alignSelf: 'center',
	},
	FieldInput: {
		borderBottomWidth: 2,
		borderBottomColor: 'rgba(0,0,0,0.07)',
		width: "100%",
		flex: 1,
		alignSelf: 'center',
	},
	FieldInputMultiline: {
		flex: 2,
		// height: 45,
		alignSelf: 'center',
		textAlign: 'left',
	},
	CustomDatePicker: {
		// borderWidth: 0,
		backgroundColor: 'yellow'
	},
	SaveButtonContainer: {
		flexDirection: 'row',
		position: 'relative',
		justifyContent: 'flex-end',
		backgroundColor: 'rgba(0,0,0,0.025)',
		// backgroundColor: 'skyblue',
		// shadowColor: '#000',
		// shadowOffset: { width: 0, height: 2 },
		// shadowOpacity: 1,
		// shadowRadius: 2,
		// elevation: 5,
		padding: 10,
		// marginBottom: 20,
	},
	SaveButton: {
		width: 90,
		// backgroundColor: 'rgb(85, 114, 125)',
		backgroundColor: '#2091ec',
		// backgroundColor: '#47b5ad',
		padding: 10,
		borderRadius: 5,
		elevation: 5,
	},
	SaveButtonText: {
		fontSize: 17,
		fontWeight: '100',
		textAlign: 'center',
		color: 'white'
	},
});
