// Module Imports
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Platform,
  PermissionsAndroid,
  ScrollView,
  Animated,
  Modal,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CameraScreen} from 'react-native-camera-kit';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {layoutStyle} from '../../Layout/Layout';
import {TextInput} from 'react-native-gesture-handler';
import {
  Request,
  AlertError,
  AlertMessage,
  isnull,
} from '../../../Helpers/HelperMethods';
import {
  globalFontObject,
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';

export default class BarcodeHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Expirationdate: '23/10/2022',
      isFetched: true,
      //   lengthArray: 10,
      show: false,
      width: Dimensions.get('window').width,

      APIData: {
        ItemDetail: '',
        StockDetail: [],
        CategoryDetail: [],
        IcodeDetails: [],
        DupBarcodeList: [],
      },
      // removeHeight:25,
      removeHeight: 0,
      Barcode: '',
      isDupBarcode: false,
      StockPointName: '',
      Qty: '',
      CategoryName1: '',
      CategoryName2: '',
      CategoryName3: '',
      CategoryName4: '',
      CategoryName5: '',
      CategoryName6: '',
      CategoryName7: '',
      CategoryName8: '',
      CategoryName9: '',
      CategoryName10: '',
      CategoryName11: '',
      CategoryName12: '',
      CategoryName13: '',
      CategoryName14: '',
      CategoryName15: '',
      CategoryName16: '',
      Category1: '',
      Category2: '',
      Category3: '',
      Category4: '',
      Category5: '',
      Category6: '',
      Category7: '',
      Category8: '',
      Category9: '',
      Category10: '',
      Category11: '',
      Category12: '',
      Category13: '',
      Category14: '',
      Category15: '',
      Category16: '',

      RCDate: '',
      RCNo: '',

      VendorName: '',
      VendorID: '',
      VendorCity: '',
      SupplierDiscount: '',
      Merchandiser: '',

      Rate: '',
      StdRate: '',
      EffRate: '',
      WSP: '',
      RSP: '',
      ListedMRP: '',
      OnlineMRP: '',
      ScreenValueIcode: '',
      scanText: '',
      Invalidcolor: false,
      CommonForApi: '',
      Found: '',
      iSDates: true,
      isPrice: true,
      isRate: true,
      FreezedContentHeight: new Animated.Value(0),
      FreezedPriceHeight: new Animated.Value(0),
      FreezedRateHeight: new Animated.Value(0),
      ModalVisible: false,
      qrvalue: '',
      openScanner: false,
      DubList: false,
      IcodeTest: '',
      Icode: '',
      Icodes: '',
      height: 0,
      //   FilterLength: 16,
    };
  }
  componentDidMount() {
    this.LoadInitialData();
  }
  LoadInitialData = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    this.GetCaption();
  };
  GetCaption = async () => {
    Request.get(
      `${this.IP}api/Common/GetInventoryCaptionDetails`,
      this.Token,
    ).then(res => {
      this.setState(PrevState => ({
        APIData: {...PrevState.APIData, CategoryDetail: res.data},
      }));
      const result1 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category1',
      );
      const result2 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category2',
      );
      const result3 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category3',
      );
      const result4 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category4',
      );
      const result5 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category5',
      );
      const result6 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category6',
      );
      const result7 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category7',
      );
      const result8 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category8',
      );
      const result9 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category9',
      );
      const result10 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category10',
      );
      const result11 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category11',
      );
      const result12 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category12',
      );
      const result13 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category13',
      );
      const result14 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category14',
      );
      const result15 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category15',
      );
      const result16 = this.state.APIData.CategoryDetail.find(
        ({Category}) => Category === 'Category16',
      );
      this.setState({CategoryName1: result1.DisplayName});
      this.setState({CategoryName2: result2.DisplayName});
      this.setState({CategoryName3: result3.DisplayName});
      this.setState({CategoryName4: result4.DisplayName});
      this.setState({CategoryName5: result5.DisplayName});
      this.setState({CategoryName6: result6.DisplayName});
      this.setState({CategoryName7: result7.DisplayName});
      this.setState({CategoryName8: result8.DisplayName});
      this.setState({CategoryName9: result9.DisplayName});
      this.setState({CategoryName10: result10.DisplayName});
      this.setState({CategoryName11: result11.DisplayName});
      this.setState({CategoryName12: result12.DisplayName});
      this.setState({CategoryName13: result13.DisplayName});
      this.setState({CategoryName14: result14.DisplayName});
      this.setState({CategoryName15: result15.DisplayName});
      this.setState({CategoryName16: result16.DisplayName});
    });
  };
  DuplicateBarcodeAPI = async qrvalues => {
    this.setState({openScanner: false});
    console.log(
      'GetCaption Ip',
      `${
        this.IP
      }/api/Inventory/BarcodeHistory/ValidateScanedBarcode?ScanBarcode=${
        qrvalues ? qrvalues : this.state.scanText
      }`,
    );
    Request.get(
      `${
        this.IP
      }/api/Inventory/BarcodeHistory/ValidateScanedBarcode?ScanBarcode=${
        qrvalues ? qrvalues : this.state.scanText
      }`,
      this.Token,
    )
      .then(res => {
        console.log('duplicate data : ', res.data);
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, IcodeDetails: res.data},
        }));
        this.state.APIData.IcodeDetails.find(e =>
          this.setState({Barcode: e.Barcode, Icode: e.Icode}),
        );
        const BarcodeDetails = this.state.APIData.IcodeDetails.map(
          obj => obj.Icode,
        );
        this.setState({Icodes: BarcodeDetails});

        if (BarcodeDetails.length >= 2) {
          this.setState({DubList: true});
        } else if (qrvalues) {
          this.GeneralApiNew(qrvalues);
        } else {
          this.GeneralApiNew();
        }
      })
      .catch(err => AlertError('No response from the server'));
  };

  GeneralApiNew = async qrvalues => {
    console.log(
      'icode data : ',
      this.state.Icode,
      '',
      `${this.IP}/api/Inventory/BarcodeHistory/GetSelectedIcodeDetailsForMobile?Icode=${this.state.Icode}`,
    );
    Request.get(
      `${this.IP}/api/Inventory/BarcodeHistory/GetSelectedIcodeDetailsForMobile?Icode=${this.state.Icode}`,
      this.Token,
    )
      .then(res => {
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, ItemDetail: res.data.ItemDetail},
        }));
        this.setState(PrevState => ({
          APIData: {...PrevState.APIData, StockDetail: res.data.StockDetail},
        }));
        if (this.state.APIData.StockDetail.length === 0) {
          AlertMessage('Invalid Barcode');
          this.clearAll();
          this.setState({Invalidcolor: true});
        } else {
          const Stoc_qty = this.state.APIData.StockDetail.reduce(
            (accumulator, object) => {
              return accumulator + isnull(object.Qty, 0);
            },
            0,
          );
          console.log('stock qty : ', Stoc_qty);
          this.setState({
            openScanner: false,
            Qty: Stoc_qty,
            StockPointName:
              this.state.APIData.StockDetail?.length == 1
                ? this.state.APIData.StockDetail[0].StockPointName
                : '',
          });
          const result = this.state.APIData.ItemDetail.map(obj => {
            return {
              key: Object.keys(obj),
              val: Object.values(obj),
            };
          });
          console.log('object get Completed');
          result[0].key.map((getind_key, getind_index) => {
            this.setState({[getind_key]: result[0].val[getind_index]});
          });
          console.log('completed object data');
        }
      })
      .catch();
  };
  measureView(event) {
    event.nativeEvent.layout.height < 11
      ? this.setState({height: 210})
      : this.setState({height: event.nativeEvent.layout.height});
    // console.log(event.nativeEvent.layout.height);
    // console.log(this.state.height);
  }
  ToggleCategorys = () => {
    if (this.state.VendorName === '') {
      AlertMessage('Enter or scan Barcode');
    } else {
      this.setState({iSDates: !this.state.iSDates}, () => {
        this.ToggleCategorysDetail();
        Animated.timing(this.state.FreezedContentHeight, {
          toValue: this.state.iSDates ? 0 : this.state.height,
          duration: 500,
        }).start();
      });
    }
  };
  TogglePriceDetailest = () => {
    if (this.state.VendorName === '') {
      AlertMessage('Enter or scan Barcode');
    } else {
      this.setState({isPrice: !this.state.isPrice}, () => {
        this.TogglePriceDetail();
        Animated.timing(this.state.FreezedPriceHeight, {
          toValue: this.state.isPrice ? 0 : 190,
          duration: 500,
        }).start();
      });
    }
  };
  ToggleRateDetailest = () => {
    if (this.state.VendorName === '') {
      AlertMessage('Enter or scan Barcode');
    } else {
      this.setState({isRate: !this.state.isRate}, () => {
        this.ToggleRate();
        Animated.timing(this.state.FreezedRateHeight, {
          toValue: this.state.isRate ? 0 : 70,
          duration: 500,
        }).start();
      });
    }
  };
  ToggleCategorysDetail = () => {
    if (this.state.iSDates === true) {
      return (
        <Image
          style={{width: 20, height: 20}}
          source={require('../../../assets/images/sort_down.png')}
        />
      );
    } else {
      return (
        <Image
          style={{width: 20, height: 20}}
          source={require('../../../assets/images/sort_Up.png')}
        />
      );
    }
  };
  TogglePriceDetail = () => {
    if (this.state.isPrice === true) {
      return (
        <Image
          style={{width: 20, height: 20}}
          source={require('../../../assets/images/sort_down.png')}
        />
      );
    } else {
      return (
        <Image
          style={{width: 20, height: 20}}
          source={require('../../../assets/images/sort_Up.png')}
        />
      );
    }
  };
  onBarcodeScan = qrvalues => {
    const ScanValue = qrvalues;
    this.setState({scanText: ScanValue});
    console.log('Cam Value let', ScanValue);
    this.DuplicateBarcodeAPI(qrvalues);
  };

  onOpenScanner = () => {
    this.clearAll();
    var that = this;
    // To Start Scanning

    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'CameraExample App Camera Permission',
              message: 'CameraExample App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({qrvalue: '', openScanner: true});
            // that.setState({ openScanner: true });
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      this.setState({qrvalue: ''});
      this.setState({openScanner: true});
    }
  };

  clearAll = () => {
    this.setState({
      Barcode: '',
      VendorName: '',
      VendorID: '',
      VendorCity: '',
      SupplierDiscount: '',
      Merchandiser: '',
      StockPointName: '',
      Qty: '',
      CategoryName1: '',
      CategoryName2: '',
      CategoryName3: '',
      CategoryName4: '',
      CategoryName5: '',
      CategoryName6: '',
      CategoryName7: '',
      CategoryName8: '',
      CategoryName9: '',
      CategoryName10: '',
      CategoryName11: '',
      CategoryName12: '',
      CategoryName13: '',
      CategoryName14: '',
      CategoryName15: '',
      CategoryName16: '',
      Category1: '',
      Category2: '',
      Category3: '',
      Category4: '',
      Category5: '',
      Category6: '',
      Category7: '',
      Category8: '',
      Category9: '',
      Category10: '',
      Category11: '',
      Category12: '',
      Category13: '',
      Category14: '',
      Category15: '',
      Category16: '',
      RCDate: '',
      RCNo: '',
      Rate: '',
      StdRate: '',
      EffRate: '',
      WSP: '',
      RSP: '',
      ListedMRP: '',
      OnlineMRP: '',
    });
  };
  ToggleRate = () => {
    if (this.state.isRate === true) {
      return (
        <Image
          style={{width: 20, height: 20, marginRight: 15}}
          source={require('../../../assets/images/sort_down.png')}
        />
      );
    } else {
      return (
        <Image
          style={{width: 20, height: 20, marginRight: 15}}
          source={require('../../../assets/images/sort_Up.png')}
        />
      );
    }
  };
  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={
            this.state.width < 393
              ? styles.closeButtonParent
              : styles.LandscapcloseButtonParent
          }
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  clearInput = () => {
    this.setState({
      scanText: '',
    });
  };
  InfoText = () => {
    if (this.state.scanText.length > 1) {
      var A = this.state.scanText;
      var B = A.toUpperCase();
      this.setState({scanText: B});
      this.DuplicateBarcodeAPI();
      console.log('Scan value', this.state.scanText);
    }
  };

  render() {
    const {navigation} = this.props;

    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }

    if (this.state.openScanner) {
      const handleBarcodeRead = event => {
        this.onBarcodeScan(event.nativeEvent.codeStringValue);
        const ScandedCode = event.nativeEvent.codeStringValue;
        this.setState({scanText: ScandedCode});
      };
      return (
        <View style={styles.modalContainer}>
          <CameraScreen
            actions={{leftButtonText: 'Cancel'}}
            onBottomButtonPressed={() => {
              this.setState({openScanner: false});
            }}
            showFrame={true}
            // style={{height: '100%'}}
            scanBarcode={true}
            // laserColor={'white'}
            // frameColor={'lightgreen'}
            colorForScannerFrame={'black'}
            onReadCode={handleBarcodeRead}
          />
        </View>
      );
    }
    return (
      <View style={styles.Container}>
        <ScrollView>
          <View style={styles.cView}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.DubList}
              onRequestClose={() => {
                alert('Modal has been closed.');
              }}>
              <SafeAreaView style={{height: '100%', width: '100%'}}>
                <View>
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      backgroundColor: globalColorObject.Color.Lightprimary,
                    }}>
                    <View style={styles.StickeyHeaderCard}>
                      <View style={{flexDirection: 'row'}}>
                        <View
                          style={[styles.CardTextContainer, {width: '60%'}]}>
                          <Text
                            numberOfLines={1}
                            style={[
                              styles.cardTextFieldHeader,

                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Bold,
                                globalFontObject.Size.small.sl,
                                globalColorObject.Color.oppPrimary,
                                globalColorObject.ColorPropetyType.Color,
                              ),
                            ]}>
                            Barcode
                          </Text>
                        </View>
                        <View
                          style={[styles.CardTextContainer, {width: '40%'}]}>
                          <Text
                            numberOfLines={1}
                            style={[
                              styles.cardTextFieldHeader,
                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Bold,
                                globalFontObject.Size.small.sl,
                                globalColorObject.Color.oppPrimary,
                                globalColorObject.ColorPropetyType.Color,
                              ),
                            ]}>
                            Icode
                          </Text>
                        </View>
                      </View>
                      <View style={[styles.CardTextContainer, {width: '100%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.cardTextFieldHeader,
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          Description
                        </Text>
                      </View>
                    </View>
                    {this.state.APIData.IcodeDetails.map((person, index) => (
                      <View
                        key={`icodedet_${index}`}
                        style={{
                          borderWidth: 2,
                          borderColor: 'white',

                          color: 'white',
                          margin: 5,
                          borderRadius: 15,
                          padding: 5,
                          backgroundColor: globalColorObject.Color.oppPrimary,
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.clearAll();
                            this.setState({
                              scanText: person.Icode,
                              Icode: person.Icode,
                              DubList: false,
                            });
                            this.GeneralApiNew(person.Icode);
                          }}>
                          <View style={{flexDirection: 'row'}}>
                            <Text
                              // key={index}
                              style={[
                                styles.CardTextContainer,
                                {width: '60%'},

                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                ),
                              ]}>
                              {person.Barcode}
                            </Text>
                            <Text
                              // key={index}
                              style={[
                                styles.CardTextContainer,
                                {width: '40%'},
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                ),
                              ]}>
                              {person.Icode}
                            </Text>
                          </View>
                          <Text
                            // key={index}
                            style={[
                              styles.CardTextContainer,
                              {width: '100%'},

                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Regular,
                                globalFontObject.Size.small.sl,
                              ),
                            ]}>
                            {person.Description}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                  </View>
                </View>
              </SafeAreaView>
            </Modal>
          </View>
          <View style={[styles.SearhBarWrapper, {flexDirection: 'row'}]}>
            <TextInput
              style={[
                styles.SearchInput,
                layoutStyle.FieldInput,
                {
                  borderBottomColor: globalColorObject.Color.Primary,
                  borderBottomWidth: 1,
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                {width: '88%'},
              ]}
              onChangeText={scanText => this.setState({scanText})}
              value={this.state.scanText}
              autoCapitalize="characters"
              returnKeyType="next"
              autoCorrect={false}
              onBlur={this.InfoText}
              placeholder="Scan Here"
              placeholderTextColor="#AFAFAF"
              onFocus={this.showCancel}
            />
            {this.renderTouchableHighlight()}
            <TouchableOpacity
              fieldWrapperStyle={{height: 30, width: '10%', paddingTop: 5}}
              onPress={() => this.onOpenScanner()}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  marginTop: 8,
                  marginLeft: 5,
                }}
                source={require('../../../assets/images/ScanBarcode.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.StockPosition}>
            <Text style={{paddingTop: 8, fontWeight: 'bold'}}>
              {this.state.StockPointName === ''
                ? 'Total Stock'
                : this.state.StockPointName}
            </Text>
            <Text style={{paddingTop: 2, fontWeight: 'bold', fontSize: 22}}>
              {this.state.Qty === '' ? 'Total Qty' : this.state.Qty}
            </Text>
            <View style={styles.cView}>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.ModalVisible}
                onRequestClose={() => {
                  Alert('Modal has been closed.');
                }}>
                <View style={{height: '100%', width: '100%'}}>
                    <View
                      style={{
                        width: '100%',
                        height: '100%',
                        backgroundColor: 'white',
                      }}>

                        {(this.state.APIData.StockDetail || []).map((item , index) => {
                          return (
                            
                      <View key={item.StockPointName} style={{width: '100%', flexDirection: 'row'}}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          width: '60%',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        {item.StockPointName}
                      </Text>
                      <Text
                        style={{
                          width: '40%',
                          paddingTop: 2,
                          paddingBottom: 2,
                          textAlign: 'center',
                        }}>
                        {item.Qty}
                      </Text>
                    </View>
                          )
                        } )

                        }
                      <TouchableHighlight
                        style={{
                          backgroundColor: '#2196F3',
                          width: '100%',
                          alignContent: 'center',
                        }}
                        onPress={() => {
                          this.setState({ModalVisible: false});
                        }}>
                        <Text style={styles.CloseStyle}>Close</Text>
                      </TouchableHighlight>
                    </View>
                </View>
              </Modal>

              <TouchableOpacity
                onPress={() => {
                  this.setState({ModalVisible: true});
                }}>
                <Image
                  style={{
                    width: 25,
                    height: 30,
                    marginRight: 15,
                    margin: 2,
                    alignSelf: 'flex-end',
                  }}
                  source={require('../../../assets/images/add_shopping_cart.png')}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.ItemCard}>
            <View
              style={{
                width: '100%',
                borderBottomWidth: 1,
                borderColor: 'rgba(0,0,0,0.15)',
                flexDirection: 'row',
              }}>
              <Text style={styles.ItemInfo}>Item Information</Text>
              <Text
                style={
                  this.state.Invalidcolor
                    ? styles.BarcodeInfo
                    : styles.InvalidBarcode
                }>
                {this.state.scanText}
              </Text>
            </View>
            <View style={{width: '100%'}}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Vendor Name
                </Text>
                <Text style={{width: '49%', paddingTop: 2, paddingBottom: 2}}>
                  {this.state.VendorName.slice(0, 20)}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Vendor City
                </Text>
                <Text style={{width: '49%', paddingTop: 2, paddingBottom: 2}}>
                  {this.state.VendorCity}
                </Text>
              </View>

              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Merchandiser
                </Text>
                <Text style={{width: '49%', paddingTop: 2, paddingBottom: 2}}>
                  {this.state.Merchandiser}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Expiration date
                </Text>
                <Text style={{width: '49%', paddingTop: 2, paddingBottom: 2}}>
                  {this.state.VendorName === ''
                    ? ''
                    : this.state.Expirationdate}
                </Text>

                {/* <Text style={{width:"49%",paddingTop:2,paddingBottom:2}} >{moment(this.state.Expirationdate).format('DD-MM-YYYY')}</Text> */}
              </View>
              {/* <View style={{width:"100%",flexDirection:'row'}}>
			<Text style={{fontWeight:'bold',width:"50%",paddingTop:2,paddingBottom:2}}>Merchandiser</Text>
			<Text style={{width:"49%",paddingTop:2,paddingBottom:2}} >{this.state.Merchandiser}</Text>
			</View> */}
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={this.ToggleCategorys}
              style={styles.TouchableText}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Image
                  style={{width: 20, height: 35, width: '10%', marginRight: 10}}
                  source={require('../../../assets/images/popup-blue.png')}
                />
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: 'blue',
                    fontSize: 20,
                    width: '80%',
                  }}>
                  Categories
                </Text>
                {this.ToggleCategorysDetail()}
              </View>
            </TouchableOpacity>
            <Animated.View
              onLayout={event => this.measureView(event)}
              style={[
                styles.CardWrapper1,
                {height: this.state.FreezedContentHeight},
              ]}>
              <View
                style={
                  this.state.Category1 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName1 === ''
                    ? 'Categorie 1'
                    : this.state.CategoryName1}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category1}</Text>
              </View>
              <View
                style={
                  this.state.Category2 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName2 === ''
                    ? 'Categorie 2'
                    : this.state.CategoryName2}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category2}</Text>
              </View>
              <View
                style={
                  this.state.Category3 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName3 === ''
                    ? 'Categorie 3'
                    : this.state.CategoryName3}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category3}</Text>
              </View>
              <View
                style={
                  this.state.Category4 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName4 === ''
                    ? 'Categorie 4'
                    : this.state.CategoryName4}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category4}</Text>
              </View>
              <View
                style={
                  this.state.Category5 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName5 === ''
                    ? 'Categorie 5'
                    : this.state.CategoryName5}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category5}</Text>
              </View>
              <View
                style={
                  this.state.Category6 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName6 === ''
                    ? 'Categorie 6'
                    : this.state.CategoryName6}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category6}</Text>
              </View>
              <View
                style={
                  this.state.Category7 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName7 === ''
                    ? 'Categorie 7'
                    : this.state.CategoryName7}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category7}</Text>
              </View>
              <View
                style={
                  this.state.Category8 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName8 === ''
                    ? 'Categorie 8'
                    : this.state.CategoryName8}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category8}</Text>
              </View>
              <View
                style={
                  this.state.Category9 != '' ? styles.loaded : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName9 === ''
                    ? 'Categorie 9'
                    : this.state.CategoryName9}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category9}</Text>
              </View>
              <View
                style={
                  this.state.Category10 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName10 === ''
                    ? 'Categorie 10'
                    : this.state.CategoryName10}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category10}</Text>
              </View>
              <View
                style={
                  this.state.Category11 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName11 === ''
                    ? 'Categorie 11'
                    : this.state.CategoryName11}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category11}</Text>
              </View>
              <View
                style={
                  this.state.Category12 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName12 === ''
                    ? 'Categorie 12'
                    : this.state.CategoryName12}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category12}</Text>
              </View>
              <View
                style={
                  this.state.Category13 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName13 === ''
                    ? 'Categorie 13'
                    : this.state.CategoryName13}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category13}</Text>
              </View>
              <View
                style={
                  this.state.Category14 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName14 === ''
                    ? 'Categorie 14'
                    : this.state.CategoryName14}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category14}</Text>
              </View>
              <View
                style={
                  this.state.Category15 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName15 === ''
                    ? 'Categorie 15'
                    : this.state.CategoryName15}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category15}</Text>
              </View>
              <View
                style={
                  this.state.Category16 != ''
                    ? styles.loaded
                    : {display: 'none'}
                }>
                <Text style={{fontWeight: 'bold', width: '50%'}}>
                  {this.state.CategoryName16 === ''
                    ? 'Categorie 16'
                    : this.state.CategoryName16}
                </Text>
                <Text style={{width: '49%'}}>{this.state.Category16}</Text>
              </View>
            </Animated.View>
          </View>
          <View>
            <TouchableOpacity
              onPress={this.TogglePriceDetailest}
              style={styles.TouchableText}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Image
                  style={{width: 20, height: 35, width: '10%', marginRight: 10}}
                  source={require('../../../assets/images/discount.png')}
                />
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: 'blue',
                    fontSize: 20,
                    width: '80%',
                  }}>
                  Price Details
                </Text>
                {this.TogglePriceDetail()}
              </View>
            </TouchableOpacity>
            <Animated.View
              style={
                (styles.CardWrapper, {height: this.state.FreezedPriceHeight})
              }>
              {/* <View style={styles.CardWrapper}> */}
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Rate
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.Rate}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Std Rate
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.StdRate}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Eff Rate
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.EffRate}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  WSP
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.WSP}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  RSP
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.RSP}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Listed MRP
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.ListedMRP}
                </Text>
              </View>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    width: '50%',
                    paddingTop: 2,
                    paddingBottom: 2,
                  }}>
                  Online MRP
                </Text>
                <Text
                  style={{
                    width: '49%',
                    paddingTop: 2,
                    paddingBottom: 2,
                    textAlign: 'right',
                  }}>
                  {this.state.OnlineMRP}
                </Text>
              </View>
              {/* </View> */}
            </Animated.View>
          </View>
          {/* <TouchableOpacity
		onPress={this.ToggleRateDetailest}
		style={styles.TouchableText}>
	<View style={{width:"100%",flexDirection:'row'}}>
	<Image
					style={{ width: 20, height: 35 ,width:'10%',marginRight:10}}
				source={require('../../../assets/images/RateChange.png')}
				/>

		<Text style={{fontWeight:'bold',color:'blue',fontSize:20,width:'80%',justifyContent:'space-evenly'}}>
		Rate Change</Text>
		{this.ToggleRate()}

	</View>
	</TouchableOpacity>
	<Animated.View style={{ height: this.state.FreezedRateHeight }}>
			<View style={styles.CardWrapper}>
			<View style={{width:"100%",flexDirection:'row'}}>
			<Text style={{fontWeight:'bold',width:"50%",paddingTop:2,paddingBottom:2}}>Rc No</Text>
			<Text style={{width:"49%",paddingTop:2,paddingBottom:2}} >{this.state.RCNo}</Text>
			</View>
			<View style={{width:"100%",flexDirection:'row'}}>
			<Text style={{fontWeight:'bold',width:"50%",paddingTop:2,paddingBottom:2}}>Rc Date</Text>
			<Text style={{width:"49%",paddingTop:2,paddingBottom:2}} >{this.state.RCDate.toString().slice(0,10)}</Text>
			</View>
		</View>
			</Animated.View> */}
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('TransctionDetailNav', {
                Icode: this.state.scanText,
              })
            }
            style={{
              textAlign: 'center',
              // borderBottomWidth:1,
              // borderColor:'#D3D3D3',
              width: '100%',
              backgroundColor: 'white',
              // borderRadius:10,
              justifyContent: 'space-around',
              // margin:5,
              padding: 5,
              marginBottom: 5,
            }}>
            <View style={{width: '100%', flexDirection: 'row'}}>
              <Image
                style={{width: 20, height: 35, width: '10%', marginRight: 10}}
                source={require('../../../assets/images/TD.png')}
              />

              <Text
                style={{
                  fontWeight: 'bold',
                  color: 'blue',
                  fontSize: 20,
                  width: '78%',
                  justifyContent: 'space-evenly',
                }}>
                Transaction Details
              </Text>
              <Image
                style={{width: 10, height: 25, width: '10%', marginTop: 2}}
                source={require('../../../assets/images/RightNav.png')}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>

        <TouchableOpacity
          style={[
            styles.AddNewInvoice,
            ApplyStyleFontAndSizeAndColor(
              globalColorObject.Color.Primary,
              globalColorObject.ColorPropetyType.backgroundColor,
            ),
          ]}
          onPress={() => {
            this.clearAll();
          }}>
          <Image
            style={{width: 30, height: 30}}
            source={require('../../../assets/images/reset-light.png')}
          />
        </TouchableOpacity>
        {/* <View style={{width: '100%',bottom:0,alignSelf:'flex-end',justifyContent: 'space-between',flexDirection:'row'}}> */}

        {/* </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  closeButton: {
    height: 30,
    width: 30,
    marginLeft: 10,
    marginTop: 4,
  },

  LandscapcloseButtonParent: {
    position: 'absolute',
    right: 60,
    top: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 45,
    top: 10,
    backgroundColor: globalColorObject.Color.Primary,
  },
  loaded: {
    width: '100%',
    flexDirection: 'row',
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    backgroundColor: globalColorObject.Color.Primary,
  },

  modalContainer: {
    flex: 1,
  },
  CardLine: {
    alignSelf: 'center',
  },
  Container: {
    backgroundColor: globalColorObject.Color.Lightprimary,
    flex: 1,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderRadius: 5,
    borderWidth: 1,

    borderBottomColor: globalColorObject.Color.Primary,
  },
  StockPosition: {
    flexDirection: 'row',
    backgroundColor: 'white',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingLeft: 10,
    borderWidth: 1,
    margin: 4,
    padding: 2,
    borderRadius: 15,
    marginTop: 10,
  },

  ItemInfo: {
    width: '75%',
    color: 'blue',
    fontWeight: 'bold',
    paddingBottom: 5,
    // textAlign:'center',
    fontSize: 18,
  },
  BarcodeInfo: {
    fontSize: 18,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: 'red',
  },
  InvalidBarcode: {
    fontSize: 18,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: 'green',
  },
  ItemCard: {
    backgroundColor: 'white',
    borderColor: 'rgba(0,0,0,0.15)',
    borderRadius: 10,
    margin: 2,
    padding: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    margin: 8,
  },

  CardWrapper1: {
    width: '99%',
    backgroundColor: 'white',
    borderRadius: 10,
    justifyContent: 'space-around',
    margin: 2,
  },
  CardWrapper: {
    width: '99%',
    backgroundColor: 'white',
    borderRadius: 10,
    justifyContent: 'space-around',
    padding: 2,
    margin: 2,
  },
  TouchableText: {
    textAlign: 'center',
    // borderBottomWidth:1,
    // borderColor:'#D3D3D3',
    width: '100%',
    backgroundColor: 'white',
    // borderRadius:10,
    justifyContent: 'space-around',
    // margin:5,
    padding: 5,
    marginBottom: 5,
  },
  ScanWrapper: {
    padding: 10,
    backgroundColor: '#ededed',
  },
  TouchableImage: {
    // textAlign: 'center',
    // width: '100%',
    padding: 5,
    flexDirection: 'row',
  },
  cView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  CloseStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    height: 35,
    padding: 5,
  },
  TStyle: {
    color: 'black',
    fontWeight: 'bold',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  StickeyHeaderCard: {
    backgroundColor: globalColorObject.Color.Primary,

    margin: 5,
  },
  SearhBarWrapper: {
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
    width: '100%',
  },
  CardTextContainer: {
    width: '50%',
    marginVertical: 3,
  },
  cardTextFieldHeader: {
    fontSize: 16,
    color: 'white',
    paddingLeft: 10,
  },
});
