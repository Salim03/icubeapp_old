import React, { PureComponent } from 'react';
import {
	View, SafeAreaView, Text, StyleSheet, ScrollView, Image, TouchableOpacity
} from 'react-native';
import { getFormsForModule, AlertError, getPropsFromArray } from '../../../Helpers/HelperMethods';


export default class ProcurenmentHome extends PureComponent {

	state = {
		forms: []
	};

	componentDidMount() {
		getFormsForModule(3)
			.then(data => {
				const forms = data && getPropsFromArray(data, ["MENUNAME"])["MENUNAME"].map(elem => elem.replace(/ /g, ""));
				this.setState({ forms });
			})
			.catch(AlertError);
	}

	NavigateToForm = (nav, HeaderTitle, CallFrom, FormIndex) => {
		this.props.navigation.navigate(nav, {
			HeaderTitle,
			CallFrom,
			FormIndex
		});
	}

	FormPartyMaster = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "BussinessPartner", "Bussiness Partner","3.10.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/BussinessPartner.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Bussiness</Text>
				<Text style={styles.FormLabel}>Partner</Text>
			</TouchableOpacity>
		)
	}

	FormPurchaseOrder = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "PurchaseNav", "Purchase Order", "PO","3.1.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/purchase.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Purchase</Text>
				<Text style={styles.FormLabel}>Order</Text>
			</TouchableOpacity>
		)
	}

	FormGoodsReceivedNote = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "PurchaseNav", "Goods Received Note", "GRN","3.2.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/grn.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Goods</Text>
				<Text style={styles.FormLabel}>Received</Text>
				<Text style={styles.FormLabel}>Note</Text>
			</TouchableOpacity>
		)
	}

	FormGoodsReturnNote = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "PurchaseNav", "Goods Return Note", "GRT","3.3.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/purchase.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Goods</Text>
				<Text style={styles.FormLabel}>Return</Text>
				<Text style={styles.FormLabel}>Note</Text>
			</TouchableOpacity>
		)
	}

	FormPurchaseInvoice = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "PurchaseNav", "Purchase Invoice", "PI","3.4.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/purchase.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Purchase</Text>
				<Text style={styles.FormLabel}>Invoice</Text>
			</TouchableOpacity>
		)
	}

	FormDebitNote = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "PurchaseNav", "Debit Note", "DN","22.11.0")}>
				<Image
					source={require('../../../assets/images/Home/Procurement/purchase.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Debit</Text>
				<Text style={styles.FormLabel}>Note</Text>
			</TouchableOpacity>
		)
	}

	FormVendorOutStanding = key => {
		return (
			<TouchableOpacity key={key} style={styles.FormContainer} onPress={this.NavigateToForm.bind(this, "SupplierHistoryNav", "Vendor History", "Supplier","3.12.0")}>
				<Image
					source={require('../../../assets/images/Home/common/vendor-history.png')}
					style={styles.FormImages}
				/>
				<Text style={styles.FormLabel}>Vendor</Text>
				<Text style={styles.FormLabel}>History</Text>
			</TouchableOpacity>
		)
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<View style={styles.LoadContainer}>
					<ScrollView style={styles.DataContainer}>
						<View style={styles.ListContainer}>
							{this.state.forms?.map(form => this[`Form${form}`]?.(form))}
						</View>
					</ScrollView>
				</View>
				<View style={{ backgroundColor: 'rgba(0,0,0,0)', position: 'absolute', bottom: 0, right: 0 }}>
					{/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
					<View style={{ alignItems: 'flex-end', marginRight: 20, flexDirection: 'row', marginBottom: 5 }}>
						<Text style={{ textAlign: 'right' }}>Powered By</Text>
						<Image
							source={require('../../../assets/images/Drawer/Logo.png')}
							style={{ height: 30, width: 100 }}
						/>
					</View>
				</View>
			</SafeAreaView>
		);
	}
};

const styles = StyleSheet.create({
	Text: {
		marginTop: '80%',
		textAlign: 'center',
		fontSize: 22,
		fontWeight: 'bold',
	},
	LoadContainer: {
		// backgroundColor: 'rgb(43, 54, 255)',
		backgroundColor: '#69e781',
		flex: 1
	},
	FormImages: {
		width: 30,
		height: 30,
		marginLeft: 5,
		marginRight: 5,
		marginBottom: 5,
		marginTop: 5
	},
	FormLabel: {
		textAlign: 'center',
		fontSize: 15,
		// fontWeight: 'bold',
	},
	DataContainer: {
		backgroundColor: 'white',
		marginTop: 30,
		// marginLeft: 5,
		// marginRight: 5,
		borderTopLeftRadius: 35,
		borderTopRightRadius: 35,
		flex: 1,
		width: '100%',
		elevation: 50,
	},
	ListContainer: {
		marginTop: 10,
		// marginLeft:20,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		width: '100%',
		marginBottom: 5,
		flexWrap: 'wrap',
	},
	FormContainer: {
		// backgroundColor: 'rgb(252, 172, 182)',
		backgroundColor: 'white',
		marginBottom: 10,
		padding: 5,
		height: 120,
		width: 100,
		alignItems: 'center',
		borderRadius: 10,
		elevation: 15,
	},
});