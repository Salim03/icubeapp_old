/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import moment from 'moment';
// Local Imports
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {
  Request,
  AlertStatusError,
  AlertError,
  sharefileForCallFRom,
  GetSessionData,
  AlertMessage,
  DeleteSelectedInvoice,
} from '../../../Helpers/HelperMethods';
import LayoutWrapper, {CardStyle} from '../../Layout/Layout';

import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
} from '../../../Style/GlobalStyle';

const ListItemHeader = ({isPO}) => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, CardItemLayout]}>
        <View style={[styles.CardTextContainer, {width: '75%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Vendor
          </Text>
        </View>

        {isPO && (
          <View style={[styles.CardTextContainer, {width: '25%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),

                {textAlign: 'right'},
              ]}>
              Status
            </Text>
          </View>
        )}

        <View style={[styles.CardTextContainer, {width: '35%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Invoice
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '35%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Doc
          </Text>
        </View>

        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),

              {textAlign: 'right'},
            ]}>
            Amount
          </Text>
        </View>
      </View>
    </View>
  );
};

const ListItem = props => {
  const data = props.data;
  let FullInvoiceDate = new Date(data.InvoiceDate),
    FullDocDate = data.DOCDATE ? new Date(data.DOCDATE) : '',
    FullAgainstDate =
      data.AgainstDate || data.AgainstDate1
        ? new Date(data.AgainstDate || data.AgainstDate1)
        : '';
  let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
    '0' +
    (FullInvoiceDate.getMonth() + 1)
  ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
  let DocDate = FullDocDate
    ? `${('0' + FullDocDate.getDate()).slice(-2)}/${(
        '0' +
        (FullDocDate.getMonth() + 1)
      ).slice(-2)}/${FullDocDate.getFullYear()}`
    : '';
  let AgainstDate = FullAgainstDate
    ? `${('0' + FullAgainstDate.getDate()).slice(-2)}/${(
        '0' +
        (FullAgainstDate.getMonth() + 1)
      ).slice(-2)}/${FullAgainstDate.getFullYear()}`
    : '';
  const {CardItemLayout} = CardStyle;
  let statusvalue = data.StatusFlag;
  let statuscolor =
    statusvalue == 2 //'Approved'
      ? 'blue'
      : statusvalue == 4 //'Delivered'
      ? 'maroon'
      : statusvalue == 3 || statusvalue == 4 //statusvalue == 'In Progress' || statusvalue == 'Partial Delivered'
      ? '#684E53'
      : statusvalue == 5 //'Canceled'
      ? 'red'
      : statusvalue == 1 //'Open'
      ? 'darkgreen'
      : statusvalue == 7 //'Hold'
      ? 'gold'
      : 'black';
  return (
    <TouchableWithoutFeedback
      onLongPress={() => props.onLongPress(data.ID, data.InvoiceNo)}>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View
            style={[
              styles.CardTextContainer,
              {
                width: props.isPO ? '75%' : '100%',
              },
            ]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.VENDORNAME}
            </Text>
          </View>
          {props.isPO && (
            <ModalSearchableLabel
              placeholder={
                data.Status && data.Status != '' ? data.Status : 'Status'
              }
              fieldWrapperStyle={[
                styles.CardTextContainer,
                {width: '25%', marginHorizontal: 0, marginVertical: 5},
              ]}
              data={props.ChangeStatusDataList(data.StatusFlag)}
              labelProp="label"
              valueProp="value"
              selectedValue={data.StatusFlag}
              inputStyle={[
                {
                  marginTop: 0,
                  paddingBottom: 0,
                  borderBottomWidth: 0,
                  paddingTop: 0,
                  padding: 0,
                  marginHorizontal: 0,
                  paddingHorizontal: 0,
                  marginBottom: 0,
                  height: 'auto',
                  textAlign: 'right',
                  color: statuscolor,
                },
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                ),
              ]}
              onValueSelected={changevalue =>
                props.ChangeStatus(changevalue, data.ID)
              }
            />
          )}
          <View style={[styles.CardTextContainer, {width: '35%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.InvoiceNo} {InvoiceDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '35%'}]}>
            <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {data.DOCNO} {DocDate}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '30%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'right'},
              ]}>
              {(Math.round(data.NetAmount * 100) / 100).toFixed(2)}
            </Text>
          </View>
        </View>

        <View
          style={[
            {
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              backgroundColor: globalColorObject.Color.Lightprimary,
            },
          ]}>
          <View
            style={[
              {
                width: 0,
                height: 0,
                backgroundColor: globalColorObject.Color.Lightprimary,
                borderRightWidth: 20,
                borderTopWidth: 20,
                borderRightColor: globalColorObject.Color.Lightprimary,
                borderTopColor: globalColorObject.Color.oppPrimary,
              },
              {
                transform: [{rotate: '90deg'}],
              },
            ]}></View>

          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                data.ID,
                data.InvoiceNo,
                moment(new Date(InvoiceDate)).format('YYYY-MM-DD'),
                true,
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/128/3022/3022251.png',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => props.onDeleteInvoice(data.ID)}
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/3096/3096673.png',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                data.ID,
                data.InvoiceNo,
                moment(new Date(InvoiceDate)).format('YYYY-MM-DD'),
                false,
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1059/1059106.png',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class Purchase extends Component {
  constructor() {
    super();
    this.LocalData = {
      Title: '',
      CallFrom: '',
      Loccode: 0,
      FormIndex: '',
      RoleId: '',
      UserCode: '',
      FinancialYearId: '0',
    };
    this.state = {
      isFetched: false,
      POList: [],
      ListSourceData: [],
      StatusGroupData: [],
      SelectedStatus: 'All',
      NoDataAvailable: false,
      SearchText: '',
      show: false,
    };
  }

  componentDidMount() {
    let params = this.props.route.params;
    this.LocalData.Title = params.HeaderTitle || 'Purchase Order';
    this.LocalData.CallFrom = params.CallFrom || 'PO';
    this.LocalData.FormIndex = params.FormIndex || '3.1.0';
    this.props.navigation.setOptions({
      title: this.LocalData.Title,
    });
    this.props.navigation.setOptions({
      headerRight: () =>
        this.LocalData.FormIndex == '3.1.0' && (
          <View style={{flexDirection: 'row', paddingRight: 10}}>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 8}}
              onPress={() => {
                this.props.navigation.navigate('OffLine_PurchaseOrder', {
                  CallFrom: this.LocalData.CallFrom,
                  HeaderTitle: this.LocalData.Title,
                });
              }}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/Offline.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{width: 30, height: 30, marginHorizontal: 8}}
              onPress={() => {
                this.props.navigation.navigate('OffLine_ImportData');
              }}>
              <Image
                style={{width: 30, height: 30}}
                source={require('../../../assets/images/popup-blue1.png')}
              />
            </TouchableOpacity>
          </View>
        ),
    });
    this.LoadInitialData();
  }

  ChangeStatus = (changestatus, invoiceid) => {
    this.setState({isFetched: false});
    Request.post(
      `${this.IP}/api/PoApproval/ApprovalSave?PassID=${invoiceid}^&PassType=${changestatus}&ID=0&LocCode=${this.LocalData.Loccode}&FinancialID=${this.LocalData.FinancialYearId}&User=${this.LocalData.UserCode}`,
      null,
      this.Token,
    )
      .then(res => res.json())
      .then(json => {
        if (json && json.Message == 'Saved') {
          AlertMessage(`Status updated successfully`, this.LoadAllInvoice);
        } else {
          AlertMessage('Failed to update status');
        }
        this.setState({isFetched: true});
      })
      .catch(err => {
        this.setState({isFetched: true});
      });
  };

  EnableLoadScreen = () => {
    this.setState({isFetched: false});
  };

  onDeleteSelectedInvoice = async invoiceid => {
    await DeleteSelectedInvoice(
      this.LocalData.CallFrom,
      this.LocalData.FormIndex,
      invoiceid,
      this.LocalData.Loccode,
      this.LocalData.RoleId,
      this.LocalData.UserCode,
      null, //this.EnableLoadScreen,
      this.LoadAllInvoice,
    );
    // this.DsableLoadScreen();
  };

  ChangeStatusDataList = passstatusflag => {
    return passstatusflag == 1 // Open
      ? [
          {label: 'Approve', value: 'Approval'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : passstatusflag == 2 // Approved
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : passstatusflag == 3 // Receiving
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Close', value: 'Close'},
        ]
      : passstatusflag == 7 // Hold
      ? [{label: 'Release', value: 'Release'}]
      : [];
  };

  LoadInitialData = async () => {
    const {
      get_IP,
      get_Token,
      get_UserCode,
      get_Location,
      get_FinancialYearId,
      get_RoleId,
    } = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.Loccode = get_Location;
    this.LocalData.RoleId = get_RoleId;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.FinancialYearId = get_FinancialYearId;
    this.LoadAllInvoice();
  };

  LoadAllInvoice = () => {
    Request.get(
      `${this.IP}/api/Purchase/GetAllInvoice?CallFrom=${this.LocalData.CallFrom}&_isShowOnlySAB=0&LocCode=${this.LocalData.Loccode}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            let GetresData = res.data;
            let GroupStatusOut = [];
            if (this.LocalData.CallFrom == 'PO') {
              let groupData = {};
              GetresData.reduce((r, o) => {
                let key = o.Status;
                if (!groupData[key]) {
                  groupData[key] = Object.assign(
                    {},
                    {
                      Status: o.Status,
                      Count: 1,
                    },
                  ); // create a copy of o
                  // r.push(groupData[key]);
                } else {
                  groupData[key].Count += 1;
                }
              }, []);
              let TotalCount = GetresData.length;
              GroupStatusOut.push(
                Object.assign(
                  {},
                  {
                    Status: 'All'.toString(),
                    Count: TotalCount,
                    Label: 'All' + ` (${TotalCount})`,
                  },
                ),
              );
              Object.keys(groupData).map(key => {
                // console.log(key, groupData[key], groupData[key].Count);
                GroupStatusOut.push(
                  Object.assign(
                    {},
                    {
                      Status: key.toString(),
                      Count: groupData[key].Count,
                      Label: key + ` (${groupData[key].Count})`,
                    },
                  ),
                );
              });
            }
            this.setState({
              isFetched: true,
              POList: GetresData,
              ListSourceData: GetresData,
              StatusGroupData: GroupStatusOut,
              SelectedStatus: 'All',
            });
          } else {
            this.setState({isFetched: true, NoDataAvailable: true});
          }
        } else {
          AlertStatusError(res);
          this.setState({isFetched: true});
        }
      })
      .catch(err => AlertError(err) && this.setState({isFetched: true}));
  };

  HandleSearch = async SearchText => {
    this.setState({SearchText});
    let POList = this.state.POList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = POList.filter(obj => {
      // Get all the texts
      let FullInvoiceDate = new Date(obj.InvoiceDate),
        FullDocDate = obj.DOCDATE ? new Date(obj.DOCDATE) : '',
        FullAgainstDate = obj.AgainstDate ? new Date(obj.AgainstDate) : '';
      let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
        '0' +
        (FullInvoiceDate.getMonth() + 1)
      ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
      let DocDate = FullDocDate
        ? `${('0' + FullDocDate.getDate()).slice(-2)}/${(
            '0' +
            (FullDocDate.getMonth() + 1)
          ).slice(-2)}/${FullDocDate.getFullYear()}`
        : '';
      let AgainstDate = FullAgainstDate
        ? `${('0' + FullAgainstDate.getDate()).slice(-2)}/${(
            '0' +
            (FullAgainstDate.getMonth() + 1)
          ).slice(-2)}/${FullAgainstDate.getFullYear()}`
        : '';
      let InvoiceNo = (obj.InvoiceNo || '').toString().toLowerCase();
      let Status = (obj.Status || '').toString().toLowerCase();
      let VENDORNAME = (obj.VENDORNAME || '').toString().toLowerCase();
      let DOCNO = (obj.DOCNO || '').toString().toLowerCase();
      let AgainstNo = (obj.AgainstNo || '').toString().toLowerCase();
      let AgainstNo1 = (obj.AgainstNo1 || '').toString().toLowerCase();
      let Amount = (Math.round(obj.NetAmount * 100) / 100)
        .toFixed(2)
        .toString();

      return (
        InvoiceDate.includes(txt) ||
        DocDate.includes(txt) ||
        AgainstDate.includes(txt) ||
        InvoiceNo.includes(txt) ||
        Status.includes(txt) ||
        VENDORNAME.includes(txt) ||
        DOCNO.includes(txt) ||
        AgainstNo.includes(txt) ||
        AgainstNo1.includes(txt) ||
        Amount.includes(txt)
      );
    });
    this.setState({ListSourceData});
  };
  _get = async url => {
    const res = await fetch(url, {
      headers: {
        Authorization: this.Token,
      },
    });
    const json = await res.json();
    return {
      data: json,
      status: res.status,
    };
  };

  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  clearInput = () => {
    this.HandleSearch('');
  };
  NavigateToPO = (InvoiceID, InvoiceNO) => {
    this.props.navigation.navigate('AddPurchaseNav', {
      CallFrom: this.LocalData.CallFrom,
      HeaderTitle: this.LocalData.Title,
      InvoiceID,
      InvoiceNO,
      onUpdateInvoice: this.LoadAllInvoice,
    });
  };

  CallShareFile = async (InvoiceID, InvoiceNO, InvoiceDate, isPrint) => {
    this.HandleFetchFlag(false);
    await sharefileForCallFRom(
      this.LocalData.CallFrom,
      InvoiceID,
      InvoiceNO,
      InvoiceDate,
      this.LocalData.FormIndex,
      this.LocalData.Loccode,
      isPrint,
    );
    this.HandleFetchFlag(true);
  };

  HandleFetchFlag = isFetched => {
    this.setState({isFetched: isFetched});
  };

  refreshLayout = () => {
    /*this.forceUpdate();*/
  };
  render() {
    console.log(this.state.SelectedStatus, 'Fetching Status');
    let isDoubleCard = false;
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    return (
      <>
        <LayoutWrapper
          onLayoutChanged={this.refreshLayout}
          backgroundColor={globalColorObject.Color.Lightprimary}>
          <View
            style={[
              styles.SearhBarWrapper,
              {
                //paddingBottom: this.LocalData.CallFrom == 'PO' ? 0 : 10,
                width: '100%',
                flexDirection: 'row',
              },
            ]}>
            <TextInput
              style={[
                styles.SearchInput,
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                {width: this.LocalData.CallFrom == 'PO' ? '70%' : '100%'},
              ]}
              placeholder={'Search here'}
              value={this.state.SearchText}
              onChangeText={this.HandleSearch}
              autoCorrect={false}
              returnKeyType="search"
              autoCapitalize="none"
              onFocus={this.showCancel}
              onBlur={this.hideCancel}
            />
            {this.LocalData.CallFrom == 'PO' && (
              <View style={{width: '30%'}}>
                <ModalSearchableLabel
                  placeholder="Status"
                  fieldWrapperStyle={{height: 30, width: '100%'}}
                  data={this.state.StatusGroupData}
                  labelProp="Label"
                  valueProp="Status"
                  selectedValue={this.state.SelectedStatus}
                  inputStyle={[
                    {
                      paddingLeft: 5,
                      marginTop: 10,
                      marginBottom: 0,
                      borderBottomWidth: 0,
                      textAlign: 'center',
                      textDecorationLine: 'underline',
                    },
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderBottomColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}
                  onValueSelected={SelectedStatus => {
                    this.setState({SelectedStatus});
                    this.HandleSearch(
                      SelectedStatus != 'All' ? SelectedStatus : '',
                    );
                  }}
                />
              </View>
            )}
          </View>
          {/* {this.LocalData.CallFrom == 'PO' && (
            <View style={[styles.SearhBarWrapper, {paddingBottom: 0}]}>
              <ScrollView>
                <ModalSearchableLabel
                  placeholder="Status"
                  fieldWrapperStyle={{width: '50%'}}
                  data={this.state.StatusGroupData}
                  labelProp="Label"
                  valueProp="Status"
                  selectedValue={this.state.selectedStatus}
                  inputStyle={[
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sxl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderBottomColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}
                  onValueSelected={SelectedStatus => {
                    this.setState({SelectedStatus});
                    this.HandleSearch(
                      SelectedStatus != 'All' ? SelectedStatus : '',
                    );
                  }}
                />
                <ModalSearchablePicker
                  placeholder="Status"
                  fieldWrapperStyle={{width: '50%'}}
                  data={this.state.StatusGroupData}
                  labelProp="Label"
                  valueProp="Status"
                  selectedValue={this.state.selectedStatus}
                  inputStyle={[
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sxl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderBottomColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}
                  onValueSelected={SelectedStatus => {
                    this.setState({SelectedStatus});
                    this.HandleSearch(
                      SelectedStatus != 'All' ? SelectedStatus : '',
                    );
                  }}
                />
              </ScrollView>
            </View>
          )} */}
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={{
                  fontSize: 25,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 20,
                }}>
                No Data Found
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 5,
                }}>
                Pull to refresh
              </Text>
            </View>
          )}
          <FlatList
            style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            data={this.state.ListSourceData}
            renderItem={({item, index}) => (
              <ListItem
                isPO={this.LocalData.CallFrom === 'PO'}
                data={item}
                onLongPress={this.NavigateToPO}
                index={index}
                onPdfClick={this.CallShareFile}
                onDeleteInvoice={this.onDeleteSelectedInvoice}
                ChangeStatusDataList={this.ChangeStatusDataList}
                ChangeStatus={this.ChangeStatus}
              />
            )}
            keyExtractor={item => item.ID.toString()}
            refreshing={false}
            onRefresh={this.LoadAllInvoice}
            ListHeaderComponent={
              <ListItemHeader isPO={this.LocalData.CallFrom === 'PO'} />
            }
            stickyHeaderIndices={[0]}
            // to have some breathing space on bottom
            ListFooterComponent={() => (
              <View style={{width: '100%', marginTop: 85}} />
            )}
          />
          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => this.NavigateToPO()}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
        </LayoutWrapper>
        <ICubeAIndicator isShow={!this.state.isFetched} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  SearhBarWrapper: {
    padding: 10,
    // backgroundColor: '#ededed',
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
    top: 70,
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  Card: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // borderRadius: 5,
    // paddingTop: 15,
    // paddingBottom: 7,
    // // borderWidth: 1,
    // borderBottomWidth: 0,
    // borderLeftWidth: 0,
    // borderColor: 'lightgray',
    marginTop: 5,
    marginHorizontal: 9,
    // elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  removeItemIconContainer: {
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },
  removeItemIcon: {
    width: 20,
    height: 20,
    margin: 2,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  closeButton: {
    height: 30,
    width: 30,
    // paddingLeft:30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 12,
    top: 14,
  },

  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});
