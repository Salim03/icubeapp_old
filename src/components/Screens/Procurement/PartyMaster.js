import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  StyleSheet,
  Picker,
  TextInput,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  globalFontObject,
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {FloatingAction} from 'react-native-floating-action';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import LayoutWrapper, {CardStyle} from '../../Layout/Layout';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';

import {Request, AlertMessage} from '../../../Helpers/HelperMethods';

const ListItemHeader = () => {
  const {CardItemLayout} = CardStyle;
  return (
    <View style={[      
        styles.StickeyHeaderCard,
          
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
        
      <View style={[styles.CardTextContainer,{width: 330}]}>
        <Text numberOfLines={1} style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
             {'  '}
          Name{''}
        </Text>
      </View>
      
      <View style={[styles.CardTextContainer,CardItemLayout,{width: 180}]}>
        <Text numberOfLines={1} style={[styles.cardTextFieldHeader,
         ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
               //{textAlign: 'center'},
              ]}>                
          {''}
          Alias{''}
        </Text>
      </View>
      <View style={[styles.CardTextContainer,CardItemLayout,{width: 125}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
             ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),            
          ]}>
            {' '}
          Code
        </Text>
      </View>
      <View style={[styles.CardTextContainer,CardItemLayout,{width: 120,alignSelf: "flex-end"}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.cardTextFieldHeader,
             ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
           {textAlign:'right',},
          ]}>            
          City {/* { AgainstDate || ''} */}
        </Text>
      </View>
    </View>
  );
};

const ListItem = props => {
  const {CardItemLayout} = CardStyle;
  return (
    <TouchableWithoutFeedback
      onLongPress={() => props.onLongPress(props.data.ENTID)}>
      <View style={[
        styles.Card,
      ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
      ]}>
        <View style={styles.CardLine}>
          <View style={[styles.CardTextContainer,  {width: 330,}]}>
            <Text numberOfLines={1} style={[styles.CardSupplierText,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            ]}>{'  '}
              {props.data.Name}
            </Text>
          </View>
               
          <View style={[ styles.CardTextContainer, CardItemLayout,{width: 180,}]}>
            <Text
              numberOfLines={1}
              style={[ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              ]}>
                {'  '}
              {props.data.Alais}
            </Text>
          </View>
          <View style={[styles.CardTextContainer,CardItemLayout, {width: 125,}]}>
            <Text
              numberOfLines={1}
              style={[styles.CardAmountText,
              ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
              ]}>{'  '}
              {props.data.Code}
            </Text>
          </View>  
          <View style={[styles.InvoiceInfo, styles.CardTextContainer, CardItemLayout,{width: 120,}]}>
            <Text
              numberOfLines={1}
              style={[ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign:'right'},
              ]}>                
              {props.data.CITY}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class PartyMaster extends Component {
  constructor() {
    super();
    this.state = {
      isFetched: false,
      isRefresh: false,
      PartyType: 'SUPPLIER',
      NoDataAvailable: false,
      ListSourceData: [],
      Party: [],
      SearchText: '',
      show: false,
    };
    // this.ActionButtons = [
    // 	{
    // 		text: 'New',
    // 		icon: require('../../../assets/images/add-light.png'),
    // 		name: 'AddNew',
    // 		color: '#414d4c',
    // 		buttonSize: 45,
    // 		position: 1,
    // 	},
    // ];
  }
  HandleActionButtonClick = name => {
    if (name === 'AddNew')
      this.props.navigation.navigate('AddPartyNavigator', {
        Type: this.state.PartyType,
        PartyCodeId: 0,
      });
  };

  componentDidMount() {
    this.InitialSetup();
  }

  InitialSetup = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    this.LoadAllPart();
  };
  HandlePartyType = async ParType => {
    this.setState({PartyType: ParType, isFetched: false}, () => {
      // console.log(this.state.PartyType);
      let uri = `${this.IP}/api/BusinessPartner/Party?SearchType=${this.state.PartyType}`;
      console.log('call api for party : ' + uri);
      Request.get(uri, this.Token)
        .then(res => {
          // console.log(res);
          if (res.status === 200) {
            if (res.data.length) {
              this.setState({
                isFetched: true,
                Party: res.data,
                ListSourceData: res.data,
              });
            } else {
              AlertMessage(`No ${this.state.PartyType} found`);
              this.setState({
                isFetched: true,
                Party: [],
                ListSourceData: [],
                NoDataAvailable: true,
              });
            }
          }
        })
        .catch(err => {
          // console.error(err);
          AlertMessage('Something Went wrong\n' + JSON.stringify(err));
          this.setState({isFetched: true});
        });
    });
  };
  LoadAllPart = async () => {
    Request.get(
      `${this.IP}/api/BusinessPartner/Party?SearchType=${this.state.PartyType}`,
      this.Token,
    )
      .then(res => {
        //console.log(this.IP);
        //console.log(this.Token);
        if (res.status === 200) {
          this.setState({
            isFetched: true,
            Party: res.data,
            ListSourceData: res.data,
          });
          if (res.data.length === 0) {
            AlertMessage(`No ${this.state.PartyType} found`);
            this.setState({NoDataAvailable: true});
          } else this.setState({NoDataAvailable: false});
        }
      })
      .catch(err => {
        // console.error(err);
        AlertMessage('Something Went wrong\n' + JSON.stringify(err));
        this.setState({isFetched: true});
      });
  };

  NavigateToEditParty = ID => {
    this.props.navigation.navigate('AddPartyNavigator', {
      Type: this.state.PartyType,
      PartyCodeId: ID,
    });
    // console.log(ID);
  };

  HandleSearch = async SearchText => {
    this.setState({SearchText});
    let Party = this.state.Party;
    let txt = SearchText.toLowerCase();
    let ListSourceData = Party.filter(obj => {
      let Name = (obj.Name || '').toString().toLowerCase();
      let Code = (obj.Code || '').toString().toLowerCase();
      let Alais = (obj.Alais || '').toString().toLowerCase();
      let CITY = (obj.CITY || '').toString().toLowerCase();

      return (
        Name.includes(txt) ||
        Code.includes(txt) ||
        Alais.includes(txt) ||
        CITY.includes(txt)
      );
    });

    this.setState({ListSourceData});
  };
  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={styles.closeButtonParent}
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  clearInput = () => {
    this.HandleSearch('');
  };

  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }
    FieldSelectParty = key => (
      <ModalSearchablePicker 
        key={key}
        placeholder="Select Party"        
        data={[
          {label: 'SUPPLIER', value: 'SUPPLIER'},
          {label: 'TRANSPORTER', value: 'TRANSPORTER'},
          {label: 'AGENT', value: 'AGENT'},
          {label: 'JOBBER', value: 'JOBBER'},
          {label: 'CUSTOMER', value: 'CUSTOMER'},
          {label: 'LEAD', value: 'LEAD'},
        ]}
        labelProp="label"
        valueProp="value"
        selectedValue={this.state.PartyType}
        onValueSelected={this.HandlePartyType}
        inputStyle={[
                    {
                      paddingLeft: 5,
                      marginTop: 10,
                      marginBottom: 0,
                      borderBottomWidth: 0,
                      textAlign: 'center',
                      textDecorationLine: 'underline',
                    },
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.small.sl,
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderBottomColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}
                />
    );

    return (
      <>
       
        <LayoutWrapper
        backgroundColor={globalColorObject.Color.Lightprimary}>
          <View style={[styles.SearhBarWrapper,{flexDirection:'row'} ]}>
            <TextInput
              style={[styles.SearchInput,
              ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                {width:'70%'}
              ]}
              placeholder={'Search here'}
              value={this.state.SearchText}
              onChangeText={this.HandleSearch}
              autoCorrect={false}
              returnKeyType="search"
              autoCapitalize="none"
              onFocus={this.showCancel}
              onBlur={this.hideCancel}
            />
            <ModalSearchableLabel
              placeholder="Select Party"
              fieldWrapperStyle={{height: 30, width: '30%'}}
              data={[
                {label: 'SUPPLIER', value: 'SUPPLIER'},
                {label: 'TRANSPORTER', value: 'TRANSPORTER'},
                {label: 'AGENT', value: 'AGENT'},
                {label: 'JOBBER', value: 'JOBBER'},
                {label: 'CUSTOMER', value: 'CUSTOMER'},
                {label: 'LEAD', value: 'LEAD'},
              ]}
              labelProp="label"
              valueProp="value"                  
              selectedValue={this.state.PartyType}
              onValueSelected={this.HandlePartyType}
              inputStyle={[
                {
                  paddingLeft: 5,
                  marginTop: 10,
                  marginBottom: 0,
                  borderBottomWidth: 0,
                  textAlign: 'center',
                  textDecorationLine: 'underline',
                },
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderBottomColor,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}
            />
            
          </View>
          {this.state.NoDataAvailable && (
            <View style={styles.NoDataBanner}>
              <Text
                style={{
                  fontSize: 25,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 20,
                }}>
                No Data Found
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: 'rgba(0,0,0,0.2)',
                  textAlign: 'center',
                  marginTop: 5,
                }}>
                Pull to refresh
              </Text>
            </View>
          )}
          <FlatList
          style={[
              ApplyStyleColor(
                globalColorObject.Color.Lightprimary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            data={this.state.ListSourceData}
            ListHeaderComponent={() => <ListItemHeader />}
            renderItem={({item}) => (
              <ListItem
                data={item}
                type={this.state.PartyType}
                onLongPress={this.NavigateToEditParty}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            refreshing={this.state.isRefresh}
            onRefresh={this.LoadAllPart}
            stickyHeaderIndices={[0]}
          />
          {/* <FloatingAction
						actions={this.ActionButtons}
						onPressItem={this.HandleActionButtonClick}
						// color="rgba(50,74,84,1)" iconWidth={20} iconHeight={20}
						color="#093d62" iconWidth={20} iconHeight={20}
					/> */}
          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.props.navigation.navigate('AddPartyNavigator', {
                Type: this.state.PartyType,
                PartyCodeId: 0,
              });
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/add-light.png')}
            />
          </TouchableOpacity>
        </LayoutWrapper>        
      </>
    );
  }
}

const styles = StyleSheet.create({
 SearhBarWrapper: {
    padding: 10,   
    //marginTop: 60,
  },
 SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth:1,
  },
  FieldInput: {
    width: '98%',
    // position: 'relative',
    borderRadius: 8,
    backgroundColor: 'rgba(255,255,255,0.7)',
    borderColor: 'black',
    borderWidth: 1,
    margin: 5,
  },
  FlatList: {
    paddingVertical: 10,
  },
  NoDataBanner: {
    width: '100%',
    position: 'absolute',
  },
  Card: {
    width: '98%',
    alignSelf: 'center',
    //backgroundColor: 'white',
    marginVertical: 1,
    //borderRadius: 5,
    
    paddingTop: 10,
    paddingBottom: 10,
    //elevation: 5,
    
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  CardLine: {
    //width: '98%',
    flex:1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingHorizontal: 10,
    //position: 'relative',
    
    //justifyContent: 'space-between',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  CardSupplierText: {
    fontSize: 12,
    //marginVertical: 5,
    color: '#222',
  },
  CardTransporterText: {
    marginVertical: 3,
    color: '#222',
  },
  TypeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    elevation: 3,
    height: 40,
  },
  TypeText: {
    fontSize: 22,
    color: '#222',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  TypeSwitch: {
    marginRight: 10,
    transform: [{scaleX: 1.2}, {scaleY: 1.2}],
  },
  StickeyHeaderCard: {
    //width: '98%',
    //alignSelf: 'center',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    //backgroundColor: globalColorObject.Color.Primary,
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    // elevation: 5,
  },
  cardTextFieldHeader: {
    fontSize: 16,
    color: 'white',
    paddingLeft: 5,
  },
  
  container: {
    flex: 1,
  },
  FieldSelectParty: {
    //elevation: 5,
    //backgroundColor: 'white',
    //flex:1 ,
    width: '30%',
    //position: 'absolute',
    //justifyContent:'center',
  },
  
  closeButton: {
    height: 30,
    width: 30,
    marginLeft: 10,
  },
  closeButtonParent: {
    position: 'absolute',
    right: 12,
    top: 10,
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
});
