import React, { Component } from 'react';
import {
    Text, StyleSheet, TouchableOpacity, Image, View, FlatList, Picker,
    StatusBar, SafeAreaView, TouchableWithoutFeedback, Switch
} from 'react-native';
import moment from "moment";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Request, AlertMessage } from '../../../Helpers/HelperMethods';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
const ListItem = (props) => {

    return (
        <TouchableWithoutFeedback onLongPress={() => props.onLongPress(props.data.ReqID,props.data.ReqNo)}>
            <View style={styles.Card}>
                {/* Second Line to show TrackDate and LrDate */}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, borderBottomColor: 'black', width: '100%' }}>
                    <View style={{ borderRightWidth: 1, borderRightColor: 'black', width: '30%' }} >
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Entry No</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{props.data.ReqNo}</Text>
                    </View>
                    <View style={{ borderRightWidth: 1, borderRightColor: 'black', width: '30%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Entry Date</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{moment(props.data.ReqDate).format("DD-MM-YYYY")}</Text>
                    </View>
                    <View style={{ width: '40%' }} >
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Stockpoint</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{props.data.StockPointName || ""}</Text>
                    </View>


                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, borderBottomColor: 'black', width: '100%' }}>
                    <View style={{ borderRightWidth: 1, borderRightColor: 'black', width: '30%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Status</Text>
                        <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{(props.data.Status || "")}</Text>
                    </View>
                    <View style={{ borderRightWidth: 1, borderRightColor: 'black', width: '30%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Qty</Text>
                        <Text numberOfLines={1} style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center' }} >{(props.data.ReqQty || 0).toFixed(2)}</Text>
                    </View>
                    <View style={{ width: '40%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Amount</Text>
                        <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }} >{(props.data.Amount || 0).toFixed(2)}</Text>
                    </View>

                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                    <View style={{ borderRightWidth: 1, borderRightColor: 'black', width: '60%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Destination</Text>
                        <Text numberOfLines={1} style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{props.data.DestinationName || ""}</Text>
                    </View>
                    <View style={{ width: '25%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Created BY</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', textAlign: 'center' }} >{props.data.CreatedBy || ""}</Text>
                    </View>
                    <View style={{ alignItems: 'center', width: '15%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 12, }}>Select</Text>
                        <Switch
                            style={{
                                marginRight: 10,
                                transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }]
                            }}
                            onValueChange={Select => { props.onHandleSelect("isActive", props.index, Select) }}
                            value={props.data.Select}
                        />
                    </View>
                </View>

            </View>
        </TouchableWithoutFeedback>
    );
}


export default class PoApprovalDetail extends Component {

    constructor() {
        super();
        this.LocalData = {
            Title: '',
        };
        this.state = {

            isFetched: false,
            isRefresh: false,
            Type: '',
            Status: '',
            LedgerType: '',

            TypeList: [],
            LedgerList: [],
            ApproveList: [],

            Loccode: 1,
            FinancialId: 1003

        };
    }

    componentDidMount() {
        let { params } = this.props.route;
        this.LocalData.Title = params["HeaderTitle"];
        
		props.navigation.setOptions({ 
            title: this.LocalData.Title,
			headerRight: () =>  (
					
    <View style={styles.headerContainer}>
    <View style={styles.headerIconsContainer}>
      <TouchableOpacity onPress={this.InitialSetup}>
        <Image
          source={require('../../../assets/images/popup-blue.png')}
          style={styles.headerIcon}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.SaveApproval}>
        <Image
          source={require('../../../assets/images/save-blue.png')}
          style={styles.headerIcon}
        />
      </TouchableOpacity>
    </View>
  </View>
			), 
		});
        this.InitialSetup();
    }
    NavigateToForm = (nav, HeaderTitle, CallFrom) => {
        this.props.navigation.navigate(nav, {
            HeaderTitle,
            CallFrom
        });
    }
    InitialSetup = async () => {
        this.IP = await AsyncStorage.getItem("IP");
        this.Token = await AsyncStorage.getItem("Token");
        let params = this.props.route.params;
        this.setState({ Type: params.CallFrom, Status: params.CallFrom == "PO" ? "Approval" : params.CallFrom == "PI" ? "PI Against GRN" : "Close" })

        this.LoadInitialData();
    }
    LoadInitialData = async () => {
        this.setState({isFetched:false})
        console.log(`${this.IP}/api/PoApproval/GetDetailLoad?Status=${this.state.Status}&Loccode=${this.state.Loccode}&Type=${this.state.Type}`)
        Request.get(`${this.IP}/api/PoApproval/GetDetailLoad?Status=${this.state.Status}&Loccode=${this.state.Loccode}&Type=${this.state.Type}`, this.Token)
            .then(res => {
                console.log('GetData', res.data.Detail);
                if (res.status === 200) {
                    // this.setState({ isFetched: true, Voucher: res.data });
                    this.setState(({
                        isFetched: true, ApproveList: res.data.Detail, LedgerList: res.data.Ledgers,
                        TypeList: this.state.Type == "PO" ? [{ name: "Approval" }, { name: "Hold" }, { name: "Release" }, { name: "Cancel" }, { name: "Close" }] : this.state.Type == "JO" ? [{ name: "Close" }, { name: "Cancel" }] : [{ name: "PI Against GRN" }]
                    }), this.PrepareListForUnChangedData);

                    if (res.data.length === 0) {
                        AlertMessage(`No Voucher found`);
                        this.setState({ NoDataAvailable: true });
                    } else this.setState({ NoDataAvailable: false });
                }
            })
            .catch(err => {
                console.error(err);
                AlertMessage("Something Went wrong\n" + JSON.stringify(err));
                this.setState({ isFetched: true });
            });
    }
    
    SaveApproval = () => {
        try {
            let PassType = this.state.Type != "JO" ? this.state.Type : "Job Order";
            if (this.state.Status == "")
                AlertMessage("");
            else if (this.state.ApproveList.length==0)
                AlertMessage("PO Approval list cant be empty");
            else if (this.state.Type == "PI" && this.state.LedgerType == "")
                AlertMessage("Purchase Account Cant Be Empty.");
            else if (this.state.ApproveList.length > 0) {
                var SelectedList = this.state.ApproveList.filter(function (el) {
                    return el.Select == true;
                });

                if (SelectedList.length > 0) {

                    let PassStringId = SelectedList.map(function (obj) { return obj.ReqID }).join("^");
                    this.setState({isFetched:false})
                    if (PassType == "Job Order") {

                        let Code = this.state.Status == "Cancel" ? 5 : 6;
                        console.log('save',`${this.IP}/api/PoApproval/ApprovalSave?PassID=${PassStringId}&PassType=${PassType}&ID=${Code}`)
                        Request.post(`${this.IP}/api/PoApproval/ApprovalSave?PassID=${PassStringId}&PassType=${PassType}&ID=${Code}`,[], this.Token)
                            .then(async res => {
                                console.log(res)
                                let json = await res.json();
                                this.setState({ isFetched: true });
                                if (json != "Failed") {
                                    AlertMessage(`Record saved Successfully.`);
                                    this.InitialSetup();
                                    return true;
                                } else {
                                    AlertMessage("Something went wrong\nCould not save Voucher.");
                                    this.InitialSetup();
                                    return false;
                                }
                            })
                            .catch(err => { AlertMessage("Could not save \n" + err); this.setState({ isFetched: true }); });
                    }else {

                        Request.post(`${this.IP}/api/PoApproval/ApprovalSave?PassID=${PassStringId}&PassType=${PassType}&ID=${this.state.LedgerType}`, this.Token)
                        .then(async res => {
                            let json = await res.json();
                            this.setState({ isFetched: true });
                            if (json != "Failed") {
                                AlertMessage(`Record saved Successfully.`);
                                this.InitialSetup()
                                return true;
                            } else {
                                AlertMessage("Something went wrong\nCould not save Voucher.");
                                this.InitialSetup();
                                return false;
                            }
                        })
                        .catch(err => { AlertMessage("Could not save \n" + err); this.setState({ isFetched: true }); }); 
                    }

                } else {
                    AlertMessage("Select Atleast One Item To Approve.")
                }
            } else {
                AlertMessage("No Details To Approve.")
            }
        } catch (err) {
            AlertMessage("Failed To Save Record." + err);
        }
    }
    UpdateCodeValue = (Type, index, Value) => {
        console.log(Type, index, Value)
        if (Type === "isActive") {
            this.state.ApproveList[index].Select = Value;
            this.forceUpdate();
        }
    }
    HandleTypeChange = (Status) => {
        this.setState({ Status }, () => {
            this.LoadInitialData();
        })
    }
    NavigateToDetail = (InvoiceID,InvoiceNO) => {
       console.log('dd',InvoiceID,InvoiceNO)
        if(this.state.Type=='PO' || this.state.Type=='PI'){
            this.props.navigation.navigate("AddPurchaseNav", {
                CallFrom: this.state.Type=='PO'?this.state.Type:'GRN',
                HeaderTitle: this.state.Type=='PO'?'Purchase Order':'Goods Received Note',
                InvoiceID,
                InvoiceNO,
            });
        }
        
        
        // console.log(ID);
    }

    MLedgerList; MTypeList;
    PrepareListForUnChangedData = () => {
        // Preparing List items for Invoice Type

        if (this.state.LedgerList.length) {
            this.MLedgerList = this.state.LedgerList.map((item, index) => <Picker.Item key={index} label={item.GLName} value={item.GLID} />);
        }

        if (this.state.TypeList.length) {
            this.MTypeList = this.state.TypeList.map((item, index) => <Picker.Item key={index} label={item.name} value={item.name} />);
        }

        // Force Update the view to render all the data
        this.forceUpdate();

    }
    render() {
        if (!this.state.isFetched) {
            return (
                <>
                    <ICubeAIndicator />
                </>
            )
        }
        FieldLedger = key => (
            <ModalSearchablePicker
            key={key}
    placeholder="LedgerType"
    data={this.state.MLedgerList}
    labelProp="GLName"
    valueProp= "GLID"
    selectedValue={this.state.LedgerType}
    onValueSelected={(LedgerType) => this.setState({ LedgerType })}
    />
     );
     FieldType = key => (
        <ModalSearchablePicker
        key={key}
placeholder="Status"
data={this.state.MTypeList}
labelProp="name"
valueProp= "name"
selectedValue={this.state.Status}
onValueSelected={(Status) => this.HandleTypeChange(Status)}

/>
 );
        return (
            <>
                <SafeAreaView style={styles.container}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.FieldContainer}>
                        {FieldType()}

    
                     
                        </View>
                        {(this.state.Type === "PI") &&
                            <View style={styles.FieldContainer}>

                                {FieldLedger()}
       
                           
                            </View>}


                        <FlatList
                            style={styles.FlatList}
                            data={this.state.ApproveList}
                            renderItem={({ item, index }) => <ListItem data={item} index={index} onHandleSelect={this.UpdateCodeValue} onLongPress={this.NavigateToDetail} />}
                            keyExtractor={(item, index) => index.toString()}
                            refreshing={this.state.isRefresh}
                            onRefresh={this.LoadAllPart}
                        />

                    </View>
                </SafeAreaView>
            </>


        );
    }
};

const styles = StyleSheet.create({
    ListContainer: {
        marginTop: 10,
        // marginLeft:20,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        marginBottom: 5,
        flexWrap: 'wrap',
    },
    FieldContainer: {
        flexDirection: 'row',
        // position: 'relative',
        borderBottomWidth: 2,
        borderBottomColor: 'rgba(0,0,0,0.1)',
        marginVertical: 5,
    },
    FieldLabel: {
        flex: 1,
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: 'bold',
        position: 'absolute',
        top: 0,
        left: 5,
        color: 'gray',
    },
    FieldInput: {
        flex: 2,
        height: 40,
        alignSelf: 'center',
        marginTop: 8,
    },
    container: {
        flex: 1,
    },
    Card: {
        width: '95%',
        alignSelf: 'center',
        backgroundColor: 'rgb(242, 242, 242)',
        marginVertical: 5,
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 10,
        elevation: 3,
    },
    headerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      headerIconsContainer: {
        flexDirection: 'row',
      },
      headerIcon: {
        width: 35,
        height: 35,
        marginRight: 8,
        marginLeft: 8,
      },
    
});
	// export {Retail};