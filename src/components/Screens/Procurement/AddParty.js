import React, {Component} from 'react';
import {
  Alert,
  View,
  SafeAreaView,
  StatusBar,
  Text,
  Switch,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Image,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import RichTextInput from '../../Custom/RichTextInput';
import ViewPager from '@react-native-community/viewpager';

import ModalDatePicker from '../../Custom/ModalDatePicker';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import {FloatingAction} from 'react-native-floating-action';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import {Row} from 'react-native-table-component';
export default class AddParty extends Component {
  constructor(props) {
    super(props);
    // Local Object to store all the navigation and other informations
    this.ExistingBarcode = [];

    this.APILink = {
      API_LoadInitialData: `/api/BusinessPartner/Load_InitialData?`, //CallFrom=PO&LocCode=1
      API_PartySave: `/api/BusinessPartner/PartySave`,
      API_PartyDetails: `/api/BusinessPartner/GetPartyDetails?`,
      API_CustomerDetails: `/api/BusinessPartner/GetCustomerDetails?`,
    };
    this.state = {
      isFetched: true,
      // Holds the data to be set to controls ( filled later by the data from API )
      APIData: {
        PrimaryGroupList: [],
        SalesTypeList: [],
        GeneralLedgerList: [],
        CalculateOnList: [],
        RatingList: [],
        PriceList: [],
        GstTypeList: [],
        CurrencyList: [],
        SiteIdList: [],
        VillageList: [],
        StreetList: [],
        CountryList: [],
        CityList: [],
        StateList: [],
        BankList: [],
        TaxNameList: [],
        PaymentTermList: [],
        LocationList: [],
        TransporterList: [],
        AgentList: [],
      },
      // Hold selected or entered values of controls
      //Partner Details
      PartType: '',
      PartCode: '',
      LastAlias: '',
      ENTID: 0,
      GstTypeList: [
        {label: 'Registered', value: 'Registered'},
        {label: 'UnRegistered', value: 'UnRegistered'},
      ],
      Name: '',
      AName: '',
      PrimaryGroup: '',
      GLedger: '',
      SalesType: 'OUTRIGHT',
      Rating: '',
      PriceRule: '',
      CalculateOn: 'WSP',
      GstType: '',
      GstNo: '',
      Currency: '',

      //Contact Details
      SiteId: '',
      ContactName: '',
      Mobile: '',
      WhatsAppNo: '',
      WebSite: '',
      Phone: '',
      IsdNo: '',
      PlotNo: '',
      Village: '',
      Street: '',
      Street2: '',
      Country: '',
      City: '',
      Zip: '',
      State: '',
      LCode1: '',
      LCode2: '',
      LandLine1: '',
      LandLine2: '',
      Fax: '',
      Email: '',
      Remarks: '',

      //TaxNumber
      Pan: '',
      Date: '',
      Bank: '',

      //Other
      Transporter: '',
      Agent: '',
      AgentCommission: '',
      JobberRate: '',

      //Supplier
      TaxName: '',
      Discount: '',
      CreditLimit: '',
      CreditDays: '',
      PaymentTerms: '',
      Mandatory: false,
      Location: '',
      LocationType: false,
      RangeWiseDisc: false,

      //Tracking
      LogisticIn: false,
      LogisticOut: false,
      CourierIn: false,
      CourierOut: false,
      GateIn: false,
      GateOut: false,
      SalesForcasting: false,
      Replenisment: false,
      Extinct: false,

      ActiveTab: 0,
    };
    // this.ActionButtons = [
    //   {
    //     text: 'Clear',
    //     icon: require('../../../assets/images/Clear.png'),
    //     name: 'Clear',
    //     color: '#414d4c',
    //     buttonSize: 45,
    //     position: 1,
    //   },
    //   {
    //     text: 'Save',
    //     icon: require('../../../assets/images/Save.png'),
    //     name: 'Save',
    //     color: '#414d4c',
    //     buttonSize: 45,
    //     position: 2,
    //   },
    // ];
  }
  HandleClearData() {
    this.setState({
      //Partner Details

      ENTID: 0,
      Name: '',
      AName: '',
      PrimaryGroup: '',
      GLedger: '',
      SalesType: 'OUTRIGHT',
      Rating: '',
      PriceRule: '',
      CalculateOn: 'WSP',
      GstType: '',
      GstNo: '',
      Currency: '',

      //Contact Details
      SiteId: '',
      ContactName: '',
      Mobile: '',
      WhatsAppNo: '',
      WebSite: '',
      Phone: '',
      IsdNo: '',
      PlotNo: '',
      Village: '',
      Street: '',
      Street2: '',
      Country: '',
      City: '',
      Zip: '',
      State: '',
      LCode1: '',
      LCode2: '',
      LandLine1: '',
      LandLine2: '',
      Fax: '',
      Email: '',
      Remarks: '',

      //TaxNumber
      Pan: '',
      Date: '',
      Bank: '',

      //Other
      Transporter: '',
      Agent: '',
      AgentCommission: '',
      JobberRate: '',

      //Supplier
      TaxName: '',
      Discount: '',
      CreditLimit: '',
      CreditDays: '',
      PaymentTerms: '',
      Mandatory: false,

      Location: '',
      LocationType: false,
      RangeWiseDisc: false,

      //Tracking
      LogisticIn: false,
      LogisticOut: false,
      CourierIn: false,
      CourierOut: false,
      GateIn: false,
      GateOut: false,
      SalesForcasting: false,
      Replenisment: false,
      Extinct: false,

      ActiveTab: 0,
    });
  }
  SaveParty = async () => {
    Keyboard.dismiss();

    let SaveObject = {
      ENTID: this.state.ENTID,
      Type: this.state.PartType,
      VendorCode: this.state.PartCode,
      Name: this.state.Name,
      AlaisName: this.state.AName,
      SiteId: this.state.SiteId,
      paymentTerms: this.state.PaymentTerms,
      Default: 0,
      PaymentMandatory: this.state.Mandatory,
      GeneralLedger:
        this.state.GLedger == null || this.state.GLedger == ''
          ? '0'
          : this.state.GLedger,
      LocationType: (this.state.LocationType = false ? 0 : 1),
      LocationList: this.state.Location,
      Contactname: this.state.ContactName,
      Mobileno: this.state.Mobile,
      Street1: this.state.Street,
      Street2: this.state.Street2,
      City: this.state.City,
      State: this.state.State,
      Country: this.state.Country,
      Zone: '',
      Area: '',
      Zip: this.state.Zip,
      Phone: this.state.Phone,
      Fax: this.state.Fax,
      Email: this.state.Email,
      WebSite: this.state.WebSite,
      TinNo: '',
      CstNo: '',
      PanNo: this.state.Pan,
      IGstNo: this.state.GstNo,
      GstType: this.state.GstType,
      IECode: '',
      MFGLicenseCode: '',
      Administrator: '',
      Password: '',
      MarginRules: '',
      Discount:
        this.state.Discount == '' || this.state.Discount == null
          ? 0
          : this.state.Discount,
      CreditDay:
        this.state.CreditDays == '' || this.state.CreditDays == null
          ? 0
          : this.state.CreditDays,
      CreditLimits:
        this.state.CreditLimit == '' || this.state.CreditLimit == null
          ? 0
          : this.state.CreditLimit,
      AgentName: this.state.Agent,
      AgentAmount:
        this.state.AgentCommission == '' || this.state.AgentCommission == null
          ? 0
          : this.state.AgentCommission,
      TransportName: this.state.Transporter,
      JobberRate:
        this.state.JobberRate == '' || this.state.JobberRate == null
          ? 0
          : this.state.JobberRate,
      Replenishment: this.state.Replenisment,
      SalesForecasting: this.state.SalesForcasting,
      HeadOffice: true,
      Extinct: this.state.Extinct,
      LogisticsInward: this.state.LogisticIn ? 'Y' : 'N',
      LogisticsOutward: this.state.LogisticOut ? 'Y' : 'N',
      GatewayInward: this.state.GateIn,
      GatewayOutward: this.state.GateOut,
      CourierInward: this.state.CourierIn,
      CourierOutward: this.state.CourierOut,
      GroupCode: '',
      Remarks: this.state.Remarks,
      BaleRate: '0',
      KGRate: '0',
      RateEach: '0',
      LocationCode: '1',
      StockPoint: '1',
      CompanyCode: '001',
      IsMultipleLocation: true,
      LogUser: '',
      CurrencySetUp: '',
      PrimaryGroup: '',
      TaxName: '',
      TaxDate: '',
      CreatedBy: 'admin',
      Landline2: this.state.LandLine2,
      Rating: this.state.Rating,
      PriceRule: '1',
      Landline1Code: '',
      LandLine2Code: '',
      CalculateOn: this.state.CalculateOn,
      SalesType: this.state.SalesType,
      BankID: '',
      IsdNo: 76,
      WhatsAppNo: '',
      CustomText1: '',
      CustomText2: '',
      CustomText3: '',
      CustomText4: '',
      CustomText5: '',
      CustomCombo1: '',
      CustomCombo2: '',
      CustomDate1: '',
      CustomDate2: '',
      CustomRadio1: false,
      CustomRadio2: false,
      LeadDays: '0',
      ConsignmentLocationId: 0,
      M_id: '',
      M_Entid: '',
      M_PartyCode: '',
      M_SiteName: '',
      M_ContactName: '',
      M_MobileName: '',
      M_PlotNo: '',
      M_Street: '',
      M_Village: '',
      M_Zip: '',
      M_Citys: '',
      M_Zone: '',
      M_State: '',
      M_LandLine: '',
      M_Country: '',
      M_Extinct: '',
      IsRangeWiseDiscount: this.state.RangeWiseDisc,
      RangeWiseIdList: '',
      RangeWiseFromList: '',
      RangeWiseToList: '',
      RangeWisePercentageList: '',
      BillingFor: 'Supplier',
      isLoginAccess: false,
      LoginPassword: '',
      LoginUsername: '',
      Latitude: '',
      Longitude: '',
      AccountManagerEmpID: 0,
      LogisticTransporterDetailsID: '',
      LogisticTransporterRate: '',
      CityID: '',
      UOMID: '',
      ProductGRPCODE: '',
    };
    let get_ddd = `${this.IP}${this.APILink.API_PartySave}`;
    console.log('API :', get_ddd);
    console.log('Data :', SaveObject);
    this._post(`${this.IP}${this.APILink.API_PartySave}`, SaveObject)
      .then(async res => {
        this.setState({ShowModalLoader: false});
        let json = await res.json();
        console.log('Json Result : ', json);
        if (res.ok) {
          this.AlertMessage(`${JSON.stringify(json)} Successfully`);
          //   this.HandleClearData();
          this.props.navigation.goBack();
          //   this.LoadInitialData();
          return true;
        } else {
          this.AlertMessage(`Could not save\n ${JSON.stringify(json)}`);
          return false;
        }
      })
      .catch(err => {
        this.AlertMessage('Could not save \n' + err);
        this.setState({ShowModalLoader: false});
      });
    this.setState({ShowModalLoader: true, ModalLoaderText: 'Saving Data...'});
  };
  AlertMessage = msg => {
    Alert.alert('iCube Alert', msg);
  };
  _get = async url => {
    const res = await fetch(url, {
      headers: {
        Authorization: this.Token,
      },
    });
    const json = await res.json();
    return {
      data: json,
      status: res.status,
    };
  };
  _post = async (url, data) => {
    const res = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Authorization: this.Token,
      },
    });
    return res;
  };
  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };
  HandleTabsClick = ActiveTab => {
    this.setState({ActiveTab});
    this.refs.ViewPager.setPage(ActiveTab || 0);
  };

  HighlightSelectedTab = event => {
    Keyboard.dismiss();
    this.setState({ActiveTab: event.nativeEvent.position});
  };
  componentDidMount() {
    this.LoadInitialData();
    const isCreate =
      this.ENTID && this.EditItemSeqNo ? 'update' : 'create' === 'create';
    const iconStyle = {width: 30, height: 30};
    const saveIcon = require('../../../assets/images/save-light.png');
    this.props.navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[
              isCreate ? iconStyle : {width: 0, height: 0},
              {marginRight: 15, overflow: 'hidden'},
            ]}
            onPress={this.HandleActionButtonClick.bind(this, 'Save')}>
            <Image style={iconStyle} source={saveIcon} />
          </TouchableOpacity>
        </View>
      ),
    });
  }
  ValidateInputs = async () => {
    if (this.state.Name === '') {
      this.AlertMessage('Enter Name');
      try {
        this.refs.Name.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else if (this.state.AName === '') {
      this.AlertMessage('Enter Alias Name');
      try {
        this.refs.AName.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else if (
      this.state.PartType === 'CUSTOMER' &&
      this.state.CalculateOn === ''
    ) {
      this.AlertMessage(' Calculate On should not be empty ');
      try {
        this.refs.CalculateOn.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else if (
      this.state.PartType === 'CUSTOMER' &&
      this.state.SalesType === ''
    ) {
      this.AlertMessage('Sales Type should not be empty');
      try {
        this.refs.AName.focus();
      } catch (error) {
        console.log(error);
      }
      return;
    } else {
      this.SaveParty();
    }
  };
  LoadSelectedParty(Type, PId, InitialData) {
    //	console.log(`${this.IP}${this.APILink.API_SelectedInvoiceDetails}CallFrom=${Type}&PassInvoiceID=${Sid}&LocCode=${Loccode}`);

    if (Type !== 'CUSTOMER') {
      this._get(`${this.IP}${this.APILink.API_PartyDetails}Code=${PId}`)
        .then(res => {
          console.log('selected Party : ', res.data[0]);
          this.setState(
            prevState => ({
              APIData: {
                ...prevState.APIData,
                PrimaryGroupList: InitialData.data.PrimaryGroups,
                GeneralLedgerList: InitialData.data.GLedger,
                RatingList: InitialData.data.CustRating,
                PriceList: InitialData.data.PriceRul,
                CurrencyList: InitialData.data.CurrencyDetails,
                // GstTypeList:res.data.GLedger ,SiteIdList:res.data.GLedger ,BankList:res.data.GLedger ,
                CalculateOnList: InitialData.data.CalculatedOn,
                SalesTypeList: InitialData.data.SaleType,
                VillageList: InitialData.data.Village,
                StreetList: InitialData.data.Street,
                CountryList: InitialData.data.COUNTRY,
                CityList: InitialData.data.Taluk,
                StateList: InitialData.data.State,
                TaxNameList: InitialData.data.TaxList,
                PaymentTermList: InitialData.data.paymentterms,
                LocationList: InitialData.data.LocationLists,
                TransporterList: InitialData.data.Transporter,
                AgentList: InitialData.data.Agent,
              },
              PartType: Type,
              ENTID: PId,

              PartCode: res.data[0].PARTYCODE,
              Name: res.data[0].PARTYNAME,
              AName: res.data[0].PARTYID,
              LastAlias: InitialData.data.PartyAlias[0].PARTYID,
              PrimaryGroup: res.data[0].PrimaryGroup,
              GLedger: res.data[0].LedgerID,
              SalesType: 'OUTRIGHT',
              Rating: res.data[0].Rating,
              PriceRule: res.data[0].PriceRule,
              CalculateOn: '',
              GstType: res.data[0].GSTType,
              GstNo: res.data[0].GSTNo,
              Currency: res.data[0].Currency,

              //Contact Details
              SiteId: '',
              ContactName: res.data[0].CONTACT,
              Mobile: res.data[0].MOBILE,
              WhatsAppNo: res.data[0].WhatsAppNo,
              WebSite: '',
              Phone: '',
              IsdNo: res.data[0].IsdNo,
              PlotNo: res.data[0].Street1,
              Village: res.data[0].Area,
              Street: res.data[0].Street2,
              Country: res.data[0].COUNTRY,
              City: res.data[0].CITY,
              Zip: res.data[0].PINCODE,
              State: res.data[0].STATES,
              LCode1: res.data[0].Landline1code,
              LCode2: res.data[0].Landline2Code,
              LandLine1: res.data[0].TELEPHONE,
              LandLine2: res.data[0].Landline2,
              Fax: res.data[0].FAX,
              Email: res.data[0].EMAIL,
              Remarks: res.data[0].REMARKS,

              //TaxNumber
              Pan: res.data[0].PanNo,
              Date: '',
              Bank: '',

              //Other
              Transporter: res.data[0].TRANSPORTERNAME.toString(),
              Agent: res.data[0].AGENTNAME.toString(),
              AgentCommission: res.data[0].AGENTRATE.toString(),
              JobberRate: res.data[0].JOBBERRATE,

              //Supplier
              TaxName: res.data[0].TAXNAME.toString(),
              Discount: res.data[0].DISCOUNT.toString(),
              CreditLimit: res.data[0].CREDITLT.toString(),
              CreditDays: res.data[0].CREDITDAYS,
              PaymentTerms: '',
              Mandatory: false,

              Location: '',
              LocationType: false,
              RangeWiseDisc: false,

              //Tracking
              LogisticIn: res.data[0].LogInWard == 'N' ? false : true,
              LogisticOut: res.data[0].LogOutWard == 'N' ? false : true,
              CourierIn: res.data[0].CourierIn,
              CourierOut: res.data[0].CourierOut,
              GateIn: res.data[0].GateIn,
              GateOut: res.data[0].GateOut,
              SalesForcasting: res.data[0].SalesForecast,
              Replenisment: res.data[0].Replenishment,
              Extinct: res.data[0].Extinct,

              //   ActiveTab: 'Tax',

              LastAlias: InitialData.data.PartyAlias[0].PARTYID,
            }),
            () => {
              //  this.PrepareListForUnChangedData();
            },
          );
        })
        .catch(err => this.AlertMessage(JSON.stringify(err)));
    } else {
      this._get(`${this.IP}${this.APILink.API_CustomerDetails}Code=${PId}`)
        .then(res => {
          console.log('selected customer : ', res.data[0]);
          this.setState(
            prevState => ({
              PartType: Type,
              ENTID: PId,

              PartCode: res.data[0].CUSTOMERCODE,
              Name: res.data[0].CUSTOMERNAME,
              AName: res.data[0].CUSTOMERID,
              PrimaryGroup: res.data[0].PrimaryGroup,
              GLedger: res.data[0].LedgerID,
              SalesType: res.data[0].SALESTYPE,
              Rating: res.data[0].Rating,
              PriceRule: res.data[0].PriceRule,
              CalculateOn: res.data[0].CALCON,
              GstType: res.data[0].GSTType,
              GstNo: res.data[0].GSTin,
              Currency:res.data[0].Currency,

              //Contact Details
              SiteId: '',
              ContactName: res.data[0].CONTACT,
              Mobile: res.data[0].MOBILE,
              WhatsAppNo: res.data[0].WhatsAppNo,
              WebSite: '',
              Phone: '',
              IsdNo: res.data[0].IsdNo,
              PlotNo: res.data[0].Street1,
              Village: res.data[0].Area,
              Street: res.data[0].Street2,
              Country: res.data[0].COUNTRY,
              City: res.data[0].CITY,
              Zip: res.data[0].PINCODE,
              State: res.data[0].STATES,
              LCode1: res.data[0].Landline1code,
              LCode2: res.data[0].Landline2Code,
              LandLine1: res.data[0].TELEPHONE,
              LandLine2: res.data[0].Landline2 ? '' : '',
              Fax: res.data[0].FAX,
              Email: res.data[0].EMAIL,
              Remarks: res.data[0].REMARKS,

              //TaxNumber
              Pan: res.data[0].PanNo,
              Date: '',
              Bank: '',

              //Other
              Transporter: res.data[0].Transporter,
              Agent: res.data[0].Agent.toString(),
              AgentCommission: res.data[0].AgentRate.toString(),
              JobberRate: '',

              //Supplier
              TaxName: res.data[0].TAXNAME,
              Discount: res.data[0].DISCOUNT.toString(),
              CreditLimit: res.data[0].CREDITLT.toString(),
              CreditDays: res.data[0].CREDITDAYS,
              PaymentTerms: '',
              Mandatory: false,

              Location: '',
              LocationType: false,
              RangeWiseDisc: false,

              //Tracking
              LogisticIn: res.data[0].LogInWard == 'N' ? false : true,
              LogisticOut: res.data[0].LogOutWard == 'N' ? false : true,
              CourierIn: res.data[0].CInward,
              CourierOut: res.data[0].COutward,
              GateIn: res.data[0].GInward,
              GateOut: res.data[0].GOutward,
              SalesForcasting: res.data[0].SalesForecast,
              Replenisment: res.data[0].Replenishment,
              Extinct: res.data[0].Extinct,

              //   ActiveTab: 'Tax',

              LastAlias: InitialData.data.PartyAlias[0].PARTYID,
              APIData: {
                ...prevState.APIData,
                PrimaryGroupList: InitialData.data.PrimaryGroups,
                GeneralLedgerList: InitialData.data.GLedger,
                RatingList: InitialData.data.CustRating,
                PriceList: InitialData.data.PriceRul,
                CurrencyList: InitialData.data.CurrencyDetails,
                // GstTypeList:res.data.GLedger ,SiteIdList:res.data.GLedger ,BankList:res.data.GLedger ,
                CalculateOnList: InitialData.data.CalculatedOn,
                SalesTypeList: InitialData.data.SaleType,
                VillageList: InitialData.data.Village,
                StreetList: InitialData.data.Street,
                CountryList: InitialData.data.COUNTRY,
                CityList: InitialData.data.Taluk,
                StateList: InitialData.data.State,
                TaxNameList: InitialData.data.TaxList,
                PaymentTermList: InitialData.data.paymentterms,
                LocationList: InitialData.data.LocationLists,
                TransporterList: InitialData.data.Transporter,
                AgentList: InitialData.data.Agent,
              },
            }),
            () => {
              // this.PrepareListForUnChangedData();
            },
          );
        })
        .catch(err => this.AlertMessage(JSON.stringify(err)));
    }
  }
  HandleActionButtonClick = name => {
    console.log('call click ', name);
    if (name === 'Save') this.ValidateInputs();
    else if (name === 'Clear') this.HandleClearData();
  };

  LoadInitialData = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    let params = this.props.route.params;
    console.log(params);
    console.log(
      `${this.IP}${this.APILink.API_LoadInitialData}Type=${params.Type}`,
    );
    this._get(
      `${this.IP}${this.APILink.API_LoadInitialData}Type=${params.Type}`,
    )
      .then(res => {
        if (params.PartyCodeId != 0) {
          this.LoadSelectedParty(params.Type, params.PartyCodeId, res);
        } else {
          console.log(res.data.SaleType);
          this.setState(
            prevState => ({
              isFetched: true,
              PartType: params.Type,
              LastAlias: res.data.PartyAlias[0].PARTYID,
              PartCode: res.data.PartyCode[0].PartyCode,
              APIData: {
                ...prevState.APIData,
                PrimaryGroupList: res.data.PrimaryGroups,
                GeneralLedgerList: res.data.GLedger,
                RatingList: res.data.CustRating,
                PriceList: res.data.PriceRul,
                CurrencyList: res.data.CurrencyDetails,
                // GstTypeList:res.data.GLedger ,SiteIdList:res.data.GLedger ,BankList:res.data.GLedger ,
                CalculateOnList: res.data.CalculatedOn,
                SalesTypeList: res.data.SaleType,
                VillageList: res.data.Village,
                StreetList: res.data.Street,
                CountryList: res.data.COUNTRY,
                CityList: res.data.Taluk,
                StateList: res.data.State,
                TaxNameList: res.data.TaxList,
                PaymentTermList: res.data.paymentterms,
                LocationList: res.data.LocationLists,
                TransporterList: res.data.Transporter,
                AgentList: res.data.Agent,
              },
            }),
            // this.PrepareListForUnChangedData,
          );
        }
      })
      .catch(err => this.AlertMessage(JSON.stringify(err)));
  };
  //MPrimaryGroupList = [];
  //MGeneralLedgerList = [];
  //MRatingList = [];
  //MPriceList = [];
  //MCurrencyList = [];
  //MSalesTypeList = [];
  //MVillageList = [];
  //MStreetList = [];
  //MCountryList = [];
  //MCityList = [];
  // MStateList = [];
  //MTaxNameList = [];
  //MPaymentTermList;
  //MLocationList = [];
  //MTransporterList = [];
  //MAgentList = [];
  //MCalculateOnList = [];
  // PrepareListForUnChangedData = () => {
  /*let {APIData} = this.state;

    if (APIData.PrimaryGroupList.length) {
      this.MPrimaryGroupList = APIData.PrimaryGroupList.map(item => ({
        label: item.Name,
        value: item.Code,
      }));
    }
    if (APIData.GeneralLedgerList.length) {
      this.MGeneralLedgerList = APIData.GeneralLedgerList.map(item => ({
        label: item.GLNAME,
        value: item.GLID,
      }));
    }
    if (APIData.RatingList.length) {
      this.MRatingList = APIData.RatingList.map(item => ({label: item.Rating, value: item.Rating,}));
    }
    if (APIData.PriceList.length) {
      this.MPriceList = APIData.PriceList.map(item => ({
        label: item.PriceList,
        value: item.id,
      }));
    }
    if (APIData.CalculateOnList.length) {
      this.MCalculateOnList = APIData.CalculateOnList.map(item => ({
        label: item.CalculateOn,
        value: item.CalculateOn,
      }));
    }

    if (APIData.CurrencyList.length) {
      this.MCurrencyList = APIData.CurrencyList.map(item => ({
        label: item.CurrencyName,
        value: item.SetupId,
      }));
    }

    if (APIData.VillageList.length) {
      this.MVillageList = APIData.VillageList.map(item => ({
        label: item.VILLAGE,
        value: item.CID,
      }));
    }

    if (APIData.StreetList.length) {
      this.MStreetList = APIData.StreetList.map(item => ({
        label: item.STREET,
        value: item.CID,
      }));
    }

    if (APIData.CountryList.length) {
      this.MCountryList = APIData.CountryList.map(item => ({
        label: item.COUNTRY,
        value: item.CID,
      }));
    }

    if (APIData.CityList.length) {
      this.MCityList = APIData.CityList.map(item => ({
        label: item.CITY,
        value: item.CID,
      }));
    }

    if (APIData.StateList.length) {
      this.MStateList = APIData.StateList.map(item => ({
        label: item.STATE,
        value: item.CID,
      }));
    }

    if (APIData.TaxNameList.length) {
      this.MTaxNameList = APIData.TaxNameList.map(item => ({
        label: item.TaxName,
        value: item.TaxCode,
      }));
    }

    if (APIData.PaymentTermList.length) {
      this.MPaymentTermList = APIData.PaymentTermList.map(item => ({
        label: item.TERMNAME,
        value: item.ID,
      }));
    }
    if (APIData.SalesTypeList.length) {
      this.MSalesTypeList = APIData.SalesTypeList.map(item => ({
        label: item.SalesType,
        value: item.SalesType,
      }));
    }
   
  };
*/
  FieldSupplierCode = key => (
    <RichTextInput
      key={key}
      placeholder="SUPPLIER CODE"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.PartCode}
      onChangeText={PartCode => this.setState({PartCode})}
      editable={false}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'PartCode'}
    />
  );
FieldCustomerCode=(key)=>(
<RichTextInput key={key}
placeholder="CUSTOMER CODE"
onBlur={this.HandleRateBlur}
//style={styles.FieldInput}
value={this.state.PartCode}
onChangeText={PartCode => this.setState({PartCode})}
editable={false}
returnKeyType="next"
autoCorrect={false}
autoCapitalize="none"
ref={'PartCode'}
/>
);
  FieldName = key => (
    <RichTextInput
      key={key}
      placeholder="NAME"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Name}
      onChangeText={Name => this.setState({Name})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="sentences"
      autoCapitalize="none"
      ref={'Name'}
    />
  );
  FieldAliasName = key => (
    <RichTextInput
      key={key}
      placeholder="Alias Name"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.AName}
      onChangeText={AName => this.setState({AName})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'AName'}
    />
  );
  FieldLastAliasName = key => (
    <RichTextInput
      key={key}
      placeholder="Last Alias"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.LastAlias}
      onChangeText={LastAlias => this.setState({LastAlias})}
      editable={false}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'LastAlias'}
    />
  );
  FieldPrimaryGroup = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="PrimaryGroup"
      data={this.state.APIData.PrimaryGroupList}
      labelProp="Name"
      valueProp="Code"
      selectedValue={this.state.PrimaryGroup}
      onValueSelected={PrimaryGroup => this.setState({PrimaryGroup})}
    />
  );
  FieldGeneralLedger = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="General Ledger"
      data={this.state.APIData.GeneralLedgerList}
      labelProp="GLNAME"
      valueProp="GLID"
      selectedValue={this.state.GLedger}
      onValueSelected={GLedger => this.setState({GLedger})}
    />
  );
  FieldRating = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Rating"
      data={this.state.APIData.RatingList}
      labelProp="Rating"
      valueProp="Rating"
      selectedValue={this.state.Rating}
      onValueSelected={Rating => this.setState({Rating})}
    />
  );
  FieldPriceRule = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Price Rule"
      data={this.state.APIData.PriceList}
      labelProp="PriceList"
      valueProp="id"
      selectedValue={this.state.PriceRule}
      onValueSelected={PriceRule => this.setState({PriceRule})}
    />
  );
  FieldCalculateOn = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Calculate On"
      data={this.state.APIData.CalculateOnList}
      labelProp="CalculateOn"
      valueProp="CalculateOn"
      selectedValue={this.state.CalculateOn}
      onValueSelected={CalculateOn => this.setState({CalculateOn})}
    />
  );
  FieldCurrency = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Currency"
      data={this.state.APIData.CurrencyList}
      labelProp="CurrencyName"
      valueProp="SetupId"
      selectedValue={this.state.Currency}
      onValueSelected={Currency => this.setState({Currency})}
    />
  );
  FieldContactName = key => (
    <RichTextInput
      key={key}
      placeholder="Contact Name"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.ContactName}
      onChangeText={ContactName => this.setState({ContactName})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'ContactName'}
    />
  );
  FieldMobileNo = key => (
    <RichTextInput
      placeholder="Mobile No"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Mobile}
      onChangeText={Mobile => this.setState({Mobile})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Mobile'}
      keyboardType={'phone-pad'}
    />
  );
  FieldWhatsappNo = key => (
    <RichTextInput
      placeholder="WhatsApp No"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.WhatsAppNo}
      onChangeText={WhatsAppNo => this.setState({WhatsAppNo})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'WhatsAppNo'}
    />
  );
  FieldISDNo = key => (
    <RichTextInput
      key={key}
      placeholder="ISD No"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.IsdNo}
      onChangeText={IsdNo => this.setState({IsdNo})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'IsdNo'}
    />
  );
  FieldPlotNo = key => (
    <RichTextInput
      key={key}
      placeholder="Plot No"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.PlotNo}
      onChangeText={PlotNo => this.setState({PlotNo})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'PlotNo'}
    />
  );
  FieldVillage = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Village"
      data={this.state.APIData.VillageList}
      labelProp="VILLAGE"
      valueProp="CID"
      selectedValue={this.state.Village}
      onValueSelected={Village => this.setState({Village})}
    />
  );
  FieldStreet = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Street"
      data={this.state.APIData.StreetList}
      labelProp="STREET"
      valueProp="CID"
      selectedValue={this.state.Street}
      onValueSelected={Street => this.setState({Street})}
    />
  );
  FieldCountry = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Country"
      data={this.state.APIData.CountryList}
      labelProp="COUNTRY"
      valueProp="CID"
      selectedValue={this.state.Country}
      onValueSelected={Country => this.setState({Country})}
    />
  );
  FieldCity = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="City"
      data={this.state.APIData.CityList}
      labelProp="CITY"
      valueProp="CID"
      selectedValue={this.state.City}
      onValueSelected={City => this.setState({City})}
    />
  );
  FieldState = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="State"
      data={this.state.APIData.StateList}
      labelProp="STATE"
      valueProp="CID"
      selectedValue={this.state.State}
      onValueSelected={State => this.setState({State})}
    />
  );
  FieldZip = key => (
    <RichTextInput
      key={key}
      placeholder="Zip"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Zip}
      onChangeText={Zip => this.setState({Zip})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Zip'}
    />
  );
  FieldLandline1 = key => (
    <RichTextInput
      key={key}
      placeholder="LandLine1"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.LandLine1}
      onChangeText={LandLine1 => this.setState({LandLine1})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'LandLine1'}
    />
  );
  FieldLandline2 = key => (
    <RichTextInput
      key={key}
      placeholder="LandLine2"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.LandLine2}
      onChangeText={LandLine2 => this.setState({LandLine2})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'LandLine2'}
    />
  );
  FieldFax = key => (
    <RichTextInput
      key={key}
      placeholder="Fax"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Fax}
      onChangeText={Fax => this.setState({Fax})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Fax'}
    />
  );
  FieldEmail = key => (
    <RichTextInput
      key={key}
      placeholder="Email"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Email}
      onChangeText={Email => this.setState({Email})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Email'}
    />
  );
  FieldRemarks = key => (
    <RichTextInput
      key={key}
      placeholder="Remarks"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Remarks}
      onChangeText={Remarks => this.setState({Remarks})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Remarks'}
    />
  );
  FieldTransporter = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Transporter"
      data={this.state.APIData.TransporterList}
      labelProp="Name"
      valueProp="Code"
      selectedValue={this.state.Transporter}
      onValueSelected={Transporter => this.setState({Transporter})}
    />
  );
  FieldDate = key => (
    <ModalDatePicker
      placeholder={'Date'}
      selectedValue={this.state.Date}
      onValueSelected={(selectedDate) => this.HandleDateChange('Date',selectedDate ) }
    />
  );
  FieldPAN = key => (
    <RichTextInput
      key={key}
      placeholder="PAN"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Pan}
      onChangeText={Pan => this.setState({Pan})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Pan'}
    />
  );
  FieldAgent = key => (
    <ModalSearchablePicker
      key={key}
      //style={styles.FieldInput}
      placeholder="Agent"
      data={this.state.APIData.AgentList}
      labelProp="Name"
      valueProp="Code"
      selectedValue={this.state.Agent}
      onValueSelected={Agent => this.setState({Agent})}
    />
  );
  FieldPaymentTerms = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Payment Terms"
      data={this.state.APIData.PaymentTermList}
      labelProp="TERMNAME"
      valueProp="ID"
      selectedValue={this.state.PaymentTerms}
      onValueSelected={PaymentTerms => this.setState({PaymentTerms})}
    />
  );
  FieldMandatory = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginTop: 20,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Mandatory
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={Mandatory => this.setState({Mandatory})}
          value={this.state.Mandatory}
        />
      </View>
    </View>
  );
  FieldTaxName = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Tax Name"
      data={this.state.APIData.TaxNameList}
      labelProp="TaxName"
      valueProp="TaxCode"
      selectedValue={this.state.TaxName}
      onValueSelected={TaxName => this.setState({TaxName})}
    />
  );
  FieldDiscount = key => (
    <RichTextInput
      key={key}
      placeholder="Discount (%)"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.Discount}
      onChangeText={Discount => this.setState({Discount})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'Discount'}
    />
  );
  FieldAgentCommission = key => (
    <RichTextInput
      key={key}
      placeholder="Agent Commission"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.AgentCommission}
      onChangeText={AgentCommission => this.setState({AgentCommission})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'AName'}
    />
  );
  FieldJobberRate = key => (
    <RichTextInput
      placeholder="Jobber Rate"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.JobberRate}
      onChangeText={JobberRate => this.setState({JobberRate})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'JobberRate'}
    />
  );
  FieldCreditLimit = key => (
    <RichTextInput
      key={key}
      placeholder="Credit Limit (Rs)"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.CreditLimit}
      onChangeText={CreditLimit => this.setState({CreditLimit})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'CreditLimit'}
    />
  );
  FieldCreditDays = key => (
    <RichTextInput
      key={key}
      placeholder="Credit Days"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.CreditDays}
      onChangeText={CreditDays => this.setState({CreditDays})}
      keyboardType="numeric"
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'CreditDays'}
    />
  );
  FieldSalesType = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Sales Type"
      data={this.state.APIData.SalesTypeList}
      labelProp="SalesType"
      valueProp="SalesType"
      selectedValue={this.state.SalesType}
      onValueSelected={SalesType => this.setState({SalesType})}
    />
  );
  FieldGstType = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Gst Type"
      data={this.state.GstTypeList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.GstType}
      onValueSelected={GstType => this.setState({GstType})}
    />
  );
  FieldGstNo = key => (
    <RichTextInput
      key={key}
      placeholder="GST No"
      onBlur={this.HandleRateBlur}
      //style={styles.FieldInput}
      value={this.state.GstNo}
      fieldWrapperStyle={{width: '48%'}}
      onChangeText={GstNo => this.setState({GstNo})}
      returnKeyType="next"
      autoCorrect={false}
      autoCapitalize="none"
      ref={'GstNo'}
    />
  );
  FieldSingle = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          {this.state.LocationType ? 'Multiple' : 'Single'}
        </Text>
        <Switch
          // style={styles.TypeSwitch}
          fieldWrapperStyle={{width: '48%'}}
          onValueChange={LocationType => this.setState({LocationType})}
          value={this.state.LocationType}
        />
      </View>
    </View>
  );
  FieldLogisticinward = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Logistic InWard
        </Text>
        <Switch
          style={styles.TypeSwitch}
          onValueChange={LogisticIn => this.setState({LogisticIn})}
          value={this.state.LogisticIn}
        />
      </View>
    </View>
  );
  FieldLogisticOutWard = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Logistic OutWard
        </Text>
        <Switch
          style={styles.TypeSwitch}
          onValueChange={LogisticOut => this.setState({LogisticOut})}
          value={this.state.LogisticOut}
        />
      </View>
    </View>
  );
  FieldCourierInward = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Courier InWard
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={CourierIn => this.setState({CourierIn})}
          value={this.state.CourierIn}
        />
      </View>
    </View>
  );
  FieldCourierOutward = key => (
    <View key={key} style={layoutStyle.FieldLayout}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Courier OutWard
        </Text>
        <Switch
          style={styles.TypeSwitch}
          onValueChange={CourierOut => this.setState({CourierOut})}
          value={this.state.CourierOut}
        />
      </View>
    </View>
  );
  FieldGatewayInward = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          GateWay InWard
        </Text>
        <Switch
          style={styles.TypeSwitch}
          onValueChange={GateIn => this.setState({GateIn})}
          value={this.state.GateIn}
        />
      </View>
    </View>
  );
  FieldGatewayOutward = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          GateWay OutWard
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={GateOut => this.setState({GateOut})}
          value={this.state.GateOut}
        />
      </View>
    </View>
  );
  FieldSalesForcasting = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Sales Forcasting
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={SalesForcasting => this.setState({SalesForcasting})}
          value={this.state.SalesForcasting}
        />
      </View>
    </View>
  );
  FieldReplenisment = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Replenisment
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={Replenisment => this.setState({Replenisment})}
          value={this.state.Replenisment}
        />
      </View>
    </View>
  );
  FieldExtinct = key => (
    <View key={key} style={[layoutStyle.FieldLayout]}>
      <View
        style={[
          layoutStyle.FieldContainer,
          {
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          },
        ]}>
        <Text
          numberOfLines={1}
          style={[
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {marginLeft: 10, marginRight: 15},
          ]}>
          Extinct
        </Text>
        <Switch
          style={styles.TypeSwitch1}
          onValueChange={IsCashSales => this.setState({IsCashSales})}
          value={this.state.IsCashSales}
        />
      </View>
    </View>
  );

  refreshLayout = () => this.forceUpdate();
  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <ICubeAIndicator />
        </>
      );
    }

    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.oppPrimary}
          onLayoutChanged={this.refreshLayout}>
          <View style={styles.Tabs}>
            {/* Details */}
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 0)}
              style={[styles.Tab]}>
              <Text
                numberOfLines={1}
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Details
              </Text>
            </TouchableOpacity>
            {/* Contact */}
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 1)}
              style={[styles.Tab]}>
              <Text
                numberOfLines={1}
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Contact
              </Text>
            </TouchableOpacity>
            {/* Tax */}
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 2)}
              style={[styles.Tab]}>
              <Text
                numberOfLines={1}
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    this.state.ActiveTab === 2
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 2
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Tax
              </Text>
            </TouchableOpacity>
            {/* {(this.state.PartType === 'SUPPLIER' || this.state.PartType === 'CUSTOMER') && (  */}
            {/* SUPPLIER */}
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 3)}
              style={[styles.Tab]}>
              <Text
                numberOfLines={1}
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    this.state.ActiveTab === 3
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 3
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Supplier
              </Text>
            </TouchableOpacity>
            {/* )}  */}
            {/* Others */}
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 4)}
              style={[styles.Tab]}>
              <Text
                numberOfLines={1}
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    this.state.ActiveTab === 4
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 4
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Others
              </Text>
            </TouchableOpacity>
          </View>
          <ViewPager
            style={styles.ViewPager}
            initialPage={0}
            onPageSelected={this.HighlightSelectedTab}
            ref={'ViewPager'}>
            <View style={styles.InvoiceTabWrapper}>
              <ScrollView>
                <View style={layoutStyle.ScrollContentWrapper}>
                  {this.FieldSupplierCode()}
                  {this.FieldName()}
                  {this.FieldAliasName()}
                  {this.FieldLastAliasName()}
                  {this.state.PartType !== 'CUSTOMER' &&
                    this.FieldPrimaryGroup()}
                  {this.state.PartType === 'CUSTOMER' && this.FieldSalesType()}
                  {this.state.PartType !== 'CUSTOMER' &&
                    this.FieldGeneralLedger()}
                  {this.FieldRating()}
                  {this.FieldPriceRule()}
                  {this.state.PartType === 'CUSTOMER' &&
                    this.FieldCalculateOn()}
                  {this.FieldGstType()}
                  {this.state.GstType === 'Registered' && this.FieldGstNo()}
                  {this.state.PartType !== 'CUSTOMER' && this.FieldCurrency()}
                </View>
              </ScrollView>
            </View>
            {/* Contact */}
            <View>
              <ScrollView>
                <View style={layoutStyle.ScrollContentWrapper}>
                  {this.FieldContactName()}
                  {this.FieldMobileNo()}
                  {this.FieldWhatsappNo()}
                  {this.FieldISDNo()}
                  {this.FieldPlotNo()}
                  {this.FieldVillage()}
                  {this.FieldStreet()}
                  {this.FieldCountry()}
                  {this.FieldCity()}
                  {this.FieldState()}
                  {this.FieldZip()}
                  {this.FieldLandline1()}
                  {this.FieldLandline2()}
                  {this.FieldFax()}
                  {this.FieldEmail()}
                  {this.FieldRemarks()}
                </View>
              </ScrollView>
            </View>
            {/* Tax */}
            <View>
              <ScrollView>
                <View style={layoutStyle.ScrollContentWrapper}>
                  {this.state.PartType !== 'AGENT' && this.FieldPAN()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'TRANSPORTER' ||
                    this.state.PartType === 'JOBBER') &&
                    this.FieldDate()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldTransporter()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldAgent()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'AGENT' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldAgentCommission()}
                  {this.state.PartType === 'JOBBER' && this.FieldJobberRate()}
                </View>
              </ScrollView>
            </View>
            {/* Supplier */}
            <View>
              <ScrollView>
                <View style={layoutStyle.ScrollContentWrapper}>
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldTaxName()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldDiscount()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldCreditLimit()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldCreditDays()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldPaymentTerms()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldMandatory()}
                </View>
              </ScrollView>
            </View>
            {/* Others */}
            <View>
              <ScrollView>
                <View style={layoutStyle.ScrollContentWrapper}>
                  {this.state.PartType !== 'CUSTOMER' && this.FieldSingle()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldLogisticinward()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldLogisticOutWard()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldCourierInward()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldCourierOutward()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldGatewayInward()}
                  {(this.state.PartType === 'SUPPLIER' ||
                    this.state.PartType === 'CUSTOMER') &&
                    this.FieldGatewayOutward()}
                  {this.state.PartType === 'SUPPLIER' &&
                    this.FieldSalesForcasting()}
                  {this.state.PartType === 'SUPPLIER' &&
                    this.FieldReplenisment()}
                  {this.FieldExtinct()}
                </View>
              </ScrollView>
            </View>
          </ViewPager>
        </LayoutWrapper>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    marginTop: '80%',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  SwitchContainer: {
    flexDirection: 'row',
    marginTop: 15,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  ViewPager: {
    flex: 1,
  },

  SwitchContainer1: {
    flexDirection: 'row',

    alignItems: 'center',
    justifyContent: 'space-between',
  },
  TabView: {
    paddingTop: 10,
    paddingBottom: 10,
    // borderTopWidth: 2,
    width: '100%',
    textAlign: 'center',
  },
  Card: {},

  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  Tabs: {
    flexDirection: 'row',
    //backgroundColor: 'rgb(221, 221, 221)',
    // marginBottom: 10,
  },
  Tab: {
    //paddingHorizontal: 10,
    //paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // marginHorizontal: 3,
    // marginTop: 5,
    width: '20%',
    //backgroundColor: 'rgb(221, 221, 221)',
  },
  ActiveTab: {
    width: '100%',
    textAlign: 'center',
  },
  Loadercontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  FieldLabelToPay: {
    flex: 1,
    alignSelf: 'center',
  },
  SwitchFieldContainer: {
    flexDirection: 'row',
    position: 'relative',
    // borderBottomWidth: 2,
    // borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingVertical: 5,
    marginVertical: 8,
  },
  FieldContainer: {
    width: '100%',
    marginHorizontal: 4,
    marginVertical: 2.5,
    overflow: 'visible',
    padding: 0,
  },

  FieldContainer1: {
    width: '100%',
    marginHorizontal: 4,
    marginVertical: 2.5,
    overflow: 'visible',
    padding: 0,
  },
  FieldLabel: {
    flex: 1,
    alignSelf: 'center',
    position: 'absolute',
    top: 0,
    left: 5,
  },
  TypeSwitch1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  FieldInput1: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    textAlign: 'left',
  },
  FieldInput: {
    flex: 2,
    height: 40,
    alignSelf: 'center',
    marginTop: 8,
  },
  container: {
    flex: 1,
  },
});
// export {Retail};
