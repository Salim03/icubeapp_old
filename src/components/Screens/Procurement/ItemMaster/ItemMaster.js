import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  Picker,
  ScrollView,
  Keyboard,
  Button,
  TouchableOpacity,
  Modal,
  Switch,
  Image,
  TouchableHighlight,
  BackHandler,
  Platform,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import ViewPager from '@react-native-community/viewpager';
import ImagePicker from 'react-native-image-picker';

import ModalDatePicker from '../../../Custom/ModalDatePicker';
import RichTextInput from '../../../Custom/RichTextInput';
import Accordion, {AccordionStyle} from '../../../Custom/Accordion';
import {
  AlertMessage,
  AlertError,
  AlertStatusError,
  Confirmation,
  dismissKeyboard,
  Request,
  getLayoutImageDimension,
  ShowSuccessToastMsg,
  DocumentDirectoryFolder,
} from '../../../../Helpers/HelperMethods';
import LayoutWrapper, {layoutStyle} from '../../../Layout/Layout';
import ModalSearchablePicker from '../../../Custom/ModalSearchablePicker';

// to hold the Image Dimension value
let ImageDimension;

export default class ItemMaster extends Component {
  constructor() {
    super();
    // class variables
    this.Icode = '';
    this.visibleProductGroupFields = [];
    this.visibleCategoryGroupFields = [];
    this.visiblePriceGroupFields = [];

    // Group Refs
    this.PriceGroupRefs = {};

    // Calculating Today's date
    let dt = new Date();
    let TodayDate = `${dt.getFullYear()}-${('0' + (dt.getMonth() + 1)).slice(
      -2,
    )}-${('0' + dt.getDate()).slice(-2)}`;

    this.APILink = {
      API_LoadInitialData: `/api/ItemMaster/LoadInitialData?`, //LocCode=1&CompanyCode=1&PartyCode=S140&LoginName=sa
      API_LoadAllDepartment: `/api/ItemMaster/GetDepartmentItemCreationForGRN?`, //LocCode=1&CompanyCode=1&PartyCode=S140
      API_LoadAllItemName: `/api/ItemMaster/GetItemNameCollectionDetails?`, //LoginName=sa
      API_LoadDetailsForSelectedDepartment: `/api/ItemMaster/GetInventoryGroupByGRPCode?`, //GRPCode=
      API_CreateItem: `/api/ItemMaster/SaveCreatedItem?`, //isGroupItem=true&GroupName=TestColor&GroupCode=0&SelectedCatCode=5&Loccode=1&StockCode=1
      API_LoadSelectedItemForEdit: `/api/ItemMaster/GetSelectedItemForEdit?`, //EntID=800
      API_CheckGroupForSelectedValue: `/api/ItemMaster/CheckGroupForSelectedValue?`, //GroupID=GN0042
    };
    this.state = {
      isFetched: false,
      // Properties to hold API data
      APIData: {
        DepartmentList: [],
        ItemNameList: [],
        Cat1: [],
        Cat2: [],
        Cat3: [],
        Cat4: [],
        Cat5: [],
        Cat6: [],
        Cat7: [],
        Cat8: [],
        Cat9: [],
        Cat10: [],
        Cat11: [],
        Cat12: [],
        Cat13: [],
        Cat14: [],
        Cat15: [],
        Cat16: [],
      },
      // Properties to hold category names to set to the labels ( Example  1: "Brand", 2: "Size" )
      CategoryNames: {
        1: '',
        2: '',
        3: '',
        4: '',
        5: '',
        6: '',
        7: '',
        8: '',
        9: '',
        10: '',
        11: '',
        12: '',
        13: '',
        14: '',
        15: '',
        16: '',
      },
      // properties to hold the values of the controls
      // DeptCode , ItemCode - should be generated automatically
      DeptCode: '',
      ItemCode: '',
      // Product
      Department: '',
      ItemName: '',
      OemCode: '',
      Tax: '',
      UOM: '',
      MfgDate: TodayDate, // Setting Today's date by default
      ExpDate: TodayDate, // Setting Today's date by default
      // Categories - Cat1 is the code && CatName1 is the name ( Example Cat1: 5, CatName1: "Peter England" )
      Cat1: '',
      CatName1: '',
      Cat2: '',
      CatName2: '',
      Cat3: '',
      CatName3: '',
      Cat4: '',
      CatName4: '',
      Cat5: '',
      CatName5: '',
      Cat6: '',
      CatName6: '',
      Cat7: '',
      CatName7: '',
      Cat8: '',
      CatName8: '',
      Cat9: '',
      CatName9: '',
      Cat10: '',
      CatName10: '',
      Cat11: '',
      CatName11: '',
      Cat12: '',
      CatName12: '',
      Cat13: '',
      CatName13: '',
      Cat14: '',
      CatName14: '',
      Cat15: '',
      CatName15: '',
      Cat16: '',
      CatName16: '',
      // Mini Fields data
      Qty: '',
      Rate: '',
      Discount: '',
      isDiscountPercent: true, // whether calculate discount as Amount or Percentage ( Percentage by default )
      WSP: '',
      RSP: '', // Also refered to as MRP
      ListedMRP: '',
      OnlineMRP: '',

      // // Created Items List
      // CreatedItems: [],
      isProductCreated: false,
      // to expand or collapse the details
      ActiveTab: 0,

      // whether to create single product or groupwise product
      // dependencies for Group wise Product creation
      ShowGroupModal: false,
      CreateGroupwiseProduct: false,
      SelectedGroupCatNo: '',
      SelectedGroupCatLabel: '',
      SelectedGroupCatCode: '',
      SelectedGroupCatName: '',
      CurrentGroupData: [],
      GroupData: {}, // conatins Group data like { 1: [data...], 2: [data...] }									- this contains all the group data
      SelectedGroupData: {}, // conatins Selected Group data like { 1: [data...], 2: [data...] }			- this contains only the selected categories for specific group by the user
      GroupcatCode: {}, // conatins Group Category Code like { 1: 'GR001', 2: 'GR0003' }
      GroupcatName: {}, // conatins Group Category Name like { 1: 'TestGroupColor', 2: 'TestGroupWidth' }
      GroupcatLabel: {}, // conatins Group Category Label like { 1: 'Color', 2: 'Width' }
      GroupcatNo: [], // conatins Group Category Number like [1,2]

      // Images for Item
      ItemImages: [],

      // hold whether to show or hide camera
      ShowImagePreview: false,
      PreviewImagePath: '',

      // Whether user is selecting Images or not
      ImageSelection: false,
    };

    // To hold createdItem ENTID Array
    this.CreatedItemENTID = [];

    // to maintain the ByteArray data of the Images selected
    this.ItemImagesData = [];
  }

  componentDidMount() {
    // adding event listener for hardware back button press
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    // getting and setting navigation state
    this.getAndSetNavigationState();

    // fetching all the configured fields for this screen and Loading initial data will be fired after fetching the fields
    this.getConfiguredFields();
  }

  getAndSetNavigationState = () => {
    // getting all the navigation states
    const state = this.props.route;
    this.PartyCode = state.params['PartyCode'] || '';
    this.PartyName = state.params['PartyName'] || '';
    this.ENTID = state.params['ItemMasterID'] || 0;
    this.EditItemSeqNo = state.params['SeqNo'] || 0;
    this.ItemID = state.params['ItemID'] || 0;
    this.Icode = state.params['Icode'] || '';
    this.InvoiceInvDetailID = state.params['InvoiceInvDetailID'] || 0;

    const isCreate =
      this.ENTID && this.EditItemSeqNo ? 'update' : 'create' === 'create';
    const iconStyle = {width: 30, height: 30};
    const saveIcon = require('../../../../assets/images/save-light.png');
    const addIcon = isCreate
      ? require('../../../../assets/images/plus-light.png')
      : saveIcon;
    this.props.navigation.setOptions({
      headerRight: () => (
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[
              isCreate ? iconStyle : {width: 0, height: 0},
              {marginRight: 15, overflow: 'hidden'},
            ]}
            onPress={this.HandleFinishCreation}>
            <Image style={iconStyle} source={saveIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            style={[iconStyle, {marginRight: 15}]}
            onPress={this.ValidateEnrty}>
            <Image style={iconStyle} source={addIcon} />
          </TouchableOpacity>
        </View>
      ),
    });
  };

  RemoveBackButtonListener = () => {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  };

  getConfiguredFields = () => {
    Request.loadConfiguredFields('Item Master')
      .then(({Product, Category, Price}) => {
        this.visibleProductGroupFields = Product || [];
        this.visibleCategoryGroupFields = Category || [];
        this.visiblePriceGroupFields = Price || [];
      })
      .catch(err => AlertError(err))
      // calling LoadInitialData method in finally block to load all the data
      // it will get executed even if the method thrown any error
      .finally(this.LoadInitialData);
  };

  // Declaring variables to hold prepared List data
  TaxList;
  UOMList;

  LoadInitialData = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.Token = await AsyncStorage.getItem('Token');
    this.LoginName = await AsyncStorage.getItem('UserName');

    // Load all the initial data
    Request.get(
      `${this.IP}${this.APILink.API_LoadInitialData}LocCode=1&CompanyCode=1&PartyCode=${this.PartyCode}&LoginName=${this.LoginName}`,
      this.Token,
    )
      .then(res => {
        // console.log(res.data);
        if (res.status === 200)
          this.setState(
            prevState => ({
              isFetched: true,
              APIData: {
                ...prevState.APIData,
                DepartmentList: res.data['Department'],
                ItemNameList: res.data['ItemName'],
                Cat1: res.data['Cat1'],
                Cat2: res.data['Cat2'],
                Cat3: res.data['Cat3'],
                Cat4: res.data['Cat4'],
                Cat5: res.data['Cat5'],
                Cat6: res.data['Cat6'],
                Cat7: res.data['Cat7'],
                Cat8: res.data['Cat8'],
                Cat9: res.data['Cat9'],
                Cat10: res.data['Cat10'],
                Cat11: res.data['Cat11'],
                Cat12: res.data['Cat12'],
                Cat13: res.data['Cat13'],
                Cat14: res.data['Cat14'],
                Cat15: res.data['Cat15'],
                Cat16: res.data['Cat16'],
              },
            }),
            () => {
              this.PrepareListDataForInitialData(res.data);
              this.LoadSelectedItemForEdit(this.ENTID);
            },
          );
        else AlertStatusError(res);
      })
      .catch(AlertError);
  };

  LoadSelectedItemForEdit = ENTID => {
    // return the function if no item selected
    if (ENTID === 0) return;

    // Getting Information of Selected Item
    Request.get(
      `${this.IP}${this.APILink.API_LoadSelectedItemForEdit}EntID=${ENTID}`,
      this.Token,
    )
      .then(res => {
        // console.log("Item Detail", res.data);
        if (res.status === 200) {
          if (res.data.length) {
            // set state to fill data to the corresponding fields
            let data = res.data[0];
            let ItemImages =
              (data.IMAGEPATH &&
                data.IMAGEPATH.split(' * ').map(img => {
                  return {selected: false, path: img};
                })) ||
              [];
            this.setState({
              // DeptCode , ItemCode - should be generated automatically
              DeptCode: data.DeptCode || '',
              ItemCode: data.ItemCode || '',
              // Product
              Department: data.GRPCODE,
              ItemName: data.ITEM_NAME.toString(),
              OemCode: data.BARCODE.toString(),
              Tax: data.TAXCODE,
              UOM: data.UNITNAME,
              MfgDate: data.MfgDate,
              ExpDate: data.EXPIRY_DATE,
              Vendor: data.PARTYCODE,
              // Categories
              Cat1: data.CCODE1,
              CatName1: data.CNAME1,
              Cat2: data.CCODE2,
              CatName2: data.CNAME2,
              Cat3: data.CCODE3,
              CatName3: data.CNAME3,
              Cat4: data.CCODE4,
              CatName4: data.CNAME4,
              Cat5: data.CCODE5,
              CatName5: data.CNAME5,
              Cat6: data.CCODE6,
              CatName6: data.CNAME6,
              Cat7: data.CCODE7,
              CatName7: data.CNAME7,
              Cat8: data.CCODE8,
              CatName8: data.CNAME8,
              Cat9: data.CCODE9,
              CatName9: data.CNAME9,
              Cat10: data.CCODE10,
              CatName10: data.CNAME10,
              Cat11: data.CCODE11,
              CatName11: data.CNAME11,
              Cat12: data.CCODE12,
              CatName12: data.CNAME12,
              Cat13: data.CCODE13,
              CatName13: data.CNAME13,
              Cat14: data.CCODE14,
              CatName14: data.CNAME14,
              Cat15: data.CCODE15,
              CatName15: data.CNAME15,
              Cat16: data.CCODE16,
              CatName16: data.CNAME16,
              // Mini Fields data
              Qty: data.PART_QTY.toString(),
              Rate: data.RATE.toString(),
              Discount: data.DISCOUNT.toString(),
              WSP: data.WSP.toString(),
              RSP: data.MRP.toString(), // Also refered to as MRP
              ListedMRP: data.LISTED_MRP.toString(),
              OnlineMRP: data.OnlineMRP.toString(),
              ItemImages,
            });
          } else AlertMessage('No data found for the selected Item');
        } else AlertStatusError(res);
      })
      .catch(AlertError);
  };

  PrepareListDataForInitialData = data => {
    this.PrepareListDataToRender(
      data['Tax'],
      'DisplayName',
      'TaxCode',
      'TaxList',
    );
    this.PrepareListDataToRender(data['UOM'], 'UOMName', 'UOMID', 'UOMList');
    // Prepare Category name labels
    this.PrepareCategoryNames(data['CategoryCaptions']);
    // To make the prepared list to render manually
    this.forceUpdate();
  };

  PrepareCategoryNames = data => {
    let CategoryNames = {};
    for (let i = 0; i < 16; i++) {
      CategoryNames[`${i + 1}`] = data[i]['DisplayName'];
    }
    this.setState({CategoryNames});
  };

  PrepareItemImagesList = () => {
    return this.state.ItemImages.map((img, index) => (
      <View key={index} style={styles.ItemImageWrapper}>
        {this.state.ImageSelection && (
          <TouchableHighlight
            underlayColor={'rgba(0,0,0,0.5)'}
            onPress={this.ToggleImageSelection.bind(this, index)}
            style={styles.ImageSelector}>
            <View
              style={img.selected ? styles.SelectedImage : undefined}></View>
          </TouchableHighlight>
        )}
        <TouchableOpacity
          onPress={this.PreviewItemImage.bind(this, index)}
          onLongPress={this.ToggleImageSelection.bind(this, index)}>
          <Image style={styles.ItemImage} source={{uri: img.path}} />
        </TouchableOpacity>
      </View>
    ));
  };

  PrepareListDataToRender = (data, label, value, variableHolder) => {
    // this[variableHolder] = data.map((item, index) => <Picker.Item key={index} label={item[label]} value={item[value]} />);

    this[variableHolder] = data.map(item => ({
      label: item[label],
      value: item[value],
    }));
  };

  PreviewItemImage = index => {
    if (index > -1) {
      this.setState({
        ShowImagePreview: true,
        PreviewImagePath: this.state.ItemImages[index].path,
      });
    }
  };

  CloseImagePreview = () => {
    this.setState({ShowImagePreview: false, PreviewImagePath: ''});
  };

  ToggleImageSelection = index => {
    let ItemImages = [...this.state.ItemImages];
    ItemImages[index].selected = !ItemImages[index].selected;
    this.setState({ImageSelection: true, ItemImages});
  };

  RemoveSelectedImages = () => {
    let ItemImages = [...this.state.ItemImages].filter(item => !item.selected);
    this.setState({ImageSelection: false, ItemImages});
  };

  CancelImageSelection = () => {
    let ItemImages = [...this.state.ItemImages];
    ItemImages.forEach(itm => {
      itm.selected = false;
    });
    this.setState({ImageSelection: false});
  };

  FinishCreation = () => {
    if (this.CreatedItemENTID.length) {
      let ENTIDArr = this.CreatedItemENTID.map(item => item.ENTID);
      console.log('Final EntId List', ENTIDArr);
      this.RemoveBackButtonListener();
      this.props.navigation.goBack();
      if (this.ENTID)
        this.props.route.params.onUpdateItem(
          ENTIDArr,
          this.ENTID,
          this.EditItemSeqNo,
          this.ItemID,
          this.Icode,
          this.InvoiceInvDetailID,
        );
      else this.props.route.params.onFinishCreation(ENTIDArr);
    }
  };

  handleBackButtonClick = async () => {
    Confirmation('Do you want to go back ?', () => {
      this.RemoveBackButtonListener();
      this.props.navigation.goBack();
    });
    return false;
  };

  HandleDepartmentChange = Department => {
    // Get Selected Department details
    if (Department === '') {
      AlertMessage('Select Department');
      return;
    }

    Request.get(
      `${this.IP}${this.APILink.API_LoadDetailsForSelectedDepartment}GRPCode=${Department}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          if (res.data.length) {
            console.log('sdfs', res.data);
            this.setState({
              ItemName: res.data[0]['GRPNAME'],
              Tax: res.data[0]['TAXCODE'] || '',
              UOM: res.data[0]['UOM'] || '',
            });
          }
        } else AlertStatusError(res);
      })
      .catch(AlertError);
    this.setState({Department});
  };

  HandleTabsClick = ActiveTab => {
    Keyboard.dismiss();
    this.setState({ActiveTab});
    this.refs.ViewPager.setPage(ActiveTab || 0);
  };

  HandleFinishCreation = () => {
    this.state.isProductCreated
      ? this.FinishCreation()
      : AlertMessage('Create atleast one product');
  };

  HandleCategoriesChange = (code, CCODE, CNAME) => {
    // console.log(code, CCODE, CNAME);
    // console.log({ [`Cat${code}`]: CCODE, [`CatName${code}`]: CNAME });
    this.setState({
      [`Cat${code}`]: CCODE,
      [`CatName${code}`]: CNAME,
      ShowGroupModal: false,
      CreateGroupwiseProduct: false,
      SelectedGroupCatNo: '',
      SelectedGroupCatLabel: '',
      SelectedGroupCatCode: '',
      SelectedGroupCatName: '',
      CurrentGroupData: [],
    });
    if (CCODE === '' && CNAME === '') return;
    // AlertMessage(`Select ${this.state.CategoryNames[code] || `Category ${code}`}`);

    // Fetching Group data if user selected a group name
    Request.get(
      `${this.IP}${this.APILink.API_CheckGroupForSelectedValue}GroupID=${CCODE}`,
      this.Token,
    )
      .then(res => {
        // console.log("Group Data", res);
        if (res.status === 200) {
          // To check whether Group has been selected or not
          if (res.data.length)
            this.ApplyGroupWiseProductCreation(
              res.data,
              CCODE,
              CNAME,
              this.state.CategoryNames[code],
              code,
            );
        } else AlertStatusError(res);
      })
      .catch(AlertError);
  };

  ApplyGroupWiseProductCreation = (data, code, name, catName, catNo) => {
    Keyboard.dismiss();
    this.setState(
      {
        ShowGroupModal: true, // setting to show Group categories selection Modal
        SelectedGroupCatNo: catNo, // setting Currently selected group's category No
        SelectedGroupCatLabel: catName, // setting Currently selected group's category label
        SelectedGroupCatCode: code, // setting Currently selected group's Code
        SelectedGroupCatName: name, // setting Currently selected group's Name
        CurrentGroupData: data, // setting Currently selected group's data
      },
      () => console.log('ApplyGroupWiseProductCreation', this.state),
    );
  };

  ApplySelectedCategoriesForGroup = () => {
    // to set state if the user has selected categories for group Product
    let CurrentGroupData = [...this.state.CurrentGroupData];
    let CurrentSelectedGroupData = CurrentGroupData.filter(
      item => item.CatSelect === true,
    );

    if (!CurrentSelectedGroupData.length)
      AlertMessage(`Select atleast one ${this.state.SelectedGroupCatLabel}`);
    else {
      let SelectedGroupData = {...this.state.SelectedGroupData};
      let catNo = this.state.SelectedGroupCatNo;
      SelectedGroupData[this.state.SelectedGroupCatNo] =
        CurrentSelectedGroupData;
      this.setState(
        prevState => ({
          ShowGroupModal: false,
          CreateGroupwiseProduct: true,
          SelectedGroupCatLabel: '',
          SelectedGroupCatNo: 0,
          SelectedGroupCatCode: '',
          SelectedGroupCatName: '',
          CurrentGroupData: [],
          SelectedGroupData,

          GroupData: {...prevState.GroupData, [`${catNo}`]: CurrentGroupData}, // appending fetched group data into GroupData state like {1: [GroupDataArray for Cat1], 5: [GroupDataArray for Cat1]}
          SelectedGroupData: {
            ...prevState.SelectedGroupData,
            [`${catNo}`]: CurrentSelectedGroupData,
          }, // appending fetched group data into SelectedGroupData state as well like {1: [GroupDataArray for Cat1], 5: [GroupDataArray for Cat1]}
          GroupcatCode: {
            ...prevState.GroupcatCode,
            [`${catNo}`]: this.state.SelectedGroupCatCode,
          }, // appending Selected groupCode into GroupcatCode state like {1: 'GR0010', 5: 'GR0007'}
          GroupcatName: {
            ...prevState.GroupcatName,
            [`${catNo}`]: this.state.SelectedGroupCatName,
          }, // appending Selected groupName into GroupcatName state like {1: 'TestColor', 5: 'TestWidth'}
          GroupcatLabel: {
            ...prevState.GroupcatLabel,
            [`${catNo}`]: this.state.SelectedGroupCatLabel,
          }, // appending Selected group's Category label into GroupcatLabel state like {1: 'Color', 5: 'Width'}
          GroupcatNo: [...prevState.GroupcatNo, catNo], // appending Selected group's Category No into GroupcatNo state like [1,5]
        }),
        () => console.log('ApplySelectedCategoriesForGroup', this.state),
      );
    }
  };

  HideGroupModal = () => {
    this.setState({ShowGroupModal: false});
  };

  UpdateGroupCatSelection = (index, val) => {
    let CurrentGroupData = [...this.state.CurrentGroupData];
    CurrentGroupData[index].CatSelect = val;
    this.setState({CurrentGroupData});
  };

  ValidateEnrty = () => {
    let state = this.state;
    let allowSave = false;

    if (state.Department === '') AlertMessage('Select Department');
    else if (state.Qty == 0) AlertMessage('Quantity Cant Be Zero');
    else if (state.Qty === '') AlertMessage('Enter Quantity');
    else if (state.Tax === '') AlertMessage('Select Tax');
    else if (state.UOM === '') AlertMessage('Select UOM');
    else if (this.state.CreateGroupwiseProduct && !state.SelectedGroupData)
      AlertMessage(
        `Select atleast one ${state.GroupcatLabel} to create group product`,
      );
    else allowSave = true;

    // return from method if allowsave is false
    if (!allowSave) return;

    // Calculating provisional data... for saving Item
    let STD_RATE = 0,
      DiscountPercent = 0;
    if (state.isDiscountPercent) {
      DiscountPercent = (Math.round((state.Discount || 0) * 100) / 100).toFixed(
        2,
      );
      STD_RATE = state.Rate - state.Rate * (DiscountPercent / 100);
    } else {
      DiscountPercent = (
        Math.round((state.Discount / this.state.Rate) * 100 * 100) / 100
      ).toFixed(2); // calculating discount percent from discount amount
      STD_RATE = state.Rate - state.Discount;
    }

    let ImagePath = state.ItemImages.map(img => img.path).join(' * ');
    let ImageData = this.ItemImagesData.join('*');

    // Getting Data from state
    let ObjItem = [
      {
        ENTID: this.ENTID,
        // Categories data with CCODE and CNAME
        CNAME1: state.CatName1,
        CCODE1: state.Cat1,
        CNAME2: state.CatName2,
        CCODE2: state.Cat2,
        CNAME3: state.CatName3,
        CCODE3: state.Cat3,
        CNAME4: state.CatName4,
        CCODE4: state.Cat4,
        CNAME5: state.CatName5,
        CCODE5: state.Cat5,
        CNAME6: state.CatName6,
        CCODE6: state.Cat6,
        CNAME7: state.CatName7,
        CCODE7: state.Cat7,
        CNAME8: state.CatName8,
        CCODE8: state.Cat8,
        CNAME9: state.CatName9,
        CCODE9: state.Cat9,
        CNAME10: state.CatName10,
        CCODE10: state.Cat10,
        CNAME11: state.CatName11,
        CCODE11: state.Cat11,
        CNAME12: state.CatName12,
        CCODE12: state.Cat12,
        CNAME13: state.CatName13,
        CCODE13: state.Cat13,
        CNAME14: state.CatName14,
        CCODE14: state.Cat14,
        CNAME15: state.CatName15,
        CCODE15: state.Cat15,
        CNAME16: state.CatName16,
        CCODE16: state.Cat16,
        // Department Details
        GRPCODE: state.Department,
        ITEM_NAME: state.ItemName,
        DeptCode: state.DeptCode,
        ItemCode: state.ItemCode,
        PARTYCODE: this.PartyCode,
        PARTYNAME: this.PartyName,
        BARCODE: state.OemCode,
        TAXCODE: state.Tax,
        UNITNAME: state.UOM,
        BARUNIT: 1,
        MfgDate: state.MfgDate,
        ExpDate: state.ExpDate,
        PART_QTY: state.Qty || 0,
        RATE: state.Rate || 0,
        DISCOUNT: DiscountPercent || 0,
        STD_RATE: STD_RATE || 0,
        WSP: state.WSP || 0,
        RSP: state.RSP || 0,
        LISTED_MRP: state.ListedMRP || 0,
        OnlineMRP: state.OnlineMRP || 0,
        ImagePath: ImagePath,
        ImageData: '',
        MasterItem: true,
      },
    ];

    // Creating group wise Items if group cat selected
    if (state.CreateGroupwiseProduct) {
      let RootItem = [...ObjItem][0];
      console.log('Root Object ', RootItem);
      console.log('Group Info  :  ', state.GroupcatNo);
      console.log('SelectedGroupData Info  :  ', state.SelectedGroupData);
      console.log('GroupWiseItemsArray  :  ', this.GroupWiseItemsArra);
      this.GroupWiseItemsArray = [];
      // to create group wise Items
      this.CreateGroupWiseItems(
        [...state.GroupcatNo],
        state.SelectedGroupData,
        {...RootItem},
      );
      ObjItem = ObjItem.concat(this.GroupWiseItemsArray);
      console.log('GroupWiseItemsArray final :  ', this.GroupWiseItemsArra);
    }

    // setting ImageData to MasterItem only
    ObjItem[0].ImageData = ImageData;
    this.SaveCreatedItem(ObjItem);
  };

  GroupWiseItemsArray = [];

  CreateGroupWiseItems = (arr, arrObj, obj) => {
    /* 
		getting the current category No and current category array of Objects 
		LIKE 
			curCatNo = 1						- 		
			splice will remove Items from the index we given to it to the lenght we given 
			and will return the removed item from the array
			For Example ,
						"arr" has some values like [1,2,3]
						and we are going to take value "1" from the array like 

						curCatNo = (arr.splice(0, 1))[0]; 
						after executing this line 

						"curCatNo" will hold the value "1" and "arr" will hold the value [2,3]
						reason to take First Index of the spliced value is splice will return the removed elements as an array
						we are removing one Element from the array so we want the first element from the array returned

			curCatArr = arrObj["1"]     	- 		it is similar to do arrObj.1 in js
		*/
    let curCatNo = arr.splice(0, 1)[0];
    let curCatArr = arrObj[curCatNo];
    for (let j = 0; j < curCatArr.length; j++) {
      /* 
			Filling the Existing Object with the current Category value 
				Initially the "obj" will be empty LIKE {} at first time of recursion

				"obj" will hold some existing values 
				after executing the line it will update the corresponding value

			*/
      obj[`CNAME${curCatNo}`] = curCatArr[j].CNAME;
      obj[`CCODE${curCatNo}`] = curCatArr[j].CCODE;
      obj[`MasterItem`] = false;

      if (arr.length) {
        // calling the same method for recursion if there is another Group selected
        this.CreateGroupWiseItems([...arr], arrObj, obj);
      } else {
        // if there is no other group remaining then pushing the updated object in "GroupWiseItemsArray" array
        this.GroupWiseItemsArray.push({...obj});
      }
    }
  };

  SaveCreatedItem = async CreatedItem => {
    let state = this.state;
    let GroupCode = state.GroupcatNo.map(cat => state[`Cat${cat}`]).join(',');
    console.log(`My Temp data => ShowGroupModal : ${this.state.ShowGroupModal}, SelectedGroupCatNo : ${this.state.SelectedGroupCatNo}
		, SelectedGroupCatLabel : ${this.state.SelectedGroupCatLabel}, SelectedGroupCatCode : ${this.state.SelectedGroupCatCode}
		, SelectedGroupCatName : ${this.state.SelectedGroupCatName}, CurrentGroupData : ${this.state.CurrentGroupData}`);
    // Saving product
    Request.post(
      `${this.IP}${this.APILink.API_CreateItem}isGroupItem=${state.CreateGroupwiseProduct}&GroupCode=${GroupCode}&Loccode=1&StockCode=1`,
      CreatedItem,
      this.Token,
    )
      .then(res => res.json())
      .then(json => {
        console.log('Saved Item response', json);
        if (json.length) {
          this.CreatedItemENTID = this.CreatedItemENTID.concat(json);
          // // to add ENTID property to all created items
          // for (let i = 0, len = CreatedItem.length; i < len; i++)
          // 	CreatedItem[i]["ENTID"] = json[0].ENTID;

          this.setState(
            prevState => ({
              isProductCreated: true,
              ItemImages: [],
            }),
            () => {
              if ((this.ENTID, this.EditItemSeqNo)) {
                ShowSuccessToastMsg('Product updated successfully');
                this.FinishCreation();
              } else {
                ShowSuccessToastMsg('Product created successfully');
                this.ClearEntry();
              }
            },
          );
        } else AlertMessage('Something went wrong');
      })
      .catch(AlertError);
  };

  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };

  OpenImageCapture = () => {
    let filedir = DocumentDirectoryFolder.ProductImage;
    const options = {
      title: 'Select Item Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: filedir,
      },
      maxWidth: 500,
      maxHeight: 500,
      quality: 0.6,
      base64: true,
    };
    ImagePicker.showImagePicker(options, res => {
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        AlertMessage(`Something went wrong\n ${res.error}`);
      } else {
        let filepath =
          Platform.OS === 'android' ? 'file://' + res.path : '' + res.uri;
        console.log(
          'save image path : ',
          res.path,
          ' image uri : ',
          res.uri,
          ' Final Path : ',
          filepath,
        );
        let ItemImages = [
          ...this.state.ItemImages,
          {selected: false, path: filepath},
        ];
        this.ItemImagesData = this.ItemImagesData.concat(res.data);
        this.setState({ItemImages});
      }
    });
  };

  ClearEntry = () => {
    this.setState({
      DeptCode: '',
      ItemCode: '',
      // // Categories
      // Cat1: '', CatName1: '',
      // Cat2: '', CatName2: '',
      // Cat3: '', CatName3: '',
      // Cat4: '', CatName4: '',
      // Cat5: '', CatName5: '',
      // Cat6: '', CatName6: '',
      // Cat7: '', CatName7: '',
      // Cat8: '', CatName8: '',
      // Cat9: '', CatName9: '',
      // Cat10: '', CatName10: '',
      // Cat11: '', CatName11: '',
      // Cat12: '', CatName12: '',
      // Cat13: '', CatName13: '',
      // Cat14: '', CatName14: '',
      // Cat15: '', CatName15: '',
      // Cat16: '', CatName16: '',

      // Mini Fields data
      Qty: '',
      Rate: '',
      Discount: '',
      isDiscountPercent: true, // whether calculate discount as Amount or Percentage ( Percentage by default )
      WSP: '',
      RSP: '', // Also refered to as MRP
      ListedMRP: '',
      OnlineMRP: '',
      // CreateGroupwiseProduct: false,
    });
    this.ItemImagesData = [];
  };

  HighlightSelectedTab = event => {
    Keyboard.dismiss();
    this.setState({ActiveTab: event.nativeEvent.position});
  };

  HandleDiscountTypeChange = isDiscountPercent =>
    this.setState({isDiscountPercent});

  refreshLayout = () => {
    ImageDimension = getLayoutImageDimension();
    this.forceUpdate();
  };

  moveToNextElement = key => {
    try {
      const nextElem = this.PriceGroupRefs[`field${key + 1}`];
      nextElem && nextElem.focus();
    } catch (err) {}
  };

  FieldDepartment = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Department"
        data={this.state.APIData.DepartmentList}
        labelProp="Department"
        valueProp="GRPCODE"
        selectedValue={this.state.Department}
        onValueSelected={this.HandleDepartmentChange}
      />
    );
  };

  FieldItemName = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Item Name"
        data={this.state.APIData.ItemNameList}
        labelProp="ItemCollectionName"
        valueProp="ItemCollectionName"
        selectedValue={this.state.ItemName}
        defaultText={this.state.ItemName}
        onValueSelected={ItemName => this.setState({ItemName})}
        allowAddNew={true}
        newItemTextAsValue={true}
      />
    );
  };

  FieldOemCode = key => {
    return (
      <RichTextInput
        key={key}
        placeholder="Oem Code"
        value={this.state.OemCode}
        onChangeText={OemCode => this.setState({OemCode})}
        inputref={ref => {
          this.OemCodeRef = ref;
        }}
        inputProps={{returnKeyType: 'next'}}
      />
    );
  };

  FieldTax = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="Tax"
        data={this.TaxList || []}
        labelProp="label"
        valueProp="value"
        selectedValue={this.state.Tax}
        onValueSelected={Tax => this.setState({Tax})}
      />
    );
  };

  FieldUOM = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder="UOM"
        data={this.UOMList || []}
        labelProp="label"
        valueProp="value"
        selectedValue={this.state.UOM}
        onValueSelected={UOM => this.setState({UOM})}
      />
    );
  };

  FieldMfgDate = key => {
    return (
		<ModalDatePicker
        key={key}
        placeholder={'Mfg Date'}
        selectedValue={this.state.MfgDate}
        onValueSelected={(selectedDate) => this.HandleDateChange('MfgDate',selectedDate ) }
      />
    );
  };

  FieldExpDate = key => {
    return (
	<ModalDatePicker
        key={key}
        placeholder={'Exp Date'}
        selectedValue={this.state.ExpDate}
        onValueSelected={(selectedDate) => this.HandleDateChange('ExpDate',selectedDate ) }
      />
    );
  };

  FieldCat1 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['1'] || 'Category 1'}
        data={this.state.APIData.Cat1}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat1}
        defaultText={this.state.CatName1}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(1, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat2 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['2'] || 'Category 2'}
        data={this.state.APIData.Cat2}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat2}
        defaultText={this.state.CatName2}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(2, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat3 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['3'] || 'Category 3'}
        data={this.state.APIData.Cat3}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat3}
        defaultText={this.state.CatName3}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(3, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat4 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['4'] || 'Category 4'}
        data={this.state.APIData.Cat4}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat4}
        defaultText={this.state.CatName4}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(4, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat5 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['5'] || 'Category 5'}
        data={this.state.APIData.Cat5}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat5}
        defaultText={this.state.CatName5}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(5, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat6 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['6'] || 'Category 6'}
        data={this.state.APIData.Cat6}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat6}
        defaultText={this.state.CatName6}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(6, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat7 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['7'] || 'Category 7'}
        data={this.state.APIData.Cat7}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat7}
        defaultText={this.state.CatName7}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(7, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat8 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['8'] || 'Category 8'}
        data={this.state.APIData.Cat8}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat8}
        defaultText={this.state.CatName8}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(8, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat9 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['9'] || 'Category 9'}
        data={this.state.APIData.Cat9}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat9}
        defaultText={this.state.CatName9}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(9, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat10 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['10'] || 'Category 10'}
        data={this.state.APIData.Cat10}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat10}
        defaultText={this.state.CatName10}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(10, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat11 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['11'] || 'Category 11'}
        data={this.state.APIData.Cat11}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat11}
        defaultText={this.state.CatName11}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(11, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat12 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['12'] || 'Category 12'}
        data={this.state.APIData.Cat12}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat12}
        defaultText={this.state.CatName12}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(12, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat13 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['13'] || 'Category 13'}
        data={this.state.APIData.Cat13}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat13}
        defaultText={this.state.CatName13}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(13, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat14 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['14'] || 'Category 14'}
        data={this.state.APIData.Cat14}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat14}
        defaultText={this.state.CatName14}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(14, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat15 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['15'] || 'Category 15'}
        data={this.state.APIData.Cat15}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat15}
        defaultText={this.state.CatName15}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(15, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldCat16 = key => {
    return (
      <ModalSearchablePicker
        key={key}
        placeholder={this.state.CategoryNames['16'] || 'Category 16'}
        data={this.state.APIData.Cat16}
        labelProp="CNAME"
        valueProp="CCODE"
        selectedValue={this.state.Cat16}
        defaultText={this.state.CatName16}
        onValueSelected={(value, label) =>
          this.HandleCategoriesChange(16, value, label)
        }
        allowAddNew={true}
      />
    );
  };

  FieldQuantity = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          Quantity
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.Qty}
          onChangeText={Qty => this.setState({Qty})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldRate = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          Rate
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.Rate}
          onChangeText={Rate => this.setState({Rate})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldDiscount = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          Discount
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.Discount}
          // onFocus={this.HandleDiscountFocus}
          onChangeText={Discount => this.setState({Discount})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldDiscountPercentageOrAmount = key => {
    return (
      <View
        key={key}
        style={[
          layoutStyle.miniFieldContainer,
          layoutStyle.miniFieldLayout,
          {flexDirection: 'row', paddingTop: 10},
        ]}>
        <Switch
          value={this.state.isDiscountPercent}
          onValueChange={this.HandleDiscountTypeChange}
        />
        {/*  &nbsp; is for space , &#8377; is for rupees symbol */}
        {this.state.isDiscountPercent ? (
          <Text>&nbsp; %</Text>
        ) : (
          <Text>&nbsp; &#8377;</Text>
        )}
      </View>
    );
  };

  FieldWSP = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          WSP
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.WSP}
          onChangeText={WSP => this.setState({WSP})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldRSP = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          RSP
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.RSP}
          onChangeText={RSP => this.setState({RSP})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldListedMRP = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          Listed MRP
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.ListedMRP}
          onChangeText={ListedMRP => this.setState({ListedMRP})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  FieldOnlineMRP = key => {
    return (
      <View
        key={key}
        style={[layoutStyle.miniFieldContainer, layoutStyle.miniFieldLayout]}>
        <Text
          style={[layoutStyle.MiniInputFieldLabel, layoutStyle.FieldLabelFont]}>
          Online MRP
        </Text>
        <TextInput
          style={layoutStyle.MiniInputField}
          value={this.state.OnlineMRP}
          onChangeText={OnlineMRP => this.setState({OnlineMRP})}
          returnKeyType="next"
          autoCorrect={false}
          autoCapitalize="none"
          keyboardType="numeric"
          ref={ref => {
            this.PriceGroupRefs[`field${key}`] = ref;
          }}
          onSubmitEditing={this.moveToNextElement.bind(this, key)}
        />
      </View>
    );
  };

  render() {
    if (!this.state.isFetched) {
      return (
        <>
          <SafeAreaView style={styles.Loadercontainer}>
            <ActivityIndicator size="large" />
          </SafeAreaView>
        </>
      );
    }

    return (
      <LayoutWrapper onLayoutChanged={this.refreshLayout}>
        {this.Icode !== '' && (
          <View style={{width: '100%', paddingTop: 5, alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold'}}>{this.Icode || ''}</Text>
          </View>
        )}
        <View style={styles.Tabs}>
          <TouchableOpacity
            onPress={this.HandleTabsClick.bind(this, 0)}
            style={styles.Tab}>
            <Text
              style={[
                this.state.ActiveTab === 0
                  ? styles.ActiveTab
                  : styles.InActiveTab,
                layoutStyle.AppFont,
              ]}>
              Product
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.HandleTabsClick.bind(this, 1)}
            style={styles.Tab}>
            <Text
              style={[
                this.state.ActiveTab === 1
                  ? styles.ActiveTab
                  : styles.InActiveTab,
                layoutStyle.AppFont,
              ]}>
              Images
            </Text>
          </TouchableOpacity>
        </View>
        <ViewPager
          style={styles.ViewPager}
          initialPage={0}
          onPageSelected={this.HighlightSelectedTab}
          ref={'ViewPager'}>
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            style={styles.ScrollView}
            contentContainerStyle={{paddingBottom: 20}}>
            {/* Product Group */}
            <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
              <View style={[AccordionStyle.header]}>
                <Text
                  style={[
                    AccordionStyle.headerTitle,
                    layoutStyle.AppFont,
                    {paddingTop: 5},
                  ]}>
                  Product
                </Text>
              </View>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {this.visibleProductGroupFields.map((fd, ind) =>
                  this[`Field${fd.Field}`](ind),
                )}
              </View>
            </View>
            {/* Category Group */}
            {/* <Accordion title="Category" onCollapse={dismissKeyboard}
							contentWrapperStyle={layoutStyle.AccordionContentWrapper}
						> */}
            <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
              <View style={[AccordionStyle.header]}>
                <Text
                  style={[
                    AccordionStyle.headerTitle,
                    layoutStyle.AppFont,
                    {paddingTop: 5},
                  ]}>
                  Category
                </Text>
              </View>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {this.visibleCategoryGroupFields.map((fd, ind) =>
                  this[`Field${fd.Field}`](ind),
                )}
                {/* </Accordion> */}
              </View>
            </View>
            {/* Price Group */}
            {/* <Accordion title="Price" expandInitially={true} onCollapse={dismissKeyboard}
							contentWrapperStyle={[layoutStyle.AccordionContentWrapper]}
						> */}

            <View style={[AccordionStyle.wrapper, {paddingVertical: 10}]}>
              <View style={[AccordionStyle.header]}>
                <Text
                  style={[
                    AccordionStyle.headerTitle,
                    layoutStyle.AppFont,
                    {paddingTop: 5},
                  ]}>
                  Price
                </Text>
              </View>
              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {this.visiblePriceGroupFields.map((fd, ind) =>
                  this[`Field${fd.Field}`](ind),
                )}
                {/* </Accordion> */}
              </View>
            </View>
          </ScrollView>
          <View style={styles.ImagesTab}>
            {/* Captured Images list */}
            <ScrollView style={styles.ScrollView}>
              <View style={styles.ItemImagesWrapper}>
                {this.PrepareItemImagesList()}
              </View>
            </ScrollView>
            {/* Capture floating Button on bottom */}
            {!this.state.ImageSelection && (
              <TouchableOpacity
                style={styles.CaptureButton}
                onPress={this.OpenImageCapture}>
                <Image
                  style={{width: 30, height: 30}}
                  source={require('../../../../assets/images/camera-light.png')}
                />
              </TouchableOpacity>
            )}
            {this.state.ImageSelection && (
              <TouchableOpacity
                style={styles.RemoveImageButton}
                onPress={this.RemoveSelectedImages}>
                <Image
                  style={{width: 25, height: 25}}
                  source={require('../../../../assets/images/remove-light.png')}
                />
              </TouchableOpacity>
            )}
            {this.state.ImageSelection && (
              <TouchableOpacity
                style={styles.CancelSelectionButton}
                onPress={this.CancelImageSelection}>
                <Image
                  style={{width: 30, height: 30}}
                  source={require('../../../../assets/images/add-light.png')}
                />
              </TouchableOpacity>
            )}
          </View>
        </ViewPager>
        {/* Modal Popup for Group details starts here */}
        <Modal
          transparent={true}
          animationType="fade"
          visible={this.state.ShowGroupModal}>
          <View style={styles.ModalBackDrop}>
            <View style={styles.GroupDetailsWrapper}>
              <Text style={styles.GroupModalTitle}>
                Select {this.state.SelectedGroupCatLabel || 'Categories'}
              </Text>
              <ScrollView style={styles.GroupScrollViewWrapper}>
                {(this.state.CurrentGroupData || []).map((group, index) => {
                  return (
                    <View key={index} style={styles.GroupCatRow}>
                      <Text style={styles.GroupCatRowText}>{group.CNAME}</Text>
                      <Switch
                        onValueChange={this.UpdateGroupCatSelection.bind(
                          this,
                          index,
                        )}
                        value={group.CatSelect}
                      />
                    </View>
                  );
                })}
              </ScrollView>
              <View style={styles.GroupActionButtonWrapper}>
                <View style={{marginHorizontal: 10, width: 60}}>
                  <Button
                    title="Ok"
                    onPress={this.ApplySelectedCategoriesForGroup}
                  />
                </View>
                <View style={{marginHorizontal: 10, width: 80}}>
                  <Button title="Cancel" onPress={this.HideGroupModal} />
                </View>
              </View>
            </View>
          </View>
        </Modal>
        {/* Modal Popup for Group details ends here */}
        {/* Modal for Previewing ItemImage start here */}
        <Modal visible={this.state.ShowImagePreview} animationType={'slide'}>
          <View style={styles.ImagePreviewModal}>
            <View style={styles.ImagePreviewWrapper}>
              <Image
                style={styles.ImagePreviewer}
                source={{uri: this.state.PreviewImagePath}}
              />
            </View>
            <TouchableHighlight
              style={styles.ImagePreviewCloseWrapper}
              onPress={this.CloseImagePreview}>
              <Text style={styles.ImagePreviewCloseButton}>&times;</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        {/* Modal for Previewing ItemImage ends here */}
      </LayoutWrapper>
    );
  }
}

const styles = StyleSheet.create({
  Loadercontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Tabs: {
    flexDirection: 'row',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  ActiveTab: {
    color: '#0089ff',
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#0089ff',
    width: '100%',
    textAlign: 'center',
  },
  InActiveTab: {
    padding: 10,
    color: 'gray',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.15)',
    textAlign: 'center',
  },
  ViewPager: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  ScrollView: {
    flex: 1,
  },
  ImagesTab: {
    flex: 1,
  },
  CaptureButton: {
    width: 60,
    height: 60,
    backgroundColor: '#47b5ad',
    position: 'absolute',
    bottom: 30,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
  },
  ModalBackDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  GroupDetailsWrapper: {
    backgroundColor: 'white',
    width: '90%',
    height: 350,
    maxHeight: 350,
    borderRadius: 10,
    overflow: 'hidden',
  },
  GroupModalTitle: {
    fontSize: 20,
    textAlign: 'center',
    color: '#575575',
    marginVertical: 10,
  },
  GroupScrollViewWrapper: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.05)',
    marginHorizontal: 10,
    borderRadius: 5,
  },
  GroupActionButtonWrapper: {
    paddingVertical: 12,
    flexDirection: 'row-reverse',
  },
  GroupCatRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  GroupCatRowText: {
    fontSize: 18,
  },
  CustomControl: {
    width: '100%',
  },
  ItemImagesWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    flexWrap: 'wrap',
  },
  get ItemImageWrapper() {
    return {
      position: 'relative',
      width: ImageDimension || 100,
      height: ImageDimension || 100,
      backgroundColor: 'rgba(0,0,0,0.06)',
    };
  },
  get ItemImage() {
    return {
      margin: 3,
      width: (ImageDimension || 100) - 6,
      height: (ImageDimension || 100) - 6,
      zIndex: 1,
    };
  },
  ImagePreviewModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.9)',
  },
  ImagePreviewWrapper: {
    flex: 1,
  },
  ImagePreviewer: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  ImagePreviewCloseWrapper: {
    width: '100%',
    height: 60,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  ImagePreviewCloseButton: {
    fontSize: 25,
    textAlign: 'center',
    color: 'rgba(255,255,255,.5)',
  },
  SelectedImage: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 20,
    height: 20,
    borderRadius: 20,
    backgroundColor: 'skyblue',
    zIndex: 10,
  },
  ImageSelector: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 9,
  },
  RemoveImageButton: {
    width: 60,
    height: 60,
    backgroundColor: 'rgba(50,74,84,1)',
    position: 'absolute',
    bottom: 110,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
  },
  CancelSelectionButton: {
    width: 60,
    height: 60,
    backgroundColor: 'rgba(50,74,84,1)',
    position: 'absolute',
    bottom: 30,
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
    elevation: 10,
    transform: [{rotateZ: '45deg'}],
  },
});
