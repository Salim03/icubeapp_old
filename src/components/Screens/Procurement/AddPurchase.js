/* eslint-disable react-native/no-inline-styles */
// Module Imports
import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  Modal,
  Keyboard,
  BackHandler,
} from 'react-native';
import ModalSearchableLabel from '../../Custom/ModalSearchableLabel';

import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import ViewPager from '@react-native-community/viewpager';

import ModalDatePicker from '../../Custom/ModalDatePicker';
import Menu, {MenuItem} from 'react-native-material-menu';

// Local Imports
import ImgDotsMenu from '../../../assets/images/dot-menu-grey.png';
import RichTextInput from '../../Custom/RichTextInput';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import {
  Confirmation,
  AlertMessage,
  AlertError,
  AlertStatusError,
  Request,
  getPropsFromArray,
  GetSessionData,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';

const ListItemHeader = React.memo(({headers, isAgainst}) => {
  const {CardItemLayout} = CardStyle;
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        <View style={[styles.CardTextContainer, {width: 90}, CardItemLayout]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            ICODE
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: 260}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Item Name
          </Text>
        </View>
        {headers.map(head => (
          <View key={head} style={[styles.CardTextContainer, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              )}>
              {head || ''}
            </Text>
          </View>
        ))}
        {isAgainst && (
          <>
            <View style={[styles.CardTextContainer, CardItemLayout]}>
              <Text
                numberOfLines={1}
                style={ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                )}>
                Ordered
              </Text>
            </View>
            <View style={[styles.CardTextContainer, CardItemLayout]}>
              <Text
                numberOfLines={1}
                style={ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                )}>
                Pending
              </Text>
            </View>
          </>
        )}
      </View>
    </View>
  );
});

const ListItem = props => {
  let data = props.data;
  let index = props.index;
  let AgainstAllowEdit = !(props.isAgainst && !props.allowEdit); // based on isAgainst and AllowEdit Settings
  const {CardItemLayout} = CardStyle;
  return (
    <TouchableWithoutFeedback
      onLongPress={() =>
        AgainstAllowEdit &&
        props.HandleEditProduct(
          index,
          data.ItemMasterID,
          data.SeqNo,
          data.ItemID,
          data.Icode,
          data.InvoiceInvDetailID,
        )
      }>
      <View
        style={[
          styles.Card,
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
        ]}>
        <View style={[styles.CardLine]}>
          <View style={[styles.CardTextContainer, {width: 90}, CardItemLayout]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Barcode || data.Icode}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: 260}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {data.Description}
            </Text>
          </View>
          {props.headers.map(field => (
            <View
              key={field}
              style={[styles.CardTextContainer, CardItemLayout]}>
              <Text
                numberOfLines={1}
                style={ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                )}>
                {data[field]}
              </Text>
            </View>
          ))}
          {props.isAgainst && (
            <>
              <View style={[styles.CardTextContainer, CardItemLayout]}>
                <Text
                  numberOfLines={1}
                  style={ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  )}>
                  {(Math.round(data.OrderQty * 100) / 100).toFixed(2)}
                </Text>
              </View>
              <View style={[styles.CardTextContainer, CardItemLayout]}>
                <Text
                  numberOfLines={1}
                  style={ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  )}>
                  {(Math.round(data.PendingQty * 100) / 100).toFixed(2)}
                </Text>
              </View>
            </>
          )}
        </View>
        {AgainstAllowEdit && (
          <View style={styles.CardBtnOptionWrapper}>
            <TouchableOpacity
              style={styles.CardBtnOption}
              onPress={() => props.HandleShowMenu(index)}>
              <Text
                style={[
                  styles.CardBtnOptionText,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.mediam.mm,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                &#8964;
              </Text>
            </TouchableOpacity>
            <Menu
              ref={props.setMenuRef}
              button={<Text style={{width: 0, height: 0}} />}>
              <MenuItem
                onPress={() =>
                  props.HandleEditProduct(
                    index,
                    data.ItemMasterID,
                    data.SeqNo,
                    data.ItemID,
                    data.Icode,
                    data.InvoiceInvDetailID,
                  )
                }>
                Edit
              </MenuItem>
              <MenuItem onPress={() => props.HandleRemoveProduct(index)}>
                Remove
              </MenuItem>
            </Menu>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default class AddPurchase extends Component {
  // Declaring class variables
  SummaryDetails;
  visibleProductGroupFields = [];
  visibleCreatedItemsGroupFields = [];
  visibleCreatedItemsGroupCaptions = [];

  constructor(props) {
    super(props);

    // Local Object to store all the navigation and other informations
    this.LocalData = {
      CallFrom: '',
      Title: '',
      InvoiceNo: '',
      InvoiceID: 0,
      StatusFlag: 0,
      Status: '',
      InvoiceDate: '',
      Location: '1',
      UserCode: '',
      FinancialYearId: '0',
      SearchText: '',
      show: false,
    };
    this.APILink = {
      API_LoadInitialData: '/api/Purchase/LoadInitialData?', //CallFrom=PO&LocCode=1
      API_AgainstList: '/api/Purchase/GetAgaintDataSourceList?', //CallFrom={CallFrom}&Vendor={Vendor}&InvoiceNo={InvoiceNo}&InvoiceType={InvoiceType}&BasedOn={BasedOn}&LocCode={LocCode}`
      API_SelectedAgainst: '/api/Purchase/GetSelectedAgainstInvoiceDetails?', //CallFrom=PO&GetTypeID=6&GetAgainstID=100012
      API_PaymentTermList: '/api/Purchase/GetAllPaymentTermList?', //CallFrom=PO&Vendor=&LocCode=1
      API_LogisticList: '/api/Purchase/GetAllLogistic?', //CallFrom={CallFrom}&Vendor={Vendor}&InvoiceNo={InvoiceNo}&LocCode={LocCode}
      API_ScanBarcode: '/api/Purchase/GetItemBindDataForSearchItemInfo?', //CallFrom=PO&PassStockCode=1&PassVendor=&PassSearchItemInfo=M0443&GetEntIDList=23845&GetMaxSeqNo=1&IsScanSetcode=false&GetSeqnoList=1&LocCode=1
      API_CreatedItemByID: '/api/Purchase/GetCreatedInvoiceDetails?', //CallFrom=PO&PassInvoiceMasterIdList='1245^4565^'&SeqNo='4'
      API_EditedInvoiceItem: '/api/Purchase/GetEditedInvoiceDetails?', //CallFrom=PO&PassInvoiceMasterIdList='1245^4565^'
      API_EditedLotItem: '/api/Purchase/GetEditedLotDetails?', //CallFrom=PO&InvoiceID=&SeqNo=&InvoiceInvDetailID=&ValidateIsAgainstInvoice=&Srch_Against=&InvoiceType=&InvoiceNo=
      API_GenerateLotNo: '/api/Purchase/GenerateLotNo?', // ItemMasterID=831^&SeqNo=1^&InvDetailID=0^&Loccode=1
      API_SavePO: '/api/Purchase/SavePurchaseOrder', // To Save PO
      API_SaveGRN: '/api/Purchase/SaveGoodsReceiveNote', // To Save GRN
      API_SaveGRT: '/api/Purchase/SaveGoodsReturnNote', // To Save GRT
      API_SavePI: '/api/Purchase/SavePurchaseInvoice', // To Save PI
      API_SaveDN: '/api/Purchase/SaveDebitNote', // To Save DN
      API_DataToEditPO: '/api/Purchase/GetSelectedInvoiceDetails?', //CallFrom=PO&PassInvoiceID=2045&LocCode=1
      API_StatusChange: '/api/PoApproval/ApprovalSave?', //CallFrom=PO&PassInvoiceID=2045&LocCode=1
      API_ValidateAgainstRaised:
        '/api/Purchase/ValidateAgainstRaisedForInvoice?', //CallFrom=PO&passInvoiceID=50
    };
    this.state = {
      ShowModalLoader: false, // show or hide Save Modal popup
      ModalMessage: 'Saving Transaction',
      isFetched: false,
      // Holds the data to be set to controls ( filled later by the data from API )
      APIData: {
        InvoiceType: [],
        BasedOnList: [],
        AgainstList: [],
        VendorList: [],
        PaymentTermList: [],
        ReqCompanyList: [],
        LogisticList: [],
        TransporterList: [],
        AgentList: [],
        MerchandiserList: [],
        OrderModeList: [],
        CurrencyList: [],
        StockPointList: [],
        ScanBarcode: [],
      },
      // Hold selected or entered values of controls
      Type: 1,
      BasedOn: '',
      Against: '',
      Vendor: '',
      DocNo: '',
      DocDate: '',
      PaymentTerm: '',
      StockPoint: '',
      Remarks: '',
      ReqCompany: '',
      ValidFrom: '',
      ValidTo: '',
      Transporter: '',
      Agent: '',
      Currency: '',
      Logistic: '',
      Merchandiser: '',
      MerchandiserName: '',
      OrderMode: '',
      CurrentStatus: '1',

      // To Hold Active tab
      ActiveTab: 0,
      // ActiveTab: "Products",

      // To hold the calculated amount value
      BasicAmount: '0.00',
      TotalAmount: '0.00',

      // holds CreatedItems Array
      CreatedItems: [],
      ProductSearchText: '',

      // To hold Edit
      FocusedProductIndex: -1,
    };

    // to maintain the max SeqNo ( Initially its 1 )
    this.NextSeqNo = 1;

    // to maintain the CreatedItems source array
    this.CreatedItemsSource = [];

    // to maintain whether user selected an Against Type or not
    this.isAgainst = false;

    // to maintain the settings
    this.Settings = new Map(); // maintain the settings as Map instead of Object for better perfomance and usability
  }

  componentDidMount() {
    // Adding a event listener for hardware back press
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    let {params} = this.props.route;
    this.LocalData.Title = params.HeaderTitle;
    this.LocalData.CallFrom = params.CallFrom;
    this.LocalData.InvoiceID = params.InvoiceID || 0;
    // console.log(this.LocalData);

    this.props.navigation.setOptions({
      title: this.LocalData.Title,
      headerRight: () => (
        <View style={{flexDirection: 'row', paddingRight: 10}}>
          <TouchableOpacity
            style={{width: 30, height: 30}}
            onPress={this.ValidateInputs}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/save-light.png')}
            />
          </TouchableOpacity>

          {/* <TouchableOpacity
            style={{width: 30, height: 30}}
            onPress={this.ValidateInputs}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../../../assets/images/save-light.png')}
            />
          </TouchableOpacity> */}
        </View>
      ),
    });
    // Fetching all the configured fields and then calling LoadInitialData() inside that
    this.getConfiguredFields();
    // this.setState({ isFetched: true });
  }

  getConfiguredFields = () => {
    Request.loadConfiguredFields(this.LocalData.CallFrom)
      .then(({Product, CreatedItems}) => {
        this.visibleProductGroupFields = Product || [];
        // getting specific properties from the array and setting to its corresponding
        const {Field, Caption} = getPropsFromArray(CreatedItems, [
          'Field',
          'Caption',
        ]);
        this.visibleCreatedItemsGroupFields = Field;
        this.visibleCreatedItemsGroupCaptions = Caption;
      })
      .catch(err => AlertError(err))
      // calling LoadInitialData method in finally block to load all the data
      // it will get executed even if the method thrown any error
      .finally(this.LoadInitialData);
  };

  RemoveBackButtonListener = () => {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  };

  LoadInitialData = async () => {
    let {get_IP, get_Token, get_UserCode, get_Location, get_FinancialYearId} =
      await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.UserCode = get_UserCode;
    this.LocalData.Location = get_Location;
    this.LocalData.FinancialYearId = get_FinancialYearId;

    Request.get(
      `${this.IP}${this.APILink.API_LoadInitialData}CallFrom=${this.LocalData.CallFrom}&LocCode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          if (res.data) {
            this.setState(
              prevState => ({
                isFetched: true,
                APIData: {
                  ...prevState.APIData,
                  InvoiceType: res.data.InvoiceType || [],
                  BasedOnList: res.data.BasedOn || [],
                  ReqCompanyList: res.data.Locations || [],
                  TransporterList: res.data.Transpoter || [],
                  VendorList: res.data.Vendor || [],
                  AgentList: res.data.Agent || [],
                  MerchandiserList: res.data.Employee || [],
                  OrderModeList: res.data.OrderMode || [],
                  CurrencyList: res.data.Currency || [],
                  StockPointList: res.data.StockPoint || [],
                  LogisticList: res.data.Logistic || [],
                },
              }),
              () => {
                this.PrepareListForUnChangedData();

                if (this.LocalData.InvoiceID) {
                  this.LoadSelectedInvoice();
                } else {
                  this.forceUpdate();
                } // Force Update the view to render all the data
              },
            );
            // Setting the Settings to allow editing the Items
            this.LocalData.CallFrom === 'GRN' &&
              this.PrepareSettings(res.data.Settings || undefined);
          } else {
            AlertMessage('Something went wrong\nNo data found');
          }
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertMessage(JSON.stringify(err)));
  };

  LoadSelectedInvoice = () => {
    // fetching selected Invoice data
    Request.get(
      `${this.IP}${this.APILink.API_DataToEditPO}CallFrom=${this.LocalData.CallFrom}&PassInvoiceID=${this.LocalData.InvoiceID}&LocCode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          if (res.data.Table.length) {
            let PO = res.data.Table[0];
            // Setting Local Data
            let FullInvoiceDate = new Date(PO.InvoiceDate);
            this.LocalData.InvoiceID = PO.Id;
            this.LocalData.StatusFlag = PO.StatusFlag || 0;
            this.LocalData.Status = PO.Status || '';
            this.LocalData.InvoiceNo = PO.InvoiceNo;
            this.LocalData.InvoiceDate = `${(
              '0' + FullInvoiceDate.getDate()
            ).slice(-2)}/${('0' + (FullInvoiceDate.getMonth() + 1)).slice(
              -2,
            )}/${FullInvoiceDate.getFullYear()}`;
            // Setting state
            this.setState(
              {
                // Type: PO.Type || "",
                BasedOn: PO.BasedOn || '',
                Against: PO.AgainstID || '',
                Vendor: PO.VendorName || '',
                DocNo: PO.DocNo.toString(),
                DocDate: PO.DocDate || '',
                PaymentTerm: PO.PaymentTerm || '',
                StockPoint: PO.Stockpoint || '',
                Remarks: PO.Remarks.toString(),
                ReqCompany: PO.ReqLocationCode || '',
                ValidFrom: PO.ValidFrm || '',
                ValidTo: PO.ValidTill || '',
                Transporter: PO.TransportName || '',
                Agent: PO.AgentName || '',
                Currency: PO.CurrencyID || '',
                Logistic: PO.LogisticID || '',
                Merchandiser: PO.EmployeeID || '',
                OrderMode: PO.OrderMode || '',
                // Filling CreatedItems with Table1
                CreatedItems: res.data.Table1,
              },
              () => {
                this.HandleTypeChange(PO.Type); // Setting Type Explicitely to execute Against
                this.UpdateNextSeqNo();
                this.UpdateAmountCalculation();
                this.UpdateProductsSourceArray();
              },
            );
          } else {
            AlertMessage('Some went wrong\nNo data found for Invioce');
          }
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => {
        console.log(err);
        AlertMessage(JSON.stringify(err));
      });
  };

  PrepareSettings = settings => {
    this.Settings =
      settings && settings.reduce((a, c) => a.set(c.Key, c.Value), new Map());
    console.log('Settings', this.Settings.get('AllowEditItemAgainstInvoice'));
  };

  handleBackButtonClick = async () => {
    await Confirmation('Do you want to go back ?', () => {
      this.RemoveBackButtonListener();
      this.props.navigation.goBack();
      console.log('Going Back');
    });
    return false;
  };

  SetIsAgainst = type => {
    switch (this.LocalData.CallFrom) {
      case 'PO':
        this.isAgainst = type === 6 || type === 7;
        break;
      case 'GRN':
        this.isAgainst = type === 2;
        break;
      case 'GRT':
        this.isAgainst = type === 2;
        break;
      case 'PI':
        this.isAgainst = type === 2 || type === 3 || type === 4;
        break;
      case 'DN':
        this.isAgainst = type === 2 || type === 3;
        break;
      default:
        break;
    }
  };

  HandleTypeChange = Type => {
    // Checking whether user has selected Against or not
    this.SetIsAgainst(Type);
    this.setState({Type});

    // get Against data for selected Type
    if (this.isAgainst) {
      Request.get(
        `${this.IP}${this.APILink.API_AgainstList}CallFrom=${this.LocalData.CallFrom}&Vendor=${this.state.Vendor}&InvoiceNo=${this.LocalData.InvoiceID}&InvoiceType=${Type}&BasedOn=${this.state.BasedOn}&LocCode=${this.LocalData.Location}`,
        this.Token,
      )
        .then(res => {
          this.PrepareListDataToRender(
            res.data,
            'Invoice No',
            'ID',
            'AgainstList',
          );
          this.setState(prevState => ({
            APIData: {...prevState.APIData, AgainstList: res.data || []},
          }));
        })
        .catch(AlertError);
    } else {
      this.AgainstList = [];
      this.setState(prevState => ({
        Against: '',
        APIData: {...prevState.APIData, AgainstList: []},
      }));
    }
  };

  HandleVendorChange = Vendor => {
    // Fetch PaymentTerms for selected Vendor
    Request.get(
      `${this.IP}${this.APILink.API_PaymentTermList}CallFrom=${this.LocalData.CallFrom}&Vendor=${this.state.Vendor}&LocCode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        // console.log(res.data);
        this.PrepareListDataToRender(res.data, 'NAME', 'ID', 'PaymentTermList');
        this.setState(prevState => ({
          APIData: {...prevState.APIData, PaymentTermList: res.data},
        }));
      })
      .catch(err => AlertMessage(JSON.stringify(err)));

    this.setState({Vendor});
  };

  HandleDateChange = (name, date) => {
    this.setState({[name]: date});
  };

  handleMerchandiserChange = Merchandiser => {
    const obj = this.state.APIData.MerchandiserList.find(
      itm => itm.ID === Merchandiser,
    );
    const MerchandiserName = (obj && obj.NAME) || '';
    this.setState({Merchandiser, MerchandiserName});
  };

  HandleAgainstChange = Against => {
    this.setState({Against});

    // Fetching and filling selected Aginst data
    if (Against !== '') {
      Request.get(
        `${this.IP}${this.APILink.API_SelectedAgainst}CallFrom=${this.LocalData.CallFrom}&GetTypeID=${this.state.Type}&GetAgainstID=${Against}`,
        this.Token,
      )
        .then(res => {
          console.log(res);
          if (res.status === 200) {
            if (res.data.Table?.length && res.data.Table1?.length) {
              this.setState(
                {
                  Vendor: res.data.Table[0].VendorName,
                  CreatedItems: res.data.Table1,
                },
                () => {
                  this.UpdateAmountCalculation();
                  this.UpdateProductsSourceArray();
                },
              );
            } else {
              AlertMessage('No data found for the selected Against');
            }
          } else {
            AlertMessage('Error while fetching data from server');
          }
        })
        .catch(err => {
          console.log(err);
          AlertMessage(JSON.stringify(err));
        });
    } else {
      this.setState({CreatedItems: []});
    }
  };

  CompareForSort = (a, b) => {
    if (a.SeqNo > b.SeqNo) {
      return 1;
    }
    if (b.SeqNo > a.SeqNo) {
      return -1;
    }

    return 0;
  };

  UpdateCreatedItem = data => {
    // return if no data returned
    if (!data.length) {
      return;
    }

    // Adding newly CreatedItems to this.CreatedItemList
    let ENTIDList = data.join('^'); // concat all the returned ENTID
    // console.log(this.NextSeqNo - 1);
    Request.get(
      `${this.IP}${
        this.APILink.API_CreatedItemByID
      }CallFrom=PO&PassInvoiceMasterIdList=${ENTIDList}&SeqNo=${
        this.NextSeqNo - 1
      }`,
      this.Token,
    )
      .then(res => {
        // console.log("Merged Items", res.data);
        this.setState(
          prevState => ({
            CreatedItems: prevState.CreatedItems.concat(res.data),
          }),
          () => {
            this.UpdateNextSeqNo();
            console.log(
              'Created Items after state setted',
              this.state.CreatedItems,
            );
            this.UpdateAmountCalculation();
            this.UpdateProductsSourceArray();
          },
        ); //console.log(this.state.CreatedItems);
      })
      .catch(AlertError);
  };

  RefreshUpdatedItem = (
    data,
    ItemMasterID,
    SeqNo,
    ItemID,
    Icode,
    InvoiceInvDetailID,
  ) => {
    if (!data.length) {
      return;
    }

    if (ItemID && Icode) {
      let {state} = this;

      Request.get(
        `${this.IP}${this.APILink.API_EditedLotItem}CallFrom=PO&InvoiceID=${ItemMasterID}&SeqNo=${SeqNo}&InvoiceInvDetailID=${InvoiceInvDetailID}&ValidateIsAgainstInvoice=${this.isAgainst}&Srch_Against=${state.Against}&InvoiceType=${state.Type}&InvoiceNo=${this.LocalData.InvoiceID}`,
        this.Token,
      )
        .then(res => {
          if (res.status === 200) {
            if (res.data.length) {
              let ItemIndex = this.state.CreatedItems.findIndex(
                item =>
                  item.ItemMasterID === ItemMasterID &&
                  item.ItemID === ItemID &&
                  item.SeqNo === SeqNo,
              );

              let {CreatedItems} = this.state;
              CreatedItems[ItemIndex] = {
                ...CreatedItems[ItemIndex],
                ...res.data[0],
              };
              CreatedItems.sort(this.CompareForSort);
              // console.log("response from API", res.data[0]);
              // console.log("CreatedItems with Lot generated", CreatedItems);
              this.setState({CreatedItems}, () => {
                this.UpdateNextSeqNo();
                console.log(
                  'Updated Lot Items after state setted',
                  this.state.CreatedItems,
                );
                this.UpdateAmountCalculation();
                this.UpdateProductsSourceArray();
              });
            } else {
              AlertMessage('Something went wrong');
            }
          } else {
            AlertStatusError(res);
          }
        })
        .catch(AlertError);
    } else {
      Request.get(
        `${this.IP}${this.APILink.API_EditedInvoiceItem}CallFrom=PO&PassInvoiceMasterIdList=${ItemMasterID}`,
        this.Token,
      )
        .then(res => {
          if (res.status === 200) {
            if (res.data.length) {
              let ItemIndex = this.state.CreatedItems.findIndex(
                item =>
                  item.ItemMasterID === ItemMasterID &&
                  item.ItemID === 0 &&
                  item.SeqNo === SeqNo,
              );

              let {CreatedItems} = this.state;
              CreatedItems[ItemIndex] = {
                ...CreatedItems[ItemIndex],
                ...res.data[0],
              };
              CreatedItems.sort(this.CompareForSort);
              // console.log(CreatedItems);
              this.setState({CreatedItems}, () => {
                this.UpdateNextSeqNo();
                console.log(
                  'Updated Invoice Items after state setted',
                  this.state.CreatedItems,
                );
                this.UpdateAmountCalculation();
                this.UpdateProductsSourceArray();
              });
            } else {
              AlertMessage('Something went wrong');
            }
          } else {
            AlertStatusError(res);
          }
        })
        .catch(AlertError);
    }
  };

  UpdateProductsSourceArray = () => {
    let CreatedItems = [...this.state.CreatedItems];
    this.CreatedItemsSource = CreatedItems;
  };

  UpdateAmountCalculation = () => {
    let BasicAmount = 0,
      TotalAmount = 0;
    BasicAmount = TotalAmount = this.state.CreatedItems.reduce(
      (total, currentObj) =>
        total + (currentObj.Quantity || 0) * (currentObj.StdRate || 0),
      0,
    ).toFixed(2);
    this.setState({BasicAmount, TotalAmount});
  };

  ValidateAgainstRaisedForInvoice = async () => {
    let {InvoiceID, CallFrom} = this.LocalData;
    let res = await Request.get(
      `${this.IP}${this.APILink.API_ValidateAgainstRaised}CallFrom=${CallFrom}&passInvoiceID=${InvoiceID}`,
      this.Token,
    );
    if (res.status === 200 && res.data.length) {
      return res.data[0].RESULT === 'NOT-EXISTS';
    } else {
      AlertMessage('Error while validating the invoice ');
      return false;
    }
  };

  ValidateInputs = async () => {
    try {
      Keyboard.dismiss();
      // Validate All Mandatory fields
      let {state} = this;

      console.log(
        'Against Raised',
        await this.ValidateAgainstRaisedForInvoice(),
      );

      // Validate Lot No has been generated to all items except Purchase Order
      let ItemsWithoutLot = state.CreatedItems.filter(
        item => item.ItemID === 0,
      );
      if (this.LocalData.CallFrom !== 'PO' && ItemsWithoutLot.length) {
        AlertMessage('Generate Lot No for all items');
      } else if (!(await this.ValidateAgainstRaisedForInvoice())) {
        AlertMessage('Cannot modify Invoice');
      }
      // validate whether against data is selected for Against Invoice
      else if (this.isAgainst && state.Against === '') {
        if (!state.Against) {
          AlertMessage('Select Against record');
        }
      } else if (!state.Vendor) {
        AlertMessage('Select Vendor');
      } else if (!state.StockPoint) {
        AlertMessage('Select StockPoint');
      } else if (!state.CreatedItems.length) {
        AlertMessage('Add atleast one product');
      } else {
        let {state} = this;

        // Preparing Invoice data with ItemID = 0 and Items with ItemID != 0

        // Preparing Invoice Data
        let InvoiceDetail = state.CreatedItems.filter(item => item.ItemID == 0);

        // Preparing Lot data
        let LotDetails = state.CreatedItems.filter(item => item.ItemID != 0);

        // concatenating the grouped Lot Items in InvoiceDetail
        let GroupedLotItems = [
          ...LotDetails.reduce((prev, cur) => {
            // creating key
            let key = `${cur.InvoiceInvDetailID}-${cur.ItemMasterID}`;
            // grouping and updating quantity
            let obj = prev.get(key) || Object.assign({}, cur, {Quantity: 0});
            obj.Quantity += cur.Quantity;
            // returning the upated object
            return prev.set(key, obj);
          }, new Map()).values(),
        ]; // using Map type in reduce function - Map is a new feature in ES6 it is similar to Object in JS

        InvoiceDetail = InvoiceDetail.concat(GroupedLotItems);
        console.log(InvoiceDetail);

        let SelectedInvoiceType = state.APIData.InvoiceType.find(
          item => item.ID === state.Type,
        );

        let ObjSave = {
          // Invoice Master data
          InvoiceID: this.LocalData.InvoiceID || 0,
          InvoiceNo: this.LocalData.InvoiceNo || '',
          InvoiceType: SelectedInvoiceType.TYPENAME || 'Standard',
          BasedOn: state.BasedOn,
          InvoiceDate:
            this.LocalData.InvoiceDate || new Date().toLocaleDateString(),
          LOCCODE: this.LocalData.Location,
          VendorCode: state.Vendor,
          DCNO: state.DocNo,
          DCDATE: state.DocDate,
          ValidFrm: state.ValidFrom,
          ValidTill: state.ValidTo,
          Stockpoint: state.StockPoint,
          TransportName: state.Transporter,
          BuyerName: state.MerchandiserName, // merchandiser Name
          AgentName: state.Agent,
          Remarks: state.Remarks,
          BasicAmount: state.BasicAmount || 0, // sum of amount of state.CreatedItems
          NetAmount: state.TotalAmount || 0,
          Round_Off: state.Round_Off || 0,
          PaymentTerm: state.PaymentTerm,
          OrderMode: state.OrderMode,
          ReqLocationCode: state.ReqCompany,
          EmployeeID: state.Merchandiser || '', // Merchandiser ID
          Agentcommission: state.Agentcommission || 0, // get the commission of selected agent
          CurrencyID: state.Currency,
          AgainstID: state.Against || 0,
          LogisticID: state.Logistic || 0, // will be valid only for GRN
          CreatedByOrModifyBy: await AsyncStorage.getItem('UserCode'),
          // Custom fields
          CustomField1: '',
          CustomField2: '',
          CustomField3: '',
          CustomField4: '',
          CustomField5: '',
          CFCombo1: '',
          CFCombo2: '',
          CFRadio1: '',
          CFRadio2: '',
          CFDate1: '',
          CFDate2: '',
          // Array Data with properties similar with DB Type
          InvoiceDetails: InvoiceDetail, // passing Invoice detail Array
          LotDetails: LotDetails, // passing Lot detail Array
          ChargesDetails: [],
          ReceiveRatioDetails: [],
        };
        this.SaveInvoice(ObjSave, `API_Save${this.LocalData.CallFrom || 'PO'}`);
      }
    } catch (err) {
      console.error(err);
    }
  };

  NavigateBackToList = () => {
    this.props.navigation.goBack();
    this.props.route.params.onUpdateInvoice();
  };

  SaveInvoice = (data, type) => {
    this.setState({ShowModalLoader: true, ModalMessage: 'Saving Transaction'});
    Request.post(`${this.IP}${this.APILink[type]}`, data, this.Token)
      .then(res => res.json())
      .then(json => {
        console.log('JSON', json);
        if (json.length) {
          let InvoiceNumber = json[0].InvoiceNumber;
          this.LocalData.InvoiceID
            ? AlertMessage(
                `${InvoiceNumber} saved successfully`,
                this.NavigateBackToList,
              )
            : AlertMessage(`${InvoiceNumber} saved successfully`);
          this.clearEntry();
        } else {
          this.HideModalLoader();
          AlertMessage('Error while saving Transaction');
        }
      })
      .catch(AlertError);
  };

  HideModalLoader = () => {
    this.setState({ShowModalLoader: false});
  };

  clearEntry = () => {
    this.setState({
      ShowModalLoader: false, // show or hide Save Modal popup
      // Clearing Filed data
      Type: 1,
      BasedOn: '',
      Against: '',
      Vendor: '',
      DocNo: '',
      DocDate: '',
      PaymentTerm: '',
      StockPoint: '',
      Remarks: '',
      ReqCompany: '',
      ValidFrom: '',
      ValidTo: '',
      Transporter: '',
      Agent: '',
      Currency: '',
      Logistic: '',
      Merchandiser: '',
      OrderMode: '',
      // Clearing Created Items
      CreatedItems: [],
    });

    // resetting Next seq no to 1
    this.NextSeqNo = 1;

    // clearing LocalData
    this.LocalData = {
      InvoiceNo: '',
      InvoiceID: 0,
      StatusFlag: 0,
      Status: ''  ,
    };
    this.UpdateAmountCalculation();
  };

  UpdateNextSeqNo = () => {
    let CreatedItems = this.state.CreatedItems;
    let ind = CreatedItems.length - 1;
    this.NextSeqNo = ind >= 0 ? CreatedItems[ind].SeqNo + 1 : 1;
  };

  HandleAddProduct = () => {
    // Check whether Vendor has been selected
    if (this.state.Vendor === '') {
      AlertMessage('Select Vendor');
    } else {
      this.NavigateToItemMaster();
    }
  };

  HandleProductSearch = ProductSearchText => {
    this.setState({ProductSearchText});
    let txt = ProductSearchText.toLowerCase();

    let CreatedItems = this.CreatedItemsSource.filter(obj => {
      // get all texts
      let Barcode = (obj.Barcode || '').toString().toLowerCase(),
        Icode = (obj.Icode || '').toString().toLowerCase(),
        Description = (obj.Description || '').toString().toLowerCase(),
        Quantity = ((Math.round(obj.Quantity * 100) / 100).toFixed(2) || '')
          .toString()
          .toLowerCase(),
        Rate = ((Math.round(obj.Rate * 100) / 100).toFixed(2) || '')
          .toString()
          .toLowerCase(),
        WSP = ((Math.round(obj.WSP * 100) / 100).toFixed(2) || '')
          .toString()
          .toLowerCase(),
        MRP = ((Math.round(obj.MRP * 100) / 100).toFixed(2) || '')
          .toString()
          .toLowerCase();

      return (
        Barcode.includes(txt) ||
        Icode.includes(txt) ||
        Description.includes(txt) ||
        Quantity.includes(txt) ||
        Rate.includes(txt) ||
        WSP.includes(txt) ||
        MRP.includes(txt)
      );
    });

    this.setState({CreatedItems});
  };
  showCancel = () => {
    this.setState({show: true});
  };

  hideCancel = () => {
    this.setState({show: false});
  };

  renderTouchableHighlight() {
    if (this.state.show) {
      return (
        <TouchableOpacity
          style={styles.closeButtonParent}
          onPress={this.clearInput}>
          <Image
            style={styles.closeButton}
            source={require('../../../assets/images/Clear_1.png')}
          />
        </TouchableOpacity>
      );
    }
    return null;
  }

  clearInput = () => {
    this.HandleProductSearch('');
  };

  ReceiveAllQty = () => {
    // Hiding the Menu
    this.HideExtraActionsMenu();

    // Receiving all Ordered Qty
    let CreatedItems = [...this.state.CreatedItems];
    CreatedItems.map(item => {
      item.Quantity = item.OrderQty;
      item.PendingQty = 0.0;
      return item;
    });
    this.setState({CreatedItems}, this.UpdateAmountCalculation);
  };

  GenerateLotNo = () => {
    // Hiding the Menu
    this.HideExtraActionsMenu();

    // Generate Lot No for Items having ItemID = 0
    let ItemsToGenerateLotNo = this.state.CreatedItems.filter(
      item => item.ItemID === 0,
    );
    if (!ItemsToGenerateLotNo.length) {
      AlertMessage('No product to generate Lot No');
      return;
    }

    // Showing loader sreen
    this.setState({ShowModalLoader: true, ModalMessage: 'Generating Lot No'});

    let ItemMasterID = '',
      SeqNo = '',
      InvDetailID = '';
    ItemsToGenerateLotNo.map(item => {
      ItemMasterID += `${item.ItemMasterID}^`;
      SeqNo += `${item.SeqNo}^`;
      InvDetailID += `${item.InvoiceInvDetailID}^`;
    });

    Request.get(
      `${this.IP}${this.APILink.API_GenerateLotNo}ItemMasterID=${ItemMasterID}&SeqNo=${SeqNo}&InvDetailID=${InvDetailID}&Loccode=${this.LocalData.Location}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          if (res.data.length) {
            // Getting existing data with Lot No
            let ExistingLotData = this.state.CreatedItems.filter(
              item => item.ItemID > 0,
            );
            let RefreshedData = res.data.concat(ExistingLotData);
            this.setState({
              CreatedItems: RefreshedData.sort(this.CompareForSort),
            });
          } else {
            AlertMessage('Something went wrong.');
          }
        } else {
          AlertStatusError(res);
        }
        this.setState({ShowModalLoader: false});
      })
      .catch(err => {
        this.setState({ShowModalLoader: false});
        AlertError(err);
      });
  };

  HandleEditItem = (
    index,
    ItemMasterID,
    SeqNo,
    ItemID,
    Icode,
    InvoiceInvDetailID,
  ) => {
    this.hideMenu(index);
    this.NavigateToItemMaster(
      ItemMasterID,
      SeqNo,
      ItemID,
      Icode,
      InvoiceInvDetailID,
    );
  };

  HandleRemoveItem = index => {
    this.hideMenu(index);
    Confirmation('Do you want to remove ?', () => {
      let CreatedItems = [...this.state.CreatedItems];
      CreatedItems.splice(index, 1);
      this.setState({CreatedItems}, this.UpdateAmountCalculation);
    });
  };

  HandleProductQtyChange = (index, value) => {
    value = value || 0;
    let CreatedItems = [...this.state.CreatedItems];
    CreatedItems[index].Quantity = parseFloat(value);
    if (this.state.Against !== '') {
      let PendingQty = CreatedItems[index].OrderQty - parseFloat(value);
      CreatedItems[index].PendingQty = PendingQty < 0 ? 0 : PendingQty;
    }
    this.setState({CreatedItems}, this.UpdateAmountCalculation);
  };

  NavigateToItemMaster = (
    ItemMasterID,
    SeqNo,
    ItemID,
    Icode,
    InvoiceInvDetailID,
  ) => {
    let VendorCode = this.state.Vendor;
    if (VendorCode !== '') {
      let Vendor = this.state.APIData.VendorList.find(
        data => data.PARTYCODE === VendorCode,
      );
      let VendorName = Vendor ? Vendor.PARTYNAME : '';
      this.RemoveBackButtonListener();
      this.props.navigation.navigate('AddItemPurchseNav', {
        PartyCode: VendorCode,
        PartyName: VendorName,
        ItemMasterID,
        SeqNo,
        ItemID,
        Icode,
        InvoiceInvDetailID,
        onFinishCreation: this.UpdateCreatedItem,
        onUpdateItem: this.RefreshUpdatedItem,
      });
    } else {
      AlertMessage('Select Vendor');
    }
  };

  PrepareListDataToRender = (data, label, value, variableHolder) => {
    this[variableHolder] = data.map(item => ({
      label: item[label],
      value: item[value],
    }));
  };

  setMenuRef = (ref, index) => {
    this[`_menu${index}`] = ref;
  };

  setExtraActionMenuRef = ref => {
    this._ExtraActionMenu = ref;
  };

  showExtraActionsMenu = () => this._ExtraActionMenu.show();

  HideExtraActionsMenu = () => this._ExtraActionMenu.hide();

  hideMenu = index => this[`_menu${index}`].hide();

  showMenu = index => this[`_menu${index}`].show();

  // Declaring variables to hold values of the prepared list
  InvoiceTypeList = [];
  BasedOnList = [];
  ReqCompanyList = [];
  TranspoterList = [];
  AgentList = [];
  MerchandiserList = [];
  OrderModeList = [];
  AgainstList = [];
  PaymentTermList = [];
  CurrencyList = [];
  StockPointList = [];
  LogisticList = [];

  PrepareListForUnChangedData = () => {
    let {APIData} = this.state;
    // Preparing List items for Invoice Type
    if (APIData.InvoiceType.length) {
      // this.InvoiceTypeList = APIData.InvoiceType.map((item, index) => <Picker.Item key={index} label={item.TYPENAME} value={item.ID} />);
      this.InvoiceTypeList = APIData.InvoiceType.map(item => ({
        label: item.TYPENAME,
        value: item.ID,
      }));
    }
    // Preparing List items for ReqCompany
    if (APIData.ReqCompanyList.length) {
      // this.ReqCompanyList = APIData.ReqCompanyList.map((item, index) => <Picker.Item key={index} label={item.LocationName} value={item.LocationCode} />);
      this.ReqCompanyList = APIData.ReqCompanyList.map(item => ({
        label: item.LocationName,
        value: item.LocationCode,
      }));
    }
    // Preparing List items for Transporter
    if (APIData.TransporterList.length) {
      // this.TranspoterList = APIData.TransporterList.map((item, index) => <Picker.Item key={index} label={item.PARTYNAME} value={item.PARTYCODE} />);
      this.TranspoterList = APIData.TransporterList.map(item => ({
        label: item.PARTYNAME,
        value: item.PARTYCODE,
      }));
    }
    // Prepare List items for Agent
    if (APIData.AgentList.length) {
      // this.AgentList = APIData.AgentList.map((item, index) => <Picker.Item key={index} label={item.PARTYNAME} value={item.PARTYCODE} />);
      this.AgentList = APIData.AgentList.map(item => ({
        label: item.PARTYNAME,
        value: item.PARTYCODE,
      }));
    }
    // Preparing List items for Merchandiser
    if (APIData.MerchandiserList.length) {
      // this.MerchandiserList = APIData.MerchandiserList.map((item, index) => <Picker.Item key={index} label={item.NAME} value={item.ID} />);
      this.MerchandiserList = APIData.MerchandiserList.map(item => ({
        label: item.NAME,
        value: item.ID,
      }));
    }
    // Preparing List items for OrderMode
    if (APIData.OrderModeList.length) {
      // this.OrderModeList = APIData.OrderModeList.map((item, index) => <Picker.Item key={index} label={item.OrderMode} value={item.OrderMode} />);
      this.OrderModeList = APIData.OrderModeList.map(item => ({
        label: item.OrderMode,
        value: item.OrderMode,
      }));
    }
    // Preparing List items for Currency
    if (APIData.CurrencyList.length) {
      // this.CurrencyList = APIData.CurrencyList.map((item, index) => <Picker.Item key={index} label={item.CurrencyName} value={item.SetupId} />);
      this.CurrencyList = APIData.CurrencyList.map(item => ({
        label: item.CurrencyName,
        value: item.SetupId,
      }));
    }
    // Preparing List items for Stock Points
    if (APIData.StockPointList.length) {
      // this.StockPointList = APIData.StockPointList.map((item, index) => <Picker.Item key={index} label={item.STKPTNAME} value={item.STKPTCODE} />);
      let DEfaultStockCode = '';
      APIData.StockPointList.map(item => {
        if (DEfaultStockCode == '' && item.DEFAULT) {
          DEfaultStockCode = item.STKPTCODE;
        }
        this.StockPointList.push(
          Object.assign({}, {label: item.STKPTNAME, value: item.STKPTCODE}),
        );
      });
      this.setState({StockPoint: DEfaultStockCode || ''});
    }
    // Preparing List items for Logistic
    if (APIData.LogisticList.length) {
      // this.LogisticList = APIData.LogisticList.map((item, index) => <Picker.Item key={index} label={item["Track No"]} value={item.ID} />);
      this.LogisticList = APIData.LogisticList.map(item => ({
        label: item['Track No'],
        value: item.ID,
      }));
    }
  };
  ChangeStatus = changestatus => {
    this.setState({isFetched: false});
    Request.post(
      `${this.IP}${this.APILink.API_StatusChange}PassID=${this.LocalData.InvoiceID}^&PassType=${changestatus}&ID=0&LocCode=${this.LocalData.Location}&FinancialID=${this.LocalData.FinancialYearId}&User=${this.LocalData.UserCode}`,
      null,
      this.Token,
    )
      .then(res => res.json())
      .then(json => {
        if (json && json.Message == 'Saved') {
          AlertMessage(`Status updated successfully`, this.NavigateBackToList);
        } else {
          AlertMessage('Failed to update status');
        }
        this.setState({isFetched: true});
      })
      .catch(err => {
        this.setState({isFetched: true});
      });
  };

  HandleTabsClick = ActiveTab => {
    Keyboard.dismiss();
    this.setState({ActiveTab});
    this.refs.ViewPager.setPage(ActiveTab || 0);
  };

  HighlightSelectedTab = event => {
    Keyboard.dismiss();
    this.setState({ActiveTab: event.nativeEvent.position});
  };

  IsAgainst = () => {
    let IsAgainst = false;
    if (
      (this.LocalData.CallFrom == 'PO' &&
        (this.state.Type == 6 || this.state.Type == 7)) ||
      ((this.LocalData.CallFrom == 'GRN' || this.LocalData.CallFrom == 'GRT') &&
        this.state.Type == 2) ||
      ((this.LocalData.CallFrom == 'PI' || this.LocalData.CallFrom == 'DN') &&
        (this.state.Type == 2 || this.state.Type == 3 || this.state.Type == 4))
    ) {
      IsAgainst = true;
    }
    return IsAgainst;
  };

  // Fields holder
  FieldType = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <ModalSearchablePicker
        placeholder="Type"
        fieldWrapperStyle={{width: this.IsAgainst() ? '48%' : '98%'}}
        data={this.InvoiceTypeList}
        labelProp="label"
        valueProp="value"
        selectedValue={this.state.Type}
        onValueSelected={this.HandleTypeChange}
      />
      {this.IsAgainst() && (
        <ModalSearchablePicker
          placeholder="Against"
          data={this.AgainstList}
          labelProp="label"
          valueProp="value"
          fieldWrapperStyle={{width: '48%'}}
          selectedValue={this.state.Against}
          onValueSelected={this.HandleAgainstChange}
        />
      )}
    </View>
  );

  FieldVendor = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Vendor"
      data={this.state.APIData.VendorList}
      labelProp="PARTYNAME"
      valueProp="PARTYCODE"
      selectedValue={this.state.Vendor}
      onValueSelected={this.HandleVendorChange}
    />
  );

  FieldDocInfo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      <RichTextInput
        placeholder="Doc No"
        wrapperStyle={{width: '48%'}}
        value={this.state.DocNo}
        onChangeText={DocNo => this.setState({DocNo})}
        inputProps={{
          onSubmitEditing: () => {
            Keyboard.dismiss();
          },
        }}
      />
      
    <ModalDatePicker
      placeholder={'Doc Date'}
        fieldWrapperStyle={{width: '48%'}}
      selectedValue={this.state.DocDate}
      onValueSelected={(selectedDate) => this.HandleDateChange('DocDate',selectedDate ) }
    />
    </View>
  );

  FieldPaymentTerm = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Payment Term"
      data={this.PaymentTermList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.PaymentTerm}
      onValueSelected={PaymentTerm => this.setState({PaymentTerm})}
    />
  );

  FieldStockPoint = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Stock Point"
      data={this.StockPointList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.StockPoint}
      onValueSelected={StockPoint => this.setState({StockPoint})}
    />
  );

  FieldRemarks = key => (
    <RichTextInput
      key={key}
      // inputStyle={{height: null, maxHeight: 100, lineHeight: 15}}
      placeholder="Remarks"
      value={this.state.Remarks}
      onChangeText={Remarks => this.setState({Remarks})}
      inputProps={{multiline: true, numberOfLines: 2}}
    />
  );

  FieldReqCompany = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Req Company"
      data={this.ReqCompanyList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.ReqCompany}
      onValueSelected={ReqCompany => this.setState({ReqCompany})}
    />
  );

  FieldValidFromAndTo = key => (
    <View key={key} style={[layoutStyle.FieldLayout, {flexDirection: 'row'}]}>
      
    <ModalDatePicker
      placeholder={'Valid From'}
        fieldWrapperStyle={{width: '48%'}}
      selectedValue={this.state.ValidFrom}
      onValueSelected={(selectedDate) => this.HandleDateChange('ValidFrom',selectedDate ) }
    />
    <ModalDatePicker
      placeholder={'Valid To'}
        fieldWrapperStyle={{width: '48%'}}
      selectedValue={this.state.ValidTo}
      onValueSelected={(selectedDate) => this.HandleDateChange('ValidTo',selectedDate ) }
    />
    </View>
  );

  FieldTransporter = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Transporter"
      data={this.TranspoterList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Transporter}
      onValueSelected={Transporter => this.setState({Transporter})}
    />
  );

  FieldAgent = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Agent"
      data={this.AgentList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Agent}
      onValueSelected={Agent => this.setState({Agent})}
    />
  );

  FieldCurrency = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Currency"
      data={this.CurrencyList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Currency}
      onValueSelected={Currency => this.setState({Currency})}
    />
  );
  // {
  // 	this.LocalData.CallFrom === "GRN" || this.LocalData.CallFrom === "PI" &&
  FieldLogistic = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Logistic"
      data={this.LogisticList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Logistic}
      onValueSelected={Logistic => this.setState({Logistic})}
    />
  );
  // }
  FieldMerchandiser = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Merchandiser"
      data={this.MerchandiserList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.Merchandiser}
      onValueSelected={this.handleMerchandiserChange}
    />
  );

  FieldOrderMode = key => (
    <ModalSearchablePicker
      key={key}
      placeholder="Order Mode"
      data={this.OrderModeList}
      labelProp="label"
      valueProp="value"
      selectedValue={this.state.OrderMode}
      onValueSelected={OrderMode => this.setState({OrderMode})}
    />
  );

  refreshLayout = () => this.forceUpdate();

  ChangeStatusDataList = () => {
    return this.LocalData.StatusFlag == 1 // Open
      ? [
          {label: 'Approve', value: 'Approval'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : this.LocalData.StatusFlag == 2 // Approved
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Cancel', value: 'Cancel'},
        ]
      : this.LocalData.StatusFlag == 3 // Receiving
      ? [
          {label: 'Hold', value: 'Hold'},
          {label: 'Close', value: 'Close'},
        ]
      : this.LocalData.StatusFlag == 7 // Hold
      ? [{label: 'Release', value: 'Release'}]
      : [];
  };

  render() {
    let IsChangeStatus = this.ChangeStatusDataList().length > 0;
    // console.log(
    //   'my log status : ',
    //   this.LocalData.StatusFlag,
    //   ' load data : ',
    //   IsChangeStatus,
    // );
    return (
      <>
        <LayoutWrapper
          backgroundColor={globalColorObject.Color.oppPrimary}
          onLayoutChanged={this.refreshLayout}>
          <View style={styles.Tabs}>
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 0)}
              style={[
                styles.Tab,
                // {width: (IsChangeStatus ? '31%' : '50%'),},
              ]}>
              <Text
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxxl,
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 0
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Invoice
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.HandleTabsClick.bind(this, 1)}
              style={[
                styles.Tab,
                // {width: (IsChangeStatus ? '31%' : '50%'),}
              ]}>
              <Text
                style={[
                  styles.TabView,
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxxl,
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.GrayColor
                      : globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  ApplyStyleColor(
                    this.state.ActiveTab === 1
                      ? globalColorObject.Color.oppPrimary
                      : globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                Products
              </Text>
            </TouchableOpacity>
            {/* {IsChangeStatus && (
                    <ModalSearchableLabel
                    fieldWrapperStyle={
                      styles.Tab,
                      ApplyStyleColor(
                        globalColorObject.Color.BlackColor,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),{width: '37%',borderColor:'black',borderWidth:1,borderRadius:10}}
                      placeholder= {`${this.LocalData.Status && this.LocalData.Status != '' ?   this.LocalData.Status : 'Status'}`}
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.LocalData.StatusFlag}
                      inputStyle={[
                        {
                          borderBottomWidth: 0,
                          paddingTop:0,
                          marginBottom: 0,
                          borderRadius:10,
                          textAlign: 'center',
                        },
                        ApplyStyleFontAndSize(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxl,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )} */}
            {/* {IsChangeStatus  && (
                    <ModalSearchableLabel
                      placeholder="Status"
                      fieldWrapperStyle={
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),{width: '20%'}}
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.LocalData.StatusFlag}
                      inputStyle={[
                        {
                          // borderBottomWidth: 1,
                          textAlign: 'center',
                          alignItems: 'center',
                        },
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Regular,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )} */}
          </View>

          <ViewPager
            style={styles.ViewPager}
            initialPage={0}
            onPageSelected={this.HighlightSelectedTab}
            ref={'ViewPager'}>
            <View style={styles.InvoiceTabWrapper}>
              <ScrollView
                keyboardShouldPersistTaps="always"
                nestedScrollEnabled={true}
                style={styles.ViewPager}>
                <View style={[layoutStyle.ScrollContentWrapper]}>
                {IsChangeStatus && (
                    <ModalSearchableLabel
                    fieldWrapperStyle={
                      ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),{width: '100%',paddingLeft:'72%',height:50,paddingRight:5,marginVertical:0,}}
                      placeholder= {`${this.LocalData.Status && this.LocalData.Status != '' ?   this.LocalData.Status : 'Status'}`}
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.LocalData.StatusFlag}
                      inputStyle={[
                        {
                          marginTop: 0,
                          borderBottomWidth: 0,
                          paddingRight:5,
                          paddingTop:15,
                          marginBottom: 0,
                          borderTopLeftRadius:10,
                          borderBottomLeftRadius:10,
                          textAlign: 'center',
                        },
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )}
                  {/* {(this.LocalData.StatusFlag == 1 ||
                    this.LocalData.StatusFlag == 2 ||
                    this.LocalData.StatusFlag == 3 ||
                    this.LocalData.StatusFlag == 7) && (
                    <ModalSearchableLabel
                      placeholder="Status"
                      // fieldWrapperStyle={{width: '100%'}}
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.LocalData.StatusFlag}
                      inputStyle={[
                        {
                          borderBottomWidth: 1,
                        },
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Regular,
                          globalFontObject.Size.small.sl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BorderBottomColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )} */}
                  {/* <ModalSearchablePicker
                  placeholder="Status"
                  data={[{label: 'Open',value : '1'},{label: 'Approved',value : '2'},{label: 'Close',value : '3'}]}
                  labelProp="label"
                  valueProp="value"
                  selectedValue={this.state.CurrentStatus}
                  onValueSelected={CurrentStatus => this.setState({CurrentStatus})}
                /> */}
                  {/* {(this.LocalData.CallFrom === "GRN" || this.LocalData.CallFrom === "PI") && this.FieldLogistic()} */}
                  {this.visibleProductGroupFields.map((fd, ind) =>
                    this[`Field${fd.Field}`](ind),
                  )}
                  {/* {this.visibleProductGroupFields} */}
                </View>
              </ScrollView>

              <View
                style={[
                  styles.SummaryTab,
                  {backgroundColor: globalColorObject.Color.Lightprimary},
                ]}>
                {/* <View style={styles.SingleLine}>
                  <Text
                    style={[
                      styles.AmountLabel,
                      ApplyStyleFontAndSize(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sl,
                      ),
                    ]}>
                    Basic
                  </Text>
                  <Text
                    style={[
                      styles.AmountText,
                      ApplyStyleFontAndSize(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sl,
                      ),
                    ]}>
                    {this.state.BasicAmount}
                  </Text>
                </View> */}
                {/* Showing all the charges details and calculations  */}
                {this.SummaryDetails}
                  {/* {IsChangeStatus && (
                    <ModalSearchableLabel
                      placeholder= {this.LocalData.Status && this.LocalData.Status != '' ?   this.LocalData.Status : 'Status'}
                      fieldWrapperStyle={
                        (styles.SingleLine,
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        {width: '20%', marginVertical: 0})
                      }
                      data={this.ChangeStatusDataList()}
                      labelProp="label"
                      valueProp="value"
                      selectedValue={this.LocalData.StatusFlag}
                      inputStyle={[
                        {
                          borderBottomWidth: 0,
                          textAlign: 'center',
                          textDecorationLine: 'underline',
                        },
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Lightprimary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}
                      onValueSelected={this.ChangeStatus}
                    />
                  )} */}
                  <View
                    style={[
                      styles.SingleLine,
                      {width: '98%'},
                    ]}>
                    <Text
                      style={[
                        styles.AmountLabel,
                        ApplyStyleFontAndSize(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sl,
                        ),
                      ]}>
                      Total Amount
                    </Text>
                    <Text
                      style={[
                        styles.AmountText,
                        ApplyStyleFontAndSize(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sl,
                        ),
                      ]}>
                      {this.state.TotalAmount}
                    </Text>
                  </View>
              </View>
            </View>
            <View
              style={[
                styles.CreatedItemsTab,
                {backgroundColor: globalColorObject.Color.Lightprimary},
              ]}>
              <View style={styles.ButtonsContainer}>
                <View style={styles.SearhBarWrapper}>
                  <TextInput
                    style={[
                      styles.SearchInput,
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Regular,
                        globalFontObject.Size.sxl,
                        globalColorObject.Color.oppPrimary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),
                      ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BorderColor,
                      ),
                    ]}
                    placeholder="Search"
                    value={this.state.ProductSearchText}
                    onChangeText={this.HandleProductSearch}
                    autoCorrect={false}
                    returnKeyType="search"
                    autoCapitalize="none"
                    onFocus={this.showCancel}
                    onBlur={this.hideCancel}
                  />
                  {this.renderTouchableHighlight()}
                </View>
                <View style={styles.BtnExtraActionsWrapper}>
                  <Menu
                    ref={this.setExtraActionMenuRef}
                    button={
                      <TouchableOpacity
                        style={styles.BtnExtraActions}
                        onPress={this.showExtraActionsMenu}>
                        <Image
                          style={{width: '70%', height: '70%'}}
                          source={ImgDotsMenu}
                        />
                      </TouchableOpacity>
                    }>
                    <MenuItem onPress={this.GenerateLotNo}>Lot No</MenuItem>
                    {this.state.Against !== '' && (
                      <MenuItem onPress={this.ReceiveAllQty}>
                        Receive All
                      </MenuItem>
                    )}
                  </Menu>
                </View>
              </View>
              <FlatList
                style={[
                  ApplyStyleColor(
                    globalColorObject.Color.Lightprimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}
                data={this.state.CreatedItems}
                // data={ItemListData}
                renderItem={({item, index}) => (
                  <ListItem
                    data={item}
                    FocsuedIndex={this.state.FocusedProductIndex}
                    index={index}
                    headers={this.visibleCreatedItemsGroupFields}
                    setMenuRef={ref => this.setMenuRef(ref, index)}
                    isAgainst={this.state.Against !== ''}
                    allowEdit={this.Settings.get('AllowEditItemAgainstInvoice')}
                    HandleShowMenu={this.showMenu}
                    HandleEditProduct={this.HandleEditItem}
                    HandleRemoveProduct={this.HandleRemoveItem}
                    HandleQtyChange={this.HandleProductQtyChange}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
                ListHeaderComponent={
                  <ListItemHeader
                    isAgainst={this.state.Against !== ''}
                    headers={[...this.visibleCreatedItemsGroupCaptions]}
                  /> // [...] spreading this to create a new array everytime, just to make the header rerender
                }
                ListFooterComponent={() => (
                  <View style={{width: '100%', marginTop: 90}} />
                )}
                stickyHeaderIndices={[0]}
              />
              {!this.isAgainst &&
                this.LocalData.CallFrom !== 'GRT' &&
                this.LocalData.CallFrom !== 'DN' && (
                  <TouchableOpacity
                    style={[
                      styles.AddNewItem,
                      ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),
                    ]}
                    onPress={this.HandleAddProduct}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={require('../../../assets/images/add-light.png')}
                    />
                  </TouchableOpacity>
                )}
            </View>
          </ViewPager>
          {/* Custom Popup Loader starts here*/}
          {/* Show Modal Saving loader */}
          <Modal
            transparent={true}
            animationType="fade"
            visible={this.state.ShowModalLoader}>
            <View
              style={[
                styles.ModalLoaderBackDrop,
                ApplyStyleColor(
                  globalColorObject.Color.GrayColor,
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
              ]}>
              <View
                style={[
                  styles.ModalLoaderContainer,
                  ApplyStyleColor(
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.BackgroundColor,
                  ),
                ]}>
                <ActivityIndicator size="large" style={styles.ModalLoader} />
                <Text
                  style={[
                    styles.ModalLoaderText,
                    ApplyStyleFontAndSize(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.sxl,
                    ),
                  ]}>
                  {this.state.ModalMessage}
                </Text>
              </View>
            </View>
          </Modal>
          {/* Custom Popup Loader ends here*/}
        </LayoutWrapper>
        <ICubeAIndicator isShow={!this.state.isFetched} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  FieldInput: {
    width: '100%',
    flex: 2,
    height: 40,
    alignSelf: 'center',
    textAlign: 'left',
  },
  closeButtonParent: {
    position: 'absolute',
    right: 5,
    top: 5,
  },
  closeButton: {
    height: 25,
    width: 25,
  },
  ModalLoaderBackDrop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalLoaderContainer: {
    width: '70%',
    height: 100,
    borderRadius: 5,
    flexDirection: 'row',
  },
  ModalLoader: {
    flex: 1,
    alignSelf: 'center',
  },
  ModalLoaderText: {
    flex: 2,
    alignSelf: 'center',
  },
  InvoiceTabWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  ViewPager: {
    flex: 1,
  },
  Tabs: {
    flexDirection: 'row',
    // borderTopColor: 'rgb(221, 221, 221)',
  },
  Tab: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  TabView: {
    padding: 10,
    // borderBottomWidth: 2,
    width: '100%',
    textAlign: 'center',
  },
  CreatedItemsTab: {
    flex: 1,
    // backgroundColor: '#f9f9f9',
  },
  AddNewItem: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  // Created Items List Styling starts here
  ButtonsContainer: {
    width: '100%',
    paddingVertical: 10,
    flexDirection: 'row',
    // backgroundColor: '#ededed',
    alignItems: 'center',
  },
  SearhBarWrapper: {
    paddingLeft: 10,
    flex: 1,
  },
  SearchInput: {
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    borderWidth: 1,
  },
  BtnExtraActionsWrapper: {
    width: 40,
  },
  BtnExtraActions: {
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  StickeyHeaderCard: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 13,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 5,
    paddingVertical: 3,
    marginVertical: 3,
    marginHorizontal: 13,
  },
  CardBtnOptionWrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  CardBtnOption: {
    width: 35,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  CardBtnOptionText: {
    lineHeight: 25,
    transform: [{scaleX: 1.7}],
    top: -5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 3,
  },
  // Created Items List Styling ends here
  SummaryTab: {
    justifyContent: 'flex-end',
    paddingBottom: 5,
  },
  SingleLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  AmountLabel: {
    textAlign: 'left',
  },
  AmountText: {
    textAlign: 'left',
  },
});
