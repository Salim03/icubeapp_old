import React, {Component, useEffect, useState} from 'react';
import {BlurView} from '@react-native-community/blur';
import {
  Modal,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import RichTextInput from '../../Custom/RichTextInput';
import {
  GetSessionData,
  MobileDeviceName,
  isnull,
  Request,
  nullif,
  nullif_Number,
  AlertMessage,
  ConfirmationWithAllInput,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import {TextInput} from 'react-native-gesture-handler';

const TellerSummary = ({CloseScreen, reStartApp}) => {
  const [sessionData, setSessionData] = useState({
    UserName: '',
    DeviceName: '',
    ProfileId: '',
    IP: '',
    Token: '',
    Loccode: 0,
    LoginDateTime: '1900-01-01',
  });

  const [tellerData, setTellerData] = useState({
    ModalType: 'Main',
    MOPDetails: [],
    DenominationList: [],
    OpeningDetails: {Amount: 0, Id: 0},
    PayInDetails: {Amount: 0, Data: []},
    PayOutDetails: {Amount: 0, Data: []},
    AllData: [],
    EditData: [],
    DrawerOpenedDate: '1900-01-01',
    isOpened: false,
    TotalAmount: 0,
    TotalAmountDenomination: 0,
  });

  useEffect(() => {
    LoadInitialData();
  }, []);

  const BindData = (AllData, passDrawerOpenedDate) => {
    AllData = AllData || [];
    let {
      CashAmount,
      OpeningAmount,
      OpeningId,
      get_PayinData,
      get_PayinAmount,
      get_PayOutData,
      get_PayOutAmount,
      MOPDetails,
    } = CalculateTotalCash(AllData);
    if (MOPDetails.length > 0) {
      MOPDetails.push({
        RID: -1,
        Credit: get_PayinAmount,
        Description: 'Payin',
      });
      MOPDetails.push({
        RID: -1,
        Debit: get_PayOutAmount,
        Description: 'Payout',
      });
    }
    setTellerData(prevstate => ({
      ...prevstate,
      AllData,
      OpeningDetails: {Amount: OpeningAmount, Id: OpeningId},
      PayInDetails: {Amount: get_PayinAmount, Data: get_PayinData},
      PayOutDetails: {Amount: get_PayOutAmount, Data: get_PayOutData},
      MOPDetails,
      DrawerOpenedDate: passDrawerOpenedDate,
      ModalType: 'Main',
      isOpened: AllData.length > 0 ? true : false,
      TotalAmount:
        CashAmount + OpeningAmount + get_PayinAmount - get_PayOutAmount,
    }));
  };

  const CalculateTotalCash = AllData => {
    AllData = AllData || [];
    let CashAmount = 0;
    let OpeningAmount = 0;
    let OpeningId = 0;
    let get_PayinData = [];
    let get_PayinAmount = 0;
    let get_PayOutData = [];
    let get_PayOutAmount = 0;
    let MOPDetails = [];
    AllData.map(item => {
      if (
        parseInt(isnull(item.RID, '-1')) == -1 &&
        isnull(item.Description, '') == 'Cash'
      ) {
        CashAmount =
          parseFloat(isnull(item.Credit, '0')) -
          parseFloat(isnull(item.Debit, '0'));
        MOPDetails.push(item);
      } else if (
        parseInt(isnull(item.RID, '-1')) > 0 &&
        isnull(item.Description, '') == 'Opening Amount'
      ) {
        OpeningAmount = parseFloat(isnull(item.Credit, '0'));
        OpeningId = parseInt(isnull(item.RID, '0'));
        MOPDetails.push(item);
      } else if (
        parseFloat(isnull(item.Credit, '0')) > 0 &&
        parseInt(isnull(item.RID, '-1')) > 0 &&
        isnull(item.Description, '') != ''
      ) {
        get_PayinAmount =
          get_PayinAmount + parseFloat(isnull(item.Credit, '0'));
        get_PayinData.push(item);
      } else if (
        parseFloat(isnull(item.Debit, '0')) > 0 &&
        parseInt(isnull(item.RID, '-1')) > 0 &&
        isnull(item.Description, '') != ''
      ) {
        get_PayOutAmount =
          get_PayOutAmount + parseFloat(isnull(item.Debit, '0'));
        get_PayOutData.push(item);
      } else if (parseInt(isnull(item.RID, '-1')) == -1) {
        MOPDetails.push(item);
      }
    });
    return {
      CashAmount,
      OpeningAmount,
      OpeningId,
      get_PayinData,
      get_PayinAmount,
      get_PayOutData,
      get_PayOutAmount,
      MOPDetails,
    };
  };

  const callCloseWhileOpen = (
    locsessionData,
    AllData,
    passDrawerOpenedDate,
  ) => {
    BindData(AllData, passDrawerOpenedDate);
    closeDrawer(locsessionData);
  };

  const LoadInitialData = async () => {
    let {
      get_IP,
      get_Token,
      get_Location,
      get_ProfileId,
      get_UserName,
      get_LoginDateTime,
      get_UserCode,
    } = await GetSessionData();
    let locsessionData = {};
    locsessionData.IP = get_IP;
    locsessionData.Token = get_Token;
    locsessionData.ProfileId = get_ProfileId;
    locsessionData.UserCode = get_UserCode;
    locsessionData.Loccode = get_Location;
    locsessionData.LoginDateTime = get_LoginDateTime;
    let DeviceName = await MobileDeviceName();
    let UserName = get_UserName;
    // let DeviceName = 'icube-1005';
    // let UserName = 'su';
    locsessionData.DeviceName = DeviceName;
    locsessionData.UserName = UserName;
    setSessionData(locsessionData);
    console.log(
      'LoadInitialData LoginDateTime : ',
      locsessionData.LoginDateTime,
    );
    Request.get(
      `${locsessionData.IP}/api/POS/GetTellerSummaryList?LoginName=${UserName}&Terminal=${DeviceName}&LoginDateTime=${get_LoginDateTime}&Loccode=${get_Location}`,
      locsessionData.Token,
    )
      .then(res => {
        let is_Open = res.data.isOpened ? true : false;
        let is_PrevDay_Open = res.data.isOpenedAndCurrentDiff ? true : false;
        let get_DrawerOpenedDate = isnull(
          res.data.DrawerOpenedDate,
          '1900-01-01',
        );
        console.log(
          'is_PrevDay_Open : ',
          is_PrevDay_Open,
          ' is_Open : ',
          is_Open,
          ' get_DrawerOpenedDate : ',
          get_DrawerOpenedDate,
        );
        if (is_PrevDay_Open) {
          ConfirmationWithAllInput(
            'Do you want to continue with previous day?',
            'Yes',
            'No',
            BindData.bind(this, res.data.TellerData, get_DrawerOpenedDate),
            callCloseWhileOpen.bind(
              this,
              locsessionData,
              res.data.TellerData,
              get_DrawerOpenedDate,
            ),
          );
        } else if (is_Open) {
          BindData.call(this, res.data.TellerData, get_DrawerOpenedDate);
        } else {
          // AlertMessage('Drawer not opened.', CloseScreen);
          ConfirmationWithAllInput(
            'Drawer not opened, Do you want to open drawer?',
            'Yes',
            'No',
            OpenDrawer.bind(this, locsessionData),
            CloseScreen,
          );
        }
      })
      .catch(err => {
        console.error(err);
      });
  };

  const RefreshTellerData = async (passsessionData, DrawerOpenedDate) => {
    Request.get(
      `${passsessionData.IP}/api/POS/GetTellerSummaryList?LoginName=${passsessionData.UserName}&Terminal=${passsessionData.DeviceName}&LoginDateTime=${DrawerOpenedDate}&Loccode=${passsessionData.Loccode}`,
      passsessionData.Token,
    )
      .then(res => {
        let is_Open = res.data.isOpened ? true : false;
        let is_PrevDay_Open = res.data.isOpenedAndCurrentDiff ? true : false;
        let get_DrawerOpenedDate = isnull(
          res.data.DrawerOpenedDate,
          '1900-01-01',
        );
        if (is_PrevDay_Open || is_Open) {
          BindData.call(this, res.data.TellerData, get_DrawerOpenedDate);
        } else {
          AlertMessage('Drawer not opened');
        }
      })
      .catch(err => {
        console.error(err);
      });
  };

  const ChnageOpeningAmunt = (index, value) => {
    let get_data = tellerData.MOPDetails;
    let get_Value = isNaN(value) ? 0 : value;
    get_data[index].Credit = get_Value;
    setTellerData(prevstate => ({
      ...prevstate,
      EditData: get_data,
      OpeningDetails: {
        ...prevstate.OpeningDetails,
        Amount: parseFloat(isnull(get_Value, '0')),
      },
    }));
  };

  const ChangeAmount = (index, value) => {
    let get_data = tellerData.EditData;
    const type = tellerData.ModalType;
    get_data[index][type == 'Payin' ? 'Credit' : 'Debit'] = value;
    setTellerData(prevstate => ({
      ...prevstate,
      EditData: get_data,
    }));
  };

  const ChangeDenominationUnit = (index, value) => {
    let get_data = tellerData.DenominationList;
    value = parseFloat(isnull(value, 0));
    const type = tellerData.ModalType;
    get_data[index].Noofunits = value;
    get_data[index].Amount =
      parseFloat(isnull(get_data[index].Factor, 0)) * value;
    let get_TotalAmount = get_data.reduce(
      (acc, item) => acc + parseFloat(isnull(item.Amount, '0')),
      0,
    );
    setTellerData(prevstate => ({
      ...prevstate,
      EditData: get_data,
      TotalAmountDenomination: get_TotalAmount,
    }));
  };

  const ChangeDescription = (index, value) => {
    let get_data = tellerData.EditData;
    get_data[index].Description = value;
    setTellerData(prevstate => ({
      ...prevstate,
      EditData: get_data,
    }));
  };

  const SaveOpeningData = () => {
    let getOpenAmount = parseFloat(isnull(tellerData.OpeningDetails.Amount, 0));
    let getOpenAmountId = parseInt(isnull(tellerData.OpeningDetails.Id, 0));
    let result_Save = {
      LoginName: sessionData.UserName,
      Terminal: sessionData.DeviceName,
      LoginDateTime: tellerData.DrawerOpenedDate,
      Loccode: sessionData.Loccode,
      Details: [
        {
          ID: getOpenAmountId,
          Description: 'Opening Amount',
          Debit: getOpenAmount > 0 ? 0 : Math.abs(getOpenAmount),
          Credit: getOpenAmount > 0 ? getOpenAmount : 0,
          GLedger: 0,
          SLedger: 0,
          LoginDate: tellerData.DrawerOpenedDate,
          RID: getOpenAmountId,
          Ack: false,
        },
      ],
    };
    Request.post(
      `${sessionData.IP}/api/POS/SaveTellerSummary`,
      result_Save,
      sessionData.Token,
    )
      .then(res => {
        let Error_MEssage = isnull(res?.Message, 'Failed to save');
        if (res.status && res.status == '200') {
          RefreshTellerData(sessionData, tellerData.DrawerOpenedDate);
        } else {
          AlertMessage(Error_MEssage);
        }
      })
      .catch(err => console.log('err : ', err));
  };

  const OpenDrawer = sessionData => {
    let result_Save = {
      LoginName: sessionData.UserName,
      Terminal: sessionData.DeviceName,
      LoginDateTime: sessionData.LoginDateTime,
      Loccode: sessionData.Loccode,
      Details: [
        {
          ID: 0,
          Description: 'Opening Amount',
          Debit: 0,
          Credit: 0,
          GLedger: 0,
          SLedger: 0,
          LoginDate: sessionData.LoginDateTime,
          RID: 0,
          Ack: false,
        },
      ],
    };
    Request.post(
      `${sessionData.IP}/api/POS/SaveTellerSummary`,
      result_Save,
      sessionData.Token,
    )
      .then(res => {
        let Error_MEssage = isnull(res?.Message, 'Failed to save');
        if (res.status && res.status == '200') {
          AlertMessage('Drawer opened sucessfully.');
          RefreshTellerData(sessionData, sessionData.LoginDateTime);
        } else {
          AlertMessage(Error_MEssage);
          CloseScreen();
        }
      })
      .catch(err => {
        AlertMessage(err);
        CloseScreen();
      });
  };

  const SaveData = () => {
    let get_data = tellerData.EditData;
    if (get_data.length > 0) {
      let result_Save = {
        LoginName: sessionData.UserName,
        Terminal: sessionData.DeviceName,
        LoginDateTime: tellerData.DrawerOpenedDate,
        Loccode: sessionData.Loccode,
        Details: get_data,
      };
      Request.post(
        `${sessionData.IP}/api/POS/SaveTellerSummary`,
        result_Save,
        sessionData.Token,
      )
        .then(res => {
          if (res.status && res.status == '200') {
            RefreshTellerData(sessionData, tellerData.DrawerOpenedDate);
          } else {
            AlertMessage(isnull(res?.Message, 'Failed to save'));
          }
        })
        .catch(err => console.log('err : ', err));
    } else {
      AlertMessage('No data available to save.');
    }
  };

  const SaveTellerDenominationData = () => {
    let get_data = tellerData.DenominationList;
    if (get_data.length > 0) {
      let result_Save = {
        EmpId: sessionData.UserCode,
        LoginName: sessionData.UserName,
        Terminal: sessionData.DeviceName,
        LoginDateTime: tellerData.DrawerOpenedDate,
        Amount: tellerData.TotalAmountDenomination,
        Loccode: sessionData.Loccode,
        Details: get_data,
      };
      console.log('res save : ', JSON.stringify(result_Save));
      Request.post(
        `${sessionData.IP}/api/POS/SaveTellerSummaryDenomin`,
        result_Save,
        sessionData.Token,
      )
        .then(res => {
          if (res.status && res.status == '200') {
            // CloseScreen();
            reStartApp();
          } else {
            AlertMessage(isnull(res?.Message, 'Failed to save'));
          }
        })
        .catch(err => console.log('err : ', err));
    } else {
      AlertMessage('No data available to save.');
    }
  };

  // const SaveData = () => {
  //   let type = tellerData.ModalType;
  //   let get_data = tellerData.EditData;
  //   if (get_data.length > 0) {
  //     let get_TotalAmount = get_data.reduce(
  //       (acc, item) =>
  //         acc +
  //         parseFloat(isnull(item.Credit, '0')) +
  //         parseFloat(isnull(item.Debit, '0')),
  //       0,
  //     );
  //     let result_Save = {
  //       LoginName: sessionData.UserName,
  //       Terminal: sessionData.DeviceName,
  //       LoginDateTime: tellerData.DrawerOpenedDate,
  //       Loccode: sessionData.Loccode,
  //       Details: get_data,
  //     };
  //     // console.log(`${sessionData.IP}/api/POS/SaveTellerSummary`);
  //     // console.log(sessionData.Token);
  //     // console.log('save object ', JSON.stringify(result_Save));
  //     Request.post(
  //       `${sessionData.IP}/api/POS/SaveTellerSummary`,
  //       result_Save,
  //       sessionData.Token,
  //     )
  //       .then(res => {
  //         console.log('res : ', res);
  //         if (type == 'Payin') {
  //           setTellerData(prevstate => ({
  //             ...prevstate,
  //             PayInDetails: {
  //               ...prevstate.PayInDetails,
  //               Data: get_data,
  //               Amount: get_TotalAmount,
  //               EditData: [],
  //             },
  //             ModalType: 'Main',
  //           }));
  //         } else {
  //           setTellerData(prevstate => ({
  //             ...prevstate,
  //             PayOutDetails: {
  //               ...prevstate.PayOutDetails,
  //               Data: get_data,
  //               Amount: get_TotalAmount,
  //               EditData: [],
  //             },
  //             ModalType: 'Main',
  //           }));
  //         }
  //       })
  //       .catch(err => console.log('err : ', err));
  //   } else {
  //     AlertMessage('No data available to save.');
  //   }
  // };

  const closeDrawer = passsessionData => {
    Request.get(
      `${passsessionData.IP}/api/POS/GetTellerDenominationData?CurrencyId=101`,
      passsessionData.Token,
    )
      .then(res => {
        if (res.status && res.status == '200') {
          let get_DenominationList = res.data || [];
          if (get_DenominationList.length > 0) {
            setTellerData(prevstate => ({
              ...prevstate,
              DenominationList: get_DenominationList,
              ModalType: 'Close',
            }));
          } else {
            AlertMessage('No data available.');
            CloseScreen();
          }
        } else {
          AlertMessage('No data available.');
          CloseScreen();
        }
      })
      .catch(err => {
        AlertMessage('Failed to get denomination.');
        CloseScreen();
      });
  };

  const AddRow = () => {
    let isAllowAdd = false;
    let get_EditInfo = tellerData.EditData;
    if (get_EditInfo.length > 0) {
      isAllowAdd =
        isnull(get_EditInfo[get_EditInfo.length - 1].Description, '') != '';
    } else {
      isAllowAdd = true;
    }
    if (isAllowAdd) {
      get_EditInfo.push({
        RID: 0,
        Description: '',
        Debit: 0,
        Credit: 0,
      });
      setTellerData(prevstate => ({
        ...prevstate,
        EditData: get_EditInfo,
      }));
    } else {
      AlertMessage('Fill all details to add new.');
    }
  };

  return (
    <>
      <View style={styles.Header}>
        <TouchableWithoutFeedback
          style={styles.HeaderTitleWrapper}
          onPress={CloseScreen}>
          <Text
            style={[
              styles.HeaderTitle,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.mediam.ml,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            &#60; Payment
          </Text>
        </TouchableWithoutFeedback>
      </View>

      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: globalColorObject.Color.oppPrimary,
        }}>
        <FlatList
          data={tellerData.MOPDetails}
          renderItem={({item, index}) => {
            return (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  marginHorizontal: 10,
                }}>
                <Text
                  style={[
                    {width: '58%', alignSelf: 'flex-end'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.BlackColor,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  ₹
                  {`  ${
                    item.Description == 'Opening Amount'
                      ? 'Floating Amount'
                      : item.Description
                  }`}
                </Text>
                {isnull(item.Description, '') == 'Payin' ||
                isnull(item.Description, '') == 'Payout' ? (
                  <TouchableOpacity
                    style={{width: '40%'}}
                    onPress={() => {
                      let get_EditData =
                        item.Description == 'Payin'
                          ? tellerData.PayInDetails.Data
                          : tellerData.PayOutDetails.Data;
                      setTellerData(prevstate => ({
                        ...prevstate,
                        ModalType: isnull(item.Description, ''),
                        EditData: get_EditData,
                      }));
                    }}>
                    <RichTextInput
                      isShowAnimateText={false}
                      placeholder={
                        isnull(
                          nullif_Number(
                            item.Description == 'Payin'
                              ? tellerData.PayInDetails.Amount
                              : tellerData.PayOutDetails.Amount,
                            0,
                          ),
                          'N/A',
                        ) == 'N/A'
                          ? 'N/A'
                          : ''
                      }
                      value={
                        item.Description == 'Payin'
                          ? tellerData.PayInDetails.Amount
                          : tellerData.PayOutDetails.Amount
                      }
                      inputStyle={[
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                        {borderBottomWidth: 0},
                      ]}
                      wrapperStyle={[
                        {width: '100%', borderBottomWidth: 0, paddingRight: 10},
                      ]}
                      inputProps={{
                        returnKeyType: 'next',
                        textAlign: 'right',
                        autoCorrect: false,
                        autoCapitalize: 'none',
                        textDecorationLine: 'underline',
                        editable: false,
                      }}
                    />
                  </TouchableOpacity>
                ) : (
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={
                      isnull(
                        nullif_Number(
                          parseFloat(isnull(item.Credit, 0)) +
                            parseFloat(isnull(item.Debit, 0)),
                          0,
                        ),
                        'N/A',
                      ) == 'N/A'
                        ? 'N/A'
                        : ''
                    }
                    value={
                      parseFloat(isnull(item.Credit, 0)) +
                      parseFloat(isnull(item.Debit, 0))
                    }
                    onChangeText={Value => ChnageOpeningAmunt(index, Value)}
                    onBlur={SaveOpeningData}
                    inputStyle={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {borderBottomWidth: 0},
                    ]}
                    wrapperStyle={[
                      {width: '40%', borderBottomWidth: 0, paddingRight: 10},
                    ]}
                    inputProps={{
                      keyboardType: 'phone-pad',
                      returnKeyType: 'next',
                      textAlign: 'right',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                      textDecorationLine:
                        item.Description == 'Opening Amount'
                          ? 'underline'
                          : 'none',
                      editable: item.Description == 'Opening Amount',
                    }}
                  />
                )}
              </View>
            );
          }}
          keyExtractor={(item, index) => `k_m_${index}`}
          // to have some breathing space on bottom
          ListFooterComponent={<View style={{width: '100%', marginTop: 15}} />}
        />
      </SafeAreaView>

      <View
        style={{
          flexDirection: 'row',
          backgroundColor: globalColorObject.Color.oppPrimary,
        }}>
        <TouchableOpacity
          style={{
            //borderWidth: 1,
            width: '50%',
            backgroundColor: globalColorObject.Color.oppPrimary,
            height: 50,
            borderWidth: 1,
            borderColor: globalColorObject.Color.Primary,
            justifyContent: 'center',
          }}
          onPress={() => {}}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.mediam.ml,
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            ₹ {tellerData.TotalAmount}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (tellerData.isOpened) {
              ConfirmationWithAllInput(
                'Are you sure want to close the drawer?',
                'Yes',
                'No',
                closeDrawer.bind(this, sessionData),
                () => {},
              );
            } else {
              AlertMessage('Drawer not opened.');
            }
          }}
          style={{
            width: '50%',
            backgroundColor: globalColorObject.Color.Primary,
            justifyContent: 'center',
            height: 50,
          }}>
          <Text
            style={[
              {
                textAlign: 'center',
              },
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.mediam.ml,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Close Drawer
          </Text>
        </TouchableOpacity>
      </View>
      {/* Edit Paymnt */}

      <Modal
        animationType="slide"
        transparent={true}
        visible={
          tellerData.ModalType == 'Payin' || tellerData.ModalType == 'Payout'
            ? true
            : false
        }
        style={{marginTop: 50, paddingTop: 100}}
        onRequestClose={() => {}}>
        <BlurView style={styles.blurredBackdrop} blurType="dark" />
        <View
          style={{
            flex: 1,
            marginTop: '25%',
            marginHorizontal: 5,
            paddingHorizontal: 20,
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            paddingTop: 10,
            backgroundColor: globalColorObject.Color.oppPrimary,
          }}>
            
          <View style={{flexDirection: 'row', paddingHorizontal:5,}}>
            <View
              style={{
                //borderWidth: 1,
                width: '50%',
                backgroundColor: globalColorObject.Color.oppPrimary,
                height: 40,
                borderColor: globalColorObject.Color.Primary,
                justifyContent: 'center',
              }}
              onPress={SaveData}>
              <Text
                style={[
                  {
                    textAlign: 'left',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                {tellerData.ModalType}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                //borderWidth: 1,
                width: '50%',
                backgroundColor: globalColorObject.Color.oppPrimary,
                height: 40,
                justifyContent: 'center',
              }}
              onPress={AddRow}>
              <Text
                style={[
                  {
                    textAlign: 'right',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Regular,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                &#x271A;
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            removeClippedSubviews={false}
            data={tellerData.EditData}
            renderItem={({item, index}) => {
              return (
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={'Whom'}
                    value={item.Description}
                    onChangeText={Value => ChangeDescription(index, Value)}
                    wrapperStyle={[{width: '50%', borderBottomWidth: 1}]}
                    inputProps={{
                      returnKeyType: 'next',
                      textAlign: 'left',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={'How much'}
                    value={isnull(
                      tellerData.ModalType == 'Payin'
                        ? item.Credit
                        : item.Debit,
                      0,
                    )}
                    onChangeText={Value => ChangeAmount(index, Value)}
                    wrapperStyle={[{width: '50%', borderBottomWidth: 1}]}
                    inputProps={{
                      keyboardType: 'phone-pad',
                      returnKeyType: 'next',
                      textAlign: 'center',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                    }}
                  />
                </View>
              );
            }}
            keyExtractor={(item, index) => `k_sed_${index}`}
            // to have some breathing space on bottom
            ListFooterComponent={
              <View style={{width: '100%', marginTop: 15}} />
            }
          />

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                //borderWidth: 1,
                width: '50%',
                backgroundColor: globalColorObject.Color.oppPrimary,
                height: 40,
                borderWidth: 1,
                borderColor: globalColorObject.Color.Primary,
                justifyContent: 'center',
              }}
              onPress={SaveData}>
              <Text
                style={[
                  {
                    textAlign: 'center',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                Save
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: '50%',
                backgroundColor: globalColorObject.Color.Primary,
                justifyContent: 'center',
                height: 40,
              }}
              onPress={() => {
                setTellerData(prevstate => ({...prevstate, ModalType: 'Main'}));
              }}>
              <Text
                style={[
                  {
                    textAlign: 'center',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                Cancel
              </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
              style={{
                //borderWidth: 1,
                width: '20%',
                backgroundColor: globalColorObject.Color.oppPrimary,
                height: 40,
                borderWidth: 1,
                borderColor: globalColorObject.Color.Primary,
                justifyContent: 'center',
              }}
              onPress={AddRow}>
              <Text
                style={[
                  {
                    textAlign: 'center',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                +
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </Modal>

      {/* Teller Denomination */}

      <Modal
        animationType="slide"
        transparent={true}
        visible={tellerData.ModalType == 'Close' ? true : false}
        style={{marginTop: 50, paddingTop: 100}}
        onRequestClose={() => {}}>
        <BlurView style={styles.blurredBackdrop} blurType="dark" />
        <View
          style={{
            flex: 1,
            marginTop: '15%',
            marginHorizontal: 5,
            paddingHorizontal: 20,
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            paddingTop: 10,
            backgroundColor: globalColorObject.Color.oppPrimary,
          }}>
          <FlatList
            removeClippedSubviews={false}
            data={tellerData.DenominationList}
            renderItem={({item, index}) => {
              return (
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={''}
                    value={`${item.Factor} x `}
                    wrapperStyle={[{width: '30%', borderBottomWidth: 0}]}
                    inputStyle={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.BlackColor,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {borderBottomWidth: 0},
                    ]}
                    inputProps={{
                      returnKeyType: 'next',
                      textAlign: 'left',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                      placeholderTextColor: globalColorObject.Color.BlackColor,
                      editable: false,
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={''}
                    value={isnull(item.Noofunits, 0)}
                    onChangeText={Value => ChangeDenominationUnit(index, Value)}
                    wrapperStyle={[{width: '30%', borderBottomWidth: 1}]}
                    inputProps={{
                      keyboardType: 'phone-pad',
                      returnKeyType: 'next',
                      textAlign: 'center',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                      placeholderTextColor: globalColorObject.Color.BlackColor,
                    }}
                  />
                  <RichTextInput
                    isShowAnimateText={false}
                    placeholder={''}
                    value={` = ${item.Amount}`}
                    wrapperStyle={[{width: '30%', borderBottomWidth: 0}]}
                    inputStyle={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {borderBottomWidth: 0},
                    ]}
                    inputProps={{
                      returnKeyType: 'next',
                      textAlign: 'right',
                      autoCorrect: false,
                      autoCapitalize: 'none',
                      placeholderTextColor: globalColorObject.Color.BlackColor,
                      editable: false,
                    }}
                  />
                </View>
              );
            }}
            keyExtractor={(item, index) => `k_den_l_${index}`}
            // to have some breathing space on bottom
            ListFooterComponent={
              <View style={{width: '100%', marginTop: 15}} />
            }
          />

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                width: '30%',
                backgroundColor: globalColorObject.Color.oppPrimary,
                justifyContent: 'center',
                height: 40,
              }}
              onPress={CloseScreen}>
              <Text
                style={[
                  {
                    textAlign: 'center',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                //borderWidth: 1,
                width: '70%',
                backgroundColor: globalColorObject.Color.Primary,
                height: 40,
                borderWidth: 1,
                borderColor: globalColorObject.Color.oppPrimary,
                justifyContent: 'center',
              }}
              onPress={() => {
                if(tellerData.TotalAmount == tellerData.TotalAmountDenomination){
                  SaveTellerDenominationData();
                } else if(tellerData.TotalAmount < tellerData.TotalAmountDenomination){
                  ConfirmationWithAllInput('Denomination amount is greater than total cash value. Any way do you want to continue?',
                  'Yes',
                  'No',
                  SaveTellerDenominationData,
                  () => {});
                } else {
                  AlertMessage('Enter Denomination amount is less than the total cash amount.')
                }
              }}>
              <Text
                style={[
                  {
                    textAlign: 'right',
                    paddingRight: 10,
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                ₹ {` ${tellerData.TotalAmountDenomination} / ${tellerData.TotalAmount}`}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  Header: {
    height: 55,
    alignItems: 'center',
    backgroundColor: globalColorObject.Color.Primary,
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  HeaderTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  HeaderTitle: {
    marginLeft: 20,
  },
  blurredBackdrop: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  item: {
    paddingLeft: 25,
    paddingVertical: 15,
  },
});

export default TellerSummary;
