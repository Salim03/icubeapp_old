import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  getFormsForModule,
  AlertError,
  getPropsFromArray,
} from '../../../Helpers/HelperMethods';

export default class RetailHome extends Component {
  state = {
    forms: [],
  };

  componentDidMount() {
    getFormsForModule(2)
      .then(data => {
        const forms =
          data &&
          getPropsFromArray(data, ['MENUNAME'])['MENUNAME'].map(elem =>
            elem.replace(/ /g, ''),
          );
        this.setState({forms});
      })
      .catch(AlertError);
  }

  NavigateToForm = form => {
    if (form === 'Footfall') {
      this.props.navigation.navigate('PeopleCountNav');
    } else if (form === 'Shop') {
      this.props.navigation.navigate('ShopNav');
    } else if (form === 'CusLoy') {
      this.props.navigation.navigate('CustomerNav');
    }
  };

  FormPeopleCount = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Footfall')}>
        <Image
          source={require('../../../assets/images/Home/Retail/Footfall.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Foot Fall</Text>
      </TouchableOpacity>
    );
  };
  FormCashMemo = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'Shop')}>
        <Image
          source={require('../../../assets/images/add_shopping_cart.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Pos</Text>
      </TouchableOpacity>
    );
  };
  FormCus = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'CusLoy')}>
        <Image
          source={require('../../../assets/images/add_shopping_cart.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Customer loyalty</Text>
      </TouchableOpacity>
    );
  };
  FormReturn = key => {
    return (
      <TouchableOpacity
        key={key}
        style={styles.FormContainer}
        onPress={this.NavigateToForm.bind(this, 'CusLoy')}>
        <Image
          source={require('../../../assets/images/error.png')}
          style={styles.FormImages}
        />
        <Text style={styles.FormLabel}>Return </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.LoadContainer}>
            <ScrollView style={styles.DataContainer}>
              <View style={styles.ListContainer}>
                {this.state.forms?.map(form => this[`Form${form}`]?.(form))}
                {/* {this.FormCus()}
                {this.FormReturn()} */}
              </View>
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0)',
              position: 'absolute',
              bottom: 0,
              right: 0,
            }}>
            {/* <Text style={{ textAlign: 'right', marginRight: 10 }}>iCube Bussiness Solution Pvt.Ltd</Text> */}
            <View
              style={{
                alignItems: 'flex-end',
                marginRight: 20,
                flexDirection: 'row',
                marginBottom: 5,
              }}>
              <Text style={{textAlign: 'right'}}>Powered By</Text>
              <Image
                source={require('../../../assets/images/Drawer/Logo.png')}
                style={{height: 30, width: 100}}
              />
            </View>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Text: {
    marginTop: '80%',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
  LoadContainer: {
    // backgroundColor: 'rgb(43, 54, 255)',
    backgroundColor: '#81D4FA',
    flex: 1,
  },
  FormImages: {
    width: 30,
    height: 30,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    marginTop: 5,
  },
  FormLabel: {
    textAlign: 'center',
    fontSize: 16,
    // fontWeight: 'bold',
  },
  DataContainer: {
    backgroundColor: 'white',
    marginTop: 30,
    // marginLeft: 5,
    // marginRight: 5,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    flex: 1,
    width: '100%',
    elevation: 50,
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  FormContainer: {
    // backgroundColor: 'rgb(252, 172, 182)',
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 5,
    height: 90,
    width: 100,
    alignItems: 'center',
    borderRadius: 10,
    elevation: 15,
  },
});
// export {Retail};
