import React, {Component} from 'react';
import {
  StyleSheet,
  Alert,
  Platform,
  PermissionsAndroid,
  Image,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
  TextInput,
  Modal,
  Animated,
  Button,
  Keyboard,
  Vibration,
} from 'react-native';
import CheckInput from '../../../components/Custom/CheckInput';
import ModalSearchablePicker from '../../Custom/ModalSearchablePicker';
import ItemLevelDiscount from './ItemDiscount';
import RichTextInput from '../../Custom/RichTextInput';
import {FloatingAction} from 'react-native-floating-action';
import {CameraScreen} from 'react-native-camera-kit';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';
import TellerSummary from './TellerSummary';
import {
  Request,
  AlertMessage,
  AlertError,
  AlertStatusError,
  DeviceMacAddress,
  MobileDeviceName,
  isnull,
  isnull_bool,
  GetSessionData,
  sharefileForCallFRom,
  ConfirmationWithAllInput,
  nullif,
  isnull_number,
  CallBluetoothPrint,
  isBluetoothPrinterConnected,
} from '../../../Helpers/HelperMethods';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import moment from 'moment';
import {
  ApplyStyleColor,
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../../Style/GlobalStyle';
import {SafeAreaView, withSafeAreaInsets} from 'react-native-safe-area-context';

const Separator = () => <View />;
const ListItemMain = props => {
  const data = props.data;
  const get_NegatveQtyAlertType = props.NegatveQtyAlertType;
  // console.log('mess type : ', get_NegatveQtyAlertType);
  let isValidstock = true;
  if (get_NegatveQtyAlertType == 'Restrict') {
    isValidstock =
      parseFloat(isnull(data.stock, 0)) > parseFloat(isnull(data.qty, 0))
        ? true
        : false;
  }
  let isAllowEdit =
    parseFloat(isnull(data.iqty, 0)) >= 0 ? isValidstock : false;
  // console.log('stock ', data.stock, ' curr qty : ', data.qty);
  let index = props.index;
  return (
    <Swipeable
      renderRightActions={(progress, dragX) => {
        const scale = dragX.interpolate({
          inputRange: [-40, 0],
          outputRange: [0.7, 0],
        });
        const pressHandler = () => {
          props.onLongPress(index);
        };
        return (
          <>
            <TouchableOpacity onPress={pressHandler}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'red',
                  justifyContent: 'center',
                }}>
                <Animated.Text
                  style={{
                    color: 'white',
                    paddingHorizontal: 10,
                    fontWeight: '600',
                    transform: [{scale}],
                  }}>
                  Delete
                </Animated.Text>
              </View>
            </TouchableOpacity>
          </>
        );
      }}>
      <View
        style={{
          width: '100%',
          paddingRight: 5,
          marginTop: 5,
          backgroundColor: 'white',
        }}>
        <View style={{flexDirection: 'row', width: '100%'}}>
          {/* {props.HandalSerialNo(index)} */}
          <Text
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {width: '20%', paddingLeft: 2},
            ]}>
            {data.icode}
          </Text>
          <Text style={{color: 'black', fontSize: 16}}>{data.itemname}</Text>
        </View>
        <View
          style={[
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
              globalColorObject.Color.BlackColor,
              globalColorObject.ColorPropetyType.Color,
            ),
            {flexDirection: 'row', width: '100%', paddingRight: 5},
          ]}>
          <View style={{width: '25%'}}>
            <ModalSearchablePicker
              placeholder="SM Code"
              data={props.SMList}
              labelProp="SALESMANCODE"
              valueProp="SALESMANCODE"
              selectedValue={data.SP_No}
              onValueSelected={SMcode => {
                props.TagSMcode(SMcode, index);
              }}
            />
          </View>
          <View
            pointerEvents={
              parseFloat(isnull(data.iqty, 0)) >= 0 ? 'auto' : 'none'
            }
            style={{flexDirection: 'row', width: '30%'}}>
            <TouchableOpacity
              onPress={() => props.quantityHandler('less', index)}>
              <Image
                style={[styles.ConfigLogo, {marginTop: 20}]}
                source={require('../../../assets/images/minus-salmon.png')}
              />
            </TouchableOpacity>
            <View style={{marginLeft: 5}}>
              <Text
                style={
                  ([layoutStyle.FieldLabelFont],
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxl,
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {textAlign: 'center'})
                }>
                Qty
              </Text>
              <TextInput
                style={styles.MiniInputField}
                value={
                  data.unitofmeasure === 'PCS'
                    ? data.qty.toString()
                    : parseFloat(data.qty)
                }
                onChangeText={qty => {
                  props.quantityHandler(qty, index);
                }}
                returnKeyType="next"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="numeric"
                editable={isAllowEdit}
              />
            </View>
            {isAllowEdit && (
              <TouchableOpacity
                onPress={() => props.quantityHandler('more', index)}>
                <Image
                  style={[styles.ConfigLogo, {marginTop: 20}]}
                  source={require('../../../assets/images/icons8_add_48px.png')}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={{marginRight: 5}}>
            <Text
              style={
                ([layoutStyle.FieldLabelFont],
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {textAlign: 'center'})
              }>
              Rate
            </Text>
            <TextInput
              style={styles.MiniInputField}
              value={data.baseprice.toString()}
              returnKeyType="next"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="numeric"
            />
          </View>
          <View style={{marginRight: 5, marginTop: 3}}>
            <ItemLevelDiscount
              Memo_Dis_data={props.DiscList}
              itemDetail={props.data}
              onValueSelected={item => {
                props.HandalItemDiscount(item, index);
              }}
            />
            <TextInput
              style={styles.MiniInputField}
              value={
                data.unitofmeasure === 'PCS'
                  ? data.discount.toString()
                  : parseFloat(data.discount)
              }
              // onChangeText={D_Amount => {
              //   props.HandleDiscountChange(D_Amount, index);
              // }}
              returnKeyType="next"
              autoCorrect={false}
              autoCapitalize="none"
              // keyboardType="numeric"
              keyboardType="phone-pad"
              // editable={data.disc_AlowChng === '' ? true : data.disc_AlowChng}
              editable={false}
            />
          </View>
          <View style={{flex: 1}}>
            <Text
              style={
                ([layoutStyle.FieldLabelFont],
                {textAlign: 'right', marginTop: 3})
              }>
              Amount
            </Text>
            <Text
              style={
                ([styles.MiniInputField],
                {textAlign: 'right', fontWeight: 'bold', paddingTop: 2})
              }>
              ₹ {data.salesprice}
            </Text>
          </View>
        </View>
        {data.discount > 0 && (
          <View>
            <Text style={{color: 'blue', fontWeight: 'bold', fontSize: 16}}>
              Saved ₹{data.discount} with Discount of {data.disc_id_Factor}
              {data.disc_Type === 'Percentage' ? '%' : '₹'}
            </Text>
          </View>
        )}
        <View></View>
      </View>
    </Swipeable>
  );
};
const ListItemHeader = () => {
  return (
    <View
      style={[
        styles.StickeyHeaderCard,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={{flexDirection: 'row'}}>
        <View style={[styles.CardTextContainer, {width: '60%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.CardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Customer Name
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '40%'}]}>
          <Text
            numberOfLines={1}
            style={[
              styles.CardTextFieldHeader,
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            Member No
          </Text>
        </View>
      </View>
      <View style={[styles.CardTextContainer, {width: '100%'}]}>
        <Text
          numberOfLines={1}
          style={[
            styles.CardTextFieldHeader,
            ApplyStyleFontAndSizeAndColor(
              globalFontObject.Font.Bold,
              globalFontObject.Size.small.sxl,
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.Color,
            ),
          ]}>
          Mobile No
        </Text>
      </View>
    </View>
  );
};
const ListItem = props => {
  const data = props.data;
  return (
    <TouchableWithoutFeedback onLongPress={() => props.onLongPress(data)}>
      <View style={styles.Card}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={[styles.CardTextContainer, {width: '60%', color: 'red'}]}>
            <Text numberOfLines={1} style={[styles.CardTextFieldHeader1]}>
              {' '}
              {data.Displayname}
            </Text>
          </View>
          <View style={[styles.CardTextContainer, {width: '40%'}]}>
            <Text numberOfLines={1} style={[styles.CardTextFieldHeader1]}>
              {data.Memberno}
            </Text>
          </View>
        </View>
        <View style={[styles.CardTextContainer, {width: '100%'}]}>
          <Text numberOfLines={1} style={[styles.CardTextFieldHeader1]}>
            {data.Mobileno}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const BillListItem = props => {
  const data = props.data;
  let isShowDelete = isnull(props.isShowDelete, false);
  let isShowReprint =
    isnull(props.AllowedRePrintCount, 0) > isnull(data.PRINTCOUNT, 0);
  let FullInvoiceDate = new Date(data.MemoDate);
  let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
    '0' +
    (FullInvoiceDate.getMonth() + 1)
  ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
  let CustomerName = isnull(data['Customer Name'], '');
  return (
    <View
      style={[
        styles.BillCard,
        ApplyStyleColor(
          globalColorObject.Color.oppPrimary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine]}>
        {CustomerName != '' && (
          <View style={[styles.CardTextContainer, {width: '100%'}]}>
            <Text
              numberOfLines={1}
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sl,
                  globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              {CustomerName}
            </Text>
          </View>
        )}
        <View style={[styles.CardTextContainer, {width: '70%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {/* {`[${isnull(data.PRINTCOUNT, 0)}] ${data.MemoNo}`}  */}
            {`${data.MemoNo} ${InvoiceDate} ${data.Time}`}
          </Text>
        </View>
        <View style={[styles.CardTextContainer, {width: '30%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right'},
            ]}>
            {(Math.round(data.NetValue * 100) / 100).toFixed(2)}
          </Text>
        </View>
      </View>

      <View
        style={[
          {
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            backgroundColor: globalColorObject.Color.Lightprimary,
          },
        ]}>
        <View
          style={[
            {
              width: 0,
              height: 0,
              backgroundColor: globalColorObject.Color.Lightprimary,
              borderRightWidth: 20,
              borderTopWidth: 20,
              borderRightColor: globalColorObject.Color.Lightprimary,
              borderTopColor: globalColorObject.Color.oppPrimary,
            },
            {
              transform: [{rotate: '90deg'}],
            },
          ]}></View>
        {isShowReprint && (
          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                'CM',
                data.ID,
                data.MemoNo,
                moment(new Date(data.MemoDate)).format('YYYY-MM-DD'),
                true,
                props.index,
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/128/3022/3022251.png',
              }}
            />
          </TouchableOpacity>
        )}

        {isShowDelete && (
          <TouchableOpacity
            onPress={() =>
              props.onDeleteInvoice(
                data.MemoNo,
                moment(new Date(data.MemoDate)).format('YYYY-MM-DD'),
                props.index,
              )
            }
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/3096/3096673.png',
              }}
            />
          </TouchableOpacity>
        )}
        {isShowReprint && (
          <TouchableOpacity
            style={{
              paddingVertical: 2,
              paddingHorizontal: 10,
              height: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
              alignItems: 'center',
            }}
            onPress={() =>
              props.onPdfClick(
                'CM',
                data.ID,
                data.MemoNo,
                moment(new Date(InvoiceDate)).format('YYYY-MM-DD'),
                false,
                props.index,
              )
            }>
            <Image
              style={styles.ActionImage}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1059/1059106.png',
              }}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default class Shop extends Component {
  DefaultNewCustomerField = {
    CardTypeCode: '',
    CardNo: '',
    MemberNo: '',
    MobileNo: '',
    Name: 'No Name',
    AreaCode: 0,
    AreaName: '',
    GSTNo: '',
  };
  constructor(props) {
    super(props);
    this.SessionData = {
      UserName: '',
      DeviceName: '',
      ProfileId: '',
      IP: '',
      Token: '',
      Loccode: 0,
      LoginDateTime: '1900-01-01',
    };
    this.LocalData = {
      Mobileno: '',
      Memberno: '',
    };
    this.APILink = {
      API_POSMobileScan: 'api/POS/spGetPOSItemDetail_POSMobileScan?',
      API_GetPOSSettings: (DeviceName, ProfileId) => {
        return `api/POS/GetPOSSettings?TerminalName=${DeviceName}&ProfileId=${ProfileId}`;
      },
      API_MaxMemberNumber: Cardtype => {
        return `api/POS/GetMaxCardNo?Cardtype=${Cardtype}`;
      },
      API_LoyaltyGetVillage: 'api/CRM/GetVillage',
      API_UpdatePrintCountBasedOnType: 'api/POS/UpdatePrintCountBasedOnType?',
      API_getSalesMan: 'api/POS/SalesmanLocSearch?Loccode=1',
      API_GetCustomerLoyaltyF2: 'api/POS/POSCustomerSearch?Membersearch=2',
      API_POSMemberSearch: 'api/POS/POSMemberSearch?',
      API_LoyaltyGetCardTypeList: 'api/POS/GetCardTypeList',
      API_getHoldsummary:
        "api/POS/getHoldsummary_Cash?id=0&Terminal=''&User=''",
      API_GetHoldDetails: 'api/POS/spGetHoldDetails_CashMemo?',
      API_Return:
        '/api/POS/SpMasterDetail_ReturnMemo?Memodate=1900-01-01&Memono=/RM0001&Returndays=500',
      API_PosSave: 'api/POS/sp_POSSave',
      API_getReprint: (MemoType, User, DeviceName) => {
        return `api/POS/SpMasterDetail_ReprintSelect?MemoType=${MemoType}&User=${User}&Terminal=${DeviceName}&Days=300`;
      },
      API_ItemDiscount:
        'api/POS/Get_pos_ItemDiscount?disc=5&Profileid=p1&LocCode=1',
      API_MemoDiscount: 'api/POS/Get_pos_Discount?disc=5&',
      API_GetVerifyReturn:
        'api/POS_Verify/GetVerifyReturnData?Terminal=ICUBE-1005',
    };
    this.state = {
      APIData: {
        IcodeDetails: [],
      },
      SaveFormet: {
        detaildata: [],
      },
      TakenPrintCount: 1,
      ReturnPrintCount: 1,
      isTaken: true,
      ListSourceData: [],
      HoldMemoNo: '',
      show: false,
      CustomerName: '',
      Icode: '',
      width: Dimensions.get('window').width,
      qrvalue: '',
      SMcode: '',
      scanText: '',
      openScanner: false,
      DiscountVisible: 'flex',
      OnCallList: null,
      CustomerList: [],
      SMList: [],
      SelectedCustomer: [],
      CustomerNumber: '',
      detaildata: [],
      CardTypeList: [],
      VillageList: [],
      DubBarcode: false,
      SelectedCus: '',
      CustomerValidation: true,
      CustomerFilter: false,
      NewCustomer: false,
      NewCustomerDetails: {
        ...this.DefaultNewCustomerField,
        DefaultCardTypeCode: '',
        AllowMemberNoEdit: false,
      },
      VRetrive: false,
      AllBill: false,
      BillItemDetails: false,
      ItemDiscount: false,
      AllBillList: [],
      AllBillItemList: [],
      B_ItemDetais: [],
      SearchText: '',
      IsHold: 0,
      RetriveList: [],
      ItemDiountList: [],
      VerifyReturnList: [],
      C_Memberno: '',
      C_LoctionCode: '',
      C_CardCode: '',
      RateChangable: false,
      MemoType: 'CM',
      PrintType: 'Preview',
      PrinterName: '',
      isMandatoryScanSalesManAfterBarcode: false,
      isAllowReturnNonSaleItem: true,
      isTagSalesManMandatory: false,
      isTagCustomerMandatory: false,
      isEnterSalesManCode: false,
      isAllowCancelCM: false,
      isAllowCancelDS: false,
      AllowedRePrintCount: 0,
      NegatveQtyAlertType: 'Empty',
      isShowTeller: false,
    };
    this.CustomerActionButtons = [
      {
        text: 'Add Customer',
        icon: require('../../../assets/images/import.png'),
        name: 'Add Customer',
        color: globalColorObject.Color.Primary,
        buttonSize: 45,
        position: 2,
      },
      {
        text: 'Clear',
        icon: require('../../../assets/images/Clear.png'),
        name: 'Clear',
        color: globalColorObject.Color.Primary,
        buttonSize: 45,
        position: 2,
      },
    ];
    this.ScanInputText = React.createRef();
    this.BarcodeFlatList = React.createRef();
  }

  ActionButtons = () => {
    let StaticButton = [
      {
        text: 'Hold',
        icon: require('../../../assets/images/holding_box.png'),
        name: 'Hold',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      },
      {
        text: 'Bills',
        icon: require('../../../assets/images/Print.png'),
        name: 'Bills',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      },
      {
        text: 'Reports',
        icon: require('../../../assets/images/Print.png'),
        name: 'Reports',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      },
      {
        text: 'Retrive',
        icon: require('../../../assets/images/import.png'),
        name: 'Retrive',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      },
      {
        text: 'Clear',
        icon: require('../../../assets/images/Clear.png'),
        name: 'Clear',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      },
    ];
    if (this.state.MemoType == 'CM') {
      StaticButton.unshift({
        text: 'Teller',
        icon: require('../../../assets/images/info-i.png'),
        name: 'Teller',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      });
      StaticButton.unshift({
        text: this.state.isTaken ? 'Return' : 'Taken',
        icon: require('../../../assets/images/undo-light.png'),
        name: 'isTaken',
        color: globalColorObject.Color.Primary,
        buttonSize: 30,
        size: 25,
      });
    }
    return StaticButton;
  };
  CallShareFile = async (
    MemoType,
    InvoiceID,
    InvoiceNO,
    InvoiceDate,
    isPrint,
    BillListIndex = -1,
  ) => {
    try {
      BillListIndex = parseInt(isnull(String(BillListIndex), -1));
      if (BillListIndex >= 0) {
        let get_AllBillList = [...this.state.AllBillList];
        if (get_AllBillList.length > BillListIndex) {
          this.UpdatePrintCountBasedOnType(MemoType, InvoiceID);
          get_AllBillList[BillListIndex].PRINTCOUNT =
          parseInt(isnull(get_AllBillList[BillListIndex].PRINTCOUNT, 0)) + 1;
          this.setState(prevstate => ({
            ...prevstate,
            AllBillList: get_AllBillList,
          }));
        }
      }
      if (isPrint) {
        if (this.state.PrintType == 'Print') {
          Request.get(
            `${this.IP}/api/POS/GetPOSPrintControlFile?CallFrom=${MemoType}&Id=${InvoiceID}`,
            this.Token,
          ).then(res => {
            let printdata = res.data;
            let PrintCount = parseInt(isnull(this.state.TakenPrintCount, 1));
            if (MemoType == 'RM') {
              PrintCount = parseInt(isnull(this.state.ReturnPrintCount, 1));
            }
            CallBluetoothPrint(
              isnull(this.state.PrinterName, ''),
              PrintCount,
              printdata,
            );
            // BLEPrinter.init().then(() => {
            //   // console.log('call printBill inti : ', MemoType);
            //   BLEPrinter.getDeviceList()
            //     .then(printer => {
            //       let get_PrintList = printer || [];
            //       let filter_PrintName = isnull(this.state.PrinterName, '');
            //       if (get_PrintList.length > 0) {
            //         let selectedPrinter = get_PrintList.find(
            //           printer =>
            //             printer.device_name == filter_PrintName ||
            //             filter_PrintName == '',
            //         );
            //         if (selectedPrinter) {
            //           BLEPrinter.connectPrinter(
            //             selectedPrinter.inner_mac_address,
            //           )
            //             .then(res => {
            //               let PrintCount = parseInt(
            //                 isnull(this.state.TakenPrintCount, 1),
            //               );
            //               if (MemoType == 'RM') {
            //                 PrintCount = parseInt(
            //                   isnull(this.state.ReturnPrintCount, 1),
            //                 );
            //               }
            //               do {
            //                 PrintCount--;
            //                 BLEPrinter.printText(printdata);
            //               } while (PrintCount > 0);
            //             })
            //             .catch(err => AlertMessage(err));
            //         } else {
            //           AlertMessage(`Invalid Printer '${filter_PrintName}'.`);
            //         }
            //       } else {
            //         AlertMessage('No Printer available.');
            //       }
            //     })
            //     .catch(err => AlertMessage(err));
            // });
          });
        } else if (this.state.PrintType == 'Preview') {
          await sharefileForCallFRom(
            MemoType,
            InvoiceID,
            InvoiceNO,
            InvoiceDate,
            MemoType == 'DS' ? '7.1.0' : MemoType == 'RM' ? '7.3.0' : '7.2.0',
            this.SessionData.Loccode,
            isPrint,
          );
        }
      } else {
        await sharefileForCallFRom(
          MemoType,
          InvoiceID,
          InvoiceNO,
          InvoiceDate,
          MemoType == 'DS' ? '7.1.0' : MemoType == 'RM' ? '7.3.0' : '7.2.0',
          this.SessionData.Loccode,
          isPrint,
        );
      }
      // this.HandleFetchFlag(true);
    } catch (error) {
      AlertError(error);
    }
  };

  OpenNewCustomer = async () => {
    let get_DefaultCardCode = isnull(
      this.state.NewCustomerDetails.DefaultCardTypeCode,
      '',
    );
    if (get_DefaultCardCode != '') {
      let get_MaxNumber = await this.GetMemberNo(get_DefaultCardCode);
      // console.log('max mmebr return data : ', get_MaxNumber);
      this.setState(prevState => ({
        ...prevState,
        NewCustomer: true,
        NewCustomerDetails: {
          ...prevState.NewCustomerDetails,
          ...this.DefaultNewCustomerField,
          CardTypeCode: get_DefaultCardCode,
          MemberNo: isnull(get_MaxNumber.MemberNo, ''),
          CardNo: isnull(get_MaxNumber.CardNo, ''),
        },
      }));
    } else {
      AlertMessage('No card type available to create customer.');
    }
  };

  UpdatePrintCountBasedOnType = (MemoType,Id) => {
    Request.get(
      `${this.IP}${this.APILink.API_UpdatePrintCountBasedOnType}MemoType=${MemoType}&Id=${Id}`,
      this.Token,
    ).then(res => console.log(res.status)).catch(err => console.error(err));
  };

  GetMemberNo = async CardTypeCode => {
    let get_MaxNumber = {CardNo: '', MemberNo: ''};
    if (CardTypeCode != '') {
      get_MaxNumber = await Request.get(
        `${this.IP}${this.APILink.API_MaxMemberNumber(CardTypeCode)}`,
        this.Token,
      )
        .then(res => {
          if (res.status === 200) {
            let get_MaxNumList = res.data;
            // console.log('max member no : ', get_MaxNumList);
            if (get_MaxNumList.length > 0) {
              return {
                CardNo: isnull(get_MaxNumList[0].CardNo, ''),
                MemberNo: isnull(get_MaxNumList[0].MemberNo, ''),
              };
            }
          }
          return get_MaxNumber;
        })
        .catch(err => AlertStatusError(err));
    }
    // this.setState(prevState => ({
    //   ...prevState,
    //   NewCustomerDetails: {
    //     ...prevState.NewCustomerDetails,
    //     CardTypeCode: CardTypeCode,
    //     MemberNo: isnull(get_MaxNumber.MemberNo, ''),
    //     CardNo: isnull(get_MaxNumber.CardNo, ''),
    //   },
    // }));
    return get_MaxNumber;
  };
  componentDidMount() {
    this.LoadInitialData();
  }

  componentWillReceiveProps() {
    this.clear();
  }
  LoadInitialData = async () => {
    const {
      get_IP,
      get_Token,
      get_Location,
      get_ProfileId,
      get_UserName,
      get_LoginDateTime,
      get_POSPrintTerminalName,
    } = await GetSessionData();
    this.SessionData.IP = get_IP;
    this.SessionData.Token = get_Token;
    this.SessionData.ProfileId = get_ProfileId;
    this.SessionData.UserName = get_UserName;
    this.SessionData.Loccode = get_Location;
    this.SessionData.LoginDateTime = get_LoginDateTime;
    this.SessionData.DeviceName = await MobileDeviceName();
    this.IP = get_IP;
    this.Token = get_Token;
    // console.log('LoginDateTime : ', this.SessionData.LoginDateTime);
    //     Request.get(
    //     `${this.IP}/api/POS/GetPOSPrintControlFile?CallFrom=CM&Id=33`,
    //     this.Token,
    //   ).then(res => {
    //     let printdata = res.data;

    //  BLEPrinter.init().then(()=> {
    //     console.log('printer inti ');
    //     BLEPrinter.getDeviceList().then(printer => {
    //       BLEPrinter.connectPrinter(printer[0].inner_mac_address).then((res) => {
    //         console.log('object', printer[0].inner_mac_address);
    //         console.log('printdata', printdata);
    //         BLEPrinter.printText(printdata);
    //       }).catch(err => console.error('print connectPrinter : ', err));
    //     }).catch(err => console.error('print err : ', err));
    //   });
    //   });
    this.ReturnPOSGlobalSettings();
    // this.GetReturnDetails();
    this.GetSMDetail();
    this.ItemDiscount();
    this.GetCardTypeList();
    // this.MemoDiscount();
    // this.GetVerifyReturn();
    this.LoadVillage();
  };

  // GetReturnDetails = async () => {
  //   Request.get(`${this.IP}${this.APILink.API_Return}`, this.Token)
  //     .then(res => {
  //       // console.log('GetReturnDetails', JSON.stringify(res.data));
  //     })
  //     .catch(err => AlertError(err));
  // };

  GetDiscountType = item => {
    // console.log('In', item);
    discountFactor = item.Factor;
    this.SendDicound(discountFactor);
    // console.log('discountFactor', discountFactor);
  };
  MemoDiscount = () => {
    Request.get(
      `${this.IP}${this.APILink.API_MemoDiscount}Profileid=${this.SessionData.ProfileId}`,
      this.Token,
    ).then(res => {
      console.log('MemoDiscount Discount', res);
    });
  };
  GetVerifyReturn = () => {
    Request.get(
      `${this.IP}${this.APILink.API_GetVerifyReturn}`,
      this.Token,
    ).then(res => {
      // console.log('GetVerifyReturn ', res);
      if (res.status === 200) {
        this.setState({
          VerifyReturnList: res.data,
        });
      } else {
        AlertStatusError(res);
      }
    });
  };

  GetCardTypeList = async () => {
    Request.get(
      `${this.IP}${this.APILink.API_LoyaltyGetCardTypeList}`,
      this.Token,
    )
      .then(res => {
        // console.log('card type res : ', res);
        if (res.status === 200) {
          let getdata = res.data;
          let get_CardTypeCode =
            getdata.length > 0 ? getdata[0].CardTypeCode : '';
          // console.log(
          //   'CardTypeList : ',
          //   getdata,
          //   ' get_CardTypeCode : ',
          //   get_CardTypeCode,
          // );
          this.setState(prevState => ({
            CardTypeList: getdata,
            NewCustomerDetails: {
              ...prevState.NewCustomerDetails,
              DefaultCardTypeCode: get_CardTypeCode,
            },
          }));
        }
      })
      .catch(err => AlertError(err));
  };

  LoadVillage = async () => {
    Request.get(`${this.IP}${this.APILink.API_LoyaltyGetVillage}`, this.Token)
      .then(res => {
        // console.log('village : ', JSON.stringify(res));
        if (res.status === 200) {
          this.setState({
            VillageList: res.data,
          });
        }
      })
      .catch(err => AlertError(err));
  };

  ReturnPOSGlobalSettings = async () => {
    Request.get(
      `${this.IP}${this.APILink.API_GetPOSSettings(
        this.SessionData.DeviceName,
        this.SessionData.ProfileId,
      )}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          let getdata = JSON.parse(res.data);
          if (getdata) {
            let get_MemoType = isnull(getdata.MemoType, 'CM');
            // let get_MemoType = 'DS';
            let get_PrintType = isnull(getdata.PrintType, 'Preview');
            let get_PrinterName = isnull(getdata.PrinterName, '');
            let get_NegativeQty = isnull_bool(getdata.NegativeQty, false);
            let get_NegativeQtyAllow = isnull_bool(getdata.NegativeQtyAllow, false);
            let get_isAllowMemberNoEdit = isnull(
              getdata.isAllowMemberNoEdit,
              false,
            );

            let get_TakenPrintCount = isnull(getdata.TakenPrintCount, 1);
            let get_ReturnPrintCount = isnull(getdata.ReturnPrintCount, 1);
            let get_isAllowCancelCM = isnull_bool(getdata.isAllowCancelCM, false);
            let get_AllowedRePrintCount = isnull(
              getdata.AllowedRePrintCount,
              0,
            );
            let get_isAllowCancelDS = isnull(getdata.isAllowCancelDS, false);
            let get_isAllowReturnNonSaleItem = isnull_bool(getdata.isAllowReturnNonSaleItem, true);
            let get_isTagSalesManMandatory = isnull_bool(
              getdata.isTagSalesManMandatory,
              false,
            );
            let get_isTagCustomerMandatory = isnull_bool(
              getdata.isTagCustomerMandatory,
              false,
            );
            let get_MandatoryScanSalesManAfterBarcode =
            isnull_bool(
                getdata.MandatoryScanSalesManAfterBarcode,
                false,
              );
            // console.log(
            //   'get_isAllowReturnNonSaleItem : ',
            //   get_isAllowReturnNonSaleItem,
            //   ' type off get_isAllowReturnNonSaleItem : ',
            //   typeof(get_isAllowReturnNonSaleItem),
            //   ' getdata.isAllowReturnNonSaleItem :',
            //   getdata.isAllowReturnNonSaleItem
            // );
            this.setState(prevState => ({
              ...prevState,
              MemoType: get_MemoType,
              PrintType: get_PrintType,
              PrinterName: get_PrinterName,
              isTagCustomerMandatory: get_isTagCustomerMandatory,
              isTagSalesManMandatory: get_isTagSalesManMandatory,
              isAllowReturnNonSaleItem: get_isAllowReturnNonSaleItem,
              isAllowCancelDS: get_isAllowCancelDS,
              isAllowCancelCM: get_isAllowCancelCM,
              AllowedRePrintCount: get_AllowedRePrintCount,
              TakenPrintCount: get_TakenPrintCount,
              ReturnPrintCount: get_ReturnPrintCount,
              NegatveQtyAlertType: get_NegativeQty
                ? 'Restrict'
                : get_NegativeQtyAllow
                ? 'Message'
                : 'Empty',
              isMandatoryScanSalesManAfterBarcode:
                get_MandatoryScanSalesManAfterBarcode,
              NewCustomerDetails: {
                ...prevState.NewCustomerDetails,
                AllowMemberNoEdit: get_isAllowMemberNoEdit,
              },
            }));
            this.props.navigation.setOptions({
              title: get_MemoType == 'CM' ? 'POS' : 'Delivery Slip',
            });
          }
        }
      })
      .catch(err => AlertError(err));
  };

  ItemDiscount = async () => {
    Request.get(`${this.IP}${this.APILink.API_ItemDiscount}`, this.Token)
      .then(res => {
        // console.log('Item Discount', res);
        if (res.status === 200) {
          this.setState({
            ItemDiountList: res.data,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };
  GetRetrieve = async () => {
    Request.get(`${this.IP}${this.APILink.API_getHoldsummary}`, this.Token)
      .then(res => {
        if (res.status === 200) {
          this.setState({
            RetriveList: res.data,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };
  GetAllBills = async () => {
    Request.get(
      `${this.IP}${this.APILink.API_getReprint(
        this.state.MemoType,
        this.SessionData.UserName,
        this.SessionData.DeviceName,
      )}`,
      this.Token,
    )
      .then(res => {
        // console.log('all bill : ', this.state.MemoType);
        if (res.status === 200) {
          this.setState({
            AllBillList: res.data,
          });
          // this.setState({
          //   AllBillItemList: res.data.Table1,
          // });

          // console.log(res.data.Table1);
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };

  GetSMDetail = async () => {
    Request.get(`${this.IP}${this.APILink.API_getSalesMan}`, this.Token)
      .then(res => {
        if (res.status === 200) {
          this.setState({
            SMList: res.data,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };

  HandleActionButtonClick = name => {
    // console.log('handle ', name);
    if (name === 'Hold') {
      this.Hold();
    } else if (name === 'Reports') {
      this.props.navigation.navigate('POSBlutoothPrintListNav', {
        PrinterName: this.state.PrinterName,
      });
    } else if (name === 'Bills') {
      this.AllBills();
    } else if (name === 'Retrive') {
      this.Retrive();
    } else if (name === 'Clear') {
      this.clear();
    } else if (name === 'isTaken') {
      this.setState(prevState => ({
        ...prevState,
        isTaken: this.state.isTaken ? false : true,
      }));
      this.ScanInputText.focus();
    } else if (name === 'Teller') {
      this.setState(prevState => ({
        ...prevState,
        isShowTeller: this.state.isShowTeller ? false : true,
      }));
    }
    
  };
  HandleCustomerActionButtonClick = name => {
    if (name === 'Add Customer') {
      this.props.navigation.navigate('CustomerNav', {
        TotalCash: this.subtotalPrice().toFixed(2),
      });
      this.setState({CustomerFilter: false});
    } else if (name === 'Clear') {
      this.clear();
    }
  };

  clear() {
    this.setState({
      isTaken: true,
      detaildata: [],
      SelectedCustomer: [],
      CustomerName: '',
      CustomerNumber: '',
      scanText: '',
      OnCallList: null,
      isEnterSalesManCode: false,
    }),
      this.setState(prevState => ({
        SaveFormet: {...prevState.SaveFormet, detaildata: []},
      }));
  }

  Hold() {
    this.setState({IsHold: 1});
    const Data = this.state.detaildata;
    const changeKeyObjects = (arr, replaceKeys) => {
      return arr.map(item => {
        const newItem = {};
        Object.keys(item).forEach(key => {
          newItem[replaceKeys[key]] = item[[key]];
        });
        return newItem;
      });
    };
    const replaceKeys = {
      Aggno: 'aggno',
      BenifitDiscount: 'benifitDiscount',
      CustomerBenifitID: 'customerBenifitID',
      DId: 'did',
      IsPromoGiftVoucher: 'isPromoGiftVoucher',
      IsPromoPoints: 'isPromoPoints',
      ItemPoints: 'itemPoints',
      LessQty: 'lessQty',
      Loccode: 'loccode',
      MRP: 'mrp',
      POId: 'poId',
      PromoNo: 'promoNo',
      RemoveTax: 'removeTax',
      SP_No: 'spNo',
      scanunitqty: 'scanUnit',
      TaxCode: 'taxCode',
      WSPRate: 'isWSP',
      barcode: 'barcode',
      baseprice: 'basicPrice',
      basevalue: 'basicValue',
      disc_AlowChng: 'itemDiscAc',
      disc_Md_Val: 'memoDiscValue',
      disc_Type: 'itemDiscType',
      disc_Value: 'promoValue',
      disc_id_Factor: 'itemDiscFactor',
      disc_id_Val: 'itemDiscValue',
      disc_id_name: 'itemDiscName',
      disc_md_AC: 'memoDiscAc',
      disc_md_Factor: 'memoDiscFactor',
      disc_md_name: 'memoDiscName',
      disc_md_type: 'memoDiscType',
      discount: 'discount',
      dsdate: 'dsDate',
      dsno: 'dsNo',
      gross: 'gross',
      grpcode: 'grpCode',
      icode: 'icode',
      iqty: 'iQty',
      itemname: 'itemName',
      oemcode: 'oemCode',
      qty: 'qty',
      salespersonname: 'spName',
      salesprice: 'salesPrice',
      serialno: 'serialNo',
      stkptcode: 'stkptCode',
      taxvalue: 'taxValue',
      unitofmeasure: 'uom',
    };

    const newArray = changeKeyObjects(Data, replaceKeys);
    this.state.SaveFormet.detaildata.push(...newArray);
    function getFields(input, field) {
      var output = [];
      for (var i = 0; i < input.length; ++i) output.push(input[i][field]);
      return output;
    }
    var result = getFields(Data, 'qty');
    var FinalQty = eval(result.join('+')); // returns [ 1, 3, 5 ]
    var SummaryBaseValue = getFields(Data, 'basevalue');
    var FinalBaseValue = eval(SummaryBaseValue.join('+'));
    // console.log('Final Save Data', this.state.SaveFormet.detaildata);
    let SaveObject = {
      isDeliverySlip: 0,
      detaildata: this.state.SaveFormet.detaildata,
      s_BaseValue: FinalBaseValue,
      s_Qty: FinalQty,
      s_ItemDisc: 0.0,
      s_MemoDisc: 0.0,
      s_SalesValue: 0, // Sub Total
      s_Roundoff: 0.0,
      s_ReturnValue: 0.0,
      s_NetValue: 0,
      s_CashValue: 0,
      s_CardValue: 0,
      s_CardDetail: null,
      s_CreditValue: 0.0,
      s_CreditDetail: null,
      s_TenderValue: 0,
      s_Change: 0,
      s_PrintCount: 0,
      s_Remarks: 'icube mobile',
      s_Terminal: 'ICUBE-1005',
      s_User: 'Cash',
      s_MemberNo: this.state.C_Memberno,
      s_Reedem: 0.0,
      s_MemberLocCode: this.state.C_LoctionCode,
      s_MemberCardType: this.state.C_CardCode,
      s_CashMemoNo: '',
      s_CashMemoDate: new Date().toLocaleString(),
      s_PromoDisc: 0.0,
      s_UserMemoNo: '',
      s_PayVoucher: 0.0,
      s_TaxValue: 0.0,
      s_ScanUnitQty: FinalQty,
      s_ID: 0,
      s_PromoPoint: 0.0,
      s_AgainstCashMemo: 0,
      s_HoldMemoNo: this.state.HoldMemoNo,
      s_IsHold: 1,
      s_IsDS: 0,
      s_IsCredit: 0,
      s_IsMoreMOP: 0,
      s_DiscUser: '',
      s_IsPOSOrder: 0,
      s_OrderDueDate: '0001-01-01T00:00:00',
      s_OrderDelivery: false,
      s_GiftVoucher: 0.0,
      s_GiftVoucherReference: '',
      s_BenifitDiscount: 0.0,
      s_BenifitCode: '',
      s_EarnPoints: 0.0,
      s_RedeemOTP: 0,
      s_AdjCollection: 0.0,
      s_LessQty: 0.0,
      s_ItemPoints: 0.0,
      s_RedeemPoints: 0.0,
      s_PromoGiftVoucher: 0.0,
      s_WSPUser: '',
    };
    // console.log('SaveObject', SaveObject);
    Request.post(
      `${this.IP}${this.APILink.API_PosSave}`,
      SaveObject,
      this.Token,
    )
      .then(
        res => console.log('Save Res', res),
        AlertMessage('Saved Successfully'),
        this.clear(),
      )
      .catch(err => AlertError(err));
  }

  SaveLoyalty = async () => {
    let SaveObject = {
      Cardtype: this.state.NewCustomerDetails.CardTypeCode,
      Cardno: this.state.NewCustomerDetails.CardNo,
      Memberno: this.state.NewCustomerDetails.MemberNo,
      Displayname: this.state.NewCustomerDetails.Name,
      Mobileno: this.state.NewCustomerDetails.MobileNo,
      VillageID: isnull(nullif(this.state.NewCustomerDetails.VillageID, ''), 0),
      Village: this.state.NewCustomerDetails.Village,
      CreatedBy: this.SessionData.UserName,
      GSTNO: this.state.NewCustomerDetails.GSTNo,
    };
    // console.log('Final Save', SaveObject);
    // AlertMessage("Saved Successfully")
    await Request.post(
      `${this.IP}api/POS/spSaveLoyalty`,
      SaveObject,
      this.Token,
    )
      .then(res => {
        let Error_MEssage = isnull(res?.Message, 'Failed to save');
        if (res.status && res.status == '200') {
          let get_ReturnData = res.json();
          return get_ReturnData;
        } else {
          AlertMessage(Error_MEssage);
        }
      })
      .then(get_ReturnData => {
        // console.log('get_ReturnData : ', get_ReturnData);
        if (get_ReturnData) {
          let is_Save = false;
          let ID = 0;
          ID = isnull_number(get_ReturnData.ID, 0);
          if (ID > 0) {
            is_Save = true;
          }
          if (is_Save) {
            this.SetCustomerData(get_ReturnData);
            AlertMessage('Saved Successfully');
          } else {
            AlertMessage('Failed to save.');
          }
        }
      })
      .catch(err => AlertError('' + err));
  };

  AllBills = () => {
    this.setState({AllBill: true});

    // this.GetRetrieve();
    this.GetAllBills();
  };
  Retrive = () => {
    this.setState({VRetrive: true});
    this.GetRetrieve();
  };
  ApiHit = event => {
    // let currentPosition = event.nativeEvent.contentOffset.y;
    // if(currentPosition>1000){
    // 	Request.get(``,this.Token)
    // 	.then(res => {
    // 	  if (res.status === 200) {
    //
    // 		} else {
    // 		  AlertStatusError(res);
    // 		}
    // 	})
    // 	.catch(err => AlertError(err));
    //   }
    // console.log('Position Load Additional Data', currentPosition > 50);
    // console.log('Position', currentPosition);
  };
  SetCustomerData = data => {
    this.setState({
      C_Memberno: data.Memberno,
      C_LoctionCode: data.LocCode,
      C_CardCode: data.CardCode,
      SelectedCustomer: data,
      CustomerName: data.Displayname,
      CustomerNumber: data.Mobileno,
      CustomerFilter: false,
      NewCustomer: false,
    });
  };

  ClearSearch = () => {
    this.setState({SearchText: ''});
  };
  SearchBillItem = async SearchText => {
    await this.setState({SearchText});
    let AllBillItem = this.state.B_ItemDetais;
    let txt = SearchText.toLowerCase();
    let ListSourceData = AllBillItem.filter(obj => {
      let BasicValue = (obj.BasicValue || '').toString().toLowerCase();
      let Discount = (obj.Discount || '').toString().toLowerCase();
      let icode = (obj.icode || '').toString().toLowerCase();
      let MasterID = (obj.MasterID || '').toString().toLowerCase();
      let ItemName = (obj.ItemName || '').toString().toLowerCase();
      let SalesPrice = (obj.SalesPrice || '').toString().toLowerCase();

      return (
        BasicValue.includes(txt) ||
        Discount.includes(txt) ||
        icode.includes(txt) ||
        MasterID.includes(txt) ||
        ItemName.includes(txt) ||
        SalesPrice.includes(txt)
      );
    });
    // console.log('ListSourceData', ListSourceData);
    this.setState({ListSourceData});
  };
  SearchBillList = async SearchText => {
    await this.setState({SearchText});
    let AllBillList = this.state.AllBillList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = AllBillList.filter(obj => {
      let NetValue = (obj.NetValue || '').toString().toLowerCase();
      let ReturnValue = (obj.ReturnValue || '').toString().toLowerCase();
      let Time = (obj.Time || '').toString().toLowerCase();
      let MemoDate = (obj.MemoDate || '').toString().toLowerCase();
      let SalesValue = (obj.SalesValue || '').toString().toLowerCase();
      let MemoNo = (obj.MemoNo || '').toString().toLowerCase();
      let ID = (obj.ID || '').toString().toLowerCase();

      return (
        NetValue.includes(txt) ||
        ReturnValue.includes(txt) ||
        Time.includes(txt) ||
        MemoDate.includes(txt) ||
        SalesValue.includes(txt) ||
        ID.includes(txt) ||
        MemoNo.includes(txt)
      );
    });
    // console.log('ListSourceData', ListSourceData);
    this.setState({ListSourceData});
  };
  SearchRetrive = async SearchText => {
    await this.setState({SearchText});
    let RetList = this.state.RetriveList;
    let txt = SearchText.toLowerCase();
    let ListSourceData = RetList.filter(obj => {
      let MemberName = (obj.MemberName || '').toString().toLowerCase();
      let MemoDate = (obj.MemoDate || '').toString().toLowerCase();
      let MemoNo = (obj.MemoNo || '').toString().toLowerCase();
      let Net = (obj.Net || '').toString().toLowerCase();
      let QTY = (obj.QTY || '').toString().toLowerCase();
      let Mobileno = (obj.Mobileno || '').toString().toLowerCase();
      return (
        MemberName.includes(txt) ||
        MemoDate.includes(txt) ||
        MemoNo.includes(txt) ||
        Net.includes(txt) ||
        QTY.includes(txt) ||
        Mobileno.includes(txt)
      );
    });
    this.setState({ListSourceData});
  };
  SearchAndSetCustomer = async Customer => {
    let retREsult;
    // console.log('customer',Customer);
    // console.log('cust api : ',
    //   `${this.IP}${this.APILink.API_POSMemberSearch}Memberno=${Customer}&Membersearch=1`,
    //   this.Token,
    // );
    if (isnull(Customer, '') == '') {
      this.GetCustomerLoyaltyF2();
    } else {
      retREsult = await Request.get(
        `${this.IP}${this.APILink.API_POSMemberSearch}Memberno=${Customer}&Membersearch=1`,
        this.Token,
      )
        .then(res => {
          if (res.status === 200) {
            this.setState({
              ListSourceData: res.data,
            });
            return res.data;
          } else {
            AlertStatusError(res);
          }
        })
        .catch(err => AlertStatusError(err));
    }
    return retREsult;
  };

  CustomerNav = () => {
    this.GetCustomerLoyaltyF2();
    this.setState({CustomerFilter: true});
  };
  HandalCus = async () => {
    if (
      this.state.CustomerNumber == undefined ||
      this.state.CustomerNumber == ''
    ) {
      AlertMessage('Enter Customer Number');
    } else if (this.state.CustomerNumber >= 0) {
      const Number = this.state.CustomerNumber;
      const CustomerList = await this.SearchAndSetCustomer(
        isnull(Number, 'N/A'),
      );
      const obj = CustomerList.length > 0 ? CustomerList[0] : undefined;
      // console.log(
      //   'CustomerList : ',
      //   CustomerList,
      //   ' Number : ',
      //   Number,
      //   ' obj : ',
      //   obj,
      // );
      if (obj) {
        this.setState({
          SelectedCustomer: obj,
          CustomerName: obj.Displayname,
          CustomerNumber: obj.Mobileno,
          CustomerValidation: true,
        });
      }
    } else {
      this.setState({CustomerValidation: false});
      AlertMessage('Invalid Customer Number');
    }
  };

  GetCustomerLoyaltyF2 = async () => {
    Request.get(
      `${this.IP}${this.APILink.API_GetCustomerLoyaltyF2}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          this.setState({
            ListSourceData: res.data,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };

  ApplyITemINformationBasedOnSCanType = (objScanItemDetails, MathQty) => {
    // console.log('objScanItemDetails : ', objScanItemDetails);
    let Qty_Abs = Math.abs(parseInt(objScanItemDetails['qty'])) + MathQty;
    let BasePrice_Abs = Math.abs(parseFloat(objScanItemDetails['baseprice']));
    let SingValue = this.state.isTaken ? 1 : -1;
    let Qty_Sign = SingValue * Qty_Abs;
    objScanItemDetails['qty'] = Qty_Sign;
    objScanItemDetails['MRP'] =
      SingValue * Math.abs(parseFloat(objScanItemDetails['MRP']));
    objScanItemDetails['baseprice'] = SingValue * BasePrice_Abs;
    objScanItemDetails['basevalue'] = BasePrice_Abs * Qty_Sign;
    objScanItemDetails = this.ApplyDiscountCalculation(
      objScanItemDetails,
      objScanItemDetails['disc_Type'],
      BasePrice_Abs,
    );
    objScanItemDetails['salesprice'] =
      SingValue *
      parseFloat(
        Math.abs(objScanItemDetails['basevalue']) -
          Math.abs(objScanItemDetails['discount']),
      ).toFixed(2);
    objScanItemDetails['gross'] = Math.abs(objScanItemDetails['salesprice']);
    objScanItemDetails['scanUnit'] = Qty_Sign;
    objScanItemDetails['scanunitqty'] = Qty_Sign;
    objScanItemDetails['iqty'] = Qty_Sign;
    return objScanItemDetails;
  };

  BindProductItemInfo = (newArray, objScanItemDetails, IcodeDetailes) => {
    // console.log('object newArray : ', newArray);
    // console.log('object objScanItemDetails : ', objScanItemDetails);
    // console.log('object IcodeDetailes : ', IcodeDetailes);
    if (IcodeDetailes) {
      if (!IcodeDetailes.Individual) {
        IcodeDetailes = this.ApplyITemINformationBasedOnSCanType(
          IcodeDetailes,
          parseFloat(IcodeDetailes['BARUNIT']),
        );
      } else if (IcodeDetailes.Individual) {
        objScanItemDetails = this.ApplyITemINformationBasedOnSCanType(
          objScanItemDetails,
          parseFloat(0),
        );
        newArray.push(objScanItemDetails);
      }
    } else {
      objScanItemDetails = this.ApplyITemINformationBasedOnSCanType(
        objScanItemDetails,
        parseFloat(0),
      );
      newArray.push(objScanItemDetails);
    }
    this.setState({
      OnCallList: null,
      detaildata: newArray,
      ValidateIcode: true,
      isEnterSalesManCode: false,
      openScanner: false,
      DubBarcode: false,
      scanText: '',
    });
  };

  BindProductListData = (objScanItemDetails, SalesManCode = '') => {
    const newArray = this.state.detaildata; // Create a copy
    let LAsTAgSp = isnull(SalesManCode, '');
    if (isnull(SalesManCode, '') == '') {
      LAsTAgSp =
        isnull(objScanItemDetails.SP_No, '') == ''
          ? newArray.length > 0
            ? newArray[newArray.length - 1].SP_No
            : ''
          : objScanItemDetails.SP_No;
    }
    objScanItemDetails.SP_No =
      LAsTAgSp != '' ? LAsTAgSp : objScanItemDetails.SP_No;
    let get_FilterItemDetails = newArray.filter(
      e =>
        e.icode.toLowerCase() === objScanItemDetails.icode.toLowerCase() &&
        ((this.state.isTaken && e.qty > 0) || !this.state.isTaken) &&
        ((!this.state.isTaken && e.qty < 0) || this.state.isTaken),
    );
    // console.log('my filter :', IcodeDetailes);
    let IcodeDetailes =
      (get_FilterItemDetails || []).length > 0
        ? get_FilterItemDetails[0]
        : undefined;

    if (
      this.state.isTaken &&
      (this.state.NegatveQtyAlertType == 'Message' ||
        this.state.NegatveQtyAlertType == 'Restrict')
    ) {
      let get_StockQty = parseFloat(isnull(objScanItemDetails.stock, 0));
      let get_Curr_Qty = parseFloat(isnull(objScanItemDetails.BARUNIT, 0));
      let get_Prev_ScanQty = parseFloat(
        get_FilterItemDetails.reduce((accuQty, item) => {
          return accuQty + isnull(item.qty, 0);
        }, 0),
      );
      if (get_StockQty < get_Curr_Qty + get_Prev_ScanQty) {
        // console.log(
        //   'Stock Exceeded : ',
        //   get_StockQty,
        //   ' get_Curr_Qty : ',
        //   get_Curr_Qty,
        //   ' get_Prev_ScanQty : ',
        //   get_Prev_ScanQty,
        // );
        if (this.state.NegatveQtyAlertType == 'Message') {
          ConfirmationWithAllInput(
            `Available Stock is [${get_StockQty}]. Any way do you want to continue.`,
            'Yes',
            'No',
            this.BindProductItemInfo.bind(
              this,
              newArray,
              objScanItemDetails,
              IcodeDetailes,
            ),
            () => {},
          );
        } else {
          AlertMessage(`Invalid Stock [${get_StockQty}]`);
        }
      } else {
        this.BindProductItemInfo(newArray, objScanItemDetails, IcodeDetailes);
      }
    } else {
      this.BindProductItemInfo(newArray, objScanItemDetails, IcodeDetailes);
    }
    // let isAllowScan = true;
    // if (isAllowScan) {
    //   if (IcodeDetailes) {
    //     if (!IcodeDetailes.Individual) {
    //       IcodeDetailes = this.ApplyITemINformationBasedOnSCanType(
    //         IcodeDetailes,
    //         parseFloat(IcodeDetailes['BARUNIT']),
    //       );
    //     } else if (IcodeDetailes.Individual) {
    //       objScanItemDetails = this.ApplyITemINformationBasedOnSCanType(
    //         objScanItemDetails,
    //         parseFloat(0),
    //       );
    //       newArray.push(objScanItemDetails);
    //     }
    //   } else {
    //     objScanItemDetails = this.ApplyITemINformationBasedOnSCanType(
    //       objScanItemDetails,
    //       parseFloat(0),
    //     );
    //     newArray.push(objScanItemDetails);
    //   }
    // }

    // this.setState({
    //   OnCallList: null,
    //   detaildata: newArray,
    //   ValidateIcode: true,
    //   isEnterSalesManCode: false,
    //   openScanner: false,
    //   DubBarcode: false,
    //   scanText: '',
    // });
  };

  GetPOSItemDetail = async ScandedCode => {
    if (isnull(ScandedCode, '') != '') {
      Request.get(
        `${this.IP}${this.APILink.API_POSMobileScan}ScanBarcode=${
          ScandedCode ? ScandedCode : this.state.scanText
        }&Terminal=${
          this.SessionData.DeviceName
        }&currentStkpt=0&uomid=0&_WSPRate=0&ScanType=${
          this.state.isTaken ? this.state.MemoType : 'RM'
        }&AllowReturnNonSaleItem=${this.state.isAllowReturnNonSaleItem}`,
        this.Token,
      )
        .then(res => {
          if (res.status === 200) {
            let Check = res.data;
            if (Check.length > 1) {
              this.setState(PrevState => ({
                APIData: {...PrevState.APIData, IcodeDetails: Check},
              }));
              this.setState({DubBarcode: true});
            } else if (Check.length) {
              if (this.state.isMandatoryScanSalesManAfterBarcode) {
                this.setState({OnCallList: Check, isEnterSalesManCode: true});
              } else {
                this.BindProductListData(Check[0]);
              }
            } else {
              AlertMessage('Invalid Barcode');
              this.setState({ValidateIcode: false, isEnterSalesManCode: false});
            }
          } else {
            let show_Mesage = isnull(res.data.Message, 'Invalid Barcode');
            AlertMessage(show_Mesage);
            this.setState({ValidateIcode: false, isEnterSalesManCode: false});
          }
        })
        .catch(err => AlertError(err));
    } else {
      AlertMessage('Enter Barcode');
    }
    this.BarcodeFlatList.scrollToEnd();
    this.ScanInputText.focus();
  };

  HandalSerialNo = index => {
    const newItems = [...this.state.detaildata]; // clone the array
    newItems[index]['serialno'] = index + 1;
    newItems[index]['baseprice'] = newItems[index]['gross'];
    newItems[index]['basevalue'] =
      newItems[index]['gross'] * newItems[index]['qty'];
    let TotalDiscountPrice =
      newItems[index]['disc_id_Val'] + newItems[index]['disc_Md_Val'];
    // console.log('TotalDiscountPrice', TotalDiscountPrice);
    newItems[index]['discount'] = newItems[index]['qty'] * TotalDiscountPrice;
    // this.setState({detaildata: newItems})
  };

  RepeintGetItemDetails = async item => {
    var ItemDetails = this.state.AllBillItemList;
    var filters = {
      MasterID: item.ID,
    };
    var ItemList = ItemDetails.filter(item => {
      return Object.keys(filters).every(filter => {
        return filters[filter] === item[filter];
      });
    });
    IncodeArray = ItemList.map(function (obj) {
      return obj.icode;
    });
    this.setState({
      B_ItemDetais: ItemList,
      BillItemDetails: true,
      AllBill: false,
    });
    // console.log('ItemList', ItemList);
    const keys_to_keep = ['icode'];

    const GetIcode = array =>
      array.map(o =>
        keys_to_keep.reduce((acc, curr) => {
          acc[curr] = o[curr];
          return acc;
        }, {}),
      );
    // console.log('GetIcode', GetIcode(ItemList));
    let Check = GetIcode(ItemList);
    var finalArray = Check.map(function (obj) {
      return obj.icode;
    });
    // console.log('HISMAN', finalArray);
  };
  RetriveHandaler = async item => {
    const MemberName = item.MemberName;
    const MemoNo = item.MemoNo;
    const MemoDate = item.MemoDate;
    const MobineNo = item.Mobileno;
    // console.log(MobineNo);
    // console.log(MemberName);
    await this.setState({CustomerNumber: MobineNo});
    await this.HandalCus();
    // console.log('ccc: ',`${this.IP}${this.APILink.API_GetHoldDetails}Memono=${MemoNo}&Memodate=${MemoDate}`);
    Request.get(
      `${this.IP}${this.APILink.API_GetHoldDetails}Memono=${MemoNo}&Memodate=${MemoDate}`,
      this.Token,
    )
      .then(res => {
        if (res.status === 200) {
          this.setState({
            detaildata: res.data,
            VRetrive: false,
            HoldMemoNo: MemoNo,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };
  DeleteHandaler = index => {
    Alert.alert(
      'Are you sure you want to delete this item ?',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Delete',
          onPress: () => {
            let updatedCart = this.state.detaildata; /* Clone it first */
            updatedCart.splice(
              index,
              1,
            ); /* Remove item from the cloned cart state */
            this.setState(updatedCart); /* Update the state */
          },
        },
      ],
      {cancelable: false},
    );
  };
  SendDicound = e => {
    // console.log('SSS', e);
    return (discountAmmount = e);
  };

  ShowDiscount = (index, data) => {
    // console.log('ShowDiscount', index);
    // console.log('ShowDiscount data', data);

    // console.log('Chec1', index);
    // console.log('SendDicound', this.SendDicound);
    this.setState({ItemDiscount: true});
    // this.HandleDAChange(discountAmmount,index)
  };

  TagSMcode = (SMcode, index) => {
    const newItems = [...this.state.detaildata];
    const SmList = this.state.SMList;
    // console.log('State', this.state.SMList);
    const SmCodeValue = SMcode;
    function search(nameKey, myArray) {
      for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].SALESMANCODE === nameKey) {
          return myArray[i];
        }
      }
    }
    var resultObject = search(SmCodeValue, SmList);
    const SMName =
      resultObject.FIRSTNAME +
      resultObject.LASTNAME +
      resultObject.MIDDLENAME +
      '-' +
      resultObject.SALESMANCODE;
    newItems[index]['SP_No'] = SmCodeValue;
    newItems[index]['salespersonname'] = SMName;
    this.setState({detaildata: newItems});
  };
  ApplyDiscountCalculation = (objItem, DiscountType, baseprice) => {
    let Qty = parseFloat(objItem['qty']).toFixed(2);
    let Sign_Qty = Math.sign(Qty);
    objItem['disc_id_Val'] =
      DiscountType === 'Percentage'
        ? Qty * ((baseprice * objItem['disc_id_Factor']) / 100)
        : Sign_Qty * objItem['disc_id_Factor'];
    objItem['discount'] = objItem['disc_id_Val'] + objItem['disc_Md_Val'];
    return objItem;
  };
  quantityHandler = (action, index) => {
    const Updatety = (ExistingItems, index, newqty) => {
      // console.log('apply changes');
      //salim
      // const ExistingItems = [...newItems]; // clone the array
      let Qty = parseFloat(newqty).toFixed(2);
      ExistingItems[index]['qty'] = Qty;
      let Sign_Qty = Math.sign(Qty);
      let SalePrice = Math.abs(
        parseFloat(ExistingItems[index]['baseprice']).toFixed(2),
      );
      let DiscountType = ExistingItems[index]['disc_Type'];
      ExistingItems[index]['basevalue'] = SalePrice * Qty;
      ExistingItems[index] = this.ApplyDiscountCalculation(
        ExistingItems[index],
        DiscountType,
        SalePrice,
      );
      ExistingItems[index]['salesprice'] = parseFloat(
        Sign_Qty *
          (Math.abs(ExistingItems[index]['basevalue']) -
            Math.abs(ExistingItems[index]['discount'])),
      ).toFixed(2);
      ExistingItems[index]['scanUnit'] = Qty;
      ExistingItems[index]['scanunitqty'] = Qty;
      ExistingItems[index]['iqty'] = Qty;
      this.setState({detaildata: ExistingItems}); // set new state
    };
    let newItems = [...this.state.detaildata]; // clone the array
    // console.log(' data : ', newItems);
    let currentQty = isnull(newItems[index]['qty'], 0);
    let get_StockQty = isnull(newItems[index]['stock'], 0);
    let get_NewQty = 0;
    // console.log('currentQty : ', currentQty,' action: ', action);
    action === '' ? (newItems[index]['qty'] = 0) : action;

    if (action == 'more') {
      get_NewQty = parseFloat((currentQty * 1).toFixed(0)) + 1;
    } else if (action == 'less') {
      get_NewQty =
        currentQty > 1 ? parseFloat((currentQty * 1).toFixed(0)) - 1 : 1;
    } else if (action) {
      get_NewQty = parseFloat(isnull(isNaN(action) ? 0 : action, 0));
    }
    // console.log('modify qty : ', get_NewQty, ' get_StockQty : ', get_StockQty);
    if (
      action != 'less' &&
      get_NewQty > get_StockQty &&
      (this.state.NegatveQtyAlertType == 'Restrict' ||
        this.state.NegatveQtyAlertType == 'Message')
    ) {
      if (this.state.NegatveQtyAlertType == 'Message') {
        ConfirmationWithAllInput(
          `Available Stock is [${get_StockQty}]. Any way do you want to continue.`,
          'Yes',
          'No',
          Updatety.bind(this, newItems, index, get_NewQty),
          () => {},
        );
      } else {
        AlertMessage(`Invalid Stock [${get_StockQty}]`);
      }
    } else {
      Updatety(newItems, index, get_NewQty);
    }
  };
  // HandalDiscountChange = (item, index) => {
  //   const newItems = [...this.state.detaildata]; // clone the array
  //   // console.log('item', item);
  //   if (newItems[index]['disc_Type'] === 'Percentage') {
  //     ItemSaveRs = (newItems[index]['MRP'] * item) / 100;
  //     newItems[index]['disc_id_Val'] = ItemSaveRs;
  //     newItems[index]['salesprice'] =
  //       newItems[index]['basevalue'] -
  //       newItems[index]['qty'] *
  //         (newItems[index]['disc_id_Val'] + newItems[index]['disc_Md_Val']);
  //   } else {
  //     newItems[index]['disc_id_Val'] = item;

  //     // console.log('item.newItems[inde', newItems[index]['disc_id_Val']);
  //     newItems[index]['salesprice'] =
  //       newItems[index]['basevalue'] -
  //       newItems[index]['qty'] *
  //         (newItems[index]['disc_id_Val'] + newItems[index]['disc_Md_Val']);
  //     newItems[index]['discount'] = item;
  //   }

  //   this.setState({detaildata: newItems}); // set new state
  // };
  SelectedDiscount = (item, index) => {
    const newItems = [...this.state.detaildata]; // clone the array
    let qty_SingValue = Math.sign(newItems[index]['iqty']);
    let get_DiscountFactor = item.EnterValue;
    newItems[index]['disc_id_Factor'] = get_DiscountFactor;
    newItems[index]['disc_Type'] = item.Type;
    // newItems[index]['disc_AlowChng'] = !(item.AllowChangable === 'Y');
    // console.log('item.Type : ', item.Type);
    // item.AllowChangable === 'Y'
    //   ? (newItems[index]['disc_AlowChng'] = false)
    //   : (newItems[index]['disc_AlowChng'] = true);
    // let L_DiscountValue =
    //   item.Type === 'Percentage'
    //     ? (Math.abs(newItems[index]['baseprice']) * get_DiscountFactor) / 100
    //     : get_DiscountFactor;

    newItems[index] = this.ApplyDiscountCalculation(
      newItems[index],
      item.Type,
      Math.abs(newItems[index]['baseprice']),
    );
    newItems[index]['salesprice'] =
      qty_SingValue *
      (Math.abs(newItems[index]['basevalue']) -
        Math.abs(newItems[index]['discount']));
    this.setState({detaildata: newItems});
  };
  totalDiscount(check) {
    // console.log('check', check);
    return Math.abs(((1 - check / 100) * (1 - 0 / 100) - 1) * 100).toFixed(2);
  }
  // HandleDAChange = async (DA, index) => {
  //   // await
  //   // console.log('input che', DA, index);
  //   // {DA.length>0 ? DA="" :DA}
  //   // {DA === "." ? DA="0." :DA}
  //   const newItems = [...this.state.detaildata]; // clone the array
  //   let baseprice = newItems[index]['baseprice'];
  //   let Qty = newItems[index]['qty'];
  //   let Disc = DA;
  //   newItems[index]['discount'] = DA;
  //   newItems[index]['disc_id_Factor'] = DA;
  //   if (newItems[index]['disc_Type'] === 'Percentage') {
  //     ItemSaveRs = (newItems[index]['baseprice'] * DA) / 100;
  //     newItems[index]['disc_id_Val'] = ItemSaveRs;
  //     newItems[index]['salesprice'] = parseFloat(
  //       newItems[index]['basevalue'] -
  //         newItems[index]['qty'] *
  //           (newItems[index]['disc_id_Val'] + newItems[index]['disc_Md_Val']),
  //     ).toFixed(2);
  //   } else {
  //     newItems[index]['disc_id_Val'] = DA;
  //     // newItems[index]['disc_id_Factor']=
  //     // console.log('item.newItems[inde', newItems[index]['disc_id_Val']);
  //     newItems[index]['salesprice'] = parseFloat(
  //       (baseprice - Disc) * Qty,
  //     ).toFixed(2);
  //     /* newItems[index]['basevalue'] -
  //       newItems[index]['qty'] *
  //   (newItems[index]['disc_id_Val'] + newItems[index]['disc_Md_Val']);*/
  //   }
  //   // console.log(
  //   //   'naaaaaaaadaaaaakuuuudddhhuuuuuuu',
  //   //   'item.newItems[inde',
  //   //   newItems[index]['discount'],
  //   // );

  //   await this.setState({detaildata: newItems});
  // };

  subtotalPrice = () => {
    const SmList = this.state.detaildata;
    // console.log('SmList', SmList, SmList.length);
    if (SmList.length > 0) {
      const getFields = (input, field) => {
        var output = [];
        for (var i = 0; i < input.length; ++i) output.push(input[i][field]);
        return output;
      };
      var result = getFields(SmList, 'salesprice');
      var FinalQty = eval(result.join('+')); // returns [ 1, 3, 5 ]
      return FinalQty;
    } else {
      return 0;
    }
  };

  ClearText = () => {
    this.setState({scanText: ''});
  };

  HandleEnterSalesMan = () => {
    // console.log('sample data : ', this.state.OnCallList);
    let get_ScanData = isnull(this.state.scanText, '').toUpperCase();
    if (get_ScanData.length > 1) {
      let get_scanSalesMAnDetail = this.state.SMList.find(
        man => isnull(man.SALESMANCODE, '').toUpperCase() == get_ScanData,
      );
      // console.log('scan sales man : ', get_scanSalesMAnDetail);
      if (get_scanSalesMAnDetail) {
        this.BindProductListData(this.state.OnCallList[0], get_ScanData);
      } else {
        AlertMessage('Invalid Salesman code.');
      }
      this.ScanInputText.focus();
    } else {
      AlertMessage('Enter Salesman.');
    }
  };

  InfoText = () => {
    if (this.state.scanText.length > 1) {
      var A = this.state.scanText;
      var B = A.toUpperCase();
      this.setState({scanText: B});
      this.GetPOSItemDetail(B);
      this.setState({show: false});
      this.ScanInputText.focus();
    }
  };

  onOpenScanner() {
    var that = this;
    //To Start Scanning
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'iCube App Camera Permission',
              message: 'iCube App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({qrvalue: '', openScanner: true});
            // that.setState({ openScanner: true });
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      that.setState({qrvalue: ''});
      that.setState({openScanner: true});
    }
  }

  SetDetailes = item => {
    if (this.state.isMandatoryScanSalesManAfterBarcode) {
      this.setState({
        OnCallList: [{...item}],
        isEnterSalesManCode: true,
        DubBarcode: false,
        scanText: '',
      });
    } else {
      this.BindProductListData(item);
    }
  };
  handleScroll = event => {
    const positionX = event.nativeEvent.contentOffset.x;
    const positionY = event.nativeEvent.contentOffset.y;
    // console.log(positionX, positionY);
  };
  onBarcodeScan(qrvalue) {
    // console.log('qrvalue : ', qrvalue);
    this.setState(
      {Barcode: qrvalue, openScanner: false},
      this.GetPOSItemDetail.bind(this, qrvalue),
    );
    this.BarcodeFlatList.scrollToEnd();
    this.ScanInputText.focus();
  }
  onCAncelBillBasedOnType = (MemoNo, MemoDate, index) => {
    ConfirmationWithAllInput(
      `Are you sure want to delete [${MemoNo}] ?`,
      'Yes',
      'No',
      this.onDeleteSelectedInvoice.bind(this, MemoNo, MemoDate, index),
      () => {},
    );
  };
  onDeleteSelectedInvoice = (MemoNo, MemoDate, index) => {
    Request.get(
      `${this.IP}/api/POS/CancelMemoBasedOnType?MemoType=${this.state.MemoType}&MemoNo=${MemoNo}&MemoDate=${MemoDate}&Terminal=${this.SessionData.DeviceName}&User=${this.SessionData.UserName}&Remarks=MobileAppCancel&Loccode=${this.SessionData.Loccode}`,
      this.Token,
    )
      .then(res => {
        // console.log('cancel : ', JSON.stringify(res));
        if (res.status === 200) {
          let GetresData = res.data;
          if (GetresData && GetresData.isCanceled) {
            AlertMessage(`${MemoNo} delete successfully`);
            let getPOList = [...this.state.AllBillList];
            getPOList.splice(index, 1); // 2nd parameter means remove one item only
            this.setState(prevstate => ({
              ...prevstate,
              AllBillList: getPOList,
            }));
          } else {
            AlertMessage(`Failed to delete. \n${isnull(GetresData.message)}`);
          }
        } else {
          AlertMessage(`Failed to delete. \n${isnull(res?.data?.message)}`);
        }
      })
      .catch(err => AlertError(err));
  };

  render() {
    const fetchBarcodeData = barcode => {
      this.setState({openScanner: false});
    };
    const Scanner = ({onCancel, onReadCode}) => {
      const handleBarcodeRead = event => {
        onReadCode(event.nativeEvent.codeStringValue);
      };
      return (
        <View style={styles.modalContainer}>
          <CameraScreen
            actions={{leftButtonText: 'Cancel'}}
            onBottomButtonPressed={() => {
              this.setState({openScanner: false});
              // this.isShowModal('Scanner', !this.state.openScanner);
            }}
            showFrame={true}
            // style={{height: '100%'}}
            scanBarcode={true}
            // laserColor={'white'}
            // frameColor={'lightgreen'}
            colorForScannerFrame={'black'}
            onReadCode={event => {
              Vibration.vibrate();
              this.onBarcodeScan(event.nativeEvent.codeStringValue);
            }}
          />
        </View>
      );
    };
    let isShowSalesEnterManCode = false;
    if (
      this.state.isMandatoryScanSalesManAfterBarcode &&
      this.state.isEnterSalesManCode &&
      this.state.OnCallList
    ) {
      isShowSalesEnterManCode = true;
    }
    // console.log('isShowSalesEnterManCode : ', isShowSalesEnterManCode);
    return (
      <>
        <LayoutWrapper backgroundColor={globalColorObject.Color.Lightprimary}>
          <View
            style={[
              styles.SearhBarWrapper,
              {
                width: '99%',
                flexDirection: 'row',
                marginLeft: '1%',
              },
            ]}>
            <TextInput
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  this.state.isTaken
                    ? globalColorObject.Color.oppPrimary
                    : 'red',
                  globalColorObject.ColorPropetyType.BackgroundColor,
                ),
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sl,
                  this.state.isTaken
                    ? globalColorObject.Color.BlackColor
                    : 'white',
                  globalColorObject.ColorPropetyType.Color,
                ),
                ApplyStyleColor(
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.BorderColor,
                ),
                this.state.ValidateIcode === true
                  ? [
                      layoutStyle.FieldInput,
                      //{marginTop: 0, paddingBottom: 8, fontSize: 18},
                    ]
                  : [
                      layoutStyle.FieldInput,
                      //{marginTop: 0, paddingBottom: 8, fontSize: 18},
                    ],
                styles.SearchInput,
              ]}
              ref={ref => {
                this.ScanInputText = ref;
              }}
              onChangeText={scanText => this.setState({scanText})}
              value={this.state.scanText}
              autoCapitalize="characters"
              returnKeyType="next"
              autoCorrect={false}
              onBlur={
                isShowSalesEnterManCode
                  ? this.HandleEnterSalesMan
                  : this.InfoText
              }
              onFocus={this.ClearText}
              placeholder={`Scan ${
                isShowSalesEnterManCode ? 'Sales Man' : 'Barcode'
              } Here`}
              placeholderTextColor="#AFAFAF"
            />
            <TouchableOpacity onPress={() => this.onOpenScanner()}>
              <Image
                style={styles.ScanBarcode}
                source={require('../../../assets/images/ScanBarcode.png')}
              />
            </TouchableOpacity>
            <Modal animationType="slide" visible={this.state.openScanner}>
              <Scanner
                onCancel={() => {
                  this.setState({openScanner: false});
                }}
                onReadCode={fetchBarcodeData}
              />
            </Modal>
          </View>

          <FlatList
            ref={ref => {
              this.BarcodeFlatList = ref;
            }}
            data={this.state.detaildata}
            ItemSeparatorComponent={() => <Separator />}
            keyExtractor={(item, index) => index + '_' + item.icode}
            renderItem={({item, index}) => (
              <ListItemMain
                SMList={this.state.SMList}
                DiscList={this.state.ItemDiountList}
                // HandleDiscountChange={this.HandleDAChange}
                HandalItemDiscount={this.SelectedDiscount}
                // HandalSerialNo={this.HandalSerialNo}
                quantityHandler={this.quantityHandler}
                TagSMcode={this.TagSMcode}
                DiscountList={this.ShowDiscount}
                onLongPress={this.DeleteHandaler}
                data={item}
                index={index}
                NegatveQtyAlertType={this.state.NegatveQtyAlertType}
              />
            )}
          />

          <FloatingAction
            actions={this.ActionButtons()}
            onPressItem={this.HandleActionButtonClick}
            color={globalColorObject.Color.Primary}
            iconWidth={25}
            iconHeight={25}
            animated={true}
            position='left'
            distanceToEdge={{vertical: 110, horizontal: 15}}
          />
          {this.state.CustomerName ? (
            <View style={styles.SearhBarWrapper}>
              <Text
                style={[
                  layoutStyle.FieldInput,
                  {fontSize: 18, marginLeft: 4, marginBottom: -14},
                ]}>
                {this.state.CustomerName}
              </Text>
              <TouchableOpacity onPress={this.CustomerNav}>
                <Image
                  style={[styles.ConfigLogo]}
                  source={require('../../../assets/images/correct.png')}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.SearhBarWrapper}>
              <TouchableOpacity
                style={{
                  backgroundColor: globalColorObject.Color.Primary,
                  justifyContent: 'center',
                }}
                onPress={this.CustomerNav}>
                <Image
                  style={styles.ConfigLogo}
                  source={require('../../../assets/images/popup-blue1.png')}
                />
              </TouchableOpacity>
              <TextInput
                placeholder="Customer Number"
                style={
                  this.state.CustomerValidation === false
                    ? [
                        layoutStyle.FieldInput,
                        {
                          marginTop: 0,
                          paddingBottom: 4,
                          fontSize: 18,
                          marginLeft: 6,
                        },
                      ]
                    : [
                        layoutStyle.FieldInput,
                        {
                          marginTop: 0,
                          paddingBottom: 4,
                          fontSize: 18,
                          marginLeft: 6,
                          marginRight: 2,
                          borderColor: globalColorObject.Color.Primary,
                          borderWidth: 1,
                          borderRadius: 5,
                          borderBottomWidth: 1,
                          borderBottomColor: globalColorObject.Color.Primary,
                        },
                      ]
                }
                value={this.state.CustomerNumber}
                onChangeText={CustomerNumber => this.setState({CustomerNumber})}
                onBlur={this.HandalCus}
                returnKeyType="next"
                autoCorrect={false}
                autoCapitalize="none"
              />

              <TouchableOpacity
                style={{
                  backgroundColor: globalColorObject.Color.Primary,
                  justifyContent: 'center',
                }}
                onPress={this.OpenNewCustomer}>
                <Image
                  style={styles.ConfigLogo}
                  source={require('../../../assets/images/AddIma.png')}
                />
              </TouchableOpacity>
            </View>
          )}
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                //borderWidth: 1,
                width: '50%',
                backgroundColor: globalColorObject.Color.Primary,
                height: 50,
              }}
              onPress={() => {
                let get_ItemDetails = this.state.detaildata || [];
                if (
                  this.state.isTagCustomerMandatory &&
                  isnull(this.state.CustomerNumber, '') == ''
                ) {
                  AlertMessage('Tag customer to pay.');
                } else if (
                  get_ItemDetails.length == 0 &&
                  this.state.MemoType == 'DS'
                ) {
                  AlertMessage('No item available to save.');
                } else {
                  let filter_UntagSAlesMan = [];
                  if (this.state.isTagSalesManMandatory) {
                    filter_UntagSAlesMan =
                      get_ItemDetails.filter(
                        item => isnull(item.SP_No, '') == '',
                      ) || [];
                    // console.log('tag sales man : ', filter_UntagSAlesMan);
                  }
                  if (
                    this.state.isTagSalesManMandatory &&
                    filter_UntagSAlesMan.length > 0
                  ) {
                    AlertMessage("Some item's did not tagged with salesman.");
                  } else {
                    this.props.navigation.navigate('PaymentNav', {
                      TotalCash: this.subtotalPrice(),
                      ItemDetailes: this.state.detaildata,
                      CustomerDetailes: this.state.SelectedCustomer,
                      IsHold: this.state.IsHold,
                      HoldMemoNo: this.state.HoldMemoNo,
                      isDeliverySlip: this.state.MemoType == 'DS',
                      PrintType: this.state.PrintType,
                      Loccode: this.SessionData.Loccode,
                      PrinterName: this.state.PrinterName,
                      OnCallShareFile: this.CallShareFile,
                    });
                  }
                }
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 4,
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                {this.state.MemoType == 'DS' ? 'Save' : 'Pay'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: '50%',
                backgroundColor: globalColorObject.Color.Primary,
                height: 50,
                borderLeftWidth: 1,
                borderColor: 'black',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 4,
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                ₹{this.subtotalPrice()}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.DubBarcode}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
              }}>
              <SafeAreaView style={{height: '100%', width: '100%'}}>
                <View>
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      backgroundColor: globalColorObject.Color.Lightprimary,
                    }}>
                    <View
                      style={[
                        styles.StickeyHeaderCard,
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <View style={{flexDirection: 'row'}}>
                        <View
                          style={[styles.CardTextContainer, {width: '60%'}]}>
                          <Text
                            numberOfLines={1}
                            style={[
                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Bold,
                                globalFontObject.Size.small.sxl,
                                globalColorObject.Color.oppPrimary,
                                globalColorObject.ColorPropetyType.Color,
                              ),
                            ]}>
                            {' '}
                            Barcode
                          </Text>
                        </View>
                        <View
                          style={[styles.CardTextContainer, {width: '40%'}]}>
                          <Text
                            numberOfLines={1}
                            style={[
                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Bold,
                                globalFontObject.Size.small.sxl,
                                globalColorObject.Color.oppPrimary,
                                globalColorObject.ColorPropetyType.Color,
                              ),
                            ]}>
                            Icode
                          </Text>
                        </View>
                      </View>
                      <View style={[styles.CardTextContainer, {width: '100%'}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          {' '}
                          Description
                        </Text>
                      </View>
                    </View>
                    {this.state.APIData.IcodeDetails.map((item, index) => (
                      <View
                        key={index}
                        style={{
                          backgroundColor: 'white',
                          margin: 2,
                          borderRadius: 5,
                          padding: 5,
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.SetDetailes(item);
                          }}>
                          <View style={{flexDirection: 'row'}}>
                            <Text
                              style={[
                                styles.CardTextContainer,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                                {width: '60%'},
                              ]}>
                              {'  '}
                              {item.barcode}
                            </Text>
                            <Text
                              style={[
                                styles.CardTextContainer,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                                {width: '40%'},
                              ]}>
                              {'  '}
                              {item.icode}
                            </Text>
                          </View>
                          <Text
                            style={[
                              styles.CardTextContainer,
                              ApplyStyleFontAndSizeAndColor(
                                globalFontObject.Font.Regular,
                                globalFontObject.Size.small.sl,
                                globalColorObject.Color.BlackColor,
                                globalColorObject.ColorPropetyType.Color,
                              ),
                              {width: '100%'},
                            ]}>
                            {'  '}
                            {item.itemname}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                  </View>
                </View>
              </SafeAreaView>
            </Modal>
          </View>
          {/* New Customer*/}
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.NewCustomer}
            onRequestClose={() => {
              this.setState({NewCustomer: false});
            }}>
            <SafeAreaView
              style={{
                width: '100%',
                height: '100%',
                backgroundColor: globalColorObject.Color.oppPrimary,
              }}>
              <ScrollView style={{flex: 1}}>
                <View style={styles.Header}>
                  <TouchableOpacity style={styles.closeBottom}>
                    <Text
                      style={[
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxxl,
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                        {textAlign: 'center'},
                      ]}>
                      Customer Details
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.setState({NewCustomer: false})}
                    style={styles.closeBottom1}>
                    <Image
                      style={{width: 40, height: 40}}
                      fadeDuration={300}
                      source={require('../../../assets/images/Cancel.png')}
                    />
                  </TouchableOpacity>
                </View>

                <View style={[layoutStyle.ScrollContentWrapper]}>
                  <ModalSearchablePicker
                    placeholder="Card type"
                    data={this.state.CardTypeList}
                    labelProp="CardName"
                    valueProp="CardTypeCode"
                    selectedValue={this.state.NewCustomerDetails.CardTypeCode}
                    onValueSelected={async cardtypecode => {
                      let getmodifyMemberCode = await this.GetMemberNo(
                        cardtypecode,
                      );
                      // console.log('getmodifyMemberCode : ', getmodifyMemberCode);
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          CardTypeCode: cardtypecode,
                          MemberNo: isnull(getmodifyMemberCode.MemberNo, ''),
                          CardNo: isnull(getmodifyMemberCode.CardNo, ''),
                        },
                      }));
                    }}
                  />
                  <RichTextInput
                    placeholder="Member No"
                    value={this.state.NewCustomerDetails.MemberNo}
                    onChangeText={MemberNo =>
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          MemberNo: isnull(MemberNo, ''),
                        },
                      }))
                    }
                    inputProps={{
                      onSubmitEditing: () => {
                        Keyboard.dismiss();
                      },
                      editable: this.state.NewCustomerDetails.AllowMemberNoEdit,
                    }}
                  />
                  <RichTextInput
                    placeholder="Name"
                    value={this.state.NewCustomerDetails.Name}
                    onChangeText={Name =>
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          Name: isnull(Name, ''),
                        },
                      }))
                    }
                    inputProps={{
                      onSubmitEditing: () => {
                        Keyboard.dismiss();
                      },
                    }}
                  />
                  <RichTextInput
                    placeholder="Mobile No"
                    value={this.state.NewCustomerDetails.MobileNo}
                    onChangeText={MobileNo =>
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          MobileNo: isnull(MobileNo, ''),
                        },
                      }))
                    }
                    inputProps={{
                      onSubmitEditing: () => {
                        Keyboard.dismiss();
                      },
                      keyboardType: 'phone-pad',
                    }}
                  />
                  <ModalSearchablePicker
                    placeholder="Area"
                    data={this.state.VillageList}
                    labelProp="Village"
                    valueProp="VCID"
                    selectedValue={this.state.NewCustomerDetails.AreaCode}
                    defaultText={this.state.NewCustomerDetails.AreaName}
                    onValueSelected={(value, label) =>
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          AreaCode: isnull(value, 0),
                          AreaName: isnull(label, ''),
                        },
                      }))
                    }
                    allowAddNew={true}
                  />
                  <RichTextInput
                    placeholder="GST No"
                    value={this.state.NewCustomerDetails.GSTNo}
                    onChangeText={GSTNo =>
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomerDetails: {
                          ...prevState.NewCustomerDetails,
                          GSTNo: isnull(GSTNo, ''),
                        },
                      }))
                    }
                    inputProps={{
                      onSubmitEditing: () => {
                        Keyboard.dismiss();
                      },
                    }}
                  />
                </View>

                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                  <TouchableOpacity
                    style={{
                      //borderWidth: 1,
                      width: '50%',
                      backgroundColor: globalColorObject.Color.Primary,
                      height: 50,
                    }}
                    onPress={() => {
                      this.setState(prevState => ({
                        ...prevState,
                        NewCustomer: false,
                      }));
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        marginTop: 4,
                        fontSize: 24,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '50%',
                      backgroundColor: globalColorObject.Color.Primary,
                      height: 50,
                      borderLeftWidth: 1,
                      borderColor: 'black',
                    }}
                    onPress={() => {
                      if (
                        isnull(
                          this.state.NewCustomerDetails.CardTypeCode,
                          '',
                        ) == ''
                      ) {
                        AlertMessage('Select card type');
                      } else if (
                        isnull(this.state.NewCustomerDetails.MemberNo, '') == ''
                      ) {
                        AlertMessage('Enter Member No');
                      } else if (
                        isnull(this.state.NewCustomerDetails.Name, '') == ''
                      ) {
                        AlertMessage('Enter Name');
                      } else if (
                        isnull(this.state.NewCustomerDetails.MobileNo, '') == ''
                      ) {
                        AlertMessage('Enter Mobile No');
                      } else {
                        this.SaveLoyalty();
                      }
                    }}>
                    <Text
                      style={{
                        textAlign: 'center',
                        marginTop: 4,
                        fontSize: 24,
                        fontWeight: 'bold',
                        color: 'white',
                      }}>
                      Save
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </SafeAreaView>
          </Modal>

          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.CustomerFilter}
            onRequestClose={() => {
              this.setState({CustomerFilter: false});
            }}>
            <SafeAreaView
              style={{
                width: '100%',
                height: '100%',
                backgroundColor: globalColorObject.Color.oppPrimary,
              }}>
              <View style={styles.Header}>
                {/* <TouchableWithoutFeedback
	 onPress={()=>this.setState({ CustomerFilter:false })}>
	 <View>
	 <Text style={[styles.HeaderIcon]}> &#8249;</Text>
	 </View>
</TouchableWithoutFeedback>
              <View style={styles.HeaderTitleWrapper}>
                <Text style={styles.HeaderTitle}>Customer Detailes</Text>
              </View> */}
                <TouchableOpacity
                  // onPress={() => this.setState({CustomerFilter: false})}
                  style={styles.closeBottom}>
                  <Text
                    style={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.oppPrimary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {textAlign: 'center'},
                    ]}>
                    Customer Details
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({CustomerFilter: false})}
                  style={styles.closeBottom1}>
                  {/* <Text style={[ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),{textAlign:'center'},]}> Close
                  </Text> */}
                  <Image
                    style={{width: 40, height: 40}}
                    fadeDuration={300}
                    source={require('../../../assets/images/Cancel.png')}
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  backgroundColor: globalColorObject.Color.oppPrimary,
                  padding: 2,
                  margin: 2,
                }}>
                <TextInput
                  style={[
                    styles.SearchInput,
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Regular,
                      globalFontObject.Size.sxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.BackgroundColor,
                    ),
                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BorderColor,
                    ),
                  ]}
                  placeholder={'Search here'}
                  onChangeText={Customer => this.SearchAndSetCustomer(Customer)}
                />
              </View>
              <FlatList
                data={this.state.ListSourceData}
                ListHeaderComponent={() => <ListItemHeader />}
                renderItem={({item, index}) => (
                  <ListItem
                    data={item}
                    onLongPress={this.SetCustomerData}
                    index={index}
                  />
                )}
                onScroll={event => {
                  this.ApiHit(event);
                }}
                keyExtractor={item =>
                  item.Memberno.toString() +
                  '_' +
                  item.CardCode +
                  '_' +
                  item.LocCode
                }
                stickyHeaderIndices={[0]}
                refreshing={false}
              />
              {/* <FloatingAction
			actions={this.CustomerActionButtons}
			onPressItem={this.HandleCustomerActionButtonClick}
			color="#093d62"
			iconWidth={25}
			iconHeight={25}
			animated={true}
			distanceToEdge={{vertical: 35, horizontal: 25}}
		/> */}
            </SafeAreaView>
          </Modal>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.VRetrive}
              onRequestClose={() => {
                this.setState({VRetrive: false});
              }}>
              <SafeAreaView
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: globalColorObject.Color.oppPrimary,
                }}>
                <View style={styles.Header}>
                  {/* <TouchableWithoutFeedback
                  onPress={() => this.setState({VRetrive: false})}>
                  <Text style={[styles.HeaderIcon]}> &#8249;</Text>
                </TouchableWithoutFeedback>
                <View style={styles.HeaderTitleWrapper}>
                  <Text style={styles.HeaderTitle}>Retrive Detailes</Text>
                </View> */}
                  <TouchableOpacity
                    // onPress={() => this.setState({VRetrive: false})}
                    style={styles.closeBottom}>
                    <Text
                      style={[
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxxl,
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                        {textAlign: 'center'},
                      ]}>
                      Retrive Detailes
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.setState({VRetrive: false})}
                    style={styles.closeBottom1}>
                    {/* <Text style={[ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),{textAlign:'center'},]}> Close
                  </Text> */}
                    <Image
                      style={{width: 40, height: 40}}
                      fadeDuration={300}
                      source={require('../../../assets/images/Cancel.png')}
                    />
                  </TouchableOpacity>
                </View>

                <View style={{padding: 2, margin: 2}}>
                  <TextInput
                    style={[
                      styles.SearchInput,
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Regular,
                        globalFontObject.Size.small.sl,
                        globalColorObject.Color.oppPrimary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      ),
                      ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BorderColor,
                      ),
                      {width: '100%'},
                    ]}
                    placeholder={'Search here'}
                    onChangeText={this.SearchRetrive}
                    onFocus={this.ClearSearch}
                    value={this.state.SearchText}
                    autoFocus={true}
                  />
                </View>
                <View
                  style={[
                    styles.StickeyHeaderCard,

                    ApplyStyleColor(
                      globalColorObject.Color.Primary,
                      globalColorObject.ColorPropetyType.BackgroundColor,
                    ),
                  ]}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={[styles.CardTextContainer, {width: 160}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.CardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        MemberName
                      </Text>
                    </View>
                    <View style={[styles.CardTextContainer, {width: 160}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.CardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        MemoDate
                      </Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={[styles.CardTextContainer, {width: 160}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.CardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        MemoNo
                      </Text>
                    </View>
                    <View style={[styles.CardTextContainer, {width: 80}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.CardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        Net
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={[styles.CardTextContainer, {width: 80}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.CardTextFieldHeader,
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          {' '}
                          Qty
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={[styles.CardTextContainer, {width: 160}]}>
                      <Text
                        numberOfLines={1}
                        style={[
                          styles.CardTextFieldHeader,
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        Mobileno
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={[styles.CardTextContainer, {width: 160}]}>
                        <Text
                          numberOfLines={1}
                          style={[
                            styles.CardTextFieldHeader,
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          Time Terminal
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <ScrollView onScroll={this.handleScroll}>
                  {this.state.RetriveList.map((item, index) => (
                    <TouchableWithoutFeedback
                      onPress={() => this.RetriveHandaler(item)}>
                      <View style={styles.Card}>
                        <View style={styles.CardLine}>
                          <View
                            style={[styles.CardTextContainer, {width: 160}]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.MemberName}
                            </Text>
                          </View>
                          <View
                            style={[styles.CardTextContainer, {width: 160}]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.MemoDate}
                            </Text>
                          </View>
                          <View
                            style={[styles.CardTextContainer, {width: 160}]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.MemoNo}
                            </Text>
                          </View>
                          <View style={[styles.CardTextContainer, {width: 80}]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.Net}
                            </Text>
                          </View>
                          <View style={[styles.CardTextContainer, {width: 80}]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.QTY}
                            </Text>
                          </View>
                          <View
                            style={[
                              styles.CardTextContainer,
                              {
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              },
                            ]}>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                                {width: 160},
                              ]}>
                              {item.Mobileno}
                            </Text>
                            <Text
                              numberOfLines={1}
                              style={[
                                styles.CardTextField,
                                ApplyStyleFontAndSizeAndColor(
                                  globalFontObject.Font.Regular,
                                  globalFontObject.Size.small.sl,
                                  globalColorObject.Color.BlackColor,
                                  globalColorObject.ColorPropetyType.Color,
                                ),
                              ]}>
                              {item.Time} {item.Terminal}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  ))}
                </ScrollView>
              </SafeAreaView>
            </Modal>

            <Modal
              backgroundColor={globalColorObject.Color.Lightprimary}
              animationType="slide"
              transparent={true}
              visible={this.state.AllBill}
              onRequestClose={() => {
                this.setState({AllBill: false});
              }}>
              <SafeAreaView
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: globalColorObject.Color.Lightprimary,
                }}>
                <View style={styles.Header}>
                  <View style={styles.closeBottom}>
                    <Text
                      style={[
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.small.sxxl,
                          globalColorObject.Color.oppPrimary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                      ]}>
                      {'  '} Bills
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.setState({AllBill: false})}
                    style={styles.closeBottom1}>
                    <Image
                      style={{width: 40, height: 40}}
                      fadeDuration={300}
                      source={require('../../../assets/images/Cancel.png')}
                    />
                  </TouchableOpacity>
                </View>
                <FlatList
                  style={[
                    ApplyStyleColor(
                      globalColorObject.Color.Lightprimary,
                      globalColorObject.ColorPropetyType.BackgroundColor,
                    ),
                  ]}
                  data={this.state.AllBillList}
                  renderItem={({item, index}) => (
                    <BillListItem
                      data={item}
                      index={index}
                      onDeleteInvoice={this.onCAncelBillBasedOnType}
                      onPdfClick={this.CallShareFile}
                      AllowedRePrintCount={this.state.AllowedRePrintCount}
                      isShowDelete={
                        this.state.MemoType == 'DS'
                          ? this.state.isAllowCancelDS
                          : this.state.isAllowCancelCM
                      }
                    />
                  )}
                  keyExtractor={item => item.ID.toString()}
                  refreshing={false}
                  // to have some breathing space on bottom
                  ListFooterComponent={() => (
                    <View style={{width: '100%', marginTop: 85}} />
                  )}
                />
              </SafeAreaView>
            </Modal>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.BillItemDetails}
              onRequestClose={() => {
                this.setState({BillItemDetails: false});
              }}>
              <SafeAreaView
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: 'white',
                }}>
                <View style={styles.Header}>
                  <TouchableWithoutFeedback
                    onPress={() => this.setState({BillItemDetails: false})}>
                    <Text style={[styles.HeaderIcon]}> &#8249;</Text>
                  </TouchableWithoutFeedback>
                  <View style={styles.HeaderTitleWrapper}>
                    <Text style={styles.HeaderTitle}>Bill Item Details</Text>
                  </View>
                </View>

                <View
                  style={{backgroundColor: '#ededed', padding: 2, margin: 2}}>
                  <TextInput
                    style={styles.SearchInput}
                    placeholder={'Search here'}
                    onChangeText={this.SearchBillItem}
                    onFocus={this.ClearSearch}
                    value={this.state.SearchText}
                    autoFocus={true}
                  />
                </View>
                {/* <View style={styles.StickeyHeaderCard}>
			<View style={{flexDirection:'row'}}>
				<View style={[styles.CardTextContainer, { width: 160 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}>BasicValue</Text>
				</View>
				<View style={[styles.CardTextContainer, { width: 160 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}>Discount</Text>
				</View>
			</View>
			<View style={{flexDirection:'row'}}>
				<View style={[styles.CardTextContainer, { width: 160 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}>ItemName</Text>
				</View>
				<View style={[styles.CardTextContainer, { width: 80 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}>MasterID</Text>
				</View>
			<View style={{flexDirection:'row'}}>
				<View style={[styles.CardTextContainer, { width: 80 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}> Icode</Text>
				</View>
			</View>
			</View>
			<View style={{flexDirection:'row'}}>
				<View style={[styles.CardTextContainer, { width: 160 }]}>
					<Text numberOfLines={1} style={[styles.CardTextFieldHeader]}>SalesPrice</Text>
				</View>

			</View>
			</View> */}
                <ScrollView onScroll={this.handleScroll}>
                  {this.state.B_ItemDetais.map((item, index) => (
                    <View
                      style={{
                        margin: 2,
                        borderWidth: 0.5,
                        padding: 2,
                        backgroundColor: 'white',
                        borderRadius: 5,
                        marginVertical: 5,
                        marginHorizontal: 9,
                        elevation: 3,
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{fontWeight: 'bold', width: '80%'}}>
                          {item.icode} - {item.ItemName}{' '}
                        </Text>
                        <Text
                          style={{fontSize: 18, textAlign: 'right', flex: 1}}>
                          ₹{item.SalesPrice}{' '}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          borderTopWidth: 0.5,
                          padding: 2,
                          borderBottomWidth: 0,
                        }}>
                        <View style={{borderRightWidth: 0.5, width: '60%'}}>
                          <Text
                            style={{
                              fontSize: 12,
                              textAlign: 'center',
                              fontWeight: 'bold',
                              padding: 2,
                            }}>
                            Expiry Date Time
                          </Text>
                        </View>

                        <View style={{borderRightWidth: 0.5, padding: 2}}>
                          <Text
                            style={{
                              fontSize: 12,
                              textAlign: 'center',
                              fontWeight: 'bold',
                            }}>
                            {' '}
                            Basic Value
                          </Text>
                          <Text style={{textAlign: 'center'}}>
                            ₹{item.BasicValue}
                          </Text>
                        </View>

                        <View style={{padding: 2, paddingLeft: 10}}>
                          <Text
                            style={{
                              fontSize: 12,
                              textAlign: 'right',
                              fontWeight: 'bold',
                            }}>
                            Discount
                          </Text>
                          {/*<Text>₹{item.Discount}</Text>*/}
                        </View>
                      </View>
                    </View>
                  ))}
                  {/* {this.state.B_ItemDetais.map((item, index) => (
			 <View style={styles.Card}>
				<View style={styles.CardLine}>
				  <View style={[styles.CardTextContainer, {width: 160}]}>
					 <Text numberOfLines={1} style={[styles.CardTextField, styles.BoldText]}>
						 {item.BasicValue}
					 </Text>
				  </View>
				  <View style={[styles.CardTextContainer, {width: 160}]}>
					 <Text numberOfLines={1} style={styles.CardTextField}>
					 	{item.Discount}
					 </Text>
				  </View>
				  <View style={[styles.CardTextContainer, {width: 160}]}>
					 <Text numberOfLines={1} style={[styles.CardTextField, styles.BoldText]}>
						{item.ItemName}
					 </Text>
				  </View>
				  <View style={[styles.CardTextContainer, {width: 80}]}>
					 <Text numberOfLines={1} style={styles.CardTextField}>
					 	{item.MasterID}
					 </Text>
				  </View>
				  <View style={[styles.CardTextContainer, {width: 80}]}>
					 <Text numberOfLines={1} style={styles.CardTextField}>
						{item.icode}
					 </Text>
				  </View>
				  <View
					 style={[styles.CardTextContainer,{flexDirection: 'row', justifyContent: 'space-between'}]}>
					 <Text numberOfLines={1} style={[styles.CardTextField, styles.BoldText ,{width: 160}]}>
						{item.SalesPrice}
					</Text>

				  </View>
				</View>
			 </View>
    ))}   */}
                </ScrollView>
              </SafeAreaView>
            </Modal>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.ItemDiscount}
              onRequestClose={() => {
                this.setState({ItemDiscount: false});
              }}>
              <SafeAreaView
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: 'white',
                }}>
                <View style={styles.Header}>
                  <TouchableWithoutFeedback
                    onPress={() => this.setState({ItemDiscount: false})}>
                    <Text style={[styles.HeaderIcon]}> &#8249;</Text>
                  </TouchableWithoutFeedback>
                  <View style={styles.HeaderTitleWrapper}>
                    <Text style={styles.HeaderTitle}>Item Discount</Text>
                  </View>
                </View>

                <ScrollView onScroll={this.handleScroll}>
                  {this.state.VerifyReturnList.map((item, index) => (
                    <View
                      style={{
                        margin: 2,
                        borderWidth: 0.5,
                        padding: 2,
                        backgroundColor: 'white',
                        borderRadius: 5,
                        marginVertical: 5,
                        marginHorizontal: 9,
                        elevation: 3,
                      }}>
                      <TouchableWithoutFeedback
                        onPress={() => this.GetDiscountType(item)}>
                        {/* {"BARCODE": "", "BARCODE2": "", "BARCODE3": "", "BARCODE4": "", "BARCODE5": "",
	"BARCODELIST": "", "ICODE": "X30",
	"ITEM_NAME": "Mac-Socks", "LOCCODE": "1", "MRP": 217.5,
	"QTY": 10, "STKPTCODE": "1", "STKPTNAME": "SHOWROOM", "ScanBarcode": "",
	"ScanQty": 1, "Serialno": 0, "UNITNAME": "PCS", "UOMID": 1} */}
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            borderTopWidth: 0.5,
                            padding: 2,
                            borderBottomWidth: 0,
                          }}>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              BARCODE 1-5
                            </Text>
                            <Text>{item.BARCODE}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              BARCODELIST
                            </Text>
                            <Text>₹{item.BARCODELIST}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              MRP
                            </Text>
                            <Text>₹{item.MRP}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              ICODE
                            </Text>
                            <Text>₹{item.ICODE}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              LOCCODE
                            </Text>
                            <Text>₹{item.LOCCODE}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              ITEM_NAME
                            </Text>
                            <Text>₹{item.ITEM_NAME}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              QTY
                            </Text>
                            <Text>₹{item.QTY}</Text>
                          </View>{' '}
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              STKPTCODE
                            </Text>
                            <Text>₹{item.STKPTCODE}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              STKPTNAME
                            </Text>
                            <Text>₹{item.STKPTNAME}</Text>
                          </View>{' '}
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              STKPTCODE
                            </Text>
                            <Text>₹{item.STKPTCODE}</Text>
                          </View>
                          <View style={{padding: 2, paddingLeft: 10}}>
                            <Text
                              style={{
                                fontSize: 12,
                                textAlign: 'right',
                                fontWeight: 'bold',
                              }}>
                              ScanQty
                            </Text>
                            <Text>₹{item.ScanQty}</Text>
                          </View>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                  ))}
                </ScrollView>
              </SafeAreaView>
            </Modal>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.isShowTeller}
              onRequestClose={() => {
                this.setState({ItemDiscount: false});
              }}>
              <TellerSummary
                reStartApp={this.props.route.params.reStartApp}
                CloseScreen={() => this.setState({isShowTeller: false})}
              />
            </Modal>
          </View>
        </LayoutWrapper>
      </>
    );
  }
}

const styles = StyleSheet.create({
  Header: {
    height: 55,
    alignItems: 'center',
    backgroundColor: globalColorObject.Color.Primary,
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
  },
  closeBottom: {
    width: '35%',
    borderRadius: 5,
    //borderWidth: 0.3,
    left: 3,
    // borderColor:globalColorObject.Color.Lightprimary,
  },
  closeBottom1: {
    //width: '15%',
    //borderRadius: 5,
    //borderWidth: 0.3,
    right: 5,
    // borderColor:globalColorObject.Color.Lightprimary,
  },
  SaveBottom1: {
    width: '35%',
    borderRadius: 25,
    //borderWidth: 1,
    //borderColor:globalColorObject.Color.Lightprimary,
    backgroundColor: globalColorObject.Color.Primary,
    position: 'absolute',
    bottom: 0,
    margin: 5,
    padding: 5,
  },
  HeaderTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  HeaderTitle: {
    color: 'white',
    fontFamily: 'Dosis',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  modalContainer: {
    flex: 1,
  },
  HeaderIcon: {
    color: 'white',
    fontFamily: 'Dosis',
    fontSize: 50,
    fontWeight: 'bold',
    justifyContent: 'space-around',
    paddingBottom: 10,
  },
  CardTextField: {
    fontSize: 16,
    color: '#2f2f2f',
  },
  AddNewInvoice: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 30,
    right: 30,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  MiniInputField: {
    height: 40,
    borderWidth: 0.2,
    borderColor: 'gray',
    width: 40,
    borderRadius: 5,
    fontSize: 12,
    textAlign: 'center',
  },
  ActiveTab: {
    height: 40,
    width: 35,
    backgroundColor: '#98FB98',
    borderRadius: 5,
    color: 'black',
    textAlign: 'center',
    paddingTop: 10,
  },
  InActiveTab: {
    height: 40,
    width: 35,
    backgroundColor: '#f4f4f4',
    borderRadius: 5,
    color: 'red',
    textAlign: 'center',
    paddingTop: 10,
  },
  ConfigLogo: {
    marginTop: 5,
    width: 30,
    height: 30,
    alignSelf: 'flex-end',
  },
  SearhBarWrapper: {
    flexDirection: 'row',
    paddingVertical: 4,
    alignItems: 'center',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  CardTextFieldHeader: {
    fontSize: 16,
    color: 'white',
    paddingLeft: 10,
  },
  CardTextFieldHeader1: {
    fontSize: 16,
    color: 'black',
    paddingLeft: 10,
  },
  StickeyHeaderCard: {
    backgroundColor: '#393D41',
    margin: 10,
    marginHorizontal: 9,
    marginBottom: 5,
    borderRadius: 5,
    paddingVertical: 7,
    elevation: 5,
  },
  SearchInput: {
    //backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    //fontSize: 15,
    borderWidth: 1,
    borderBottomWidth: 1,
    borderBottomColor: globalColorObject.Color.Primary,
  },
  ScanBarcode: {
    top: 5,
    left: 1,
  },
  BillCard: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 9,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'white',
    borderRadius: 5,
    paddingTop: 15,
    paddingBottom: 7,
    marginVertical: 5,
    marginHorizontal: 9,
    elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  ActionImage: {height: 18, width: 18, resizeMode: 'contain'},
});
