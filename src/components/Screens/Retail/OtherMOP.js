import React, {Component, useState} from 'react';
import {BlurView} from '@react-native-community/blur';
import {
  Modal,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import RichTextInput from '../../Custom/RichTextInput';
import {AlertMessage, isnull} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';

const OtherMOP = ({
  MOPList,
  isShowMOP,
  ChangeMOPValue,
  ApplyOtherMOP,
  CloseOtherMOP,
}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isShowMOP}
      style={{marginTop: 50, paddingTop: 100}}
      onRequestClose={() => {}}>
      <BlurView style={styles.blurredBackdrop} blurType="dark" />
      <View
        style={{
          flex: 1,
          marginTop: '30%',
          marginHorizontal: 5,
          paddingHorizontal: 20,
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          paddingTop: 20,
          backgroundColor: globalColorObject.Color.oppPrimary,
        }}>
        <FlatList
          data={MOPList}
          renderItem={({item, index}) => {
            return (
              <View key={item.ID} style={{width: '100%', flexDirection: 'row'}}>
                <Text style={{width: '50%', alignSelf: 'flex-end'}}>
                  {item.mop}
                </Text>
                <RichTextInput
                  isShowAnimateText={false}
                  placeholder={''}
                  value={isnull(item.Value, 0)}
                  onChangeText={Value => ChangeMOPValue(index, Value)}
                  wrapperStyle={[{width: '50%', borderBottomWidth: 1}]}
                  inputProps={{
                    keyboardType: 'phone-pad',
                    returnKeyType: 'next',
                    textAlign: 'center',
                    autoCorrect: false,
                    autoCapitalize: 'none',
                    placeholderTextColor: globalColorObject.Color.BlackColor,
                  }}
                />
              </View>
            );
          }}
          keyExtractor={item => item.ID}
          // to have some breathing space on bottom
          ListFooterComponent={<View style={{width: '100%', marginTop: 15}} />}
        />
        <View
          style={[
            styles.CardTextContainer,
            {
              width: '100%',
              // marginLeft: 5,
              // marginRight: 5,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: globalColorObject.Color.Primary,
            },
          ]}>
          <TouchableOpacity
            style={{
              width: '50%',
              height: 35,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignItems: 'center',
              borderRadius: 10,
              elevation: 5,
              backgroundColor: globalColorObject.Color.Primary,
            }}
            onPress={() => ApplyOtherMOP(MOPList)}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                {textAlign: 'center'},
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              Ok
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              paddingLeft: 10,
              width: '48%',
              height: 35,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignItems: 'center',
              borderRadius: 10,
              elevation: 5,
              backgroundColor: globalColorObject.Color.Primary,
            }}
            onPress={() => CloseOtherMOP()}>
            <Text
              numberOfLines={1}
              style={[
                styles.cardTextFieldHeader,
                {textAlign: 'center'},
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.small.sxxl,
                  globalColorObject.Color.oppPrimary,
                  globalColorObject.ColorPropetyType.Color,
                ),
              ]}>
              Cancel
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  CustomLabel: {
    borderBottomWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    backgroundColor: globalColorObject.Color.oppPrimary,
    padding: 8,
  },
  blurredBackdrop: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  BillCard: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 9,
  },
  ActiveTab1: {
    width: '50%',
  },
  InActiveTab1: {
    width: '50%',
  },
  ActiveTab: {
    borderWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    padding: 10,
    //textAlign: 'center',
    marginBottom: 0,
    width: '50%',
    backgroundColor: globalColorObject.Color.oppPrimary,
    color: 'black',
  },
  InActiveTab: {
    borderWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    padding: 10,
    marginBottom: 0,
    width: '50%',
    backgroundColor: globalColorObject.Color.Primary,
    color: 'white',
    fontWeight: 'bold',
  },
  floatingActionButton1: {
    display: 'none',
  },
  floatingActionButton: {
    position: 'absolute',
    borderRadius: 10,
    width: '100%',
    backgroundColor: globalColorObject.Color.oppPrimary,
  },
  SaveBottom: {
    width: '98%',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: globalColorObject.Color.Primary,
  },
  ConfigLogo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 35,
    marginRight: 2,
    backgroundColor: globalColorObject.Color.oppPrimary,
    borderRadius: 22,
    borderColor: globalColorObject.Color.Lightprimary,
    borderWidth: 0.2,
    margin: 5,
  },
  ClearModalButton: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 60,
    height: 35,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: globalColorObject.Color.Lightprimary,
    marginTop: 3,
    backgroundColor: globalColorObject.Color.Primary,
  },
  clearButtonText: {
    color: globalColorObject.Color.oppPrimary,
  },
  ConfigLogo2: {
    width: 30,
    height: 30,
    backgroundColor: globalColorObject.Color.oppPrimary,
    borderRadius: 22,
    borderColor: globalColorObject.Color.Lightprimary,
    borderWidth: 0.2,
    margin: 5,
    position: 'absolute',
    right: 5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  MiniInputField: {
    height: 40,
    borderWidth: 0.2,
    borderColor: 'gray',
    width: 50,
    borderRadius: 5,
    fontSize: 12,
  },
  item: {
    paddingLeft: 25,
    paddingVertical: 15,
  },
  title: {
    fontSize: 16,
  },
});

export default OtherMOP;
