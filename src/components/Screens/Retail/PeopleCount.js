// Modules Imports
import React, { Component } from 'react';
import {
	Alert,
	SafeAreaView,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	View,
	Text,
	Image,
	StatusBar,
} from 'react-native';
import {
	globalColorObject,
	ApplyStyleFontAndSizeAndColor,
	ApplyStyleFontAndSize,
	ApplyStyleColor,
	globalFontObject,
  } from '../../../Style/GlobalStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FloatingAction } from 'react-native-floating-action';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
// Local Imports
import {AlertMessage,Request,DeviceMacAddress} from '../../../Helpers/HelperMethods';

let ImageDimension = 80;

export default class PeopleCount extends Component {
	constructor() {
		super();
		this.slabs = [];
		this.IP = '';
		this.state = {
			PeopleCategories: [],
			isFetched: false,
			PeopleCategoriesCount: {},
			PeopleCategoriesResetedCount: {},
			LastClickCategoryID: null,
			LastClickSlabID: null,
			LastClickDate: null,
		};
	}

	componentDidMount() {
		this.LoadInitialData();
	}

	LoadInitialData = async () => {
		this.IP = await AsyncStorage.getItem("IP");
		this.Token = await AsyncStorage.getItem("Token");
		// getting slab information
		Request.get(`${this.IP}/api/FootFall/GetAllPeopleCountSlab`,this.Token)
			.then(res => {
				// Preparing slab information
				this.PrepareSlabs(res.data)
					.then(() => {
						// getting Categories information
						Request.get(`${this.IP}/api/FootFall/GetAllPeopleCountCategory`,this.Token)
							.then(res => {
								// preparing categories information
								this.PrepareCategories(res.data);
							})
							.catch(err => {
								// console.log("Fetching Categories from Offline data");
								// this.CheckOfflineCategories();
								AlertMessage("Error while getting Categories \n\n" + JSON.stringify(err));
							});
					});
			})
			.catch(err => {
				// console.log("Fetching Slabs from Offline data");
				// this.CheckOfflineData();
				AlertMessage("Error while getting slab data \n\n" + JSON.stringify(err));
			});
	}

	CheckOfflineData = async () => {
		let SlabData = await AsyncStorage.getItem("Slabs");
		if (SlabData) {
			this.slabs = JSON.parse(SlabData);
			this.CheckOfflineCategories();
		} else {
			console.error(err);
			AlertMessage("Error while getting slab data \n\n" + JSON.stringify(err));
		}
	}

	CheckOfflineCategories = async () => {
		let CategoryData = await AsyncStorage.getItem("Categories");
		if (CategoryData)
			this.PrepareCategories(JSON.parse(CategoryData));
		else {
			console.error(err);
			AlertMessage("Error while getting Categories \n\n" + JSON.stringify(err));
		}
	}

	PrepareSlabs = async (SlabData) => {
		this.slabs = SlabData;
		await AsyncStorage.setItem("Slabs", JSON.stringify(SlabData));
	}

	PrepareCategories = async data => {
		let PeopleCategoriesCountObject = {}, PeopleCategoriesResetedCountObject = {};
		ImageDimension = data.length > 4 ? 80 : 120;

		// Getting Existing clicks from AsyncStorage and generating the object
		let ExistingClicksStr = await AsyncStorage.getItem("TotalClicks");
		let ExistingClicks = JSON.parse(ExistingClicksStr) || {};

		data.map(category => {
			PeopleCategoriesCountObject[category.ID] = ExistingClicks[category.ID] || 0;
			PeopleCategoriesResetedCountObject[category.ID] = 0;
		});

		await AsyncStorage.setItem("Categories", JSON.stringify(data));

		this.setState({
			PeopleCategories: data,
			isFetched: true,
			PeopleCategoriesCount: PeopleCategoriesCountObject,
			PeopleCategoriesResetedCount: PeopleCategoriesResetedCountObject
		});
	}

	NavigateToLoginPage = () => {
		AsyncStorage.multiRemove(["UserCode", "Token", "TotalClicks", "SlabWiseClicks"]);
		this.props.navigation.navigate('LoginPageNav');
	}

	HandlePeopleCategoryClick = async (id) => {
		let { PeopleCategoriesCount } = this.state;
		let UpdatedCountObject = { ...PeopleCategoriesCount };
		UpdatedCountObject[id] += 1;
		// let dt = new Date();
		// let timestamp = `${dt.getFullYear()}-${dt.getMonth()}-${dt.getDate()} ${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`;
		let resultObject = await this.AddClicksInStorage(id);
		if (resultObject) {
			this.setState({
				PeopleCategoriesCount: UpdatedCountObject,
				LastClickCategoryID: id,
				LastClickSlabID: resultObject.CurrentSlabID,
				LastClickDate: resultObject.CurrentDate,
			});
		}
	}

	AddClicksInStorage = async (id) => {
		let ExistingTotalClicks = await AsyncStorage.getItem("TotalClicks");
		let ExistingSlabWiseClicks = await AsyncStorage.getItem("SlabWiseClicks");
		let UpdatedTotalClicks = {};
		let UpdatedSlabWiseClicks = [];

		// SlabWiseClicks Array of Object Prototype
		// [
		// 	{
		// 		date: "2019-02-05",
		// 		slabs: [
		// 			{
		// 				slabID: 1,
		// 				categories: [
		// 					{ catID: 1, clicks: 3 },
		// 					{ catID: 3, clicks: 13 },
		// 					{ catID: 4, clicks: 10 },
		// 				]
		// 			},
		// 			{
		// 				slabID: 2,
		// 				categories: [
		// 					{ catID: 2, clicks: 8 },
		// 					{ catID: 4, clicks: 15 },
		// 					{ catID: 5, clicks: 14 },
		// 				]
		// 			},
		// 		]
		// 	}
		// ]

		// Check date and corrensonding slab then increment the count
		let dt = new Date();
		let CurrentDate = `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}`;
		// console.log("CurrentDate",CurrentDate);
		let currentTime = Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + (("0" + dt.getHours()).slice(-2)) + ":" + (("0" + dt.getMinutes()).slice(-2)) + ":" + (("0" + dt.getSeconds()).slice(-2)));
		// console.log("CurrentTime",Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + (("0" + dt.getHours()).slice(-2)) + ":" + (("0" + dt.getMinutes()).slice(-2)) + ":" + (("0" + dt.getSeconds()).slice(-2))));
		let FromTime, ToTime; //= Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" +);
		// let to = Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" +);
		let CurrentSlabID = undefined;
		let { slabs } = this;

		// Loop Through the slabs and find the correct slab
		for (let i = 0, len = slabs.length; i < len; i++) {
			// console.log("FromTime",dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + slabs[i].From);
			// console.log("ToTime",dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + slabs[i].To);
			FromTime = Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + slabs[i].From);
			ToTime = Date.parse(dt.getFullYear() + "-" + (("0" + (dt.getMonth() + 1)).slice(-2)) + "-" + (("0" + dt.getDate()).slice(-2)) + "T" + slabs[i].To);
			// console.log(FromTime ,ToTime , currentTime);
			// console.log(FromTime <= currentTime && ToTime >= currentTime);
			if (FromTime <= currentTime && ToTime >= currentTime) {
				CurrentSlabID = slabs[i].PSId;
				break;
			}
		}

		// returning the method if there is no slabs found
		if (CurrentSlabID === undefined) {
			AlertMessage("No slabs for this time");
			return undefined;
		};

		// Variables to generate object
		let DateIndex, SlabIndex, CategoryIndex;
		// console.log("ExistingSlabWiseClicks", ExistingSlabWiseClicks);
		if (ExistingSlabWiseClicks) {
			// console.log("Inside ExistingSlabWiseClicks");
			UpdatedSlabWiseClicks = JSON.parse(ExistingSlabWiseClicks);
			// Loop through the entire object to find current date
			for (let i = 0, ilen = UpdatedSlabWiseClicks.length; i < ilen; i++) {
				if (UpdatedSlabWiseClicks[i].date === CurrentDate) {
					// console.log("got current date object");
					// Updating CurrentDate index in the variable
					DateIndex = i;
					let CurrentDateSlabs = UpdatedSlabWiseClicks[i].slabs;
					for (let j = 0, jlen = CurrentDateSlabs.length; j < jlen; j++) {
						if (CurrentDateSlabs[j].slabID === CurrentSlabID) {
							// console.log("got current slab object");
							SlabIndex = j
							let { categories } = CurrentDateSlabs[j];
							for (let k = 0, klen = categories.length; k < klen; k++) {
								if (categories[k].catID === id) {
									// console.log("got current category object");
									CategoryIndex = k;
									categories[k].clicks += 1;
									// console.log("clicks Updated to", categories[k].clicks);
									break;
								}
							}
						}
					}
				}
			}

			if (DateIndex === undefined) {
				// Generate a new Object for the current date
				let Obj = {
					date: CurrentDate,
					slabs: [
						{
							slabID: CurrentSlabID,
							categories: [
								{ catID: id, clicks: 1 },
							]
						},
					]
				};
				UpdatedSlabWiseClicks.push(Obj);
			} else if (SlabIndex === undefined) {
				// Generate a new Object for the current slab
				let Obj = {
					slabID: CurrentSlabID,
					categories: [
						{ catID: id, clicks: 1 },
					]
				};
				UpdatedSlabWiseClicks[DateIndex].slabs.push(Obj);
			} else if (CategoryIndex === undefined) {
				// Generate a new Object for the current Category
				UpdatedSlabWiseClicks[DateIndex].slabs[SlabIndex].categories.push({ catID: id, clicks: 1 });
			}

		} else {
			UpdatedSlabWiseClicks = [{
				date: CurrentDate,
				slabs: [
					{
						slabID: CurrentSlabID,
						categories: [
							{ catID: id, clicks: 1 },
						]
					},
				]
			}];
		}

		if (ExistingTotalClicks) {
			UpdatedTotalClicks = JSON.parse(ExistingTotalClicks);
			if (UpdatedTotalClicks[id])
				UpdatedTotalClicks[id] += 1;
			else
				UpdatedTotalClicks[id] = 1;
		}
		else
			UpdatedTotalClicks[id] = 1;

		// console.log(UpdatedTotalClicks);
		// console.log(UpdatedSlabWiseClicks);
		await AsyncStorage.setItem("SlabWiseClicks", JSON.stringify(UpdatedSlabWiseClicks));
		await AsyncStorage.setItem("TotalClicks", JSON.stringify(UpdatedTotalClicks));
		return {
			CurrentDate: CurrentDate,
			CurrentSlabID: CurrentSlabID,
		};
	}

	PostPeopleCount = async () => {

		let SlabWiseClicksStr = await AsyncStorage.getItem("SlabWiseClicks");
		let UserCode = await AsyncStorage.getItem("UserCode");
		let SlabWiseClicks = JSON.parse(SlabWiseClicksStr) || {};
		let DeviceUniqueID = DeviceMacAddress();

		// Adding DeviceID and User in the PostObject
		for (let i = 0, len = SlabWiseClicks.length; i < len; i++) {
			SlabWiseClicks[i]["user"] = UserCode;
			SlabWiseClicks[i]["deviceID"] = DeviceUniqueID;
		}
		// console.log(SlabWiseClicks);
		// let PostObject = {
		// 	user: 'sa',
		// 	deviceID: "lapsokdaslnzN564dsf4dssd5",
		// 	"category": SlabWiseClicks,
		// }
		// Alert.alert(
		// 	"iCube Alert",
		// 	JSON.stringify(PostObject)
		// );
		Request.post(`${this.IP}/api/FootFall/PostPeopleCounts`, SlabWiseClicks,this.Token)
			.then(res => {
				// console.log(res);
				if (res.status === 200) {
					AlertMessage("Posted Successfully");
					this.ResetPeopleCount();
				} else if (res.status === 500) {
					AlertMessage("Something went wrong , Please Try again.");
				}
			})
			.catch(err => {
				AlertMessage("Could not connect to the server.\nPlease try again");
				console.log(err);
			});
	}

	ResetPeopleCount = () => {
		let { PeopleCategoriesResetedCount } = this.state;
		this.ClearClicksStorage()
			.then(() => {
				this.setState({
					PeopleCategoriesCount: PeopleCategoriesResetedCount,
					LastClickCategoryID: null,
					LastClickSlabID: null,
					LastClickDate: null
				});
			});
	}

	ClearClicksStorage = async () => {
		AsyncStorage.removeItem("SlabWiseClicks");
		AsyncStorage.removeItem("TotalClicks");
	}

	HandleActionButtonClick = name => {
		if (name === "btn_reset")
			this.AlertMessageForActionButton("Are you sure to reset ?", "Reset");
		else if (name === "btn_post")
			this.AlertMessageForActionButton("Are you sure to Post ?", "Post");
		else if (name === "btn_undo")
			this.undoLastClick();
	}

	undoLastClick = async () => {
		let { PeopleCategoriesCount, LastClickCategoryID, LastClickSlabID, LastClickDate } = this.state;
		// Returning if the last click id is null (means no clicks or already done undo)
		if (LastClickCategoryID === null) return;
		// Undoing the last click
		let UpdatedCountObject = { ...PeopleCategoriesCount };
		UpdatedCountObject[LastClickCategoryID] -= 1;

		// Undoing the last click in AsyncStorage
		await this.UndoLastClickInStorage(LastClickDate, LastClickSlabID, LastClickCategoryID)
		this.setState({
			PeopleCategoriesCount: UpdatedCountObject,
			LastClickCategoryID: null,
			LastClickSlabID: null,
			LastClickDate: null
		});
	}

	UndoLastClickInStorage = async (ClickDate, SlabID, CategoryID) => {
		let ExistingSLabWiseClicksStr = await AsyncStorage.getItem("SlabWiseClicks");
		let ExistingTotalClicksStr = await AsyncStorage.getItem("TotalClicks");
		let ExistingTotalClicks = JSON.parse(ExistingTotalClicksStr) || {};
		let ExistingSlabWiseClicks = JSON.parse(ExistingSLabWiseClicksStr) || [];

		ExistingTotalClicks[CategoryID] -= 1;
		for (let i = 0, ilen = ExistingSlabWiseClicks.length; i < ilen; i++) {
			if (ExistingSlabWiseClicks[i].date === ClickDate) {
				let CurrentDateSlabs = ExistingSlabWiseClicks[i].slabs;
				for (let j = 0, jlen = CurrentDateSlabs.length; j < jlen; j++) {
					if (CurrentDateSlabs[j].slabID === SlabID) {
						let { categories } = CurrentDateSlabs[j];
						for (let k = 0, klen = categories.length; k < klen; k++) {
							if (categories[k].catID === CategoryID) {
								categories[k].clicks -= 1;
								// console.log("clicks Updated to", categories[k].clicks);
								break;
							}
						}
					}
				}
			}
		}

		// console.log(ExistingSlabWiseClicks);
		await AsyncStorage.setItem("SlabWiseClicks", JSON.stringify(ExistingSlabWiseClicks));
		await AsyncStorage.setItem("TotalClicks", JSON.stringify(ExistingTotalClicks));
	}
	Logout = async () => {
		await AsyncStorage.multiRemove(["UserCode", "Token", "TotalClicks", "SlabWiseClicks"]);
		this.props.navigation.navigate('LoginPageNav');
	}
	AlertMessageForActionButton = (msg, action) => {
		// Preventing Empty data from posting
		if (action === "Post") {
			let { PeopleCategoriesCount } = this.state, Empty = true;
			let ObjKeys = Object.keys(PeopleCategoriesCount);
			for (let i = 0, len = ObjKeys.length; i < len; i++) {
				if (PeopleCategoriesCount[ObjKeys[i]] !== 0) { Empty = false; break; }
			}
			if (Empty) {
				Alert.alert("iCube Alert", "No data to Post");
				return;
			}
		}
		Alert.alert(
			"iCube Alert",
			msg,
			[
				{ text: "cancel" },
				{
					text: "Ok",
					onPress: () => {
						switch (action) {
							case "Post":
								this.PostPeopleCount();
								break;
							case "Reset":
								this.ResetPeopleCount();
								break;
							case "Logout":
								this.Logout();
								break;
							default:
								break;
						}
					}
				},

			]
		);
	}

	render() {
		// rendering Loader if data 
		let { PeopleCategories, isFetched, PeopleCategoriesCount, LastClickCategoryID } = this.state;
		let ActionButtons = [
			{
				text: 'Post',
				icon: require('../../../assets/images/post-light.png'),
				name: 'btn_post',
				color:globalColorObject.Color.Primary,
				buttonSize: 45,
				position: 1,
			},
			{
				text: 'Reset',
				icon: require('../../../assets/images/reset-light.png'),
				name: 'btn_reset',
				color:globalColorObject.Color.Primary,
				buttonSize: 45,
				position: 2,
			},
		];
		if (LastClickCategoryID !== null) {
			ActionButtons.push({
				text: 'Undo',
				icon: require('../../../assets/images/undo-light.png'),
				name: 'btn_undo',
				color: 'rgba(50,74,84,1)',
				buttonSize: 45,
				position: 3,
			});
		}
		let PeopleCategory;
		if (!isFetched) {
			return (
				<>
				<ICubeAIndicator/>
			</>
			)
		} else {
			if (PeopleCategories) {
				PeopleCategory = PeopleCategories.map(category => {
					return (
						<View style={styles.category} key={category.ID}>
							<View style={styles.CategoryCountView}>
								<Text style={styles.CategoryCount}>{PeopleCategoriesCount[category.ID] || 0}</Text>
							</View>
							<TouchableOpacity
								onPress={this.HandlePeopleCategoryClick.bind(this, category.ID)}>
								<Image
									style={styles.CategoryImage, { width: ImageDimension, height: ImageDimension }}
									source={{ uri: category.ImgURL }} />
							</TouchableOpacity>
							<Text style={styles.CategoryName}>{category.Name}</Text>
						</View>
					);
				})
			} else {
				Alert.alert(
					"iCube Alert",
					"No People Category Exists",
					[
						{ text: "Ok" },
					],
				);
			}
		}
		return (
			<>
				
				<SafeAreaView backgroundColor={globalColorObject.Color.Lightprimary}
				style={styles.container}>
					{/* <Header HeaderData={this.state.HeaderData} /> */}
					<View style={styles.ScrollViewContainer}>
						<ScrollView backgroundColor={globalColorObject.Color.Lightprimary}
						style={styles.ScrollView}>
							<View style={styles.ScrollViewWrapperContainer}>
								<View style={styles.CategoriesContainer}>
									{PeopleCategory}
								</View>
							</View>
						</ScrollView>
					</View>
					<FloatingAction
						actions={ActionButtons}
						onPressItem={this.HandleActionButtonClick}
						color={globalColorObject.Color.Primary} iconWidth={20} iconHeight={20}
					/>
				</SafeAreaView>
			</>
		);
	}
};

const styles = StyleSheet.create({
	baseText: {
		fontFamily: 'roboto',
	},
	loader: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	container: {
		flex: 1,
		justifyContent: 'flex-end',
	},
	ScrollViewContainer: {
		// flex: 1,
	},
	ScrollViewWrapperContainer: {
		flex: 1,
		marginBottom: 80,
	},
	CategoriesContainer: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
	},
	category: {
		justifyContent: 'center',
		margin: 20,
	},
	CategoryImage: {
		marginVertical: 10,
	},
	CategoryName: {
		fontSize: 20,
		textAlign: 'center',
	},
	CategoryCountView: {
		width: 60,
		height: 40,
		backgroundColor: '#607d8a',
		borderRadius: 50,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		marginVertical: 5,
	},
	CategoryCount: {
		textAlign: 'center',
		fontSize: 17,
		color: 'white',
	},
});