import React, {useState, useEffect} from 'react';
import {
  Modal,
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  SafeAreaView,
} from 'react-native';
import RichTextInput from '../../Custom/RichTextInput';
import Ripple from 'react-native-material-ripple';
import {string, func, object, array, oneOfType, bool, number} from 'prop-types';
import {dismissKeyboard} from '../../../Helpers/HelperMethods';
import {
  Request,
  AlertMessage,
  AlertError,
  AlertStatusError,
  isnull,
  nullif_Number,
} from '../../../Helpers/HelperMethods';
import {globalColorObject} from '../../../Style/GlobalStyle';

const MemoLevelDiscount = props => {
  // console.log(props)
  const {data, labelProp, valueProp, selectedValue, defaultText, itemDetail} =
    props;
  const [searchText, setSearchText] = useState('');
  const [showModal, setShowModal] = useState(false);

  const [selectedValueText, setSelectedValueText] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  // Header , Search , Header Item, DiscountList

  useEffect(() => {
    // getting the label value from the selected code value
    const selectedItem = data.find(itm => itm[valueProp] === selectedValue);
    const selectedItemText =
      (selectedItem && selectedItem[labelProp]) || defaultText;
    // console.log('Value',Value)
    setSearchText(selectedItemText);
    setSelectedValueText(selectedItemText);
  }, [selectedValue, defaultText, data, valueProp, labelProp]);

  const openModal = () => {
    setShowModal(true);
    setSearchText(selectedValueText || '');
    let get_Discount = [...data];
    get_Discount.forEach(element => {
      element.EnterValue = element.Factor;
    });
    setSuggestions(get_Discount);
  };
  const closeModal = () => {
    dismissKeyboard();
    setShowModal(false);
    setSuggestions([]);
  };

  const RetriveHandaler = (value, label) => {
    if (parseFloat(isnull(value.EnterValue, '0')) > 0) {
      closeModal();
      props.onValueSelected(value, label);
    } else {
      AlertMessage('Discount should be greater than 0.');
    }
  };

  const ChangeDiscountValue = (value, index) => {
    let get_DisList = [...suggestions];
    get_DisList[index]['EnterValue'] = value;
    setSuggestions(get_DisList);
  };

  return (
    <>
      <Ripple onPress={openModal}>
        <Text
          style={{
            borderBottomWidth: 1,
            borderColor: '#F5FBFB',
            backgroundColor: 'white',
            padding: 8,
          }}>
          Discount
        </Text>
      </Ripple>
      <Modal visible={showModal} animationType="fade">
        <SafeAreaView style={styles.ModalContainer}>
          <View style={styles.Header}>
            <View style={styles.HeaderTitleWrapper}>
              <Text style={styles.HeaderTitle}>Discount</Text>
            </View>
            <TouchableOpacity onPress={closeModal} style={styles.closeBottom1}>
              <Image
                style={{width: 40, height: 40}}
                fadeDuration={300}
                source={require('../../../assets/images/Clear_2.png')}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            keyboardShouldPersistTaps={'always'}
            contentContainerStyle={styles.FlatListStyle}
            data={suggestions}
            renderItem={({item, index}) => (
              // console.log("Itemsss",Object.values(data)),
              <View
                style={{
                  marginTop: 10,
                  margin: 5,
                  borderWidth: 0.5,
                  padding: 4,
                  backgroundColor: 'white',
                  borderRadius: 15,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{width: '85%', flexDirection: 'row'}}>
                    <Text
                      style={{
                        width: '60%',
                        margin: 4,
                        borderWidth: 0.5,
                        fontWeight: 'bold',
                        backgroundColor: '#FAFAD2',
                        padding: 4,
                      }}>
                      {item.DiscountName}
                    </Text>
                    <RichTextInput
                      placeholder=""
                      value={isnull(nullif_Number(item.EnterValue, 0), '')}
                      wrapperStyle={{width: '40%'}}
                      onChangeText={EnterValue =>
                        ChangeDiscountValue(EnterValue, index)
                      }
                      inputProps={{
                        keyboardType: 'phone-pad',
                        editable: item.AllowChangable == 'Y',
                      }}
                    />
                  </View>
                  <Ripple
                    style={{paddingTop: 14}}
                    onPress={() => RetriveHandaler(item, index)}>
                    <Text
                      style={{
                        fontSize: 14,
                        textAlign: 'right',
                        flex: 1,
                        color: globalColorObject.Color.Primary,
                        margin: 5,
                        padding: 4,
                      }}>
                      APPLY
                    </Text>
                  </Ripple>
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        margin: 4,
                        paddingBottom: 10,
                        borderBottomColor: 'gray',
                        borderBottomWidth: 0,
                        fontSize: 16,
                      }}>
                      Get {item.Factor}
                      {item.Type === 'Percentage' ? '%' : '₹'} Discount
                    </Text>
                  </View>
                </View>
              </View>
            )}
          />
        </SafeAreaView>
      </Modal>
    </>
  );
};

MemoLevelDiscount.defaultProps = {
  defaultText: '',
  allowAddNew: false,
  newItemTextAsValue: false,
};

MemoLevelDiscount.propTypes = {
  data: array.isRequired,
  selectedValue: oneOfType([string, number]).isRequired,
  onValueSelected: func.isRequired,
  allowAddNew: bool,
  newItemTextAsValue: bool,
  fieldWrapperStyle: oneOfType([object, array]),
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderRadius: 5,
    margin: 12,
    backgroundColor: 'white',
  },
  item: {
    width: Dimensions.get('window').width * 0.5,
    height: 100,
    borderWidth: 1,
    borderColor: 'lightgray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemIcon: {
    width: 200,
    marginTop: 30,
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 0.5,
  },
  itemTitle: {
    margin: 5,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  CardTextContainer: {
    width: 90,
  },
  CardTextField: {
    fontSize: 16,
    color: globalColorObject.Color.Primary,
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 5,
    paddingTop: 15,
    marginVertical: 5,
    marginHorizontal: 9,
    elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  MiniInputField: {
    height: 40,
    borderWidth: 0.2,
    borderColor: 'gray',
    width: 50,
    borderRadius: 5,
    fontSize: 12,
  },

  ModalContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
  },
  blurredBackdrop: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  Header: {
    height: 55,
    alignItems: 'center',
    backgroundColor: globalColorObject.Color.Primary,
    flexDirection: 'row',
  },
  HeaderTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  HeaderTitle: {
    color: 'white',
    // fontFamily: 'Dosis',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  SearchInput: {
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    fontSize: 15,
  },
  HeaderIcon: {
    color: 'white',
    // fontFamily: 'Dosis',
    fontSize: 50,
    fontWeight: 'bold',
    justifyContent: 'space-around',
    paddingBottom: 10,
  },
  searchWrapper: {
    marginBottom: 6,
    flex: 1,
    width: '100%',
    borderRadius: 5,
    overflow: 'hidden',
  },
  searchFieldContainer: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.07)',
  },

  clearModalButton: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 55,
    height: 32,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: 'rgba(0,0,0,0.09)',
  },
  closeModalButton: {
    width: 35,
    height: 35,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  closeBottom1: {
    //width: '15%',
    //borderRadius: 5,
    //borderWidth: 0.3,
    right: 5,
    // borderColor:globalColorObject.Color.Lightprimary,
  },
  closeModalButtonText: {
    transform: [{scale: 2}],
    color: 'gray',
    lineHeight: 18,
  },
  suggestionContainer: {
    flex: 1,
    width: '100%',
  },
  FlatListStyle: {
    backgroundColor: 'white',
  },

  suggestionItemText: {
    color: '#4d5156',
    padding: 13,
  },
  StickeyHeaderCard: {
    backgroundColor: '#393D41',
    borderRadius: 15,
    margin: 5,
  },

  BoldText: {
    fontWeight: 'bold',
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
  },
});

export default MemoLevelDiscount;
export {styles as MSPStyles};
