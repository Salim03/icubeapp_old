import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import LayoutWrapper from '../../../components/Layout/Layout';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import {
  Request,
  isnull,
  CallBluetoothPrint,
  ReturnDisplayStringForDate,
  SqlDateFormat,
  AlertError,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
} from '../../../Style/GlobalStyle';
import {GroupValuesByKey, GetSessionData} from '../../../Helpers/HelperMethods';

class POSBlutoothPrintList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetched: false,
      forms: {},
      Storage: {
        IP: '',
        Token: '',
        RoleID: '',
        FinancialYearId: 0,
        ProfileId: '',
        Location: 0,
        UserCode: '',
        PrinterName: '',
      },
    };
  }

  InitialSetup = async () => {
    let {
      get_IP,
      get_Token,
      get_RoleId,
      get_Location,
      get_UserCode,
      get_ProfileId,
      get_FinancialYearId,
    } = await GetSessionData();
    let {params} = this.props.route;
    let myobject = {};
    myobject.IP = get_IP;
    myobject.Token = get_Token;
    myobject.RoleID = get_RoleId;
    myobject.Location = get_Location == null ? 0 : parseInt(get_Location);
    myobject.UserCode = get_UserCode;
    myobject.FinancialYearId = get_FinancialYearId;
    myobject.ProfileId = get_ProfileId;
    myobject.PrinterName = isnull(params.PrinterName, '');
    await Request.get(
      `${myobject.IP}/api/POS/GetPOSBluetoothReportList?RoleId=${myobject.RoleID}&EmployeeId=${myobject.UserCode}&Loccode=${myobject.Location}`,
      myobject.Token,
    )
      .then(res => res.data || [])
      .then(data => {
        // console.log('res : ', data);
        const forms = data && GroupValuesByKey(data, 'Group');
        // console.log('res forms : ', forms);
        this.setState(prevstate => ({
          ...prevstate,
          forms,
        }));
      })
      .catch(err => {
        AlertError(err);
      });

    this.setState(prevstate => {
      return {
        ...prevstate,
        isFetched: true,
        Storage: myobject,
      };
    });
  };

  CallPrintBAsedOnSelectedReport = async ReportId => {
    this.setState(prevstate => ({
      ...prevstate,
      isFetched: false,
    }));
    await Request.get(
      `${
        this.state.Storage.IP
      }/api/POS/GetPOSBluetoothSelectedReportPrintData?ReportId=${ReportId}&PassReportDateTime=${ReturnDisplayStringForDate(
        new Date(),
        SqlDateFormat,
      )}
      &RoleId=${this.state.Storage.RoleID}&ProfileId=${
        this.state.Storage.ProfileId
      }&EmployeeId=${this.state.Storage.UserCode}&Loccode=${
        this.state.Storage.Location
      }&FinancialYearId=${this.state.Storage.FinancialYearId}`,
      this.state.Storage.Token,
    )
      .then(res => {
        let printdata = res.data;
        CallBluetoothPrint(
          isnull(this.state.Storage.PrinterName, ''),
          1,
          printdata,
        );
      })
      .catch(err => AlertError(err));
    this.setState(prevstate => ({
      ...prevstate,
      isFetched: true,
    }));
  };

  componentDidMount() {
    this.InitialSetup();
  }

  refreshLayout = () => this.forceUpdate();

  render() {
    let binddata = (
      <View
        style={[
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
          {flex: 1, flexDirection: 'row', justifyContent: 'center'},
        ]}>
        <Text>No Data</Text>
      </View>
    );
    let homedata = Object.keys(this.state.forms || {}).map((tem, i) => {
      return (
        <View
          key={'grp_mod_' + tem}
          style={{
            flexDirection: 'column',
            paddingBottom: 5,
          }}>
          <Text
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.mediam.mm,
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.Color,
              ),
              {
                textAlign: 'left',
                paddingLeft: 5,
                textDecorationLine: 'underline',
              },
            ]}>
            {tem}
          </Text>
          <View
            style={{
              flexWrap: 'wrap',
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              paddingTop: 5,
            }}>
            {this.state.forms[tem].map(head => {
              return (
                <TouchableOpacity
                  key={'inm_' + head.ID}
                  style={styles.FormContainer}
                  onPress={this.CallPrintBAsedOnSelectedReport.bind(
                    this,
                    head.ID,
                  )}>
                  <Image
                    source={{
                      uri: head.ImageURI,
                    }}
                    style={styles.FormImages}
                  />
                  <Text
                    style={[
                      ApplyStyleFontAndSize(
                        globalFontObject.Font.Regular,
                        globalFontObject.Size.small.sl,
                      ),
                      styles.FormLabel,
                    ]}>
                    {head.Name}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      );
    });
    binddata = (
      <ScrollView
        style={[
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
          {
            marginTop: 10,
          },
        ]}>
        {homedata}
      </ScrollView>
    );

    return (
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={this.refreshLayout}>
        <ICubeAIndicator isShow={!this.state.isFetched} />
        {binddata}
      </LayoutWrapper>
    );
  }
}

const styles = StyleSheet.create({
  FormImages: {
    width: 30,
    height: 30,
    margin: 5,
    resizeMode: 'stretch',
  },
  FormLabel: {
    textAlign: 'center',
    // fontWeight: 'bold',
  },
  FormContainer: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
  },
});

export default POSBlutoothPrintList;
