import React, {useState} from 'react';
import {
  Modal,
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  SafeAreaView,
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import {BlurView} from '@react-native-community/blur';
import {func, array} from 'prop-types';
import {
  dismissKeyboard,
  isnull,
  AlertMessage,
} from '../../../Helpers/HelperMethods';
import {layoutStyle} from '../../Layout/Layout';
import {AccordionStyle} from '../../Custom/Accordion';
import {globalColorObject} from '../../../Style/GlobalStyle';
import RichTextInput from '../../Custom/RichTextInput';

const ItemLevelDiscount = props => {
  const {Memo_Dis_data, itemDetail} = props;
  // console.log('itemDetail', itemDetail);
  // const [searchText, setSearchText] = useState('');
  const [showModal, setShowModal] = useState(false);

  // const [selectedValueText, setSelectedValueText] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  // const [HeaderItem, setHeaderItem] = useState([]);
  // Header , Search , Header Item, DiscountList

  const openModal = () => {
    console.log('open mdal');
    setShowModal(true);
    // setSearchText(selectedValueText || '');
    let get_Discount = [...Memo_Dis_data];
    get_Discount.forEach(element => {
      element.EnterValue = element.Factor;
    });
    //console.log('Final Dis Value : ', get_Discount);
    setSuggestions(get_Discount);
  };
  const closeModal = () => {
    console.log('close mdal');
    dismissKeyboard();
    setShowModal(false);
    setSuggestions([]);
  };

  const RetriveHandaler = (item, index) => {
    console.log('ref');
    if (parseFloat(isnull(item.EnterValue, '0')) > 0) {
      closeModal();
      props.onValueSelected(item, index);
    } else {
      AlertMessage('Discount should be greater than 0.');
    }
  };

  const ChangeDiscountValue = (value, index) => {
    console.log('change sdf');
    let get_DisList = [...suggestions];
    get_DisList[index]['EnterValue'] = value;
    setSuggestions(get_DisList);
  };

  return (
    <SafeAreaView>
      <Ripple onPress={() => openModal()}>
        <Text style={[layoutStyle.FieldLabelFont]}>Discount</Text>
      </Ripple>
      <Modal visible={showModal} animationType="fade">
        <SafeAreaView style={styles.ModalContainer}>
          <BlurView
            style={styles.blurredBackdrop}
            blurType="dark"
            blurAmount={7}
          />

          <View style={styles.Header}>
            <View style={styles.HeaderTitleWrapper}>
              <Text style={styles.HeaderTitle}>Discount</Text>
            </View>
            <TouchableOpacity
              onPress={() => closeModal()}
              style={styles.closeBottom1}>
              <Image
                style={{width: 40, height: 40}}
                fadeDuration={300}
                source={require('../../../assets/images/Clear_2.png')}
              />
            </TouchableOpacity>
          </View>
          {/* {console.log('iitteemm', props.data)} */}
          <View style={[AccordionStyle.header]}>
            <Text
              style={[
                AccordionStyle.headerTitle,
                {
                  padding: 5,

                  backgroundColor: 'cyan',
                  color: 'black',
                  width: '100%',
                  elevation: 15,
                  fontWeight: 'bold',
                },
              ]}>
              Item Detail
            </Text>
          </View>
          <View
            style={{
              margin: 2,
              borderWidth: 0.5,
              padding: 2,
              backgroundColor: 'white',
              borderRadius: 5,
              marginVertical: 5,
              marginHorizontal: 9,
              elevation: 3,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontWeight: 'bold', width: '100%'}}>
                {itemDetail.itemname}{' '}
              </Text>
              {/* <Text style={{fontSize: 18, textAlign:'right', flex:1}}>₹{itemDetail.salesprice}	</Text> */}
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                borderTopWidth: 0.5,
                padding: 2,
                borderBottomWidth: 0,
              }}>
              <View style={{borderRightWidth: 0.5, padding: 2, width: '33%'}}>
                <Text
                  style={{
                    fontSize: 12,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}>
                  {' '}
                  MRP
                </Text>
                <Text style={{textAlign: 'center'}}>{itemDetail.MRP}</Text>
              </View>

              <View style={{borderRightWidth: 0.5, padding: 2, width: '33%'}}>
                <Text
                  style={{
                    fontSize: 12,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}>
                  Qty
                </Text>
                <Text style={{textAlign: 'center'}}>
                  {itemDetail.scanunitqty}
                </Text>
              </View>
              <View
                style={{padding: 2, alignContent: 'flex-end', width: '33%'}}>
                <Text
                  style={{
                    fontSize: 12,
                    textAlign: 'right',
                    fontWeight: 'bold',
                  }}>
                  Sales Price
                </Text>
                <Text style={{textAlign: 'right'}}>
                  {itemDetail.salesprice}
                </Text>
              </View>
            </View>
          </View>
          <View style={[AccordionStyle.header]}>
            <Text
              style={[
                AccordionStyle.headerTitle,
                {
                  padding: 5,
                  backgroundColor: 'cyan',
                  color: 'black',
                  width: '100%',
                  elevation: 15,
                  fontWeight: 'bold',
                },
              ]}>
              Discount
            </Text>
          </View>

          <FlatList
            keyboardShouldPersistTaps={'always'}
            contentContainerStyle={styles.FlatListStyle}
            data={suggestions}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              // AllowChangable,Factor
              // console.log('Itemsss', item.AllowChangable),
              <View
                style={{
                  marginTop: 10,
                  margin: 5,
                  borderWidth: 0.5,
                  padding: 4,
                  backgroundColor: 'white',
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{width: '85%', flexDirection: 'row'}}>
                    <Text
                      style={{
                        margin: 4,
                        borderWidth: 0.5,
                        width: '60%',
                        fontWeight: 'bold',
                        backgroundColor: '#FAFAD2',
                        padding: 4,
                      }}
                      returnKeyType="next"
                      autoCorrect={false}
                      editable={false}
                      autoCapitalize="none">
                      {item.DiscountName}
                    </Text>
                    <RichTextInput
                      isShowAnimateText={false}
                      placeholder=""
                      value={isnull(item.EnterValue, 0)}
                      wrapperStyle={{width: '40%'}}
                      onChangeText={EnterValue =>
                        ChangeDiscountValue(EnterValue, index)
                      }
                      inputProps={{
                        keyboardType: 'phone-pad',
                        // editable: item.AllowChangable == 'Y',
                      }}
                    />
                  </View>

                  <Ripple
                    style={{paddingTop: 14}}
                    onPress={() => RetriveHandaler(item, index)}>
                    <Text
                      style={{
                        fontSize: 14,
                        textAlign: 'right',
                        flex: 1,
                        color: globalColorObject.Color.Primary,
                        margin: 5,
                        padding: 4,
                      }}>
                      APPLY
                    </Text>
                  </Ripple>
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        margin: 4,
                        paddingBottom: 10,
                        borderBottomColor: 'gray',
                        borderBottomWidth: 0.5,
                        fontSize: 16,
                      }}>
                      Get {item.Factor}
                      {item.Type === 'Percentage' ? '%' : '₹'} Discount
                    </Text>
                    {/* <Text>Detail infomation about Discount</Text> */}
                  </View>
                </View>
              </View>
            )}
          />
        </SafeAreaView>
      </Modal>
    </SafeAreaView>
  );
};

ItemLevelDiscount.defaultProps = {};

ItemLevelDiscount.propTypes = {
  Memo_Dis_data: array.isRequired,
  onValueSelected: func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderRadius: 5,
    margin: 12,
    backgroundColor: 'white',
  },
  item: {
    width: Dimensions.get('window').width * 0.5,
    height: 100,
    borderWidth: 1,
    borderColor: 'lightgray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemIcon: {
    width: 200,
    marginTop: 30,
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 0.5,
  },
  itemTitle: {
    margin: 5,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  CardTextContainer: {
    width: 90,
  },
  CardTextField: {
    fontSize: 16,
    color: '#2f2f2f',
  },
  Card: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 5,
    paddingTop: 15,
    marginVertical: 5,
    marginHorizontal: 9,
    elevation: 3,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  MiniInputField: {
    height: 40,
    borderWidth: 0.2,
    borderColor: 'gray',
    width: 50,
    borderRadius: 5,
    fontSize: 12,
  },

  ModalContainer: {
    flex: 1,
    width: '100%',
  },
  blurredBackdrop: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  Header: {
    height: 55,
    alignItems: 'center',
    backgroundColor: globalColorObject.Color.Primary,
    flexDirection: 'row',
  },
  HeaderTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  HeaderTitle: {
    color: 'white',
    // fontFamily: 'Dosis',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  SearchInput: {
    backgroundColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 5,
    fontSize: 15,
  },
  HeaderIcon: {
    color: 'white',
    // fontFamily: 'Dosis',
    fontSize: 50,
    fontWeight: 'bold',
    justifyContent: 'space-around',
    paddingBottom: 10,
    marginTop: 5,
  },
  closeBottom1: {
    //width: '15%',
    //borderRadius: 5,
    //borderWidth: 0.3,
    right: 5,
    // borderColor:globalColorObject.Color.Lightprimary,
  },
  searchWrapper: {
    marginBottom: 6,
    flex: 1,
    width: '100%',
    borderRadius: 5,
    overflow: 'hidden',
  },
  searchFieldContainer: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.07)',
  },

  clearModalButton: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 55,
    height: 32,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: 'rgba(0,0,0,0.09)',
  },
  closeModalButton: {
    width: 35,
    height: 35,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  closeModalButtonText: {
    transform: [{scale: 2}],
    color: 'gray',
    lineHeight: 18,
  },
  suggestionContainer: {
    flex: 1,
    width: '100%',
  },
  FlatListStyle: {
    backgroundColor: 'white',
  },

  suggestionItemText: {
    color: '#4d5156',
    padding: 13,
  },
  StickeyHeaderCard: {
    backgroundColor: '#393D41',
    borderRadius: 15,
    margin: 5,
  },

  BoldText: {
    fontWeight: 'bold',
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
  },
});

export default ItemLevelDiscount;
export {styles as MSPStyles};
