import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
  Button,
  SafeAreaView,
  FlatList,
  Modal,
} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';
import RichTextInput from '../../Custom/RichTextInput';
import CheckInput from '../../../components/Custom/CheckInput';
import OtherMOP from './OtherMOP';
import {
  Request,
  AlertMessage,
  DeviceUniqueId,
  MobileDeviceName,
  isnull,
  AlertError,
  AlertStatusError,
  GetSessionData,
  sharefileForCallFRom,
  nullif_Number,
  isBluetoothPrinterConnected,
} from '../../../Helpers/HelperMethods';
import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../../Style/GlobalStyle';
import ICubeAIndicator from '../../Tools/ICubeIndicator';
import MemoLevelDiscount from './MemoDiscount';
import LayoutWrapper, {layoutStyle, CardStyle} from '../../Layout/Layout';

const BillListItem = props => {
  const data = props.data;
  const index = props.index;
  let FullInvoiceDate = new Date(data.ReturnMemoDate);
  let InvoiceDate = `${('0' + FullInvoiceDate.getDate()).slice(-2)}/${(
    '0' +
    (FullInvoiceDate.getMonth() + 1)
  ).slice(-2)}/${FullInvoiceDate.getFullYear()}`;
  return (
    <View
      style={[
        styles.BillCard,
        ApplyStyleColor(
          globalColorObject.Color.oppPrimary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}>
      <View style={[styles.CardLine, {alignItems: 'center'}]}>
        <View style={[styles.CardTextContainer, {width: '60%'}]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}>
            {data.ReturnMemoNo} {InvoiceDate}
          </Text>
        </View>
        <View
          style={[
            styles.CardTextContainer,
            {width: '40%', flexDirection: 'row', alignItems: 'center'},
          ]}>
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Bold,
                globalFontObject.Size.small.sl,
                globalColorObject.Color.BlackColor,
                globalColorObject.ColorPropetyType.Color,
              ),
              {textAlign: 'right', width: '75%'},
            ]}>
            {`${Math.round(data.Return_Value * 100) / 100} [${
              Math.round(data.Qty * 100) / 100
            }]`}
          </Text>
          <CheckInput
            customControl={true}
            wrapperStyle={{width: '25%', alignItems: 'flex-end'}}
            placeholder=""
            value={data.Select}
            onValueChange={select => {
              props.ChangeSelectHandler(select, index);
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default class Payment extends Component {
  constructor() {
    super();
    this.LocalData = {
      SubTotalCash: 0,
      data: [],
      CustomerData: [],
      IsHold: 0,
      HoldMemoNo: '',
      UserName: '',
      Loccode: 0,
      PrinterName: '',
    };
    this.state = {
      ReturnList: [],
      AdjReturnList: '',
      isShowReturn: false,
      isLoading: true,
      totalqty: 0,
      discoutprice: 0,
      AfterDiscount: 0,
      discouttype: '',
      editable: false,
      C_LoctionCode: '',
      C_Memberno: '',
      C_CardCode: '',
      TextInputDisableStatus: true,
      CustomeName: '',
      Tender: 0,
      Cardamount: 0,
      CashValue: 0,
      OnAccount: 0,
      SplAmount: 0,
      IsCredit: 0,
      data: [],
      ChangeValue: 0,
      SummarySalesValue: 0,
      SummaryReturnValue: 0,
      SummaryBaseValue: 0,
      Getcardtype: [],
      SelectedCardDetails: [],
      OtherMOPList: [],
      isShowMOP: false,
      isShowCardDetails: false,
      SplMOPList: [],
      detaildata: [],
      TestData: {Aggno: '', BARUNIT: '1.00'},
      ReturnValue: 0.0,
      RoudOff: 0,
      s_CreditValue: 0,
      ItemDiountList: [],
    };
  }

  ChangeCArdDetailsValue = (index, value, field) => {
    let getmop = [...this.state.Getcardtype];
    getmop[index][field] = value;
    this.setState(prevState => ({...prevState, Getcardtype: getmop}));
  };

  ChangeMOPValue = (index, value) => {
    let getmop = [...this.state.OtherMOPList];
    getmop[index].Value = value;
    this.setState(prevState => ({...prevState, OtherMOPList: getmop}));
  };
  componentDidMount() {
    let params = this.props.route.params;
    this.LocalData.data = params.ItemDetailes || [];
    this.LocalData.data = params.ItemDetailes || [];
    this.LocalData.SubTotalCash = params.TotalCash || 0;
    this.LocalData.CustomerData = params.CustomerDetailes || [];
    this.LocalData.IsHold = params.IsHold || 0;
    this.LocalData.HoldMemoNo = params.HoldMemoNo || '';
    this.LocalData.isDeliverySlip = params.isDeliverySlip || false;
    this.LocalData.PrintType = params.PrintType || 'Preview';
    this.LocalData.PrinterName = params.PrinterName || '';
    this.LocalData.Loccode = params.Loccode || 0;
    // console.log('this.LocalData.CustomerData : ', this.LocalData.CustomerData);
    this.LoadInitialData();
  }

  ValidateSpecialAmount = async (passAmount, get_MOPData) => {
    passAmount = parseFloat(isnull(passAmount, '0'));
    const NetValue = parseFloat(isnull(this.state.AfterDiscount, '0'));
    if (passAmount > NetValue) {
      AlertMessage('MOP value must be less than Net Value');
    } else {
      const Change =
        parseFloat(NetValue) -
        parseFloat(this.state.Cardamount) -
        parseFloat(this.state.OnAccount) -
        passAmount;
      // console.log(' Change : ', Change);
      this.setState({
        Tender: Change > 0 ? Change : 0,
        ChangeValue: Change > 0 ? 0 : Math.abs(Change),
        SplMOPList: get_MOPData,
        SplAmount: passAmount,
        isShowMOP: false,
      });
    }
  };

  ValidateCardDetailsAmount = async (passAmount, get_MOPData) => {
    passAmount = parseFloat(isnull(passAmount, '0'));
    const NetValue = parseFloat(isnull(this.state.AfterDiscount, '0'));
    if (passAmount > NetValue) {
      AlertMessage('MOP value must be less than Net Value');
    } else {
      const Change =
        parseFloat(NetValue) -
        parseFloat(this.state.SplAmount) -
        parseFloat(this.state.OnAccount) -
        passAmount;
      console.log(' Change : ', Change);
      this.setState({
        Tender: Change > 0 ? Change : 0,
        ChangeValue: Change > 0 ? 0 : Math.abs(Change),
        SelectedCardDetails: get_MOPData,
        Cardamount: passAmount,
        isShowCardDetails: false,
      });
    }
  };

  ApplyCard = () => {
    let get_FilterData = (this.state.Getcardtype || []).filter(
      item => parseFloat(isnull(item.Value, '0')) > 0,
    );
    if (get_FilterData.length > 0) {
      let get_TotalAmount = get_FilterData.reduce((acc, cur) => {
        return acc + parseFloat(isnull(cur.Value, '0'));
      }, 0);
      console.log('get_TotalAmount : ', get_TotalAmount);
      this.ValidateCardDetailsAmount(get_TotalAmount, get_FilterData);
    } else {
      AlertMessage('Enter value to apply.');
    }
  };

  ApplyOtherMOP = MOPData => {
    let get_MOPData = (MOPData || []).filter(
      item => parseFloat(isnull(item.Value, '0')) > 0,
    );
    if (get_MOPData.length > 0) {
      let get_SplAmount = get_MOPData.reduce((acc, cur) => {
        return acc + parseFloat(isnull(cur.Value, '0'));
      }, 0);
      // console.log('get_SplAmount : ', get_SplAmount);
      this.ValidateSpecialAmount(get_SplAmount, get_MOPData);
    } else {
      AlertMessage('Enter value to apply.');
    }
  };

  CloseOtherMOP = () => {
    const NetValue = parseFloat(isnull(this.state.AfterDiscount, '0'));
    const Change =
      parseFloat(NetValue) -
      parseFloat(this.state.Cardamount) -
      parseFloat(this.state.OnAccount);
    this.setState({
      Tender: Change > 0 ? Change : 0,
      ChangeValue: Change > 0 ? 0 : Math.abs(Change),
      SplMOPList: [],
      SplAmount: 0,
      isShowMOP: false,
    });
  };

  ItemDiscount = async () => {
    Request.get(
      `${this.IP}/api/POS/Get_pos_ItemDiscount?disc=5&Profileid=p1&LocCode=1`,
      this.Token,
    )
      .then(res => {
        // console.log('Item Discount', res);
        const ItemDiscount = res.data;
        if (res.status === 200) {
          this.setState({
            ItemDiountList: ItemDiscount,
          });
        } else {
          AlertStatusError(res);
        }
      })
      .catch(err => AlertError(err));
  };

  GetReturnDetails = async () => {
    Request.get(
      `${this.IP}/api/POS/SpMasterDetail_ReturnMemo?Memodate=1900-01-01&Memono=&Returndays=90`,
      this.Token,
    )
      .then(res => {
        this.setState({
          ReturnList: res.data.Table || [],
          AdjReturnList: '',
        });
        // console.log('GetReturnDetails', JSON.stringify(res.data.Table || []));
      })
      .catch(err => {
        AlertError(err);
        this.setState({ReturnList: [], AdjReturnList: ''});
      });
  };

  LoadInitialData = async () => {
    let {get_IP, get_Token, get_UserName} = await GetSessionData();
    this.IP = get_IP;
    this.Token = get_Token;
    this.LocalData.UserName = get_UserName;
    this.GetExtraMOP();
    if (!this.LocalData.isDeliverySlip) {
      this.GetCardD();
      this.ItemDiscount();
      this.AssiginData();
      this.GetReturnDetails();
    } else {
      await this.AssiginData();
    }
  };

  GetCardD = async () => {
    Request.get(`${this.IP}api/POS/GetTablecardtype`, this.Token).then(res => {
      // console.log('GetTablecardtype : ', res.data);
      this.setState({Getcardtype: res.data});
    });
  };

  GetExtraMOP = async () => {
    Request.get(
      `${this.IP}api/POS/POSMOP?isDeliveryslip=${
        this.LocalData.isDeliverySlip ? '1' : '0'
      }`,
      this.Token,
    ).then(res => {
      let getdate = (res.data || []).filter(item => item.mopid > 7);
      // console.log('getdate : ', getdate);
      this.setState({OtherMOPList: getdate});
    });
  };

  ChangeSelectHandler = (Value, index) => {
    let newItems = [...this.state.ReturnList]; // clone the array
    newItems[index]['Select'] = Value;
    this.setState({ReturnList: newItems});
  };

  isOnlyReturn = () => {
    let get_ScanData = this.LocalData.data || [];
    let filteronlyReturn = get_ScanData.filter(item => item.qty < 0);
    if (
      filteronlyReturn.length == get_ScanData.length &&
      get_ScanData.length > 0
    ) {
      return true;
    } else {
      return false;
    }
  };

  RoundOfValue = passvalue => {
    passvalue = parseFloat(Math.round(passvalue) - passvalue);
    passvalue = Math.round(passvalue * 100) / 100;
    return passvalue;
  };

  AssiginData = () => {
    const SmList = this.LocalData.data;
    const getFields = (input, field, FilterType) => {
      var output = [];
      for (var i = 0; i < input.length; ++i) {
        // if (field == 'qty' && input[i]['qty'] > 0) {
        //   output.push(input[i][field]);
        // } else if (field == 'takenbasevalue') {
        //   if (input[i]['qty'] > 0) {
        //     output.push(Math.abs(input[i]['basevalue']));
        //   }
        // } else if (field == 'returnbasevalue') {
        //   if (input[i]['qty'] < 0) {
        //     output.push(Math.abs(input[i]['basevalue']));
        //   }
        // } else if (field != 'qty') {
        //   output.push(input[i][field]);
        // }
        if (
          (FilterType == 'Taken' && input[i]['qty'] > 0) ||
          (FilterType == 'Return' && input[i]['qty'] < 0)
        ) {
          output.push(Math.abs(input[i][field]));
        } else if (FilterType == 'All') {
          output.push(input[i][field]);
        }
      }
      return output;
    };
    var result = getFields(SmList, 'qty', 'Taken');
    var FinalQty = isnull(eval(result.join('+')), 0); // returns [ 1, 3, 5 ]
    var SummaryBaseValue = getFields(SmList, 'basevalue', 'Taken');
    var FinalBaseValue = isnull(eval(SummaryBaseValue.join('+')), 0);
    SummaryBaseValue = getFields(SmList, 'salesprice', 'Taken');
    let FinalSalesValue = isnull(eval(SummaryBaseValue.join('+')), 0);
    SummaryBaseValue = getFields(SmList, 'salesprice', 'Return');
    let ReturnValue = isnull(eval(SummaryBaseValue.join('+')), 0);
    let Memberno = '';
    let LocCode = '0';
    let CardCode = '';
    let CustomeName = '';
    let Basic_NetValue = 0;
    let get_NetValue = this.NumberRoudTwo(FinalSalesValue - ReturnValue);
    let get_RoundOff = this.RoundOfValue(get_NetValue);
    // get_RoundOff = Math.round(get_RoundOff * 100) / 100;
    if (this.LocalData.CustomerData) {
      Memberno = this.LocalData.CustomerData.Memberno;
      LocCode = this.LocalData.CustomerData.LocCode;
      CardCode = this.LocalData.CustomerData.CardCode;
      CustomeName = this.LocalData.CustomerData.Displayname;
    }
    this.setState(
      {
        RoudOff: get_RoundOff,
        AfterDiscount: get_NetValue + get_RoundOff,
        C_Memberno: Memberno,
        C_LoctionCode: LocCode,
        C_CardCode: CardCode,
        CustomeName: CustomeName,
        totalqty: FinalQty,
        SummaryReturnValue: ReturnValue,
        SummarySalesValue: FinalSalesValue,
        SummaryBaseValue: FinalBaseValue,
        Tender: this.LocalData.isDeliverySlip
          ? FinalSalesValue - ReturnValue
          : 0,
        isLoading: this.LocalData.isDeliverySlip,
        ReturnValue: ReturnValue,
      },
      this.LocalData.isDeliverySlip ? this.SaveHandler : () => {},
    );
  };

  SaveHandler = async () => {
    this.setState({isLoading: true});
    let Amountvalidate =
      parseFloat(this.state.Tender) +
      parseFloat(this.state.Cardamount) +
      parseFloat(this.state.SplAmount) +
      parseFloat(this.state.OnAccount);
    // console.log(this.state.RoudOff);
    if (Amountvalidate === 0 && this.state.AfterDiscount > 0) {
      Amountvalidate = this.state.AfterDiscount;
      await this.setState(prevState => ({
        ...prevState,
        Tender: this.state.AfterDiscount,
        ChangeValue: 0,
      }));
    }
    if (this.LocalData.PrintType == 'Print') {
      if (isnull(this.LocalData.PrinterName, '') == '') {
        AlertMessage('Tag bluetooth printer to save.');
        this.setState({isLoading: false});
        return;
      } else if (
        !(await isBluetoothPrinterConnected(this.LocalData.PrinterName))
      ) {
        AlertMessage('Please configure bluetooth printer to save.');
        this.setState({isLoading: false});
        return;
      }
    }
    if (
      parseFloat(isnull(this.state.totalqty, 0)) == 0 &&
      parseFloat(isnull(this.state.SummarySalesValue, 0)) == 0 &&
      parseFloat(isnull(this.state.SummaryReturnValue, 0)) == 0 &&
      parseFloat(isnull(this.state.ReturnValue, 0)) == 0
    ) {
      AlertMessage('No record available to save.');
    } else if (Amountvalidate >= parseFloat(this.state.AfterDiscount)) {
      const Data = this.LocalData.data;
      const changeKeyObjects = (arr, replaceKeys) => {
        return arr.map(item => {
          const newItem = {};
          Object.keys(item).forEach(key => {
            newItem[replaceKeys[key]] = item[[key]];
          });
          return newItem;
        });
      };
      const replaceKeys = {
        Aggno: 'aggno',
        // BARUNIT: "1.00",
        BenifitDiscount: 'benifitDiscount',
        // BlockDiscount: 0,
        CustomerBenifitID: 'customerBenifitID',
        DId: 'did',
        // EffRate: -16234.3125,
        // ExpiryDate: "2020-07-25T00:00:00",
        // Individual: false,
        IsPromoGiftVoucher: 'isPromoGiftVoucher',
        IsPromoPoints: 'isPromoPoints',
        // ItemImage: "",
        ItemPoints: 'itemPoints',
        // LBLess: "0.00",
        LessQty: 'lessQty',
        Loccode: 'loccode',
        MRP: 'mrp',
        // MRPDisc: "0.00",
        POId: 'poId',
        // Pdiscount: "0.00",
        // PriceEdit: false,
        PromoNo: 'promoNo',
        RemoveTax: 'removeTax',
        SP_No: 'spNo',
        scanunitqty: 'scanUnit',
        // StockPoint: "SHOWROOM",
        TaxCode: 'taxCode',
        // UOMID: 1,
        WSPRate: 'isWSP',
        barcode: 'barcode',
        baseprice: 'basicPrice',
        basevalue: 'basicValue',
        // basicPrice: "215.00",
        // basicValue: 215,
        disc_AlowChng: 'itemDiscAc',
        disc_Md_Val: 'memoDiscValue',
        disc_Type: 'itemDiscType',
        disc_Value: 'promoValue',
        disc_id_Factor: 'itemDiscFactor',
        disc_id_Val: 'itemDiscValue',
        disc_id_name: 'itemDiscName',
        disc_md_AC: 'memoDiscAc',
        disc_md_Factor: 'memoDiscFactor',
        disc_md_name: 'memoDiscName',
        disc_md_type: 'memoDiscType',
        discount: 'discount',
        dsdate: 'dsDate',
        dsno: 'dsNo',
        gross: 'gross',
        grpcode: 'grpCode',
        icode: 'icode',
        iqty: 'iQty',
        itemname: 'itemName',
        oemcode: 'oemCode',
        qty: 'qty',
        // port: false,
        salespersonname: 'spName',
        salesprice: 'salesPrice',
        //ScanUnit: "scanunitqty",
        serialno: 'serialNo',
        stkptcode: 'stkptCode',
        // stock: -93,
        // taxname: "",
        taxvalue: 'taxValue',
        unitofmeasure: 'uom',
      };
      const newArray = changeKeyObjects(Data, replaceKeys);
      this.state.detaildata.push(...newArray);
      await this.SaveInvoice();
    } else {
      AlertMessage('Pay Rs ' + this.state.ChangeValue);
    }
    this.setState({isLoading: false});
  };

  SaveInvoice = async () => {
    if (this.state.OnAccount.length > 0) {
      this.setState({IsCredit: 1});
    }
    let get_deviceId = DeviceUniqueId();
    let get_SplAmount = parseFloat(isnull(this.state.SplAmount, '0'));
    let get_CardAmount = parseFloat(isnull(this.state.Cardamount, '0'));
    let get_SplMOPList = this.state.SplMOPList || [];
    let get_CardDetails = this.state.SelectedCardDetails || [];
    if (
      get_SplAmount > 0 &&
      get_SplMOPList.length == 0 &&
      this.state.OtherMOPList.length > 0
    ) {
      get_SplMOPList.push(this.state.OtherMOPList[0]);
      get_SplMOPList[0].Value = get_SplAmount;
    }
    if (
      get_CardAmount > 0 &&
      get_CardDetails.length == 0 &&
      this.state.Getcardtype.length > 0
    ) {
      get_CardDetails.push(this.state.Getcardtype[0]);
      get_CardDetails[0].Value = get_CardAmount;
    }
    let SaveObject = {
      S_AdjustmentRetunIdList: isnull(this.state.AdjReturnList, ''),
      isDeliverySlip: this.LocalData.isDeliverySlip ? 1 : 0,
      detaildata: this.state.detaildata,
      s_BaseValue: this.state.SummaryBaseValue,
      s_Qty: this.state.totalqty,
      s_ItemDisc: 0.0,
      s_MemoDisc: this.state.discoutprice,
      s_SalesValue: this.state.SummarySalesValue, // Sub Total
      s_Roundoff: this.state.RoudOff,
      s_ReturnValue: this.state.ReturnValue,
      s_NetValue: this.state.AfterDiscount,
      s_CashValue:
        this.state.Tender > this.state.AfterDiscount
          ? this.state.AfterDiscount
          : this.state.Tender, // NetValue (Ex :850) Card :300 Cash :1000 (Tender)
      s_CardValue: get_CardAmount,
      s_CardDetail:
        get_CardDetails.length == 1 ? get_CardDetails[0]['CardName'] : '',
      s_CreditValue: this.state.s_CreditValue, // sum cash+ card
      s_CreditDetail: '',
      s_TenderValue: this.state.Tender,
      s_Change: this.state.ChangeValue,
      s_PrintCount: 1,
      s_Remarks: ``,
      s_Terminal: get_deviceId,
      s_User: this.LocalData.UserName,
      s_MemberNo: isnull(this.state.C_Memberno, ''),
      s_Reedem: 0.0,
      s_MemberLocCode: isnull(this.state.C_LoctionCode, ''),
      s_MemberCardType: isnull(this.state.C_CardCode, ''),
      s_CashMemoNo: '',
      s_CashMemoDate: new Date().toLocaleString(),
      s_PromoDisc: 0.0,
      s_UserMemoNo: '',
      s_PayVoucher: 0.0,
      s_TaxValue: 0.0,
      s_ScanUnitQty: this.state.totalqty,
      s_ID: 0,
      s_PromoPoint: 0.0,
      s_AgainstCashMemo: 0,
      s_HoldMemoNo: this.LocalData.HoldMemoNo,
      s_IsHold: 0,
      s_IsDS: this.LocalData.isDeliverySlip ? 1 : 0,
      s_IsCredit: this.state.IsCredit,
      s_IsMoreMOP: this.state.get_SplAmount > 0 ? 1 : 0,
      s_DiscUser: '',
      s_IsPOSOrder: 0,
      s_OrderDueDate: '1900-01-01T00:00:00',
      s_OrderDelivery: false,
      s_GiftVoucher: 0.0,
      s_GiftVoucherReference: '',
      s_BenifitDiscount: 0.0,
      S_TakBenifitDiscount: 0.0,
      S_RetBenifitDiscount: 0.0,
      s_BenifitCode: '',
      s_EarnPoints: 0.0,
      s_RedeemOTP: 0,
      s_AdjCollection: 0.0,
      s_LessQty: 0.0,
      s_ItemPoints: 0.0,
      s_RedeemPoints: 0.0,
      s_PromoGiftVoucher: 0.0,
      s_WSPUser: '',
    };
    // AlertMessage("Saved Successfully")
    let get_Allinfo = {
      ProductAndSummaryData: SaveObject,
      CardDetails: get_CardDetails,
      SpecialMOPData: get_SplMOPList,
    };
    // console.log('Final Save', JSON.stringify(get_Allinfo));
    await Request.post(`${this.IP}api/POS/sp_POSSave`, get_Allinfo, this.Token)
      .then(res => {
        // console.log('res : ', JSON.stringify(res));
        if (res.status && res.status == '200') {
          let get_ReturnData = res.json();
          return get_ReturnData;
        }
      })
      .then(get_ReturnData => {
        if (get_ReturnData) {
          // console.log('Save Res ', get_ReturnData);
          let is_Save = false;
          let ID = 0;
          let MemoNo = '';
          let MemoDate = '1900-01-01';
          if (get_ReturnData?.length > 0) {
            let get_Data = get_ReturnData[0];
            ID = get_Data.ID;
            MemoNo = get_Data.MemoNo;
            MemoDate = get_Data.MemoDate;
            // console.log(
            //   'ID : ',
            //   ID,
            //   ' Memo No : ',
            //   MemoNo,
            //   ' MemoDate : ',
            //   MemoDate,
            // );
            if (ID > 0) {
              is_Save = true;
            }
          }
          // console.log(get_ReturnData);
          if (is_Save) {
            let MemoType = this.isOnlyReturn()
              ? 'RM'
              : this.LocalData.isDeliverySlip
              ? 'DS'
              : 'CM';
            if (
              this.LocalData.PrintType == 'Print' ||
              this.LocalData.PrintType == 'Preview'
            ) {
              this.props.route.params.OnCallShareFile(
                MemoType,
                ID,
                MemoNo,
                MemoDate,
                true,
              );
            } else {
              AlertMessage('Saved Successfully');
            }
            this.props.navigation.navigate('ShopNav', {
              ItemData: this.LocalData.data,
            });
          } else {
            AlertMessage('Failed to save.');
          }
        } else {
          AlertMessage('Failed to save.');
        }
      })
      .catch(err => {
        console.error(err);
        AlertError('' + err);
      });
  };

  Clearpayment = () => {
    this.setState({
      Cardamount: 0,
      SplAmount: 0,
      ChangeValue: 0,
      Tender: 0,
      OnAccount: 0,
      IsCredit: 0,
      s_CreditValue: 0,
    });
  };

  ValidateCash = async Tender => {
    // console.log('CAMount', Tender < 0);
    Tender === NaN ? (Tender = 0) : Tender;
    Tender < 0 ? (Tender = 0) : Tender;
    Tender == '' ? (Tender = 0) : Tender;
    await this.setState({Tender});
    const NetValue = this.state.AfterDiscount;
    const CardValue = this.state.Cardamount;
    const CashValue =
      parseInt(NetValue) -
      parseInt(CardValue) -
      parseFloat(this.state.SplAmount);
    // console.log('Net value From Cashamount', NetValue);
    // console.log('CashValue From Cashamount', CashValue);
    await this.setState({CashValue: CashValue});
    const Change = parseInt(this.state.Tender) - parseInt(CashValue);
    // console.log('Change From Cashamount', Change);
    // console.log('State Cashamount', this.state.Tender);
    await this.setState({ChangeValue: Change});
  };

  ValidateCardAmount = async passAmount => {
    passAmount = parseFloat(isnull(passAmount, '0'));
    const NetValue = parseFloat(isnull(this.state.AfterDiscount, '0'));
    if (passAmount > NetValue) {
      AlertMessage('Card value must be less than Net Value');
    } else {
      const Change =
        parseFloat(NetValue) -
        parseFloat(this.state.SplAmount) -
        parseFloat(this.state.OnAccount) -
        passAmount;
      this.setState({
        Tender: Change > 0 ? Change : 0,
        ChangeValue: Change > 0 ? 0 : Math.abs(Change),
        Cardamount: passAmount,
      });
    }
  };

  ValidateDiscountEdit = async DiscountAmount => {
    await this.setState({discoutprice: DiscountAmount});
  };

  NumberRoudTwo = passvalue => {
    return Math.round(passvalue * 100) / 100;
  };

  DiscountsubtotalPrice = () => {
    let currentsalePrice =
      parseFloat(this.state.SummarySalesValue) -
      parseFloat(this.state.ReturnValue);
    let discountValue = this.ReturnDiscountValue(currentsalePrice);
    let get_NetValue = this.NumberRoudTwo(currentsalePrice - discountValue);
    let get_RoundOff = this.RoundOfValue(get_NetValue);
    this.setState({
      AfterDiscount: get_NetValue + get_RoundOff,
      RoudOff: get_RoundOff,
    });
  };

  ReturnDiscountValue = AfterReturnSAlesValue => {
    let returnDiscountValue = 0;
    if (this.state.discouttype === 'Percentage') {
      let Check =
        (parseInt(this.state.discoutprice) / 100) * AfterReturnSAlesValue;
      returnDiscountValue = Check;
    } else if (this.state.discouttype === 'Amount') {
      returnDiscountValue = this.state.discoutprice;
    }
    return returnDiscountValue;
  };

  HandalItemDiscount = (item, index) => {
    let get_DisFactor = item.EnterValue;
    this.setState(
      {discoutprice: get_DisFactor, discouttype: item.Type},
      this.DiscountsubtotalPrice,
    );
    // console.log('I am In', item, index);
  };
  ValidateOnAccount = async () => {
    const OnAccountData =
      this.state.AfterDiscount -
      this.state.Tender -
      this.state.Cardamount -
      this.state.SplAmount;
    if (this.state.CustomeName == '' || this.state.CustomeName === undefined) {
      AlertMessage('Tag Customer');
    } else if (OnAccountData < 0) {
      AlertMessage('Net Value should be greater than Cash value');
      const change =
        parseInt(this.state.Tender) +
        parseInt(this.state.Cardamount) +
        parseFloat(this.state.SplAmount) -
        parseInt(this.state.AfterDiscount);
      this.setState({ChangeValue: change});
    } else if (OnAccountData > 0) {
      this.setState(prevState => ({
        TextInputDisableStatus: !prevState.TextInputDisableStatus,
      }));
      const change =
        parseInt(this.state.Tender) +
        parseInt(this.state.Cardamount) +
        parseInt(OnAccountData) +
        parseFloat(this.state.SplAmount) -
        parseInt(this.state.AfterDiscount);
      this.setState({IsCredit: 1});
      const S_Value =
        parseInt(this.state.Cardamount) +
        parseInt(this.state.Tender) +
        parseFloat(this.state.SplAmount);
      // console.log(S_Value);
      this.setState({
        OnAccount: OnAccountData,
        ChangeValue: change,
        s_CreditValue: S_Value,
      });
    }
    if (this.state.TextInputDisableStatus === false) {
      const change =
        parseInt(this.state.Tender) +
        parseInt(this.state.Cardamount) +
        parseFloat(this.state.SplAmount) -
        parseInt(this.state.AfterDiscount);
      this.setState({ChangeValue: change});
      this.setState({IsCredit: 0});
      this.setState({OnAccount: 0});
    }
  };

  isReturnItemReturnShow = () => {
    if (this.state.SummaryReturnValue > 0) {
      return true;
    } else {
      return false;
    }
  };

  isReturnAdjustedEnable = () => {
    if (
      (this.state.SummarySalesValue - this.state.SummaryReturnValue > 0 ||
        this.state.SummarySalesValue == 0) &&
      this.state.SummaryReturnValue == 0
    ) {
      return true;
    } else {
      return false;
    }
  };

  render() {
    return (
      <>
        <ICubeAIndicator isShow={this.state.isLoading} />
        <View
          style={[
            ApplyStyleColor(
              globalColorObject.Color.Lightprimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
            {flex: 1},
          ]}>
          <View style={{flex: 3}}>
            <ScrollView>
              {/* Customer Details */}
              {isnull(this.state.CustomeName, '') != '' && (
                <>
                  <Text
                    style={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {margin: 8},
                    ]}>
                    Customer Details
                  </Text>
                  <View
                    style={{
                      borderWidth: 1,
                      marginHorizontal: 8,
                      borderColor: globalColorObject.Color.Lightprimary,
                      borderRadius: 5,
                    }}>
                    <View style={{flexDirection: 'column', width: '100%'}}>
                      <Text style={styles.CustomLabel}>
                        # {isnull(this.LocalData.CustomerData.Memberno, '')} -{' '}
                        {this.state.CustomeName}
                      </Text>
                      {isnull(this.LocalData.CustomerData.Mobileno, '') !=
                        '' && (
                        <Text style={styles.CustomLabel}>
                          &#x2706; XXXXX
                          {this.LocalData.CustomerData.Mobileno.substr(
                            this.LocalData.CustomerData.Mobileno.length > 5
                              ? this.LocalData.CustomerData.Mobileno.length - 5
                              : 0,
                            5,
                          )}
                        </Text>
                      )}
                    </View>
                  </View>
                </>
              )}

              {/* Amount Details */}

              <Text
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sxxl,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {margin: 8},
                ]}>
                Amount Details
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  marginHorizontal: 8,
                  borderColor: globalColorObject.Color.Lightprimary,
                  borderRadius: 5,
                }}>
                <View style={{flexDirection: 'row', width: '100%'}}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.CustomLabel}>No. of Products</Text>
                    <Text style={styles.CustomLabel}>Price</Text>
                    {this.isReturnAdjustedEnable() ? (
                      <TouchableOpacity
                        onPress={() => {
                          // console.log('return press');
                          this.setState({isShowReturn: true});
                        }}>
                        <Text style={styles.CustomLabel}>Return</Text>
                      </TouchableOpacity>
                    ) : (
                      this.isReturnItemReturnShow() && (
                        <Text style={styles.CustomLabel}>Return</Text>
                      )
                    )}
                    <MemoLevelDiscount
                      selectedValue={''}
                      backgroundColor={globalColorObject.Color.oppPrimary}
                      data={this.state.ItemDiountList || []}
                      onValueSelected={item => {
                        this.HandalItemDiscount(item, 1);
                      }}
                    />
                    {this.state.RoudOff != 0 && (
                      <Text
                        style={{
                          padding: 8,
                          backgroundColor: globalColorObject.Color.oppPrimary,
                        }}>
                        Round
                      </Text>
                    )}
                    <Text
                      style={{
                        padding: 8,
                        backgroundColor: globalColorObject.Color.oppPrimary,
                      }}>
                      Total
                    </Text>
                  </View>
                  <View style={{width: '50%'}}>
                    <Text style={[styles.CustomLabel, {textAlign: 'right'}]}>
                      {this.state.totalqty}
                    </Text>
                    <Text style={[styles.CustomLabel, {textAlign: 'right'}]}>
                      ₹ {this.state.SummarySalesValue}
                    </Text>
                    {this.isReturnAdjustedEnable() ? (
                      <Text
                        style={[
                          styles.CustomLabel,
                          {textAlign: 'right', color: 'red'},
                        ]}>
                        {' '}
                        ₹ {this.state.ReturnValue}{' '}
                      </Text>
                    ) : (
                      this.isReturnItemReturnShow() && (
                        <Text
                          style={[
                            styles.CustomLabel,
                            {textAlign: 'right', color: 'red'},
                          ]}>
                          ₹ {this.state.ReturnValue}
                        </Text>
                      )
                    )}
                    {
                      <Text style={[styles.CustomLabel, {textAlign: 'right'}]}>
                        {this.state.discouttype === 'Amount' ? '₹' : ''}{' '}
                        {this.state.discoutprice}{' '}
                        {this.state.discouttype === 'Percentage' ? '%' : ''}
                      </Text>
                    }
                    {this.state.RoudOff != 0 && (
                      <Text style={[styles.CustomLabel, {textAlign: 'right'}]}>
                        {' '}
                        ₹ {this.state.RoudOff}{' '}
                      </Text>
                    )}
                    <Text
                      style={[
                        styles.CustomLabel,
                        {fontWeight: 'bold', textAlign: 'right'},
                      ]}>
                      ₹ {this.state.AfterDiscount}
                    </Text>
                  </View>
                </View>
              </View>
              {/* Payment Mode */}
              {this.state.AfterDiscount > 0 && (
                <>
                  <Text
                    style={[
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                      {margin: 8},
                    ]}>
                    Select Payment
                  </Text>
                  <View
                    style={[
                      {
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                      },
                      ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.oppPrimary,
                      ),
                    ]}>
                    {/* On Account */}
                    <View
                      style={[
                        {
                          flexDirection: 'column',
                          width: 100,
                          paddingTop: 5,
                          borderRadius: 10,
                        },
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <TouchableOpacity
                        onPress={() => this.ValidateOnAccount()}>
                        <Text
                          style={[
                            // {width: 30, height: 30, alignSelf: 'center', color: 'white', fontSize: 30,},
                            {alignSelf: 'center'},

                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.mediam.mxxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          {/* &#9977; */}
                          &#9817;
                        </Text>
                        <RichTextInput
                          placeholder="Credit"
                          isShowAnimateText={false}
                          value={isnull(this.state.OnAccount, 0)}
                          inputStyle={[
                            {
                              textAlign: 'center',
                              borderBottomWidth: 0,
                              marginTop: 0,
                              paddingTop: 0,
                            },
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.small.sxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}
                          wrapperStyle={[{width: '100%'}]}
                          inputProps={{
                            keyboardType: 'phone-pad',
                            editable: false,
                            returnKeyType: 'next',
                            autoCorrect: false,
                            autoCapitalize: 'none',
                            placeholderTextColor:
                              globalColorObject.Color.oppPrimary,
                          }}
                        />
                      </TouchableOpacity>
                    </View>

                    {/* UPI */}
                    <View
                      style={[
                        {
                          flexDirection: 'column',
                          width: 100,
                          paddingTop: 5,
                          borderRadius: 10,
                        },
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState(prevState => ({
                            ...prevState,
                            isShowMOP: true,
                          }))
                        }>
                        <Text
                          style={[
                            // {width: 30, height: 30, alignSelf: 'center', color: 'white', fontSize: 30,},
                            {alignSelf: 'center'},

                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.mediam.mxxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          &#9974;
                        </Text>
                      </TouchableOpacity>

                      <RichTextInput
                        placeholder="UPI"
                        isShowAnimateText={false}
                        value={isnull(this.state.SplAmount, 0)}
                        inputStyle={[
                          {
                            textAlign: 'center',
                            borderBottomWidth: 0,
                            marginTop: 0,
                            paddingTop: 0,
                          },
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}
                        wrapperStyle={[{width: '100%'}]}
                        onChangeText={BasicAmount =>
                          this.ValidateSpecialAmount(BasicAmount, [])
                        }
                        inputProps={{
                          keyboardType: 'phone-pad',
                          editable: this.state.TextInputDisableStatus
                            ? (this.state.OtherMOPList || []).length == 1
                            : false,
                          returnKeyType: 'next',
                          autoCorrect: false,
                          autoCapitalize: 'none',
                          placeholderTextColor:
                            globalColorObject.Color.oppPrimary,
                        }}
                      />
                    </View>
                    {/* Cash */}
                    <View
                      style={[
                        {
                          flexDirection: 'column',
                          width: 100,
                          paddingTop: 5,
                          borderRadius: 10,
                        },
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <Text
                        style={[
                          {alignSelf: 'center'},
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.mediam.mxxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}>
                        ₹
                      </Text>
                      <RichTextInput
                        placeholder="Cash"
                        isShowAnimateText={false}
                        value={isnull(this.state.Tender, 0)}
                        inputStyle={[
                          {
                            textAlign: 'center',
                            borderBottomWidth: 0,
                            marginTop: 0,
                            paddingTop: 0,
                          },
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}
                        wrapperStyle={[{width: '100%'}]}
                        onChangeText={BasicAmount =>
                          this.ValidateCash(BasicAmount)
                        }
                        inputProps={{
                          keyboardType: 'phone-pad',
                          editable: this.state.TextInputDisableStatus,
                          returnKeyType: 'next',
                          autoCorrect: false,
                          autoCapitalize: 'none',
                          placeholderTextColor:
                            globalColorObject.Color.oppPrimary,
                        }}
                      />
                    </View>
                    {/* Card */}
                    <View
                      style={[
                        {
                          flexDirection: 'column',
                          width: 100,
                          paddingTop: 5,
                          borderRadius: 10,
                        },
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BackgroundColor,
                        ),
                      ]}>
                      <TouchableOpacity
                        onPress={() => {
                          this.setState(prevState => ({
                            ...prevState,
                            isShowCardDetails: true,
                          }));
                        }}>
                        <Text
                          style={[
                            {alignSelf: 'center'},
                            ApplyStyleFontAndSizeAndColor(
                              globalFontObject.Font.Bold,
                              globalFontObject.Size.mediam.mxxl,
                              globalColorObject.Color.oppPrimary,
                              globalColorObject.ColorPropetyType.Color,
                            ),
                          ]}>
                          &#9782;
                        </Text>
                      </TouchableOpacity>
                      <RichTextInput
                        placeholder="Card"
                        isShowAnimateText={false}
                        value={isnull(this.state.Cardamount, 0)}
                        inputStyle={[
                          {
                            textAlign: 'center',
                            borderBottomWidth: 0,
                            marginTop: 0,
                            paddingTop: 0,
                          },
                          ApplyStyleFontAndSizeAndColor(
                            globalFontObject.Font.Bold,
                            globalFontObject.Size.small.sxl,
                            globalColorObject.Color.oppPrimary,
                            globalColorObject.ColorPropetyType.Color,
                          ),
                        ]}
                        wrapperStyle={[{width: '100%'}]}
                        onChangeText={BasicAmount =>
                          this.ValidateCardAmount(BasicAmount)
                        }
                        inputProps={{
                          keyboardType: 'phone-pad',
                          editable: this.state.TextInputDisableStatus
                            ? (this.state.SelectedCardDetails || []).length == 0
                            : false,
                          returnKeyType: 'next',
                          autoCorrect: false,
                          autoCapitalize: 'none',
                          placeholderTextColor:
                            globalColorObject.Color.oppPrimary,
                        }}
                      />
                    </View>
                  </View>
                </>
              )}
            </ScrollView>
          </View>
          <View
            style={{
              flexDirection: 'row',
              height: 40,
              justifyContent: 'space-evenly',
              alignContent: 'center',
            }}>
            {this.state.ChangeValue != 0 && (
              <Text
                style={[
                  {
                    textAlign: 'center',
                    alignSelf: 'center',
                    width: '48%',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.BlackColor,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                ₹ {this.state.ChangeValue.toFixed(2)}
              </Text>
            )}
            <TouchableOpacity
              onPress={() => this.SaveHandler()}
              style={[
                styles.SaveBottom,
                this.state.ChangeValue != 0 && {width: '48%'},
              ]}>
              <Text
                style={[
                  {
                    textAlign: 'center',
                  },
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.mediam.ml,
                    globalColorObject.Color.oppPrimary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                ]}>
                Save
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.isShowReturn}
          onRequestClose={() => {
            Alert.alert('Screen has been closed.');
          }}>
          <View
            style={{
              flex: 1,
              margin: 5,
              backgroundColor: globalColorObject.Color.Lightprimary,
            }}>
            <FlatList
              data={this.state.ReturnList}
              renderItem={({item, index}) => (
                <BillListItem
                  data={item}
                  index={index}
                  ChangeSelectHandler={this.ChangeSelectHandler}
                />
              )}
              keyExtractor={item => item.ID}
              // to have some breathing space on bottom
              ListFooterComponent={
                <View style={{width: '100%', marginTop: 15}} />
              }
            />
            <View
              style={[
                styles.CardTextContainer,
                {
                  width: '100%',
                  // marginLeft: 5,
                  // marginRight: 5,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: globalColorObject.Color.Primary,
                },
              ]}>
              <TouchableOpacity
                style={{
                  width: '50%',
                  height: 35,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  borderRadius: 10,
                  elevation: 5,
                  backgroundColor: globalColorObject.Color.Primary,
                }}
                onPress={() => {
                  let get_SelectedREcord = (this.state.ReturnList || []).filter(
                    item => item.Select,
                  );
                  // console.log('log filter : ', get_SelectedREcord);
                  if (get_SelectedREcord.length > 0) {
                    let REt_Val = 0;
                    let AdjREt = '';
                    get_SelectedREcord.forEach(item => {
                      REt_Val = REt_Val + parseFloat(item.Return_Value);
                      AdjREt = AdjREt + item.ID.toString() + ',';
                    });
                    AdjREt = AdjREt != '' ? AdjREt.slice(0, -1) : '';
                    // console.log('REt_Val : ', REt_Val, ' AdjREt : ', AdjREt);
                    let RetDiscouValue = this.ReturnDiscountValue(
                      parseFloat(this.state.SummarySalesValue) -
                        parseFloat(REt_Val),
                    );

                    let get_NetValue = this.NumberRoudTwo(
                      this.state.SummarySalesValue - REt_Val - RetDiscouValue,
                    );
                    let get_RoundOff = this.RoundOfValue(get_NetValue);

                    // console.log(
                    //   'Selected REcord',
                    //   JSON.stringify(get_SelectedREcord),
                    // );
                    this.setState({
                      ReturnValue: REt_Val,
                      AdjReturnList: AdjREt,
                      isShowReturn: false,
                      AfterDiscount: get_NetValue + get_RoundOff,
                      Tender: get_NetValue + get_RoundOff,
                      RoudOff: get_RoundOff,
                    });
                  } else {
                    AlertMessage('Please select any one return.');
                  }
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.cardTextFieldHeader,
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  Ok
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  paddingLeft: 10,
                  width: '48%',
                  height: 35,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  borderRadius: 10,
                  elevation: 5,
                  backgroundColor: globalColorObject.Color.Primary,
                }}
                onPress={() => {
                  let RetDiscouValue = this.ReturnDiscountValue(
                    parseFloat(this.state.SummarySalesValue),
                  );
                  let get_NetValue = this.NumberRoudTwo(
                    this.state.SummarySalesValue - RetDiscouValue,
                  );
                  let get_RoundOff = this.RoundOfValue(get_NetValue);
                  this.setState({
                    isShowReturn: false,
                    AdjReturnList: '',
                    ReturnValue: 0,
                    AfterDiscount: get_NetValue + get_RoundOff,
                    Tender: get_NetValue + get_RoundOff,
                  });
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.cardTextFieldHeader,
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <OtherMOP
          MOPList={this.state.OtherMOPList}
          isShowMOP={this.state.isShowMOP}
          ChangeMOPValue={this.ChangeMOPValue.bind(this)}
          ApplyOtherMOP={this.ApplyOtherMOP.bind(this)}
          CloseOtherMOP={this.CloseOtherMOP}
        />

        {/* CArd DEtails */}

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isShowCardDetails}
          style={{marginTop: 50, paddingTop: 100}}
          onRequestClose={() => {}}>
          <BlurView style={styles.blurredBackdrop} blurType="dark" />
          <View
            style={{
              flex: 1,
              marginTop: '30%',
              marginHorizontal: 5,
              paddingHorizontal: 20,
              borderTopLeftRadius: 25,
              borderTopRightRadius: 25,
              paddingTop: 20,
              backgroundColor: globalColorObject.Color.oppPrimary,
            }}>
            <FlatList
              data={this.state.Getcardtype}
              renderItem={({item, index}) => {
                return (
                  <View
                    key={item.ID}
                    style={{width: '100%', flexDirection: 'row'}}>
                    <Text style={{width: '50%', alignSelf: 'flex-end'}}>
                      {item.CardName}
                    </Text>
                    <RichTextInput
                      isShowAnimateText={false}
                      placeholder={''}
                      value={isnull(item.Value, 0)}
                      onChangeText={Value =>
                        this.ChangeCArdDetailsValue(index, Value, 'Value')
                      }
                      wrapperStyle={[{width: '50%', borderBottomWidth: 1}]}
                      inputProps={{
                        keyboardType: 'phone-pad',
                        returnKeyType: 'next',
                        textAlign: 'center',
                        autoCorrect: false,
                        autoCapitalize: 'none',
                        placeholderTextColor:
                          globalColorObject.Color.BlackColor,
                      }}
                    />
                  </View>
                );
              }}
              keyExtractor={item => item.ID}
              // to have some breathing space on bottom
              ListFooterComponent={
                <View style={{width: '100%', marginTop: 15}} />
              }
            />
            <View
              style={[
                styles.CardTextContainer,
                {
                  width: '100%',
                  // marginLeft: 5,
                  // marginRight: 5,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: globalColorObject.Color.Primary,
                },
              ]}>
              <TouchableOpacity
                style={{
                  width: '50%',
                  height: 35,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  borderRadius: 10,
                  elevation: 5,
                  backgroundColor: globalColorObject.Color.Primary,
                }}
                onPress={() => this.ApplyCard()}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.cardTextFieldHeader,
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  Ok
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  paddingLeft: 10,
                  width: '48%',
                  height: 35,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  borderRadius: 10,
                  elevation: 5,
                  backgroundColor: globalColorObject.Color.Primary,
                }}
                onPress={() => {
                  const NetValue = parseFloat(
                    isnull(this.state.AfterDiscount, '0'),
                  );
                  const Change =
                    parseFloat(NetValue) -
                    parseFloat(this.state.SplAmount) -
                    parseFloat(this.state.OnAccount);
                  this.setState({
                    Tender: Change > 0 ? Change : 0,
                    ChangeValue: Change > 0 ? 0 : Math.abs(Change),
                    SelectedCardDetails: [],
                    Cardamount: 0,
                    isShowCardDetails: false,
                  });
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.cardTextFieldHeader,
                    {textAlign: 'center'},
                    ApplyStyleFontAndSizeAndColor(
                      globalFontObject.Font.Bold,
                      globalFontObject.Size.small.sxxl,
                      globalColorObject.Color.oppPrimary,
                      globalColorObject.ColorPropetyType.Color,
                    ),
                  ]}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </>
    );
  }
}

const styles = StyleSheet.create({
  blurredBackdrop: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  CustomLabel: {
    borderBottomWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    backgroundColor: globalColorObject.Color.oppPrimary,
    padding: 8,
  },
  CardTextContainer: {
    width: '25%',
    marginVertical: 5,
  },
  BillCard: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginHorizontal: 9,
  },
  ActiveTab1: {
    width: '50%',
  },
  InActiveTab1: {
    width: '50%',
  },
  ActiveTab: {
    borderWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    padding: 10,
    //textAlign: 'center',
    marginBottom: 0,
    width: '50%',
    backgroundColor: globalColorObject.Color.oppPrimary,
    color: 'black',
  },
  InActiveTab: {
    borderWidth: 1,
    borderColor: globalColorObject.Color.Lightprimary,
    padding: 10,
    marginBottom: 0,
    width: '50%',
    backgroundColor: globalColorObject.Color.Primary,
    color: 'white',
    fontWeight: 'bold',
  },
  floatingActionButton1: {
    display: 'none',
  },
  floatingActionButton: {
    position: 'absolute',
    borderRadius: 10,
    width: '100%',
    backgroundColor: globalColorObject.Color.oppPrimary,
  },
  SaveBottom: {
    width: '98%',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: globalColorObject.Color.Primary,
  },
  ConfigLogo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 35,
    marginRight: 2,
    backgroundColor: globalColorObject.Color.oppPrimary,
    borderRadius: 22,
    borderColor: globalColorObject.Color.Lightprimary,
    borderWidth: 0.2,
    margin: 5,
  },
  ClearModalButton: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 60,
    height: 35,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: globalColorObject.Color.Lightprimary,
    marginTop: 3,
    backgroundColor: globalColorObject.Color.Primary,
  },
  clearButtonText: {
    color: globalColorObject.Color.oppPrimary,
  },
  ConfigLogo2: {
    width: 30,
    height: 30,
    backgroundColor: globalColorObject.Color.oppPrimary,
    borderRadius: 22,
    borderColor: globalColorObject.Color.Lightprimary,
    borderWidth: 0.2,
    margin: 5,
    position: 'absolute',
    right: 5,
  },
  CardLine: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
  },
  MiniInputField: {
    height: 40,
    borderWidth: 0.2,
    borderColor: 'gray',
    width: 50,
    borderRadius: 5,
    fontSize: 12,
  },
});
