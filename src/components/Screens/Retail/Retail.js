import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';

export default class Retail extends Component {
  render() {
    return <Text style={styles.Text}>Retail Under Contruction</Text>;
  }
}

const styles = StyleSheet.create({
  Text: {
    marginTop: '80%',
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
  },
});
