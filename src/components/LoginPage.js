/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-string-refs */
/* eslint-disable eqeqeq */
import React from 'react';
import {StackActions} from '@react-navigation/native';
import {
  Alert,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  Text,
  Image,
  KeyboardAvoidingView,
  Platform,
  BackHandler,
} from 'react-native';
import moment from 'moment';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../Style/GlobalStyle';
import {ConfirmationWithAllInput} from '../Helpers/HelperMethods';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Orientation from 'react-native-orientation';
import ICubeAIndicator from './Tools/ICubeIndicator';
import {Button} from '../components/Custom/Button';

// import {myAppContext} from '../../App';
// Local Imports
import PinView from './Pin';
import {
  Encrypter,
  Request,
  AlertMessage,
  DeviceMacAddress,
  MobileDeviceName,
  Confirmation,
  AlertError,
  CheckCurrentSession,
  LogoutUser,
} from '../Helpers/HelperMethods';

class LoginPage extends React.Component {
  pinView = React.createRef();
  constructor(props) {
    super(props);
    this.IP = '';
    this.state = {
      username: '',
      password: '',
      LoginClicked: false,
      ActiveTab: 1,
      isFetched: true,
      Location: 1,
      enteredPin: '',
      showCompletedButton: true,
    };
  }

  componentDidMount() {
    // console.log('login context : ', this.context);
    this.setState({isFetched: false});
    Orientation.lockToPortrait();
    this.GetIP();
    //  this.props.fetchlocation();
    //  console.log(this.props.fetchlocation())
  }

  Logoutdata = () => {
    LogoutUser();
    this.props.stopsessioninterval && this.props.stopsessioninterval();
    this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
  };
  GetIP = async () => {
    try {
      const [
        [, getGUID],
        [, getip],
        [, getLocationCode],
        [, getSessionId],
        [, getisLogin],
      ] = await AsyncStorage.multiGet([
        'GUID',
        'IP',
        'LocationCode',
        'SessionId',
        'isLogin',
      ]);
      // console.log(
      //   ' getip : ',
      //   getip,
      //   ' getLocationCode : ',
      //   getLocationCode,
      //   ' getGUID : ',
      //   getGUID,
      //   ' getisLogin : ',
      //   getisLogin,
      // );
      this.IP = getip;
      this.setState({Location: getLocationCode});
      if (!this.IP) {
        this.props.navigation.dispatch(StackActions.replace('Config'));
      } else {
        try {
          const {
            isValidResponse,
            isInvlaid,
            IsNeedtoClose,
            ValidateString,
            ValidateReturnString,
          } = await CheckCurrentSession(getip, getSessionId);
          if (isValidResponse && !isInvlaid && !IsNeedtoClose && getisLogin) {
            // console.log('auto login ');
            this.AfterLoginSucess();
          } else if (isValidResponse && getisLogin) {
            console.log('invalid session.');
            LogoutUser();
            this.props.stopsessioninterval && this.props.stopsessioninterval();
          }
        } catch (ee) {}
      }
    } catch (dd) {}
    this.setState({isFetched: true});
  };

  AfterLoginSucess = () => {
    Orientation.unlockAllOrientations();
    this.props.navigation.dispatch(StackActions.replace('iCubeNavHome'));
    this.props.startsessioninterval(this.Logoutdata);
    // this.context.changeSessionData({test: 'my test', tkndkf: 'dfdfg'});
  };

  ValidateCredentials = () => {
    let {username, password} = this.state;
    if (username === '') {
      AlertMessage('Enter username');
      this.refs.Username.focus();
    } else if (password === '') {
      AlertMessage('Enter password');
      this.refs.Password.focus();
    } else {
      this.setState({LoginClicked: true});
      this.ValidateCredentialsWithAPI();
    }
  };

  AlertMessage = msg => {
    Alert.alert('iCube Alert', msg);
  };
  HandleTabsClick = () => {
    this.setState(prevState => {
      return {
        ...prevState,
        ActiveTab: prevState.ActiveTab == 1 ? 0 : 1,
      };
    });
  };
  ValidateCredentialsWithAPI = async () => {
    Keyboard.dismiss();
    // Encrypt Username and password
    let EncryptedData = Encrypter.EncryptCredentials(
      this.state.username,
      this.state.ActiveTab == 1 ? this.state.password : this.state.enteredPin,
    );
    this.setState({enteredPin: '', LoginClicked: true});
    // console.log('myUN : ', EncryptedData.encryptedUsername.toString());
    // console.log('myPS : ', EncryptedData.encryptedPassword.toString());
    let EncUsername = encodeURIComponent(
      EncryptedData.encryptedUsername.toString(),
    );
    let EncPassword = encodeURIComponent(
      EncryptedData.encryptedPassword.toString(),
    );
    // let EncPin = encodeURIComponent(EncryptedData.encryptedPin.toString());
    let loginuri = `${this.IP}/icubekey`;
    // &enteredPin=${EncPin}
    let get_DeviceMacAddress = DeviceMacAddress();
    let get_deviceNamenew = await MobileDeviceName();
    let loginuridata = `username=${EncUsername}&password=${EncPassword}&grant_type=password&TerminalName=${get_DeviceMacAddress}&LoginType=${
      this.state.ActiveTab == 0 ? 'pin' : 'user'
    }&UI_ViewType=mobile&CurrentDate=${moment(new Date()).format(
      'YYYY-MM-DD HH:mm:ss',
    )}&LocationCode=${this.state.Location}`;
    // console.log('etst' + loginuri + loginuridata);
    Request.login(loginuri, loginuridata)
      .then(json => {
        // console.log('Login Check', json);
        if (json.access_token) {
          this.SetUserTokenInStorage(json)
            .then(() => {
              // just refresh the app and it will automatically redirect to Home screen
              // this.props.refreshOnLogin();
              this.AfterLoginSucess();
            })
            .catch(err => {
              console.error(err);
              AlertMessage(JSON.stringify(err));
            });
        } else {
          let ErrorMessage = '';
          if (json.error_description) {
            ErrorMessage = json.error_description;
          } else if (json.error) {
            ErrorMessage = json.error;
          } else {
            ErrorMessage = JSON.stringify(json);
          }
          if (ErrorMessage == 'device not registered') {
            // console.log('get device name : ', get_deviceNamenew);
            Confirmation(
              'Thanks for installing icube APP.your device has to register and activate to continue.Do you want to register now ?',
              () => {
                Request.post(`${this.IP}/api/Common/InsertNewMobileDevice`, {
                  Name: get_deviceNamenew,
                  UIC: get_DeviceMacAddress,
                  Type: Platform.OS === 'ios' ? 'iPhone' : 'Android',
                })
                  .then(res => res.json())
                  .then(json => {
                    // console.log('Post Device : ', json);
                    if (json.Message == 'Saved') {
                      AlertMessage('Registerd.');
                    } else {
                      AlertMessage('Failed to Register.');
                    }
                  })
                  .catch(AlertError);
              },
            );
          } else {
            AlertMessage(ErrorMessage);
          }

          if (this.state.ActiveTab === 0) {
            this.pinView.current.clearAll();
          }
          this.setState({LoginClicked: false});
        }
      })
      .catch(err => {
        
        AlertMessage('Something went wrong \n' + JSON.stringify(err));
        this.setState({LoginClicked: false});
      });
    // .finally(() => this.setState({ LoginClicked: false }));
  };

  SetUserTokenInStorage = async ({
    LicenseType,
    LicenseInfo,
    ShowLicenseInfo,
    DateFormat,
    DecimaString,
    SessionId,
    UserCode,
    UserId,
    RoleId,
    ProfileId,
    FinancialYearId,
    UserName,
    EmployeeName,
    access_token,
    token_type,
    LoginDateTime,
    POSPrintTerminalName,
  }) => {
    await AsyncStorage.multiSet([
      ['LicenseInfo', LicenseInfo],
      ['ShowLicenseInfo', ShowLicenseInfo],
      ['DateFormat', DateFormat],
      ['DecimaString', DecimaString],
      ['LicenseType', LicenseType],
      ['SessionId', SessionId],
      ['UserCode', UserCode],
      ['UserId', UserId],
      ['RoleId', RoleId],
      ['UserName', UserName],
      ['EmployeeName', EmployeeName],
      ['Token', `${token_type} ${access_token}`],
      ['ProfileId', ProfileId],
      ['FinancialYearId', FinancialYearId],
      ['isLogin', 'true'],
      ['LoginDateTime', LoginDateTime],
      ['POSPrintTerminalName', POSPrintTerminalName],
    ]);
    return true;
  };
  render() {
    return (
      <>
        {this.state.LoginClicked || !this.state.isFetched ? (
          <View style={{paddingTop: 20}}>
            <ICubeAIndicator />
          </View>
        ) : (
          <View
            style={{
              backgroundColor: 'white',
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              height: '100%',
              padding: 20,
            }}>
            <KeyboardAvoidingView
              style={{flex: 1, width: '100%', height: '100%', maxWidth: 500}}
              behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
              <View
                style={{
                  width: '100%',
                  height: '90%',
                }}>
                <View
                  style={{
                    height: 150,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    resizeMode="contain"
                    style={{
                      width: 150,
                      height: 150,
                    }}
                    source={require('../assets/images/newAppLogo.png')}
                  />
                </View>
                {this.state.ActiveTab === 0 && (
                  <View style={styles.PinContainer}>
                    <PinView
                      inputSize={20}
                      ref={this.pinView}
                      pinLength={4}
                      buttonSize={50}
                      onValueChange={value => {
                        this.setState(
                          prevState => {
                            return {enteredPin: value};
                          },
                          () => {
                            if (value.length >= 4) {
                              // console.log('pin > = 4');
                              this.ValidateCredentialsWithAPI();
                            }
                          },
                        );
                      }}
                      inputViewFilledStyle={ApplyStyleColor(
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      )}
                      inputViewEmptyStyle={ApplyStyleColor(
                        globalColorObject.Color.Lightprimary,
                        globalColorObject.ColorPropetyType.BackgroundColor,
                      )}
                      buttonViewStyle={[
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BorderColor,
                        ),
                        {
                          borderWidth: 2,
                          borderRadius: 15,
                        },
                      ]}
                      buttonTextStyle={[
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Bold,
                          globalFontObject.Size.mediam.ml,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BorderColor,
                        ),
                        ApplyStyleColor(
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.Color,
                        ),
                        styles.TextDefault,
                        // Color.primaryBorderColor,
                        // Color.primaryColor,
                        // {textDecorationLine: 'none', fontSize: 25},
                      ]}
                      onButtonPress={key => {
                        if (key === 'custom_right') {
                          this.pinView.current.clear();
                        }
                      }}
                      // customLeftButton={
                      //   this.state.enteredPin.length >= 4
                      //     ? this.ValidateCredentialsWithAPI()
                      //     : undefined
                      // }
                      customRightButton={
                        this.state.enteredPin.length > 0 ? (
                          <Text
                            style={[
                              styles.TextDefault,
                              {
                                padding: 5,
                                textAlign: 'center',
                                fontSize: 25,
                                textDecorationLine: 'none',
                              },
                            ]}>
                            X
                          </Text>
                        ) : undefined
                      }
                    />
                  </View>
                )}
                {this.state.ActiveTab === 1 && (
                  <View style={[styles.InputsContainer]}>
                    <TextInput
                      style={[
                        styles.Credentials,
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Regular,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BorderColor,
                        ),
                      ]}
                      placeholder="User Name"
                      value={this.state.username}
                      placeholderTextColor={globalColorObject.Color.Primary}
                      returnKeyType="next"
                      autoCorrect={false}
                      onSubmitEditing={() => {
                        this.refs.Password.focus();
                      }}
                      autoCapitalize="none"
                      ref={'Username'}
                      onChangeText={username => this.setState({username})}
                    />
                    <TextInput
                      style={[
                        styles.Credentials,
                        ApplyStyleFontAndSizeAndColor(
                          globalFontObject.Font.Regular,
                          globalFontObject.Size.small.sxl,
                          globalColorObject.Color.Primary,
                          globalColorObject.ColorPropetyType.BorderColor,
                        ),
                      ]}
                      placeholder="Password"
                      value={this.state.password}
                      placeholderTextColor={globalColorObject.Color.Primary}
                      secureTextEntry={true}
                      returnKeyType="go"
                      autoCorrect={false}
                      autoCapitalize="none"
                      ref={'Password'}
                      onChangeText={password => this.setState({password})}
                    />
                    {!this.state.LoginClicked && (
                      <Button
                        value="Login"
                        onPressEvent={this.ValidateCredentials}
                      />
                    )}
                  </View>
                )}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  height: '10%',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity onPress={this.HandleTabsClick}>
                  <Text
                    style={[
                      {textDecorationLine: 'underline'},
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                    ]}>
                    {this.state.ActiveTab == 0 ? 'User' : 'Pin'} Login
                  </Text>
                </TouchableOpacity>
                
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.dispatch(
                      StackActions.replace('PriceCheckNav'),
                    );
                  }}>
                  <Text
                    style={[
                      {textDecorationLine: 'underline'},
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                    ]}>
                    Price Checker
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.dispatch(
                      StackActions.replace('Config'),
                    );
                  }}>
                  <Text
                    style={[
                      {textDecorationLine: 'underline'},
                      ApplyStyleFontAndSizeAndColor(
                        globalFontObject.Font.Bold,
                        globalFontObject.Size.small.sxxl,
                        globalColorObject.Color.Primary,
                        globalColorObject.ColorPropetyType.Color,
                      ),
                    ]}>
                    Configuration
                  </Text>
                </TouchableOpacity>
              </View>
              
            </KeyboardAvoidingView>
          </View>
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  InputsContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  Credentials: {
    margin: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 8,
    borderWidth: 2,
  },

  PinContainer: {
    flex: 1,
    width: '100%',
    height: 325,
  },
});

// LoginPage.contextType = myAppContext;
export default LoginPage;
