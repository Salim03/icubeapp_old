/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, Modal, Animated, Easing, View, Text} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import {string, bool} from 'prop-types';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../Style/GlobalStyle';

export default class ICubeAIndicator extends React.Component {
  constructor(props) {
    super(props);
    this.RotateValueHolder = new Animated.Value(0);
  }

  componentDidMount() {
    this.StartImageRotateFunction();
  }

  StartImageRotateFunction() {
    this.RotateValueHolder.setValue(0);

    Animated.timing(this.RotateValueHolder, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => this.StartImageRotateFunction());
  }
  render() {
    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (
      <>
        <Modal
          transparent={true}
          animationType="slide"
          visible={this.props.isShow || false}>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <BlurView
              style={{
                flex: 1,
                position: 'absolute',
                width: '100%',
                height: '100%',
              }}
              blurType="light"
              blurAmount={7}
            />
            <SafeAreaView
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Animated.Image
                style={{
                  width: 60,
                  height: 60,
                  transform: [{rotate: RotateData}],
                }}
                source={require('../../assets/images/newAppLogo.png')}
              />
              <Text
                style={[
                  ApplyStyleFontAndSizeAndColor(
                    globalFontObject.Font.Bold,
                    globalFontObject.Size.small.sl,
                    globalColorObject.Color.Primary,
                    globalColorObject.ColorPropetyType.Color,
                  ),
                  {textAlign: 'center', paddingVertical: 10},
                ]}>
                {' '}
                {this.props.ModalText ? this.props.ModalText : 'Loading .....'}
              </Text>
            </SafeAreaView>
          </View>
        </Modal>
      </>
    );
  }
}

// default props
ICubeAIndicator.defaultProps = {
  isShow: true,
  ModalText: '',
};

// prop type defenition
ICubeAIndicator.propTypes = {
  isShow: bool,
  ModalText: string,
};
