import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Ripple from 'react-native-material-ripple';
// import DateTimePicker from '@react-native-community/datetimepicker';
import PropTypes, {
  string,
  func,
  object,
  array,
  oneOfType,
  bool,
} from 'prop-types';

import DateTimePickerModal from 'react-native-modal-datetime-picker';
import RichTextInput from './RichTextInput';
import {layoutStyle} from '../Layout/Layout';
import {
  dismissKeyboard,
  ReturnDisplayStringForDate,
  isnull,
} from '../../Helpers/HelperMethods';

const ModalDatePicker = props => {
  const {selectedValue, isShowAnimateText} = props;
  const [showModal, setShowModal] = useState(false);
  const [l_selectedValue, setl_selectedValue] = useState(new Date());
  const [selectedValueText, setSelectedValueText] = useState('');

  useEffect(() => {
    setl_selectedValue(
      isnull(selectedValue, '') != '' ? new Date(selectedValue) : new Date(),
    );
    setSelectedValueText(
      isnull(selectedValue, '') != ''
        ? ReturnDisplayStringForDate(new Date(selectedValue))
        : '',
    );
  }, [selectedValue]);

  const openModal = () => {
    setShowModal(true);
  };
  const closeModal = () => {
    dismissKeyboard();
    setShowModal(false);
  };

  const onChange = selectedDate => {
    // setSelectedValueText(ReturnDisplayStringForDate(selectedDate));
    let get_selectedDate =
      isnull(selectedDate, '') != '' ? new Date(selectedDate) : '';
    // console.log('change value : ', get_selectedDate);
    closeModal();
    props.onValueSelected(get_selectedDate, get_selectedDate);
  };
  return (
    <>
      <Ripple
        onPress={openModal}
        style={[
          layoutStyle.FieldContainer,
          layoutStyle.FieldLayout,
          props.fieldWrapperStyle,
        ]}>
        <View style={styles.passwordContainer}>
          <RichTextInput
            placeholder={props.placeholder}
            value={selectedValueText}
            isShowAnimateText={isShowAnimateText}
            wrapperStyle={[{width: '100%', flex: 1, marginHorizontal: 0}]}
          />
          <Image
            source={require('../../assets/images/calendar1.png')}
            style={styles.headerIcon}
          />
        </View>
      </Ripple>
      {/* suggestion Modal */}
      {/* {showModal && (
        <DateTimePicker
          testID="dateTimePicker"
          value={l_selectedValue}
          mode={'date'}
          display="default"
          onChange={onChange}
        />
      )} */}
      <DateTimePickerModal
        isVisible={showModal}
        mode="date"
        onConfirm={onChange}
        onCancel={() => closeModal()}
      />
    </>
  );
};

// default props
ModalDatePicker.defaultProps = {
  isShowAnimateText: true,
};

// prop type defenition
ModalDatePicker.propTypes = {
  placeholder: string.isRequired,
  selectedValue: PropTypes.any,
  onValueSelected: func.isRequired,
  isShowAnimateText: bool,
  fieldWrapperStyle: oneOfType([object, array]),
};

const styles = StyleSheet.create({
  passwordContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  headerIconsContainer: {
    flexDirection: 'row',
  },
  headerIcon: {
    width: 20,
    height: 30,
    paddingHorizontal: 8,
    marginBottom: 5,
  },
});

export default ModalDatePicker;
export {styles as MSPStyles};
