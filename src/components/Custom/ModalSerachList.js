import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { Modal, View, Text, FlatList } from 'react-native';
import { BlurView } from '@react-native-community/blur';
import Ripple from 'react-native-material-ripple';
import { string, func, object, array, oneOfType, bool, number, elementType, oneOf, any } from 'prop-types';

import RichTextInput from './RichTextInput';
import { MSPStyles } from './ModalSearchablePicker';

const ModalSearchList = ({ value, showModal, animType, dataArray, RenderItemComp, searchPlaceholder, onCancel, labelProp, valueProp, focusSearchonOpen, onSelectItem }) => {

   // states
   const [searchText, setSearchText] = useState("");
   const [filteredData, setFilteredData] = useState(dataArray);

   // refs
   const modalInputRef = useRef();

   useEffect(() => {
      showModal && focusSearchonOpen && setTimeout(() => modalInputRef.current.focus(), 500);
   }, [showModal, focusSearchonOpen]);

   useEffect(() => {
      const SelectedItem = findSelectedData();
      // console.log("Value passed", value, "Selected Value", SelectedItem);
      SelectedItem ? (setSearchText(SelectedItem[labelProp]), setFilteredData([SelectedItem])) : (setSearchText(""), setFilteredData(dataArray));
   }, [value, valueProp, labelProp]);

   const handleSearchTextChange = text => { setSearchText(text); setFilteredData(prepareFilterData(text)); };
   const prepareFilterData = txt => (dataArray.filter(itm => itm[labelProp].toString().toLowerCase().includes(txt.toLowerCase())) || []);
   const findSelectedData = () => (dataArray.find(itm => itm[valueProp] === value));
   const setModalInputRef = ref => { modalInputRef.current = ref; }
   const handleItemSelect = data => { data && onSelectItem(data[valueProp], data); }

   return (
      <Modal visible={showModal} animationType={animType} onRequestClose={onCancel}>
         <View style={MSPStyles.ModalContainer}>
            <BlurView style={MSPStyles.blurredBackdrop} blurType="dark" blurAmount={7} />
            <View style={MSPStyles.searchWrapper}>
               <View style={MSPStyles.searchFieldContainer}>
                  <RichTextInput
                     placeholder={searchPlaceholder || ""}
                     value={searchText}
                     onChangeText={handleSearchTextChange}
                     wrapperStyle={{ flex: 1 }}
                     inputref={setModalInputRef}
                     inputStyle={{ borderBottomWidth: 0 }}
                  // inputProps={{ onSubmitEditing: handleSearchSubmit }}
                  />
                  {/* cancel button to close the modal */}
                  <Ripple onPress={onCancel} rippleCentered={true} rippleDuration={300} rippleContainerBorderRadius={35} >
                     <View style={MSPStyles.closeModalButton}>
                        <Text style={MSPStyles.closeModalButtonText}>&times;</Text>
                     </View>
                  </Ripple>
               </View>
               <View style={MSPStyles.suggestionContainer}>
                  <FlatList
                     keyboardShouldPersistTaps={"always"}
                     contentContainerStyle={MSPStyles.FlatListStyle}
                     ListHeaderComponent={<View style={{ width: "100%", marginTop: 10 }}></View>}
                     ListFooterComponent={<View style={{ width: "100%", marginTop: 10 }}></View>}
                     data={filteredData}
                     renderItem={({ item }) => <RenderItemComp data={item} onSelectItem={handleItemSelect} />}
                     keyExtractor={(itm, index) => index.toString()}
                  />
               </View>
            </View>
         </View>
      </Modal>
   );
}

ModalSearchList.defaultProps = {
   animType: "fade",
   searchPlaceholder: "Search",
   focusSearchonOpen: true,
};

ModalSearchList.propTypes = {
   value: any.isRequired,
   valueProp: string.isRequired,
   labelProp: string.isRequired,
   showModal: bool.isRequired,
   animType: oneOf(["none", "slide", "fade"]),
   dataArray: array.isRequired,
   RenderItemComp: elementType.isRequired,
   searchPlaceholder: string,
   onCancel: func.isRequired,
   onSelectItem: func.isRequired,
   focusSearchonOpen: bool,
};


export default ModalSearchList;