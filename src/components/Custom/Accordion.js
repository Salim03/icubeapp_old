import React, { useRef, useState } from 'react';
import {
   View, Text, StyleSheet, Animated, Easing
} from 'react-native';
import PropTypes from 'prop-types';
import Ripple from 'react-native-material-ripple';

import { layoutStyle } from '../Layout/Layout';

const Accordion = props => {

   let contentHeight = useRef();
   let initialHeight = useRef(props.initialHeight);
   let AnimatedHeight = useRef(new Animated.Value(initialHeight.current));
   let AnimatedPosition = useRef(new Animated.Value(0));

   const [Expanded, setExpanded] = useState(false);

   const animateHeight = (expanded, forOrientationChange) => {
      return forOrientationChange ?
         Animated.timing(AnimatedHeight.current, {
            toValue: (contentHeight.current || 0) + initialHeight.current,
            duration: 400,
         })
         :
         Animated.timing(AnimatedHeight.current, {
            toValue: expanded ? 50 : (contentHeight.current || 0) + initialHeight.current,
            duration: 400,
         });
   }

   const animatePosition = (expanded) => {
      return Animated.timing(AnimatedPosition.current, {
         toValue: expanded ? 0 : 7,
         duration: 400,
         delay: 100,
         easing: Easing.elastic(2),
         useNativeDriver: true
      });
   }

   const handleLayoutChange = event => {
      const previousHeight = contentHeight.current;
      contentHeight.current = Math.round(event.nativeEvent.layout.height);
      const heightChanged = contentHeight.current !== previousHeight;
      // console.log("Height", heightChanged, contentHeight.current, previousHeight);
      !previousHeight && props.expandInitially && !Expanded && toggleAccordion();
      heightChanged && Expanded && animateHeight(Expanded, true).start();
   }

   const handleAccordionCollapse = async () => {
      Expanded && props.onCollapse && props.onCollapse();
   }

   const toggleAccordion = () => {
      handleAccordionCollapse();
      const currentState = Expanded;
      currentState && setExpanded(!currentState);
      Animated.parallel([
         animateHeight(currentState),
         props.animateHeader && animatePosition(currentState)
      ]).start(() => !currentState && setExpanded(!currentState));
   }

   const animatedWrapperStyle = {
      height: AnimatedHeight.current
   };

   const animatedHeaderStyle = {
      transform: [{ translateY: AnimatedPosition.current }]
   }

   const animatedRotation = AnimatedPosition.current.interpolate({
      inputRange: [0, 10],
      outputRange: ['0deg', '-68deg']
   });

   const expanderStyle = {
      color: '#6d6d6d',
      transform: [{ rotateZ: animatedRotation }, { scale: 1.6 }]
   }
   // { overflow: Expanded ? "visible" : "hidden" }, 
   return (
      <Animated.View style={[styles.wrapper, animatedWrapperStyle, props.wrapperStyle]}>
         <Animated.View style={[styles.header, animatedHeaderStyle, props.headerStyle]}>
            <Text style={[styles.headerTitle, layoutStyle.AppFont, props.titleStyle]} >{props.title}</Text>
            <Ripple onPress={toggleAccordion} rippleCentered={true} rippleDuration={300} rippleContainerBorderRadius={50}>
               <View style={[styles.headerToggler]}>
                  <Animated.Text style={[expanderStyle, layoutStyle.AppFont]}>+</Animated.Text>
               </View>
            </Ripple>
         </Animated.View>
         {/* [props.contentWrapperStyle] */}
         <View style={props.contentWrapperStyle} onLayout={handleLayoutChange}>
            {props.children}
         </View>
      </Animated.View>
   )
};

// Default props
Accordion.defaultProps = {
   initialHeight: 50,
   expandInitially: false,
   animateHeader: true,
};

// Prop types defenition
Accordion.propTypes = {
   title: PropTypes.string.isRequired,
   initialHeight: PropTypes.number,
   expandInitially: PropTypes.bool,
   animateHeader: PropTypes.bool,
   onCollapse: PropTypes.func,
   wrapperStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
   headerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
   titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
   contentWrapperStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

const styles = StyleSheet.create({
   wrapper: {
      margin: 10,
      marginBottom: 3,
      paddingHorizontal: 7,
      backgroundColor: 'white',
      borderRadius: 5,
      elevation: 5,
      overflow: "hidden"
   },
   header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      zIndex: 5,
   },
   headerTitle: {
      paddingVertical: 10,
      paddingLeft: 7,
      color: '#565656',
      fontWeight: 'bold',
      zIndex: 5,
   },
   headerToggler: {
      width: 50,
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
      textAlignVertical: 'center',
      zIndex: 5,
   }
});

export default Accordion;
export { styles as AccordionStyle }