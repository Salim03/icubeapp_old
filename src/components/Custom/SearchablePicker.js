import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, Text, TextInput, ScrollView, Keyboard } from 'react-native';

import RichTextInput from './RichTextInput';
import { layoutStyle } from '../Layout/Layout';
import { calculateDropdownPosition } from '../../Helpers/HelperMethods';

const initialState = {
   // searchText: '',
   selectedItemText: '',
   showSuggestion: false,
   suggestionList: [],
   suggestionPosition: { top: '100%' },
};

const SearchablePicker = (props) => {

   let [suggestion, setSuggestion] = useState(initialState);
   const [searchText, setSearchText] = useState("");
   let inputRef;
   let EmptyText = true;
   const { name, value } = props.propsName;

   useEffect(() => {
      let selectedItem = props.data.find(item => item[value] === props.value);
      let selectedItemText = (selectedItem && selectedItem[name]) || "";
      setSuggestion({
         ...suggestion,
         // searchText: selectedItemText,
         selectedItemText,
         showSuggestion: false,
         suggestionList: [],
      });
      setSearchText(selectedItemText);
   }, [props.value, props.data]);

   const selectItem = (name, value) => {
      setSuggestion({
         ...suggestion,
         // searchText: name,
         selectedItemText: name,
         showSuggestion: false,
         suggestionList: []
      });
      setSearchText(name);
      Keyboard.dismiss();
      props.onItemSelected(value);
   }

   const prepareSuggestionList = txt => {
      let FilteredList = props.data.filter(item => {
         return item[name].toString().toLowerCase().includes(txt);
      });
      return FilteredList.map((item, index) =>
         <TouchableOpacity key={index} onPress={() => selectItem(item[name], item[value])}>
            <Text style={{ padding: 10 }}>{item[name]}</Text>
         </TouchableOpacity>
      );
   }

   const setSuggestionData = (text, suggestionPosition) => {
      let suggestionList = prepareSuggestionList(text.toLowerCase());
      setSuggestion({
         ...suggestion,
         // searchText: text,
         showSuggestion: !(EmptyText),
         suggestionList,
         suggestionPosition: suggestionPosition || suggestion.suggestionPosition
      });
   }

   const HandleTextChange = (text) => {
      setSearchText(text);
      if (text !== "") {
         EmptyText = false;
         if (props.dynamicPopup) {
            calculateDropdownPosition(props.containerRef, inputRef, (suggestionPosition) => {
               setSuggestionData(text, suggestionPosition);
            });
         } else setSuggestionData(text);
      } else {
         EmptyText = true;
         setSuggestion({
            ...suggestion,
            // searchText: "",
            showSuggestion: false,
            suggestionList: []
         });
      }
   }

   const dismissSuggestion = () => {
      setSuggestion({
         ...suggestion,
         // searchText: suggestion.selectedItemText,
         showSuggestion: false,
         suggestionList: [],
      });
      setSearchText(suggestion.selectedItemText);
      props.onBlur && props.onBlur();
   }

   // const inputStyle = () => {
   //    const inputStyle = props.inputStyle;
   //    if (inputStyle && Array.isArray(inputStyle)) return [layoutStyle.CustomInput, ...inputStyle];
   //    else return [layoutStyle.CustomInput, inputStyle];
   // }

   // Note if you are using this Custom component inside a scrollView 
   // then make sure you have setted the ""keyboardShouldPersistTaps"" property to ""always"" in <ScrollView> tag
   // like this <ScrollView keyboardShouldPersistTaps={'always'}>

   // To make the dropdown work perfect don't give border for wrapper View
   // because that will make the position absolute drop down to work improperly
   return (
      <View style={[layoutStyle.FieldContainer, layoutStyle.FieldLayout, props.wrapperStyle]}>
         <View style={{ width: '100%' }}>
            {/* <Text style={[layoutStyle.CustomLabel,layoutStyle.FieldLabelFont, props.labelStyle]}>{props.label}</Text>
            <TextInput
               style={[layoutStyle.CustomInput, props.inputStyle]}
               value={searchText}
               onChangeText={HandleTextChange}
               returnKeyType='next'
               autoCorrect={false}
               autoCapitalize="none"
               onBlur={dismissSuggestion}
               onSubmitEditing={dismissSuggestion}
               ref={ref => { inputRef = ref }}
            /> */}
            <RichTextInput
               placeholder={props.label}
               rawStyle={[layoutStyle.CustomInput, props.inputStyle]}
               customControl={true}
               
               // inputStyle={[layoutStyle.CustomInput, props.inputStyle]}
               value={searchText}
               onChangeText={HandleTextChange}
               onBlur={dismissSuggestion}
               inputref={ref => {  inputRef = ref }}
               inputProps={{ onSubmitEditing: () => { dismissSuggestion(); }, returnKeyType: 'next' }}
            />
            {suggestion.showSuggestion &&
               <ScrollView keyboardShouldPersistTaps={'always'} nestedScrollEnabled={true}
                  style={[layoutStyle.CustomControlDropDown, props.dropDownStyle || suggestion.suggestionPosition]}>
                  {suggestion.suggestionList}
               </ScrollView>
            }
         </View>
      </View>
   );
}

export default SearchablePicker;