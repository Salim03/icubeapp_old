import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import {func, string, oneOfType, object, array} from 'prop-types';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleColor,
} from '../../Style/GlobalStyle';
export const Button = props => {
  return (
    <TouchableOpacity
      style={[
        styles.LoginButton,
        ApplyStyleColor(
          globalColorObject.Color.Primary,
          globalColorObject.ColorPropetyType.BackgroundColor,
        ),
      ]}
      onPress={props.onPressEvent}>
      <Text
        style={[
          ApplyStyleFontAndSizeAndColor(
            globalFontObject.Font.Bold,
            globalFontObject.Size.mediam.mxxl,
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.Color,
          ),
        ]}>
        {props.value}
      </Text>
    </TouchableOpacity>
  );
};

// Prop types defenition
Button.propTypes = {
  value: string.isRequired,
  onPressEvent: func,
  // wrapperStyle: oneOfType([object, array]),
  // textStyle: oneOfType([object, array]),
};

const styles = StyleSheet.create({
  LoginButton: {
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    height: 45,
  },
});
