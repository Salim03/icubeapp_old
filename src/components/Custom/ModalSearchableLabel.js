import React, {useState, useEffect, useRef} from 'react';
import {Modal, View, Text, FlatList, StyleSheet} from 'react-native';
import Ripple from 'react-native-material-ripple';
import {BlurView} from '@react-native-community/blur';
import {string, func, object, array, oneOfType, number} from 'prop-types';

import RichTextInput from './RichTextInput';
import {layoutStyle} from '../Layout/Layout';
import {dismissKeyboard} from '../../Helpers/HelperMethods';
import {globalFontObject, ApplyStyleFontAndSize} from '../../Style/GlobalStyle';

const ModalSearchableLabel = props => {
  const {data, labelProp, valueProp, selectedValue} = props;
  const [searchText, setSearchText] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [selectedValueText, setSelectedValueText] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const modalInputRef = useRef();

  useEffect(() => {
    // getting the label value from the selected code value
    const selectedItem = data.find(itm => itm[valueProp] == selectedValue);
    const selectedItemText = (selectedItem && selectedItem[labelProp]) || '';
    // console.log("Selected Value", selectedItem, selectedItemText);
    // setSearchText(selectedItemText);
    setSelectedValueText(selectedItemText);
  }, [selectedValue, data, valueProp, labelProp]);

  const openModal = () => {
    setShowModal(true);
    // setSearchText(selectedValueText || '');
    prepareSuggestionData('');
    setTimeout(() => modalInputRef.current.focus(), 200);
  };
  const closeModal = () => {
    dismissKeyboard();
    ClearModal();
    setShowModal(false);
    setSuggestions([]);
  };
  const ClearModal = () => {
    handleSearchTextChange('');
  };
  const setModalInputRef = ref => {
    modalInputRef.current = ref;
  };

  const handleSearchTextChange = txt => {
    setSearchText(txt);
    prepareSuggestionData(txt);
  };

  const prepareSuggestionData = txt => {
    setSuggestions(
      txt != ''
        ? data.filter(itm =>
            itm[labelProp].toString().toLowerCase().includes(txt.toLowerCase()),
          ) || []
        : data,
    );
  };

  const handleSearchSubmit = event => {};

  const handleValueSelected = (value, label, index, item) => {
    setSelectedValueText(label);
    closeModal();
    props.onValueSelected(value, label, index, item);
  };

  return (
    <>
      <Ripple
        onPress={openModal}
        style={[
          layoutStyle.FieldContainer,
          layoutStyle.FieldLayout,
          props.fieldWrapperStyle,
        ]}>
        <Text
          style={[
            layoutStyle.FieldInput,
            ApplyStyleFontAndSize(
              globalFontObject.Font.Regular,
              globalFontObject.Size.small.sl,
            ),
            props.inputStyle,
          ]}>
          {selectedValueText || props.placeholder}
        </Text>
      </Ripple>
      {/* suggestion Modal */}
      <Modal visible={showModal} animationType="fade">
        <View style={styles.ModalContainer}>
          <BlurView
            style={styles.blurredBackdrop}
            blurType="dark"
            blurAmount={7}
          />
          <View style={styles.searchWrapper}>
            <View style={styles.searchFieldContainer}>
              <RichTextInput
                placeholder={props.placeholder}
                value={searchText}
                onChangeText={handleSearchTextChange}
                wrapperStyle={{flex: 1}}
                inputref={setModalInputRef}
                inputStyle={{borderBottomWidth: 0}}
                inputProps={{onSubmitEditing: handleSearchSubmit}}
              />
              <Ripple
                onPress={closeModal}
                rippleCentered={true}
                rippleDuration={300}
                rippleContainerBorderRadius={35}>
                <View style={styles.clearModalButton}>
                  <Text style={styles.closeModalButtonText}>&times;</Text>
                </View>
              </Ripple>
            </View>
            <View style={styles.suggestionContainer}>
              <FlatList
                keyboardShouldPersistTaps={'always'}
                contentContainerStyle={styles.FlatListStyle}
                ListHeaderComponent={
                  <View style={{width: '100%', marginTop: 10}}></View>
                }
                data={suggestions}
                renderItem={({item, index}) => (
                  <Ripple
                    onPress={() =>
                      handleValueSelected(
                        item[valueProp],
                        item[labelProp],
                        index,
                        item,
                      )
                    }>
                    <View style={styles.suggestionItem}>
                      <Text
                        style={[
                          styles.suggestionItemText,
                          layoutStyle.AppFont,
                        ]}>
                        {item[labelProp]}
                      </Text>
                    </View>
                  </Ripple>
                )}
                keyExtractor={itm => itm[valueProp].toString()}
              />
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

// prop type defenition
ModalSearchableLabel.propTypes = {
  placeholder: string.isRequired,
  data: array.isRequired,
  labelProp: string.isRequired,
  valueProp: string.isRequired,
  selectedValue: oneOfType([string, number]).isRequired,
  onValueSelected: func.isRequired,
  inputStyle: oneOfType([object, array]),
  fieldWrapperStyle: oneOfType([object, array]),
};

const styles = StyleSheet.create({
  ModalContainer: {
    flex: 1,
    alignItems: 'center',
  },
  blurredBackdrop: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  searchWrapper: {
    marginTop: 30,
    marginBottom: 6,
    flex: 1,
    width: '94%',
    borderRadius: 5,
    overflow: 'hidden',
  },
  searchFieldContainer: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(0,0,0,0.07)',
  },

  clearModalButton: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 55,
    height: 35,
    borderWidth: 2,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.09)',
  },
  closeModalButton: {
    width: 35,
    height: 35,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 35,
  },
  closeModalButtonText: {
    transform: [{scale: 2}],
    color: 'gray',
    lineHeight: 18,
  },
  suggestionContainer: {
    flex: 1,
    width: '100%',
  },
  FlatListStyle: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    overflow: 'hidden',
    backgroundColor: 'white',
  },

  suggestionItemText: {
    color: '#4d5156',
    padding: 13,
  },
});

export default ModalSearchableLabel;
export {styles as MSPStyles};
