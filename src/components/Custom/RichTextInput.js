import React, {useState, useRef, useLayoutEffect} from 'react';
import {View, TextInput, Animated, Easing, StyleSheet} from 'react-native';
import {func, any, bool, oneOfType, object, array, string} from 'prop-types';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
} from '../../Style/GlobalStyle';
import {layoutStyle} from '../Layout/Layout';
import {isnull} from '../../Helpers/HelperMethods';

const RichTextInput = props => {
  let labelVisible = useRef(false);
  let labelOpacity = useRef(new Animated.Value(0));
  let inputRef = useRef();
  const [labelColor, setLabelColor] = useState(
    globalColorObject.Color.GrayColor,
  );

  useLayoutEffect(() => {
    // console.log(
    //   'typeof : ',
    //   typeof props.value,
    //   ' value : ',
    //   props.value,
    //   ' curr : ',
    //   labelVisible.current,
    // );
    if (
      (typeof props.value === 'string' && isnull(props.value, '') !== '') ||
      (typeof props.value === 'number' && isnull(props.value, 0) !== 0)
    ) {
      showLabel();
    } else {
      hideLabel();
    }
  }, [props.value]);

  const showLabel = () => {
    //console.log('call layout for show : ', props.placeholder);
    labelVisible.current = true;
    // console.log("Showing Label");
    Animated.timing(labelOpacity.current, {
      toValue: 1,
      duration: 100,
      easing: Easing.linear,
      useNativeDriver: true,
      // easing: Easing.elastic(2)
    }).start();
  };

  const hideLabel = () => {
    //console.log('call layout for hide : ', props.placeholder);
    labelVisible.current = false;
    // console.log("Hiding Label");
    Animated.timing(labelOpacity.current, {
      toValue: 0,
      duration: 60,
      useNativeDriver: true,
    }).start();
  };

  const changeLabelColorBlue = () => {
    //console.log('focus color');
    setLabelColor(globalColorObject.Color.Primary);
    props.onFocus && props.onFocus();
  };
  const changeLabelColorGray = () => {
    setLabelColor(globalColorObject.Color.GrayColor);
    props.onBlur && props.onBlur(props.value);
  };

  const setRef = ref => {
    inputRef = ref;
    props.inputref && props.inputref(ref);
  };

  const handleTextChange = txt => {
    props.onChangeText(txt);
  };

  const handleKeyPress = () => {
    const txt = props.value;
    if (txt !== '' && !labelVisible.current) {
      showLabel();
    } else if (txt === '' && labelVisible.current) {
      hideLabel();
    }
  };

  const labelPosition = labelOpacity.current.interpolate({
    inputRange: [0, 1],
    outputRange: [7, -1],
  });

  const animatedLabelStyle = [
    styles.label,
    ApplyStyleFontAndSize(
      globalFontObject.Font.Bold,
      globalFontObject.Size.small.sl,
    ),
    {
      opacity: labelOpacity.current,
      transform: [{translateY: labelPosition}],
    },
  ];

  return (
    <View
      style={
        props.customControl
          ? props.wrapperStyle
          : [
              layoutStyle.FieldContainer,
              layoutStyle.FieldLayout,
              props.wrapperStyle,
            ]
      }>
      {props.isShowAnimateText && (
        <Animated.Text
          style={[
            animatedLabelStyle,
            {color: labelColor || globalColorObject.Color.GrayColor},
          ]}>
          {props.placeholder}
        </Animated.Text>
      )}
      <TextInput
        style={
          props.rawStyle
            ? props.rawStyle
            : [
                layoutStyle.FieldInput,
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sxl,
                ),
                props.inputStyle,
              ]
        }
        value={(typeof props.value === 'number' && props.value == 0
          ? ''
          : props.value || ''
        ).toString()}
        onChangeText={handleTextChange}
        onKeyPress={handleKeyPress}
        autoCorrect={false}
        autoCapitalize="none"
        placeholder={props.placeholder}
        onFocus={changeLabelColorBlue}
        onBlur={changeLabelColorGray}
        {...(props.inputProps || [])}
        ref={setRef}
      />
    </View>
  );
};

// Default props
RichTextInput.defaultProps = {
  onChangeText: () => {}, // an empty function just to avoid errors if function is not passed on custom controls
  isShowAnimateText: true,
};

// Prop types defenition
RichTextInput.propTypes = {
  value: any,
  placeholder: string.isRequired,
  onChangeText: func,
  onFocus: func,
  onBlur: func,
  inputref: func,
  customControl: bool,
  isShowAnimateText: bool,
  wrapperStyle: oneOfType([object, array]),
  rawStyle: oneOfType([object, array]),
  inputStyle: oneOfType([object, array]),
  inputProps: object, // to get other desired props to the TextInput Element
};

const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    left: 5,
  },
});

export default RichTextInput;
