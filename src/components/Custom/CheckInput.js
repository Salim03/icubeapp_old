import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {func, bool, oneOfType, object, array, string} from 'prop-types';
import CheckBox from '@react-native-community/checkbox';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
} from '../../Style/GlobalStyle';
import {layoutStyle} from '../Layout/Layout';

const CheckInput = props => {
  const handleValueChange = value => {
    props.onValueChange(value);
  };
  return (
    <View
      style={
        props.customControl
          ? props.wrapperStyle
          : [
              layoutStyle.FieldContainer,
              layoutStyle.FieldLayout,
              props.wrapperStyle,
            ]
      }>
      <View
        style={[
          layoutStyle.FieldInput,
          {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          },
        ]}>
        {props.placeholder != '' && (
          <Text
            numberOfLines={1}
            style={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.BlackColor,
              ),
            ]}>
            {props.placeholder}
          </Text>
        )}
        <CheckBox
          disabled={props.disabled}
          // fieldWrapperStyle={{width: '48%'}}
          onValueChange={handleValueChange}
          value={props.value}
          tintColors={{
            true: globalColorObject.Color.Primary,
            false: globalColorObject.Color.GrayColor,
          }}
        />
      </View>
    </View>
  );
};

// Default props
CheckInput.defaultProps = {
  disabled: false,
  onValueChange: () => {}, // an empty function just to avoid errors if function is not passed on custom controls
};

// Prop types defenition
CheckInput.propTypes = {
  disabled: bool,
  value: bool.isRequired,
  placeholder: string.isRequired,
  onValueChange: func,
  customControl: bool,
  wrapperStyle: oneOfType([object, array]),
  inputStyle: oneOfType([object, array]),
};

const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    left: 5,
  },
});

export default CheckInput;
