import React, {useState, useEffect} from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import {getResponsiveLayout} from '../../Helpers/HelperMethods';
import {func, string} from 'prop-types';

import {
  globalColorObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
  globalFontObject,
} from '../../Style/GlobalStyle';

const initialState = {
  isPotrait: null,
};

let responsiveWidth,
  responsiveMiniWidth,
  responsiveInputFont,
  responsiveLabelFont,
  responsiveCardItemWidth,
  responsiveCardFixedItemWidth,
  responsiveFieldContainerCount;

const LayoutWrapper = props => {
  const [layout, setLayout] = useState(initialState);

  useEffect(() => {
    calculateResponsiveLayout();
  }, []);

  const calculateResponsiveLayout = () => {
    const {
      dynamicWidth,
      dynamicMiniWidth,
      dynamicInputFont,
      dynamicLabelFont,
      dynamicCardItemWidth,
      dynamicCardFixedItemWidth,
      orientationChanged,
      isPotrait,
      dynamicFieldContainerCount,
    } = getResponsiveLayout(layout.isPotrait);
    // console.log(width, dynamicWidth, orientationChanged, isPotrait);
    if (orientationChanged) {
      responsiveWidth = dynamicWidth;
      responsiveFieldContainerCount = dynamicFieldContainerCount;
      responsiveMiniWidth = dynamicMiniWidth;
      responsiveInputFont = dynamicInputFont;
      responsiveLabelFont = dynamicLabelFont;
      responsiveCardItemWidth = dynamicCardItemWidth;
      responsiveCardFixedItemWidth = dynamicCardFixedItemWidth;
      setLayout({isPotrait}); // width, dynamicWidth,
      props.onLayoutChanged && props.onLayoutChanged();
    }
  };

  return (
    <>
      <SafeAreaView
        onLayout={calculateResponsiveLayout}
        style={[
          {flex: 1},
          props.backgroundColor && {backgroundColor: props.backgroundColor},
        ]}>
        {props.children}
      </SafeAreaView>
    </>
  );
};

LayoutWrapper.propTypes = {
  onLayoutChanged: func.isRequired,
  backgroundColor: string,
};

export default LayoutWrapper;

const BorderBottomWidth = 2;

export const layoutStyle = StyleSheet.create({
  get FieldLayout() {
    return {width: `${(responsiveWidth || 100) - 2}%`};
  },
  get FieldContainerCount() {
    return responsiveFieldContainerCount || 1;
  },
  get miniFieldLayout() {
    return {width: `${responsiveMiniWidth || 33}%`};
  },
  get FieldInputFont() {
    return {fontSize: responsiveInputFont || 14};
  },
  get FieldLabelFont() {
    return {fontSize: responsiveLabelFont || 12};
  },
  get AppFont() {
    return {fontSize: responsiveInputFont || 14};
  },
  ScrollContentWrapper: {
    paddingTop: 10,
    paddingBottom: 20,
    // paddingHorizontal: 8,
    flexDirection: 'row',
    flexWrap: 'wrap',
    zIndex: 0,
  },
  FieldContainer: {
    marginHorizontal: 4,
    marginVertical: 2.5,
    overflow: 'visible',
    padding: 0,
  },
  PickerContainer: {
    borderBottomWidth: BorderBottomWidth,
    borderBottomColor: globalColorObject.Color.Lightprimary,
  },
  FieldLabel: {
    flex: 1,
    alignSelf: 'center',
    fontSize: globalFontObject.Size.small.sl,
    position: 'absolute',
    top: 0,
    left: 5,
    color: globalColorObject.Color.GrayColor,
    fontFamily: globalFontObject.Font.Bold,
  },
  FieldInput: {
    width: '100%',
    fontSize: globalFontObject.Size.small.sxl,
    flex: 1,
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
    paddingBottom: 3,
    borderBottomWidth: BorderBottomWidth,
    borderBottomColor: globalColorObject.Color.Lightprimary,
    fontFamily: globalFontObject.Font.Regular,
  },
  FieldInputMultiline: {
    flex: 2,
    alignSelf: 'center',
    textAlign: 'left',
    borderBottomWidth: BorderBottomWidth,
    borderBottomColor: globalColorObject.Color.Lightprimary,
  },
  CustomLabel: {
    flex: 1,
    alignSelf: 'center',
    fontSize: globalFontObject.Size.small.sm,
    position: 'absolute',
    top: 0,
    left: 5,
    color: 'gray',
  },
  CustomInput: {
    // flex: 1,
    marginTop: 10,
    height: 40,
    paddingBottom: 3,
    borderBottomWidth: BorderBottomWidth,
    borderBottomColor: globalColorObject.Color.LightGrayColor,
  },
  CustomControlDropDown: {
    position: 'absolute',
    width: '100%',
    overflow: 'hidden',
    maxHeight: 150,
    backgroundColor: '#f9f9f9',
    elevation: 10,
    zIndex: 50,
  },
  AccordionContentWrapper: {
    paddingTop: 25,
    paddingBottom: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  miniFieldContainer: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
  },
  MiniInputField: {
    height: 35,
    width: 100,
    marginTop: 13,
    backgroundColor: '#f4f4f4',
    borderRadius: 5,
    elevation: 2,
  },
  MiniInputFieldLabel: {
    position: 'absolute',
    top: 0,
    width: 100,
    color: 'gray',
    alignSelf: 'center',
    textAlign: 'left',
    paddingLeft: 5,
  },
});

export const CardStyle = StyleSheet.create({
  get RawCardItemLayout() {
    return responsiveCardItemWidth || 25;
  },
  get CardItemLayout() {
    return {
      width: `${(responsiveCardItemWidth || 25) - 1}%`,
      marginHorizontal: '0.35%',
    };
  },
  get CardFixedItemLayout() {
    return {width: `${responsiveCardFixedItemWidth || 100}%`};
  },
});
