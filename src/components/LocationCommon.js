import React from 'react';
import {StyleSheet, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSearchablePicker from './Custom/ModalSearchablePicker';
import {Request, AlertMessage} from '../Helpers/HelperMethods';

export default class Location extends React.Component {
  constructor() {
    super();
    this.IP = '';
    this.state = {
      Location: 1,
      LocationList: [],
    };
  }
  componentDidMount() {
    this.GetIP();
  }
  GetIP = async () => {
    this.IP = await AsyncStorage.getItem('IP');
    this.HandleLoctiondata();
  };
  HandleLoctiondata = async () => {
    Request.get(`${this.IP}api/common/getactivelocation`)
      .then(res => {
        if (res.data.length) {
          this.setState({
            LocationList: res.data,
          });
        } else {
          AlertMessage('No Location found');
        }
      })
      .catch(err => {
        console.error(err);
        // AlertMessage("Something Went wrong");
      });
  };

  render() {
    FieldLocation = key => (
      <ModalSearchablePicker
        key={key}
        placeholder="Location"
        data={this.state.LocationList}
        labelProp="LocationName"
        valueProp="LocationCode"
        selectedValue={this.state.Location}
        onValueSelected={Location => this.setState({Location})}
        inputStyle={styles.InputStylesData}
      />
    );

    return <View style={styles.LocationStyle}>{FieldLocation()}</View>;
  }
}
const styles = StyleSheet.create({
  LocationStyle: {
    width: '100%',
  },
  InputStylesData: {
    borderBottomColor: '#26B1AD',
  },
});
