import React, { Component } from 'react';
import {
   StyleSheet,
   View,
   Button,
   Text,
   TouchableNativeFeedback
} from 'react-native';

export default class Header extends Component {
   render() {
      let {HeaderData} = this.props;
      return (
         <View style={styles.TitleBar}>
            <View style={styles.PageTitleView}>
               <Text style={styles.PageTitleText}>{HeaderData.title}</Text>
            </View>
            {HeaderData.showButton &&
            <View style={styles.buttonView}>
               <Button title={HeaderData.buttonName} style={styles.Button} />
            </View>}
         </View>
      );
   }
}

const styles = StyleSheet.create({
   TitleBar: {
      backgroundColor: 'white',
      height: 55,
      flexDirection: 'row',
      position: 'absolute',
   },
   PageTitleView: {
      flex: 1,
      justifyContent: "center",
      alignContent: "flex-start",
      paddingLeft: 15,
   },
   PageTitleText: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#333',
   },
   buttonView: {
      width: 125,
      justifyContent: 'center',
      alignItems: 'flex-end',
      paddingRight: 15,
   },
});   