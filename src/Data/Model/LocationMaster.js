import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const LocationMasterSchema_Name = 'LocationMaster';
export const LocationMasterSchema = {
  name: LocationMasterSchema_Name,
  primaryKey: 'Code',
  properties: {
    Code: 'int',
    Name: 'string',
    Alias: 'string',
    Extinct: 'bool',
  },
};
export const LocationMasterDbOption = {
  path: iCubeDB_Name,
  schema: [LocationMasterSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};
