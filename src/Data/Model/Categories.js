import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const CategoriesSchema_Name = 'Categories';
export const CategoriesSchema = {
  name: CategoriesSchema_Name,
  primaryKey: 'Type',
  properties: {
    Type: 'string',
    List: 'CategoriesList[]',
  },
};
export const CategoriesListSchema = {
  name: 'CategoriesList',
  embedded: true, // default: false
  properties: {
    Code: 'int',
    Name: 'string',
  },
};

export const CategoriesDbOption = {
  path: iCubeDB_Name,
  schema: [CategoriesSchema, CategoriesListSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};

export const CategoryCaptionSchema_Name = 'CategoryCaption';

export const CategoryCaptionSchema = {
  name: CategoryCaptionSchema_Name,
  primaryKey: 'Name',
  properties: {
    Name: 'string',
    Caption: 'string',
    isUsed: 'bool',
  },
};

export const CategoryCaptionDbOption = {
  path: iCubeDB_Name,
  schema: [CategoryCaptionSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};
