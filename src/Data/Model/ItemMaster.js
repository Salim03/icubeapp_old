import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';
import {DepartmentSchema} from '../Model/Department';
import {ItemNameListSchema, ItemNameSchema} from '../Model/ItemName';
import {
  CategoriesListSchema,
  CategoriesSchema,
  CategoryCaptionSchema,
} from '../Model/Categories';
import {
  CategoryGroupSchema,
  CategoryGroupListSchema,
  CategoryGroupCategoriesListSchema,
} from '../Model/CategoryGroup';

export const CombinedItemMasterDbOption = {
  path: iCubeDB_Name,
  schema: [
    DepartmentSchema,
    ItemNameSchema,
    ItemNameListSchema,
    CategoryCaptionSchema,
    CategoriesSchema,
    CategoriesListSchema,
    CategoryGroupSchema,
    CategoryGroupListSchema,
    CategoryGroupCategoriesListSchema,
  ],
  schemaVersion: iCubeDB_SchemaVersion,
};
