import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const SupplierSchema_Name = 'Supplier';
export const SupplierSchema = {
  name: SupplierSchema_Name,
  primaryKey: 'Code',
  properties: {
    Code: 'string',
    Name: 'string',
  },
};

export const SupplierDbOption = {
  path: iCubeDB_Name,
  schema: [SupplierSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};
