import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const ItemNameSchema_Name = 'ItemName';
export const ItemNameSchema = {
  name: ItemNameSchema_Name,
  primaryKey: 'GroupCode',
  properties: {
    GroupCode: 'int',
    List: 'ItemNameList[]',
  },
};
export const ItemNameListSchema = {
  name: 'ItemNameList',
  embedded: true, // default: false
  properties: {
    Code: 'string',
    Name: 'string',
  },
};

export const ItemNameDbOption = {
  path: iCubeDB_Name,
  schema: [ItemNameSchema, ItemNameListSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};
