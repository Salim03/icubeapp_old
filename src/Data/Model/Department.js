import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const DepartmentSchema_Name = 'Department';
export const DepartmentSchema = {
  name: DepartmentSchema_Name,
  primaryKey: 'Code',
  properties: {
    Code: 'int',
    Name: 'string',
  },
};

export const DepartmentDbOption = {
  path: iCubeDB_Name,
  schema: [DepartmentSchema],
  schemaVersion: iCubeDB_SchemaVersion,
};
