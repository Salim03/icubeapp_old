import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';
import {SupplierSchema} from './Supplier';
import {LocationMasterSchema} from './LocationMaster';

export const PurchaseOrderSchema_Name = 'PurchaseOrder';
export const PurchaseOrderSchema = {
  name: PurchaseOrderSchema_Name,
  primaryKey: 'No',
  properties: {
    Id: 'string',
    No: 'string',
    Date: 'date',
    DocNo: 'string?',
    DocDate: 'date?',
    Name: 'string',
    Code: 'string',
    Remarks: 'string?',
    Amount: 'double',
    Qty: 'float',
    Status: 'string',
    Location: 'int',
    Details: 'PurchaseOrderDetails[]',
  },
};
export const PurchaseOrderDetailsSchema = {
  name: 'PurchaseOrderDetails',
  embedded: true, // default: false
  properties: {
    Id: 'string',
    GroupCode: 'int',
    ItemName: 'string',
    CatGroupCode: 'string?',
    CatGroupType: 'int?',
    OemCode: 'string?',
    MfgDate: 'date?',
    Cat1: 'string?',
    Cat2: 'string?',
    Cat3: 'string?',
    Cat4: 'string?',
    Cat5: 'string?',
    Cat6: 'string?',
    Cat7: 'string?',
    Cat8: 'string?',
    Cat9: 'string?',
    Cat10: 'string?',
    Cat11: 'string?',
    Cat12: 'string?',
    Cat13: 'string?',
    Cat14: 'string?',
    Cat15: 'string?',
    Cat16: 'string?',
    Qty: 'float',
    Rate: 'float',
    StdRate: 'float',
    WSP: 'float?',
    RSP: 'float?',
    ListedMRP: 'float?',
    OnlineMRP: 'float?',
    ImageData: 'PurchaseOrderDetailsImageDataSchema[]',
    SubItemInfo: 'PurchaseOrderDetailsSubItemInfo[]',
  },
};

export const PurchaseOrderDetailsImageDataSchema = {
  name: 'PurchaseOrderDetailsImageDataSchema',
  embedded: true, // default: false
  properties: {
    Path: 'string',
    // Image: 'string',
  },
};

export const PurchaseOrderDetailsSubItemInfoSchema = {
  name: 'PurchaseOrderDetailsSubItemInfo',
  embedded: true, // default: false
  properties: {
    Cat1: 'string?',
    Cat2: 'string?',
    Cat3: 'string?',
    Cat4: 'string?',
    Cat5: 'string?',
    Cat6: 'string?',
    Cat7: 'string?',
    Cat8: 'string?',
    Cat9: 'string?',
    Cat10: 'string?',
    Cat11: 'string?',
    Cat12: 'string?',
    Cat13: 'string?',
    Cat14: 'string?',
    Cat15: 'string?',
    Cat16: 'string?',
    Qty: 'float',
    Rate: 'float',
    StdRate: 'float',
    WSP: 'float?',
    RSP: 'float?',
    ListedMRP: 'float?',
    OnlineMRP: 'float?',
  },
};

export const PurchaseOrderDbOption = {
  path: iCubeDB_Name,
  schema: [
    PurchaseOrderSchema,
    PurchaseOrderDetailsSchema,
    PurchaseOrderDetailsImageDataSchema,
    PurchaseOrderDetailsSubItemInfoSchema,
  ],
  schemaVersion: iCubeDB_SchemaVersion,
};

export const PurchaseOrderWithSupplierDbOption = {
  path: iCubeDB_Name,
  schema: [
    LocationMasterSchema,
    PurchaseOrderSchema,
    PurchaseOrderDetailsSchema,
    PurchaseOrderDetailsImageDataSchema,
    PurchaseOrderDetailsSubItemInfoSchema,
    SupplierSchema,
  ],
  schemaVersion: iCubeDB_SchemaVersion,
};
