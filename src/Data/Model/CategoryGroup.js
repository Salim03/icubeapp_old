import {iCubeDB_Name, iCubeDB_SchemaVersion} from '../DB/DbHelper';

export const CategoryGroupSchema_Name = 'CategoryGroup';
export const CategoryGroupSchema = {
  name: CategoryGroupSchema_Name,
  primaryKey: 'Type',
  properties: {
    Type: 'string',
    List: 'CategoryGroupList[]',
  },
};
export const CategoryGroupListSchema = {
  name: 'CategoryGroupList',
  embedded: true, // default: false
  properties: {
    Code: 'string',
    Name: 'string',
    List: 'CategoryGroupCategoriesList[]',
  },
};
export const CategoryGroupCategoriesListSchema = {
  name: 'CategoryGroupCategoriesList',
  embedded: true, // default: false
  properties: {
    Code: 'int',
    Name: 'string',
    Sequence: 'int',
  },
};
export const CategoryGroupDbOption = {
  path: iCubeDB_Name,
  schema: [
    CategoryGroupSchema,
    CategoryGroupListSchema,
    CategoryGroupCategoriesListSchema,
  ],
  schemaVersion: iCubeDB_SchemaVersion,
};
