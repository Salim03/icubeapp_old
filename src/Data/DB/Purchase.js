import Realm from 'realm';
import {SupplierSchema_Name, SupplierDbOption} from '../Model/Supplier';
import {
  LocationMasterSchema_Name,
  LocationMasterDbOption,
} from '../Model/LocationMaster';
import {DepartmentSchema_Name, DepartmentDbOption} from '../Model/Department';
import {ItemNameSchema_Name, ItemNameDbOption} from '../Model/ItemName';
import {
  CategoriesSchema_Name,
  CategoriesDbOption,
  CategoryCaptionDbOption,
  CategoryCaptionSchema_Name,
} from '../Model/Categories';
import {CloseDBConnection} from '../DB/DbHelper';
import {
  PurchaseOrderSchema_Name,
  PurchaseOrderWithSupplierDbOption,
  PurchaseOrderDbOption,
} from '../Model/PurchaseOrder';
import {
  CategoryGroupSchema_Name,
  CategoryGroupDbOption,
} from '../Model/CategoryGroup';
import {CombinedItemMasterDbOption} from '../Model/ItemMaster';
import {isnull} from '../../Helpers/HelperMethods';

export const GetNewUUID = () => {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
    /[xy]/g,
    function (c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
    },
  );
  return uuid;
};

//#region Save Data

export const SaveLocation = LocationData => {
  return new Promise((resolve, reject) => {
    if (LocationData.length > 0) {
      Realm.open(LocationMasterDbOption)
        .then(realm => {
          realm.write(() => {
            LocationData.forEach(obj => {
              realm.create(LocationMasterSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveLocation : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveSupplier = SupplierData => {
  return new Promise((resolve, reject) => {
    if (SupplierData.length > 0) {
      Realm.open(SupplierDbOption)
        .then(realm => {
          realm.write(() => {
            // realm.delete(realm.objects(SupplierSchema_Name));
            SupplierData.forEach(obj => {
              realm.create(SupplierSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveSupplier : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveDepartment = DepartmentData => {
  return new Promise((resolve, reject) => {
    if (DepartmentData.length > 0) {
      Realm.open(DepartmentDbOption)
        .then(realm => {
          realm.write(() => {
            DepartmentData.forEach(obj => {
              realm.create(DepartmentSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveDepartment : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveItemName = ItemNameData => {
  return new Promise((resolve, reject) => {
    if (ItemNameData.length > 0) {
      Realm.open(ItemNameDbOption)
        .then(realm => {
          realm.write(() => {
            ItemNameData.forEach(obj => {
              realm.create(ItemNameSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveItemName : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveCategoryGroup = CategoryGroupData => {
  return new Promise((resolve, reject) => {
    if (CategoryGroupData.length > 0) {
      Realm.open(CategoryGroupDbOption)
        .then(realm => {
          realm.write(() => {
            CategoryGroupData.forEach(obj => {
              realm.create(CategoryGroupSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveCategoryGroup : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveCategoryCaption = CategorCaptionData => {
  return new Promise((resolve, reject) => {
    if (CategorCaptionData.length > 0) {
      Realm.open(CategoryCaptionDbOption)
        .then(realm => {
          realm.write(() => {
            CategorCaptionData.forEach(obj => {
              realm.create(CategoryCaptionSchema_Name, obj, 'modified');
            });
          });
          // console.log(
          //   'SaveCategoryCaption : ',
          //   JSON.stringify(realm.objects(CategoryCaptionSchema_Name)),
          // );
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveCategoryCaption : ', err);
          reject('Failed');
        });
    }
  });
};

export const SaveCategories = CategoriesData => {
  return new Promise((resolve, reject) => {
    if (CategoriesData.length > 0) {
      Realm.open(CategoriesDbOption)
        .then(realm => {
          realm.write(() => {
            CategoriesData.forEach(obj => {
              realm.create(CategoriesSchema_Name, obj, 'modified');
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('SaveCategories : ', err);
          reject('Failed');
        });
    }
  });
};

export const SavePurchaseOrder = PurchaseOrderData => {
  return new Promise((resolve, reject) => {
    if (PurchaseOrderData && PurchaseOrderData.length > 0) {
      Realm.open(PurchaseOrderDbOption)
        .then(realm => {
          let createditem = {};
          realm.write(() => {
            // realm.delete(realm.objects(PurchaseOrderSchema_Name));
            PurchaseOrderData.forEach(obj => {
              createditem = realm.create(
                PurchaseOrderSchema_Name,
                obj,
                'modified',
              );
            });
          });
          // console.log('save Purchase : ', JSON.stringify(createditem));
          resolve({
            status: 'Saved',
            data: {Id: createditem.Id, No: createditem.No},
          });
          CloseDBConnection(realm);
          // return 'Saved';
        })
        .catch(err => {
          reject({status: 'Failed', data: err});
          console.error('SavePurchaseOrder : ', err);
        });
    }
  });
};

//#endregion

//#region Flush Data

export const FlushSupplier = () => {
  return new Promise((resolve, reject) => {
    Realm.open(SupplierDbOption)
      .then(realm => {
        realm.write(() => {
          realm.delete(realm.objects(SupplierSchema_Name));
        });
        resolve('Saved');
        CloseDBConnection(realm);
      })
      .catch(err => {
        console.error('FlushSupplier : ', err);
        reject('Failed');
      });
  });
};

export const FlushDepartment = () => {
  return new Promise((resolve, reject) => {
    Realm.open(DepartmentDbOption)
      .then(realm => {
        realm.write(() => {
          realm.delete(realm.objects(DepartmentSchema_Name));
        });
        resolve('Saved');
        CloseDBConnection(realm);
      })
      .catch(err => {
        console.error('FlushDepartment : ', err);
        reject('Failed');
      });
  });
};

export const FlushItemName = () => {
  return new Promise((resolve, reject) => {
    Realm.open(ItemNameDbOption)
      .then(realm => {
        realm.write(() => {
          realm.delete(realm.objects(ItemNameSchema_Name));
        });
        resolve('Saved');
        CloseDBConnection(realm);
      })
      .catch(err => {
        console.error('FlushItemName : ', err);
        reject('Failed');
      });
  });
};

export const FlushCategoryGroup = () => {
  return new Promise((resolve, reject) => {
    Realm.open(CategoryGroupDbOption)
      .then(realm => {
        realm.write(() => {
          realm.delete(realm.objects(CategoryGroupSchema_Name));
        });
        resolve('Saved');
        CloseDBConnection(realm);
      })
      .catch(err => {
        console.error('FlushCategoryGroup : ', err);
        reject('Failed');
      });
  });
};

export const FlushCategories = CatTypeNameList => {
  return new Promise((resolve, reject) => {
    if (CatTypeNameList != '') {
      Realm.open(CategoriesDbOption)
        .then(realm => {
          realm.write(() => {
            CatTypeNameList.split(',').forEach(cat_TypeName => {
              if (isnull(cat_TypeName, '') != '') {
                realm.delete(
                  realm
                    .objects(CategoriesSchema_Name)
                    .filtered(`Type == '${cat_TypeName}'`),
                );
              }
            });
          });
          resolve('Saved');
          CloseDBConnection(realm);
        })
        .catch(err => {
          console.error('FlushCategories : ', err);
          reject('Failed');
        });
    }
  });
};

//#endregion

export const DeletePurchaseOrder = passPOItem => {
  if (passPOItem) {
    Realm.open(PurchaseOrderDbOption)
      .then(realm => {
        realm.write(() => {
          realm.delete(
            realm.objectForPrimaryKey(PurchaseOrderSchema_Name, passPOItem.No),
          );
        });
        CloseDBConnection(realm);
      })
      .catch(err => console.error('DeletePurchaseOrder : ', err));
  }
};

export const GetAllPurchaseOrderWithSupplierForPO = (
  setReturnData,
  Loccode,
) => {
  Realm.open(PurchaseOrderWithSupplierDbOption)
    .then(realm => {
      let Return_AllSupplier = realm.objects(SupplierSchema_Name).toJSON();
      let Return_AllLocation = realm
        .objects(LocationMasterSchema_Name)
        .toJSON();
      let Return_AllPurchaseOrder = realm
        .objects(PurchaseOrderSchema_Name)
        .filtered(`Location == ${Loccode}`)
        .toJSON()
        .reverse();
      if (Return_AllSupplier.length > 0) {
        Return_AllPurchaseOrder = Return_AllPurchaseOrder.map(item => {
          let get_SuppName = isnull(item.Name, '');
          let getFilterSupplier = Return_AllSupplier.find(
            element => element.Code == item.Code,
          );
          if (getFilterSupplier) {
            get_SuppName = isnull(getFilterSupplier.Name, '');
          }
          item.Name = get_SuppName;
          return item;
        });
      }
      setReturnData({
        SupplierList: Return_AllSupplier,
        POList: Return_AllPurchaseOrder,
        Location: Return_AllLocation,
      });
      CloseDBConnection(realm);
    })
    .catch(err =>
      console.error('GetAllPurchaseOrderWithSupplierForPO : ', err),
    );
};

// export const GetAllSupplier = setSupplierList => {
//   Realm.open(SupplierDbOption)
//     .then(realm => {
//       setSupplierList(realm.objects(SupplierSchema_Name).toJSON());
//       CloseDBConnection(realm);
//     })
//     .catch(err => console.error('GetAllSupplier : ', err));
// };

// export const GetAllPurchaseOrder = setPurchaseOrderList => {
//   Realm.open(PurchaseOrderDbOption)
//     .then(realm => {
//       let Return_AllPurchaseOrder = realm
//         .objects(PurchaseOrderSchema_Name)
//         .toJSON();
//       setPurchaseOrderList({
//         POList: Return_AllPurchaseOrder,
//         ListSourceData: Return_AllPurchaseOrder,
//       });
//       CloseDBConnection(realm);
//     })
//     .catch(err => console.error('GetAllPurchaseOrder : ', err));
// };

export const GetAllDepartment = setDepartmentList => {
  Realm.open(DepartmentDbOption)
    .then(realm => {
      setDepartmentList(realm.objects(DepartmentSchema_Name).toJSON());
      CloseDBConnection(realm);
    })
    .catch(err => console.error('GetAllDepartment : ', err));
};

export const GetAllItemName = setItemNameList => {
  Realm.open(ItemNameDbOption)
    .then(realm => {
      setItemNameList(realm.objects(ItemNameSchema_Name).toJSON());
      CloseDBConnection(realm);
    })
    .catch(err => console.error('GetAllItemName : ', err));
};

export const GetAllCategoryCaption = setCategoryCaptionList => {
  Realm.open(CategoryCaptionDbOption)
    .then(realm => {
      setCategoryCaptionList(
        realm.objects(CategoryCaptionSchema_Name).toJSON(),
      );
      CloseDBConnection(realm);
    })
    .catch(err => console.error('GetAllCategoryCaption : ', err));
};

export const GetAllCategories = setCategoriesList => {
  Realm.open(CategoriesDbOption)
    .then(realm => {
      setCategoriesList(realm.objects(CategoriesSchema_Name).toJSON());
      CloseDBConnection(realm);
    })
    .catch(err => console.error('GetAllCategories : ', err));
};

export const GetAllCategoryGroup = setCategoryGroupList => {
  Realm.open(CategoryGroupDbOption)
    .then(realm => {
      setCategoryGroupList(realm.objects(CategoryGroupSchema_Name).toJSON());
      CloseDBConnection(realm);
    })
    .catch(err => console.error('GetAllCategoryGroup : ', err));
};

export const GetNextPurchaseOrderNumber = () => {
  // Get the current 'global' time from an API using Promise
  return new Promise((resolve, reject) => {
    Realm.open(PurchaseOrderDbOption)
      .then(realm => {
        let ret_NextNumber = 'OPO0001';
        let get_OrderData = realm.objects(PurchaseOrderSchema_Name);
        if (get_OrderData.length > 0) {
          ret_NextNumber = get_OrderData[get_OrderData.length - 1].No;
          let MaxNumber = parseInt(ret_NextNumber.replace('OPO', '')) + 1;
          ret_NextNumber = 'OPO' + MaxNumber.toString().padStart(4, '0');
        }
        CloseDBConnection(realm);
        resolve(ret_NextNumber);
        return ret_NextNumber;
      })
      .catch(err => reject(err));
  });
};

export const GetAllCombinedItemMasterData = () => {
  return new Promise((resolve, reject) => {
    Realm.open(CombinedItemMasterDbOption)
      .then(realm => {
        let get_Department = realm.objects(DepartmentSchema_Name).toJSON();
        let get_ItemName = realm.objects(ItemNameSchema_Name).toJSON();
        let get_CategoryCaption = realm
          .objects(CategoryCaptionSchema_Name)
          .toJSON();
        let get_CategoryGroup = realm
          .objects(CategoryGroupSchema_Name)
          .toJSON();
        let get_Categories = realm.objects(CategoriesSchema_Name).toJSON();
        let resdata = {
          DepartmentList: get_Department,
          ItemNameList: get_ItemName,
          CategoryCaptionList: get_CategoryCaption,
          CategoryGroupList: get_CategoryGroup,
          CategoriesList: get_Categories,
        };
        CloseDBConnection(realm);
        resolve(resdata);
      })
      .catch(err => {
        reject(err);
        console.error('GetAllCombinedItemMasterData : ', err);
      });
  });
};

export const GetSelectedPurchaseOrder = PO_Number => {
  return new Promise((resolve, reject) => {
    Realm.open(PurchaseOrderDbOption)
      .then(realm => {
        let Return_PurchaseOrder = realm
          .objectForPrimaryKey(PurchaseOrderSchema_Name, PO_Number)
          .toJSON();
        CloseDBConnection(realm);
        resolve(Return_PurchaseOrder);
      })
      .catch(err => {
        console.error('GetAllPurchaseOrderWithSupplierForPO : ', err);
        reject(err);
      });
  });
};

export const UpdateSelectedPurchaseOrderStatus = OrderDetails => {
  return new Promise((resolve, reject) => {
    Realm.open(PurchaseOrderDbOption)
      .then(realm => {
        let Return_List = [];
        realm.write(() => {
          OrderDetails.forEach(obj => {
            let Return_PurchaseOrder = realm.objectForPrimaryKey(
              PurchaseOrderSchema_Name,
              obj.No,
            );
            Return_PurchaseOrder.Status =
              obj.Status == 'Saved' ? 'Exported' : Return_PurchaseOrder.Status;
            Return_List.push(
              realm.create(
                PurchaseOrderSchema_Name,
                Return_PurchaseOrder,
                'modified',
              ),
            );
          });
        });
        let ret_data = JSON.stringify(Return_List);
        CloseDBConnection(realm);
        resolve(ret_data);
      })
      .catch(err => {
        console.error('UpdateSelectedPurchaseOrderStatus : ', err);
        reject(err);
      });
  });
};
