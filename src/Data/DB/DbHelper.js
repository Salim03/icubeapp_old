export const iCubeDB_Name = 'iCubeOffline.realm';
export const iCubeDB_SchemaVersion = 1;
export const CloseDBConnection = realm => {
  if (realm !== null && !realm.isClosed) {
    realm.close();
  }
};
