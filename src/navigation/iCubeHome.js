import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  BackHandler,
  Linking,
} from 'react-native';
import LayoutWrapper from '../components/Layout/Layout';
import {FloatingAction} from 'react-native-floating-action';
import {Confirmation, getOrientation} from '../Helpers/HelperMethods';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalSearchablePicker from '../components/Custom/ModalSearchablePicker';
import {Button} from '../components/Custom/Button';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSizeAndColor,
  ApplyStyleFontAndSize,
  ApplyStyleColor,
} from '../Style/GlobalStyle';
import CheckInAndOut from '../components/Screens/Dashboard/CheckInAndOut';
import {
  LogoutUser,
  ConfirmationWithAllInput,
  getFormsForModuleWithoutFilter,
  GroupValuesByKey,
  Request,
  GetSessionData,
} from '../Helpers/HelperMethods';
import {StackActions, CommonActions} from '@react-navigation/native';

export const MyRouteInfoForSeqNo = {
  // Default
  '0_0_0_0': {route: 'LoginPageNav', param: {}},

  // Retail
  '7_2_0_0': {route: 'ShopNav', param: {}}, // Cash Memo
  '9_4_0_0': {route: 'PeopleCountNav', param: {}}, // Foot Fall

  // Purchase
  '3_1_0_0': {
    route: 'PurchaseNav',
    param: {HeaderTitle: 'Purchase Order', CallFrom: 'PO', FormIndex: '3.1.0'},
  }, // Purchase Order
  '3_2_0_0': {
    route: 'PurchaseNav',
    param: {HeaderTitle: 'Goods Receive', CallFrom: 'GRN', FormIndex: '3.2.0'},
  }, // Goods Receive
  '3_3_0_0': {
    route: 'PurchaseNav',
    param: {HeaderTitle: 'Goods Return', CallFrom: 'GRT', FormIndex: '3.3.0'},
  }, // Goods Return
  '3_4_0_0': {
    route: 'PurchaseNav',
    param: {
      HeaderTitle: 'Purchase Invoice',
      CallFrom: 'PI',
      FormIndex: '3.4.0',
    },
  }, // Purchase Invoice
  '22_11_0_0': {
    route: 'PurchaseNav',
    param: {HeaderTitle: 'Debit Note', CallFrom: 'DN', FormIndex: '22.11.0'},
  }, // Debit Note
  '3_10_0_0': {
    route: 'BussinessPartner',
    param: {
      HeaderTitle: 'Bussiness Partner',
      CallFrom: '3.10.0',
      FormIndex: '3.10.0',
    },
  }, // Bussiness Partner
  '3_12_0_0': {
    route: 'SupplierHistoryNav',
    param: {
      HeaderTitle: 'Vendor History',
      CallFrom: 'Supplier',
      FormIndex: '3.12.0',
    },
  }, // Vendor History
  '8_1_0_0': {
    route: 'PackageNav',
    param: {
      HeaderTitle: 'Package',
      CallFrom: 'PC',
      FormIndex: '8.1.0',
    },
  }, // Package
  '8_4_0_0': {
    route: 'ReconcillationNav',
    param: {
      HeaderTitle: 'Reconciliation',
      CallFrom: 'PTF',
      FormIndex: '8.4.0',
    },
  }, // Reconciliation

  // Inventory
  // '5_19_0_0': {route: 'PeopleCountNav', param: {}}, // Item Master
  '5_7_0_0': {route: 'BarcodeHistoryNav', param: {}}, // Barcode History
  '5_23_0_0': {route: 'StockAuditNav', param: {callFrom: 'SE'}}, // Stock Entry
  '5_10_0_0': {route: 'LogisticNav', param: {}}, // Logistic
  '19_17_0_0': {route: 'CourierNav', param: {}}, // Courier

  // Wholesale
  '4_5_0_0': {
    route: 'SalesNavigator',
    param: {HeaderTitle: 'Sales Order', CallFrom: 'SO', FormIndex: '4.5.0'},
  }, // Sales Order
  '4_1_0_0': {
    route: 'SalesNavigator',
    param: {
      HeaderTitle: ' Delivery Challan',
      CallFrom: 'DC',
      FormIndex: '4.5.0',
    },
  }, // Delivery Challan
  '4_2_0_0': {
    route: 'SalesNavigator',
    param: {HeaderTitle: 'Sales Return', CallFrom: 'SR', FormIndex: '4.2.0'},
  }, // Sales Return
  '4_3_0_0': {
    route: 'SalesNavigator',
    param: {HeaderTitle: 'Sales Invoice', CallFrom: 'SI', FormIndex: '4.3.0'},
  }, // Sales Invoice
  '22_12_0_0': {
    route: 'SalesNavigator',
    param: {HeaderTitle: 'Credit Note', CallFrom: 'CN', FormIndex: '22.12.0'},
  }, // Credit Note
  '4_12_0_0': {
    route: 'SalesNavigator',
    param: {
      HeaderTitle: 'Sales Quotation',
      CallFrom: 'SQ',
      FormIndex: '4.12.0',
    },
  }, // Sales Quotation

  //Finance
  '22_1_0_0': {
    route: 'TransNav',
    param: {HeaderTitle: 'Contra Voucher', CallFrom: '3', FormIndex: '22.1.0'},
  }, // Contra Voucher
  '22_2_0_0': {
    route: 'TransNav',
    param: {HeaderTitle: 'Payment Voucher', CallFrom: '4', FormIndex: '22.2.0'},
  }, // Payment Voucher
  '22_3_0_0': {
    route: 'TransNav',
    param: {HeaderTitle: 'Receipt Voucher', CallFrom: '5', FormIndex: '22.3.0'},
  }, // Receipt Voucher
};

class ICubeHome extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      LocationList: [],
      // LocationCode: 0,
      isHavingOfflineRights: false,
      forms: {},
      DashboardList: [],
      ActivaTab: 'Home',
      orientation: '',
      Width: '100%',
      Height: '100%',
      Storage: {
        IP: '',
        Token: '',
        RoleID: '',
        DashURL: '',
        Location: '0',
        UserCode: '',
      },
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  InitialSetup = async () => {
    let {
      get_IP,
      get_Token,
      get_RoleId,
      get_DashboardURL,
      get_Location,
      get_UserCode,
    } = await GetSessionData();
    // let [
    //   [, get_IP],
    //   [, get_Token],
    //   [, get_RoleId],
    //   [, get_DashboardURL],
    //   [, get_Location],
    //   [, get_UserCode],
    // ] = await AsyncStorage.multiGet([
    //   'IP',
    //   'Token',
    //   'RoleId',
    //   'DashboardURL',
    //   'LocationCode',
    //   'UserCode',
    // ]);
    let myobject = {};
    myobject.IP = get_IP;
    myobject.Token = get_Token;
    myobject.RoleID = get_RoleId;
    myobject.DashURL = get_DashboardURL;
    myobject.Location = get_Location == null ? 0 : parseInt(get_Location);
    myobject.UserCode = get_UserCode;
    this.setState(
      prevstate => {
        return {
          ...prevstate,
          Storage: myobject,
        };
      },
      () => {
        this.LoadDashboard();
        this.HandleLoctiondata();
      },
    );
  };

  HandleLoctiondata = async () => {
    let uri = `${this.state.Storage.IP}api/common/GetActiveLocationforUser?EmpId=${this.state.Storage.UserCode}`;
    Request.get(uri, this.state.Storage.Token)
      .then(res => {
        if (res.data.length) {
          this.setState({
            LocationList: res.data,
          });
          return;
        } else {
          this.setState({
            LocationList: [],
          });
        }
      })
      .catch(err => {
        this.setState({
          LocationList: [],
        });
        console.error(err);
      });
  };

  ChangeView = () => {
    // console.log('change view');
    const {width, height, isPotrait, orientationChanged} = getOrientation(
      this.state.orientation,
    );
    this.setState({
      orientation: isPotrait ? 'portrait' : 'landscape',
      height: height,
      width: width,
    });
  };

  componentDidMount() {
    this.InitialSetup();
    this.ChangeView();
    // Dimensions.addEventListener('change', () => {
    //   this.ChangeView();
    // });
    // BackHandler.addEventListener(
    //   'hardwareBackPress',
    //   this.handleBackButtonClick,
    // );
    getFormsForModuleWithoutFilter()
      .then(data => {
        const forms = data && GroupValuesByKey(data, 'MODULENAME');
        let get_OfflineForms = data.filter(
          r => r.SeqNo == '3_1_0_0' || r.SeqNo == '4_5_0_0',
        );
        AsyncStorage.setItem('OffLine_Forms', JSON.stringify(get_OfflineForms));
        this.setState({
          forms,
          isHavingOfflineRights: get_OfflineForms.length > 0,
        });
      })
      .catch(() => {
        console.error('error');
      });
  }

  RemoveBackButtonListener() {
    // BackHandler.removeEventListener(
    //   'hardwareBackPress',
    //   this.handleBackButtonClick,
    // );
    // Dimensions.removeEventListener('change', () => {
    //   getOrientation();
    // });
  }

  RestartApp() {
    this.RemoveBackButtonListener();
    LogoutUser();
    this.props.stopsessioninterval && this.props.stopsessioninterval();
    this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
  }
  handleBackButtonClick() {
    ConfirmationWithAllInput(
      'Are you sure want to logout?',
      'Ok',
      'Back',
      () => {
        this.RestartApp();
        // this.RemoveBackButtonListener();
        // LogoutUser();
        // this.props.stopsessioninterval && this.props.stopsessioninterval();
        // this.props.navigation.dispatch(StackActions.replace('LoginPageNav'));
      },
      () => {
        if (this.props.navigation.canGoBack()) {
          this.props.navigation.dispatch(CommonActions.goBack());
        } else {
          this.props.stopsessioninterval && this.props.stopsessioninterval();
          BackHandler.exitApp();
        }
      },
    );
    return true;
  }

  NavigateToDashboard = (form, ReportId) => {
    this.props.navigation.navigate('DDynamicNav', {
      HeaderTitle: form,
      CallFrom: ReportId,
    });
  };

  ActivaTabChange(passtab) {
    this.setState(prevstate => {
      return {
        ...prevstate,
        ActivaTab: passtab,
      };
    });
  }

  LoadDashboard = async () => {
    let uri = `${this.state.Storage.IP}/api/Dashboard/GetMobileReportList?RoleId=${this.state.Storage.RoleID}&LocationCode=${this.state.Storage.Location}`;
    Request.get(uri, this.state.Storage.Token)
      .then(res => {
        if (res.status === 200) {
          // console.log('Dash data : ', res.data);
          if (res.data.length) {
            this.setState({DashboardList: res.data});
          }
        }
      })
      .catch(err => {
        console.error(err);
      });
  };

  NavigateToForm = (seqNo, menuName) => {
    let get_route = MyRouteInfoForSeqNo[seqNo]?.route;
    let get_param = {
      ...MyRouteInfoForSeqNo[seqNo]?.param,
    };
    if (seqNo == '7_2_0_0') {
      get_param = {
        ...MyRouteInfoForSeqNo[seqNo]?.param,
        reStartApp: this.RestartApp.bind(this),
      };
    }
    let old_menuName = get_param.HeaderTitle;
    get_param.HeaderTitle = menuName;
    // console.log(
    //   'selected form : ',
    //   seqNo,
    //   ' old_menuName : ',
    //   old_menuName,
    //   ' new_menuName : ',
    //   get_param.HeaderTitle,
    // );
    if (get_route) {
      // console.log('nav call');
      this.props.navigation.navigate(get_route, get_param);
    }
  };

  refreshLayout = () => this.forceUpdate();

  HandleActionButtonClick = name => {
    if (name === 'Call') {
      Linking.openURL('tel:+918220806806');
    } else if (name === 'WhatsApp') {
      Linking.openURL('whatsapp://send?text=&phone=918220806806');
    } else if (name === 'Jira') {
      Linking.openURL(
        'https://helpdesk.icubesoftware.com/servicedesk/customer/user/requests',
      );
    }
  };
  ActionButtons = [
    {
      text: 'Call',
      icon: require('../assets/images/phn_final.png'),
      name: 'Call',
      color: globalColorObject.Color.Primary,
      buttonSize: 45,
      position: 3,
    },
    {
      text: 'WhatsApp',
      icon: require('../assets/images/whatsapp.logo.png'),
      name: 'WhatsApp',
      color: globalColorObject.Color.Primary,
      buttonSize: 45,
      position: 2,
    },
    {
      text: 'Ticket',
      icon: require('../assets/images/jira__.png'),
      name: 'Jira',
      color: globalColorObject.Color.Primary,
      buttonSize: 45,
      position: 1,
    },
  ];

  NavigateToOffline = () => {
    Confirmation('Are you sure want to go offline ?', () => {
      this.props.stopsessioninterval && this.props.stopsessioninterval();
      this.props.runinDiffType && this.props.runinDiffType('Offline');
    });
  };

  render() {
    let FavIcocn = (
      <>
        {this.state.isHavingOfflineRights && (
          <TouchableOpacity
            style={[
              styles.AddNewInvoice,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => this.NavigateToOffline()}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../assets/images/Offline.png')}
            />
          </TouchableOpacity>
        )}
        {this.state.ActivaTab == 'Dashboard' ? (
          <TouchableOpacity
            style={[
              styles.AddNewInvoice1,
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BackgroundColor,
              ),
            ]}
            onPress={() => {
              this.props.navigation.navigate('AnalyticsReportNav', {
                PrinterName: this.state.PrinterName,
              });
            }}>
            <Image
              style={{width: 30, height: 30}}
              source={require('../assets/images/popup-blue1.png')}
            />
          </TouchableOpacity>
        ) : (
          <FloatingAction
            actions={this.ActionButtons}
            floatingIcon={require('../assets/images/sup.png')}
            onPressItem={this.HandleActionButtonClick}
            color={globalColorObject.Color.Primary}
            iconWidth={25}
            iconHeight={25}
            animated={true}
            visible={this.state.ActivaTab != 'Profile'}
            distanceToEdge={{vertical: 150, horizontal: 10}}
          />
        )}
      </>
    );
    let binddata = (
      <View
        style={[
          ApplyStyleColor(
            globalColorObject.Color.oppPrimary,
            globalColorObject.ColorPropetyType.BackgroundColor,
          ),
          {flex: 1, flexDirection: 'row', justifyContent: 'center'},
        ]}>
        <Text>No Data</Text>
      </View>
    );
    if (this.state.ActivaTab == 'Home') {
      let homedata = Object.keys(this.state.forms || {}).map((tem, i) => {
        return (
          <View
            key={'grp_mod_' + tem}
            style={{
              flexDirection: 'column',
              paddingBottom: 5,
            }}>
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.mm,
                  globalColorObject.Color.Primary,
                  globalColorObject.ColorPropetyType.Color,
                ),
                {
                  textAlign: 'left',
                  paddingLeft: 5,
                  textDecorationLine: 'underline',
                },
              ]}>
              {tem}
            </Text>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingTop: 5,
              }}>
              {this.state.forms[tem].map(head => {
                return (
                  <TouchableOpacity
                    key={'inm_' + head.SeqNo}
                    style={styles.FormContainer}
                    onPress={this.NavigateToForm.bind(
                      this,
                      head.SeqNo,
                      head.MENUNAME,
                    )}>
                    <Image
                      source={{
                        uri: `https://images.icubesoftware.com/iCubeMobileApp/Icons/Modules/${head.SeqNo}.png`,
                        // uri: `http://192.168.10.93:8094/iCubeIconPackage/${head.SeqNo}.png`,
                      }}
                      // source={require('../assets/images/Home/Setting/ip.png')}
                      style={styles.FormImages}
                    />
                    <Text
                      style={[
                        ApplyStyleFontAndSize(
                          globalFontObject.Font.Regular,
                          globalFontObject.Size.small.sl,
                        ),
                        styles.FormLabel,
                      ]}>
                      {head.MENUNAME}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </View>
        );
      });
      binddata = (
        <ScrollView
          style={[
            ApplyStyleColor(
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
            {
              height: this.state.height ? this.state.height - 45 : '85%',
            },
          ]}>
          {homedata}
        </ScrollView>
      );
    } else if (this.state.ActivaTab == 'Profile') {
      let profiledata = (
        <ScrollView
          keyboardShouldPersistTaps="always"
          nestedScrollEnabled={true}
          style={{flex: 1}}>
          <ModalSearchablePicker
            placeholder="Location"
            fieldWrapperStyle={{width: '98%'}}
            data={this.state.LocationList}
            labelProp="LocationName"
            valueProp="LocationCode"
            selectedValue={this.state.Storage.Location}
            inputStyle={[
              ApplyStyleFontAndSizeAndColor(
                globalFontObject.Font.Regular,
                globalFontObject.Size.small.sxl,
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.BorderBottomColor,
              ),
              ApplyStyleColor(
                globalColorObject.Color.Primary,
                globalColorObject.ColorPropetyType.Color,
              ),
            ]}
            onValueSelected={LocationCode => {
              this.setState(prevState => ({
                Storage: {...prevState.Storage, Location: LocationCode},
              }));
              AsyncStorage.setItem('LocationCode', LocationCode.toString());
            }}
          />
        </ScrollView>
      );
      binddata = (
        <View
          style={[
            ApplyStyleColor(
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
            {
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'flex-end',
              paddingHorizontal: 5,
            },
          ]}>
          {profiledata}
          <View
            style={{flexDirection: 'row', width: '100%', paddingBottom: 25}}>
            <View style={{width: '50%'}}>
              <CheckInAndOut />
            </View>
            <View style={{width: '50%'}}>
              <Button
                value="Logout"
                onPressEvent={() => {
                  this.RemoveBackButtonListener();
                  LogoutUser();
                  this.props.stopsessioninterval &&
                    this.props.stopsessioninterval();
                  this.props.navigation.dispatch(
                    StackActions.replace('LoginPageNav'),
                  );
                }}
              />
            </View>
          </View>
        </View>
      );
      FavIcocn = <></>;
    } else if (this.state.ActivaTab == 'Dashboard') {
      let dashdata = this.state.DashboardList.map((tem, i) => {
        return (
          <TouchableOpacity
            key={'dashinm_' + tem.Name}
            style={[styles.FormContainer, {width: 120}]}
            onPress={this.NavigateToDashboard.bind(this, tem.Name, tem.ID)}>
            <Image
              source={{
                uri: tem.Icon,
              }}
              // source={require('../assets/images/Home/Setting/ip.png')}
              style={[styles.FormImages, {width: 40, height: 40}]}
            />
            <Text
              style={[
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Regular,
                  globalFontObject.Size.small.sxl,
                ),
                styles.FormLabel,
              ]}>
              {tem.Name}
            </Text>
          </TouchableOpacity>
        );
      });

      binddata = (
        <View
          style={[
            ApplyStyleColor(
              globalColorObject.Color.oppPrimary,
              globalColorObject.ColorPropetyType.BackgroundColor,
            ),
            {
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              flexWrap: 'wrap',
              paddingHorizontal: 10,
            },
          ]}>
          {dashdata}
        </View>
      );
      // console.log('dash data : ', binddata);
    }

    return (
      <LayoutWrapper
        backgroundColor={globalColorObject.Color.oppPrimary}
        onLayoutChanged={this.refreshLayout}>
        {binddata}
        <View
          style={{
            flexDirection: 'row',
            height: this.state.height ? 45 : '15%',
            // paddingBottom: 2,
            justifyContent: 'center',
            backgroundColor: globalColorObject.Color.oppPrimary,
            // borderTopWidth: 2,
            borderTopColor: globalColorObject.Color.Lightprimary,
          }}>
          <TouchableOpacity
            style={[
              styles.TabContainer,
              ApplyStyleColor(
                this.state.ActivaTab == 'Dashboard'
                  ? globalColorObject.Color.Primary
                  : globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BorderTopColor,
              ),
            ]}
            onPress={this.ActivaTabChange.bind(this, 'Dashboard')}>
            <Image
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/3867/3867620.png',
              }}
              style={[styles.TabImageContainer]}
            />
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  this.state.ActivaTab == 'Dashboard'
                    ? globalFontObject.Font.Bold
                    : globalFontObject.Font.Regular,
                  globalFontObject.Size.small.ss,
                  this.state.ActivaTab == 'Dashboard'
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.FormLabel,
              ]}>
              Report
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.TabContainer,
              ApplyStyleColor(
                this.state.ActivaTab == 'Task'
                  ? globalColorObject.Color.Primary
                  : globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BorderTopColor,
              ),
            ]}
            onPress={this.ActivaTabChange.bind(this, 'Task')}>
            <Image
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/2285/2285516.png',
              }}
              style={[styles.TabImageContainer]}
            />
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  this.state.ActivaTab == 'Task'
                    ? globalFontObject.Font.Bold
                    : globalFontObject.Font.Regular,
                  globalFontObject.Size.small.ss,
                  this.state.ActivaTab == 'Task'
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.FormLabel,
              ]}>
              Task
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.TabContainer,
              ApplyStyleColor(
                this.state.ActivaTab == 'Home'
                  ? globalColorObject.Color.Primary
                  : globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BorderTopColor,
              ),
            ]}
            onPress={this.ActivaTabChange.bind(this, 'Home')}>
            <Image
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1946/1946488.png',
              }}
              style={styles.TabImageContainer}
            />
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  this.state.ActivaTab == 'Home'
                    ? globalFontObject.Font.Bold
                    : globalFontObject.Font.Regular,
                  globalFontObject.Size.small.ss,
                  this.state.ActivaTab == 'Home'
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.FormLabel,
              ]}>
              Home
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.TabContainer,
              ApplyStyleColor(
                this.state.ActivaTab == 'Notification'
                  ? globalColorObject.Color.Primary
                  : globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BorderTopColor,
              ),
            ]}
            onPress={this.ActivaTabChange.bind(this, 'Notification')}>
            <Image
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1827/1827422.png',
              }}
              style={styles.TabImageContainer}
            />
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  this.state.ActivaTab == 'Notification'
                    ? globalFontObject.Font.Bold
                    : globalFontObject.Font.Regular,
                  globalFontObject.Size.small.ss,
                  this.state.ActivaTab == 'Notification'
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.FormLabel,
              ]}>
              Alert
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.TabContainer,
              ApplyStyleColor(
                this.state.ActivaTab == 'Profile'
                  ? globalColorObject.Color.Primary
                  : globalColorObject.Color.oppPrimary,
                globalColorObject.ColorPropetyType.BorderTopColor,
              ),
            ]}
            onPress={this.ActivaTabChange.bind(this, 'Profile')}>
            <Image
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/1077/1077063.png',
              }}
              style={styles.TabImageContainer}
            />
            <Text
              style={[
                ApplyStyleFontAndSizeAndColor(
                  this.state.ActivaTab == 'Profile'
                    ? globalFontObject.Font.Bold
                    : globalFontObject.Font.Regular,
                  globalFontObject.Size.small.ss,
                  this.state.ActivaTab == 'Profile'
                    ? globalColorObject.Color.Primary
                    : globalColorObject.Color.BlackColor,
                  globalColorObject.ColorPropetyType.Color,
                ),
                styles.FormLabel,
              ]}>
              Profile
            </Text>
          </TouchableOpacity>
        </View>
        {FavIcocn}
      </LayoutWrapper>
    );
  }
}

const styles = StyleSheet.create({
  FormImages: {
    width: 30,
    height: 30,
    margin: 5,
    resizeMode: 'stretch',
  },
  AddNewInvoice1: {
    width: 55,
    height: 55,
    position: 'absolute',
    bottom: 150,
    right: 10,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  AddNewInvoice: {
    width: 55,
    height: 55,
    position: 'absolute',
    bottom: 85,
    right: 10,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 60,
  },
  FormLabel: {
    textAlign: 'center',
    // fontWeight: 'bold',
  },
  ListContainer: {
    marginTop: 10,
    // marginLeft:20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  FormContainer: {
    marginBottom: 10,
    padding: 5,
    width: 95,
    alignItems: 'center',
  },
  TabContainer: {
    width: '20%',
    alignItems: 'center',
    borderTopWidth: 3,
  },
  TabImageContainer: {
    width: 24,
    height: 26,
    resizeMode: 'stretch',
  },
});

export default ICubeHome;
