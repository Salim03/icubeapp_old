import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import iCubeNavHome from './iCubeNavHome';
import {iCubeModuleStackNav} from './iCubeModuleStackNav'
/* Default */
import Config from '../components/Config'
import LoginPage from '../components/LoginPage'

/* Dashboard */ 
import DashboardHome from "../components/Screens/Dashboard/DashboardHome";

const Tab = createBottomTabNavigator();

const HomeNavigator = () => {
  return (
    <Tab.Navigator >
      <Tab.Screen name="Tab_Config" component={Config} options={{title: 'Configuration'}} />
      <Tab.Screen name="Tab_LoginPageNav" component={LoginPage} options={{title: 'Sign in'}} />
      <Tab.Screen name="Tab_iCubeNavHome" component={iCubeModuleStackNav} options={{title: 'Home'}} />
      <Tab.Screen name="tab_DashHome" component={DashboardHome} options={{title: 'Dashboard'}} />
    </Tab.Navigator>
  );
};


export { HomeNavigator };
