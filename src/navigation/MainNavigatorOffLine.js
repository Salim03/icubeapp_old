import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

/* Default */
import OffLineHome from '../components/OffLine/OffLineHome';
import {ImportData} from '../components/OffLine/ImportData';
import {Purchase} from '../components/OffLine/Purchase/Purchase';
import {AddPurchase} from '../components/OffLine/Purchase/AddPurchase';
import {ItemMaster} from '../components/OffLine/Purchase/ItemMaster';
import {globalColorObject, globalFontObject} from '../Style/GlobalStyle';

import Config from '../components/Config';
const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: globalColorObject.Color.Primary,
  },
  headerTitleStyle: {
    fontFamily: globalFontObject.Font.Bold,
    fontSize: globalFontObject.Size.mediam.mm,
  },
  headerTintColor: globalColorObject.Color.oppPrimary,
  headerBackTitle: 'Back',
};

class MainNavigatorOffLine extends React.Component {
  constructor(props) {
    super(props);
    this.intervalID = null;
  }

  render() {
    return (
      <Stack.Navigator
        initialRouteName="OffLineiCubeNavHome"
        screenOptions={screenOptionStyle}>
        {/* Default */}
        <Stack.Screen
          name="OffLine_iCubeNavHome"
          options={{headerShown: false}}>
          {props => (
            <OffLineHome
              {...props}
              runinDiffType={this.props.runAppinDiffType}
            />
          )}
        </Stack.Screen>
        <Stack.Screen options={{headerShown: false}} name="Offline_Config">
          {props => (
            <Config {...props} runinDiffType={this.props.runAppinDiffType} />
          )}
        </Stack.Screen>
        <Stack.Screen
          name="OffLine_ImportData"
          component={ImportData}
          options={{title: 'Import'}}
        />
        <Stack.Screen
          name="OffLine_PurchaseOrder"
          component={Purchase}
          options={{title: 'Purchase Order'}}
        />
        <Stack.Screen
          name="OffLine_PurchaseOrderNew"
          component={AddPurchase}
          options={{title: 'Purchase Order'}}
        />
        <Stack.Screen
          name="OffLine_AddItemPurchse"
          component={ItemMaster}
          options={{title: 'Item Master'}}
        />
      </Stack.Navigator>
    );
  }
}

export {MainNavigatorOffLine};
