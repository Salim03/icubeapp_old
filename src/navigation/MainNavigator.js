import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ICubeHome from './iCubeHome';

/* Default */
// import Config from '../components/Config';
import LoginPage from '../components/LoginPage';

import Config from '../components/Screens/Settings/Config';

/* Common */
import VendorHistorySummary from '../components/Screens/Common/VendorHistorySummary';
import VendorHistoryDetail from '../components/Screens/Common/VendorHistoryDetail';
import VendorHistoryItemWise from '../components/Screens/Common/VendorHistoryItemWise';

/* Dashboard */
import DashboardHome from '../components/Screens/Dashboard/DashboardHome';
import DynamicDash from '../components/Screens/Dashboard/DynamicDash';
import AnalyticsReport from '../components/Screens/Dashboard/AnalyticsReport';

/* Retail */
import RetailHome from '../components/Screens/Retail/RetailHome';
import PeopleCount from '../components/Screens/Retail/PeopleCount';
import Shop from '../components/Screens/Retail/Shop';
import Payment from '../components/Screens/Retail/PaymentMethod';
import POSBlutoothPrintList from '../components/Screens/Retail/POSBlutoothPrintList';

/* Purchase */
import ProcurementHome from '../components/Screens/Procurement/ProcurenmentHome';
//Po
import Purchase from '../components/Screens/Procurement/Purchase';
import AddPurchase from '../components/Screens/Procurement/AddPurchase';
//item master
import ItemMaster from '../components/Screens/Procurement/ItemMaster/ItemMaster';
//Masters
import PartyMaster from '../components/Screens/Procurement/PartyMaster';
import AddParty from '../components/Screens/Procurement/AddParty';

/* OffLine Purchase */

//Po
import {Purchase as OffLinePurchase} from '../components/OffLine/Purchase/Purchase';
import {AddPurchase as OffLineAddPurchase} from '../components/OffLine/Purchase/AddPurchase';
//item master
import {ItemMaster as OffLineItemMaster} from '../components/OffLine/Purchase/ItemMaster';
import {ImportData} from '../components/OffLine/ImportData';

/* Inventory */
import InventoryHome from '../components/Screens/Inventory/InventoryHome';
//Stock audit
import StockAudit from '../components/Screens//Inventory/StockAudit';
import AddStockAudit from '../components/Screens//Inventory/AddStockAudit';
//Courier
import Courier from '../components/Screens/Inventory/Courier';
import AddCourier from '../components/Screens/Inventory/AddCourier';
//Logistics
import Logistic from '../components/Screens/Inventory/Logistics';
import AddLogistic from '../components/Screens/Inventory/AddLogisticsIn';
// Price Check
import PriceCheck from '../components/Screens//Inventory/PriceCheck';
//Barcode History
import BarcodeHistory from '../components/Screens//Inventory/BarcodeHistory';
import TransctionDetail from '../components/Screens//Inventory/TranscationDetails';

/* WholeSale */
import SalesHome from '../components/Screens/Sales/SalesHome';
import Sales from '../components/Screens/Sales/Sales';
import AddSales from '../components/Screens/Sales/AddSales';
import ReceivedRatio from '../components/Screens/Common/ReceivedRatio';

/* Accounts */
import FinanceHome from '../components/Screens/Finance/FinanceHome';
import TransactionVoucher from '../components/Screens/Finance/TransactionVoucher';
import AddTransactionVoucher from '../components/Screens/Finance/AddTransactionVoucher';
import Against from '../components/Screens/Finance/Against';

/* Disribution */
import {Package} from '../components/Screens/Disribution/Package';
import AddPackage from '../components/Screens/Disribution/AddPackage';

import Reconsilation from '../components/Screens/Disribution/Reconsilation';
import AddReconsilation from '../components/Screens/Disribution/AddReconsilation';


import {globalColorObject, globalFontObject} from '../Style/GlobalStyle';

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: globalColorObject.Color.Primary,
  },
  headerTitleStyle: {
    fontFamily: globalFontObject.Font.Bold,
    fontSize: globalFontObject.Size.mediam.mm,
  },
  headerTintColor: globalColorObject.Color.oppPrimary,
  headerBackTitle: 'Back',
};

class MainNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.intervalID = null;
  }

  render() {
    return (
      <Stack.Navigator
        initialRouteName={
          this.props.InitialRoutePage
            ? this.props.InitialRoutePage
            : 'LoginPageNav'
        }
        screenOptions={screenOptionStyle}>
        {/* Default */}
        <Stack.Screen
          options={{headerShown: false}}
          name="Config"
          component={Config}
        />
        <Stack.Screen name="LoginPageNav" options={{headerShown: false}}>
          {props => (
            <LoginPage
              {...props}
              startsessioninterval={this.props.runAppSessionInterval}
              stopsessioninterval={this.props.stopAppSessionInterval}
            />
          )}
        </Stack.Screen>
        <Stack.Screen name="iCubeNavHome" options={{headerShown: false}}>
          {props => (
            <ICubeHome
              {...props}
              stopsessioninterval={this.props.stopAppSessionInterval}
              runinDiffType={this.props.runAppinDiffType}
            />
          )}
        </Stack.Screen>
        {/* <Stack.Screen
          options={{headerShown: false}}
          name="LoginPageNav"
          component={LoginPage}
        />
        <Stack.Screen
          name="iCubeNavHome"
          component={iCubeHome}
          options={{title: 'Home'}}
        />*/}
        {/* Dashboard */}
        <Stack.Screen
          name="DashHome"
          component={DashboardHome}
          options={{title: 'Dashboard'}}
        />
        <Stack.Screen
          name="DDynamicNav"
          component={DynamicDash}
          options={{title: 'Dashboard 1'}}
        />
        {/* Retail */}
        <Stack.Screen
          name="RetailHomeNav"
          component={RetailHome}
          options={{title: 'Retail'}}
        />
        <Stack.Screen
          name="PeopleCountNav"
          component={PeopleCount}
          // options={{title: 'Foot Fall'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Foot Fall',
          })}
        />
        <Stack.Screen
          name="ShopNav"
          component={Shop}
          // options={{title: 'Pos'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Pos',
          })}
        />
        <Stack.Screen
          name="PaymentNav"
          component={Payment}
          options={{title: 'Payment'}}
        />
        <Stack.Screen
          name="AnalyticsReportNav"
          component={AnalyticsReport}
          options={{title: 'Fixed'}}
        />
        <Stack.Screen
          name="POSBlutoothPrintListNav"
          component={POSBlutoothPrintList}
          options={{title: 'Reports'}}
        />
        {/* Purchase */}
        <Stack.Screen
          name="PurchaseHome"
          component={ProcurementHome}
          options={{title: 'Purchase'}}
        />
        <Stack.Screen
          name="PurchaseNav"
          component={Purchase}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Purchase',
          })}
        />
        <Stack.Screen
          name="AddPurchaseNav"
          component={AddPurchase}
          options={{title: 'Add Purchase'}}
        />
        <Stack.Screen
          name="AddItemPurchseNav"
          component={ItemMaster}
          options={{title: 'Add Product'}}
        />

        {/* OffLine Purchase Order */}
        <Stack.Screen
          name="OffLine_PurchaseOrder"
          options={{title: 'Offline Order'}}>
          {props => <OffLinePurchase {...props} isOnline={true} />}
        </Stack.Screen>
        <Stack.Screen 
          name="OffLine_PurchaseOrderNew"
          options={{title: 'Add Order'}}>
          {props => <OffLineAddPurchase {...props} isOnline={true} />}
        </Stack.Screen>
        <Stack.Screen 
          name="OffLine_AddItemPurchse"
          options={{title: 'Offline Product'}}>
          {props => <OffLineItemMaster {...props} isOnline={true} />}
        </Stack.Screen>
        <Stack.Screen
          name="OffLine_ImportData"
          component={ImportData}
          options={{title: 'Import'}}
        />

        <Stack.Screen
          name="BussinessPartner"
          component={PartyMaster}
          // options={{title: 'Bussiness Partners'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Bussiness Partners',
          })}
        />
        <Stack.Screen
          name="AddPartyNavigator"
          component={AddParty}
          options={{title: 'Add Bussiness Partner'}}
        />
        <Stack.Screen
          name="SupplierHistoryNav"
          component={VendorHistorySummary}
          // options={{title: 'Partner History'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Partner History',
          })}
        />
        <Stack.Screen
          name="SupplierHistoryDetailNav"
          component={VendorHistoryDetail}
          options={{title: 'Details'}}
        />
        <Stack.Screen
          name="SupplierHistoryItemWiseDetailNav"
          component={VendorHistoryItemWise}
          options={{title: 'Invoice'}}
        />
        {/* Inventory */}
        <Stack.Screen
          name="InventoryHome"
          component={InventoryHome}
          options={{title: 'Inventory'}}
        />
        <Stack.Screen
          name="CourierNav"
          component={Courier}
          // options={{title: 'Couriers'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Courier',
          })}
        />
        <Stack.Screen
          name="AddCourierNavigator"
          component={AddCourier}
          options={{title: 'Courier'}}
        />
        <Stack.Screen
          name="LogisticNav"
          component={Logistic}
          // options={{title: 'Logistics'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Logistics',
          })}
        />
        <Stack.Screen
          name="AddLogisticsInNav"
          component={AddLogistic}
          options={{title: 'Logistic Inward'}}
        />
        <Stack.Screen
          name="StockAuditNav"
          component={StockAudit}
          // options={{title: 'Stock Audits'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Stock Audit',
          })}
        />
        <Stack.Screen
          name="AddStockAuditNav"
          component={AddStockAudit}
          options={{title: 'Stock Audit'}}
        />
        <Stack.Screen
          name="BarcodeHistoryNav"
          component={BarcodeHistory}
          // options={{title: 'Inventory Stock'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Inventory Stock',
          })}
        />
        <Stack.Screen
          name="PriceCheckNav"
          component={PriceCheck}
          // options={{title: 'Inventory Stock'}}
          options={({route}) => ({
            headerShown: false
          })}
        />
        <Stack.Screen
          name="TransctionDetailNav"
          component={TransctionDetail}
          options={{title: 'Stock Details'}}
        />
        {/* Sales */}
        <Stack.Screen
          name="SalesHome"
          component={SalesHome}
          options={{title: 'Sales'}}
        />
        <Stack.Screen
          name="SalesNavigator"
          component={Sales}
          // options={{title: 'Sales Order'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Sales',
          })}
        />
        <Stack.Screen
          name="AddSalesNavigator"
          component={AddSales}
          options={{title: 'Add Sales'}}
        />
        <Stack.Screen
          name="ReceivedRatioNavigator"
          component={ReceivedRatio}
          options={{title: 'Received Ratio'}}
        />
        <Stack.Screen
          name="CustomerHistoryNav"
          component={VendorHistorySummary}
          options={{title: 'Partner History'}}
        />
        <Stack.Screen
          name="CustomerHistoryDetailNav"
          component={VendorHistoryDetail}
          options={{title: 'Partner History Detail'}}
        />
        <Stack.Screen
          name="CustomerHistoryItemWiseDetailNav"
          component={VendorHistoryItemWise}
          options={{title: 'Invoice Detail'}}
        />
        {/* Accounts */}
        <Stack.Screen
          name="FincanceHomeNav"
          component={FinanceHome}
          options={{title: 'Finance'}}
        />
        <Stack.Screen
          name="TransNav"
          component={TransactionVoucher}
          // options={{title: 'Transaction Vouchers'}}
          options={({route}) => ({
            title: route.params.HeaderTitle || 'Transaction Voucher',
          })}
        />
        <Stack.Screen
          name="AddTransVoucherNavigator"
          component={AddTransactionVoucher}
          options={{title: 'Transaction Voucher'}}
        />
        <Stack.Screen
          name="AddAgainstNavigator"
          component={Against}
          options={{title: 'Against', headerShown: false}}
        />
        {/* Distribution */}
        {/* Package List */}
        <Stack.Screen
          name="PackageNav"
          component={Package}
          options={{title: 'Package'}}
        />
        {/* Package List */}
        <Stack.Screen
          name="AddPackageNav"
          component={AddPackage}
          options={{title: 'Add Package'}}
        />
        {/* Package List */}
        <Stack.Screen
          name="ReconcillationNav"
          component={Reconsilation}
          options={{title: 'Reconciliation'}}
        />
        {/* Package List */}
        <Stack.Screen
          name="AddReconsilationNav"
          component={AddReconsilation}
          options={{title: 'Add Reconciliation'}}
        />
      </Stack.Navigator>
    );
  }
}

export {MainNavigator};
