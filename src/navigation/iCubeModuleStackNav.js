import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import iCubeNavHome from './iCubeNavHome';

/* Default */
import Config from '../components/Config'
import LoginPage from '../components/LoginPage'

/* Common */
import VendorHistorySummary from '../components/Screens/Common/VendorHistorySummary';
import VendorHistoryDetail from '../components/Screens/Common/VendorHistoryDetail';
import VendorHistoryItemWise from '../components/Screens/Common/VendorHistoryItemWise';

/* Dashboard */ 
import DashboardHome from "../components/Screens/Dashboard/DashboardHome";
import DynamicDash from "../components/Screens/Dashboard/DynamicDash";

/* Retail */ 
import RetailHome from '../components/Screens/Retail/RetailHome';
import PeopleCount from '../components/Screens/Retail/PeopleCount';

/* Purchase */
import ProcurementHome from '../components/Screens/Procurement/ProcurenmentHome';
//Po
import Purchase from '../components/Screens/Procurement/Purchase';
import AddPurchase from '../components/Screens/Procurement/AddPurchase';
//item master
import ItemMaster from '../components/Screens/Procurement/ItemMaster/ItemMaster';
//Masters
import PartyMaster from '../components/Screens/Procurement/PartyMaster';
import AddParty from '../components/Screens/Procurement/AddParty';

/* Inventory */
import InventoryHome from '../components/Screens/Inventory/InventoryHome';
//Stock audit
import StockAudit from '../components/Screens//Inventory/StockAudit';
import AddStockAudit from '../components/Screens//Inventory/AddStockAudit';
//Courier
import Courier from '../components/Screens/Inventory/Courier';
import AddCourier from '../components/Screens/Inventory/AddCourier';
//Logistics
import Logistic from '../components/Screens/Inventory/Logistics';
import AddLogistic from '../components/Screens/Inventory/AddLogisticsIn';

/* WholeSale */
import SalesHome from '../components/Screens/Sales/SalesHome';
import Sales from '../components/Screens/Sales/Sales';
import AddSales from '../components/Screens/Sales/AddSales';
import ReceivedRatio from '../components/Screens/Common/ReceivedRatio';

/* Accounts */
import FinanceHome from '../components/Screens/Finance/FinanceHome';
import TransactionVoucher from '../components/Screens/Finance/TransactionVoucher';
import AddTransactionVoucher from '../components/Screens/Finance/AddTransactionVoucher';
import Against from '../components/Screens/Finance/Against';

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#9AC4F8",
  },
  headerStyle: {
    backgroundColor: '#093d62',
  },
  headerTintColor: '#ffffff',
  headerBackTitle: "Back",
};

class iCubeModuleStackNav extends React.Component  {
  constructor() {
    super();
  }
  
  render(){
   
  return (
    <Stack.Navigator initialRouteName = "DashHome" screenOptions={screenOptionStyle}>
    <Stack.Screen name="tabmainmodule" component={iCubeNavHome} options={{title: 'Home screen'}} />

      {/* Dashboard */}
      <Stack.Screen name="DashHome" component={DashboardHome} options={{title: 'Dashboard'}} />
      <Stack.Screen name="DDynamicNav" component={DynamicDash} options={{title: 'Dashboard 1'}} />

      {/* Retail */}
      <Stack.Screen name="RetailHomeNav" component={RetailHome} options={{title: 'Retail'}}/>
      <Stack.Screen name="PeopleCountNav" component={PeopleCount} options={{title: 'Foot Fall'}} />

      {/* Purchase */}
      <Stack.Screen name="PurchaseHome" component={ProcurementHome} options={{title: 'Purchase'}}/>
      <Stack.Screen name="PurchaseNav" component={Purchase} options={{title: 'Purchase'}} />
      <Stack.Screen name="AddPurchaseNav" component={AddPurchase} options={{title: 'Purchase'}} />
      <Stack.Screen name="AddItemPurchseNav" component={ItemMaster} options={{title: "Add Product"}}/>
      <Stack.Screen name="BussinessPartner" component={PartyMaster} options={{title: "Bussiness Partners"}}/>
      <Stack.Screen name="AddPartyNavigator" component={AddParty} options={{title: 'Bussiness Partner'}} />
      <Stack.Screen name="SupplierHistoryNav" component={VendorHistorySummary} options={{title: 'Partner History'}} />
      <Stack.Screen name="SupplierHistoryDetailNav" component={VendorHistoryDetail} options={{title: 'Partner History Details'}} />
      <Stack.Screen name="SupplierHistoryItemWiseDetailNav" component={VendorHistoryItemWise} options={{title: 'Invoice Details'}} />

      {/* Inventory */}
      <Stack.Screen name="InventoryHome" component={InventoryHome} options={{title: 'Inventory'}}/>
      <Stack.Screen name="CourierNav" component={Courier} options={{title: 'Couriers'}} />
      <Stack.Screen name="AddCourierNavigator" component={AddCourier} options={{title: 'Courier'}} />
      <Stack.Screen name="LogisticNav" component={Logistic} options={{title: "Logistics"}}/>
      <Stack.Screen name="AddLogisticsInNav" component={AddLogistic} options={{title: "Logistic Inward"}}/>
      <Stack.Screen name="StockAuditNav" component={StockAudit} options={{title: 'Stock Audits'}} />
      <Stack.Screen name="AddStockAuditNav" component={AddStockAudit} options={{title: 'Stock Audit'}} />

      {/* Sales */}
      <Stack.Screen name="SalesHome" component={SalesHome} options={{title: 'Sales'}}/>
      <Stack.Screen name="SalesNavigator" component={Sales} options={{title: 'Sales Order'}} />
      <Stack.Screen name="AddSalesNavigator" component={AddSales} options={{title: 'Sales Order'}} />
      <Stack.Screen name="ReceivedRatioNavigator" component={ReceivedRatio} options={{title: "Received Ratio"}}/>
      <Stack.Screen name="CustomerHistoryNav" component={VendorHistorySummary} options={{title: "Partner History"}}/>
      <Stack.Screen name="CustomerHistoryDetailNav" component={VendorHistoryDetail} options={{title: 'Partner History Detail'}} />
      <Stack.Screen name="CustomerHistoryItemWiseDetailNav" component={VendorHistoryItemWise} options={{title: 'Invoice Detail'}} />
      
      {/* Accounts */}
      <Stack.Screen name="FincanceHomeNav" component={FinanceHome} options={{title: 'Finance'}}/>
      <Stack.Screen name="TransNav" component={TransactionVoucher} options={{title: 'Transaction Vouchers'}} />
      <Stack.Screen name="AddTransVoucherNavigator" component={AddTransactionVoucher} options={{title: 'Transaction Voucher'}} />
      <Stack.Screen name="AddAgainstNavigator" component={Against} options={{title: "Against"}}/>

    </Stack.Navigator>
  );
  
};
};

export { iCubeModuleStackNav };
