import {
  Alert,
  Dimensions,
  UIManager,
  findNodeHandle,
  Keyboard,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TinyToast from 'react-native-tiny-toast';
import CryptoJS from './EncryptionAlgorithm/aes';
import RNFetchBlob from 'rn-fetch-blob';
import Share from 'react-native-share';
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';
import NetInfo from '@react-native-community/netinfo';
import RNPrint from 'react-native-print';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';

const Encrypter = {};
const Request = {};

export const DocumentDirectoryFolder = {
  ProductImage: RNFetchBlob.fs.dirs.DocumentDir + '/ProductImage/',
  ShareDocument: RNFetchBlob.fs.dirs.DocumentDir + '/ShareDocument/',
  TransferDocument: RNFetchBlob.fs.dirs.DocumentDir + '/Transfer/',
};

Encrypter.EncryptCredentials = (Username, Password) => {
  var key = CryptoJS.enc.Utf8.parse('5656565656565656');
  var iv = CryptoJS.enc.Utf8.parse('5656565656565656');

  var encryptedUsername = CryptoJS.AES.encrypt(
    CryptoJS.enc.Utf8.parse(Username),
    key,
    {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    },
  );

  var encryptedPassword = CryptoJS.AES.encrypt(
    CryptoJS.enc.Utf8.parse(Password),
    key,
    {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    },
  );
  console.log(encryptedUsername, encryptedPassword);
  return {encryptedUsername, encryptedPassword};
};

export const GetSessionData = async () => {
  let [
    [, IP],
    [, Token],
    [, UserCode],
    [, Location],
    [, FinancialYearId],
    [, RoleId],
    [, DateFormat],
    [, UserId],
    [, isLogin],
    [, UserName],
    [, ProfileId],
    [, LoginDateTime],
    [, POSPrintTerminalName],
  ] = await AsyncStorage.multiGet([
    'IP',
    'Token',
    'UserCode',
    'LocationCode',
    'FinancialYearId',
    'RoleId',
    'DateFormat',
    'UserId',
    'isLogin',
    'UserName',
    'ProfileId',
    'LoginDateTime',
    'POSPrintTerminalName',
  ]);
  return {
    get_IP: IP,
    get_Token: Token,
    get_UserCode: UserCode,
    get_Location: Location,
    get_FinancialYearId: FinancialYearId,
    get_RoleId: RoleId,
    get_DateFormat: DateFormat,
    get_UserId: UserId,
    get_isLogin: isLogin,
    get_UserName: UserName,
    get_ProfileId: ProfileId,
    get_LoginDateTime: LoginDateTime,
    get_POSPrintTerminalName: POSPrintTerminalName,
  };
};

export const getIPAndTokenFromAsyncStorage = async () => {
  const [[, IP], [, Token]] = await AsyncStorage.multiGet(['IP', 'Token']);
  return {IP, Token};
};

export const getItemsFromAsyncStorage = async itemArray => {
  const mappedValues = await (
    await AsyncStorage.multiGet(itemArray)
  ).reduce((prev, cur) => prev.set(cur[0], cur[1]), new Map());
  const result = {};
  itemArray.forEach(elem => {
    result[elem] = mappedValues.get(elem);
  });
  return result;
};

Request.get = async (url, Token) => {
  // console.log('mmm uri', url);
  const res = await fetch(url, {
    headers: {
      Authorization: Token,
    },
  });
  const json = await res.json();
  return {
    status: res.status,
    data: json,
  };
};

Request.get_Xml = async (url, Token) => {
  // console.log('mmm uri', url);
  const res = await fetch(url, {
    headers: {
      Authorization: Token,
    },
  });
  const json = await res.text();
  return {
    status: res.status,
    data: json,
  };
};

Request.get_File = async (url, Token) => {
  return await fetch(url, {
    headers: {
      Authorization: Token,
    },
  });
};

Request.post_ReturnObject = async (url, data, Token) => {
  try {
    const res = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Authorization: Token,
      },
    });
    const json = await res.json();
    return {
      status: res.status,
      data: json,
    };
  } catch (err) {
    console.error(`Error :  ${JSON.stringify(err)}`);
    return {
      status: 500,
      data: [],
    };
  }
};

Request.post = async (url, data, Token) => {
  return await fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Authorization: Token,
    },
  });
};

Request.login = async (url, data) => {
  const res = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    body: data,
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });
  return await res.json();
};

Request.fetchAccessForms = async () => {
  const [[, IP], [, Token], [, RoleId = '']] = await AsyncStorage.multiGet([
    'IP',
    'Token',
    'RoleId',
  ]);
  // if(IP && Token && RoleId !== ""){
  return (
    IP &&
    Token &&
    RoleId !== '' &&
    Request.get(
      `${IP}/api/common/FormsAccessForUserRole?RoleId=${RoleId}`,
      Token,
    )
      .then(HandleResponseStatus)
      .then(data => data)
      .catch(AlertError)
  );
  // } else return undefined;
};

export const LogoutUser = async callback => {
  await AsyncStorage.multiRemove(
    [
      'UserCode',
      'UserId',
      'RoleId',
      'UserName',
      'EmployeeName',
      'Token',
      'SessionId',
      'ProfileId',
      'FinancialYearId',
      'TotalClicks',
      'SlabWiseClicks',
      'isLogin',
      'LoginDateTime',
    ],
    callback,
  );
};

export const getFormsForModule = async MIndex => {
  const forms = await Request.fetchAccessForms();
  return forms && forms.filter(elem => elem.MINDEX === MIndex);
};

export const getFormsForModuleWithoutFilter = async () => {
  const forms = await Request.fetchAccessForms();
  return forms;
};

export const GroupValuesByKey = (arr, key) => {
  return arr.reduce((prev, cur) => {
    const curKey = cur[key];
    (prev[curKey] && prev[curKey].push(cur)) || (prev[curKey] = [cur]);
    return prev;
  }, {});
};

const prepareGroupedFields = data => {
  const groupedFields = {};
  data.map(grp => {
    groupedFields[grp.Group] = grp.Fields.sort(sortConfigFields);
  });
  return groupedFields;
};

const sortConfigFields = (a, b) => {
  return a.Order - b.Order;
};

const handleGroupFieldsResponse = res => {
  if (res.status === 200) {
    if (res.data.length) {
      return prepareGroupedFields(res.data);
    } else {
      AlertMessage('No Configured Fields found');
    }
  } else {
    AlertStatusError(res);
  }
};

Request.loadConfiguredFields = async callFrom => {
  const IP = await AsyncStorage.getItem('IP');
  const Token = await AsyncStorage.getItem('Token');
  const res = await Request.get(
    `${IP}/api/FieldConfiguration/GetVisibleFieldsForForm?CallFrom=Mobile&FormName=${callFrom}`,
    Token,
  );
  return handleGroupFieldsResponse(res);
};

//  to get specific properties from an array of Objects
export const getPropsFromArray = (arr, props) => {
  let result = {};
  const returnProps = [...props];
  returnProps.map(p => {
    result[p] = [];
  });
  [...arr].map(i => {
    returnProps.map(p => result[p].push(i[p] || 'Property Not Found'));
  });
  return result;
};

export const ShowToastMsg = (msg, duration = 'SHORT') =>
  TinyToast.show(msg, {duration: TinyToast.duration[duration]});

export const ShowSuccessToastMsg = (msg, duration = 'SHORT') =>
  TinyToast.showSuccess(msg, {duration: TinyToast.duration[duration]});

export const HandleResponseStatus = res => {
  if (res.status === 200) {
    return res.data;
  } else {
    AlertStatusError(res);
  }
};

export const RoundValue = (passvalue, roundby = 2) => {
  return (Math.round(passvalue * 100) / 100).toFixed(roundby);
};

export const AlertMessage = (msg, callback) =>
  Alert.alert('iCube Alert', msg, [
    {text: 'Ok', onPress: () => callback && callback()},
  ]);
export const Confirmation = (msg, callback) => {
  Alert.alert('iCube Alert', msg, [
    {text: 'Cancel'},
    {text: 'Ok', onPress: callback},
  ]);
};
const timeoutPromise = (ms, promise) => {
  return new Promise((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      reject(new Error('timeoutPromise : timeout.'));
    }, ms);
    promise.then(
      res => {
        clearTimeout(timeoutId);
        resolve(res);
      },
      err => {
        clearTimeout(timeoutId);
        reject(err);
      },
    );
  });
};
export const IsInternetAndServerConnected = async (
  isShowErrorMessage = false,
) => {
  let isConnect = await NetInfo.fetch().then(state => {
    if (state.isInternetReachable) {
      return true;
    } else {
      return false;
    }
  });
  if (isConnect) {
    const {get_IP} = await GetSessionData();
    isConnect = await timeoutPromise(5000, fetch.bind(this, get_IP))
      .then(() => {
        return true;
      })
      .catch(() => {
        return false;
      });
  }
  if (isShowErrorMessage && !isConnect) {
    AlertMessage('Could not connect to Internet (or) server.');
  }
  return isConnect;
};
export const IsServerConnected = async (
  isShowErrorMessage = false,
  passip = '',
) => {
  let ConnectIp = passip;
  if (isnull(ConnectIp, '') == '') {
    const {get_IP} = await GetSessionData();
    ConnectIp = get_IP;
  }
  // let isConnect = await NetInfo.fetch().then(state => {
  //   if (state.isInternetReachable) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // });
  // console.log('connect ip : ', ConnectIp);
  let isConnect = await timeoutPromise(7000, fetch(ConnectIp))
    .then(res => {
      // console.log('res ip : ', res.status, ConnectIp);
      return res.status == '200';
    })
    .catch(err => {
      console.log('err : ', err);
      return false;
    });
  if (isShowErrorMessage && !isConnect) {
    AlertMessage('Could not connect to Internet (or) server.');
  }
  return isConnect;
};
export const ConfirmationWithAllInput = (
  msg,
  okText,
  canceltext,
  okcallback,
  cancelcallback,
) => {
  Alert.alert('iCube Alert', msg, [
    {text: okText, onPress: okcallback},
    {text: canceltext, onPress: cancelcallback},
  ]);
};
export const AlertError = err => {
  AlertMessage(`Something went wrong\n${JSON.stringify(err, null, 2)}`);
  return err;
};
export const AlertStatusError = ({status, data}) =>
  AlertMessage(`${status} - ${data.Message}`);

export const dismissKeyboard = () => Keyboard.dismiss();

// to calculate the Orientation of the device and to check whether or not Orientation changed
export const getOrientation = wasPotrait => {
  const {width, height} = Dimensions.get('window');
  const isPotrait = height - width > 0;
  return {
    width,
    height,
    isPotrait,
    orientationChanged: wasPotrait !== isPotrait,
  };
};

// to calculate width of the control based on the layout ( width & height )
export const getResponsiveLayout = wasPotrait => {
  const {width, height, isPotrait, orientationChanged} =
    getOrientation(wasPotrait);
  let dynamicWidth,
    dynamicMiniWidth,
    dynamicInputFont,
    dynamicLabelFont,
    dynamicCardItemWidth,
    dynamicCardFixedItemWidth,
    dynamicFieldContainerCount;
  if (width < 600) {
    dynamicWidth = 100;
    dynamicFieldContainerCount = 1;
    dynamicMiniWidth = 33;
    dynamicInputFont = 14;
    dynamicLabelFont = 12;
    dynamicCardItemWidth = 25;
    dynamicCardFixedItemWidth = 100;
  } else if (width < 700) {
    dynamicWidth = 50;
    dynamicFieldContainerCount = 2;
    dynamicMiniWidth = 20;
    dynamicInputFont = 14;
    dynamicLabelFont = 12;
    dynamicCardItemWidth = 20;
    dynamicCardFixedItemWidth = 40;
  } else if (width < 960) {
    dynamicWidth = 50;
    dynamicFieldContainerCount = 2;
    dynamicMiniWidth = 10;
    dynamicInputFont = 16;
    dynamicLabelFont = 13;
    dynamicCardItemWidth = 16.6;
    dynamicCardFixedItemWidth = 35;
  } else {
    dynamicWidth = 33.33;
    dynamicFieldContainerCount = 3;
    dynamicMiniWidth = 10;
    dynamicInputFont = 18;
    dynamicLabelFont = 14;
    dynamicCardItemWidth = 10;
    dynamicCardFixedItemWidth = 22;
  }
  return {
    width,
    height,
    isPotrait,
    dynamicWidth,
    dynamicMiniWidth,
    dynamicInputFont,
    dynamicLabelFont,
    dynamicCardItemWidth,
    dynamicCardFixedItemWidth,
    orientationChanged,
    dynamicFieldContainerCount,
  };
};

// to calculate the position of the dropDown whether at the top of the textInput or at the bottom of the textInput based on the position of the Input
export const calculateDropdownPosition = async (
  containerRef,
  compRef,
  callBack,
) => {
  const container = findNodeHandle(containerRef);
  const comp = findNodeHandle(compRef);
  UIManager.measure(container, (px, py, pw, ph, ppx, ppy) => {
    UIManager.measure(comp, (cx, cy, cw, ch, cpx, cpy) => {
      const actualY = cpy - ppy;
      if (ph - (actualY + ch) < 75 && actualY > 75) {
        callBack({bottom: '100%'});
      } else {
        callBack({top: '100%'});
      }
    });
  });
};

// to calculate the dimension of Image to have three images in a row
export const getLayoutImageDimension = () => {
  const {width} = Dimensions.get('window');
  return width / 3;
};

// to Get Device UniqueId
export const DeviceUniqueId = () => {
  return DeviceInfo.getUniqueId();
};

// to Get Device MacAddress
export const DeviceMacAddress = () => {
  return DeviceInfo.getUniqueId();
};

// to Get Device Name
export const MobileDeviceName = async () => {
  let return_name = await DeviceInfo.getDeviceName().then(name => {
    return_name = name;
    return name;
  });
  return return_name;
};

export const GetGlobalSettings = async Type => {
  let ReturnData = {};
  const {get_IP, get_Token} = await GetSessionData();
  let res = await Request.get(
    `${get_IP}/api/Common/GetGlobalSettings?CallFrom=${Type}`,
    get_Token,
  );
  if (res.status === 200) {
    if (res.data) {
      ReturnData = res.data;
    }
  }
  return ReturnData;
};

export const CheckCurrentSession = async (IP, sessionId) => {
  let isValidResponse = false,
    isInvlaid = false,
    IsNeedtoClose = false,
    ValidateString = '',
    ValidateReturnString = '';
  if (IP != '' && sessionId != 0 && sessionId != '' && sessionId != null) {
    let res = await Request.get(
      `${IP}/api/Common/ValidateSession?PassCurrentDate=${moment(
        new Date(),
      ).format('YYYY-MM-DD')}&SessionId=${sessionId}`,
      '',
    );
    if (res.status === 200) {
      if (res.data) {
        isValidResponse = true;
        isInvlaid = res.data.isInvlaid;
        IsNeedtoClose = res.data.IsNeedtoClose;
        ValidateString = res.data.ValidateString;
        ValidateReturnString = res.data.ValidateReturnString;
      }
    }
  }
  return {
    isValidResponse,
    isInvlaid,
    IsNeedtoClose,
    ValidateString,
    ValidateReturnString,
  };
};
export const DeleteSelectedInvoice = async (
  CallFrom,
  FromIndex,
  invoiceid,
  Loccode,
  RoleId,
  UserCode,
  StartLoadState,
  RefreshListMethod,
) => {
  const {get_IP, get_Token} = await GetSessionData();
  await Confirmation('Do you want to delete ?', () => {
    let uri = `${get_IP}/api/Common/DeleteSelectedInvoice?CallFrom=${CallFrom}&FromIndex=${FromIndex}&InvoiceId=${invoiceid}&LocCode=${Loccode}&RoleId=${RoleId}&EmpId=${UserCode}`;
    console.log('delete uri : ', uri);
    StartLoadState && StartLoadState();
    Request.get(uri, get_Token)
      .then(res => {
        let GetresData = res.data;
        if (res.status === 200) {
          console.log('delete object : ', GetresData);
          if (GetresData) {
            if (GetresData.Message == 'Saved') {
              AlertMessage('Deleted successfully.', RefreshListMethod);
            } else {
              AlertMessage(
                GetresData.Description
                  ? GetresData.Description
                  : 'Failed to delete.',
              );
            }
          }
        } else {
          AlertMessage(
            GetresData.Description
              ? GetresData.Description
              : `Failed to delete : \n ${JSON.stringify(GetresData)}`,
          );
        }
      })
      .catch(err => AlertError(err));
  });
};

export const monthNames = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export const SqlDateFormat = 'yyyy-mm-dd';

export const nullif = (passvalue, nullifvalue) => {
  let returnvalue = passvalue;
  if (passvalue == nullifvalue) {
    returnvalue = null;
  }
  return returnvalue;
};

export const nullif_Number = (passvalue, nullifvalue) => {
  let returnvalue = passvalue;
  if (Number(passvalue) == Number(nullifvalue)) {
    returnvalue = null;
  }
  return returnvalue;
};

export const isnull = (passvalue, defaultvalue) => {
  let returnvalue = defaultvalue;
  if (passvalue != undefined && passvalue != '' && passvalue != null && passvalue != 'undefined' && passvalue != 'null') {
    returnvalue = passvalue;
  }
  return returnvalue;
};

export const isnull_bool = (passvalue, defaultvalue) => {
  let returnvalue = defaultvalue;
  if (passvalue != undefined && String(passvalue) != '' && passvalue != null && String(passvalue) != 'undefined' && String(passvalue) != 'null') {
    returnvalue = passvalue;
  }
  return Boolean(returnvalue);
};

export const isnull_number = (passvalue, defaultvalue) => {
  let returnvalue = defaultvalue;
  if (passvalue != undefined && passvalue != '' && passvalue != null) {
    returnvalue = passvalue;
  }
  return Number(returnvalue);
};

export const ReturnDisplayStringForDate = (passdate, displayformat) => {
  let FinalString = '';
  if (passdate) {
    let FullDate = passdate ? new Date(passdate) : '';
    displayformat = displayformat ? displayformat : 'dd/mm/yyyy';
    // console.log('my data displayformat : ', displayformat);
    let seperator = displayformat.includes('/') ? '/' : '-';
    const var_FormatList = displayformat.split(seperator);
    let GetTotalLenght = var_FormatList ? var_FormatList.length : 0;
    for (let loopcount = 0; loopcount < GetTotalLenght; loopcount++) {
      let getFormatstring = var_FormatList[loopcount].toLowerCase();
      let getvaluestring = '';
      if (getFormatstring == 'dd') {
        getvaluestring = `${('0' + FullDate.getDate()).slice(-2)}`;
      }
      if (getFormatstring == 'mm') {
        getvaluestring = `${('0' + (FullDate.getMonth() + 1)).slice(-2)}`;
      }
      if (getFormatstring == 'mmm') {
        getvaluestring = monthNames[FullDate.getMonth()];
      }
      if (getFormatstring == 'yy') {
        getvaluestring = `${('0' + FullDate.getFullYear()).slice(-2)}`;
      }
      if (getFormatstring == 'yyyy') {
        getvaluestring = FullDate.getFullYear();
      }
      if (getvaluestring && getvaluestring != '') {
        FinalString += `${getvaluestring}${
          loopcount + 1 != GetTotalLenght ? seperator : ''
        }`;
      }
    }
  }
  return FinalString;
};

export const isBluetoothPrinterConnected = async (
  filter_PrintName,
) => {
  let get_isConnect = await BLEPrinter.init().then(async ()  => {
    return await BLEPrinter.getDeviceList()
      .then(async printer => {
        let get_PrintList = printer || [];
        if (get_PrintList.length > 0) {
          let selectedPrinter = get_PrintList.find(
            printer =>
              printer.device_name == filter_PrintName || filter_PrintName == '',
          );
          if (selectedPrinter) {
           return await BLEPrinter.connectPrinter(selectedPrinter.inner_mac_address)
              .then(() => {
                return true;
              }).catch(err => { console.error(err); return false;});
          } else {
            return false;
          }
        }
      }).catch(err => { console.error(err); return false;});
  }).catch(err => { console.error(err); return false;});
  return get_isConnect ? true : false;
};

export const CallBluetoothPrint = async (
  filter_PrintName,
  PrintCount,
  printdata,
) => {
  BLEPrinter.init().then(() => {
    // console.log('call printBill inti : ', MemoType);
    BLEPrinter.getDeviceList()
      .then(printer => {
        let get_PrintList = printer || [];
        if (get_PrintList.length > 0) {
          let selectedPrinter = get_PrintList.find(
            printer =>
              printer.device_name == filter_PrintName || filter_PrintName == '',
          );
          if (selectedPrinter) {
            // console.log('filter_PrintName :', filter_PrintName, ' PrintCount : ', PrintCount, ' printdata : ', printdata);
            BLEPrinter.connectPrinter(selectedPrinter.inner_mac_address)
              .then(res => {
                do {
                  PrintCount--;
                  BLEPrinter.printText(printdata);
                } while (PrintCount > 0);
              })
              .catch(err => AlertMessage(err));
          } else {
            AlertMessage(`Invalid Printer '${filter_PrintName}'.`);
          }
        } else {
          AlertMessage('No Printer available.');
        }
      })
      .catch(err => AlertMessage(err));
  });
};

// Share file based on callinf from
export const sharefileForCallFRom = async (
  CallFrom,
  InvoiceID,
  InvoiceNO,
  InvoiceDate,
  FormIndex,
  Loccode,
  isPrint = false,
) => {
  try {
    let isAllow = false;

    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'External Permission',
          message: 'To Save File.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        isAllow = true;
      }
    } else {
      isAllow = true;
    }

    if (isAllow) {
      // let extension = isPrint ? 'html' : 'pdf';
      let extension = 'pdf';
      const [[, IP], [, Token]] = await AsyncStorage.multiGet(['IP', 'Token']);
      try {
        let filepath =
          DocumentDirectoryFolder.ShareDocument +
          // '/' +
          CallFrom +
          '/' +
          InvoiceNO +
          '_' +
          InvoiceDate +
          '.' +
          extension;
        // console.log(
        //   `${IP}/api/Common/CallPrint?PassPindex_Cindex_GCindex=${FormIndex}&FileId=0&FormSaveID=${InvoiceID}&FormSaveCode=${InvoiceNO}&FormSaveDate=${InvoiceDate}&FormSaveLoccode=${Loccode}&FormExtraString1=${CallFrom}&ContentTypeName=${extension}&LocationCode=${Loccode}`,
        //   ' path ',
        //   Token,
        // );

        // AlertMessage('dowbload dir : ' + filepath);
        await RNFetchBlob.config({
          fileCache: true,
          title: CallFrom + ' - ' + InvoiceNO,
          appendExt: extension,
          // response data will be saved to this path if it has access right.
          path: filepath,
        })
          .fetch(
            'GET',
            `${IP}/api/Common/CallPrint?PassPindex_Cindex_GCindex=${FormIndex}&FileId=0&FormSaveID=${InvoiceID}&FormSaveCode=${InvoiceNO}&FormSaveDate=${InvoiceDate}&FormSaveLoccode=${Loccode}&FormExtraString1=${CallFrom}&ContentTypeName=${extension}&LocationCode=${Loccode}`,
            {
              Authorization: Token,
            },
          )
          .then(async res => {
            // console.log('res : ', JSON.stringify(res));
            if (isPrint) {
              // var get_HtmlData = await res.text();
              // console.log('result path : ', res.path());
              await RNPrint.print({filePath: res.path()});
              // await RNPrint.print({html: get_HtmlData});
            } else {
              // console.log('share : ');
              // AlertMessage('result path : ' + res.path());
              await Share.open({
                filename: InvoiceNO,
                title: CallFrom + ' Report',
                message: CallFrom + ' - ' + InvoiceNO,
                url:
                  Platform.OS === 'android'
                    ? 'file://' + res.path()
                    : '' + res.path(),
                subject: 'Report',
              });
              // AlertMessage('share completeeeee');
            }
          })
          .catch(err => {
            console.error(err);
            //  AlertError('Error while share',err);
          });
      } catch (err) {
        console.error(err);
        AlertMessage('Error while generate file.');
      }
    } else {
      console.log('Camera permission denied');
      // AlertMessage('Camera permission denied');
    }
  } catch (err) {
    console.error(err);
    // AlertError('Error while method',err);
  }
};

// Share file based on callinf from
export const sharefileForCallFRomAPI = async (
  CallFrom,
  URL,
  FileName,
  Extension = 'pdf',
) => {
  try {
    let isAllow = false;

    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'External Permission',
          message: 'To Save File.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        isAllow = true;
      }
    } else {
      isAllow = true;
    }

    if (isAllow) {
      // let extension = isPrint ? 'html' : 'pdf';
      const [[, IP], [, Token]] = await AsyncStorage.multiGet(['IP', 'Token']);
      //console.log('URL: ', URL, ' Toket : ', Token);
      try {
        let filepath =
          DocumentDirectoryFolder.ShareDocument +
          CallFrom +
          '/' +
          FileName +
          '.' +
          Extension;
        // AlertMessage('dowbload dir : ' + filepath);
        await RNFetchBlob.config({
          fileCache: true,
          title: CallFrom + ' - ' + FileName,
          appendExt: Extension,
          // response data will be saved to this path if it has access right.
          path: filepath,
        })
        .fetch(
          'GET',
          URL,
          {
            Authorization: Token,
          },
        )
          .then(async res => {
            //console.log('res : ', JSON.stringify(res));
              // console.log('share : ');
              // AlertMessage('result path : ' + res.path());
              await Share.open({
                filename: FileName,
                title: CallFrom + ' Report',
                message: CallFrom + ' - ' + FileName,
                url:
                  Platform.OS === 'android'
                    ? 'file://' + res.path()
                    : '' + res.path(),
                subject: 'Report',
              });
          })
          .catch(err => {
            console.error(err);
          });
      } catch (err) {
        console.error(err);
        AlertMessage('Error while generate file.');
      }
    } else {
      console.log('Camera permission denied');
      AlertMessage('Error permission denied.');
    }
  } catch (err) {
    console.error(err);
  }
};

export const GetNewUUID = () => {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
    /[xy]/g,
    function (c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
    },
  );
  return uuid;
};

export {Encrypter};
export {Request};
