export const globalColorObject = {
  Color: {
    // Green
    Primary: '#33AAE1', //#26B1AD
    Lightprimary: '#F2F9FD', //#F5FBFB

    // // Blue
    // Primary: '#00008B',
    // Lightprimary: '#DDDDF0',

    // // Red
    // Primary: 'maroon',
    // Lightprimary: '#FFDDDD',

    oppPrimary: 'white',
    BlackColor: 'black',
    GrayColor: 'gray',
    OffLineStatus: 'red',
    LightGrayColor: 'lightgray',
  },
  ColorPropetyType: {
    BackgroundColor: 'backgroundColor',
    Color: 'color',
    BorderColor: 'borderColor',
    BorderBottomColor: 'borderBottomColor',
    BorderTopColor: 'borderTopColor',
  },
};
export const globalFontObject = {
  Font: {
    Regular: 'DMSans-Regular',
    Bold: 'DMSans-Bold',
  },
  Size: {
    small: {
      ss: 10,
      sm: 12,
      sl: 14,
      sxl: 16,
      sxxl: 18,
    },
    mediam: {
      ms: 20,
      mm: 22,
      ml: 24,
      mxl: 26,
      mxxl: 28,
    },
    large: {
      ls: 30,
      lm: 32,
      ll: 34,
      lxl: 36,
      lxxl: 38,
    },
    extraLarge: {
      xls: 40,
      xlm: 42,
      xll: 44,
      xlxl: 46,
      xlxxl: 48,
    },
  },
};

export const globalBorderWidth = 1;
/**
 * Apply Style FontSize And Color description
 * @param {globalFontObject.Font} fontStyle
 * @param {globalFontObject.Size} fontSize
 * @param {globalColorObject.Color} color
 * @param {globalColorObject.ColorPropetyType} colorType
 * @example
 * // returns "fooBar fooBar"
 * ApplyStyleFontAndSizeAndColor('Regular', '10', '#26B1AD', 'backgroundColor')
 * @returns {Object}
 */
export const ApplyStyleFontAndSizeAndColor = (
  fontStyle,
  fontSize,
  color,
  colorPropertyType,
) => {
  let returnobject = {};
  if (fontStyle) {
    returnobject.fontFamily = fontStyle;
  }
  if (fontSize) {
    returnobject.fontSize = fontSize;
  }
  if (color && colorPropertyType) {
    returnobject[colorPropertyType] = color;
  }
  // console.log('my color property : ', returnobject);
  return returnobject;
};

/**
 * ApplyStyleFontAndSize description
 * @param {globalFontObject.Font} fontStyle
 * @param {globalFontObject.Size} fontSize
 * @example
 * // returns "fooBar fooBar"
 * ApplyStyleFontAndSize('Regular', '10')
 * @returns {Object}
 */
export const ApplyStyleFontAndSize = (fontStyle, fontSize) => {
  let returnobject = {};
  if (fontStyle) {
    returnobject.fontFamily = fontStyle;
  }
  if (fontSize) {
    returnobject.fontSize = fontSize;
  }
  // console.log('my ApplyStyleFontAndSize property : ', returnobject);
  return returnobject;
};

/**
 * Apply Style Color
 * @param {globalColorObject.Color} color
 * @param {globalColorObject.ColorPropetyType} colorType
 * @example
 * // returns "fooBar fooBar"
 * ApplyStyleColor('#26B1AD', 'backgroundColor')
 * @returns {Object}
 */
export const ApplyStyleColor = (color, colorPropertyType) => {
  let returnobject = {};
  if (color && colorPropertyType) {
    returnobject[colorPropertyType] = color;
  }
  // console.log('my ApplyStyleColor property : ', returnobject);
  return returnobject;
};
