import React, {Component} from 'react';
import {StatusBar, Text, Modal, View} from 'react-native';

// import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {MainNavigator} from './src/navigation/MainNavigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  CheckCurrentSession,
  AlertMessage,
  GetSessionData,
  IsServerConnected,
  isnull,
} from './src/Helpers/HelperMethods';
import {
  globalColorObject,
  globalFontObject,
  ApplyStyleFontAndSize,
} from './src/Style/GlobalStyle';
import {Button} from './src/components/Custom/Button';
import {MainNavigatorOffLine} from './src/navigation/MainNavigatorOffLine';

// export const myAppContext = React.createContext({
//   sessionData: {},
//   changeSessionData: () => {},
// });

class App extends Component {
  constructor() {
    super();
    // this.changeSessionData = passSessionData => {
    //   console.log('call change session : ', passSessionData);
    //   this.setState({sessionData: passSessionData});
    // };
    this.state = {
      isLogin: false,
      isShowConfirm: false,
      isConnected: false,
      InitialRoutePage: 'LoginPageNav',
      AppType: 'Online',
      // sessionData: {},
      // changeSessionData: this.changeSessionData,
    };
  }
  intervalID = null;

  runAppinDiffType = passType => {
    this.setState(prevstate => ({
      ...prevstate,
      AppType: passType,
      InitialRoutePage: 'LoginPageNav',
    }));
  };

  runAppSessionInterval = callback => {
    if (this.intervalID == null) {
      this.ApplyConnectionStatus();
      this.intervalID = setInterval(
        this.getSessionStatus.bind(this, callback),
        60000,
      );
    }
  };

  stopAppSessionInterval = () => {
    console.log('call session stop : ', this.intervalID);
    clearInterval(this.intervalID);
    this.intervalID = null;
  };

  ApplyConnectionStatus = async () => {
    let Ret_isConnected = await IsServerConnected(false);
    this.setState(prevstate => ({
      ...prevstate,
      isConnected: Ret_isConnected,
      isShowConfirm: !Ret_isConnected,
      AppType: Ret_isConnected ? 'Online' : prevstate.AppType,
    }));
    return Ret_isConnected;
  };

  componentDidMount() {
    this.initialLoad();
  }

  initialLoad = async () => {
    const {get_isLogin, get_IP} = await GetSessionData();
    this.setState({
      isLogin: get_isLogin,
    });
    if (isnull(get_IP, '') != '') {
      this.ApplyConnectionStatus();
    }
  };

  getSessionStatus = async callback => {
    const [[, get_IP], [, get_SessionId]] = await AsyncStorage.multiGet([
      'IP',
      'SessionId',
    ]);
    console.log(get_SessionId, 'APP nav timer start : ', new Date().toString());
    if (get_SessionId != 0 && this.state.isConnected) {
      const {
        isValidResponse,
        isInvlaid,
        IsNeedtoClose,
        ValidateString,
        ValidateReturnString,
      } = await CheckCurrentSession(get_IP, get_SessionId);
      if (isValidResponse) {
        if (isInvlaid) {
          AlertMessage(
            ValidateReturnString ? ValidateReturnString : ValidateString,
          );
          if (IsNeedtoClose) {
            console.log('isNeed to clsoe');
            this.stopAppSessionInterval();
            callback && callback();
          }
        }
      }
    }
  };

  componentWillUnmount() {
    console.log('close app timer!');
    clearInterval(this.intervalID);
  }

  render() {
    // console.log('App state : ', this.state);
    return (
      <>
        {/* <myAppContext.Provider value={this.state}> */}
        <StatusBar
          backgroundColor={
            this.state.isConnected && this.state.AppType == 'Online'
              ? globalColorObject.Color.Primary
              : globalColorObject.Color.OffLineStatus
          }
          barStyle="default"
        />

        {!this.state.isShowConfirm &&
          (this.state.AppType == 'Online' ? (
            <NavigationContainer>
              <MainNavigator
                InitialRoutePage={this.state.InitialRoutePage}
                runAppSessionInterval={this.runAppSessionInterval}
                stopAppSessionInterval={this.stopAppSessionInterval}
                runAppinDiffType={this.runAppinDiffType}
              />
            </NavigationContainer>
          ) : (
            <NavigationContainer>
              <MainNavigatorOffLine runAppinDiffType={this.runAppinDiffType} />
            </NavigationContainer>
          ))}

        <Modal
          transparent={true}
          animationType="fade"
          visible={this.state.isShowConfirm}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              paddingHorizontal: 10,
            }}>
            <Text
              style={[
                ApplyStyleFontAndSize(
                  globalFontObject.Font.Bold,
                  globalFontObject.Size.mediam.ms,
                ),
              ]}>
              Could not connect to internet.
              {this.state.isLogin && 'Do you want to go offline?'}
            </Text>
            <Text />
            <Button value="Retry" onPressEvent={this.ApplyConnectionStatus} />
            <Button
              value="Config"
              onPressEvent={() =>
                this.setState({
                  isShowConfirm: false,
                  AppType: 'Online',
                  InitialRoutePage: 'Config',
                })
              }
            />
            {this.state.isLogin && (
              <Button
                value="OffLine"
                onPressEvent={() =>
                  this.setState({
                    isShowConfirm: false,
                    AppType: 'Offline',
                  })
                }
              />
            )}
          </View>
        </Modal>
        {/* </myAppContext.Provider> */}
      </>
    );
  }
}

export default App;
